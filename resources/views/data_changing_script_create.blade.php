<script type="text/javascript">
	$(document).ready(function() {

  $('#btnAdd').click(function(){  
      //$('#options_tagsinput').remove(); 
      var num   = $('.clonedInput').length; // how many "duplicatable" input fields we currently have
      var newNum  = new Number(num + 1);    // the numeric ID of the new input field being added
      

      // create the new element via clone(), and manipulate it's ID using newNum value
      var newElem = $('#input' + num).clone().attr('id', 'input' + newNum);
      
      // manipulate the name/id values of the input inside the new element
      //newElem.find('#option_row').attr('id', 'option_row' + newNum);
      // newElem.find('.troptions').attr('id', 'option_row' + newNum);
     
      newElem.find('#label_rt label').attr('for', 'rate_name' + newNum);
      // newElem.find('#input_rt input').attr('name', 'rate[name' + newNum+']').val('');
      // newElem.find('#option_row' + newNum).hide();

      newElem.find('#label_max_rt label').attr('for', 'rate_max' + newNum);
      // newElem.find('#input_max_rt input').attr('name', 'rate[max_rate' + newNum+']').val('');

      newElem.find('#label_min_rt label').attr('for', 'rate_min' + newNum);
      // newElem.find('#input_min_rt input').attr('name', 'rate[min_rate' + newNum+']').val('');

      newElem.find('#label_status label').attr('for', 'rate_status' + newNum);
     
      // newElem.find('#input_status select').attr('name', 'rate[status' + newNum+']');
      // newElem.find('#input_status select').attr('name', 'rate[status' + newNum+']');
     
     
     
      // insert the new element after the last "duplicatable" input field
      $('#input' + num).after(newElem);
      $('#input' + newNum).find('input').val('').end();
      // newElem.find('#options'+(newNum-1)+'_tagsinput').remove();
      $('#btnDel').attr('disabled',false);
    });

  $('#btnDel').click(function(){
        var num = $('.clonedInput').length; // how many "duplicatable" input fields we currently have
        $('#input' + num).remove();   // remove the last element
      
        // enable the "add" button
        $('#btnAdd').attr('disabled',false);
        
        // if only one element remains, disable the "remove" button
        if (num-1 == 1)
        $('#btnDel').attr('disabled',true);
      });

    
    $('#btnDel').attr('disabled',true);

});
</script>