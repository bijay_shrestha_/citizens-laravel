<aside class="main-sidebar">
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <p style="height: 40px;"></p>
            </div>
            <div class="pull-left info">
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active">
                <a href="{{ route('home') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cog"></i>
                    <span>System</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.users') }}">Users</a></li>
                    
                    <li><a href="{{ route('admin.roles') }}">Groups</a></li>
                    <li><a href="{{ route('admin.permissions') }}">Permissions</a></li> 
                </ul>
            </li>
            <li class="treeview">
                <a href="#">

                    <span>Content</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.accounts') }}">Account</a></li>
                    <li><a href="{{ route('admin.account_parents') }}">Account Parent</a></li>
                    <li><a href="{{ route('admin.loans') }}">Loan</a></li> 
                    <li><a href="{{ route('admin.loan_parents') }}">Loan Parent</a></li> 
                    <li><a href="{{ route('admin.credit_cards') }}">Credit Card</a></li> 
                    <li><a href="{{ route('admin.vacancydata') }}">Recruitment</a></li> 
                    <li><a href="{{ route('admin.banner_collections') }}">Banner</a></li> 
                    <li><a href="{{ route('admin.other_menus') }}">Other Menu Content</a></li> 
                    <li><a href="{{ route('admin.bankings') }}">Banking</a></li> 
                    <li><a href="{{ route('admin.department_contacts') }}">Department Contacts</a></li> 
                    <li><a href="{{ route('admin.services') }}">Services</a></li> 
                    <li><a href="{{ route('admin.notices') }}">ASBA Notice</a></li> 
                    <li><a href="{{ route('admin.pages') }}">Pages</a></li> 
                    <li><a href="{{ route('admin.executives') }}">Executive</a></li> 
                    <li><a href="{{ route('admin.blogs') }}">News/Blog</a></li> 
                    <li><a href="{{ route('admin.official_notices') }}">Official Notice</a></li> 
                    <li><a href="{{ route('admin.media_contacts') }}">Media Contact</a></li> 
                    <li><a href="{{ route('admin.branches') }}">Branch Location</a></li> 
                    <li><a href="{{ route('admin.cr_networks') }}">Corresponding Networks</a></li> 
                </ul>
            </li>

            <li class="treeview">
                <a href="#">

                    <span>Home Page</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.advertisements') }}">Advertisement</a></li>
                    <li><a href="{{ route('admin.products') }}">Products</a></li>
                    <li><a href="{{ route('admin.announcements') }}">Notice</a></li>
                    <li><a href="{{ route('admin.notice_categories') }}">Notice Category</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">

                    <span>Account Form</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.account_details', 'New') }}">New Form</a></li>
                    <li><a href="{{ route('admin.account_details', 'Pending') }}">Pending Form</a></li>
                    <li><a href="{{ route('admin.account_details', 'Approved') }}">Approved Form</a></li>
                    <li><a href="{{ route('admin.account_details', 'Denied') }}">Denied Form</a></li>
                </ul>
            </li>


            <li class="">
                <a href="{{ route('admin.csrs') }}">
                     <span>Corporate Social Responsibilities </span>
                </a>
            </li>
            

            <li class="treeview">
                <a href="#">

                    <span>Remittance</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.remittances') }}">Remittance</a></li>
                    <li><a href="{{ route('admin.remit_catagories') }}">Remittance Categories</a></li>
                </ul>
            </li>


            <li class="treeview">
                <a href="#">

                    <span>Rates</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.forexes') }}">Exchange Rate</a></li>
                    <li><a href="{{ route('admin.base_rates') }}">Base Rate</a></li>
                    <li><a href="{{ route('admin.interest_rate_spreads') }}">Interest Rate Spread</a></li>
                    <li><a href="{{ route('admin.fee_charges') }}">Fee and Charges</a></li>
                    <li><a href="{{ route('admin.fees_commisions') }}">Fee and commision</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">

                    <span>KYC</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.expected_transactions') }}">Expected Transaction</a></li>
                    <li><a href="{{ route('admin.expected_turnovers') }}">Expected Turnover</a></li>
                    <li><a href="{{ route('admin.purpose_of_accounts') }}">Purpose of account</a></li>
                    <li><a href="{{ route('admin.source_of_funds') }}">Source of fund</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">

                    <span>FAQ</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.faqs') }}">FAQ</a></li>
                    <li><a href="{{ route('admin.faq_collections') }}">FAQ Category</a></li>
                </ul>
            </li>
            
            <li class="">
                <a href="{{ route('admin.feedback') }}">
                     <span>Feedback </span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">

                    <span>Email Setting</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.social_media_links') }}">Social Media</a></li>
                </ul>
            </li>

            <li class="">
                <a href="{{ route('admin.activity_logs') }}">
                     <span>Activity Logs </span>
                </a>
            </li>
            

            <li class="dropdown user user-menu">
              <a href="{{ route('logout') }}" onclick="event.preventDefault(); 
              document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">
               <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                   {{ csrf_field() }}
               </form>
              <span class="hidden-xs">Logout</span>
              </a>
              
            </li>


            
        </ul>
    </section>
</aside>