<script type="text/javascript">

 $("#upload_logo").change(function(){
    uploadlogo(this);
 });

function uploadlogo(thefile)
{
    var form_data = new FormData();
    form_data.append('upload_image', thefile.files[0]);
    form_data.append('directory', directory);
    form_data.append('_token', '{{csrf_token()}}');
    $('#logo-status').html("Image is uploading....");
     $.ajax({
            url: "{{url('/admin/accounts/upload/image')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                  var err = '';
                  data.errors['upload_image'].forEach (function (element) {
                    err += element + '. ';
                  });
                   $('#logo-status').css('color', 'red').html(err);
                } else {
                  $('#logo-status').html('');
                  $('#upload_logo').hide();
                  $('#logo').val(data.filename);
                  $('#upload_logo_name').html(data.originalname);
                  $('#upload_logo_name').show();
                  $('#change-logo').css('display', 'block');
                  //  $('#file_name').val(data);
                  //  $('#preview_image').attr('src', '{{asset('uploads')}}/' + data);
                }
               
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
              //  $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
            }
        });

}
   $('#change-logo').click(function () {
    var filename = $('#logo').val();
    if (confirm('Are you sure you to want to delete this image?')) {
       $.post("{{url('/admin/accounts/delete/file')}}",
        {'filename': filename, 'directory' : directory, '_token':"{{csrf_token()}}"}, function (result) {
           if (result.success == "yes") {
             $('#logo').val('');
             $('#upload_logo').val('');
             $('#upload_logo_name').html('');
             $('#upload_logo_name').hide();
             $('#change-logo').hide();
             $('#upload_logo').show();
          } else {
            alert('Something went wrong.');
          }
        });
    }
  }); 

   $("#upload_background").change(function(){
    uploadbackground(this);
 });

function uploadbackground(thefile)
{
    var form_data = new FormData();
    form_data.append('upload_image', thefile.files[0]);
    form_data.append('directory', directory);
    form_data.append('_token', '{{csrf_token()}}');
    $('#background-status').html("Image is uploading....");
     $.ajax({
            url: "{{url('/admin/accounts/upload/image')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                  var err = '';
                  data.errors['upload_image'].forEach (function (element) {
                    err += element + '. ';
                  });
                   $('#background-status').css('color', 'red').html(err);
                } else {
                  $('#background-status').html('');
                  $('#upload_background').hide();
                  $('#background').val(data.filename);
                  $('#upload_background_name').html(data.originalname);
                  $('#upload_background_name').show();
                  $('#change-background').css('display', 'block');
                  //  $('#file_name').val(data);
                  //  $('#preview_image').attr('src', '{{asset('uploads')}}/' + data);
                }
               
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
              //  $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
            }
        });

}
   $('#change-background').click(function () {
    var filename = $('#background').val();
    if (confirm('Are you sure you to want to delete this image?')) {
       $.post("{{url('/admin/accounts/delete/file')}}",
        {'filename': filename, 'directory' : directory, '_token':"{{csrf_token()}}"}, function (result) {
           if (result.success == "yes") {
             $('#background').val('');
             $('#upload_background').val('');
             $('#upload_background_name').html('');
             $('#upload_background_name').hide();
             $('#change-background').hide();
             $('#upload_background').show();
          } else {
            alert('Something went wrong.');
          }
        });
    }
  }); 
</script>