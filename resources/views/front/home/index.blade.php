@extends('front.layout.main.master')
@section('content')
<script src="{{ url('/vendors/Chart.js/dist/Chart.bundle.js') }}"></script>
<script src="{{ url('/img/frontend/js/Utils.js') }}"></script>
@include('front.layout.includes.banner')
@php 
$banners=$data['banners'];

$data_banners = [];
for ($i = 0; $i < count($banners); $i++) {
    foreach ($banners[$i] as $index_banner => $data_banner ) {
        $data_banners[$i][$index_banner] = $data_banner; 
    }
}
$banners = $data_banners;
$advertisements=$data['advertisements'];
$products=$data['products'];
$services=$data['services'];
$blog=$data['blogs'];
$forexs=$data['forexs'];
$stockvalue=$data['stockvalue'];
$forexdata=$data['forexdata'];
@endphp     
<div class="clearfix"></div>

<section class="stock-info-section">
    <div class="outer-custom-container">
        <div class="ci-md-2">
            <div class="left-quick-links">
                <div class="side-item">
                        <span><i class="fa fa-product-hunt" aria-hidden="true"></i></span>
                        <span>Products</span>

                </div>
                
                
                <div class="side-item">
                    <a href="home/online">
                        <span><img src="{{ asset('frontcss') }}/img/icons/menu/online_service.png" alt=""></span>
                        <span>Online Services</span>
                    </a>
                </div>

                <div class="side-item">
                    <a href="account">
                        <span><img src="{{ asset('frontcss') }}/img/icons/menu/simple_pig.png" alt=""></span>
                        <span>Deposit</span>
                    </a>
                </div>

                <div class="side-item">
                    <a href="loan">
                        <span><img src="{{ asset('frontcss') }}/img/icons/menu/hand.png" alt=""></span>
                        <span>Loan</span>
                    </a>
                </div>

                <div class="side-item">
                    <a href="credit_card">
                        <span><img src="{{ asset('frontcss') }}/img/icons/menu/card.png" alt=""></span>
                        <span>Card</span>
                    </a>
                </div>

                <div class="side-item">
                    <a href="page/remit">
                        <span><img src="{{ asset('frontcss') }}/img/icons/menu/remit.png" alt=""></span>
                        <span>Remittance</span>
                    </a>
                </div>

                <!--<div class="side-item">-->
                <!--    <a href="banking">-->
                <!--        <span><img src="{{ asset('frontcss') }}/img/icons/menu/int_pig.png" alt=""></span>-->
                <!--        <span>Electronic Banking</span>-->
                <!--    </a>-->
                <!--</div>-->

                <div class="side-item">
                    <a href="home/others">
                        <span><img src="{{ asset('frontcss') }}/img/icons/menu/other.png" alt=""></span>
                        <span>Other Services</span>
                    </a>
                </div>
                
                
            </div>
        </div>

        <div class="ci-md-8">
            <div class="home-center-stock">
                <div class="ci-md-6 stock-image-container">
                    <?php foreach($advertisements as $advertisement): ?>
                    <a href="<?php echo $advertisement['link'] ?>">
                        <div class="offers-image" style="background-image: url('uploads/media/<?php echo $advertisement['background'] ?>')">
                         	<div class="image-container">
                             	<?php if($advertisement['image']== NULL OR $advertisement['image']=='' ){?>
                                 <?php }else{?>
                                <img src="uploads/media/<?php echo $advertisement['image'] ?>">
                                 <?php }?>
                            </div>
                            <div class="text-container">
                                <div class="text-inner">
                                    <div class="right-img">
                                        <img src="uploads/media/<?php echo $advertisement['logo'] ?>">
                                    </div>
                                    <div class="right-text">
                                        <h1><?php echo $advertisement['title'] ?></h1>
                                        <p><?php echo $advertisement['short_text'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
            
                    </a>
                  <?php endforeach; ?>
                </div>
                <div class="ci-md-6 stock-numbers-container">
                    <div class="stock-exchange">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#foreign-exchange" aria-controls="home" role="tab" data-toggle="tab">
                                    <span>FOREX</span>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#stock-exchange" aria-controls="profile" role="tab" data-toggle="tab">
                                    <span>STOCK</span>
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="foreign-exchange">
                                <div id="curve_chart" class="home-center-chart"></div>
                                <div class="fortex-rate table-responsive" >
                                    <div class="table-wrapper-fortex" style="display: block !important; height: 315px !important;">
                                        <table class="table">
                                        <tr>
                                            <td>Currency</td>
                                            <td>Buying</td>
                                            <td>Selling</td>
                                        </tr>
                                        
                                        <?php foreach($forexs as $forex){?>
                                        
                                        <tr>
                                            <td> <a href="forex"><?php echo $forex->currency;?> </a></td>  
                                            <td><?php echo $forex->buying ;?></td>
                                            <td><?php echo $forex->selling;?></td>
                                        </tr>
                                     

                                        <?php }?>
                                       
                                    </table>
                                    </div>
                                    
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="stock-exchange">
								<div id="linechart" class="home-center-chart"></div>
                               	<div class="fortex-rate table-responsive">
                                    <table class="table stock">
                                        <tr>
                                            <td>SYMBOL</td>
                                            <td>DATE</td>
                                            <td>CLOSING PRICE</td>
                                            
                                        </tr>

                                      	<tr>
                                            <?php foreach($stockvalue as $stock):?>
                                            <td><a href="http://nepalstock.com.np/company/display/348">CZBIL</a></td>
                                            <td><?php echo $stock['created_time']?></td>
                                            <td><?php echo $stock['closing_price'];?></td>
                                            <?php endforeach;?>
                                        </tr>                                  
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="ci-md-1">
            <div class="right-tool-links">
                <div class="side-item">
                    <span>Online Services</span>
                </div>
                <div class="side-item">
 
                    <a href="home/online_Account_opening" target="_blank">
                        <span>
                            <img src="{{ asset('frontcss') }}/img/form_green.png"> 
                        </span>
                        <span>Account Opening</span>
                    </a>

                </div>
        		<div class="side-item">
                    <a href="https://onlineservices.ctznbank.com/credit-card-form">
                        <span>
                             <img src="{{ asset('frontcss') }}/img/form_green.png"> 
                        </span>
                        <span>Credit Card Form</span>
                    </a>
                </div>

        		<div class="side-item">
                    <a href="https://onlineservices.ctznbank.com/home-loan">
                        <span>
                             <img src="{{ asset('frontcss') }}/img/form_green.png"> 
                        </span>
                        <span>Home Loan Form</span>
                    </a>
                </div>


                 <div class="side-item">
                    <a href="https://guarantee.ctznbank.com/" >
                        <span>
                           <img src="{{ asset('frontcss') }}/img/linegraph_green.png">   
                        </span>
                        <!-- <span>Mero Share</span> -->
			    		<span>Guarantee Verification</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="services-section">
    <div class="outer-custom-container">
        <div class="section-title home-title">
            <h1>Services</h1>
        </div>
         <div class="section-content">
            <div class="service-item">
                <div class="service-hover-container">
                    <div class="image-container">
                          <img src="{{url('/')}}/img/frontend/img/locker.png" alt="image">
                    </div>
                    <div class="text-container">
                        <h2>Lockers Facility</h2>
                    </div>
                    <div class="curtain-effect"><a href="{{ url("locker-facility") }}">View More</a></div>
                </div>
            </div>
     
        <div class="service-item">
            <div class="service-hover-container">
                <div class="image-container">
                    <img src="{{url('/')}}/img/frontend/img/utility.png" alt="image">
                </div>
                <div class="text-container">
                    <h2>Utility Payment</h2>
                </div>
                <div class="curtain-effect"><a href="{{ url("utility-payment") }}">View More</a></div>
            </div>
        </div>
        <div class="service-item">
            <div class="service-hover-container">
                <div class="image-container">
                    <img src="{{url('/')}}/img/frontend/img/pigg.png" alt="image">
                </div>
                <div class="text-container">
                    <h2>Privilege Banking</h2>
                </div>
                <div class="curtain-effect"><a href="{{ url("privilege") }}">View More</a></div>
            </div>
        </div>
        <div class="service-item">
            <div class="service-hover-container">
                <div class="image-container">
                    <img src="{{url('/')}}/img/frontend/img/citizen icon design_Branchless Banking-02.png" alt="image">
                </div>
            <div class="text-container">
                <h2>Branchless Banking</h2>
            </div>
            <div class="curtain-effect"><a href="{{ url('branchless-banking') }}">View More</a></div>
        </div>       
    </div>

</section>

<section class="offers-section">
    <div class="outer-custom-container">
        @foreach($products as $product)
            @if($product['image']==NULL OR $product['image']=="")
            <a href="{{ $product['link'] }}">
	            <div class="offers-item">
	                <div class="offers-image" style="background-image: url('uploads/product/{{ $product['background'] }}')">
	                    <div class="text-container">
	                        <div class="text-inner">
	                            <div class="right-img">
	                                <img src="uploads/product/{{ $product['logo'] }}">
	                            </div>
	                            <div class="right-text">
	                                <h1>{{ $product['name'] }}</h1>
	                                <p>{{ $product['short_text'] }}</p>
	                            </div>
	                        </div>
	                    </div>
	                    
	                    <div class="image-container">
	                    </div>
	                </div>
	            </div>
        	</a>
            @else
            <a href="{{ $product['link'] }}">
	            <div class="offers-item">
	                <div class="offers-image" style="background-image: url('uploads/product/{{ $product['background'] }}')">
	                    <div class="text-container">
	                        <div class="text-inner">
	                            <div class="right-img">
	                                <img src="uploads/product/{{ $product['logo'] }}">
	                            </div>
	                            <div class="right-text">
	                                <h1>{{ $product['name'] }}</h1>
	                                <p>{{ $product['short_text'] }}</p>
	                            </div>
	                        </div>
	                    </div>
	                    
	                    <div class="image-container">
	                        <img src="uploads/product/{{ $product['image'] }}" class="{{ ($product['type'] == 0)?'image':'card' }}">
	                    </div>
	                </div>
	            </div>
        	</a>
            @endif
  		@endforeach
    </div>
</section>


<section class="home-footer">
    <div class="outer-custom-container">

        <div class="ft-container">
            <div class="ft-md-1">
                <div class="ft-image" style="background-image: url('{{ asset('uploads/blog/'.getBlogs()->image_name) }}')"></div>
            </div>
            <div class="ft-md-2">
                <h5>{{ getBlogs()->blog_title }}</h5>
                <p>{!! substr(getBlogs()->contents, 0, 350) !!}..... </p>
                <a class="ft-view" href="blog/index">VIEW ALL <i class="fa fa-arrow-right"></i></a>
            </div>
        </div>

        <!--    <div class="ft-right-container">

            </div>-->

        <div class="sq-slash">
            <div class="un-sq-slash" style="top:6% !important">
                <h5> {{ get_preferences('title') }} </h5>
                <div class="ft-contact">
                    <ul>
                      <li><i class="fa fa-home"></i>{{ get_preferences('address').','.get_preferences('city').'-'.get_preferences('postalcode') }} </li>
                        <li>
                            <a href="tel:+01-4427842">
                                <i class="fa fa-phone"></i>{{ get_preferences('phone') }}
                            </a>
                        </li>
                        <li>
                            <i class="fa fa-mobile"></i>
                            <a href="tel:+16600166667">
                                {{ get_preferences('mobile') }}
                            </a>
                        </li>
                        <li>
                            <i class="fa fa-envelope"></i>
                            <a href="mailto:info@ctznbank.com">
                                {{ get_preferences('email') }}
                            </a>
                        </li>
                        <li>
                            <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                            <a href="{{ get_preferences('location') }}">Google Map</a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection