@extends('front.layout.main.master')
@section('content')
<div class="rates-page list-all-page">
    <section class="home-slider">
        <div class="bannerSlider">
            @foreach($banners as $banner)
                <div class="bannerItem bannerItemInside {{$banner['design']}}" style="background-image: url('{{url('uploads/banner/'.$banner['image_name'])}}')">
                
                 <div class="text-container">
                    <div class="text-inner">
                         <div class="right-img">
                            
                            <img src="{{url('/')}}/uploads/banner/{{$banner['logo']}}">
                        </div>
                       <div class="right-text {{$banner['color']}} bottom-{{$banner['bottom-color']}} " >
                            <h1 >{!! $banner['top_box_description'] !!}</h1>
                            <p>{!! $banner['bottom_box_description'] !!}</p>
                        </div>
                    </div>
                </div>  
                 @if($banner['type']==1)
                <div class="banner-water-mark">
                 
                  <a href="{{$banner['freepik_link']}}" target="_blank">Link for image</a>                               
                </div>
                @else
                
                @endif

            </div>
            @endforeach
        </div>
    </section>

    <section class="page-content-area">
        <div class="outer-custom-container">
            <div class="left-big-contianer">
                <div class="contianer">
                    <div class="calculator-new">
                        <div class="section-title">
                            <h1>Find The Interest Rate</h1>
                        </div>

                        <div class="section-content">
                            <div class="calculator-block">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4>Account and rate</h4>
                                    </div>
                                    <div class="col-sm-6">
                                        <select id="account-select" onchange="getAccounts()" class="form-control" style="max-width: 450px; margin:0 auto;">
                                           <option>Select Account</option>
                                    @foreach($accounts as $account)
                                        <option value="{{$account['account_id']}}">{{$account['name']}}</option>
                                        
                                    @endforeach    
                                        </select>
                                    </div>
                                    <div class="col-sm-6" id="account-rate-drop">
                                        <select id="account-interest-rate" onchange="setAccountRate()" class="form-control" style="max-width: 450px; margin:0 auto;"><option value="">Select Interest rate</option></select>
                                    </div>
                                </div>
                            </div>
                             <input type="hidden" id="apr-value-account" value=""/>    
                            <div class="row cal-2nd-row">
                                <div class="col-sm-6 cal-amount">
                                    <div class="calculator-block">
                                        <h4>Amount</h4>
                                        <div class="amt-contianer">
                                            Nrs
                                            <input type="number" value="10000" min="0" step="10000" id="percentage3">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 cal-term">
                                    <div class="calculator-block">
                                        <h4>Term</h4>
                                        <div class="amt-contianer">
                                            <input type="number" value="3"  min="1" max="12" id="percentage4">in Months
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="calculated-result-box-container">
                                <div class="calculator-result-box">
                                    <div class="box-title">
                                        <h1>Total Quarterly Interest</h1>
                                    </div>
                                    <div class="box-content">
                                        <p id="monthly_repay_account">NRS</p>
                                    </div>
                                    <div class="box-footer">
                                        <h1>Quarterly</h1>
                                    </div>
                                </div>
                                <div class="calculator-result-box">
                                    <div class="box-title">
                                        <h1>Total Quarterly Interest</h1>
                                    </div>
                                    <div class="box-content">
                                        <p id="total_amount_account">NRs.</p>
                                    </div>
                                    <div class="box-footer">
                                        <h1>Total with interest</h1>
                                    </div>
                                </div>

                                <div class="calculator-result-box">
                                    <div class="box-title">
                                        <h1>APR </h1>
                                    </div>
                                    <div class="box-content">
                                        <p id="aprAccount">Select an Account</p>
                                    </div>
                                    <div class="box-footer">
                                        <h1>Based on the details you entered</h1>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="emi-link-div">
                                {{-- site url --}}
                                    <a href="{{url('home/emi')}}"><i class="fa fa-calculator"></i>Calculate Your EMI Now ! </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>

    $(document).ready(function() {


        $('#publish').modal('show');

        $("#slider1").slider({
            min:50000,
            max:20000000

        });
        $("#slider1").on("slide", function(slideEvt) {
            $("#percentage1").val(slideEvt.value);
        });

        $("#slider2").slider({
            min:3,
            max:60,
            value:1,
            step:3
        });
        $("#slider2").on("slide", function(slideEvt) {
            $("#percentage2").val(slideEvt.value);
        });

        $("#slider3").slider({
            min:1000,
            max:20000000

        });

        $("#slider3").on("slide", function(slideEvt) {
            $("#percentage3").val(slideEvt.value);
        });

        $("#slider4").slider({
            min:3,
            max:60,
            value:1,
            step:3
        });
        $("#slider4").on("slide", function(slideEvt) {
            $("#percentage4").val(slideEvt.value);
        });







    });
    //$('.slider').slider();

    //$('#effect-1').hide();

    $(".sidebar").hover(function(){
        // alert("its here");
        // $('#search-button').hide();
        // Set the options for the effect type chosen
        // var options = { direction: 'right' };
        $( "#effect-1" ).css( "right",'0').find('.sidebar span').attr('class','');

        // $('#form-search').toggle('slide', options, 500);
    },function(){
        // var container = $("#effect-1");

        // if (!container.is(e.target) // if the target of the click isn't the container...
        //     && container.has(e.target).length === 0) // ... nor a descendant of the container
        // {
        //     container.css( "right",'-200px').find('.sidebar span').attr('class','hide');

        // }
        $("#effect-1").css("right",'-200px').find('.sidebar span').attr('class','hide');
    });


    $(document).mouseup(function (e)
    {
        var container = $("#effect-1");

        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.css( "right",'-200px').find('.sidebar span').attr('class','hide');

        }
    });

    function showModal(param)
    {
        if(param =='base_rate')
        {
            $.post('{{url("home/getBaseRateJSON")}}',{},function(data){
                // {{-- site url --}}
                if(data.success==true)
                {
                    var contain = '<table class="table-striped table-bordered"  cellpadding="10">';
                    contain +='<tr><th>Date</th><th>Rate</th></tr>';

                    $.each(data.rates,function(index,values){
                        contain +='<tr>';
                        contain +='<td>'+values.date+'</td>';
                        contain +='<td>'+values.base_rate+'</td>';
                        contain +='</tr>';
                    });
                    $('#modalLabel').html('Base Rate');
                    $('#modal-contain').html(contain);
                    $('#rateModal').modal('show');
                }
                else
                {
                    $('#modalLabel').html('Base Rate');
                    $('#modal-contain').html('No records found for base rate');
                    $('#rateModal').modal('show');
                }
            },'JSON');
        }

        if(param == 'interest_rate_spread')
        {
            $.post('{{url("home/getInterestRateSpreadJSON")}}',{},function(data){
                // {{-- site url --}}
                if(data.success==true)
                {
                    var contain = '<table class="table-striped table-bordered"  cellpadding="10">';
                    contain +='<tr><th>Date</th><th>Rate</th></tr>';

                    $.each(data.rates,function(index,values){
                        contain +='<tr>';
                        contain +='<td>'+values.date+'</td>';
                        contain +='<td>'+values.interest_rate_spread_rate+'</td>';
                        contain +='</tr>';
                    });
                    $('#modalLabel').html('Interest rate spread');
                    $('#modal-contain').html(contain);
                    $('#rateModal').modal('show');
                }
                else
                {
                    $('#modalLabel').html('Interest ratespread');
                    $('#modal-contain').html('No records found for interest rate spread');
                    $('#rateModal').modal('show');
                }
            },'JSON');
        }
    }




    /* end of loan payment*/

    function getAccounts()
    {
        var id = $('#account-select').val();
        $('#apr-value-account').val('');
        $('#aprAccount').html('Select a account');
        console.log(id);
        // {{-- site url --}}
        $.ajax({
            url : '{{url("account/getJSOnAccount")}}',
            data : {id:id},
            dataType:'json',

            success:function(data){

                var dropdown ='<select id="account-interest-rate" onchange="setAccountRate()" class="form-control" style="max-width: 450px; margin:0 auto;"><option value="">Select Interest rate</option>';
                // alert();
                $.each(data.account,function(key,value){

                    if(value.per_annum_max !=0)
                    {
                        dropdown +='<option value="'+value.per_annum_max+'">'+value.per_annum_max+'% (per annum max)('+value.rate_name+')</option>';
                    }

                    if(value.per_annum_min != 0)
                    {
                        dropdown +='<option value="'+value.per_annum_min+'">'+value.per_annum_min+'% (per annum  min)('+value.rate_name+')</option>';
                    }



                });


                dropdown +='<select>';

                $('#account-rate-drop').html(dropdown);




            }


        });

        // $.post('{{url('account/getJSOnAccount')}}',{id:id},function(data){
            // {{-- site url --}}

        //      var dropdown ='<select id="account-interest-rate" onchange="setAccountRate()" class="form-control" style="max-width: 450px; margin:0 auto;"><option value="">Select Interest rate</option>';

        //     $.each(data.account,function(key,value){

        //         if(value.per_annum_max !=0)
        //         {
        //             dropdown +='<option value="'+value.per_annum_max+'">'+value.per_annum_max+'% (per annum max)('+value.rate_name+')</option>';
        //         }

        //         if(value.per_annum_min != 0)
        //         {
        //             dropdown +='<option value="'+value.per_annum_min+'">'+value.per_annum_min+'% (per annum  min)('+value.rate_name+')</option>';
        //         }



        //     });


        //     dropdown +='<select>';
        //     // alert(dropdown);
        //     $('#account-rate-drop').html(dropdown);

        //     // $('#apr-value-account').val(data.interest_rate);
        //     // $('#aprAccount').html(data.interest_rate+'%');
        //     //  calculateAccount();
        // },'json');


    }

    function setAccountRate()
    {
        var rate = $('#account-interest-rate').val();

        if(rate !='')
        {
            $('#apr-value-account').val(rate);
            $('#aprAccount').html(rate+'%');
            calculateAccount();

        }

    }

    /* Calculation and events for account interest*/
    $('#slider3').on('slideStop',function(){
        calculateAccount();
    });
    $('#slider4').on('slideStop',function(){
        calculateAccount();
    });

    $('#percentage3').on('keyup',function(){
        calculateAccount();
    }); 
    $('#percentage4').on('keyup',function(){
        calculateAccount();
    });
        $('#percentage3').on('change',function(){
        calculateAccount();
    });
    $('#percentage4').on('change',function(){
        calculateAccount();
    });

    function calculateAccount()
    {
        var principalAccount = $('#percentage3').val();
        var termAccount      = $('#percentage4').val();
        var aprAccount       = $('#apr-value-account').val();
        // console.log(principalAccount, termAccount, aprAccount);
        if(principalAccount=='')
        {
            alert('Please Enter principal');
            return false;
        }
        else if(termAccount=='')
        {
            alert('Please Enter terms in year');
            return false;
        }
        else if(aprAccount=='')
        {
            alert('Please select interest rate after selecting account');
            return false;
        }
        else
        {
            // var monthly_payment = termAccount;
            // var N = 100;
            // var PTR = aprAccount*monthly_payment*principalAccount;
            // var I = PTR/N;
            // var monthly_pay = I/12;
            // var yearly_pay = I;

            var monthly_payment = (aprAccount/100);
            var I = (monthly_payment/4);
            var N = 4*(termAccount/12);
            var v = Math.pow((1 + I), N);
            var yearly_pay = (v*principalAccount);
            var monthly_pay = (yearly_pay-principalAccount);

            $('#monthly_repay_account').html('NRs.'+monthly_pay.toFixed(2));
            $('#total_amount_account').html('NRs.'+yearly_pay.toFixed(2));



        }
    }



</script>


@endsection






