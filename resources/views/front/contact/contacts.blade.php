@extends('front.layout.main.master')
@section('content')
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="rates-page list-all-page">
    <section class="home-slider">
        <div class="bannerSlider">
            @foreach($banners as $banner)
                <div class="bannerItem" style="background-image: url('{{ url('uploads/banner/'.$banner->image_name) }}')">
                    
                </div>
            @endforeach
        </div>
    </section>

    <section class="page-content-area">
        <div class="outer-custom-container">
            <div class="left-big-contianer">
                <!-- Nav Tabs Top-->
                	<div class="department">
        <div class="department-header">
                <div class="dept-logo">
                    {{-- <!-- <img src="{{ url(frontcss/img/dept-logo.png) }}"> --> --}}
					<img src="{{url('frontcss/img/dept-logo.png')}}">
                </div>
                <div class="blog-title">
                    <h3>Department</h3>
                </div>
            </div>
		<div class="clearfix"></div>

		<div class="branch-tabs">

			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#inside" role="tab" data-toggle="tab">Inside Valley</a></li>
				<li role="presentation"><a href="#outside" role="tab" data-toggle="tab">Outside Valley</a></li>
			</ul>
			<ul class="nav nav-tabs">
			    <li role="presentation" class="pull-right"><a href="{{ url('feedback') }}">Feed Back</a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="inside">

					<div class="emergency-calls">
						<div class="row">
						@foreach ($branches_inside as $inside)
							<div class="col-md-4">
								<div class="product1" style="background-image:url("{{ url('frontcss/img/branch-img2.png') }}">
									<div class="caption"> 
										
										<div class="position">
											<h5 class="b_name">{{ $inside->name }}</h5>
 											<h6 class="branch_manager">Branch Manager: {{ $inside->branch_manager }} </h6>
											<h6 class="b_add"> {{ $inside->address }}</h6>
											<div class="fa1">
												<i class="fa fa-phone" aria-hidden="true"> {{ $inside->contact_no }}</i>
											</div>
											@if($inside->fax !==NULL OR $inside->fax != '')
											<div class="fa3">
												<i class="fa fa-fax" aria-hidden="true"> {{ $inside->fax }}</i>
											</div>
											@endif
											<div class="fa2">
												<i class="fa fa-envelope" aria-hidden="true"> {{ $inside->email }}</i>
											</div>
										</div>
										<div class="dark-image">

										</div>
									</div>
								</div>
							</div>
						@endforeach
							
							
						</div>
					</div>  <!-- emergency-call 1 -->

					
					
				</div>

				<div role="tabpanel" class="tab-pane" id="outside">

					<div class="emergency-calls">
						<div class="row">
						@foreach($branches_outside as $outside)
							<div class="col-md-4">
								<div class="product1" style="background-image:url('{{ url('frontcss/img/branch-img2.png') }}">
									<div class="caption">
										<div class="dark-image">
											
										</div>
										<div class="position">
											<h5 class="b_name">{{ $outside->name }}</h5>
                                            <h6 class="branch_manager">Branch Manager: {{ $outside->branch_manager }}</h6>
										<h6 class="b_add"> {{ $outside->address
										 }}</h6>
											<div class="fa1">
												<i class="fa fa-phone" aria-hidden="true"> {{ $outside->contact_no }}</i>
											</div>
                                             @if($inside->fax !==NULL OR $inside->fax!= '')
											<div class="fa3">
												<i class="fa fa-fax" aria-hidden="true"> {{ $outside->fax }}</i>
											</div>
											@endif
											<div class="fa2">
												<i class="fa fa-envelope" aria-hidden="true"> {{ $outside->email }}</i>
											</div>
										</div>
									</div>
								</div>
							</div>

							@endforeach

							
						</div>
					</div>  <!-- emergency-call 1 -->

					

					
				
				</div>
			</div>
		</div>
	</div>
                <!-- Nav Tab Content End -->
            </div>
        </div>
    </section>
</div>

@endsection





<script>
     $(".sidebar").hover(function(){
                        // alert("its here");
                        // $('#search-button').hide();
                    // Set the options for the effect type chosen
                   // var options = { direction: 'right' };
                        $( "#effect-1" ).css( "right",'0').find('.sidebar span').attr('class','');
                       
                   // $('#form-search').toggle('slide', options, 500);
                },function(){
                    // var container = $("#effect-1");

                    // if (!container.is(e.target) // if the target of the click isn't the container...
                    //     && container.has(e.target).length === 0) // ... nor a descendant of the container
                    // {
                    //     container.css( "right",'-200px').find('.sidebar span').attr('class','hide');
                        
                    // }
                    $("#effect-1").css("right",'-200px').find('.sidebar span').attr('class','hide');
                });

        
        $(document).mouseup(function (e)
        {
            var container = $("#effect-1");

            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.css( "right",'-200px').find('.sidebar span').attr('class','hide');
                
            }
        });


$('#submit-contact').on('click',function(){
    
    var email       = $('#email').val();
    var name        = $('#name').val();
    var reason     = $('#reason').val();
   
    // var suburb      = $('#suburb').val();
    var contact     = $('#contact_num').val();
    var message     = $('#comment').val();
   

    if(sector=='')
    {
         $('#error-msg').text('Please select a sector by clicking on the sector icons on sector TAB!').css('color','#a82d32');
        return false;
    } 
    else if(email =='' || name =='' || address =='' || country =='' || post_code =='' || contact =='' || message =='')
    {   
        
        $('#error-msg').text('All fields are compulsory').css('color','#a82d32');
        return false;
    }
    else if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=email.length)
    {
        $('#error-msg').text('Email Address Not Valid!').css('color','#a82d32');
        return false;
    }
    

    
   
});

function setSector(name,id)
{
    $('#sector-val').val(id);
    $('#display-sector').html('Sector:'+name);
}

</script>

