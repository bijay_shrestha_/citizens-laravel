<div class="loan-page-all list-all-page chief-page">
    <section class="home-slider">
         <div class="bannerSlider">
            
              
                     <?php foreach($banners as $banner):?>
                 <div class="bannerItem bannerItemInside <?php echo $banner['design']?>" style="background-image: url('<?php echo base_url('uploads/banner/'.$banner['background'])?>')">
                
                 <div class="text-container">
                    <div class="text-inner">
                        <div class="right-img">
                            <img src="<?php echo base_url()?>uploads/banner/<?php echo $banner['logo'] ?>">
                        </div>
                       <div class="right-text <?php echo $banner['color']?> bottom-<?php echo $banner['bottom-color']?> " >
                            <h1 ><?php echo $banner['top_box_description'] ?></h1>
                            <p><?php echo $banner['bottom_box_description'] ?></p>
                        </div>
                    </div>
                </div>  
                <?php if($banner['type']==1){?>
                <div class="banner-water-mark">
                 
                    <a href="https://www.freepik.com/" target="_blank">Vectors graphics designed by Freepik</a>                              
                </div>
                <?php }else{?>
                <?php }?>
            </div>
            <?php endforeach;?>
        </div>
    </section>
    
    <section class="page-content-area">
        <div class="outer-custom-container">
            <div class="left-big-contianer">
                <div class="contianer">
                        <div class="section-content">
                            <div class="chief-info-block">
                                <div class="image-container">
                                       </div>
                                
                                <div class="text-container">
                                    <?php echo $detail['description'];?>
                                    </div>
                            </div>
                        </div>
                       
                </div>
            </div>
        </div>
    </section>
   
</div>



