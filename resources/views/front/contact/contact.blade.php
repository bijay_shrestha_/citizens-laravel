@extends('front.layout.main.master')
@section('content')
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="list-all-page contact-page">
    
    
    <section class="home-slider">
        <div class="bannerSlider">
        @foreach($banners as $banner)
        @if($banner['image_name'] == NULL || $banner['image_name'] =='' )
              <div class="bannerItem bannerItemInside {{ $banner['design'] }}" style="background-image: url('{{ url('uploads/banner/'.$banner['background']) }}')">
                
                 <div class="text-container">
                    <div class="text-inner">
                        <div class="right-img">
                            <img src="{{ url('uploads/banner/'.$banner['logo']) }} ">
                            	
                        </div>
                        <div class="right-text {{ $banner['color'] }} bottom-{{ $banner['bottom-color'] }}  " >
                            <h1 >{!! $banner['top_box_description'] !!}</h1>
                            <p>{!! $banner['bottom_box_description'] !!} </p>
                        </div>
                    </div>
                </div>
                @if($banner['type']==1)
                <div class="banner-water-mark">
                 
                    <a href="{{ $banner['freepik_link'] }}" target="_blank">Link for image</a>                            
                </div>
                @else
                @endif
            </div>
               
                @else
       
                
                 <div class="cz_banner_item {{ $banner['design'] }}" style="background-image: {{ url('uploads/banner/'.$banner['background']) }}">
                <div class="image-container">
                    <img src="{{ url('uploads/banner/'.$banner['image_name']) }}">
                </div>
                
                <div class="text-container">
                    <div class="text-inner">
                        <div class="right-img">
                            <img src="{{ url('uploads/banner/'.$banner['logo']) }} ">
                        </div>
                        <div class="right-text {{$banner['color']}} bottom-{{$banner['bottom-color']}} " >
                            <h1 >{!! $banner['top_box_description'] !!} </h1>
                            <p>{!! $banner['bottom_box_description'] !!}</p>
                        </div>
                    </div>
                </div>
                
               
            
        </div>
                @endif
        @endforeach
        
    </div>
    </section>
    

            <section class="page-content-area">
        <div class="outer-custom-container">
            <div class="left-big-contianer">
                <div class="section-title">
                    <div class="icon-container">
                        
                         <img src="{{ url('frontcss/img/dept-logo.png')}}">   
                        
                      
                    </div>
                    <div class="text-container">
                      <h1>Corporate Office </h1>
                    </div>
                </div>

                <div class="content-container">
                    <div class="section-content">
                        <div class="show-all-item">
                            <div class="content" >
                               <div>
                
                <div>
                    <ul>
                        {{--  $this->preference->item('email')  =>  preference('email')    --}}
                        {{-- {{dd(preference('address'))}} --}}
                        <li><i class="fa fa-home"></i> &nbsp;{{ @preference('address').','.@preference('city').'-'.@preference('postalcode')}} </li>
                        <li><i class="fa fa-phone"></i> &nbsp;{{ @preference('phone') }} </li><span>
                        <li><img src=" {{ url('frontcss/img/icons/cellphone.png') }}" width=20px>&nbsp;Toll Free -{{ @preference('mobile') }}</li></span>
                        <li><i class="fa fa-phone"></i> &nbsp; FAX:{{ @preference('fax') }}</li>  
                        <li><i class="fa fa-circle-o-notch" aria-hidden="true"></i> &nbsp;SWIFT:{{ @preference('swift_no') }}</li>                         
                        <li><i class="fa fa-envelope"></i> &nbsp;{{ @preference('email') }}</li>
                        <li><i class="fa fa-thumb-tack" aria-hidden="true"></i> &nbsp;<a href="{{ @preference('location') }} ">Google Map</a></li>
                    </ul>
                </div>

            </div>
                            </div>
                        </div>
                    </div>

                   
                </div>
            </div>
        </div>
    </section>
                       
                   
    
</div>
@endsection

<script>
     $(".sidebar").hover(function(){
                        // alert("its here");
                        // $('#search-button').hide();
                    // Set the options for the effect type chosen
                   // var options = { direction: 'right' };
                        $( "#effect-1" ).css( "right",'0').find('.sidebar span').attr('class','');
                       
                   // $('#form-search').toggle('slide', options, 500);
                },function(){
                    // var container = $("#effect-1");

                    // if (!container.is(e.target) // if the target of the click isn't the container...
                    //     && container.has(e.target).length === 0) // ... nor a descendant of the container
                    // {
                    //     container.css( "right",'-200px').find('.sidebar span').attr('class','hide');
                        
                    // }
                    $("#effect-1").css("right",'-200px').find('.sidebar span').attr('class','hide');
                });

        
        $(document).mouseup(function (e)
        {
            var container = $("#effect-1");

            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.css( "right",'-200px').find('.sidebar span').attr('class','hide');
                
            }
        });


$('#submit-contact').on('click',function(){
    
    var email       = $('#email').val();
    var name        = $('#name').val();
    var reason     = $('#reason').val();
   var sector  = $('#sector').val();
    // var suburb      = $('#suburb').val();
    var contact     = $('#contact_num').val();
    var message     = $('#comment').val();
   

    if(sector=='')
    {
         $('#error-msg').text('Please select a sector by clicking on the sector icons on sector TAB!').css({'opacity':'1', 'transition':'all 0.5s ease', 'margin' :'20px 0px',
    'padding': '8px 20px'});
        return false;
    } 
    else if(email =='' || name =='' || address =='' || country =='' || post_code =='' || contact =='' || message =='')
    {   
        
        $('#error-msg').text('All fields are compulsory').css({'opacity':'1', 'transition':'all 0.5s ease', 'margin' :'20px 0px',
    'padding': '8px 20px'});
        return false;
    }
    else if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=email.length)
    {
        $('#error-msg').text('Email Address Not Valid!').css({'opacity':'1', 'transition':'all 0.5s ease', 'margin' :'20px 0px',
    'padding': '8px 20px'});
        return false;
    }
    

    
   
});

function setSector(name,id)
{
    $('#sector-val').val(id);
    $('#display-sector').html('Sector:'+name);
}

function tootle_reason(reason)
{
    $('#sector').val();
    $('#sector').val(reason);
}
</script>

