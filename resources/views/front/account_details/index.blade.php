@extends('front.layout.main.master')
@section('content')

<!-- <div class="content-wrapper" id="content-wrapper">	     -->

<style>

    .three-block-container{
        display: inline-block;
        padding: 100px;
    }
    .block-row {

    }
    .blocks{

        text-align: center;
        border:solid;
        width: 300px;
        height: 250px;
        float: left;
        
    }
  
    .icon div{
        padding-top: 70px;

    }
    .icon .fa{
        font-size: 70px;
        color: #fff;
    }
    .title{
        /*padding-top: 10px;*/
    }
    h4{
        color: #fff;
    }
    .block-row a{
        border: none;
    }
    .block-row a:hover{
        border: none;
    }
    .block-acc{
       background: #8dcfb7;
    }
    .block2-acc{
       background: #a4aebb;
    }

    .block3-acc{
       background: #0a67b0;
    }
</style>

<div class="container">
    <div class="three-block-container">
        <div class="block-row">
            <div class="blog-title">
                    <h3 style="text-align: center;"> Online Account form  </h3>
                </div>
            <div class="row">
                <div class="col-md-4">
                    <a href="{{ route('account_details.form') }}">
                        <div class="blocks block-acc">
                            <div class="block-content">
                               <div class="icon">
                                   <div> <img src="https://ctznbank.com/themes/citizens/assets/img/icons/add.png" class="tool-img"></div>
                               </div>
                               <div class="title">
                                   <h4>Open a new Account</h4>
                               </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                     <a href="{{ route('account_details.tracking_form') }}">
                        <div class="blocks block2-acc">
                            <div class="block-content">
                               <div class="icon">
                                   <div><img src="https://ctznbank.com/themes/citizens/assets/img/icons/pencil.png" class="tool-img"></div>
                               </div>
                               <div class="title">
                                   <h4>Track your account</h4>
                               </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="#"  data-toggle="modal" data-target="#myModal">
                        <div class="blocks block3-acc">
                            <div class="block-content">
                               <div class="icon">
                                   <div><img src="https://ctznbank.com/themes/citizens/assets/img/icons/location.png" class="tool-img"></div>
                               </div>
                               <div class="title">
                                   <h4>Existing One</h4>
                               </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      	<!-- Modal content-->
      	<div class="modal-content">
        	<div class="modal-header">
          		<button type="button" class="close" data-dismiss="modal">&times;</button>
          		<h4 class="modal-title">Modal Header</h4>
        	</div>
         	<form action="https://ctznbank.com/account_detail/getReferenceCode" method="post" accept-charset="utf-8">
         		<div style="display:none">
					<input type="hidden" name="csrf_ctzn_token" value="529aa6bb3007849106ea3df52a3570bf" />
				</div>        
				<div class="modal-body">
					<div class="form-group">
	   					<label class="col-form-label">Enter Your Reference Code</label>
	   					<input type="text" name="reference_num" class="form-control" id="reference_num">
	  				</div>
		        </div>
		        <div class="modal-footer">
		          	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		           	<button  type="submit" class="btn btn-primary">Go</button>
		        </div>

         	</form>      
     	</div>
      
	</div>
</div>
  
@endsection