@extends('front.layout.main.master')
@section('content')

<!-- <script src='https://www.google.com/recaptcha/api.js?ver=<?php echo "STYLE_VERSION" ?>'></script> -->
 
<div class="rates-page list-all-page online-account-form-page">
    <section class="page-content-area">
        <div class="outer-custom-container">
            <div class="left-big-contianer">
                <!-- Nav Tabs Top-->
                <div class="section-title">
                    <div class="icon-container">
                        <img src="https://ctznbank.com/themes/citizens/assets/img/citizens icon _green-06-46.png">
                    </div>
                    <div class="text-container">
                        <h1> Online Account form </h1>
                    </div>
                </div>

                <div class="department">
                    <div class="dept-tabs">
                        <div class="container">
                            <div class="stepwizard">
                                <div class="stepwizard-row setup-panel">
                                    <div class="stepwizard-step">
                                        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                                        <p>Account Detail</p>
                                    </div>

                                    <div class="stepwizard-step" >
                                        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                                        <p>Depositer's Nomination </p>
                                    </div>

                                    <div class="stepwizard-step">
                                        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                                        <p>KYC</p>
                                    </div>
                                    <div class="stepwizard-step">
                                        <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                                        <p>General Rules</p>
                                    </div>
                                    <div class="stepwizard-step">
                                        <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                                        <p>Authorization</p>
                                    </div>
                                </div>
                            </div>
                            <div class="refrence-code">

                                                                <p><b>Your Reference Code : </b> TBHDMN2ZXP                                                                
                            </div>
                            <script>

                            </script>

                            <form action="https://ctznbank.com/account_detail/save" method="post" accept-charset="utf-8" enctype="multipart/form-data"><div style="display:none">
<input type="hidden" name="csrf_ctzn_token" value="529aa6bb3007849106ea3df52a3570bf" />
</div>                            <input type="hidden" name="reference_code" value=TBHDMN2ZXP>
                            
                            <div class="row setup-content" id="step-1">
                                <div class="container">
                                    <div class="steps-title">
                                        <h3> Account Detail</h3>
                                    </div>

                                    <div class="row all-round-border first-allow-round">
                                        <div class="col-sm-6">
                                            <div class="check_options">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <label class="control-label" for="applying_from"> Applying From *</label>
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <select class="styled-select capital_letter slate" onchange="checkLocation()"name="applying_from" id="applying_from">
                                                                <option value="" >Select</option>
                                                                <option value="Inside Nepal" selected>Inside Nepal</option>
                                                                <option value="Outside Nepal" >Outside Nepal</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-6">
                                            <div class="check_options">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <label class="control-label" for="branch">Branch *</label>
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <select class="styled-select capital_letter slate" name="branch" id="branch" data-live-search="true">
                                                                <option value="" selected>Select</option>
                                                                                                                                    <option value="AURAHI BRANCH">AURAHI BRANCH</option>
                                                                                                                                    <option value="BADHIYATAAL BRANCH">BADHIYATAAL BRANCH</option>
                                                                                                                                    <option value="BAGLUNG BRANCH">BAGLUNG BRANCH</option>
                                                                                                                                    <option value="BAJHANG BRANCH">BAJHANG BRANCH</option>
                                                                                                                                    <option value="BANEPA BRANCH ">BANEPA BRANCH </option>
                                                                                                                                    <option value="BANGLACHULI BRANCH">BANGLACHULI BRANCH</option>
                                                                                                                                    <option value="BARDIBASH BRANCH">BARDIBASH BRANCH</option>
                                                                                                                                    <option value="BENI BRANCH">BENI BRANCH</option>
                                                                                                                                    <option value="BHAISEPATI BRANCH">BHAISEPATI BRANCH</option>
                                                                                                                                    <option value="BHAKTAPUR BRANCH">BHAKTAPUR BRANCH</option>
                                                                                                                                    <option value="BHOJPUR BRANCH">BHOJPUR BRANCH</option>
                                                                                                                                    <option value="BHUMAHI BRANCH">BHUMAHI BRANCH</option>
                                                                                                                                    <option value="BHUME(PURBE RUKUM) BRANCH">BHUME(PURBE RUKUM) BRANCH</option>
                                                                                                                                    <option value="BIRATNAGAR BRANCH">BIRATNAGAR BRANCH</option>
                                                                                                                                    <option value="BIRGUNJ BRANCH">BIRGUNJ BRANCH</option>
                                                                                                                                    <option value="BIRTAMOD BRANCH">BIRTAMOD BRANCH</option>
                                                                                                                                    <option value="BOUDDHA BRANCH">BOUDDHA BRANCH</option>
                                                                                                                                    <option value="BUTWAL BRANCH">BUTWAL BRANCH</option>
                                                                                                                                    <option value="CHABAHIL BRANCH">CHABAHIL BRANCH</option>
                                                                                                                                    <option value="CHAME BRANCH">CHAME BRANCH</option>
                                                                                                                                    <option value="CHARIKOT BRANCH">CHARIKOT BRANCH</option>
                                                                                                                                    <option value="CHHAHARA BRANCH">CHHAHARA BRANCH</option>
                                                                                                                                    <option value="Corporate Office">Corporate Office</option>
                                                                                                                                    <option value="DAMAK BRANCH">DAMAK BRANCH</option>
                                                                                                                                    <option value="DHAKARI(ACHHAM) BRANCH">DHAKARI(ACHHAM) BRANCH</option>
                                                                                                                                    <option value="DHANGADHI BRANCH">DHANGADHI BRANCH</option>
                                                                                                                                    <option value="DHARAN BRANCH">DHARAN BRANCH</option>
                                                                                                                                    <option value="DILLIBAZAR BRANCH">DILLIBAZAR BRANCH</option>
                                                                                                                                    <option value="DOLPA BRANCH">DOLPA BRANCH</option>
                                                                                                                                    <option value="DURBARMARG BRANCH">DURBARMARG BRANCH</option>
                                                                                                                                    <option value="GAIGHAT BRANCH">GAIGHAT BRANCH</option>
                                                                                                                                    <option value="GAUSHALA BRANCH">GAUSHALA BRANCH</option>
                                                                                                                                    <option value="GHORAHI BRANCH">GHORAHI BRANCH</option>
                                                                                                                                    <option value="GWARKO BRANCH">GWARKO BRANCH</option>
                                                                                                                                    <option value="HALDIBARI BRANCH">HALDIBARI BRANCH</option>
                                                                                                                                    <option value="HARIHARPURGADI BRANCH">HARIHARPURGADI BRANCH</option>
                                                                                                                                    <option value="HETAUDA BRANCH">HETAUDA BRANCH</option>
                                                                                                                                    <option value="ITAHARI BRANCH">ITAHARI BRANCH</option>
                                                                                                                                    <option value="JAJARKOT BRANCH">JAJARKOT BRANCH</option>
                                                                                                                                    <option value="JANAKPUR BRANCH">JANAKPUR BRANCH</option>
                                                                                                                                    <option value="JHIMRUK(PYUTHAN)">JHIMRUK(PYUTHAN)</option>
                                                                                                                                    <option value="JUMLA BRANCH">JUMLA BRANCH</option>
                                                                                                                                    <option value="KALANKI BRANCH">KALANKI BRANCH</option>
                                                                                                                                    <option value="KAPAN BRANCH">KAPAN BRANCH</option>
                                                                                                                                    <option value="KIRTIPUR BRANCH">KIRTIPUR BRANCH</option>
                                                                                                                                    <option value="KOLHABI BRANCH">KOLHABI BRANCH</option>
                                                                                                                                    <option value="KOTESHWOR BRANCH">KOTESHWOR BRANCH</option>
                                                                                                                                    <option value="KULESHWOR BRANCH">KULESHWOR BRANCH</option>
                                                                                                                                    <option value="KUMARIPATI BRANCH">KUMARIPATI BRANCH</option>
                                                                                                                                    <option value="KUPONDOLE BRANCH">KUPONDOLE BRANCH</option>
                                                                                                                                    <option value="LIKHU PIKEY BRANCH">LIKHU PIKEY BRANCH</option>
                                                                                                                                    <option value="MAHABOUDHA BRANCH">MAHABOUDHA BRANCH</option>
                                                                                                                                    <option value="MAHENDRANAGAR BRANCH">MAHENDRANAGAR BRANCH</option>
                                                                                                                                    <option value="MAITIDEVI BRANCH">MAITIDEVI BRANCH</option>
                                                                                                                                    <option value="MANAHARI BRANCH">MANAHARI BRANCH</option>
                                                                                                                                    <option value="MANGSEYBUNG BRANCH">MANGSEYBUNG BRANCH</option>
                                                                                                                                    <option value="NAMKHA(HUMLA) BRANCH">NAMKHA(HUMLA) BRANCH</option>
                                                                                                                                    <option value="NARAYAN GOPAL CHOWK  BRANCH">NARAYAN GOPAL CHOWK  BRANCH</option>
                                                                                                                                    <option value="NARAYANGHAT BRANCH">NARAYANGHAT BRANCH</option>
                                                                                                                                    <option value="NAYABAZAR BRANCH">NAYABAZAR BRANCH</option>
                                                                                                                                    <option value="NEPALGUNJ BRANCH">NEPALGUNJ BRANCH</option>
                                                                                                                                    <option value="NEW BANESHWOR BRANCH">NEW BANESHWOR BRANCH</option>
                                                                                                                                    <option value="NEW ROAD BRANCH">NEW ROAD BRANCH</option>
                                                                                                                                    <option value="NIJGADH BRANCH">NIJGADH BRANCH</option>
                                                                                                                                    <option value="PATHALAIYA BRANCH">PATHALAIYA BRANCH</option>
                                                                                                                                    <option value="POKHARA BRANCH">POKHARA BRANCH</option>
                                                                                                                                    <option value="Province Office, Province 1 (Biratnagar)">Province Office, Province 1 (Biratnagar)</option>
                                                                                                                                    <option value="Province Office, Province 2 (Birgunj)">Province Office, Province 2 (Birgunj)</option>
                                                                                                                                    <option value="Province Office, Province 3  (Kathmandu)">Province Office, Province 3  (Kathmandu)</option>
                                                                                                                                    <option value="Province Office, Province 4  (Pokhara)">Province Office, Province 4  (Pokhara)</option>
                                                                                                                                    <option value="Province Office, Province 6  (Surkhet)">Province Office, Province 6  (Surkhet)</option>
                                                                                                                                    <option value="Province Office, Province 7 (Dhangadi)">Province Office, Province 7 (Dhangadi)</option>
                                                                                                                                    <option value="SAHIDBHUMI BRANCH">SAHIDBHUMI BRANCH</option>
                                                                                                                                    <option value="SAMAKHUSI BRANCH">SAMAKHUSI BRANCH</option>
                                                                                                                                    <option value="SANOSHREE BRANCH">SANOSHREE BRANCH</option>
                                                                                                                                    <option value="SARKEGHAD BRANCH">SARKEGHAD BRANCH</option>
                                                                                                                                    <option value="SIDDHARTHANAGAR BRANCH">SIDDHARTHANAGAR BRANCH</option>
                                                                                                                                    <option value="SIMALCHAUR BRANCH">SIMALCHAUR BRANCH</option>
                                                                                                                                    <option value="SIMIKOT BRANCH">SIMIKOT BRANCH</option>
                                                                                                                                    <option value="SUNAPATI BRANCH">SUNAPATI BRANCH</option>
                                                                                                                                    <option value="SURKHET BRANCH">SURKHET BRANCH</option>
                                                                                                                                    <option value="TEMAAL(KAVRE) BRANCH">TEMAAL(KAVRE) BRANCH</option>
                                                                                                                                    <option value="THAHITY BRANCH">THAHITY BRANCH</option>
                                                                                                                                    <option value="THANKOT BRANCH">THANKOT BRANCH</option>
                                                                                                                                    <option value="THAPATHALI BRANCH">THAPATHALI BRANCH</option>
                                                                                                                                    <option value="THIMI BRANCH">THIMI BRANCH</option>
                                                                                                                                    <option value="TOKHA BRANCH">TOKHA BRANCH</option>
                                                                                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="csrf_ctzn_token" value="529aa6bb3007849106ea3df52a3570bf" id="csrf"  />
                                        <div class="col-sm-6">
                                            <div class="check_options">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <label class="control-label" for="type"> Account *</label>
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <select class="styled-select capital_letter slate" name="type" id="account_name" onchange="showhide_section_minor()">
                                                                <option value="" selected>Select</option>
                                                                                                                                    <option value=" Citizens Sharedhani Bachat Khata"> Citizens Sharedhani Bachat Khata</option>
                                                                                                                                        <option value="Citizens Matribhumi Bachat">Citizens Matribhumi Bachat</option>
                                                                                                                                        <option value="Citizens Mahila Bachat">Citizens Mahila Bachat</option>
                                                                                                                                        <option value="Citizens Muna Bachat (Only for Minors)">Citizens Muna Bachat (Only for Minors)</option>
                                                                                                                                        <option value="Citizens Rastra Sewak Saving">Citizens Rastra Sewak Saving</option>
                                                                                                                                        <option value="Citizens Bidhyarthi Bachat">Citizens Bidhyarthi Bachat</option>
                                                                                                                                        <option value="Citizens Special Super Savings Account (CSSA)">Citizens Special Super Savings Account (CSSA)</option>
                                                                                                                                        <option value="Citizens G2P Savings Account">Citizens G2P Savings Account</option>
                                                                                                                                        <option value="Citizens Mofashal Bachat Khata">Citizens Mofashal Bachat Khata</option>
                                                                                                                                        <option value="Citizens Super Saving">Citizens Super Saving</option>
                                                                                                                                        <option value="Citizens Ghar Dailo Savings">Citizens Ghar Dailo Savings</option>
                                                                                                                                        <option value="Citizens Special Savings">Citizens Special Savings</option>
                                                                                                                                        <option value="Citizens Golden Savings">Citizens Golden Savings</option>
                                                                                                                                        <option value="Citizens Dasak Savings">Citizens Dasak Savings</option>
                                                                                                                                        <option value="Citizens Branchless Bachat">Citizens Branchless Bachat</option>
                                                                                                                                        <option value="Citizens Senior Citizens Savings">Citizens Senior Citizens Savings</option>
                                                                                                                                        <option value="Citizens Privilege Savings Account">Citizens Privilege Savings Account</option>
                                                                                                                                        <option value="Mero Citizens Bachat Khata">Mero Citizens Bachat Khata</option>
                                                                                                                                    </select>
                                                            </div>
                            <div class="row all-round-border first-allow-round">
                                                              <div id="acc_shareholder" hidden>
                                                                  <p class="text-center"> Only for shareholders of this bank.</p>
                                                              </div>
                                                          </div>
                                                            <input type="hidden"  name="account_name_count" id="account_name_count" value="18">

                                                        <!-- <div class="row">
                                                            <div id="intrest_rate" class="col-md-12 col-sm-12">
                                                                
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="row">
                                                <div id="intrest_rate" class="col-md-12 col-sm-12">

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row all-round-border first-allow-round">

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="row">

                                                    <div class="col-xs-12">
                                                        <label class="control-label" id='account_category' for="account_category"> Account Category *</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="radio"  name="account_category" id='individual_account' value="Individual Account" checked="checked"/>Individual Account
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="row all-round-border first-allow-round">

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="row">

                                                    <div class="col-xs-12">
                                                        <label class="control-label" id='types_of_individual' for="types_of_individual"> Types of Individual </label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="radio"  name="types_of_individual" id='types_of_individual' onchange="individual_section('Nepali')" value="Nepali" />Nepali
                                                    </div>
                                                    
                                                    <div class="col-sm-3">
                                                        <input type="radio"  name="types_of_individual" id='types_of_individual' onchange="individual_section('Indian')" value="Indian" />Indian
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="radio"  name="types_of_individual" id='types_of_individual' onchange="individual_section('NRN')" value="NRN" />NRN
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row all-round-border check_options">
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <label class="control-label" id='currency' for="currency"> Currency *</label>
                                                    </div>

                                                    <div id="currencies">
                                                        <div class="col-sm-4">
                                                            <input type="radio"  name="currency" id='npr' value="NPR" checked="checked"/>NPR
                                                        </div>
                                                        <!--<div class="col-sm-4">-->
                                                            <!--    <input type="radio"   name="currency" value="USD"/>USD-->
                                                            <!--</div>-->

                                                        </div>

                                                        <!--<div class="col-sm-4">-->
                                                            <!--    <input type="checkbox" name="currency" id="currency_others_checkbox" />Others-->
                                                            <!--</div>-->

                                                            <!--<div class="col-sm-4"  id="currencies_other">-->

                                                                <!--</div>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row all-round-border  check_options">
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label class="control-label" id='account_type'for="account_type"> Account Type *</label>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <input type="radio" name="account_type" id='single' onchange="showhide_section('single')" value="Single"/>Single
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <input type="radio"   name="account_type" onchange="showhide_section('joint')" id="joint_chechked" value="Joint" />Joint
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <input type="radio"  name="account_type" onchange="showhide_section('minor')" id="minor_checked"  value="Minor" />Minor
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                     
                                                </div>
                                            </div>
                                          <div class="row all-round-border  check_options">
                                              <div id="jointDet"hidden>
                                                  <p> Please click Add More Button to fill details of joint a/c holders.</p>
                                              </div>
                                          </div>
                                              <div class="row all-round-border  check_options">
                                                 
                                                  <div class="add-more-inner addmore_account_btn col-md-12" style="text-align:right">
                                                    <!-- <a id="add_account1" class="addmore">[+ Add more] </a>
                                                        <a id="remove_account1" class="addmore">[-Remove] </a> -->
            
                                                        <div class="ctz-btn ctz-blue" id="add_account1">Add More</div>
                                                        <div class="ctz-btn ctz-red" id="remove_account1">Remove</div>
                                                    </div>
                                                  </div>

                                             <div id="if_minor" class="middle_section">
                                                <div class="row all-round-border  check_options">
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <div class="col-sm-4 no-padding">
                                                                <label class="control-label" for="age"> Age of Minor</label>
                                                            </div>
                                                            <div class="col-sm-8 no-padding">
                                                                <input type="number" id="age_of_minor" max ="16" name="age" class="form-control minor_input" />
                                                            </div>
                                                        </div>
                                                    </div>
        
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <div class="col-sm-4 no-padding">
                                                                <label class="control-label" for="name_of_guardian">Name of Guardian</label>
                                                            </div>
                                                            <div class="col-sm-8 no-padding">
                                                                <input  maxlength="100" type="text" id="name_of_guardian" name="name_of_guardian" class="form-control minor_input" placeholder="Name of Guardian" />
                                                            </div>
                                                        </div>
                                                    </div>
        
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <div class="col-sm-4 no-padding">
                                                                <label class="control-label" for="relationship_minor">Relationship</label>
                                                            </div>
                                                            <div class="col-sm-8 no-padding">
                                                                <input  maxlength="100" type="text" id="relationship_minor" name="relationship_minor" class="form-control minor_input" placeholder="Relationship"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <!-- Middle Seciton Start -->
                                            <div class="middle_section" id="account_all">
                                                <div id="account_one1">
                                                    <div class="row all-round-border">
                                                        <div class="col-md-12">
                                                            <div class="step-1-account-header">
                                                                <h3 class="if_add_more" style="display:none">Account 1</h3>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12">
                                                            <div class="row">
                                                             <div class="form-group">
                                                                 <div class="col-md-2 col-sm-12">
                                                                     <label class="control-label" for="account_name">Account Name *</label> 
                                                                 </div>

                                                                 <div class="col-md-10 col-sm-12">

                                                                     <div class="row">
                                                                         <div class="col-sm-4 step-1-account-name">
                                                                             <input onkeyup="labelChangebykeyUp('1')"  maxlength="100" type="text" name="first_name[]" id="first_name1" class="form-control capital_letter" placeholder="First Name" required="">
                                                                         </div>
                                                                         <div class="col-sm-4">
                                                                             <input maxlength="100" type="text" name="middle_name[]" id="middle_name1" class="form-control capital_letter" placeholder="Middle Name">
                                                                         </div>
                                                                         <div class="col-sm-4">
                                                                             <input maxlength="100" type="text" name="last_name[]" id="last_name1" class="form-control capital_letter" placeholder="Last Name" required="">
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>

                                                         <div class="row">
                                                            <div class="col-md-6 col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-4 no-padding">
                                                                        <label class="control-label" for="father_name">Father's Name *</label>
                                                                    </div>
                                                                    <div class="col-sm-8 no-padding">
                                                                        <input  maxlength="100" type="text"  name="father_name[]" id="father_name1" class="form-control capital_letter" required="" placeholder="Father's Name" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-4 no-padding">
                                                                        <label class="control-label" for="grandfather_name">GrandFather's Name *</label>
                                                                    </div>
                                                                    <div class="col-sm-8 no-padding">
                                                                        <input  maxlength="100" type="text" name="grandfather_name[]" id="grandfather_name1" class="form-control capital_letter" required="" placeholder="GrandFather's Name" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6 col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-4 no-padding">
                                                                        <label class="control-label" for="spouse_name">Spouse's Name</label>
                                                                    </div>
                                                                    <div class="col-sm-8 no-padding">
                                                                        <input  maxlength="100" type="text"  name="spouse_name[]" id="spouse_name1" class="form-control capital_letter" placeholder="Spouse's Name" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="row">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-4">
                                                                            <label class="control-label" id="existing_relation_1" for="existing_relation">Existing Relation with Bank*</label>
                                                                        </div>
                                                                        <div class="col-sm-3 radio-container">
                                                                            <div class="radio-option">
                                                                                <input type="radio"  id="existing_relation1" name="existing_relation[0]" onchange="showhide_section_existing_relation('yes')"  value="yes" />Yes</div>
                                                                                <div class="radio-button">
                                                                                    <input type="radio"  id="existing_relation1" name="existing_relation[0]" onchange="showhide_section_existing_relation('no')" value="no" />No</div>

                                                                                </div>
                                                                                <div id="if_relation_yes1" class="col-sm-5">
                                                                                    <input type="text"  name="account_number[]" id="account_number1" class="form-control capital_letter" placeholder="Account Number" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div> 
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="col-sm-4 no-padding">
                                                                                <label class="control-label" for="occupation"> Occupation *</label>
                                                                            </div>
                                                                            <div class="col-sm-8 no-padding" >
                                                                                <select id="occupation1" name="occupation[]" class="styled-select slate" onchange="showhide_section_others()">
                                                                                    <option value="" selected="">Select</option>
                                                                                    <option value="salaried">Salaried</option>
                                                                                    <option value="service">Service</option>
                                                                                    <option value="self-employed">Self-employed</option>
                                                                                    <option value="student">Student</option>
                                                                                    <option value="housewife">Housewife</option>
                                                                                    <option value="others">Others</option>
                                                                                </select>
                                                                            </div>
                                                                <!-- <div class="col-sm-8 no-padding" >
                                                                <input  maxlength="100" type="text"  name="pa_district[]" id="pa_district1" class="form-control" placeholder="pa_district" />
                                                            </div> -->
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <div class="col-sm-4 no-padding">
                                                                <label class="control-label" id="date_of_birth_bs1" for="date_of_birth_bs1">Date of Birth (BS)*</label><br/>
                                                            </div>
                                                            <div class="col-sm-8 no-padding">
                                                                <select  class="styled-select slate" onchange="convert_into_english('1')"  name="yn[]" id="yn1" style="width:67px;" >
                                                                    <option value="" >Year</option>
                                                                                                                                            <option value="2076" id="2076">2076</option>
                                                                                                                                                <option value="2075" id="2075">2075</option>
                                                                                                                                                <option value="2074" id="2074">2074</option>
                                                                                                                                                <option value="2073" id="2073">2073</option>
                                                                                                                                                <option value="2072" id="2072">2072</option>
                                                                                                                                                <option value="2071" id="2071">2071</option>
                                                                                                                                                <option value="2070" id="2070">2070</option>
                                                                                                                                                <option value="2069" id="2069">2069</option>
                                                                                                                                                <option value="2068" id="2068">2068</option>
                                                                                                                                                <option value="2067" id="2067">2067</option>
                                                                                                                                                <option value="2066" id="2066">2066</option>
                                                                                                                                                <option value="2065" id="2065">2065</option>
                                                                                                                                                <option value="2064" id="2064">2064</option>
                                                                                                                                                <option value="2063" id="2063">2063</option>
                                                                                                                                                <option value="2062" id="2062">2062</option>
                                                                                                                                                <option value="2061" id="2061">2061</option>
                                                                                                                                                <option value="2060" id="2060">2060</option>
                                                                                                                                                <option value="2059" id="2059">2059</option>
                                                                                                                                                <option value="2058" id="2058">2058</option>
                                                                                                                                                <option value="2057" id="2057">2057</option>
                                                                                                                                                <option value="2056" id="2056">2056</option>
                                                                                                                                                <option value="2055" id="2055">2055</option>
                                                                                                                                                <option value="2054" id="2054">2054</option>
                                                                                                                                                <option value="2053" id="2053">2053</option>
                                                                                                                                                <option value="2052" id="2052">2052</option>
                                                                                                                                                <option value="2051" id="2051">2051</option>
                                                                                                                                                <option value="2050" id="2050">2050</option>
                                                                                                                                                <option value="2049" id="2049">2049</option>
                                                                                                                                                <option value="2048" id="2048">2048</option>
                                                                                                                                                <option value="2047" id="2047">2047</option>
                                                                                                                                                <option value="2046" id="2046">2046</option>
                                                                                                                                                <option value="2045" id="2045">2045</option>
                                                                                                                                                <option value="2044" id="2044">2044</option>
                                                                                                                                                <option value="2043" id="2043">2043</option>
                                                                                                                                                <option value="2042" id="2042">2042</option>
                                                                                                                                                <option value="2041" id="2041">2041</option>
                                                                                                                                                <option value="2040" id="2040">2040</option>
                                                                                                                                                <option value="2039" id="2039">2039</option>
                                                                                                                                                <option value="2038" id="2038">2038</option>
                                                                                                                                                <option value="2037" id="2037">2037</option>
                                                                                                                                                <option value="2036" id="2036">2036</option>
                                                                                                                                                <option value="2035" id="2035">2035</option>
                                                                                                                                                <option value="2034" id="2034">2034</option>
                                                                                                                                                <option value="2033" id="2033">2033</option>
                                                                                                                                                <option value="2032" id="2032">2032</option>
                                                                                                                                                <option value="2031" id="2031">2031</option>
                                                                                                                                                <option value="2030" id="2030">2030</option>
                                                                                                                                                <option value="2029" id="2029">2029</option>
                                                                                                                                                <option value="2028" id="2028">2028</option>
                                                                                                                                                <option value="2027" id="2027">2027</option>
                                                                                                                                                <option value="2026" id="2026">2026</option>
                                                                                                                                                <option value="2025" id="2025">2025</option>
                                                                                                                                                <option value="2024" id="2024">2024</option>
                                                                                                                                                <option value="2023" id="2023">2023</option>
                                                                                                                                                <option value="2022" id="2022">2022</option>
                                                                                                                                                <option value="2021" id="2021">2021</option>
                                                                                                                                                <option value="2020" id="2020">2020</option>
                                                                                                                                                <option value="2019" id="2019">2019</option>
                                                                                                                                                <option value="2018" id="2018">2018</option>
                                                                                                                                                <option value="2017" id="2017">2017</option>
                                                                                                                                                <option value="2016" id="2016">2016</option>
                                                                                                                                                <option value="2015" id="2015">2015</option>
                                                                                                                                                <option value="2014" id="2014">2014</option>
                                                                                                                                                <option value="2013" id="2013">2013</option>
                                                                                                                                                <option value="2012" id="2012">2012</option>
                                                                                                                                                <option value="2011" id="2011">2011</option>
                                                                                                                                                <option value="2010" id="2010">2010</option>
                                                                                                                                                <option value="2009" id="2009">2009</option>
                                                                                                                                                <option value="2008" id="2008">2008</option>
                                                                                                                                                <option value="2007" id="2007">2007</option>
                                                                                                                                                <option value="2006" id="2006">2006</option>
                                                                                                                                                <option value="2005" id="2005">2005</option>
                                                                                                                                                <option value="2004" id="2004">2004</option>
                                                                                                                                                <option value="2003" id="2003">2003</option>
                                                                                                                                                <option value="2002" id="2002">2002</option>
                                                                                                                                                <option value="2001" id="2001">2001</option>
                                                                                                                                                <option value="2000" id="2000">2000</option>
                                                                                                                                                <option value="1999" id="1999">1999</option>
                                                                                                                                                <option value="1998" id="1998">1998</option>
                                                                                                                                                <option value="1997" id="1997">1997</option>
                                                                                                                                                <option value="1996" id="1996">1996</option>
                                                                                                                                                <option value="1995" id="1995">1995</option>
                                                                                                                                                <option value="1994" id="1994">1994</option>
                                                                                                                                                <option value="1993" id="1993">1993</option>
                                                                                                                                                <option value="1992" id="1992">1992</option>
                                                                                                                                                <option value="1991" id="1991">1991</option>
                                                                                                                                                <option value="1990" id="1990">1990</option>
                                                                                                                                                <option value="1989" id="1989">1989</option>
                                                                                                                                                <option value="1988" id="1988">1988</option>
                                                                                                                                                <option value="1987" id="1987">1987</option>
                                                                                                                                                <option value="1986" id="1986">1986</option>
                                                                                                                                                <option value="1985" id="1985">1985</option>
                                                                                                                                                <option value="1984" id="1984">1984</option>
                                                                                                                                                <option value="1983" id="1983">1983</option>
                                                                                                                                                <option value="1982" id="1982">1982</option>
                                                                                                                                                <option value="1981" id="1981">1981</option>
                                                                                                                                                <option value="1980" id="1980">1980</option>
                                                                                                                                                <option value="1979" id="1979">1979</option>
                                                                                                                                                <option value="1978" id="1978">1978</option>
                                                                                                                                                <option value="1977" id="1977">1977</option>
                                                                                                                                                <option value="1976" id="1976">1976</option>
                                                                                                                                                <option value="1975" id="1975">1975</option>
                                                                                                                                            </select>
                                                                    <select class="styled-select slate" onchange="convert_into_english('1')"  name="mn[]" id="mn1" style="width:75px"
                                                                    >
                                                                    <option value="">Month</option>
                                                                                                                                            <option value="12">12</option>
                                                                                                                                                <option value="11">11</option>
                                                                                                                                                <option value="10">10</option>
                                                                                                                                                <option value="9">9</option>
                                                                                                                                                <option value="8">8</option>
                                                                                                                                                <option value="7">7</option>
                                                                                                                                                <option value="6">6</option>
                                                                                                                                                <option value="5">5</option>
                                                                                                                                                <option value="4">4</option>
                                                                                                                                                <option value="3">3</option>
                                                                                                                                                <option value="2">2</option>
                                                                                                                                                <option value="1">1</option>
                                                                                                                                            </select>
                                                                    <select  class="styled-select slate" onchange="convert_into_english('1')"  name="dn[]" id="dn1" style="width:62px"
                                                                    >
                                                                    <option value="">Day</option>
                                                                                                                                            <option value="32">32</option>
                                                                                                                                                <option value="31">31</option>
                                                                                                                                                <option value="30">30</option>
                                                                                                                                                <option value="29">29</option>
                                                                                                                                                <option value="28">28</option>
                                                                                                                                                <option value="27">27</option>
                                                                                                                                                <option value="26">26</option>
                                                                                                                                                <option value="25">25</option>
                                                                                                                                                <option value="24">24</option>
                                                                                                                                                <option value="23">23</option>
                                                                                                                                                <option value="22">22</option>
                                                                                                                                                <option value="21">21</option>
                                                                                                                                                <option value="20">20</option>
                                                                                                                                                <option value="19">19</option>
                                                                                                                                                <option value="18">18</option>
                                                                                                                                                <option value="17">17</option>
                                                                                                                                                <option value="16">16</option>
                                                                                                                                                <option value="15">15</option>
                                                                                                                                                <option value="14">14</option>
                                                                                                                                                <option value="13">13</option>
                                                                                                                                                <option value="12">12</option>
                                                                                                                                                <option value="11">11</option>
                                                                                                                                                <option value="10">10</option>
                                                                                                                                                <option value="9">9</option>
                                                                                                                                                <option value="8">8</option>
                                                                                                                                                <option value="7">7</option>
                                                                                                                                                <option value="6">6</option>
                                                                                                                                                <option value="5">5</option>
                                                                                                                                                <option value="4">4</option>
                                                                                                                                                <option value="3">3</option>
                                                                                                                                                <option value="2">2</option>
                                                                                                                                                <option value="1">1</option>
                                                                                                                                            </select>
                                                                    <!-- <div class="btn btn-success pull-right" onclick="convert_into_english('1')">Convert into AD</div> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div id="if_others1">
                                                            <div class="col-md-6 col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-4 no-padding">
                                                                        <label class="control-label"for="occupation_input"></label>
                                                                    </div>
                                                                    <div class="col-sm-8 no-padding" >
                                                                        <input type="text"  name="occupation_input[]" id="occupation_input1" class="form-control occupation_input" placeholder="Please specify the nature of business"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div id="if_services1">
                                                            <div class="col-md-6 col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-4 no-padding">
                                                                        <label class="control-label" for="occupation_service_company">Company Name</label>
                                                                    </div>
                                                                    <div class="col-sm-8 no-padding" >
                                                                        <input type="text"  name="occupation_service_company[]" id="occupation_service_company1" class="form-control occupation_service_company" placeholder="Please specify the Company Name"/>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                             <div class="col-md-6 col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-4 no-padding">
                                                                        <label class="control-label" for="occupation_service_address">Address</label>
                                                                    </div>
                                                                    <div class="col-sm-8 no-padding" >
                                                                        <input type="text"  name="occupation_service_address[]" id="occupation_service_address1" class="form-control occupation_service_address" placeholder="Please specify the Company Address"/>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                             <div class="col-md-6 col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-4 no-padding">
                                                                        <label class="control-label"for="occupation_service_position">Position</label>
                                                                    </div>
                                                                    <div class="col-sm-8 no-padding" >
                                                                       <input type="text"  name="occupation_service_position[]" id="occupation_service_position1" class="form-control occupation_service_position" placeholder="Please specify your Position"/>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="row">

                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <div class="col-sm-4 no-padding">
                                                                    <label class="control-label" for="nationality">Nationality *</label>
                                                                </div>
                                                                <div class="col-sm-8 no-padding">
                                                                    <input  maxlength="100" type="text"  name="nationality[]" id="nationality1" class="form-control" required="" placeholder="Nationality" />
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="col-md-6 col-sm-6">
                                                         <div class="form-group">
                                                            <div class="col-sm-4 no-padding">
                                                                <label class="control-label" id="date_of_birth_ad1" for="date_of_birth_ad1">Date of Birth (AD)*</label><br/>
                                                            </div>
                                                            <div class="col-sm-8 no-padding">
                                                                <select onchange="convert_into_nepali('1')"  class="styled-select slate" name="y[]" id="y1" style="width:67px;" >
                                                                    <option value=""  >Year</option>
                                                                                                                                            <option value="2019" >2019</option>
                                                                                                                                                <option value="2018" >2018</option>
                                                                                                                                                <option value="2017" >2017</option>
                                                                                                                                                <option value="2016" >2016</option>
                                                                                                                                                <option value="2015" >2015</option>
                                                                                                                                                <option value="2014" >2014</option>
                                                                                                                                                <option value="2013" >2013</option>
                                                                                                                                                <option value="2012" >2012</option>
                                                                                                                                                <option value="2011" >2011</option>
                                                                                                                                                <option value="2010" >2010</option>
                                                                                                                                                <option value="2009" >2009</option>
                                                                                                                                                <option value="2008" >2008</option>
                                                                                                                                                <option value="2007" >2007</option>
                                                                                                                                                <option value="2006" >2006</option>
                                                                                                                                                <option value="2005" >2005</option>
                                                                                                                                                <option value="2004" >2004</option>
                                                                                                                                                <option value="2003" >2003</option>
                                                                                                                                                <option value="2002" >2002</option>
                                                                                                                                                <option value="2001" >2001</option>
                                                                                                                                                <option value="2000" >2000</option>
                                                                                                                                                <option value="1999" >1999</option>
                                                                                                                                                <option value="1998" >1998</option>
                                                                                                                                                <option value="1997" >1997</option>
                                                                                                                                                <option value="1996" >1996</option>
                                                                                                                                                <option value="1995" >1995</option>
                                                                                                                                                <option value="1994" >1994</option>
                                                                                                                                                <option value="1993" >1993</option>
                                                                                                                                                <option value="1992" >1992</option>
                                                                                                                                                <option value="1991" >1991</option>
                                                                                                                                                <option value="1990" >1990</option>
                                                                                                                                                <option value="1989" >1989</option>
                                                                                                                                                <option value="1988" >1988</option>
                                                                                                                                                <option value="1987" >1987</option>
                                                                                                                                                <option value="1986" >1986</option>
                                                                                                                                                <option value="1985" >1985</option>
                                                                                                                                                <option value="1984" >1984</option>
                                                                                                                                                <option value="1983" >1983</option>
                                                                                                                                                <option value="1982" >1982</option>
                                                                                                                                                <option value="1981" >1981</option>
                                                                                                                                                <option value="1980" >1980</option>
                                                                                                                                                <option value="1979" >1979</option>
                                                                                                                                                <option value="1978" >1978</option>
                                                                                                                                                <option value="1977" >1977</option>
                                                                                                                                                <option value="1976" >1976</option>
                                                                                                                                                <option value="1975" >1975</option>
                                                                                                                                                <option value="1974" >1974</option>
                                                                                                                                                <option value="1973" >1973</option>
                                                                                                                                                <option value="1972" >1972</option>
                                                                                                                                                <option value="1971" >1971</option>
                                                                                                                                                <option value="1970" >1970</option>
                                                                                                                                                <option value="1969" >1969</option>
                                                                                                                                                <option value="1968" >1968</option>
                                                                                                                                                <option value="1967" >1967</option>
                                                                                                                                                <option value="1966" >1966</option>
                                                                                                                                                <option value="1965" >1965</option>
                                                                                                                                                <option value="1964" >1964</option>
                                                                                                                                                <option value="1963" >1963</option>
                                                                                                                                                <option value="1962" >1962</option>
                                                                                                                                                <option value="1961" >1961</option>
                                                                                                                                                <option value="1960" >1960</option>
                                                                                                                                                <option value="1959" >1959</option>
                                                                                                                                                <option value="1958" >1958</option>
                                                                                                                                                <option value="1957" >1957</option>
                                                                                                                                                <option value="1956" >1956</option>
                                                                                                                                                <option value="1955" >1955</option>
                                                                                                                                                <option value="1954" >1954</option>
                                                                                                                                                <option value="1953" >1953</option>
                                                                                                                                                <option value="1952" >1952</option>
                                                                                                                                                <option value="1951" >1951</option>
                                                                                                                                                <option value="1950" >1950</option>
                                                                                                                                                <option value="1949" >1949</option>
                                                                                                                                                <option value="1948" >1948</option>
                                                                                                                                                <option value="1947" >1947</option>
                                                                                                                                                <option value="1946" >1946</option>
                                                                                                                                                <option value="1945" >1945</option>
                                                                                                                                                <option value="1944" >1944</option>
                                                                                                                                                <option value="1943" >1943</option>
                                                                                                                                                <option value="1942" >1942</option>
                                                                                                                                                <option value="1941" >1941</option>
                                                                                                                                                <option value="1940" >1940</option>
                                                                                                                                                <option value="1939" >1939</option>
                                                                                                                                                <option value="1938" >1938</option>
                                                                                                                                                <option value="1937" >1937</option>
                                                                                                                                                <option value="1936" >1936</option>
                                                                                                                                                <option value="1935" >1935</option>
                                                                                                                                                <option value="1934" >1934</option>
                                                                                                                                                <option value="1933" >1933</option>
                                                                                                                                                <option value="1932" >1932</option>
                                                                                                                                                <option value="1931" >1931</option>
                                                                                                                                                <option value="1930" >1930</option>
                                                                                                                                                <option value="1929" >1929</option>
                                                                                                                                                <option value="1928" >1928</option>
                                                                                                                                                <option value="1927" >1927</option>
                                                                                                                                                <option value="1926" >1926</option>
                                                                                                                                                <option value="1925" >1925</option>
                                                                                                                                                <option value="1924" >1924</option>
                                                                                                                                                <option value="1923" >1923</option>
                                                                                                                                                <option value="1922" >1922</option>
                                                                                                                                                <option value="1921" >1921</option>
                                                                                                                                                <option value="1920" >1920</option>
                                                                                                                                                <option value="1919" >1919</option>
                                                                                                                                                <option value="1918" >1918</option>
                                                                                                                                                <option value="1917" >1917</option>
                                                                                                                                                <option value="1916" >1916</option>
                                                                                                                                                <option value="1915" >1915</option>
                                                                                                                                                <option value="1914" >1914</option>
                                                                                                                                                <option value="1913" >1913</option>
                                                                                                                                            </select>
                                                                    <select onchange="convert_into_nepali('1')" class="styled-select slate"  name="m[]" id="m1" style="width:75px"
                                                                    >
                                                                    <option value="">Month</option>
                                                                                                                                            <option value="12">12</option>
                                                                                                                                                <option value="11">11</option>
                                                                                                                                                <option value="10">10</option>
                                                                                                                                                <option value="9">9</option>
                                                                                                                                                <option value="8">8</option>
                                                                                                                                                <option value="7">7</option>
                                                                                                                                                <option value="6">6</option>
                                                                                                                                                <option value="5">5</option>
                                                                                                                                                <option value="4">4</option>
                                                                                                                                                <option value="3">3</option>
                                                                                                                                                <option value="2">2</option>
                                                                                                                                                <option value="1">1</option>
                                                                                                                                            </select>
                                                                    <select onchange="convert_into_nepali('1')"  class="styled-select slate" name="d[]" id="d1" style="width:62px"
                                                                    >
                                                                    <option value="">Day</option>
                                                                                                                                            <option value="32">32</option>
                                                                                                                                                <option value="31">31</option>
                                                                                                                                                <option value="30">30</option>
                                                                                                                                                <option value="29">29</option>
                                                                                                                                                <option value="28">28</option>
                                                                                                                                                <option value="27">27</option>
                                                                                                                                                <option value="26">26</option>
                                                                                                                                                <option value="25">25</option>
                                                                                                                                                <option value="24">24</option>
                                                                                                                                                <option value="23">23</option>
                                                                                                                                                <option value="22">22</option>
                                                                                                                                                <option value="21">21</option>
                                                                                                                                                <option value="20">20</option>
                                                                                                                                                <option value="19">19</option>
                                                                                                                                                <option value="18">18</option>
                                                                                                                                                <option value="17">17</option>
                                                                                                                                                <option value="16">16</option>
                                                                                                                                                <option value="15">15</option>
                                                                                                                                                <option value="14">14</option>
                                                                                                                                                <option value="13">13</option>
                                                                                                                                                <option value="12">12</option>
                                                                                                                                                <option value="11">11</option>
                                                                                                                                                <option value="10">10</option>
                                                                                                                                                <option value="9">9</option>
                                                                                                                                                <option value="8">8</option>
                                                                                                                                                <option value="7">7</option>
                                                                                                                                                <option value="6">6</option>
                                                                                                                                                <option value="5">5</option>
                                                                                                                                                <option value="4">4</option>
                                                                                                                                                <option value="3">3</option>
                                                                                                                                                <option value="2">2</option>
                                                                                                                                                <option value="1">1</option>
                                                                                                                                            </select>
                                                                <!-- <div class="btn btn-success pull-right" onclick="convert_into_nepali('1')">Convert into BS</div>
                                                                --> 
                                                            </div>
                                                        </div> 
                                                    </div>
                                                </div>

                                                

                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <div class="col-sm-4 no-padding">
                                                                <label class="control-label" for="passport_citizenship">Identity Verification Card *</label>
                                                            </div>
                                                            <div class="col-sm-8 no-padding">
                                                                <select id="passport_citizenship1" name="passport_citizenship[]" onchange="showhide_section_passport()" class="styled-select slate">
                                                                    <option value="" selected="">Select</option>
                                                                    <option value="passport">Passport</option>
                                                                    <option value="citizenship">Citizenship</option>
                                                                    <option value="nrn_card">NRN Card</option>
                                                                    <option value="birth_certificate">Birth Certificate</option>
                                                                    <option value="others">Others</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <div class="col-sm-4 no-padding">
                                                                <label class="control-label" id="gender1" for="gender">Gender *</label>
                                                            </div>
                                                            <div class="col-sm-8 no-padding radio-margin-bottom">
                                                                <div class="radio-contianer">
                                                                    <div class="radio-option">
                                                                        <input type="radio"  name="gender[0]" id="male1" value="Male" />Male
                                                                    </div>
                                                                    <div class="radio-option">
                                                                        <input type="radio"  name="gender[0]" value="Female" />Female
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div id="if_passport1">
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <div class="col-sm-4 no-padding">
                                                                    <label class="control-label" for="passport_no"> Passport No.* </label>
                                                                </div>
                                                                <div class="col-sm-8 no-padding">
                                                                    <input type="text" name="passport_no[]" id="passport_no1" class="form-control passportp_no" placeholder="Passport No"/>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="if_expiry_date1">
                                                            <div class="col-md-6 col-md-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-4 no-padding">
                                                                        <label class="control-label" for="expiry_date"> Expiry Date.* </label>
                                                                    </div>
                                                                    <div class="col-sm-8 no-padding">
                                                                        <input type="date" name="expiry_date[]" id="expiry_date1" class="form-control expiry date" placeholder="Expiry Date"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="if_citizenship1">
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <div class="col-sm-4 no-padding">
                                                                    <label class="control-label" for="citizenship_no">Citizenship No.*</label>
                                                                </div>
                                                                <div class="col-sm-8 no-padding">
                                                                    <input  maxlength="100" name="citizenship_no[]" id="citizenship_no1" type="text" class="form-control citizenship_no" placeholder="Citizenship No"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div id="if_nrn_card1">
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <div class="col-sm-4 no-padding">
                                                                    <label class="control-label" for="nrn_card">NRN Card.*</label>
                                                                </div>
                                                                <div class="col-sm-8 no-padding">
                                                                    <input  maxlength="100" name="nrn_card[]" id="nrn_card1" type="text" class="form-control nrn_card" placeholder="NRN Card"/>
                                                                </div>
                                                            </div>
                                                        </div> 

                                                        <div id="if_foreign_address1">
                                                            <div class="col-md-6 col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-4 no-padding">
                                                                        <label class="control-label" for="foreign_address">Foreign Address.*</label>
                                                                    </div>
                                                                    <div class="col-sm-8 no-padding">
                                                                        <input  maxlength="100" name="foreign_address[]" id="foreign_address1" type="text" class="form-control foreign_address" placeholder="Foreign Address"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="if_country1">
                                                            <div class="col-md-6 col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-4 no-padding">
                                                                        <label class="control-label"for="country">Country.*</label>
                                                                    </div>
                                                                    <div class="col-sm-8 no-padding">
                                                                        <input  maxlength="100" name="country[]" id="country1" type="text" class="form-control country" placeholder="Country"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="if_city_state1">
                                                            <div class="col-md-6 col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-4 no-padding">
                                                                        <label class="control-label" for="city_state">City_State.*</label>
                                                                    </div>
                                                                    <div class="col-sm-8 no-padding">
                                                                        <input  maxlength="100" name="city_state[]" id="city_state1" type="text" class="form-control City_State" placeholder="City_State"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="if_contact_no.1">
                                                            <div class="col-md-6 col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-4 no-padding">
                                                                        <label class="control-label" for="contact_no">Contact number.*</label>
                                                                    </div>
                                                                    <div class="col-sm-8 no-padding">
                                                                        <input  maxlength="100" name="contact_no[]" id="contact_no1" type="text" class="form-control contact_no" placeholder="Contact number"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="if_type_of_visa1">
                                                            <div class="col-md-6 col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-4 no-padding">
                                                                        <label class="control-label" for="type_of_visa">Type of Visa.*</label>
                                                                    </div>
                                                                    <div class="col-sm-8 no-padding">
                                                                        <input  maxlength="100" name="type_of_visa[]" id="type_of_visa1" type="text" class="form-control type_of_visa" placeholder="Type of Visa"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="if_visa_expiry_date1">
                                                            <div class="col-md-6 col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-4 no-padding">
                                                                        <label class="control-label" for="visa_expiry_date">Visa expiry date.*</label>
                                                                    </div>
                                                                    <div class="col-sm-8 no-padding">
                                                                        <input  maxlength="100" name="visa_expiry_date[]" id="visa_expiry_date1" type="date" class="form-control visa_expiry_date" placeholder="Visa expiry date"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div id="if_birth_certificate1">
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <div class="col-sm-4 no-padding">
                                                                    <label class="control-label" for="birth_certificate">Birth Certificate.*</label>
                                                                </div>
                                                                <div class="col-sm-8 no-padding">
                                                                    <input  maxlength="100" name="birth_certificate[]" id="birth_certificate1" type="text" class="form-control birth_certificate" placeholder="Birth Certificate"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="if_other_id1">
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <div class="col-sm-4 no-padding">
                                                                    <label class="control-label" for="other_id">Others.*</label>
                                                                </div>
                                                                <div class="col-sm-8 no-padding">
                                                                    <input  maxlength="100" name="other_id[]" id="other_id1" type="text" class="form-control other_id" placeholder="Others"/>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <div class="col-sm-4 no-padding">
                                                                <label class="control-label"for="place_of_issue"> Place of Issue* </label>
                                                            </div>
                                                            <div class="col-sm-8 no-padding">
                                                                <select id="place_of_issue1" name="place_of_issue[]" class="styled-select slate">
                                                                    <option value="" selected="">Select Place of Issue</option>
                                                                                                                                            <option value="Achham">Achham</option>
                                                                                                                                            <option value="Arghakhanchi">Arghakhanchi</option>
                                                                                                                                            <option value="Baglung">Baglung</option>
                                                                                                                                            <option value="Baitadi">Baitadi</option>
                                                                                                                                            <option value="Bajhang">Bajhang</option>
                                                                                                                                            <option value="Bajura">Bajura</option>
                                                                                                                                            <option value="Banke">Banke</option>
                                                                                                                                            <option value="Bara">Bara</option>
                                                                                                                                            <option value="Bardiya">Bardiya</option>
                                                                                                                                            <option value="Bhaktapur">Bhaktapur</option>
                                                                                                                                            <option value="Bhojpur">Bhojpur</option>
                                                                                                                                            <option value="Chitwan">Chitwan</option>
                                                                                                                                            <option value="Dadeldhura">Dadeldhura</option>
                                                                                                                                            <option value="Dailekh">Dailekh</option>
                                                                                                                                            <option value="Dang Deukhuri">Dang Deukhuri</option>
                                                                                                                                            <option value="Darchula">Darchula</option>
                                                                                                                                            <option value="Dhading">Dhading</option>
                                                                                                                                            <option value="Dhankuta">Dhankuta</option>
                                                                                                                                            <option value="Dhanusa">Dhanusa</option>
                                                                                                                                            <option value="Dholkha">Dholkha</option>
                                                                                                                                            <option value="Dolpa">Dolpa</option>
                                                                                                                                            <option value="Doti">Doti</option>
                                                                                                                                            <option value="Gorkha">Gorkha</option>
                                                                                                                                            <option value="Gulmi">Gulmi</option>
                                                                                                                                            <option value="Humla">Humla</option>
                                                                                                                                            <option value="Ilam">Ilam</option>
                                                                                                                                            <option value="Jajarkot">Jajarkot</option>
                                                                                                                                            <option value="Jhapa">Jhapa</option>
                                                                                                                                            <option value="Jumla">Jumla</option>
                                                                                                                                            <option value="Kailali">Kailali</option>
                                                                                                                                            <option value="Kalikot">Kalikot</option>
                                                                                                                                            <option value="Kanchanpur">Kanchanpur</option>
                                                                                                                                            <option value="Kapilvastu">Kapilvastu</option>
                                                                                                                                            <option value="Kaski">Kaski</option>
                                                                                                                                            <option value="Kathmandu">Kathmandu</option>
                                                                                                                                            <option value="Kavrepalanchok">Kavrepalanchok</option>
                                                                                                                                            <option value="Khotang">Khotang</option>
                                                                                                                                            <option value="Lalitpur">Lalitpur</option>
                                                                                                                                            <option value="Lamjung">Lamjung</option>
                                                                                                                                            <option value="Mahottari">Mahottari</option>
                                                                                                                                            <option value="Makwanpur">Makwanpur</option>
                                                                                                                                            <option value="Manang">Manang</option>
                                                                                                                                            <option value="Morang">Morang</option>
                                                                                                                                            <option value="Mugu">Mugu</option>
                                                                                                                                            <option value="Mustang">Mustang</option>
                                                                                                                                            <option value="Myagdi">Myagdi</option>
                                                                                                                                            <option value="Nawalparasi">Nawalparasi</option>
                                                                                                                                            <option value="Nuwakot">Nuwakot</option>
                                                                                                                                            <option value="Okhaldhunga">Okhaldhunga</option>
                                                                                                                                            <option value="Palpa">Palpa</option>
                                                                                                                                            <option value="Panchthar">Panchthar</option>
                                                                                                                                            <option value="Parbat">Parbat</option>
                                                                                                                                            <option value="Parsa">Parsa</option>
                                                                                                                                            <option value="Pyuthan">Pyuthan</option>
                                                                                                                                            <option value="Ramechhap">Ramechhap</option>
                                                                                                                                            <option value="Rasuwa">Rasuwa</option>
                                                                                                                                            <option value="Rautahat">Rautahat</option>
                                                                                                                                            <option value="Rolpa">Rolpa</option>
                                                                                                                                            <option value="Rukum">Rukum</option>
                                                                                                                                            <option value="Rupandehi">Rupandehi</option>
                                                                                                                                            <option value="Salyan">Salyan</option>
                                                                                                                                            <option value="Sankhuwasabha">Sankhuwasabha</option>
                                                                                                                                            <option value="Saptari">Saptari</option>
                                                                                                                                            <option value="Sarlahi">Sarlahi</option>
                                                                                                                                            <option value="Sindhuli">Sindhuli</option>
                                                                                                                                            <option value="Sindhupalchok">Sindhupalchok</option>
                                                                                                                                            <option value="Siraha">Siraha</option>
                                                                                                                                            <option value="Solukhumbu">Solukhumbu</option>
                                                                                                                                            <option value="Sunsari">Sunsari</option>
                                                                                                                                            <option value="Surkhet">Surkhet</option>
                                                                                                                                            <option value="Syangja">Syangja</option>
                                                                                                                                            <option value="Tanahu">Tanahu</option>
                                                                                                                                            <option value="Taplejung">Taplejung</option>
                                                                                                                                            <option value="Terhathum">Terhathum</option>
                                                                                                                                            <option value="Udayapur">Udayapur</option>
                                                                                                                                    </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <div class="col-sm-4 no-padding">
                                                                <label class="control-label" id = "date_of_issue_bs1" for="date_of_issue_bs1">Date of Issue(BS) *</label>
                                                            </div>
                                                            <div class="col-sm-8 no-padding">
                                                                <select  class="styled-select slate" onchange="convert_into_english_issue('1')"  name="doi_bs_year[]" id="doi_bs_year1" style="width:67px;" >
                                                                    <option value="" >Year</option>
                                                                                                                                            <option value="2076" id="2076">2076</option>
                                                                                                                                                <option value="2075" id="2075">2075</option>
                                                                                                                                                <option value="2074" id="2074">2074</option>
                                                                                                                                                <option value="2073" id="2073">2073</option>
                                                                                                                                                <option value="2072" id="2072">2072</option>
                                                                                                                                                <option value="2071" id="2071">2071</option>
                                                                                                                                                <option value="2070" id="2070">2070</option>
                                                                                                                                                <option value="2069" id="2069">2069</option>
                                                                                                                                                <option value="2068" id="2068">2068</option>
                                                                                                                                                <option value="2067" id="2067">2067</option>
                                                                                                                                                <option value="2066" id="2066">2066</option>
                                                                                                                                                <option value="2065" id="2065">2065</option>
                                                                                                                                                <option value="2064" id="2064">2064</option>
                                                                                                                                                <option value="2063" id="2063">2063</option>
                                                                                                                                                <option value="2062" id="2062">2062</option>
                                                                                                                                                <option value="2061" id="2061">2061</option>
                                                                                                                                                <option value="2060" id="2060">2060</option>
                                                                                                                                                <option value="2059" id="2059">2059</option>
                                                                                                                                                <option value="2058" id="2058">2058</option>
                                                                                                                                                <option value="2057" id="2057">2057</option>
                                                                                                                                                <option value="2056" id="2056">2056</option>
                                                                                                                                                <option value="2055" id="2055">2055</option>
                                                                                                                                                <option value="2054" id="2054">2054</option>
                                                                                                                                                <option value="2053" id="2053">2053</option>
                                                                                                                                                <option value="2052" id="2052">2052</option>
                                                                                                                                                <option value="2051" id="2051">2051</option>
                                                                                                                                                <option value="2050" id="2050">2050</option>
                                                                                                                                                <option value="2049" id="2049">2049</option>
                                                                                                                                                <option value="2048" id="2048">2048</option>
                                                                                                                                                <option value="2047" id="2047">2047</option>
                                                                                                                                                <option value="2046" id="2046">2046</option>
                                                                                                                                                <option value="2045" id="2045">2045</option>
                                                                                                                                                <option value="2044" id="2044">2044</option>
                                                                                                                                                <option value="2043" id="2043">2043</option>
                                                                                                                                                <option value="2042" id="2042">2042</option>
                                                                                                                                                <option value="2041" id="2041">2041</option>
                                                                                                                                                <option value="2040" id="2040">2040</option>
                                                                                                                                                <option value="2039" id="2039">2039</option>
                                                                                                                                                <option value="2038" id="2038">2038</option>
                                                                                                                                                <option value="2037" id="2037">2037</option>
                                                                                                                                                <option value="2036" id="2036">2036</option>
                                                                                                                                                <option value="2035" id="2035">2035</option>
                                                                                                                                                <option value="2034" id="2034">2034</option>
                                                                                                                                                <option value="2033" id="2033">2033</option>
                                                                                                                                                <option value="2032" id="2032">2032</option>
                                                                                                                                                <option value="2031" id="2031">2031</option>
                                                                                                                                                <option value="2030" id="2030">2030</option>
                                                                                                                                                <option value="2029" id="2029">2029</option>
                                                                                                                                                <option value="2028" id="2028">2028</option>
                                                                                                                                                <option value="2027" id="2027">2027</option>
                                                                                                                                                <option value="2026" id="2026">2026</option>
                                                                                                                                                <option value="2025" id="2025">2025</option>
                                                                                                                                                <option value="2024" id="2024">2024</option>
                                                                                                                                                <option value="2023" id="2023">2023</option>
                                                                                                                                                <option value="2022" id="2022">2022</option>
                                                                                                                                                <option value="2021" id="2021">2021</option>
                                                                                                                                                <option value="2020" id="2020">2020</option>
                                                                                                                                                <option value="2019" id="2019">2019</option>
                                                                                                                                                <option value="2018" id="2018">2018</option>
                                                                                                                                                <option value="2017" id="2017">2017</option>
                                                                                                                                                <option value="2016" id="2016">2016</option>
                                                                                                                                                <option value="2015" id="2015">2015</option>
                                                                                                                                                <option value="2014" id="2014">2014</option>
                                                                                                                                                <option value="2013" id="2013">2013</option>
                                                                                                                                                <option value="2012" id="2012">2012</option>
                                                                                                                                                <option value="2011" id="2011">2011</option>
                                                                                                                                                <option value="2010" id="2010">2010</option>
                                                                                                                                                <option value="2009" id="2009">2009</option>
                                                                                                                                                <option value="2008" id="2008">2008</option>
                                                                                                                                                <option value="2007" id="2007">2007</option>
                                                                                                                                                <option value="2006" id="2006">2006</option>
                                                                                                                                                <option value="2005" id="2005">2005</option>
                                                                                                                                                <option value="2004" id="2004">2004</option>
                                                                                                                                                <option value="2003" id="2003">2003</option>
                                                                                                                                                <option value="2002" id="2002">2002</option>
                                                                                                                                                <option value="2001" id="2001">2001</option>
                                                                                                                                                <option value="2000" id="2000">2000</option>
                                                                                                                                                <option value="1999" id="1999">1999</option>
                                                                                                                                                <option value="1998" id="1998">1998</option>
                                                                                                                                                <option value="1997" id="1997">1997</option>
                                                                                                                                                <option value="1996" id="1996">1996</option>
                                                                                                                                                <option value="1995" id="1995">1995</option>
                                                                                                                                                <option value="1994" id="1994">1994</option>
                                                                                                                                                <option value="1993" id="1993">1993</option>
                                                                                                                                                <option value="1992" id="1992">1992</option>
                                                                                                                                                <option value="1991" id="1991">1991</option>
                                                                                                                                                <option value="1990" id="1990">1990</option>
                                                                                                                                                <option value="1989" id="1989">1989</option>
                                                                                                                                                <option value="1988" id="1988">1988</option>
                                                                                                                                                <option value="1987" id="1987">1987</option>
                                                                                                                                                <option value="1986" id="1986">1986</option>
                                                                                                                                                <option value="1985" id="1985">1985</option>
                                                                                                                                                <option value="1984" id="1984">1984</option>
                                                                                                                                                <option value="1983" id="1983">1983</option>
                                                                                                                                                <option value="1982" id="1982">1982</option>
                                                                                                                                                <option value="1981" id="1981">1981</option>
                                                                                                                                                <option value="1980" id="1980">1980</option>
                                                                                                                                                <option value="1979" id="1979">1979</option>
                                                                                                                                                <option value="1978" id="1978">1978</option>
                                                                                                                                                <option value="1977" id="1977">1977</option>
                                                                                                                                                <option value="1976" id="1976">1976</option>
                                                                                                                                                <option value="1975" id="1975">1975</option>
                                                                                                                                            </select>
                                                                    <select class="styled-select slate" onchange="convert_into_english_issue('1')"  name="doi_bs_month[]" id="doi_bs_month1" style="width:75px"
                                                                    >
                                                                    <option value="">Month</option>
                                                                                                                                            <option value="12">12</option>
                                                                                                                                                <option value="11">11</option>
                                                                                                                                                <option value="10">10</option>
                                                                                                                                                <option value="9">9</option>
                                                                                                                                                <option value="8">8</option>
                                                                                                                                                <option value="7">7</option>
                                                                                                                                                <option value="6">6</option>
                                                                                                                                                <option value="5">5</option>
                                                                                                                                                <option value="4">4</option>
                                                                                                                                                <option value="3">3</option>
                                                                                                                                                <option value="2">2</option>
                                                                                                                                                <option value="1">1</option>
                                                                                                                                            </select>
                                                                    <select  class="styled-select slate" onchange="convert_into_english_issue('1')"  name="doi_bs_day[]" id="doi_bs_day1" style="width:62px"
                                                                    >
                                                                    <option value="">Day</option>
                                                                                                                                            <option value="32">32</option>
                                                                                                                                                <option value="31">31</option>
                                                                                                                                                <option value="30">30</option>
                                                                                                                                                <option value="29">29</option>
                                                                                                                                                <option value="28">28</option>
                                                                                                                                                <option value="27">27</option>
                                                                                                                                                <option value="26">26</option>
                                                                                                                                                <option value="25">25</option>
                                                                                                                                                <option value="24">24</option>
                                                                                                                                                <option value="23">23</option>
                                                                                                                                                <option value="22">22</option>
                                                                                                                                                <option value="21">21</option>
                                                                                                                                                <option value="20">20</option>
                                                                                                                                                <option value="19">19</option>
                                                                                                                                                <option value="18">18</option>
                                                                                                                                                <option value="17">17</option>
                                                                                                                                                <option value="16">16</option>
                                                                                                                                                <option value="15">15</option>
                                                                                                                                                <option value="14">14</option>
                                                                                                                                                <option value="13">13</option>
                                                                                                                                                <option value="12">12</option>
                                                                                                                                                <option value="11">11</option>
                                                                                                                                                <option value="10">10</option>
                                                                                                                                                <option value="9">9</option>
                                                                                                                                                <option value="8">8</option>
                                                                                                                                                <option value="7">7</option>
                                                                                                                                                <option value="6">6</option>
                                                                                                                                                <option value="5">5</option>
                                                                                                                                                <option value="4">4</option>
                                                                                                                                                <option value="3">3</option>
                                                                                                                                                <option value="2">2</option>
                                                                                                                                                <option value="1">1</option>
                                                                                                                                            </select>
                                                                    <!-- <div class="btn btn-success pull-right" onclick="convert_into_english('1')">Convert into AD</div> -->
                                                                </div>
                                                            </div>
                                                        </div>


                                                   
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <div class="col-sm-4 no-padding">
                                                                <label class="control-label" for="email">Email *</label>
                                                            </div>
                                                            <div class="col-sm-8 no-padding">
                                                                <input  maxlength="100" type="email"   name="email[]" id="email1" class="form-control" required="" placeholder="Email" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <div class="col-sm-4 no-padding">
                                                                <label class="control-label" id = "date_of_issue_ad1" for="date_of_issue_ad1">Date of Issue(AD)*</label>
                                                            </div>
                                                            <div class="col-sm-8 no-padding">
                                                                <select onchange="convert_into_nepali_issue('1')" class="styled-select slate" name="doi_year[]" id="doi_year1" style="width:67px;" >
                                                                    <option value=""  >Year</option>
                                                                                                                                            <option value="2019" >2019</option>
                                                                                                                                                <option value="2018" >2018</option>
                                                                                                                                                <option value="2017" >2017</option>
                                                                                                                                                <option value="2016" >2016</option>
                                                                                                                                                <option value="2015" >2015</option>
                                                                                                                                                <option value="2014" >2014</option>
                                                                                                                                                <option value="2013" >2013</option>
                                                                                                                                                <option value="2012" >2012</option>
                                                                                                                                                <option value="2011" >2011</option>
                                                                                                                                                <option value="2010" >2010</option>
                                                                                                                                                <option value="2009" >2009</option>
                                                                                                                                                <option value="2008" >2008</option>
                                                                                                                                                <option value="2007" >2007</option>
                                                                                                                                                <option value="2006" >2006</option>
                                                                                                                                                <option value="2005" >2005</option>
                                                                                                                                                <option value="2004" >2004</option>
                                                                                                                                                <option value="2003" >2003</option>
                                                                                                                                                <option value="2002" >2002</option>
                                                                                                                                                <option value="2001" >2001</option>
                                                                                                                                                <option value="2000" >2000</option>
                                                                                                                                                <option value="1999" >1999</option>
                                                                                                                                                <option value="1998" >1998</option>
                                                                                                                                                <option value="1997" >1997</option>
                                                                                                                                                <option value="1996" >1996</option>
                                                                                                                                                <option value="1995" >1995</option>
                                                                                                                                                <option value="1994" >1994</option>
                                                                                                                                                <option value="1993" >1993</option>
                                                                                                                                                <option value="1992" >1992</option>
                                                                                                                                                <option value="1991" >1991</option>
                                                                                                                                                <option value="1990" >1990</option>
                                                                                                                                                <option value="1989" >1989</option>
                                                                                                                                                <option value="1988" >1988</option>
                                                                                                                                                <option value="1987" >1987</option>
                                                                                                                                                <option value="1986" >1986</option>
                                                                                                                                                <option value="1985" >1985</option>
                                                                                                                                                <option value="1984" >1984</option>
                                                                                                                                                <option value="1983" >1983</option>
                                                                                                                                                <option value="1982" >1982</option>
                                                                                                                                                <option value="1981" >1981</option>
                                                                                                                                                <option value="1980" >1980</option>
                                                                                                                                                <option value="1979" >1979</option>
                                                                                                                                                <option value="1978" >1978</option>
                                                                                                                                                <option value="1977" >1977</option>
                                                                                                                                                <option value="1976" >1976</option>
                                                                                                                                                <option value="1975" >1975</option>
                                                                                                                                                <option value="1974" >1974</option>
                                                                                                                                                <option value="1973" >1973</option>
                                                                                                                                                <option value="1972" >1972</option>
                                                                                                                                                <option value="1971" >1971</option>
                                                                                                                                                <option value="1970" >1970</option>
                                                                                                                                                <option value="1969" >1969</option>
                                                                                                                                                <option value="1968" >1968</option>
                                                                                                                                                <option value="1967" >1967</option>
                                                                                                                                                <option value="1966" >1966</option>
                                                                                                                                                <option value="1965" >1965</option>
                                                                                                                                                <option value="1964" >1964</option>
                                                                                                                                                <option value="1963" >1963</option>
                                                                                                                                                <option value="1962" >1962</option>
                                                                                                                                                <option value="1961" >1961</option>
                                                                                                                                                <option value="1960" >1960</option>
                                                                                                                                                <option value="1959" >1959</option>
                                                                                                                                                <option value="1958" >1958</option>
                                                                                                                                                <option value="1957" >1957</option>
                                                                                                                                                <option value="1956" >1956</option>
                                                                                                                                                <option value="1955" >1955</option>
                                                                                                                                                <option value="1954" >1954</option>
                                                                                                                                                <option value="1953" >1953</option>
                                                                                                                                                <option value="1952" >1952</option>
                                                                                                                                                <option value="1951" >1951</option>
                                                                                                                                                <option value="1950" >1950</option>
                                                                                                                                                <option value="1949" >1949</option>
                                                                                                                                                <option value="1948" >1948</option>
                                                                                                                                                <option value="1947" >1947</option>
                                                                                                                                                <option value="1946" >1946</option>
                                                                                                                                                <option value="1945" >1945</option>
                                                                                                                                                <option value="1944" >1944</option>
                                                                                                                                                <option value="1943" >1943</option>
                                                                                                                                                <option value="1942" >1942</option>
                                                                                                                                                <option value="1941" >1941</option>
                                                                                                                                                <option value="1940" >1940</option>
                                                                                                                                                <option value="1939" >1939</option>
                                                                                                                                                <option value="1938" >1938</option>
                                                                                                                                                <option value="1937" >1937</option>
                                                                                                                                                <option value="1936" >1936</option>
                                                                                                                                                <option value="1935" >1935</option>
                                                                                                                                                <option value="1934" >1934</option>
                                                                                                                                                <option value="1933" >1933</option>
                                                                                                                                                <option value="1932" >1932</option>
                                                                                                                                                <option value="1931" >1931</option>
                                                                                                                                                <option value="1930" >1930</option>
                                                                                                                                                <option value="1929" >1929</option>
                                                                                                                                                <option value="1928" >1928</option>
                                                                                                                                                <option value="1927" >1927</option>
                                                                                                                                                <option value="1926" >1926</option>
                                                                                                                                                <option value="1925" >1925</option>
                                                                                                                                                <option value="1924" >1924</option>
                                                                                                                                                <option value="1923" >1923</option>
                                                                                                                                                <option value="1922" >1922</option>
                                                                                                                                                <option value="1921" >1921</option>
                                                                                                                                                <option value="1920" >1920</option>
                                                                                                                                                <option value="1919" >1919</option>
                                                                                                                                                <option value="1918" >1918</option>
                                                                                                                                            </select>
                                                                    <select  onchange="convert_into_nepali_issue('1')" class="styled-select slate"  name="doi_month[]" id="doi_month1" style="width:75px"
                                                                    >
                                                                    <option value="">Month</option>
                                                                                                                                            <option value="12">12</option>
                                                                                                                                                <option value="11">11</option>
                                                                                                                                                <option value="10">10</option>
                                                                                                                                                <option value="9">9</option>
                                                                                                                                                <option value="8">8</option>
                                                                                                                                                <option value="7">7</option>
                                                                                                                                                <option value="6">6</option>
                                                                                                                                                <option value="5">5</option>
                                                                                                                                                <option value="4">4</option>
                                                                                                                                                <option value="3">3</option>
                                                                                                                                                <option value="2">2</option>
                                                                                                                                                <option value="1">1</option>
                                                                                                                                            </select>
                                                                    <select onchange="convert_into_nepali_issue('1')" class="styled-select slate" name="doi_day[]" id="doi_day1" style="width:62px"
                                                                    >
                                                                    <option value="">Day</option>
                                                                                                                                            <option value="32">32</option>
                                                                                                                                                <option value="31">31</option>
                                                                                                                                                <option value="30">30</option>
                                                                                                                                                <option value="29">29</option>
                                                                                                                                                <option value="28">28</option>
                                                                                                                                                <option value="27">27</option>
                                                                                                                                                <option value="26">26</option>
                                                                                                                                                <option value="25">25</option>
                                                                                                                                                <option value="24">24</option>
                                                                                                                                                <option value="23">23</option>
                                                                                                                                                <option value="22">22</option>
                                                                                                                                                <option value="21">21</option>
                                                                                                                                                <option value="20">20</option>
                                                                                                                                                <option value="19">19</option>
                                                                                                                                                <option value="18">18</option>
                                                                                                                                                <option value="17">17</option>
                                                                                                                                                <option value="16">16</option>
                                                                                                                                                <option value="15">15</option>
                                                                                                                                                <option value="14">14</option>
                                                                                                                                                <option value="13">13</option>
                                                                                                                                                <option value="12">12</option>
                                                                                                                                                <option value="11">11</option>
                                                                                                                                                <option value="10">10</option>
                                                                                                                                                <option value="9">9</option>
                                                                                                                                                <option value="8">8</option>
                                                                                                                                                <option value="7">7</option>
                                                                                                                                                <option value="6">6</option>
                                                                                                                                                <option value="5">5</option>
                                                                                                                                                <option value="4">4</option>
                                                                                                                                                <option value="3">3</option>
                                                                                                                                                <option value="2">2</option>
                                                                                                                                                <option value="1">1</option>
                                                                                                                                            </select>
                                                                <!-- <div class="btn btn-success pull-right" onclick="convert_into_nepali('1')">Convert into BS</div>
                                                                -->  
                                                            </div>
                                                        </div>
                                                    </div>



                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <div class="col-sm-4 no-padding">
                                                                    <label class="control-label"for="pan_number">PAN Number </label>
                                                                </div>
                                                                <div class="col-sm-8 no-padding">
                                                                    <input  maxlength="100" type="text"   name="pan_number[]" id="pan_number1" class="form-control" placeholder="Pan Number"  />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <div class="col-sm-4 no-padding">
                                                                    <label class="control-label" id="marital_status_1" for="marital_status">Marital Status *</label>
                                                                </div>
                                                                <div class="col-sm-8 no-padding radio-margin-bottom">
                                                                    <div class="radio-contianer">
                                                                        <div class="radio-option">
                                                                            <input type="radio" name="marital_status[0]" class="marital_status1" id="unmarried1" value="Unmarried" />Unmarried
                                                                        </div>
                                                                        <div class="radio-option">
                                                                            <input type="radio"  name="marital_status[0]" class="marital_status1" value="Married" />Married
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row all-round-border">

                                                <div class="col-md-12">
                                                    <div class="step_header">
                                                        <h4> Permanent Address</h4>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-4 no-padding">
                                                            <label class="control-label"for="pa_house_number">House Number </label>
                                                        </div>
                                                        <div class="col-sm-8 no-padding" >
                                                            <input  maxlength="100" type="text"  name="pa_house_number[]" id="pa_house_number1" class="form-control" placeholder="House Number"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-4 no-padding">
                                                            <label class="control-label" for="pa_municipality_vdc"> Municipality / VDC *</label>
                                                        </div>
                                                        <div class="col-sm-8 no-padding" >
                                                            <input  maxlength="100" type="text"  name="pa_municipality_vdc[]" id="pa_municipality_vdc1" class="form-control" required="" placeholder=" Municipality / VDC" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-4 no-padding">
                                                            <label class="control-label" for="pa_ward_number"> Ward Number *</label>
                                                        </div>
                                                        <div class="col-sm-8 no-padding" >
                                                            <input  maxlength="100" type="text"  name="pa_ward_number[]" onkeypress='return ((event.charCode >= 46 && event.charCode <= 57) || event.charCode == 0)' id="pa_ward_number1" class="form-control" required="" placeholder=" Ward Number" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-4 no-padding">
                                                            <label class="control-label" for="pa_street"> Street </label>
                                                        </div>
                                                        <div class="col-sm-8 no-padding" >
                                                            <input  maxlength="100" type="text"  name="pa_street[]" id="pa_street1" class="form-control" required="" placeholder="Street"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-4 no-padding">
                                                            <label class="control-label" for="pa_district"> District *</label>
                                                        </div>
                                                        <div class="col-sm-8 no-padding" >
                                                            <select id="pa_district1" name="pa_district[]" class="styled-select slate">
                                                                <option value="" selected="">Select</option>
                                                                                                                                    <option value="Achham">Achham</option>
                                                                                                                                    <option value="Arghakhanchi">Arghakhanchi</option>
                                                                                                                                    <option value="Baglung">Baglung</option>
                                                                                                                                    <option value="Baitadi">Baitadi</option>
                                                                                                                                    <option value="Bajhang">Bajhang</option>
                                                                                                                                    <option value="Bajura">Bajura</option>
                                                                                                                                    <option value="Banke">Banke</option>
                                                                                                                                    <option value="Bara">Bara</option>
                                                                                                                                    <option value="Bardiya">Bardiya</option>
                                                                                                                                    <option value="Bhaktapur">Bhaktapur</option>
                                                                                                                                    <option value="Bhojpur">Bhojpur</option>
                                                                                                                                    <option value="Chitwan">Chitwan</option>
                                                                                                                                    <option value="Dadeldhura">Dadeldhura</option>
                                                                                                                                    <option value="Dailekh">Dailekh</option>
                                                                                                                                    <option value="Dang Deukhuri">Dang Deukhuri</option>
                                                                                                                                    <option value="Darchula">Darchula</option>
                                                                                                                                    <option value="Dhading">Dhading</option>
                                                                                                                                    <option value="Dhankuta">Dhankuta</option>
                                                                                                                                    <option value="Dhanusa">Dhanusa</option>
                                                                                                                                    <option value="Dholkha">Dholkha</option>
                                                                                                                                    <option value="Dolpa">Dolpa</option>
                                                                                                                                    <option value="Doti">Doti</option>
                                                                                                                                    <option value="Gorkha">Gorkha</option>
                                                                                                                                    <option value="Gulmi">Gulmi</option>
                                                                                                                                    <option value="Humla">Humla</option>
                                                                                                                                    <option value="Ilam">Ilam</option>
                                                                                                                                    <option value="Jajarkot">Jajarkot</option>
                                                                                                                                    <option value="Jhapa">Jhapa</option>
                                                                                                                                    <option value="Jumla">Jumla</option>
                                                                                                                                    <option value="Kailali">Kailali</option>
                                                                                                                                    <option value="Kalikot">Kalikot</option>
                                                                                                                                    <option value="Kanchanpur">Kanchanpur</option>
                                                                                                                                    <option value="Kapilvastu">Kapilvastu</option>
                                                                                                                                    <option value="Kaski">Kaski</option>
                                                                                                                                    <option value="Kathmandu">Kathmandu</option>
                                                                                                                                    <option value="Kavrepalanchok">Kavrepalanchok</option>
                                                                                                                                    <option value="Khotang">Khotang</option>
                                                                                                                                    <option value="Lalitpur">Lalitpur</option>
                                                                                                                                    <option value="Lamjung">Lamjung</option>
                                                                                                                                    <option value="Mahottari">Mahottari</option>
                                                                                                                                    <option value="Makwanpur">Makwanpur</option>
                                                                                                                                    <option value="Manang">Manang</option>
                                                                                                                                    <option value="Morang">Morang</option>
                                                                                                                                    <option value="Mugu">Mugu</option>
                                                                                                                                    <option value="Mustang">Mustang</option>
                                                                                                                                    <option value="Myagdi">Myagdi</option>
                                                                                                                                    <option value="Nawalparasi">Nawalparasi</option>
                                                                                                                                    <option value="Nuwakot">Nuwakot</option>
                                                                                                                                    <option value="Okhaldhunga">Okhaldhunga</option>
                                                                                                                                    <option value="Palpa">Palpa</option>
                                                                                                                                    <option value="Panchthar">Panchthar</option>
                                                                                                                                    <option value="Parbat">Parbat</option>
                                                                                                                                    <option value="Parsa">Parsa</option>
                                                                                                                                    <option value="Pyuthan">Pyuthan</option>
                                                                                                                                    <option value="Ramechhap">Ramechhap</option>
                                                                                                                                    <option value="Rasuwa">Rasuwa</option>
                                                                                                                                    <option value="Rautahat">Rautahat</option>
                                                                                                                                    <option value="Rolpa">Rolpa</option>
                                                                                                                                    <option value="Rukum">Rukum</option>
                                                                                                                                    <option value="Rupandehi">Rupandehi</option>
                                                                                                                                    <option value="Salyan">Salyan</option>
                                                                                                                                    <option value="Sankhuwasabha">Sankhuwasabha</option>
                                                                                                                                    <option value="Saptari">Saptari</option>
                                                                                                                                    <option value="Sarlahi">Sarlahi</option>
                                                                                                                                    <option value="Sindhuli">Sindhuli</option>
                                                                                                                                    <option value="Sindhupalchok">Sindhupalchok</option>
                                                                                                                                    <option value="Siraha">Siraha</option>
                                                                                                                                    <option value="Solukhumbu">Solukhumbu</option>
                                                                                                                                    <option value="Sunsari">Sunsari</option>
                                                                                                                                    <option value="Surkhet">Surkhet</option>
                                                                                                                                    <option value="Syangja">Syangja</option>
                                                                                                                                    <option value="Tanahu">Tanahu</option>
                                                                                                                                    <option value="Taplejung">Taplejung</option>
                                                                                                                                    <option value="Terhathum">Terhathum</option>
                                                                                                                                    <option value="Udayapur">Udayapur</option>
                                                                                                                            </select>
                                                        </div>   
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row all-round-border">
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-4 no-padding">
                                                            <label class="control-label" id="phone_number_1" for="phone_number">Phone Number</label>
                                                        </div>
                                                        <div class="col-sm-8 no-padding" id="number">
                                                            <input  maxlength="9" type="text" onkeypress='return ((event.charCode >= 48 && event.charCode <= 57) || event.charCode == 0)' name="phone_number[]" id="phone_number1" class="form-control" placeholder="Phone Number" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-4 no-padding">
                                                            <label class="control-label" for="mobile_no"> Mobile Number *</label>
                                                        </div>
                                                        <div class="col-sm-8 no-padding" id="number-pre-sec">
                                                            <input  maxlength="10" minlength="0" onkeypress='return ((event.charCode >= 46 && event.charCode <= 57) || event.charCode == 0) ' type="text" name="mobile_no[]" id="mobile_no1" class="form-control" required placeholder=" Mobile Number" />
                                                        </div>
                                                    </div>

                                                    
                                                </div>


                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-4 no-padding">
                                                            <label class="control-label" for="fax_number">Telex/Fax Number</label>
                                                        </div>
                                                        <div class="col-sm-8 no-padding">
                                                            <input  maxlength="100" type="text" name="fax_number[]" id="fax_number1" class="form-control" placeholder="Telex/Fax Number" >
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-4 no-padding">
                                                            <label class="control-label" for="pox_box"> Pox Box No.</label>
                                                        </div>
                                                        <div class="col-sm-8 no-padding">
                                                            <input  maxlength="100" type="text" name="pox_box[]" id="pox_box1" class="form-control" placeholder="Pox Box No">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row all-round-border">

                                                <div class="col-md-12">
                                                    <div class="col-md-6 col-sm-6 no-padding">
                                                        <div class="step_header">
                                                            <h4> Communication Address</h4>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <div class="no-padding">
                                                                <div class="radio-contianer">
                                                                    <div class="radio-option">
                                                                        <input type="checkbox" id="copy_address1" onclick="copy_permanent_address()" /><strong>Communication Address same as Permanent Address</strong>
                                                                    </div>
                                                                </div>
                                                                <!--<label class="control-label"><input type="button" id="copy_address" class="btn btn-" > Copy as Permanent Address</label>-->
                                                                <!-- <button id="copy_address1" class="ctz-btn ctz-blue" onclick="copy_permanent_address()" type="button"  >Copy Permanent Address</button> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-4 no-padding">
                                                            <label class="control-label" for="ca_house_number">House Number </label>
                                                        </div>
                                                        <div class="col-sm-8 no-padding" >
                                                            <input  maxlength="100" type="text" name="ca_house_number[]" id="ca_house_number1" class="form-control"placeholder="House Number" />
                                                        </div>
                                                    </div>
                                                </div>



                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-4 no-padding">
                                                            <label class="control-label" for="ca_municipality_vdc"> Municipality / VDC </label>
                                                        </div>
                                                        <div class="col-sm-8 no-padding" >
                                                            <input  maxlength="100" type="text" name="ca_municipality_vdc[]" id="ca_municipality_vdc1" class="form-control" placeholder="Municipality / VDC"/>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-4 no-padding">
                                                            <label class="control-label"for="ca_ward_number"> Ward Number </label>
                                                        </div>
                                                        <div class="col-sm-8 no-padding" >
                                                            <input  maxlength="100" type="text" onkeypress='return ((event.charCode >= 46 && event.charCode <= 57) || event.charCode == 0) ' name="ca_ward_number[]" id="ca_ward_number1" class="form-control" placeholder="Ward Number"/>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-4 no-padding">
                                                            <label class="control-label" for="ca_street"> Street </label>
                                                        </div>
                                                        <div class="col-sm-8 no-padding" >
                                                            <input  maxlength="100" type="text" name="ca_street[]" id="ca_street1" class="form-control" placeholder="Street"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-4 no-padding">
                                                            <label class="control-label" for="ca_district"> District </label>
                                                        </div>
                                                        <div class="col-sm-8 no-padding" >
                                                            <!-- <input  maxlength="100" type="text" name="ca_district[]" id="ca_district1" class="form-control" placeholder="District"  /> -->
                                                            <select id="ca_district1" name="ca_district[]" class="styled-select slate">
                                                                <option value="" selected="">Select</option>
                                                                                                                                    <option value="Achham">Achham</option>
                                                                                                                                    <option value="Arghakhanchi">Arghakhanchi</option>
                                                                                                                                    <option value="Baglung">Baglung</option>
                                                                                                                                    <option value="Baitadi">Baitadi</option>
                                                                                                                                    <option value="Bajhang">Bajhang</option>
                                                                                                                                    <option value="Bajura">Bajura</option>
                                                                                                                                    <option value="Banke">Banke</option>
                                                                                                                                    <option value="Bara">Bara</option>
                                                                                                                                    <option value="Bardiya">Bardiya</option>
                                                                                                                                    <option value="Bhaktapur">Bhaktapur</option>
                                                                                                                                    <option value="Bhojpur">Bhojpur</option>
                                                                                                                                    <option value="Chitwan">Chitwan</option>
                                                                                                                                    <option value="Dadeldhura">Dadeldhura</option>
                                                                                                                                    <option value="Dailekh">Dailekh</option>
                                                                                                                                    <option value="Dang Deukhuri">Dang Deukhuri</option>
                                                                                                                                    <option value="Darchula">Darchula</option>
                                                                                                                                    <option value="Dhading">Dhading</option>
                                                                                                                                    <option value="Dhankuta">Dhankuta</option>
                                                                                                                                    <option value="Dhanusa">Dhanusa</option>
                                                                                                                                    <option value="Dholkha">Dholkha</option>
                                                                                                                                    <option value="Dolpa">Dolpa</option>
                                                                                                                                    <option value="Doti">Doti</option>
                                                                                                                                    <option value="Gorkha">Gorkha</option>
                                                                                                                                    <option value="Gulmi">Gulmi</option>
                                                                                                                                    <option value="Humla">Humla</option>
                                                                                                                                    <option value="Ilam">Ilam</option>
                                                                                                                                    <option value="Jajarkot">Jajarkot</option>
                                                                                                                                    <option value="Jhapa">Jhapa</option>
                                                                                                                                    <option value="Jumla">Jumla</option>
                                                                                                                                    <option value="Kailali">Kailali</option>
                                                                                                                                    <option value="Kalikot">Kalikot</option>
                                                                                                                                    <option value="Kanchanpur">Kanchanpur</option>
                                                                                                                                    <option value="Kapilvastu">Kapilvastu</option>
                                                                                                                                    <option value="Kaski">Kaski</option>
                                                                                                                                    <option value="Kathmandu">Kathmandu</option>
                                                                                                                                    <option value="Kavrepalanchok">Kavrepalanchok</option>
                                                                                                                                    <option value="Khotang">Khotang</option>
                                                                                                                                    <option value="Lalitpur">Lalitpur</option>
                                                                                                                                    <option value="Lamjung">Lamjung</option>
                                                                                                                                    <option value="Mahottari">Mahottari</option>
                                                                                                                                    <option value="Makwanpur">Makwanpur</option>
                                                                                                                                    <option value="Manang">Manang</option>
                                                                                                                                    <option value="Morang">Morang</option>
                                                                                                                                    <option value="Mugu">Mugu</option>
                                                                                                                                    <option value="Mustang">Mustang</option>
                                                                                                                                    <option value="Myagdi">Myagdi</option>
                                                                                                                                    <option value="Nawalparasi">Nawalparasi</option>
                                                                                                                                    <option value="Nuwakot">Nuwakot</option>
                                                                                                                                    <option value="Okhaldhunga">Okhaldhunga</option>
                                                                                                                                    <option value="Palpa">Palpa</option>
                                                                                                                                    <option value="Panchthar">Panchthar</option>
                                                                                                                                    <option value="Parbat">Parbat</option>
                                                                                                                                    <option value="Parsa">Parsa</option>
                                                                                                                                    <option value="Pyuthan">Pyuthan</option>
                                                                                                                                    <option value="Ramechhap">Ramechhap</option>
                                                                                                                                    <option value="Rasuwa">Rasuwa</option>
                                                                                                                                    <option value="Rautahat">Rautahat</option>
                                                                                                                                    <option value="Rolpa">Rolpa</option>
                                                                                                                                    <option value="Rukum">Rukum</option>
                                                                                                                                    <option value="Rupandehi">Rupandehi</option>
                                                                                                                                    <option value="Salyan">Salyan</option>
                                                                                                                                    <option value="Sankhuwasabha">Sankhuwasabha</option>
                                                                                                                                    <option value="Saptari">Saptari</option>
                                                                                                                                    <option value="Sarlahi">Sarlahi</option>
                                                                                                                                    <option value="Sindhuli">Sindhuli</option>
                                                                                                                                    <option value="Sindhupalchok">Sindhupalchok</option>
                                                                                                                                    <option value="Siraha">Siraha</option>
                                                                                                                                    <option value="Solukhumbu">Solukhumbu</option>
                                                                                                                                    <option value="Sunsari">Sunsari</option>
                                                                                                                                    <option value="Surkhet">Surkhet</option>
                                                                                                                                    <option value="Syangja">Syangja</option>
                                                                                                                                    <option value="Tanahu">Tanahu</option>
                                                                                                                                    <option value="Taplejung">Taplejung</option>
                                                                                                                                    <option value="Terhathum">Terhathum</option>
                                                                                                                                    <option value="Udayapur">Udayapur</option>
                                                                                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>




                                                <input class = "hidden" type="text" id="account_status" value="Pending" name="account_status" />
                                            </div>
                                        </div><!--end account1-->

                                                <div class="added_account_container" id="added_account_container">
        
                                                </div>
                                    </div><!--end account_all-->
                                    
                                    <div class="row all-round-border check_options">
                                        <div class="col-md-12">
                                            <div class="steps-sub-title" id="citizenship_front_1">
                                                <h4>Identity Uploads</h4>
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div id="citizenship_uploads" class="repeat-holder-item-container">
                                                        <div class="col-sm-12 repeat-holder-item">
                                                            <div id = "citizenship_or_passport1">

                                                            </div>
                                                            <!--<label class="if_add_more" >Holder 1</label>-->
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <label for="citizenship_front0">Front: </label>
                                                                    <input type="file" onchange="validate_citizenship_front(1)" name="citizenship_front0" id="citizenship_front1" size="20" />
                                                                        <!-- <label>Back:</label>
                                                                            <input type="file" onchange="validate_citizenship_back(1)" name="citizenship_back0" id="citizenship_back1" size="20" /> -->
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <label for="citizenship_back0">Back:</label>
                                                                            <input type="file" onchange="validate_citizenship_back(1)" name="citizenship_back0" id="citizenship_back1" size="20" />
                                                                        </div>

                                                                        

                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="note">
                                                        <p>(Allowed formats: GIF,JPG,PNG,JPEG,PDF)</p>
                                                    </div>
                                                </div>


                                            </div>



                                                <div class="row all-round-border check_options">
                                                    <div class="col-md-12">
                                                        <div class="steps-sub-title" id="image_1">
                                                            <h4>Photo Uploads</h4>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div id="photo_uploads" class="repeat-holder-item-container all-uploads">
                                                                    <div class="col-md-4 col-sm-6 repeat-holder-item">
                                                                        <label id="labelPhotoUpload1" for="image0"></label>
                                                                        <input type="file" onchange="(1)" name="image0" id="image1" size="20" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="note">
                                                            <p>(Allowed formats: GIF,JPG,PNG,JPEG)</p>
                                                        </div>
                                                    </div>
                                                </div>

                                    <div class="citizen-add-more-btn-container middle_section">
                                       
                                    </div>

                                   

                              

                                            <div class="step-next-container">
                                                <div class="left-container">
                                                    <button onclick="save_and_continue_later(1)"  class="ctz-btn" type="button" >Save & Continue Later</button>
                                                </div>
                                                <div class="right-container">
                                                    <button id="next1" onclick = "getAddress()" class="ctz-btn ctz-blue nextBtn nextbutton1" type="button">Next</button>
                                                </div>
                                            </div>

                                        </div>


                                        <div class="row setup-content" id="step-2">
                                            <div class="col-xs-12">
                                                <div class="steps-title">
                                                    <h3> Depositer's Nomination</h3>
                                                </div>

                                                <input class ="hidden" id="address" type="text" placeholder="Enter address here" />
                                                <input class = "hidden" type="text" id="latitude_address" readonly />
                                                <input class = "hidden" type="text" id="longitude_address" readonly />
                                                <script>
                                                    function showResult(result) {
                                                        document.getElementById('latitude_address').value = result.geometry.location.lat();
                                                        document.getElementById('longitude_address').value = result.geometry.location.lng();
                                                    }

                                                    function getLatitudeLongitude(callback, address) {
                                            // If adress is not supplied, use default value 'Ferrol, Galicia, Spain'
                                            address = address;
                                            // Initialize the Geocoder
                                            geocoder = new google.maps.Geocoder();
                                            if (geocoder) {
                                                geocoder.geocode({
                                                    'address': address
                                                }, function (results, status) {
                                                    if (status == google.maps.GeocoderStatus.OK) {
                                                        callback(results[0]);
                                                    }
                                                });
                                            }
                                        }

                                        var button = document.getElementById('next1');

                                        button.addEventListener("click", function () {
                                            var address = document.getElementById('address').value;
                                            getLatitudeLongitude(showResult, address)
                                        });
                                    </script>

                                    <div class="step-content">
                                           <div class="col-xs-12">
                                                        <div class="citizen-add-more-btn-container">
                                                            <div class="add-more-inner addmore_account_btn" style="float: right;">
                                                                <div class="ctz-btn ctz-blue" id="add_nominee_btn" onclick="add_depositer()">Add More</div>
                                                                <div class="ctz-btn ctz-red" id="remove_nominee_btn" onclick="remove_depositer()">Remove</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                        <h4> Nominee 1</h4>
                                        <div class="row all-round-border check_options ">
                                            <div class=holder-section>
                                                <div class="col-sm-2">
                                                    <div class="col-sm-6 no-padding">
                                                        <label for="mr_miss">Initials</label>
                                                    </div>
                                                    <div class="col-sm-6 no-padding">
                                                        <select  class="styled-select slate" id="mr_miss1" name="mr_miss[]">
                                                            <option value="">Select</option>
                                                            <option value="Mr">Mr.</option>
                                                            <option value="Ms">Ms.</option>
                                                            <option value="Mrs">Mrs.</option>
                                                            <option value="others">others</option>
                                                        </select>
                                                            <!-- <div class="row radio-contianer">
                                                                <div class="col-md-6 radio-option">
                                                                    <input type="radio"  name="mr[0]" value="Mr." />MR.
                                                                </div>
                                                                <div class="col-md-6 radio-option">
                                                                    <input type="radio"  name="mr[0]" value="Mrs." />Mrs.
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <div class="form-group">
                                                            <div class="form-group">
                                                                <div class="col-sm-2 no-padding">
                                                                    <label class="control-label" for="depositer_name">Name</label>
                                                                </div>
                                                                <div class="col-sm-10 no-padding">
                                                                    <input maxlength="100" type="text" name="depositer_name[]" id="depositer_name1" class="form-control" placeholder="Name" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <div class="form-group">
                                                            <div class="col-sm-4 no-padding">
                                                                <label class="control-label" for="relationship">Relationship</label><br/>
                                                            </div>
                                                            <div class="col-sm-8 no-padding">
                                                                <select  class="styled-select slate"  name="relationship[]" id="relationship1" class="col-sm-4">
                                                                    <option value="">Select</option>
                                                                    <option value="Father">Father</option>
                                                                    <option value="Mother">Mother</option>
                                                                    <option value="Spouse">Spouse</option>
                                                                    <option value="Son">Son</option>
                                                                    <option value="Daughter">Daughter</option>
                                                                    <option value="Others">Others</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>       
                                            
                                               
                                            <div class="row all-round-border check_options">
                                                <div class="col-md-12">
                                                    <div class="steps-sub-title">
                                                        <h4>Nominee Photo Uploads</h4>
                                                    </div>
                                                </div>
                                                
                                               

                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div id="nominee_photo_uploads" class="repeat-holder-item-container all-uploads ">
                                                                <div class="col-md-4 col-sm-6 repeat-holder-item">
                                                                    <label class="if_add_more" style="display:none" for="nominee_image0">Nominee 1</label>
                                                                    <input type="file" onchange="(1)" name="nominee_image0" id="nominee_image1" size="20" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="note">
                                                        <p>(Allowed formats: GIF,JPG,PNG,JPEG)</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row all-round-border check_options">
                                                <div class="col-md-12">
                                                    <div class="steps-sub-title">
                                                        <h4>Nominee Citizenship Uploads</h4>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div id="nominee_citizenship_uploads" class="repeat-holder-item-container all-uploads">
                                                                <div class="col-sm-12 repeat-holder-item">
                                                                    <label class="if_add_more" for="nominee_citizenship_front0">Nominee 1</label>
                                                                    <div class="row">
                                                                        <div class="col-md-4">
                                                                            <label>Front: </label>
                                                                            <input type="file" onchange="validate_nominee_citizenship_front(1)" name="nominee_citizenship_front0" id="nominee_citizenship_front1" size="20" />
                                                                            <!-- <label>Back:</label>
                                                                                <input type="file" onchange="validate_nominee_citizenship_back(1)" name="nominee_citizenship_back0" id="nominee_citizenship_back1" size="20" /> -->
                                                                            </div>
                                                                            
                                                                            <div class="col-md-4">
                                                                                <label>Back:</label>
                                                                                <input type="file" onchange="validate_nominee_citizenship_back(1)" name="nominee_citizenship_back0" id="nominee_citizenship_back1" size="20" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="note">
                                                            <p>(Allowed formats: GIF,JPG,PNG,JPEG,PDF)</p>
                                                        </div>

                                                    </div>
                                                </div>


                                                <div id="depositer_field">

                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="row" id="depositer_image_field">

                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="row" id="depositer_citizenship_field">

                                                    </div>
                                                </div>
                                             <div class="row all-round-border check_options ">
                                                   
                                                </div>
                                                      



                                                <div class="row all-round-border check_options ">

                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group form-group-remove-bottom-margin">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label id="debit_card_" for="debit_card">Debit Card*</label>
                                                                </div>

                                                                <div class="col-sm-4">
                                                                    <input type="radio" name="debit_card" id="debit_card" value="1">Yes
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <input type="radio" name="debit_card"  value="0">No
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row all-round-border check_options ">
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group form-group-remove-bottom-margin">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label id="mobile_banking_services_" for="mobile_banking_services">Mobile Banking Services*</label>
                                                                </div>

                                                                <div class="col-sm-4">
                                                                    <input type="radio" name="mobile_banking_services" id="mobile_banking_services" onchange="showhide_mobile_banking(1)" value="1">Yes
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <input type="radio" name="mobile_banking_services"  id="mobile_banking_services" onchange="showhide_mobile_banking(0)" value="0">No
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="if_mobile_banking_yes" class="col-sm-5" hidden>
                                                           <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label id="mobile_banking_services_password" for="is_banking_password">Do you want transaction password or not?*</label>
                                                                </div>

                                                                <div class="col-sm-4">
                                                                    <input type="radio" name="is_banking_password" id="is_banking_password" value="1">Yes
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <input type="radio" name="is_banking_password" id="is_banking_password" value="0" checked>No
                                                                </div>
                                                            </div>
                                                      </div>
                                                </div>

                                                <div class="row all-round-border check_options ">
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group form-group-remove-bottom-margin">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label id="internet_banking_services_" for="internet_banking_services">Internet Banking Services*</label>
                                                                </div>

                                                                <div class="col-sm-4">
                                                                    <input type="radio" name="internet_banking_services" id="internet_banking_services" onchange="showhide_internet_banking(1)"value="1">Yes
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <input type="radio" name="internet_banking_services"  id="internet_banking_services" onchange="showhide_internet_banking(0)" value="0">No
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="if_internet_banking_yes" class="col-sm-5" hidden>
                                                           <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label id="internet_banking_services_password" for="is_internet_banking_password">Do you want transaction password or not?*</label>
                                                                </div>

                                                                <div class="col-sm-4">
                                                                    <input type="radio" name="is_internet_banking_password" id="is_internet_banking_password" value="1">Yes
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <input type="radio" name="is_internet_banking_password" id="is_internet_banking_password" value="0" checked>No
                                                                </div>
                                                            </div>
                                                      </div>
                                                    
                                                </div>

                                                <div class="row all-round-border check_options ">
                                                    <div class="col-sm-12">
                                                        <div class="form-group form-group-remove-bottom-margin">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label for="other_facilities">Other Facilities</label>
                                                                </div>

                                                                <div class="col-sm-4 label-click">
                                                                    <input type="checkbox" name="locker_facilities" id="locker_facilities" value="1"><label for="locker_facilities">Locker facilities</label>
                                                                </div>
                                                                <div class="col-sm-4 label-click">
                                                                    <input type="checkbox" name="e_statement" id="e_statement" value="1"><label for="e_statement">E-Statement</label>
                                                                </div>
                                                                <div class="col-sm-4 label-click">
                                                                    <input type="checkbox" name="ntc" id="ntc"  value="1"><label for="ntc">NTC(Prepaid/Postpaid/ADSL/PSTN)</label>
                                                                </div>
                                                                <div class="col-sm-4 label-click">
                                                                    <input type="checkbox"name="standing_instruction" id="standing_instruction" value="1"><label for="standing_instruction">Standing Instruction Service</label>
                                                                </div>

                                                                <div class="col-sm-4 label-click">
                                                                    <input type="checkbox"name="bancassurance" id="bancassurance" value="1"><label for="bancassurance">Bancassurance</label>
                                                                </div>
                                                                <div class="col-sm-4 label-click">
                                                                    <input type="checkbox" name="other_service" id="other_service" onchange="showhide_otherservice(1)" value="1"><label for="other_service">Other Services</label>
                                                                </div>

                                                                

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row all-round-border check_options " id="if_otherservice_yes" hidden>
                                                    <div class="col-sm-10" >
                                                        <div class="form-group">
                                                            <div class="form-group">
                                                                <div class="col-sm-2 no-padding">
                                                                    <label class="control-label" id="otherserviceName" for="otherservice_name">Other Service Name</label>
                                                                </div>
                                                                <div class="col-sm-10 no-padding">
                                                                    <input maxlength="100"  type="text" name="otherservice_name" id="otherservice_name"  class="form-control otherservice_name"  placeholder="Please specify Other Service Name">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="step-next-container">
                                                    <div class="left-container">
                                                        <button onclick="save_and_continue_later(2)" class="ctz-btn" type="button" >Save & Continue Later</button>
                                                    </div>
                                                    <div class="right-container">
                                                        <button class="ctz-btn ctz-blue backBtn" type="button" >Back</button>
                                                        <button class="ctz-btn ctz-blue nextBtn nextbutton2"  type="button" >Next</button>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row setup-content" id="step-3">
                                        <div class="col-xs-12">
                                            <div class="steps-title">
                                                <h3>KYC</h3>
                                            </div>
                                            <div class="step-content">
                                                <div class="row all-round-border check_options">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <div class="row">

                                                                <!-- <div class="col-md-6 col-sm-6">-->
                                                                <!--    <div class="form-group">-->
                                                                <!--        <div class="row">-->
                                                                <!--            <div class="col-sm-4">-->
                                                                <!--                <label class="control-label" for="if_nrn">Are you NRN?</label>-->
                                                                <!--            </div>-->
                                                                <!--            <div class="col-sm-8">-->
                                                                <!--               <input type="radio"  name="if_nrn"  id="if_nrn" value="1" checked/>Yes-->
                                                                <!--               <input type="radio"  name="if_nrn" id="if_nrn" value="0" />No-->
                                                                <!--            </div>-->
                                                                <!--        </div>-->
                                                                <!--    </div>-->
                                                                <!--</div>-->


                                                                <div class="col-md-6 col-sm-6">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-sm-4">
                                                                                <label class="control-label" for="if_beneficial_owner">Beneficial Owner?</label>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                               <input type="radio"  name="if_beneficial_owner"  id="if_beneficial_owner_yes" value="1" />Yes
                                                                               <input type="radio"  name="if_beneficial_owner" id="if_beneficial_owner_no" value="0" checked/>No
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div id="showBenefit" hidden>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                 <div class="col-md-6 col-sm-6">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-sm-4">
                                                                                <label class="control-label" for="is_high_risk_customer">High Risk Customer</label>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                               <input type="radio"  name="is_high_risk_customer"  id="is_high_risk_customer_yes" value="1" />Yes
                                                                               <input type="radio"  name="is_high_risk_customer" id="is_high_risk_customer_no" value="0" checked/>No
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="row">
                                                                <div id="showReason" hidden>
                                                                </div>
                                                            </div>

                                                                <div class="col-md-6 col-sm-6">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-sm-4">
                                                                                <label class="control-label" for="mother_name">Mother's Name </label>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <input  maxlength="100" type="text" name="mother_name" id="mother_name" class="form-control capital_letter" placeholder="Mother's Name"  />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-sm-6">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-sm-4">
                                                                                <label class="control-label" for="grandmother_name">Grand Mother's Name</label>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <input  maxlength="100" type="text" name="grandmother_name" id="grandmother_name" class="form-control capital_letter" placeholder="Grand Mother's Name"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-sm-4">
                                                                                <label class="control-label" for="daughter_name">Daughter's Name </label>
                                                                            </div>

                                                                            <div class="col-sm-6 col-xs-8">
                                                                                <input  maxlength="100" type="text" name="daughter_name[]" id="daughter_name1" class="form-control capital_letter" placeholder="Daughter's Name" />

                                                                            </div>
                                                                            <div class="col-sm-2 col-xs-4">
                                                                                <div class="btn btn-success pull-right" id="daughter_add" onclick="add_daughter()">Add more</div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row" id="daughter_name_field">
                                                                        </div>

                                                                        <div class="row step-3-remove" style="display: none;"  id="remove_daughter_button">
                                                                            <div class="col-sm-offset-10 col-sm-2 step-3-remove-btn-container">
                                                                                <div class=" btn btn-danger pull-right"  onclick="remove_daughter()" >Remove</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-sm-6">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-sm-4">
                                                                                <label class="control-label" for="son_name">Son's Name</label>
                                                                            </div>
                                                                            <div class="col-sm-6 col-xs-8">
                                                                                <input  maxlength="100" type="text" name="son_name[]" id="son_name1" class="form-control capital_letter" placeholder="Son's Name"/>
                                                                            </div>
                                                                            <div class="col-sm-2 col-xs-4">
                                                                                <div class="btn btn-success pull-right" onclick="add_son()" id="son_add">Add more</div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row" id="son_name_field">
                                                                        </div>

                                                                        <div class="row step-3-remove" style="display: none;"  id="remove_son_button">
                                                                            <div class="col-sm-offset-10 col-sm-2 step-3-remove-btn-container">
                                                                                <div class=" btn btn-danger pull-right"  onclick="remove_son()" >Remove
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-sm-4">
                                                                                <label class="control-label" for="daughter_in_law_name">Daughter in law's Name </label>
                                                                            </div>


                                                                            <div class="col-sm-6 col-xs-8">
                                                                                <input  maxlength="100" type="text" name="daughter_in_law_name[]" id="daughter_in_law_name1" class="form-control capital_letter" placeholder="Daughter in law's Name" />

                                                                            </div>

                                                                            <div class="col-sm-2 col-xs-4">
                                                                                <div class="btn btn-success pull-right" id="daughter_in_law_add" onclick="add_daughter_in_law()">Add more</div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row" id="daughter_in_law_name_field">
                                                                        </div>
                                                                        <div class="row step-3-remove" style="display: none;"  id="remove_daughter_in_law_button">
                                                                            <div class="col-sm-offset-10 col-sm-2 step-3-remove-btn-container">
                                                                                <div class=" btn btn-danger pull-right"  onclick="remove_daughter_in_law()" >Remove
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6 col-sm-6">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-sm-4">
                                                                                <label class="control-label" for="father_in_law_name">Father in law's Name </label>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <input  maxlength="100" type="text" name="father_in_law_name" id="father_in_law_name" class="form-control capital_letter" placeholder="Father in law's Name" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row all-round-border check_options">
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-md-6 col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-sm-4">
                                                                            <label class="control-label" for="expected_monthly_turnover">Expected Annual Turnover *</label><br/>
                                                                        </div>
                                                                        <div class="col-sm-8">
                                                                            <select  class="styled-select slate" name="expected_monthly_turnover" id="expected_monthly_turnover" style="width:100%;" >
                                                                                <option value="" selected="">Select</option>
                                                                                                                                                                    <option value="Below Rs.20.00 Lac">Below Rs.20.00 Lac</option>
                                                                                                                                                                    <option value="Between Rs.20.00 Lac to Below Rs.50.00 Lac">Between Rs.20.00 Lac to Below Rs.50.00 Lac</option>
                                                                                                                                                                    <option value="Between Rs.50.00 Lac to Below Rs.100.00 Lac">Between Rs.50.00 Lac to Below Rs.100.00 Lac</option>
                                                                                                                                                                    <option value="Above Rs.100.00 Lac">Above Rs.100.00 Lac</option>
                                                                                                                                                                
                                                                            </select>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6 col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-sm-4">
                                                                            <label class="control-label" for="expected_monthly_transaction">Expected Annual Transaction </label><br/>
                                                                        </div>
                                                                        <div class="col-sm-8">
                                                                            <select  class="styled-select slate" name="expected_monthly_transaction" id="expected_monthly_transaction" style="width:100%;" >
                                                                                <option value="" selected="">Select</option>
                                                                                                                                                                    <option value="Below 25 Transactions">Below 25 Transactions</option>
                                                                                                                                                                    <option value="Between 25 to Below 50 Transactions">Between 25 to Below 50 Transactions</option>
                                                                                                                                                                    <option value=" Between 50 to Below 100 Transactions"> Between 50 to Below 100 Transactions</option>
                                                                                                                                                                    <option value="Above 100 Transactions">Above 100 Transactions</option>
                                                                                                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6 col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-sm-4">
                                                                            <label class="control-label" for="purpose_of_account">Purpose Of Account *</label><br/>
                                                                        </div>
                                                                        <div class="col-sm-8">
                                                                            <select  class="styled-select slate" name="purpose_of_account" id="purpose_of_account" onchange="getPurposeofAction()" style="width:100%;" >
                                                                                 <option value="" selected="">Select</option>
                                                                                                                                                                   <option value="Remittance">Remittance</option>
                                                                                                                                                                    <option value="Saving">Saving</option>
                                                                                                                                                                    <option value="Business">Business</option>
                                                                                                                                                                    <option value="Others">Others</option>
                                                                                                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="col-md-6 col-sm-6">
                                                                </div>
                                                            <div id="purposeOthers" hidden>
                                                                
                                                            </div>

                                                            <div class="col-md-6 col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-sm-4">
                                                                            <label class="control-label" for="source_of_fund">Source Of Fund </label><br/>
                                                                        </div>
                                                                        <div class="col-sm-8">
                                                                            <select  class="styled-select slate" name="source_of_fund" id="source_of_fund" onchange="getSourceofAction()"style="width:100%;" >
                                                                                <option value="" selected="">Select</option>
                                                                                                                                                                    <option value="Business Income">Business Income</option>
                                                                                                                                                                    <option value="Rental Income">Rental Income</option>
                                                                                                                                                                    <option value="Salary Income">Salary Income</option>
                                                                                                                                                                    <option value="Others">Others</option>
                                                                                                                                                            </select>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6">
                                                                </div>
                                                            <div id="sourceOthers" hidden>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="step-next-container">
                                                    <div class="left-container">
                                                        <button onclick="save_and_continue_later(3)" class="ctz-btn" type="button" >Save & Continue Later</button>
                                                    </div>
                                                    <div class="right-container">
                                                        <button class="ctz-btn ctz-blue backBtn" type="button" >Back</button>
                                                        <button class="ctz-btn ctz-blue nextBtn" type="button" >Next</button>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row setup-content" id="step-4">
                                        <div class="col-xs-12">
                                            <div class="steps-title">
                                                <h3>General Rules For Current/Savings Account</h3>
                                            </div>
                                            <div class="step-content">
                                                                                                    <div class="row all-round-border">
                                                        <div class="col-sm-12">
                                                            <div class="saving-rules-container">
                                                                <ol>
 <li>The constituent(s) can only withdraw sums from account by means of cheque supplied to by the Bank for that particular account.</li>

<li>  Cheques should be signed as per specimen signature supplied to the Bank and any alternation in the cheque must be authenticated by the drawer's full signature.</li>

<li> Post dated and stale cheques will not be paid.</li>

<li> Cheques issued by the Bank are the property of constituent(s) and they should take utmost care and keep in safe place under lock. The constituent(s) shall not hold the Bank liable if such cheques are misplaced, stolen or encashed in any way by fradulent signature.</li>
<li> The Bank will register instructions from the drawer of a cheque for its payment, but it can not accept any responsibility in case such instructions are overlooked.</li>

<li> Collections are undertaken at the risk of the constituent(s) only. The Bank should endeavour to collect the cheques and the items as promptly and carefully as possible, 
                    but it can accept no responsibility in case of any delay or loss. All cheques and other instruments should be crossed before they are paid-in for credit of accounts. Uncleared items though credited in the account, shall not be available for being drawn against. The Bank shall have right to debit the customer's account, if they are not realised.</li>

<li> The Bank will take care to see that credit and debit entries are correctly recorded in the accounts of the constituents(s), in case of any error, the Bank shall be within its rights to make the correct adjusting entries without notice and recover any amount due from the constituent(s). The Bank shall not be liable for any damage, loss, etc., to constituent(s) on such errors.</li>

<li>  Any change in the address or constitution of the constituent(s) should be immediately communicated to the Bank. The post office and other Agents for delivery shall be considered Agents of the constituent(s) for all delivery of letters, remittances, etc., and no responsibility can be accepted by the Bank for delay, non-delivery, etc.</li>
<li>  In the absence of contract to the contrary the credit balance in any account in the name of two or more persons, on the death of one or more of them, shall be payable to survivor as lawfully appointed nominee(s) of the deceased and if there is a debit balance, the survivors and the estates of deceased constituent(s) shall be jointly and severally liable for repayment thereof.</li>

<li> A receipt of moneys, cheques, securities, etc., on behalf of the Bank is valid only if signed by duly authorised officers.</li>

<li> The Bank reserves to itself the right to add to or alter any or all of the rules after notification and such altered or additional rules shall immediately thereafter be deemed to be binding on all constituent(s).</li>
<li> The Bank reserves to itself the right to close (without previous notice) any account which, in its opinion, is not satisfactorily operated upon or for any other reason whatsoever which shall not be incumbent on the Bank to disclose to the constituent(s).</li>
<li> A distinctive number is allotted to each account which should be quoted in all correspondence relating to the account and when making deposits or withdrawals.</li>

<li> For personal accounts please fill in a "nomination" form.</li>

<li> Periodic statement of accounts shall be considered correct unless we receive from you in writing to the contrary within seven (7) days after dispatch thereof by us.</li>

<li> The Bank reserves the right to debit in respect of standard charges, fees and commission of the bank implemented from time to time.</li>

<li>The following certified documents has to be submitted while requesting for opening of account</li>
</ol>
                                                                           </div>
                                                        </div>
                                                    </div>
                                                    <div class="row all-round-border">
                                                        <div class="col-sm-12">
                                                            <div class="rules-tab-container">
                                                                <div class="co-tab-container">
                                                                    <ul class="nav nav-tabs" id="nav" role="tablist">
                                                                        <li class="active">
                                                                            <a href="#rule_5" role="tab" data-toggle="tab">
                                                                                <span>A.&nbsp Individual Account</span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#rule_2" role="tab" data-toggle="tab">
                                                                                <span>B. &nbsp Partnership or Sole Proprietorship Firm</span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#rule_3" role="tab" data-toggle="tab">
                                                                                <span>C.&nbsp Company Account</span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#rule_4" role="tab" data-toggle="tab">
                                                                                <span>D. &nbsp Club / Non-Governmental Organization</span>
                                                                            </a>
                                                                        </li>

                                                                    </ul>

                                                                    <div class="tab-content" role="tablist">
                                                                        <div role="tabpanel" class="tab-pane fade in active" id="rule_5">
                                                                            <div  style="padding-left:0;">
                                                                                <ol>
<li>  Photocopy of Citizenship Certificate / Passport</li> 
 <li>  Recent Photograph</li> 
<li>  Employee ID Photocopy (staff of government, public and organized institution; Staff. Teacher or Professor of School or University funded by Nepal Government ) </li>
</ol>                                                                            </div>
                                                                        </div>
                                                                        <div role="tabpanel" class="tab-pane fade" id="rule_2">
                                                                            <div  style="padding-left:0;">
                                                                                 <ol>
 <li>  Name of the Firm</li>
 <li> Registered Address of the Firm (District. Municipality / VDC, Ward No.. Tole   / Village, 
 House No.. Telephone No., Fax No.)</li>

 <li> Changed address of firm in case of change in registered address</li>
 <li>  Registration Certificate (Registration No.. Registering Office, Registered Date)</li>

  <li>  Permanent Account Number (PAN)</li>

 <li> Nature of Business</li>


  <li>  Working Area</li>
 <li>  No. of branches and location of main branches</li>
    <li> Estimated Annual Turnover</li>
     <liDetails of Proprietor. Partners and Account Operators (Position. Name. Surname,   Name of Husband / Wife, Father. Grandfather, Permanent Address, Present   Address, Telephone No., Mobile No., E-mail address)</li>
    <li> Photocopy of Citizenship Certificate / Passport and PP size photo of Proprietor,   
    Partners and Account Operators</li>
     <li> Audited Financials for the previous Fiscal Year. For firms not requiring audit, 
       self-declaration from Proprietor / Partners mentioning audit not required.</li>
       <li> Partnership Agreement / Deed    
      <li>    Mandate given for financial and administrative works in case of Partnership Firm</li>
          <li>  Board Decision and Mandate for the opening of account and its operation</li>
             <li> Mandate / Power of Attorney for Financial Transactions given 
             to Chief Executive or Other Official by the Board   </li>                 

 </ol>                                                                            </div>
                                                                        </div>

                                                                        <div role="tabpanel" class="tab-pane fade " id="rule_3">
                                                                            <div  style="padding-left:0;">
                                                                                <ol>
 <li>

<li> Company Name</li>
<li> Registered Address of the Firm (District. Municipality / VDC, Ward No., Tole /  
   Village. House No., Telephone No., Fax No., E-mail Address. Web Address)</li>
<li> Changed address of company in case of change in registered address</li>
<li> Registration Certificate (Registration No., Registering Office, Registered Date)</li>
<li> Permanent Account Number (PAN)<br/> 6.  Nature of Business</li>
 <li> Working Area</li>
  <li>No. of branches and location of main branches</li>
   <li> Estimated Annual Turnover</li>
   <li> Details of Directors. Chief Executive and Account Operators 
   (Position, Name,   Surname. Name of Husband / Wife. Father, Grandfather, 
   Permanent Address.   Present Address, Telephone No., Mobile No., E-mail address)</li>
     <li> Photocopy of Citizenship Certificate / Passport and PP size photo of Directors.   Chief Executive and Account Operators</li>
      <li> Audited Financials for the previous Fiscal Year</li>
        <li> If subsidiary of Foreign Company, name and address of the Foreign Company</li>
          <li> Certificate of Incorporation and Memorandum of Association and Articles of      Association of the Company</li>
            </ol>                                                                              </div>
                                                                        </div>

                                                                        <div role="tabpanel" class="tab-pane fade " id="rule_4">
                                                                            <div  style="padding-left:0;">
                                                                                
<ol>

<li>  Name of Club or NGO</li>
<li>  Registered Address (District. Municipality / VDC, Ward No.. Tole / Village, House No.. Telephone No., Fax No., E-mail Address, Web Address)
 <li> Changed address in case of change in registered address</li>
 <li> Registration Certificate (Registration No., Registering Office, Registered Date)</li>
   <li>  Permanent Account Number (PAN)</li>
<li>  Nature of Business</li>
<li>  Work Area</li>
    <li>  No. of branches and location of main branches</li>
    <li>  Estimated Annual Turnover</li>
   <li> Details of Executive Committee Members, Chief Executive and Account Operators 

    (Position. Name, Surname, Name of Husband / Wife, Father. Grandfather. Permanent Address, 
    Present Address, Telephone No., Mobile No., e-mail address)</li>
   <li> Photocopy of Citizenship Certificate / Passport and PP size photo of Executive Committee Members, Chief Executive and Account Operators  </li>
      <li>Audited Financials for the previous Fiscal Year       </li>             
        <li> Constitution / By-Laws</li>
         <li> Copy of Executive Committee decision for the opening of account. its operation and financial transactions</li>
          </ol>                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                              <div class="row all-round-border">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    
                                                        <div class="radio-contianer">
                                                            <div class="radio-option">
                                                                <div class="row">
                                                                    <div class="col-sm-1">
                                                                        <input type="checkbox" id="general_rules_radio" style="font-weight:bold;" name="general_rules" value="no" />
                                                                    </div>

                                                                    <div class="col-sm-11">
                                                                        <label class="control-label" id="general_rules_" for="general_rules_">I/We have read and understood all the general rules for conducting the account and I/we agree to abide by the Bank�s rules</label>
                                                                    </div>
                                                                </div>
                                                                
                                                                
                                                            </div>
                                                        </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                            <div class="step-next-container">
                                                <div class="left-container">
                                                    <button onclick="save_and_continue_later(4)" class="ctz-btn" type="button" >Save & Continue Later</button>
                                                </div>
                                                <div class="right-container">
                                                    <button class="ctz-btn ctz-blue backBtn" type="button" >Back</button>
                                                    <button id="next4" class="ctz-btn ctz-blue nextBtn" type="button">Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row setup-content" id="step-5">
                                    <div class="col-xs-12">
                                        <div class="steps-title">
                                            <h3>Authorization</h3>
                                        </div>
                                        <div class="step-content">
                                         
                                            <div class="map-container">
                                                <input type="text" name="location" id="location" class="form-control" placeholder="Search" >
                                                <p>Mark Your Home in Google Map</p>
                                                <!--<button type="button" onclick="getLocationcur()" class="btn btn-primary" style="background-color:#8dcfb7">Show Current Location</button>-->
                                                <div id="dvMap">
                                                    <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
                                                </div>
                                                <input type="hidden" name="longitude" id="longitude" required=" ">
                                                <input type="hidden" name="latitude" id="latitude" required="">
                                            </div>

                                            <div class="map-list-container">
                                                <ul>
                                                    <li>I/We agree to comply with the prevent rules of the bank in force time to time regarding conduct of the Account and agree to abide by them.</li>
                                                    <li>I/We declare that all the information and documents provided to the bank arte true and correct. In case any false statement submitted, I/We agree to be held responsible for the same, without any liability on the part of the bank or its staffs. </li>
                                                  
                                                </ul>
                                            </div>
                                        </div>

                                        
                                        <div class="step-next-container">
                                            <div class="right-container">
                                                <button class="ctz-btn ctz-blue backBtn" type="button" >Back</button>
                                                <button class="ctz-btn finish" id="finish" onclick="getAccountStatusFinish()" type="button">Finish</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- Container Ends -->
                        </div>
                        <!-- Dept Tabs Ends -->
                    </div>
                    <!-- Department Ends -->



                    <!-- Modal -->
                    <div id="form_preview" class="modal fade  ctzn-modal" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>    
                                <div class="modal-body">
                                    <div class="title">
                                        <h1>Form Preview</h1>
                                    </div>

                                    <div class="content">
                                     <div id="branch_prev">

                                     </div>
                                 </div>
                             </div>
                             <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="submit" id="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>

                    </div>
                </div>
                </form>

            </div>
            <!-- Left-big-contianer Ends -->
        </div>
        <!-- Outer Custom contianer Ends -->
    </section>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8xUqFu-Te4JAwIsNP25J6b59cfxbjgyQ&callback=initMap&libraries=places"></script>
<script type="text/javascript">
    var account_count = 1;
    var yes = "yes";
    var no = "no";
</script>

<script>

    var myRadio = document.getElementById('general_rules_radio');
    var booRadio;
    
    myRadio.onclick = function(){

        if(booRadio == this){
            this.checked = false;
            booRadio = null;
            var radio_val = $("input[name='general_rules']:checked").val();
        }else{
            booRadio = this;
        }
    };
    
    function copy_permanent_address(){
        for(i=1;i<=account_count;i++){

            var pa_house_number2= $('#pa_house_number'+i).val();
            var pa_municipality_vdc2=$('#pa_municipality_vdc'+i).val();
            var pa_ward_number2=$('#pa_ward_number'+i).val();
            var pa_street2=$('#pa_street'+i).val();
            var pa_district2=$('#pa_district'+i).val();
            $('#ca_house_number'+i).val(pa_house_number2);
            $('#ca_municipality_vdc'+i).val(pa_municipality_vdc2);
            $('#ca_ward_number'+i).val(pa_ward_number2);
            $('#ca_street'+i).val(pa_street2);
            $('#ca_district'+i).val(pa_district2);
        }
    }
</script>

<script type="text/javascript">
    function awesomeScrolling(target) {
        $target = $("#"+target);
        var targetposition = $target.offset().top - 230;

        $('html, body').stop().animate({
            'scrollTop':  targetposition
        }, 900, 'swing', function () {
                // window.location.hash = target;
            }).preventDefault();
    }
</script>

<script type="text/javascript">
    function convert_into_english_issue(index){
        var year = $("#doi_bs_year"+index).find(":selected").text();
        var month = $("#doi_bs_month"+index).find(":selected").text();
        var day = $("#doi_bs_day"+index).find(":selected").text();
        if((year != '' || year != null) && (month != '' || month != null) && (day != '' || day != null)){
            var endDate = calendarFunctions.getAdDateByBsDate(parseInt(year), parseInt(month), parseInt(day));

            var date = new Date(endDate);

            var ad_year = date.getFullYear(); 
            var ad_month = date.getMonth() +1; 
            var ad_day = date.getDate(); 
            if(ad_year != '' && ad_month != '' && ad_day != ''){
                $("#doi_year"+index).val(ad_year);
                $("#doi_month"+index).val(ad_month);
                $("#doi_day"+index).val(ad_day);
            }
        }
        return false;
    }
</script>

<script type="text/javascript">
    function convert_into_nepali_issue(index){
        var year = $("#doi_year"+index).find(":selected").text();
        var month = $("#doi_month"+index).find(":selected").text();
        var day = $("#doi_day"+index).find(":selected").text();

        if((year != '' || year != null) && (month != '' || month != null) && (day != '' || day != null)){
            var endDate = calendarFunctions.getBsDateByAdDate(parseInt(year), parseInt(month), parseInt(day));
            console.log(endDate);
            // var date = new Date(endDate);

            var bs_year = endDate.bsYear; 
            var bs_month = endDate.bsMonth; 
            var bs_day = endDate.bsDate; 
            if(bs_year != '' && bs_month != '' && bs_day != '')
            {
                $("#doi_bs_year"+index).val(bs_year);
                $("#doi_bs_month"+index).val(bs_month);
                $("#doi_bs_day"+index).val(bs_day);
                
            }
        }
        
        return false;
    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');
        allBackBtn = $('.backBtn');
        allWells.hide();

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
            $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });

        allNextBtn.click(function(){
            var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='number'],input[type='email'],input[type='url']"),
            isValid = true;


            $(".form-group").removeClass("has-error");
            for(var i=0; i<curInputs.length; i++){
                if (!curInputs[i].validity.valid){
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }


            if(curStepBtn=="step-1"){
                if(validate_form_one()==false){
                    isValid=false;
                }

                if(isValid!=false){
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }  
            }
            
            if(curStepBtn=="step-2"){
                if(validate_form_two()==false){
                    isValid=false;
                }
                if(isValid!=false){
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }
            }

            if(curStepBtn=="step-3"){
                if(validate_form_three()==false){isValid=false;}
                if(isValid!=false){
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                } 
            }

            if(curStepBtn=="step-4"){
                if(validate_form_four()==false){isValid=false;}
                if(isValid!=false){
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                } 
            }


            if (isValid)
                nextStepWizard.removeAttr('disabled').trigger('click');
                google.maps.event.trigger(dvMap, 'resize');
            });


            allBackBtn.click(function(){
            var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            backStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

            $(".form-group").removeClass("has-error");
            for(var i=0; i<curInputs.length; i++){
                if (!curInputs[i].validity.valid){
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid)
                backStepWizard.removeAttr('disabled').trigger('click');
            google.maps.event.trigger(dvMap, 'resize');
        });

        $('div.setup-panel div a.btn-primary').trigger('click');
    });
</script>

<script type="text/javascript">
    function getAddress() {
        var district = document.getElementById("pa_district1").value;
        $('#address').val(district);
    }
</script>

<script type="text/javascript">
    function validate_form_one(){

        if($('#applying_from option:selected').val()==""){
            alert("please Select Applying from");
            
            $("#applying_from").css({"border-color":"#a94442"});
            // $( "#applying_from" ).focus();
            awesomeScrolling("applying_from");
            return false;
        }
        else{
            $("#applying_from").css({"border-color":"#8dcfb7"});
        }
       
        if($('#branch option:selected').val()==""){
            alert("please Select The Branch");
            $("#branch").css({"border-color":"#a94442"});
            // $( "#branch" ).focus();
            awesomeScrolling("branch");
            return false;
        }
        else{
            $("#branch").css({"border-color":"#8dcfb7"});
        }
        
         
        
        if($('#account_name option:selected').val()==""){
            alert("please Select The Account");
            $("#account_name").css({"border-color":"#a94442"});
            // $( "#account_name" ).focus();
            awesomeScrolling("account_name");
            return false;
        }
        else{
            $("#account_name").css({"border-color":"#8dcfb7"});
        }

        if(!$("input[name='account_category']:checked").val()){
            alert("please check Account Category");
            $("#account_category").css({"color":"#a94442"});
            // $( "#individual_account" ).focus();
            awesomeScrolling("individual_account");
            return false;
        }
        else{
            $("#account_category").css({"color":"#333333"});
        }
        
        if(!$("input[name='types_of_individual']:checked").val()){
            alert("Please select the Types of Individual");
            $("#types_of_individual").css({"color":"#a94442"});
            // $( "#individual_account" ).focus();
            awesomeScrolling("individual_account");
            return false;
        }
        else{
            $("#types_of_individual").css({"color":"#333333"});
        }
        
       
        
        var x = $("input[name='currency']:checked").val();
        if($('#currency_others_checkbox').is(':checked')){
            if($("#other_country_option").val() == '')
            {
                alert('Please Select Other Currency');

                return false;
            }

        }
        
        
        else if(!x){
            alert("please check Currency");
            $("#currency").css({"color":"#a94442"});
                //  $( "#npr" ).focus();
                awesomeScrolling("npr");
                return false;
            }
            else{
                $("#currency").css({"color":"#333333"});
            }

            if(!$("input[name='account_type']:checked").val()){
                alert("please check Account Type");
                $("#account_type").css({"color":"#a94442"});
            // $( "#single" ).focus();
            awesomeScrolling("single");
            return false;
        }
        else{
            $("#account_type").css({"color":"#333333"});
        }

        for(i=1;i<=account_count;i++)
        {
            var phone = $("#phone_number"+i).val();
            var mobile = $("#mobile_no"+i).val();
            var email_val = $("#email"+i).val();
            var place_of_issue = $("#place_of_issue"+i).val();
            var a_i = i-1;
            
            var first_name = $("#first_name"+i).val();
            var last_name = $("#last_name"+i).val();
            var fathers_name = $("#father_name"+i).val();
            var grandfathers_name = $("#grandfather_name"+i).val();
            
            
            if($('#account_name option:selected').val()=="Citizens Rastra Sewak Saving" && phone.length<9){
                alert("please Enter valid Phone Number");
            // alert(phone.length);
            $("#phone_number"+i).css({"border-color":"#a94442"});
            awesomeScrolling("phone_number"+i);

            
            return false;
        }
        else{
            $("#phone_number"+i).css({"border-color":"#8dcfb7"});
        }


            // alert(first_name);
            if(first_name=="")
            {
                alert("please enter First Name");
                // $( "#first_name"+i ).focus();
                awesomeScrolling("first_name"+i);
                return false;
            }
            
            if(last_name=="")
            {
                alert("please enter Last Name");
                // $( "#last_name"+i ).focus();
                awesomeScrolling("last_name"+i);
                return false;
            }
            
            if(fathers_name=="")
            {
                alert("please enter Fathers Name");
                // $( "#father_name"+i ).focus();
                awesomeScrolling("father_name"+i);
                return false;
            }
            
            if(grandfathers_name=="")
            {
                alert("please enter GrandFathers Name");
                // $( "#grandfather_name"+i ).focus();
                awesomeScrolling("grandfather_name"+i);
                return false;
            }
            
            
            
             if(!$("input[name='existing_relation["+a_i+"]']:checked").val()){
                alert("please select the Existing Relationship");
                $("#existing_relation_"+i).css({"color":"#a94442"});
                // $( "#existing_relation"+i ).focus();
                awesomeScrolling("existing_relation"+i);
                return false;
            }
            else{
                $("#existing_relation_"+i).css({"color":"#333333"});
            }

            if($("input[name='existing_relation["+a_i+"]']:checked").val() == "yes" ){
                if($("#account_number"+i).val()=="")
                {
                    alert("please enter Account Number");
                // $( "#account_number"+i ).focus();
                awesomeScrolling("account_number"+i );
                return false;
            }
        }
            
            

            if($('#y'+i+' option:selected').val()=="" || $('#m'+i+' option:selected').val()=="" || $('#d'+i+' option:selected').val()=="")
            {
                alert("please Select Date of Birth(AD) Correctly");
                $("#date_of_birth_ad"+i).css({"color":"#a94442"});
                // $( "#y"+i ).focus();
                awesomeScrolling("y"+i);
                return false;
            }
            else{
                $("#date_of_birth_ad"+i).css({"color":"#333333"});
            }

            if($('#yn'+i+' option:selected').val()=="" || $('#mn'+i+' option:selected').val()=="" || $('#dn'+i+' option:selected').val()=="")
            {
                alert("please Select Date of Birth(BS) Correctly");
                $("#date_of_birth_bs"+i).css({"color":"#a94442"});
                // $( "#yn"+i ).focus();
                awesomeScrolling("yn"+i);
                return false;
            }
            else{
                $("#date_of_birth_bs"+i).css({"color":"#333333"});
            }

            if($("#nationality"+i).val()=="")
            {
                alert("please enter nationality");
                // $( "#nationality"+i ).focus();
                awesomeScrolling("nationality"+i);
                return false;
            }


           
            
            if($('#occupation'+i+' option:selected').val()=="")
            {
                alert("please select Occupation");
                $("#occupation"+i).css({"border-color":"#a94442"});
                // $( "#occupation"+i ).focus();
                awesomeScrolling("occupation"+i);
                return false;
            }
            else{
                $("#occupation"+i).css({"border-color":"#8dcfb7"});
            }
            if($('#occupation'+i+' option:selected').val()=="others" ){
                if($("#occupation_input"+i).val()=="")
                {
                    alert("please enter Nature of Business");
                // $( "#occupation_input"+i ).focus();
                awesomeScrolling("occupation_input"+i);
                return false;
            }
        }

        if($('#passport_citizenship'+i+' option:selected').val()=="")
        {
            alert("please select Identity Verification Card");
            $("#passport_citizenship"+i).css({"border-color":"#a94442"});
                // $( "#passport_citizenship"+i ).focus();
                awesomeScrolling("passport_citizenship"+i);
                return false;
            }
            else{
                $("#passport_citizenship"+i).css({"border-color":"#8dcfb7"});
            }




            if($('#passport_citizenship'+i+' option:selected').val()=="passport" ){
                if($("#passport_no"+i).val()=="")
                {
                    alert("please enter Passport Number");
                // $( "#passport_no"+i ).focus();
                 $("#passport_no"+i).css({"border-color":"#a94442"});
                awesomeScrolling("passport_no"+i);
                return false;
            }else if($("#expiry_date"+i).val()==""){
                alert("please enter Expiry Date");
                 $("#expiry_date"+i).css({"border-color":"#a94442"});
                awesomeScrolling("expiry_date"+i);
                return false;

            }
        } else if($('#passport_citizenship'+i+' option:selected').val()=="citizenship" ){
            if($("#citizenship_no"+i).val()=="")
            {
                alert("please enter citizenship Number");

                awesomeScrolling("citizenship_no"+i);
                return false;
            }
        } 
        else if ($('#passport_citizenship'+i+' option:selected').val()=="nrn_card" ){
            if($("#nrn_card"+i).val()==""){

                alert("please enter NRN Card Number");

                awesomeScrolling("nrn_card"+i);
                return false;

            } else if($("#foreign_address"+i).val()==""){
                alert("please enter Foreign Address");
                $("#foreign_address"+i).css({"border-color":"#a94442"});
                awesomeScrolling("foreign_address"+i);
                return false;

            }
            else if($("#country"+i).val()==""){
               alert("please enter Country");
               $("#country"+i).css({"border-color":"#a94442"});
               awesomeScrolling("country"+i);
               return false;
           }
           else if($("#city_state"+i).val()==""){
            alert("please enter city_state");
            $("#city_state"+i).css({"border-color":"#a94442"});
            awesomeScrolling("city_state"+i);
            return false;
        }
        else if($("#contact_no"+i).val()==""){
            alert("please enter contact_no");
            $("#contact_no"+i).css({"border-color":"#a94442"});
            awesomeScrolling("contact_no"+i);
            return false;
        }
        else if($("#type_of_visa"+i).val()==""){
            alert("please enter Type of visa");
            $("#type_of_visa"+i).css({"border-color":"#a94442"});
            awesomeScrolling("type_of_visa"+i);
            return false;
        }
        else if($("#visa_expiry_date"+i).val()==""){
            alert("please enter Visa Expiry date");
            $("#visa_expiry_date"+i).css({"border-color":"#a94442"});
            awesomeScrolling("visa_expiry_date"+i);
            return false;
        }

    } else if ($('#passport_citizenship'+i+' option:selected').val()=="birth_certificate" ){
       if($("#birth_certificate"+i).val()==""){

        alert("please enter Birth Certificate");
         $("#birth_certificate"+i).css({"border-color":"#a94442"});
        awesomeScrolling("birth_certificate"+i);
        return false;
    } 
}else{
    if($("#other_id"+i).val()=="")
    {
        alert("please enter Other ID Number");
         $("#other_id"+i).css({"border-color":"#a94442"});
                // $( "#citizenship_no"+i ).focus();
                awesomeScrolling("other_id"+i);
                return false;
            }
        }
        
         if(!$("input[name='gender["+a_i+"]']:checked").val()){
                alert("please select the gender");
                $("#gender"+i).css({"color":"#a94442"});
                // $( "#male"+i ).focus();
                awesomeScrolling("male"+i);
                return false;
            }
            else{
                $("#gender"+i).css({"color":"#333333"});
            }
            
           

        

        if(place_of_issue.length<1)
        {
            alert("please enter Place of Issue");
            $("#place_of_issue"+i).css({"border-color":"#a94442"});
                // $( "#place_of_issue"+i ).focus();
                awesomeScrolling("place_of_issue"+i);
                return false;
            }
            else{
                $("#place_of_issue"+i).css({"border-color":"#8dcfb7"});
            }

            // console.log($('#doi_year'+i+' option:selected').val());
            if($('#doi_year'+i+' option:selected').val()=="" || $('#doi_month'+i+' option:selected').val()=="" || $('#doi_day'+i+' option:selected').val()=="")
            {
                alert("please Select Date of Issue Correctly");
                $("#date_of_issue_ad"+i).css({"color":"#a94442"});
                // $( "#doi_year"+i ).focus()
                awesomeScrolling("doi_year"+i);

                return false;
            }
            else{
                $("#date_of_issue_ad"+i).css({"color":"#333333"});
            }

            if($('#doi_bs_year'+i+' option:selected').val()=="" || $('#doi_bs_month'+i+' option:selected').val()=="" || $('#doi_bs_day'+i+' option:selected').val()=="")
            {
                alert("please Select Date of Issue Correctly");
                $("#date_of_issue_bs"+i).css({"color":"#a94442"});
                // $( "#doi_bs_year"+i ).focus()
                awesomeScrolling("doi_bs_year"+i);

                return false;
            }
            else{
                $("#date_of_issue_bs"+i).css({"color":"#333333"});
            }
        if(email_val.length<1)
        {
            alert("please enter your Email");
                // $( "#email"+i ).focus();
                awesomeScrolling("email"+i);
                return false;
            }
            
            
            
            if(!$("input[name='marital_status["+a_i+"]']:checked").val()){
                alert("please select the marital status");
                $("#marital_status_"+i).css({"color":"#a94442"});
                // $( "#unmarried"+i ).focus()
                awesomeScrolling("unmarried"+i);

                return false;
            }
            else{
                $("#marital_status_"+i).css({"color":"#333333"});
            }

            if($("#pa_municipality_vdc"+i).val()=="")
            {
                alert("please enter Municipality / VDC");
                // $( "#pa_municipality_vdc"+i ).focus();
                awesomeScrolling("pa_municipality_vdc"+i);
                return false;
            }
            
            if($("#pa_ward_number"+i).val()=="")
            {
                alert("please enter Ward Number");
                // $( "#pa_ward_number"+i ).focus();
                awesomeScrolling("pa_ward_number"+i);
                return false;
            }
            
            // if($("#pa_street"+i).val()=="")
            // {
            //     alert("please enter Street ");
            //     // $( "#pa_street"+i ).focus();
            //     awesomeScrolling("pa_street"+i);
            //     return false;
            // }

            if($('#pa_district'+i+' option:selected').val()=="")
            {
                alert("please Select Permanent Address District");
                $("#pa_district"+i).css({"border-color":"#a94442"});
                // $( "#pa_district"+i ).focus();
                awesomeScrolling("pa_district"+i);
                return false;
            }
            else{
                $("#pa_district"+i).css({"border-color":"#8dcfb7"});
            }

            if(phone.length>0 && phone.length<9)
            {
                alert("please enter 9 digits for phone no");
                $("#phone_number_"+i).css({"color":"#a94442"});
                $("#phone_number"+i).css({"border-color":"#a94442"});
                awesomeScrolling("phone_number"+i);
                return false;
            }
            else{
               $("#phone_number"+i).css({"border-color":"#8dcfb7"});
               $("#phone_number_"+i).css({"color":"#333333"});
           }

           if(mobile.length<10)
           {
            alert("please enter 10 digits for mobile no");
                // $( "#mobile_no"+i ).focus();
                awesomeScrolling("mobile_no"+i);
                return false;
            }


        }
        
        
        if($("input[name='account_type']:checked").val() == 'Minor'){
            if(!$("#age_of_minor").val()){
                alert("Please enter the Age of Minor");
                $("#age_of_minor").css({"border-color":"#a94442"});
                awesomeScrolling("age_of_minor");
            }
            else{
               $("#age_of_minor").css({"border-color":"#8dcfb7"});
           }

            if($("#age_of_minor").val() > 18){
                alert("Minor age should be less than or equals to 18");
                $("#age_of_minor").css({"border-color":"#a94442"});
                awesomeScrolling("age_of_minor");
            }
            else{
                 $("#age_of_minor").css({"border-color":"#8dcfb7"});
            }


            
           if(!$("#name_of_guardian").val()){
            alert("Please enter the Name of Guardian");
            $("#name_of_guardian").css({"border-color":"#a94442"});
            awesomeScrolling("name_of_guardian");
        }
        else{
           $("#name_of_guardian").css({"border-color":"#8dcfb7"});
       }
       if(!$("#relationship_minor").val()){
        alert("Please enter the Relationship with Minor");
        $("#relationship_minor").css({"border-color":"#a94442"});
        awesomeScrolling("relationship_minor");
    }
    else{
       $("#relationship_minor").css({"border-color":"#8dcfb7"});
   }

}

       for(i=1;i<=account_count;i++)
            {

                if($('#image'+i+'').val()=="")
                {
                    alert("please select holder"+i+" Image to upload");
                    $("#image_"+i).css({"color":"#a94442"});
                // $( "#image"+i).focus();
                awesomeScrolling("image"+i);
                return false;
            }
            else{
                $("#image_"+i).css({"color":"#333333"});
            }

            if($('#citizenship_front'+i+'').val()=="")
            {
                alert("please select holder"+i+" citizenship Frontside to upload");
                $("#citizenship_front_"+i).css({"color":"#a94442"});
                // $( "#citizenship_front"+i).focus();
                awesomeScrolling("citizenship_front"+i);
                return false;
            }
            else{
                $("#citizenship_front_"+i).css({"color":"#333333"});
            }
            if($('#citizenship_back'+i+'').val()=="")
            {
                alert("please select holder"+i+" citizenship backside to upload");
                $("#citizenship_front_"+i).css({"color":"#a94442"});
                // $( "#citizenship_front"+i).focus();
                awesomeScrolling("citizenship_front"+i);
                return false;
            }
            else{
                $("#citizenship_front_"+i).css({"color":"#333333"});
            }
        }
                    return true;
    }


        function validate_form_two(){

            for(i=1;i<=account_count;i++)
            {
            if(!$('#depositer_name'+i+'').val()=="")
                
            
                if($('#nominee_image'+i+'').val()=="")
                {
                    alert("Please select Nominee holder"+i+" Image to upload");
                    $("#nominee_image"+i).css({"color":"#a94442"});
                awesomeScrolling("nominee_image"+i);
                return false;
            }
            

           else if($('#nominee_citizenship_front'+i+'').val()=="")
            {
                alert("Please select Nomineeholder"+i+" citizenship Frontside to upload");
                $("#nominee_citizenship_front"+i).css({"color":"#a94442"});
                awesomeScrolling("nominee_citizenship_front"+i);
                return false;
            }
            
           else if($('#nominee_citizenship_back'+i+'').val()=="")
            {
                alert("Please select Nominee holder"+i+" citizenship backside to upload");
                $("#nominee_citizenship_back"+i).css({"color":"#a94442"});
                // $( "#citizenship_front"+i).focus();
                awesomeScrolling("nominee_citizenship_back"+i);
                return false;
            }
            else{
                $("#nominee_citizenship_back"+i).css({"color":"#333333"});
            }
        }
        
        
            
               if(!$("input[name='debit_card']:checked").val()){
                        alert("Please check debit card service");
                        $("#debit_card_").css({"color":"#a94442"});
                // $( "#debit_card").focus();
                awesomeScrolling("debit_card");
                return false;
            }
            else{
                $("#debit_card_").css({"color":"#333333"});
            }
    
            if(!$("input[name='mobile_banking_services']:checked").length>0){                 
                alert("please check mobile banking service");
                $("#mobile_banking_services_").css({"color":"#a94442"});
                // $( "#mobile_banking_services").focus();
                awesomeScrolling("mobile_banking_services_");
                return false;
            }
            else{
                $("#mobile_banking_services_").css({"color":"#333333"});                    
                if(!$("input[name='is_banking_password']:checked").length > 0){
                    alert("Please check mobile banking services option");
                    $("#mobile_banking_services_password").css({"color":"#a94442"});
                    awesomeScrolling("mobile_banking_services_password");
                    return false;
                }else{
                    $("#mobile_banking_services_password").css({"color":"#333333"});
                }   
            }
            if(!$("input[name='internet_banking_services']:checked").length>0){
                alert("please check internet banking service");
                $("#internet_banking_services_").css({"color":"#a94442"});
                // $( "#internet_banking_services").focus();
                awesomeScrolling("internet_banking_services");
                return false;
            }
            else{
                $("#internet_banking_services_").css({"color":"#333333"});                   
                if(!$("input[name='is_internet_banking_password']:checked").length > 0){
                    alert("Please check Internet banking services option");
                    $("#internet_banking_services_password").css({"color":"#a94442"});
                    awesomeScrolling("internet_banking_services_password");
                    return false;
                }else{
                    $("#internet_banking_services_password").css({"color":"#333333"});
                }
            }            
            if($("input[name='other_service']:checked").val()){  
                if($("#otherservice_name").val()==""){
                    alert("Please specify other service");
                    $("#otherserviceName").css({"color":"#a94442"});
                    awesomeScrolling("otherserviceName");
                    return false;
                }
                
            }
            else{
                $("#otherserviceName").css({"color":"#333333"});
            }
        return true;
    }

    function validate_form_three(){
        if($('#expected_monthly_turnover option:selected').val()==""){
            alert("Please select expected annual turnover option");
            $("#expected_monthly_turnover").css({"border-color":"#a94442"});
            awesomeScrolling("expected_monthly_turnover");
            return false;
        }
        else{
            $("#expected_monthly_turnover").css({"border-color":"#8dcfb7"});
        }

        if($('#expected_monthly_transaction option:selected').val()==""){
            alert("Please select Expected Annual Transaction option");
            $("#expected_monthly_transaction").css({"border-color":"#a94442"});
            awesomeScrolling("expected_monthly_transaction");
            return false;
        }
        else{
            $("#expected_monthly_transaction").css({"border-color":"#8dcfb7"});
        }

        if($('#purpose_of_account option:selected').val()==""){
            alert("Please select Purpose of Account option");
            $("#purpose_of_account").css({"border-color":"#a94442"});
            awesomeScrolling("purpose_of_account");
            return false;
        }
        else{
            $("#purpose_of_account").css({"border-color":"#8dcfb7"});
        }

        if($('#source_of_fund option:selected').val()==""){
            alert("Please select Source of Fund option");
            $("#source_of_fund").css({"border-color":"#a94442"});
            awesomeScrolling("source_of_fund");
            return false;
        }
        else{
            $("#source_of_fund").css({"border-color":"#8dcfb7"});
        }
    }

    function validate_form_four(){


        if(!$("input[name='general_rules']:checked").val()){
            alert("Please approve General Rules");
            $("#general_rules_").css({"color":"#a94442"});
            awesomeScrolling("general_rules_radio");
            return false;
        }
        else{
            $("#general_rules_").css({"color":"#333333"});
        }
    }
</script>

<script type="text/javascript">
    function IdentityChangeFunction(valID)
    {
                // alert(password_citizenship);
        updateLableForIdentity(valID)
    }
    
    function labelChangebykeyUp(labelId)
    {
        updateLableForIdentity(labelId)
    }
    
    function updateLableForIdentity(ID)
    {
        var password_citizenship = $("#passport_citizenship"+ID+" option:selected").val() || 'Not specified Identity';
        var name = $('#first_name'+ID).val() || 'Name not mentioned';
        $('#labelPhotoUpload'+ID).html(name);
        $('#citizenship_or_passport'+ID).html('<label class="if_add_more" >'+name+'('+password_citizenship+')</label>');
    }
</script>
                        
<script type="text/javascript">
        function showhide_section_passport(action){
           
            for(i=1;i<=account_count;i++){
             IdentityChangeFunction(i);
                if($("#passport_citizenship"+i+" option:selected").val()=="passport")
                {  
                    $("#if_passport"+i).show();
                    $("#passport_no"+i).prop('required',true);

                    $("#if_citizenship"+i).hide();
                    $("#citizenship_no"+i).val('');
                    $("#citizenship_no"+i).prop('required',false);

                    $("#if_nrn_card"+i).hide();
                    $("#nrn_card_no"+i).val('');
                    $("#nrn_card_no"+i).prop('required',false);

                    $("#if_birth_certificate"+i).hide();
                    $("#birth_certificate_no"+i).val('');
                    $("#birth_certificate_no"+i).prop('required',false);

                    $("#if_other_id"+i).hide();
                    $("#other_id"+i).val('');
                    $("#other_id"+i).prop('required',false);
                }
                else if($("#passport_citizenship"+i+" option:selected").val()=="citizenship")
                {    
                $("#if_passport"+i).hide();
                $("#passport_no"+i).val('');
                $("#passport_no"+i).prop('required',false);

                $("#if_nrn_card"+i).hide();
                $("#nrn_card_no"+i).val('');
                $("#nrn_card_no"+i).prop('required',false);

                $("#if_birth_certificate"+i).hide();
                $("#birth_certificate_no"+i).val('');
                $("#birth_certificate_no"+i).prop('required',false);


                $("#if_other_id"+i).hide();
                $("#other_id"+i).val('');
                $("#other_id"+i).prop('required',false);


                $("#if_citizenship"+i).show();
                $("#citizenship_no"+i).prop('required',true);

            }
            else if($("#passport_citizenship"+i+" option:selected").val()=="nrn_card")
            {    
                $("#if_passport"+i).hide();
                $("#passport_no"+i).val('');
                $("#passport_no"+i).prop('required',false);

                $("#if_citizenship"+i).hide();
                $("#citizenship_no"+i).val('');
                $("#citizenship_no"+i).prop('required',false);

                $("#if_birth_certificate"+i).hide();
                $("#birth_certificate_no"+i).val('');
                $("#birth_certificate_no"+i).prop('required',false);


                $("#if_other_id"+i).hide();
                $("#other_id"+i).val('');
                $("#other_id"+i).prop('required',false);



                $("#if_nrn_card"+i).show();
                $("#nrn_card"+i).prop('required',true);
            }

            else if($("#passport_citizenship"+i+" option:selected").val()=="birth_certificate")
            {    
                $("#if_passport"+i).hide();
                $("#passport_no"+i).val('');
                $("#passport_no"+i).prop('required',false);

                $("#if_citizenship"+i).hide();
                $("#citizenship_no"+i).val('');
                $("#citizenship_no"+i).prop('required',false);

                $("#if_nrn_card"+i).hide();
                $("#nrn_card"+i).val('');
                $("#nrn_card"+i).prop('required',false);

                $("#if_other_id"+i).hide();
                $("#other_id"+i).val('');
                $("#other_id"+i).prop('required',false);


                $("#if_birth_certificate"+i).show();
                $("#birth_certificate"+i).prop('required',true);
            }

            else if($("#passport_citizenship"+i+" option:selected").val()=="others")
            {    
                $("#if_passport"+i).hide();
                $("#passport_no"+i).val('');
                $("#passport_no"+i).prop('required',false);

                $("#if_citizenship"+i).hide();
                $("#citizenship_no"+i).val('');
                $("#citizenship_no"+i).prop('required',false);

                $("#if_nrn_card"+i).hide();
                $("#nrn_card"+i).val('');
                $("#nrn_card"+i).prop('required',false);

                $("#if_birth_certificate"+i).hide();
                $("#birth_certificate_no"+i).val('');
                $("#birth_certificate_no"+i).prop('required',false);

                $("#if_other_id"+i).show();
                $("#other_id"+i).prop('required',true);


            }

            else {
                $("#if_citizenship"+i).hide();
                $("#if_passport"+i).hide();
                $("#if_nrn_card"+i).hide();
                $("#if_birth_certificate"+i).hide();
                $("#if_other_id"+i).hide();
            }
        }
        IdentityChangeFunction(i);
    }

    function showhide_section(action){
        if(action=="single")
            {    $(".minor_input").prop('required',false);
        $("#if_minor").hide();
        $("#age_of_minor").val('');
        $("#name_of_guardian").val('');
        $("#relationship_minor").val('');
            $(".addmore_account_btn").show();
            $("#jointDet").hide();
        }

        else if(action=="joint")
            {    $(".minor_input").prop('required',false);
        $("#if_minor").hide();
        $("#age_of_minor").val('');
        $("#name_of_guardian").val('');
        $("#relationship_minor").val('');
            $(".addmore_account_btn").show();
            
            $("#jointDet").show();
        }

        else
        {     
            $(".minor_input").prop('required',true);
            $("#if_minor").show();
            $('.added_account').remove();
            account_count=1;
            $(".if_add_more").hide();
            $(".addmore_account_btn").hide();
            $("#jointDet").hide();
        }
    }
</script>

<script type="text/javascript">
    function showhide_section_others(action){

    for(i=1;i<=account_count;i++){
        if($("#occupation"+i+" option:selected").val()=="others")
        {  
                 $("#if_services"+i).hide();
                $("#occupation_service_company"+i).prop('required',false);
                $("#occupation_service_address"+i).prop('required',false);
                $("#occupation_service_position"+i).prop('required',false);
                $("#if_others"+i).show();
                $("#occupation_input"+i).prop('required',true);
            }
        else if($("#occupation"+i+" option:selected").val()=="service"){

             $("#if_others"+i).hide();

             $(".occupation_input").prop('required',false);

                $("#if_services"+i).show();
        }
            else
            {    
            $("#if_others"+i).hide();
            $("#occupation_input"+i).val('');

            $("#if_services"+i).hide();
            $("#occupation_service_company"+i).val('');
            $("#occupation_service_address"+i).val('');
            $("#occupation_service_position"+i).val('');
        }
    }
}
</script>

<script type="text/javascript">
    function convert_into_nepali(index){
        var year = $("#y"+index).find(":selected").text();
        var month = $("#m"+index).find(":selected").text();
        var day = $("#d"+index).find(":selected").text();

        if((year != '' || year != null) && (month != '' || month != null) && (day != '' || day != null)){
            var endDate = calendarFunctions.getBsDateByAdDate(parseInt(year), parseInt(month), parseInt(day));
            console.log(endDate);
            // var date = new Date(endDate);

            var bs_year = endDate.bsYear; 
            var bs_month = endDate.bsMonth; 
            var bs_day = endDate.bsDate; 
            if(bs_year != '' && bs_month != '' && bs_day != '')
            {
                $("#yn"+index).val(bs_year);
                $("#mn"+index).val(bs_month);
                $("#dn"+index).val(bs_day);
                var d = new Date();
                var userday = day;
                var usermonth = month;
                var useryear = year;            
                var curday = d.getDate();
                var curmonth = d.getMonth()+1;
                var curyear = d.getFullYear();            
                var age = curyear - useryear;    
                console.log(age);       
                if((curmonth < usermonth) || ( (curmonth == usermonth) && curday < userday   )){            
                    age--;            
                }
                if(age<18){
                    alert("You are a minor. please fill in the minor details.");
                    $(".minor_input").prop('required',true);
                    $("#if_minor").show();
                    $('.added_account').remove();
                    account_count=1;
                    //   $(".if_joint").hide();
                    $(".if_add_more").hide();
                    $(".addmore_account_btn").hide();
                    $("#minor_checked").prop('checked',true);
                    awesomeScrolling("account_type");       
                     
                }
            }
        }
        
        return false;
        
                
    }

    function convert_into_english(index){
        var year = $("#yn"+index).find(":selected").text();
        var month = $("#mn"+index).find(":selected").text();
        var day = $("#dn"+index).find(":selected").text();
        // var endDate= '';
        if((year != '' || year != null) && (month != '' || month != null) && (day != '' || day != null)){
            var endDate = calendarFunctions.getAdDateByBsDate(parseInt(year), parseInt(month), parseInt(day));

            var date = new Date(endDate);

            var ad_year = date.getFullYear(); 
            var ad_month = date.getMonth() +1; 
            var ad_day = date.getDate(); 
            if(ad_year != '' && ad_month != '' && ad_day != '')
            {
                $("#y"+index).val(ad_year);
                $("#m"+index).val(ad_month);
                $("#d"+index).val(ad_day);
                var d = new Date();
                var userday = ad_day;
                var usermonth = ad_month;
                var useryear = ad_year;            
                var curday = d.getDate();
                var curmonth = d.getMonth()+1;
                var curyear = d.getFullYear();            
                var age = curyear - useryear;           
                if((curmonth < usermonth) || ( (curmonth == usermonth) && curday < userday   )){            
                    age--;            
                }
                if(age<18){
                    alert("You are a minor. please fill in the minor details.");
                    $(".minor_input").prop('required',true);
                    $("#if_minor").show();
                    $('.added_account').remove();
                    account_count=1;
                    //   $(".if_joint").hide();
                    $(".if_add_more").hide();
                    $(".addmore_account_btn").hide();

                    $("#minor_checked").prop('checked',true);
                    awesomeScrolling("account_type");       
                     
                }
            }
        }
        
        return false;
    }
</script>

<script type="text/javascript">
    function showhide_section_existing_relation(action){
        for(i=1;i<=account_count;i++){
            var existing_relation=$("#existing_relation"+i+":checked").val();
            if($("#existing_relation"+i+":checked").val()=='yes'){
                $("#if_relation_yes"+i).show();

                $("#account_number"+i).prop('required',true);
            }else{
                $("#if_relation_yes"+i).hide();
                $("#account_number"+i).val('');
                $("#account_number"+i).prop('required',false);
            }
        }
    }
</script>

<script type="text/javascript">
    function individual_section(action){
        for(i=1;i<=account_count;i++){
        var individual_section=$("#types_of_individual:checked").val();
               
                if($("#types_of_individual:checked").val()=='NRN'){
                   $('#passport_citizenship1  option[value="nrn_card"]').attr('selected','selected');
                    $("#if_nrn_card1").show();
            
                      $("#nrn_card1").prop('required',true);
                      
                      
                    
                 $("#if_passport"+i).hide();
                $("#passport_no"+i).val('');
                $("#passport_no"+i).prop('required',false);
                

                $("#if_citizenship"+i).hide();
                $("#citizenship_no"+i).val('');
                $("#citizenship_no"+i).prop('required',false);

               

                $("#if_birth_certificate"+i).hide();
                $("#birth_certificate_no"+i).val('');
                $("#birth_certificate_no"+i).prop('required',false);

                $("#if_other_id"+i).hide();
                $("#other_id"+i).val('');
                $("#other_id"+i).prop('required',false);
                      
                      
                      
                // console.log($("#existing_relation"+i).val());
                
            }
            else{
                 $('#passport_citizenship1').prop('selectedIndex',0);
                   $("#if_nrn_card1").hide();
            $("#nrn_card_no1").val('');
            $("#nrn_card_no1").prop('required',false);
            
              $("#if_passport"+i).hide();
                $("#passport_no"+i).val('');
                $("#passport_no"+i).prop('required',false);
                

                $("#if_citizenship"+i).hide();
                $("#citizenship_no"+i).val('');
                $("#citizenship_no"+i).prop('required',false);

               

                $("#if_birth_certificate"+i).hide();
                $("#birth_certificate_no"+i).val('');
                $("#birth_certificate_no"+i).prop('required',false);

                $("#if_other_id"+i).hide();
                $("#other_id"+i).val('');
                $("#other_id"+i).prop('required',false);
                      
            }
     } 
    }
</script>

<script type="text/javascript">
    function showhide_section_minor(action){
        if($('#account_name option:selected').val()==" Citizens Sharedhani Bachat Khata")
            {
                $('#acc_shareholder').show();
            }
            else{
                $('#acc_shareholder').hide();

            }
            
            if($('#account_name option:selected').val()=="Citizens Muna Bachat")
            {  
                $("#minor_checked").prop('checked',true);
                showhide_section("minor");
            }
            
            else if($('#account_name option:selected').val()=="Citizens Rastra Sewak Saving")
            {
                for(i=1;i<=account_count;i++){
                    $("#phone_number"+i).prop('required',true);
                }
            }
            else
                {    $(".minor_input").prop('required',false);
            $("#if_minor").hide();
                $(".addmore_account_btn").show();
            }
            var num=$("#account_name_count").val();
            
            var selected_option = $('#account_name option:selected').val();
            var csrf_ctzn_token = $('#csrf').val();
            $.ajax({
              url: "{{ "account_detail/getIntrestrate" }}",
              type: 'post',
              data:{name : selected_option,
                csrf_ctzn_token : csrf_ctzn_token},
                dataType: "json",
                cache: false,
                success: function(data) {
                  $('#intrest_rate .added_intrestrate').last().remove();
                  $('#intrest_rate').append('<div class="form-group added_intrestrate">'+
                    '<div class="col-sm-4 no-padding">'+
                    '<label class="control-label">Fixed Rate :'+data["fixed_rate"]+' <a href="http://pagodalabs.com.np/citizens/account/detail/'+data["slug_name"]+'">See More</a></label>'+
                    '</div>'+
                    '</div>');
            }
        });
    }
</script>

<script>
    function validate_image_upload(index){
        var filee = ("#image"+index);
        var val = $(filee).val().toLowerCase(),
        regex = new RegExp("(.*?)\.(gif|jpg|png|jpeg)$");

        if (!(regex.test(val))) {
            $(filee).val('');
            alert('Please select correct file format for Image upload');
        }


    }

    function validate_citizenship_front(index){
        var filee = ("#citizenship_front"+index);
        var val = $(filee).val().toLowerCase(),
        regex = new RegExp("(.*?)\.(gif|jpg|png|jpeg|pdf)$");

        if (!(regex.test(val))) {
            $(filee).val('');
            alert('Please select correct file format for Image upload');
        }


    }

    function validate_citizenship_back(index){
        var filee = ("#citizenship_back"+index);
        var val = $(filee).val().toLowerCase(),
        regex = new RegExp("(.*?)\.(gif|jpg|png|jpeg|pdf)$");

        if (!(regex.test(val))) {
            $(filee).val('');
            alert('Please select correct file format for Citizenship Back upload');
        }


    }
    
    function validate_nominee_image_upload(index){
        var filee = ("#nominee_image"+index);
        var val = $(filee).val().toLowerCase(),
        regex = new RegExp("(.*?)\.(gif|jpg|png|jpeg)$");

        if (!(regex.test(val))) {
            $(filee).val('');
            alert('Please select correct file format for Image upload');
        }


    }

    function validate_nominee_citizenship_front(index){
        var filee = ("#nominee_citizenship_front"+index);
        var val = $(filee).val().toLowerCase(),
        regex = new RegExp("(.*?)\.(gif|jpg|png|jpeg|pdf)$");

        if (!(regex.test(val))) {
            $(filee).val('');
            alert('Please select correct file format for Image upload');
        }


    }

    function validate_nominee_citizenship_back(index){
        var filee = ("#nominee_citizenship_back"+index);
        var val = $(filee).val().toLowerCase(),
        regex = new RegExp("(.*?)\.(gif|jpg|png|jpeg|pdf)$");

        if (!(regex.test(val))) {
            $(filee).val('');
            alert('Please select correct file format for Citizenship Back upload');
        }


    }


</script>

<script type="text/javascript">
    function showhide_mobile_banking(action){
    
        var mobile_banking_services=$("#mobile_banking_services:checked").val();
               
        if($("#mobile_banking_services:checked").val()==1){
            $("#if_mobile_banking_yes").show();

            $("#is_banking_password").prop('required',true);
        }else{
            $("#if_mobile_banking_yes").hide();
            $("#is_banking_password").val('');
            $("#is_banking_password").prop('required',false);
        }
    }

    function showhide_internet_banking(action){
    
        var internet_banking_services=$("#internet_banking_services:checked").val();
               
        if($("#internet_banking_services:checked").val()==1){
            $("#if_internet_banking_yes").show();

            $("#is_internet_banking_password").prop('required',true);
        }else{
            $("#if_internet_banking_yes").hide();
            $("#is_internet_banking_password").val('');
            $("#is_internet_banking_password").prop('required',false);
        }
        
    }

    function showhide_otherservice(action){
        var other_service=$("#other_service:checked").val();
               
        if($("#other_service:checked").val()==1){
            $("#if_otherservice_yes").show();

            $("#otherservice_name").prop('required',true);
        }else{
            $("#if_otherservice_yes").hide();
            $("#otherservice_name").val('');
            $("#otherservice_name").prop('required',false);
            
        }
    }
</script>

<script type="text/javascript">
    var depositer_count = 1;
    var daughter_count=1;
    var son_count=1;
    var daughter_in_law_count=1;

    function add_depositer(){
        $('.if_add_more').show();
        var array_index = depositer_count;
        depositer_count++;

        $("#depositer_field").append('<div class="added_depositer">'+
            '<h4>Nominee'+depositer_count+'</h4>'+
            '<div class="row all-round-border check_options">'+
            '<div class=holder-section>'+
            
            '<div class="col-sm-2">'+
            '<div class="col-sm-6 no-padding">'+
            '<label>Initials</label>'+
            '</div>'+
            '<div class="col-sm-6 no-padding">'+
            '<select  class="styled-select slate" id="mr_miss'+array_index+'" name="mr_miss['+array_index+']">'+
            '<option value="">Select</option>'+
            '<option value="Mr.">Mr.</option>'+
            '<option value="Mr.">Ms.</option>'+
            '<option value="Mrs.">Mrs.</option>'+
            ' <option value="others.">others</option>'+
            '</select>'+
            '</div>'+
            '</div>'+
            '<div class="col-sm-5">'+
            '<div class="form-group">'+
            '<div class="col-sm-2 no-padding">'+
            '<label class="control-label">Name</label>'+
            '</div>'+
            '<div class="col-sm-10 no-padding">'+
            '<input maxlength="100" type="text" name="depositer_name[]" id="depositer_name'+depositer_count+'" class="form-control" placeholder="Name" >'+
            '</div>'+
            '</div>'+
            '</div>'+

            '<div class="col-sm-5">'+
            '<div class="form-group">'+
            '<div class="col-sm-4 no-padding">'+
            '<label class="control-label">Relationship</label><br/>'+
            '</div>'+
            '<div class="col-sm-8 no-padding">'+
            '<select  class="styled-select slate"  name="relationship[]" id="relationship'+depositer_count+'" class="col-sm-4">'+
            '<option value="">Select</option>'+
            '<option value="Father">Father</option>'+
            '<option value="Mother">Mother</option>'+
            '<option value="Spouse">Spouse</option>'+
            '<option value="Son">Son</option>'+
            '<option value="Daughter">Daughter</option>'+
            '<option value="Others">Others</option>'+
            '</select>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>'+

            '<div class="row all-round-border check_options">'+
            '<div class="col-md-12">'+
            '<div class="steps-sub-title">'+
            '<h4>Nominee Photo Uploads</h4>'+
            '</div>'+
            '</div>'+
            '<div class="col-sm-12">'+
            '<div class="form-group">'+
            '<div class="row">'+
            '<div id="nominee_photo_uploads" class="repeat-holder-item-container all-uploads">'+
            '<div class="col-md-4 col-sm-4 repeat-holder-item">'+
            '<label class="if_add_more">Nominee'+depositer_count+'</label>'+
            '<input type="file" onchange="validate_nominee_image_upload('+depositer_count+')" name="nominee_image'+array_index+'" id="nominee_image'+depositer_count+'" size="20" />'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '<div class="note">'+
            '<p>(Allowed formats: GIF,JPG,PNG,JPEG)</p>'+
            '</div>'+
            '</div>'+
            '</div>'+

            '<div class="row all-round-border check_options">'+
            '<div class="col-md-12">'+
            '<div class="steps-sub-title">'+
            '<h4>Nominee Citizenship Uploads</h4>'+
            '</div>'+
            '</div>'+
            '<div class="col-sm-12">'+
            '<div class="form-group">'+
            '<div class="row">'+
            '<div id="nominee_citizenship_uploads" class="repeat-holder-item-container all-uploads">'+
            '<div class="col-sm-12 repeat-holder-item">'+
            '<label class="if_add_more">Nominee'+depositer_count+'</label>'+
            '<div class="row">'+
            '<div class="col-md-4">'+
            '<label>Front: </label>'+
            '<input type="file" onchange="validate_nominee_citizenship_front('+depositer_count+')" name="nominee_citizenship_front'+array_index+'" id="nominee_citizenship_front'+depositer_count+'" size="20" />'+
            '</div>'+
           /* '<label>Back:</label>'+
            '<input type="file" onchange="validate_nominee_citizenship_back('+depositer_count+')" name="nominee_citizenship_back'+array_index+'" id="nominee_citizenship_back'+depositer_count+'" size="20" />'+
            
            '</div>'+*/
            '<div class="col-md-4">'+
            '<label>Back:</label>'+
            '<input type="file" onchange="validate_nominee_citizenship_back('+depositer_count+')" name="nominee_citizenship_back'+array_index+'" id="nominee_citizenship_back'+depositer_count+'" size="20" />'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '<div class="note">'+
            '<p>(Allowed formats: GIF,JPG,PNG,JPEG)</p>'+
            '</div>'+
            '</div>'+
            '</div>'+

            '</div>');
    }


    function remove_depositer(){

        if(depositer_count>1)
        {
            $("#depositer_field .added_depositer").last().remove();
            depositer_count--;
        }
    }


    function add_daughter(){
        daughter_count++;
        $('#daughter_name_field').append('<div class="added_daughter">'+
            '<div class="col-sm-6 col-sm-offset-4 col-xs-8"><input class="form-control capital_letter"  maxlength="100" type="text" name="daughter_name[]" id="daughter_name'+daughter_count+'" placeholder="Daughter Name" /></div>'+
            '</div>');

        $('#remove_daughter_button').css('display','block');

    }


    function remove_daughter(){
        if(daughter_count>1){
            $('#daughter_name_field .added_daughter').last().remove();
            daughter_count--;
        }
        if(daughter_count == 1){
            $('#remove_daughter_button').hide();
        }
    }

    function add_son(){
        son_count++;
        $('#son_name_field').append('<div class="added_son">'+
            '<div class="col-sm-6 col-sm-offset-4 col-xs-8">'+
            '<input class="form-control capital_letter"  maxlength="100" type="text" name="son_name[]" id="son_name'+son_count+'" placeholder="Son Name"/></div>'+
            '</div>');
        $('#remove_son_button').show();
    }


    function remove_son(){
        if(son_count>1){
            $('#son_name_field .added_son').last().remove();
            son_count--;
        }
        if(son_count == 1){
            $('#remove_son_button').hide();
        }
    }

    function add_daughter_in_law(){
        daughter_in_law_count++;
        $('#daughter_in_law_name_field').append('<div class="added_daughter_in_law">'+
            '<div class="col-sm-6 col-sm-offset-4 col-xs-8">'+
            '<input class="form-control capital_letter"  maxlength="100" type="text" name="daughter_in_law_name[]" id="daughter_in_law_name'+daughter_in_law_count+'" placeholder="Daughter in law name"  /></div>'+
            '</div>');
        $('#remove_daughter_in_law_button').show();
    }

    function remove_daughter_in_law(){
        if(daughter_in_law_count>1){
            $('#daughter_in_law_name_field .added_daughter_in_law').last().remove();
            daughter_in_law_count--;
        }
        if(daughter_in_law_count == 1)
        {
            $('#remove_daughter_in_law_button').hide();
        }
    }
</script>

<script type="text/javascript">
    function getPurposeofAction(action){
    
        if($("#purpose_of_account option:selected").val()=="Others"){ 
            $('#purposeOthers').show();  
            var purpose_other;
            purpose_other = 
                '<div class="col-md-6 col-sm-6">'+
                     '<div class="form-group">'+
                         '<div class="col-sm-4 no-padding">'+
                             '<label class="control-label"  id="purpose_of_fund_input_">Others</label>'+
                        ' </div>'+
                        '<div class="col-sm-8 no-padding" >'+
                         '<input type="text"  name="purpose_of_fund_input" id="purpose_of_fund_input" class="form-control purpose_of_fund_input" placeholder="Please specify the Purpose of Fund"/>'+
                         '</div>'+
                      '</div>'+
                '</div>';
            $('#purposeOthers').html(purpose_other);         
        }else{
            $('#purposeOthers').html();
            $('#purposeOthers').hide();
        }
    }
</script>

<script type="text/javascript">
    function getSourceofAction(action){
        if($("#source_of_fund option:selected").val()=="Others"){ 
            $('#sourceOthers').show();  
            var source_other;
            source_other = 
                '<div class="col-md-6 col-sm-6">'+
                     '<div class="form-group">'+
                         '<div class="col-sm-4 no-padding">'+
                             '<label class="control-label"  id="source_of_fund_input_">Others</label>'+
                        ' </div>'+
                        '<div class="col-sm-8 no-padding" >'+
                         '<input type="text"  name="source_of_fund_input" id="source_of_fund_input" class="form-control source_of_fund_input" placeholder="Please specify the Source of Fund"/>'+
                         '</div>'+
                      '</div>'+
                '</div>';
            $('#sourceOthers').html(source_other);         
        }else{
             $('#sourceOthers').html();
             $('#sourceOthers').hide();
        }
    }
</script>

@endsection