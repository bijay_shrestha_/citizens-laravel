@extends('front.layout.main.master')

@section('content')

<div class="rates-page list-all-page">
    <section class="home-slider">
        <div class="bannerSlider">
        	    @foreach($banners as $banner)

	                <div class="bannerItem bannerItemInside {{$banner['design']}}" style="background-image: url('{{url('uploads/banner/'.$banner['image_name'])}}')">
	                
	                 <div class="text-container">
	                    <div class="text-inner">
	                         <div class="right-img">
	                            
	                            <img src="{{url('/')}}/uploads/banner/{{$banner['logo']}}">
	                        </div>
	                        <div class="right-text {{$banner['color']}} bottom-{{$banner['bottom-color']}} " >
	                            <h1 >{!! $banner['top_box_description'] !!}</h1>
	                            <p>{!! $banner['bottom_box_description'] !!}</p>
	                        </div>
	                    </div>
	                </div>  
	                 @if($banner['type']==1)
	                <div class="banner-water-mark">
	                 
	                  <a href="{{$banner['freepik_link']}}" target="_blank">Link for image</a>                               
	                </div>
	                @else
	                @endif

	            </div>
	            @endforeach
        </div>
    </section>

    <section class="page-content-area">
        <div class="outer-custom-container">
            <div class="left-big-contianer">
                <!-- Nav Tabs Top-->
                  
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#forex" role="tab" data-toggle="tab">FOREX</a></li>
     

  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
   <div role="tabpanel" class="tab-pane fade in active" id="interest">
        
        <div class="table-responsive">
            <table class="table">  
            <thead>
            
                <tr class="info">
                    <th>Currency</th>
                    <th>Units</th>
                    <th>Buying Cash Below Deno 50</th>
                    <th> Buying</th>
                    <th>Selling</th>
                    <th>Date and Time</th>
                </tr>
            </thead>
            <tbody>
              @foreach($forexs as $forex)
            <tr>
             
                    <td>{{$forex['currency']}}</td>
                    <td>{{$forex['unit']}}</td>
                    <td>{{$forex['cash_buying']}}</td>
                    <td>{{$forex['buying']}}</td>
                    <td>{{$forex['selling']}}</td>
                    <td>{{$forex['added_date']}}</td>
            
                    
                </tr>
                    @endforeach
            </tbody>
           
        </table>
        
        
       </div>
       
    </div>
  <p><b>Disclaimer: </b>The rates displayed in the web-site are indicative and for information purpose only. The rates may change any time during the business hour without prior information. The bank bears no liability to any financial loss due to fluctuation of foreign exchange rates.</p>
</div>
    
            </div>
        </div>
    </section>
</div>

@endsection