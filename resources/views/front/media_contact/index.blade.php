@extends('front.layout.main.master')
@section('content')
<div class="list-all-page">
      <section class="home-slider">
        <div class="bannerSlider">
            @foreach($banners as $banner)
                 <div class="bannerItem bannerItemInside {{$banner['design']}}" style="background-image: url('{{url('uploads/banner/'.$banner['background'])}}')">
                
                 <div class="text-container">
                    <div class="text-inner">
                         <div class="right-img">
                            
                            <img src="{{url('/')}}/uploads/banner/{{ $banner['logo'] }}">
                        </div>
                         <div class="right-text {{$banner['color']}} bottom-{{ $banner['bottom-color']}} " >
                            <h1 >{!! $banner['top_box_description'] !!}</h1>
                            <p>{!! $banner['bottom_box_description'] !!}</p>
                        </div>
                    </div>
                </div>  
                
            </div>
            @endforeach
        </div>
    </section>

    <section class="page-content-area">
        <div class="outer-custom-container">
            <div class="left-big-contianer">
                <div class="section-title" style="margin-bottom: 0px;">
                    <div class="icon-container">
                        <img src="{{asset('/frontcss/img/blog-img.png')}}">
                        {{-- <img src="{{theme_url()}}assets/img/blog-img.png"> --}}
                    </div>
                    <div class="text-container">
                        <h1>Media Contact</h1>
                    </div>
                </div>

                <div class="content-container">
                    <div class="section-content" style="padding-left: 75px;">
                        @if($mediacontacts==NULL)
                                <p>Content is coming soon!!!!</p>
                                @else
                        
                        @foreach($mediacontacts as $blog)
                            <div class="show-all-item">
@if($blog['blog_title'])
                                <div class="title">
                                    <div class="text-container">
                                        <h3>{{$blog['blog_title']}}</h3>
                                    </div>
                                </div>
@endif

                                {{-- <div class="date">
                                    <i class="fa fa-calendar" aria-hidden="true"> </i>
                                    <p>{{$blog['post_date']}}</p>
                                </div> --}}
                                <div class="content" style="padding-left: 0px;">                                   
                                    {!! $blog['contents'] !!}
                                    {{-- <a href="{{url('media_contacts/detail/'.$blog['blog_id'])}}">(more)...
                                    </a> --}}

                                </div>
                            </div>
                        @endforeach
                        @endif 
                    </div>

                       <div class="aside-right">
                        <div class="sidebar-content ">
                            

                            <div class="sidebar-item side-title">
                                <span><h1>Tools</h1></span>
                            </div>

                           

                            

                            <div class="sidebar-item">
                                <a href="{{url('feedbacks')}}">
                                    <span>
                                        
                                         <img src="{{asset('/frontcss/img/citizens icon _green-35.png')}}">


                                    </span>
                                    <span><h1>Grievance Handling</h1></span>
                                </a>
                            </div>


                            <div class="sidebar-item">
                                <a href="{{url('branches/showlocation/branch')}}">
                                    <span>
                                       <img src="{{asset('/frontcss/img/icons/hover-visit.png')}}">
                                        {{--    <img  src="{{theme_url()}}assets/img/icons/hover-visit.png"> --}}      
                                    </span>
                                    <span><h1>Visit Our Branch</h1></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--<div class="right-small-contianer">

            </div>-->
        </div>
    </section>
</div>

@endsection