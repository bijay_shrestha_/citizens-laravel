<div class="footer-wrapper" id="footer-wrapper">
    <div class="ci-side-menu">
	    <ul>
	        <!-- <li>-->
	        <!--       <a href="banking">-->
	        <!--        <div><i class="fa fa-bullhorn"></i></div>-->
	        <!--        <div>Electronic Banking</div>-->
	        <!--    </a>-->
	        <!--</li>-->
	         <li>
               <a href="announcement">
	                <div><i class="fa fa-bullhorn"></i></div>
	                <div>Notice</div>
	            </a>
	        </li>
	        
	        <li>
	            <a href="interest_rate">
	            <div>
	                <img src="{{ asset('frontcss') }}/img/icons/citizens_rate_blue.png" class="tool-img">
	            </div>
	            <div>Rate</div>
	            </a>
	        </li>
	        
	        <li>
	            <a href="home/calculator">
	            <div><i class="fa fa-calculator"></i></div>
	            <div>Calculator</div>
	            </a>
	        </li>

	        <li>
	                   <a href="forex">
	            <div><i class="fa fa-area-chart"></i></div>
	            <div>Forex</div>
	            </a>
	        </li>

	        

	        <!--<li>-->
	        <!--           <a href="branch/showLocation">-->
	        <!--    <div><i class="fa fa-map-marker"></i></div>-->
	        <!--    <div>Location</div>-->
	        <!--    </a>-->
	        <!--</li>-->
	
	        <!--  <li>-->
	        <!--           <a href="contact">-->
	        <!--    <div><i class="fa fa-commenting"></i></div>-->
	        <!--    <div>Grievance Handling </div>-->
	        <!--    </a>-->
	        <!--</li>-->

	       
	    </ul>
	</div>
    <div class="top-footer">
        <div class="footer-custom-container">
            <div class="footer-nav-block">
                <h1>Products</h1>
                <ul>
                    <li><a href="home/online">Online Services</a></li>
                    <li><a href="account">Deposit</a></li>
                    <li><a href="loan">Loan</a></li>
                    <li><a href="credit_card">Cards</a></li>
                    <li><a href="page/remit">Remitance</a></li>
                    <!--<li><a href="banking">Electronic Banking</a></li>-->
                    <li><a href="home/others">Other Services</a></li>
                     
                </ul>
            </div>

            <div class="footer-nav-block">
                <h1>Rates</h1>
                <ul>
                    <li><a href="interest_rate/index/ir">Interest Rates</a></li>
                    <li><a href="interest_rate/index/irs">Interest Rate Spread</a></li>
                    <li><a href="interest_rate/index/br">Base Rates</a></li>
                    <li><a href="interest_rate/index/ex">Exchange Rates</a></li>
                    <li><a href="interest_rate/index/fc">Fees and Commission</a></li>
                </ul>
            </div>

            <div class="footer-nav-block">
                <h1>Reports & Disclosures</h1>
                <ul>
                </ul>
            </div>
            <div class="footer-nav-block">
                <h1>News & Events</h1>
                <ul>
                    <li><a href="blog">News/Press Releases</a></li>
                    <li><a href="csr">CSR</a></li>
                    <li><a href="announcement"> Notices</a></li>
                </ul>
            </div>

            <div class="footer-nav-block">
                <h1>Account Opening</h1>
                <ul>
                    <li><a href="account/online_Account_opening">Online Account Opening</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="bottom-footer">
        <div class="footer-custom-container">
            <div class="text">
                Copyright © 2017 <b>Citizens Bank International Limited</b> All Rights Reserved
            </div>

            <div class="bf-social">
                <ul>
                    
                </ul>
            </div>
        </div>
    </div>
</div>