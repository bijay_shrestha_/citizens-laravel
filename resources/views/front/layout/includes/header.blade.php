<!DOCTYPE html>
<html>
<head>
   <title>citizen Bank</title>   
        <link rel="apple-touch-icon" sizes="57x57" href="{{url('/')}}/img/frontend/img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<{{url('/')}}/img/frontend/img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="{{url('/')}}/img/frontend/img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="{{url('/')}}/img/frontend//img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="{{url('/')}}/img/frontend/img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="{{url('/')}}/img/frontend/img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="{{url('/')}}/img/frontend/img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="{{url('/')}}/img/frontend/img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="{{url('/')}}/img/frontend/img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{url('/')}}/img/frontend/img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="{{url('/')}}/img/frontend/img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="{{url('/')}}/img/frontend/img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="{{url('/')}}/img/frontend/img/favicon/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/img/frontend/citizen/css/bootstrap.css">
  
    <!-- Font awesome -->
    <link rel="stylesheet" href="{{url('/')}}/img/frontend/citizen/css/font-awesome.min.css">


    <link href='http://fonts.googleapis.com/css?family=Raleway:500,600,700,400,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,900" rel="stylesheet">

    <!-- Components Start -->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/img/frontend/newcss/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/img/frontend/newcss/slick.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/img/frontend/newcss/jquery.mCustomScrollbar.min.css">
    <!-- Components Ends -->

    <!-- put at last -->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/img/frontend/css/global.css">
    <link rel="stylesheet" href="{{url('/')}}/img/frontend/css/bootstrap-slider.css">
    

    <link rel="stylesheet" type="text/css" href="{{url('/')}}/img/frontend/citizen/css/style.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/img/frontend/newcss/theStyles.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/img/frontend/css/responsive.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/img/frontend/newcss/responsive.css">
    <link rel="stylesheet" href="{{url('/')}}/img/frontend/css/nepaliDatePicker.css">

    <!-- javascript -->

    <script src="{{url('/')}}/img/frontend/js/modernizr.custom.js"></script>
    <script src="{{url('/')}}/img/frontend/js/jquery.js"></script>
    <script src="{{url('/')}}/img/frontend/js/jquery-ui.js"></script>
    <script src="{{url('/')}}/img/frontend/js/jquery.upload.js" type="text/javascript"></script>
    <script src="{{url('/')}}/img/frontend/js/jquery.mCustomScrollbar.min.js"></script>

   
     
     <script src="{{url('/')}}/img/frontend/js/bootstrap-notify.min.js" type="text/javascript"></script>
       

    <script src="{{url('/')}}/img/frontend/js/canvasjs.min.js"></script>
    
<!--<link class="jsbin" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />-->

<!--<script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>-->



<style>

.e-bank a{
  display:inline-block;
}
.e-bank span{
  float: left;
}

.e-bank span:nth-of-type(2){
  margin-top: 15px;
}
.wrapper .header-wrapper nav .container .collapse .navbar-nav .dropdown .reports li a,
.wrapper .header-wrapper nav .container .collapse .navbar-nav .dropdown .abtus li a,
.wrapper .header-wrapper nav .container .collapse .navbar-nav .dropdown .media li a,
.wrapper .header-wrapper nav .container .collapse .navbar-nav .dropdown .ebanking li a{
    padding: 0px;
}

/*.multi-bar-list {display:none; position: absolute; top:auto; left:auto;width: 500px;}
.multi-bar-list li{float: left; display:block; width: 300px; margin: 0; border-bottom: 1px solid #a4aebb;  text-align: left !important;}
.mutli-bar-list li:nth-child(last){
    border-bottom: none;
}




.multi-bar-list-details {display:none; position: absolute; top:auto; left:auto;width: 500px ; }

.multi-bar-list-details li{float: left; display:block; width: 400px; margin: 0; border-bottom: 1px solid #a4aebb;  text-align: left !important;}
.mutli-bar-list-details li:nth-child(last){
    border-bottom: none;
}
*/

.btn-success{
        background-color: #8dcfb7;
    border-color: #8dcfb7;
}
.btn-success:hover{
        background-color: #8dcfb7;
    border-color: #8dcfb7;
}
</style>


</head>

<body>

    <div class="wrapper" id="wrapper">
       
    <!---------------------- Header Wrapper Start ---------------------->
    <div class="header-wrapper" id="header-wrapper">

        <!-------------- Top Menu Start -------------->
        <div class="top-navigation-container">
            <div class="ci-custom-container">
                <div class="ci-top-toggle">
                    <div id="ci-burger" class="ci-burger"> 
                        <span>&nbsp;</span>
                        <span>&nbsp;</span>
                        <span>&nbsp;</span>
                        <span>&nbsp;</span>
                    </div>
                </div>
                
                <div class="ci-top-nav">
                    
                    <ul>
                        <li class="top-has-sub-menu">
                            <a href="#">About Us</a>
                            
                                    <ul>
                                            @if(getabout_us_menu())
                                        <li>
                                            
                                            <a href="{{ url(''.getabout_us_menu()->slug_name) }}">{{  getabout_us_menu()->page_title }} </a>
                                            
                                        </li>
                                        @endif
                                        {{--
                                        <!--@if(getoverview_menu())-->
                                        <!--<li><a  href="{{ url($overview_menu['slug_name']) }}"> {{  $overview_menu['page_title'] }} </a></li>-->
                                        <!--@endif
                                            --}}
                                       
                                                <li><a  href="{{ url('executive/index/board') }}">Board Members</a></li>
                                                <li><a  href="{{ url('executive/index/exec') }} ">Executive Team</a></li>
                                                
                                       
                                        
                                    </ul>
                          
                        </li>
                        
                        <li class="top-nav-blue">
                            <a href="https://www.cbilcapital.com/" target="_blank">CBIL Capital</a>
                        </li>
                        
                        <li class="top-has-sub-menu">
                            <a href="#">Media</a>
                            
                                    <ul>
                                        <li class="has-submenu">
                                            <a href="#">News and events</a>
                                            <ul>
                                                <li><a  href="{{ url('blog') }}">News/Press Releases</a></li>
                                                <li><a  href="{{url('csr') }}">CSR</a></li>
                                            </ul>
                                        </li>
                                        
                                        <li><a href=" {{ url('announcement') }}">Notices</a></li>
                                        <li><a href="{{ url('media_contact') }}">Media Contacts</a></li>
                                    </ul>
                          
                        </li>

                        <li class="top-has-sub-menu">
                            <a href="#">Connect</a>
                            <ul>
                                
                                
                                <!--<li>-->
                                <!--    <a href="{{ url('contact') }}">Contact Us</a>-->
                                <!--</li>-->
                                
                                 <li class="has-submenu">
                                    <a href="#">Contact Us</a>
                                    <ul>
                                        <!--<li><a href="{{ url('contact') }} ">Contact Us</a></li>-->
                                        
                                        @if(getspokeperson_menu())
                                        <li><a href=" {{ url(getspokeperson_menu()->slug_name) }}"><span> {{ getspokeperson_menu()->page_title }} </span></a></li>
                                        @endif
                                        @if(getinformation_office_menu())
                                        <li><a href="{{ url(getinformation_office_menu()->slug_name) }}"><span>{{  getinformation_office_menu()->page_title }}</span></a></li>
                                        @endif
                                        <li><a href="{{ url('feedback/index/gr') }} ">Grievance Handling</a></li>
                                        
                                        <!--<li><a href="{{ url('contact') }}">Contact Us</a></li>-->
                                        
                                        <!--<li><a href="{{ url('contact') }}"><span>General FAQ</span></a></li>-->
                                        
                                        
                                    </ul>
                                </li>
                                
                                <li class="has-submenu">
                                    <a href="#">Network</a>
                                    <ul>
                                        <li><a  href="{{ url('branches/showLocation/atm') }} ">ATM Network</a></li>
                                         <li><a  href="{{ url('branches/showLocation/branch') }}">Branch Network</a></li>
                                        
                                       <li><a href="{{ url('cr_networks') }}" >Correspondent Network</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
   
                        <li class="top-has-sub-menu">
                            <a href="#">Downloads</a>
                            <ul>
                                @if(getform_menu())
                                <li><a  href=" {{ url(getform_menu()->slug_name) }} ">{{  getform_menu()->page_title  }} </a></li>
                                @endif
                                <!--<li><a  href="{{ url('/') }}uploads/wysiwyg/gold2071.pdf" download>Gold Form</a></li>-->
                                <!--<li><a  href="{{ url('/') }}uploads/wysiwyg/silverform.pdf" download>Silver Form</a></li>-->
                                
                                <li><a  href="https://play.google.com/store/apps/details?id=com.f1soft.citizensmobilebanking" target = '_blank'>Citizens M-banking App</a></li>
                               
                               {{-- not done --}}
                                       @foreach(other_menu() as $menu)
                                           <li>
                                               @if($menu->type == 'select')
                                              <a href=" {{ url(''.$menu->link) }}">{{ $menu->title }}  </a>
                                               @else
                                                @if($menu->is_download == 1)
                                              <a href=" {{ url('uploads/wysiwyg/'.$menu->link) }}" download> {{ $menu->title  }} </a> 
                                                @else
                                               <a href="{{ $menu->link }} ">{{ $menu->title }} </a>
                                                @endif
                                                 @endif 
                                           </li>
                                       @endforeach
                                        
                                 
                        
                            </ul>
                        </li>
 
                        <li>
                            <a href="#" onclick="showvacancy()">Career</a>
                        </li>
                    </ul>
                </div>
                
                
                
                
                
            </div>
        </div>
        <!--------------- Top Menu End --------------->

        <!------------- Bottom Menu Start ------------>
       
        
        <div class="bottom-navigation-container">
            <div class="ci-custom-container">
                <div class="logo-container">
                    <a href="{{ url('home') }}">
                        <img src="{{url('/')}}/img/frontend/img/CB_logo.png" alt="logo">
                    </a>

                    <div class="ci-bottom-toggle">
                        <div id="ci-bottom-burger" class="ci-bottom-burger">
                            <span>&nbsp;</span>
                            <span>&nbsp;</span>
                            <span>&nbsp;</span>
                            <span>&nbsp;</span>
                        </div>
                    </div>
                </div>
                <?php
                    $product = array();
                    foreach(getAccounts() as $header)
                    {
                        $product[] = url('account/detail/'.$header->slug_name); 
                    }
                   
                     foreach(getLoans() as $header)
                    {
                        $product[] = url('loan/details/'.$header->slug_name); 
                    }
                    foreach(getCredit() as $header)
                    {
                        $product[] = url('credit_card/detail/'.$header->slug_name); 
                    }
                    foreach(getBanking() as $header)
                    {
                        $product[] = url('banking/details/'.$header->slug_name); 
                    }
                    
                    
                    $more = sizeof($product);
                    $product[] = url('page/remit/');
                    $product[] = url('notice');
                    $product[] = "http://www.ctznbank.com/kycform/";
                    $product[] = url('utility-paymen');
                    $product[] = url('paybill-facility');
                    $product[] = url('privilege');
                    $product[] = "http://www.ctznbank.com/OnlineCDS/";
                    $product[] = url('baking/details/1');
                    $product[] = url('baking/details/2');
                    if(getbranchless_menu()){
                    $product[] = url(''.getbranchless_menu()->slug_name);
                    }
                    // if($this->bancassurance_menu){
                    // $product[] = site_url($this->bancassurance_menu['slug_name']);
                    // }
                    $product[] = url('bancassurance');
                    if(getdemat_menu()){
                    $product[] = url(''.getdemat_menu()->slug_name);
                    }
                    if(getonlinefee_menu()){
                    $product[] = url(''.getonlinefee_menu()->slug_name);
                    }
                    // print_r($this->asbagetasba); exit;
                    if(getasba()){
                        $product[] = url(''.getasba()->slug_name);
                    }
                ?>
                
                <div class="nav-container">
                    <ul class="center-nav" id="center-nav">
                        <li class=" @if('http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] == url() || 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] == url('home')) 'active' @endif"><a href="{{ url('home') }} ">Home</a></li>
                        
                        


                        <li class="has-sub-menu product-drop product-full @if(in_array('http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],$product)) 'active' @endif ">
                            <a href="" >Products</a>
                            <div class="ci-drop-area product-tab-hover">

                                <div class="product-options">
                                    <ul class="nav nav-tabs" data-toggle="tab-hover">
                                        
                                        <li class="active">
                                            <a href="#nv-other-serve" data-toggle="tab" >
                                                <span>
                                                    <img class="initial" src="{{url('/')}}/img/frontend/img/icons/menu/online_service.png" alt="">
                                                    <img class="on-hover" src="{{url('/')}}/img/frontend/img/icons/menu/online_service_hover.png" alt="">
                                                </span>
                                                <span>Online Services</span>
                                            </a>
                                        </li>
                                        
                                        <li >
                                           
                                            <a href="#nv-account"  data-toggle="tab">
                                                <span>
                                                    <img class="initial" src="{{url('/')}}/img/frontend/img/icons/menu/simple_pig.png" alt="">
                                                    <img class="on-hover" src="{{url('/')}}/img/frontend/img/icons/menu/simple_pig_hover.png" alt="">
                                                </span>
                                                
                                                <span>Deposit</span>
                                            </a>
                                          
                                        </li>
                                        <li>
                                            <a href="#nv-loan" data-toggle="tab">
                                                
                                                <span>
                                                    <img class="initial" src="{{url('/')}}/img/frontend/img/icons/menu/hand.png" alt="">
                                                    <img class="on-hover" src="{{url('/')}}/img/frontend/img/icons/menu/hand_hover.png" alt="">
                                                </span>
                                                <span>Loan</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#nv-card" data-toggle="tab" >
                                                <span>
                                                    <img class="initial" src="{{url('/')}}/img/frontend/img/icons/menu/card.png" alt="">
                                                    <img class="on-hover" src="{{url('/')}}/img/frontend/img/icons/menu/card_hover.png" alt="">
                                                </span>
                                                <span>Cards</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#nv-remitance" data-toggle="tab">
                                                <span>
                                                    <img class="initial" src="{{url('/')}}/img/frontend/img/icons/menu/remit.png" alt="">
                                                    <img class="on-hover" src="{{url('/')}}/img/frontend/img/icons/menu/remit_hover.png" alt="">
                                                </span>
                                        
                                                <span>Remitance</span>
                                            </a>
                                        </li>
                                      
                                       
                                        <li>
                                            <a href="#nv-other" data-toggle="tab" >
                                                <span>
                                                    <img class="initial" src="{{url('/')}}/img/frontend/img/icons/menu/other.png" alt="">
                                                    <img class="on-hover" src="{{url('/')}}/img/frontend/img/icons/menu/other_hover.png" alt="">
                                                </span>
                                                <span>Other Services</span>
                                            </a>
                                        </li>
                                        
                                        
                                    </ul>
                                </div>

                                <div class="product-links">
                                    <div class="tab-content">
                                        <div class="tab-pane " id="nv-account">
                                            <div class="nav-split-container">
                                              
                                                
                                                <div class="nav-large-width">
                                                    <ul>
                                                          @foreach(getAccounts() as $value)
                                                            <li><a href=" {{ url('account/detail/'.$value->slug_name) }} ">{{  $value->name }} </a></li>
                                                       @endforeach
                                                            
                                                    </ul>
                                                </div>
                                               
                                               
                                                
                                               
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="nv-loan">
                                            <div class="nav-split-container">
                                                <div class="nav-large-width">
                                                   <ul>
                                                        @foreach(getLoans() as $key => $loans)
                                                         

                                                            <li><a href="{{ url('loan/details/'.$loans->slug_name) }}">{{ $loans->loan_name }}</a></li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                               
                                            </div>
                                            
                                        </div>

                                        <div class="tab-pane fade in" id="nv-card">
                                            <div class="nav-split-container">
                                                <div class="nav-large-width">
                                                    <ul>
                                                        @foreach (getCredit() as $key => $value)
                                                            <li><a href="{{ url('credit_card/detail/'.$value->slug_name) }}">{{ $value->credit_card_name }} </a></li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                                
                                            </div>

                                        </div>

                                      <div class="tab-pane fade in" id="nv-remitance">
                                            <div class="nav-split-container">
                                                <div class="nav-large-width">
                                                    <ul>
                                                        @foreach(getremit_menu() as $remit)
                                                         <li><a href="{{ url('page/remit/'. $remit->title) }} ">{{ $remit->title }}</a></li>
                                                        @endforeach 
                                                        <!--<li><a href="{{ url('page/remit/in') }}">International Remitance</a></li>-->
                                                    
                                               
                                                        
                                                    </ul> 
                                                </div>
                                        
                                            </div>
                                        </div>

                                        <!--<div class="tab-pane fade in" id="nv-assurance">-->
                                        <!--    <div class="nav-split-container">-->
                                        <!--        <div class="nav-large-width">-->
                                        <!--            <ul>-->
                                                    
                                                    
                                        <!--            </ul>-->
                                        <!--        </div>-->
                                        <!--    </div>-->
                                        <!--</div>-->

                                        <div class="tab-pane fade in" id="nv-banking">
                                            <ul>
                                                @foreach(getBanking() as $key => $value) 
                                                    <li><a href="{{ url('banking/details/'.$value->slug_name) }}">{{$value->name }}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>

                                        <div class="tab-pane fade in" id="nv-other">
                                            <div class="nav-split-container">
                                                <div class="nav-large-width">
                                                    <ul>
                                                        
                                                        <li><a href="{{ url('paybill-facility') }}">Pay bill Facility</a> </li>
                                                        <li><a href="{{ url('utility-payment') }} ">Utility Payment</a> </li>
                                                        <li><a href="{{ url('privilege') }}">Privilege Banking</a> </li>
                                                        <li><a href="{{ url('locker-facility') }}">Locker</a> </li> 
                                                       
                                                        <li><a href="{{ url('bancassurance') }}">Bancassurance</a> </li>

                                                        <!--<li><a href="{{ url('branch/showLocation/branch') }} ">ABBS Service</a> </li>-->
                                                        @if(getasba())                                                         <!--<li><a href="{{ url() }}uploads/wysiwyg/ONLINE ASBA activation form.pdf">ASBA</a> </li>-->
                                                         <li><a href="{{ url(''.getasba()->slug_name) }} ">{{ getasba()->page_title }}</a></li>
                                                        @endif
                                                            
                                                     @if(getbranchless_menu())
                                                        <li><a href="{{ url(''.getbranchless_menu()->slug_name) }} ">{{ getbranchless_menu()->page_title }} </a></li>
                                                    @endif
                                                          <li><a href="https://www.ctznbank.com/OnlineCDS/">Online De-Mat</a> </li>                      
                                                        <!--<li><a href="http://pagodalabs.com.np/citizens/uploads/account/Individual_Demat_Account_Opening_Form (2).pdf">Individual DEMAT</a> </li>-->
                                                        <!--<li><a href="http://pagodalabs.com.np/citizens/uploads/account/Corporate_Demat_Account_Opening_Form.pdf">Corporate DEMAT</a> </li>-->
                                                        
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                              <!--nv-other-serve-->
                                        <div class="tab-pane fade in active" id="nv-other-serve">
                                            <div class="nav-split-container">
                                                <div class="nav-large-width">
                                                    <ul>
                                                        @if(getonlinefee_menu())
                                                    <li><a href="{{ url(''.getonlinefee_menu()->slug_name) }} ">{{ getonlinefee_menu()->page_title }}</a> </li>
                                                        @endif
                                                    <li><a href="<{{ url('home/application_form') }}">Online Account Opening</a> </li>
                                                    
                                                        @if(getdemat_menu())
                                                        <li><a href="{{ url(''.getdemat_menu()->slug_name) }} ">{{ getdemat_menu()->page_title }}</a> </li>
                                                        @endif
                                                         @if(getasba())
                                                        <li><a href=" {{ url(''.getasba()->slug_name) }}">{{  getasba()->page_title }} </a> </li>
                                                        @endif
                                                       
                                                        <li><a href="https://www.ctznbank.com/kycform/">Online KYC</a> </li>
                                                        
                                                        <li><a href="https://meroshare.cdsc.com.np/" target="_blank">Mero Share</a> </li>
                                                        
                                                       @foreach (getBanking() as $key => $value) >
                                                        <li><a href="{{ url('banking/details/'.$value->slug_name) }}"> {{ $value->name }}</a></li>
                                                        @endforeach
                                                    </ul>    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="has-sub-menu product-mobile">
                            <a href="#">Products</a>
                            <div class="ci-drop-area">
                                <ul>
                                     <li class="has-sub-menu">
                                        <a href="{{ url('home/online') }} ">
                                            <span><img src="{{url('/')}}/img/frontend/img/banner icon -55.png" alt="image"></span>
                                            <span>Online Services</span>
                                        </a>
                                           <ul>       
                                           
                                        
                                                    @if(getonlinefee_menu())
                                                     <li><a href=" {{ url(getonlinefee_menu()->slug_name)  }} ">{{ getonlinefee_menu()->page_title }} </a> </li>
                                                    @endif
                                                    <li><a href="{{ url('home/application_form') }} ">Online Account Opening</a> </li>
                                                    
                                                     @if(getdemat_menu())
                                                        <li><a href="{{ url(''.getdemat_menu()->slug_name) }} ">{{  getdemat_menu()->page_title }} </a> </li>
                                                    @endif
                                                        <li><a href="https://services.ctznbank.com/ASBA/Login.aspx?ReturnUrl=%2fasba%2f">Online ASBA </a> </li>
                                                        <li><a href="https://www.ctznbank.com/kycform/">Online KYC</a> </li>
                                            <li><a href="https://meroshare.cdsc.com.np/" target="_blank">Mero Share</a> </li>
                                        </ul>

                                    </li>
                                    <li class="has-sub-menu">
                                        <a href=" {{ url('account') }}">
                                            <span><img src="{{url('/')}}/img/frontend/img/citizen icon design-21.jpg" alt=""></span>
                                            <span>Accounts</span>
                                        </a>
                                            <ul>
                                                @foreach (getAccounts() as $key => $value) 
                                                    <li><a href="{{ url('account/detail/'.$value->slug_name) }}">{{ $value->name }} </a></li>
                                                @endforeach
                                            </ul>
                                    </li>

                                    <li class="has-sub-menu">
                                        <a href="{{ url('loan') }}">
                                            <span><img src="{{url('/')}}/img/frontend/img/hand-loan-logo.png" alt="image"></span>
                                            <span>Loan</span>
                                        </a>
                                        <ul>
                                            @foreach (getLoans() as $key => $loans) 
                                                <li><a href=" {{ url('loan/details/'.$loans->slug_name) }} ">{{ $loans->loan_name }}</a></li>
                                            @endforeach
                                        </ul>

                                    </li>

                                    <li class="has-sub-menu">
                                        <a href="{{ url('credit_card') }}">
                                            <span><img src="{{url('/')}}/img/frontend/img/visa-logo.png" alt="image"></span>
                                            <span>Cards</span>
                                        </a>
                                        <ul>
                                            @foreach(getCredit() as $key => $value)
                                                <li><a href="{{ url('credit_card/detail/'.$value->slug_name) }} ">{{ $value->credit_card_name }}</a></li>
                                            @endforeach
                                        </ul>

                                    </li>

                                   <li class="has-sub-menu">
                                        <a href="{{ url('page/remit') }}">
                                            <span><img src="{{url('/')}}/img/frontend/img/citizen bank logo-03.png"></span>
                                            <span>Remitance</span>
                                        </a>
                                        <ul>
                                            <li><a href="{{ url('page/remit') }}">Remitance</a></li>
                                        </ul>

                                    </li>


                                  

                                    <!--<li class="has-sub-menu">-->
                                    <!--    <a href="<{{ url('banking') }}">-->
                                    <!--        <span><img src="{{url('/')}}/img/frontend/img/e banking-02.png"></span>-->
                                    <!--        <span>Electronic Banking</span>-->
                                    <!--    </a>-->
                                    <!--    <ul>-->
                                    <!--        @foreach (getBanking() as $key => $value) 
                                    <!--            <li><a href=" {{ url('banking/details/'.$value->slug_name) }} ">{{ $value->name }}</a></li>-->
                                    <!--        @endforeach
                                    <!--        <li><a href="{{ url('branch/branchless') }}">Branchless  Banking</a></li>-->
                                    <!--    </ul>-->

                                    <!--</li>-->

                                    <li class="has-sub-menu">
                                        <a href="{{ url('home/others') }}">
                                            <span><img src="{{url('/')}}/img/frontend/img/citizens icon _green-48.png" alt="image"></span>
                                            <span>Other Services</span>
                                        </a>
                                        <ul>
                                           
                                          
                                            <li><a href=" {{ url('bancassurance') }} ">Bancassurance</a> </li>
                                            
                                              <li><a href="{{ url('locker-facility') }} ">Locker</a> </li>
                                                        
                                            <li><a href="{{ url('utility-payment') }} ">Utility Payment</a> </li>
                                            <li><a href="{{ url('paybill-facility') }} ">Pay bill Facility</a> </li>
                                            <li><a href="{{ url('privilege') }} ">Privilege Banking</a> </li>
                                            @if(getasba())
                                                        <!--<li><a href="{{ url() }}uploads/wysiwyg/ONLINE ASBA activation form.pdf">ASBA</a> </li>-->
                                                         <li><a href="{{ url(''.getasba()->slug_name) }}">{{ getasba()->page_title }}</a></li>
                                            @endif
                                                        <li><a href="https://www.ctznbank.com/OnlineCDS/">Online De-Mat</a> </li> 
                                                        
                                            
                                        </ul>

                                    </li>
                                    
                                     
                                    
                                </ul>
                            </div>
                        </li>
                        
                        <?php $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>
                        <li class="has-sub-menu rep-med @if($url == url('interest_rate/index/ir') || $url == url('interest_rate/index/irs') || $url == url('interest_rate/index/br') || $url == url('interest_rate/index/ex') || $url == url('interest_rate/index/fc')) 'active' @endif ">
                            <a href="#">Rates</a>
                            <div class="ci-drop-area">
                                <ul>
                                    <li><a href="{{ url('interest_rate/index/ir') }} ">Interest Rates</a></li>
                                    <li><a href="{{ url('interest_rate/index/irs') }}">Interest Rate Spread</a></li>
                                    <li><a href="{{ url('interest_rate/index/br')}} ">Base Rates</a></li>
                                    <li><a href="{{ url('interest_rate/index/ex') }}">Exchange Rates</a></li>
                                    <li><a href="{{ url('interest_rate/index/fc') }}">Fees and Commission</a></li>
                                    
                            </ul>
                            </div>
                        </li>
                        <?php 
                            $publication = array();
                            $publication[] = url('capital-plan'); 
                             if(getminutes_menu()){ 
                            $publication[] = url(''.getminutes_menu()->slug_name);
                             }
                             if(getannual_report_menu()){
                            $publication[] = url($this->annual_report_menu['slug_name']); 
                             }
                             if(getannual_report_menu()){
                            $publication[] = url($this->unaudited_menu['slug_name']);
                             }
                             if(getannual_report_menu()){
                            $publication[] = url($this->disclosure_menu['slug_name']);
                             }
                             if(getaml_compliance_menu()){
                            $publication[] = url($this->aml_compliance_menu['slug_name']);
                             }
                        ?>
                        <li class="has-sub-menu rep-med @if(in_array('http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],$publication)) 'active'@endif">
                            <a href="#">Reports & Disclosures</a>
                            <div class="ci-drop-area">
                                <ul>
                                    <!--<li><a href="{{ url('capital-plan') }}">Capital Plan</a></li>-->
                                    
                                    @if(getannual_report_menu())
                                    <li><a href="{{ url(getannual_report_menu()->slug_name) }}"> {{ getannual_report_menu()->page_title }}</a></li>
                                    @endif
                                    @if(getminutes_menu())
                                    <li><a href="{{ url(''.getminutes_menu()->slug_name) }}">{{  getminutes_menu()->page_title }}</a></li>
                                    @endif
                                    @if(getunaudited_menu())
                                    <li><a href="{{ url(''.getunaudited_menu()->slug_name) }}">{{  getunaudited_menu()->page_title }} </a></li>
                                    @endif
                                    @if(getdisclosure_menu())
                                    <li><a href="{{ url(''.getdisclosure_menu()->slug_name) }} ">{{  getdisclosure_menu()->page_title }}</a></li>
                                    @endif
                                    @if(getmoa()) 
                                    <li><a href="{{ url('uploads/page/'.getmoa()->image_name) }} " target="_blank">{{  getmoa()->page_title  }}</a></li>
                                    @endif
                                   @if(getaml_compliance_menu())
                                 <li><a  href="{{ url(''.aml_compliance_menu()->slug_name) }} ">{{  aml_compliance_menu()->page_title  }}</a></li>
                                    @endif
                                    

                                </ul>
                            </div>
                        </li>
                       
                        
                        <li class="menu-int-bank">
                            <a href="https://www.ctznbank.com.np/">
                                eBank Login
                                <!--<span>Internet</span>-->
                                <!--<span>Banking</span>-->
                            </a>
                        </li>
                         
                         <!--<li class =" @if('http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] == url('home/application_form') ) 'active' @endif"><a href=" {{ url('home/application_form') }}">ACCOUNT OPENING</a></li>-->
                        
                        <!--<li class="nv-pe-a"><a href="">
                                <img src="{{url('/')}}/img/frontend/img/e banking-02.png">
                            </a>
                        </li>
                        <li class="cbl-logo"><a href="http://www.cbilcapital.com/">
                                <img src="{{url('/')}}/img/frontend/img/cbil-capital-logo.png">
                            </a>
                        </li>-->
                        
                        <li class="nv-search"><a href="#" onclick="popup_search()"><i class="fa fa-search"></i> </a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-------------- Bottom Menu End ------------->

    </div>
    <!----------------------- Header Wrapper End ----------------------->
 

<div class="content-wrapper" id="content-wrapper">
   <!--<script>-->
   <!--   function download_or_not(){-->
   <!--     if(confirm('Are you sure want to download')){-->
   <!--        window.location.href="{{ url('uploads/wysiwyg/AOA_and_MOA_After_10th_AGM.pdf') }} ";-->
   <!--     }-->
   <!--     else{-->
   <!--         window.location.href="{{ url('uploads/wysiwyg/AOA_and_MOA_After_10th_AGM.pdf') }}";    -->
   <!--     }-->
   <!-- }    -->
   <!--</script>-->

