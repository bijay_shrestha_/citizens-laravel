<section class="home-slider">
    <div class="bannerSlider">
        @foreach($data['banners'] as $banner)
          @if(@$banner->image_name== NULL || $banner->image_name=='' )
            <div class="cz_banner_item  {{ $banner->design }}  homewatermark" style="background-image: url('{{url('uploads/banner/'.$banner->background) }}')">
                 <div class="text-container">
                    <div class="text-inner">
                       @if($banner->logo)
                       <div class="right-img">
                       <img src="{{ url('uploads/banner/'.$banner->logo) }}">
                        </div>
                        @endif
                        
                        
                        <div class="right-text {{ $banner->color }}  bottom  " >
                            <h1 >{!! $banner->top_box_description !!} </h1>
                            <p> {!! $banner->bottom_box_description !!} </p>
                        </div>
                    </div>
                </div>  
                
                <a class="home-ban-more" href="{{ $banner->link }}"> See More</a>
                @if($banner->type==1)
                <div class="banner-water-mark">
                 
                    <a href="{{ $banner->freepik_link }}" target="_blank">Link for image</a>                              
                </div>
                @else
                @endif
            </div>
                @else
            <div class="cz_banner_item  {{ $banner->design }}" style="background-image: url('{{ url('uploads/banner/'.$banner->background) }} ')">
                <div class="image-container">
                    <img src="{{ url('uploads/banner/'.$banner->image_name) }}" class="@if($banner->type == 1) {{ 'make_image_top' }} @else {{ 'image'}} @endif ">
                  
                </div>
                
                <div class="text-container">
                    <div class="text-inner">
                        <div class="right-img">
                            
                            <img src="{{ url('uploads/banner/'.$banner->logo) }} ">
                        </div>
                       <div class="right-text {{ $banner->color }} bottom " >
                            <h1 >{!! $banner->top_box_description !!} </h1>
                            <p>{!!  $banner->bottom_box_description !!}</p>
                        </div>
                    </div>
                </div>
                
                <a class="home-ban-more" href="{{ $banner->link }}"> See More</a>
            
        </div>
        @endif
        
        @endforeach 
      
    </div>
</section>