<!-------------- Top Menu Start -------------->

<div class="top-navigation-container">
    <div class="ci-custom-container">
        <div class="ci-top-toggle">
            <div id="ci-burger" class="ci-burger"> 
                <span>&nbsp;</span>
                <span>&nbsp;</span>
                <span>&nbsp;</span>
                <span>&nbsp;</span>
            </div>
        </div>
        
        <div class="ci-top-nav">
            
            <ul>
                <li class="top-has-sub-menu">
                    <a href="#">About Us</a>
                    <ul>
                        @if(getabout_us_menu())
                            <li>
                                <a href="{{ getabout_us_menu()->slug_name }}">{{ getabout_us_menu()->page_title }}</a>
                                
                            </li>
                        @endif
                        <li><a  href="{{ route('executives','board') }}">Board Members</a></li>
                        <li><a  href="{{ route('executives','exec') }}">Executive Members</a></li>
                        <li><a  href="{{ route('executives','depart') }}">Department</a></li>
                                               
                    </ul>
                  
                </li>
                <li class="top-has-sub-menu">
                    <a href="#">News and events</a>
                    <ul>
                        <li><a  href="{{ route('blogs') }}">News/Press Releases</a></li>
                        <li><a  href="{{ route('csrs') }}">CSR</a></li>
                        <li><a  href="{{ route('announcements') }}">Notices</a></li>
                    </ul>
                </li>

                <li class="top-has-sub-menu">
                    <a href="#">Connect</a>
                    <ul>
                        <li>
                     	<a href="{{ route('blogs') }}">Contact Us</a></li>
						@if(getinformation_office_menu())
                        	<li><a href="{{ getinformation_office_menu()->slug_name }}"><span>{{ getinformation_office_menu()->page_title }} </span></a></li>
                        @endif
                        @if(getspokeperson_menu())
                            <li><a href="{{  getspokeperson_menu()->slug_name }}"><span>{ getspokeperson_menu()->page_title }}</span></a></li>
                        @endif
                        <li>
                            <a href="{{ route('feedback','gr') }}">Grievance Handling</a>
                        </li>
                        <li class="has-submenu">
                            <a href="#">Network</a>
                            <ul>
                                <li><a  href="{{ route('branches','atm') }}">ATM Network</a></li>
                                <li><a  href="{{ route('branches','branch') }}">Branch Network</a></li>
                                <li><a  href="{{ route('cr_networks') }}">Correspondent Network</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="top-has-sub-menu">
                    <a href="#">Downloads</a>
                    <ul>
                        @if(getform_menu())
                        <li><a  href="{{ getform_menu()->slug_name }}">{{ getform_menu()->page_title }}</a></li>
                        @endif
                        <li><a  href="https://play.google.com/store/apps/details?id=com.f1soft.citizensmobilebanking" target = '_blank'>Citizens M-banking App</a></li>
                        @foreach(other_menu() as $menu)
                            <li>
                                @if($menu->type == 'select')
                                    <a href="{{ $menu->link }}">{{ $menu->title }}</a>
                                @else
                                    @if($menu->is_download == 1)
                                        <a href="#" download>{{ $menu->title }}</a>
                                    @else
                                        <a href="{{ $menu->link }}">{{ $menu->title }}</a>
                                    @endif
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </li>

                <li>
                    <a href="#" onclick="showvacancy()">Career</a>
                </li>
				<li class="top-nav-blue">
                    <a href="https://www.cbilcapital.com/" target="_blank">CBIL Capital</a>
                </li>

				<li class="top-nav-blue">
                     <!--<a href="https://www.ctznbank.com/uploads/announcement/AGM_NOTICE_SUMMARIZED_ANNUAL_FINANCIAL_STATEMENT_2074-75.pdf" target='_blank'>AGM Notice</a> -->
                    <a href="https://meroshare.cdsc.com.np/" target='_blank'>MERO SHARE</a>
				</li>

                 <!--<li class="top-nav-blue">
                    <a href="https://www.ctznbank.com/uploads/CITIZENS BARAPARSHA HURI PIDIT RAHATKOSH 2075.pdf" target='_blank'>BARAPARSHA RAHATKOSH</a>
                 </li> -->

				<li class="top-nav-blue">
                    <a class="blinking" href="https://www.ctznbank.com/uploads/mobile banking user manual.pdf" target='_blank'>MOBILE BANKING LOGIN MANUAL</a>
                </li>

				<!--<li class="top-nav-blue">
                    <a class="blinking" href="https://www.ctznbank.com/themes/citizens/assets/img/tutorial.pptx">EBANKING TUTORIAL</a>
                </li> -->

            </ul>
        </div>
        
        
        
        
        
    </div>
</div>
<!--------------- Top Menu End --------------->

<!------------- Bottom Menu Start ------------>


<div class="bottom-navigation-container">
    <div class="ci-custom-container">
        <div class="logo-container">
            <a href="{{ route('front.index') }}">
                <img src="{{ asset('frontcss') }}/img/CB_logo.png" alt="logo">
            </a>

            <div class="ci-bottom-toggle">
                <div id="ci-bottom-burger" class="ci-bottom-burger">
                    <span>&nbsp;</span>
                    <span>&nbsp;</span>
                    <span>&nbsp;</span>
                    <span>&nbsp;</span>
                </div>
            </div>
        </div>
        
        <div class="nav-container">
            <ul class="center-nav" id="center-nav">
                <li class="active"><a href="{{ route('front.index') }}">Home</a></li>
                <li class="has-sub-menu product-drop product-full active">
                    <a href="" >Products</a>
                    <div class="ci-drop-area product-tab-hover">

                        <div class="product-options">
                            <ul class="nav nav-tabs" data-toggle="tab-hover">
                                
                                <li class="active">
                                    <a href="#nv-other-serve" data-toggle="tab" >
                                        <span>
                                            <img class="initial" src="{{ asset('frontcss') }}/img/icons/menu/online_service.png" alt="">
                                            <img class="on-hover" src="{{ asset('frontcss') }}/img/icons/menu/online_service_hover.png" alt="">
                                        </span>
                                        <span>Online Products</span>
                                    </a>
                                </li>
                                
                                <li >
                                   
                                    <a href="#nv-account"  data-toggle="tab">
                                        <span>
                                            <img class="initial" src="{{ asset('frontcss') }}/img/icons/menu/simple_pig.png" alt="">
                                            <img class="on-hover" src="{{ asset('frontcss') }}/img/icons/menu/simple_pig_hover.png" alt="">
                                        </span>
                                        
                                        <span>Deposit</span>
                                    </a>
                                  
                                </li>
                                <li>
                                    <a href="#nv-loan" data-toggle="tab">
                                        
                                        <span>
                                            <img class="initial" src="{{ asset('frontcss') }}/img/icons/menu/hand.png" alt="">
                                            <img class="on-hover" src="{{ asset('frontcss') }}/img/icons/menu/hand_hover.png" alt="">
                                        </span>
                                        <span>Loan</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#nv-card" data-toggle="tab" >
                                        <span>
                                            <img class="initial" src="{{ asset('frontcss') }}/img/icons/menu/card.png" alt="">
                                            <img class="on-hover" src="{{ asset('frontcss') }}/img/icons/menu/card_hover.png" alt="">
                                        </span>
                                        <span>Cards</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#nv-remitance" data-toggle="tab">
                                        <span>
                                            <img class="initial" src="{{ asset('frontcss') }}/img/icons/menu/remit.png" alt="">
                                            <img class="on-hover" src="{{ asset('frontcss') }}/img/icons/menu/remit_hover.png" alt="">
                                        </span>
                                
                                        <span>Remitance</span>
                                    </a>
                                </li>
                              
                               
                                <li>
                                    <a href="#nv-other" data-toggle="tab" >
                                        <span>
                                            <img class="initial" src="{{ asset('frontcss') }}/img/icons/menu/other.png" alt="">
                                            <img class="on-hover" src="{{ asset('frontcss') }}/img/icons/menu/other_hover.png" alt="">
                                        </span>
                                        <span>Other Services</span>
                                    </a>
                                </li>
                                
                                
                            </ul>
                        </div>

                        <div class="product-links">
                            <div class="tab-content">
                                <div class="tab-pane " id="nv-account">
                                    <div class="nav-split-container">
                                        <div class="nav-large-width">
                                            <ul>
                                                @foreach(getAccounts() as $value)
                                                    <li><a href="{{ $value->slug_name }}">{{ $value->name }}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="nv-loan">
                                    <div class="nav-split-container">
                                        <div class="nav-large-width">
                                           <ul>
                                               
                                                @foreach(getLoans() as $value)
                                                    <li><a href="{{ $value->slug_name }}">{{ $value->loan_name }}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade in" id="nv-card">
                                    <div class="nav-split-container">
                                        <div class="nav-large-width">
                                            <ul>
                                                @foreach (getCredit() as $key => $value)
                                                    <li><a href="{{ $value->slug_name }}">
                                                    {{ $value->credit_card_name }}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade in" id="nv-remitance">
                                    <div class="nav-split-container">
                                        <div class="nav-large-width">
                                            <ul>
                                                @foreach(getremit_menu() as $remit)
                                                    <li><a href="{{ $remit->title }}">{{ $remit->title }}</a></li>
                                                @endforeach
                                            </ul> 
                                        </div>
                                
                                    </div>
                                </div>
                                <div class="tab-pane fade in" id="nv-banking">
                                    <ul>
                                        @foreach (getBanking() as $key => $value)
                                            <li><a href="{{ $value->slug_name }}">{{ $value->name }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>

                                <div class="tab-pane fade in" id="nv-other">
                                    <div class="nav-split-container">
                                        <div class="nav-large-width">
                                            <ul>
                                                <li><a href="paybill-facility">Pay bill Facility</a> </li>
                                                <li><a href="utility-payment">Utility Payment</a> </li>
                                                <li><a href="privilege">Privilege Banking</a> </li>
                                                <li><a href="locker-facility">Locker</a> </li> 
                                                <li><a href="bancassurance">Bancassurance</a> </li>
                                                @if(getbranchless_menu())
                                                    <li><a href="{{ getbranchless_menu()->slug_name }}">{{ getbranchless_menu()->page_title }}</a></li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade in active" id="nv-other-serve">
                                    <div class="nav-split-container">
                                        <div class="nav-large-width">
                                            <ul>
                                                <li><a href="https://meroshare.cdsc.com.np/" target="_blank">Mero Share</a> </li>

                                                @if(getdemat_menu())
                                                    <li><a href="{{ getdemat_menu()->slug_name }}">{{ getdemat_menu()->page_title }}</a> </li>
                                                @endif
                                                @if(getasba())
                                                    <li><a href="{{ getasba()->slug_name }}">{{ getasba()->page_title }}</a></li>
                                                @endif
                                                @if(getonlinefee_menu())
                                                    <li><a href="{{ getonlinefee_menu()->slug_name }}">{{ getonlinefee_menu()->page_title }}</a></li>
                                                @endif
                                                <li><a href="https://services.ctznbank.com/KYCFORM/"  target="_blank">Online KYC</a> </li>
                            					<li><a href="https://onlineservices.ctznbank.com/credit-card-form">Online Credit Card Form</a> </li>
                            					<li><a href="https://onlineservices.ctznbank.com/home-loan">Online Home Loan Form</a> </li>
                            					<li><a href="https://www.ctznbank.com/home/online_Account_opening">Online Account Opening Form</a> </li>
                                                @foreach (getBanking() as $key => $value)
                                                    <li><a href="{{ $value->slug_name }}">{{ $value->name }}</a></li>
                                                @endforeach
                                            </ul>    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="has-sub-menu product-mobile">
                    <a href="#">ProductS</a>
                    <div class="ci-drop-area">
                        <ul>
                             <li class="has-sub-menu">
                                <a href="#">
                                    <span><img src="{{ asset('frontcss') }}/img/banner icon -55.png" alt="image"></span>
                                    <span>Online Products</span>
                                </a>
                                <ul>      
                                    <li><a href="https://meroshare.cdsc.com.np/" target="_blank">Mero Share</a> </li>
                                    @if(getdemat_menu())
                                        <li><a href="{{ getdemat_menu()->slug_name }}">{{ getdemat_menu()->page_title }}</a> </li>
                                    @endif
                                    @if(getasba())
                                        <li><a href="{{ getasba()->slug_name }}">{{ getasba()->page_title }}</a></li>
                                    @endif
                                    @if(getonlinefee_menu())
                                        <li><a href="{{ getonlinefee_menu()->slug_name }}">{{ getonlinefee_menu()->page_title }}</a></li>
                                    @endif
                                    <li><a href="https://services.ctznbank.com/KYCFORM/"  target="_blank">Online KYC</a> </li>
                                    <li><a href="https://onlineservices.ctznbank.com/credit-card-form">Online Credit Card Form</a> </li>
                                    <li><a href="https://onlineservices.ctznbank.com/home-loan">Online Home Loan Form</a> </li>
                                    <li><a href="https://www.ctznbank.com/home/online_Account_opening">Online Account Opening Form</a> </li>
                                    @foreach (getBanking() as $key => $value)
                                        <li><a href="{{ $value->slug_name }}">{{ $value->name }}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="has-sub-menu">
                                <a href="#">
                                    <span><img src="{{ asset('frontcss') }}/img/citizen icon design-21.jpg" alt=""></span>
                                    <span>Accounts</span>
                                </a>
                                    <ul>
                                        @foreach(getAccounts() as $value)
                                            <li><a href="{{ $value->slug_name }}">{{ $value->name }}</a></li>
                                        @endforeach
                                    </ul>
                            </li>

                            <li class="has-sub-menu">
                                <a href="#">
                                    <span><img src="{{ asset('frontcss') }}/img/hand-loan-logo.png" alt="image"></span>
                                    <span>Loan</span>
                                </a>
                                <ul>
                                    @foreach(getLoans() as $value)
                                        <li><a href="{{ $value->slug_name }}">{{ $value->loan_name }}</a></li>
                                    @endforeach
                                </ul>

                            </li>

                            <li class="has-sub-menu">
                                <a href="#">
                                    <span><img src="{{ asset('frontcss') }}/img/visa-logo.png" alt="image"></span>
                                    <span>Cards</span>
                                </a>
                                <ul>
                                    @foreach (getCredit() as $key => $value)
                                        <li><a href="{{ $value->slug_name }}">
                                        {{ $value->credit_card_name }}</a></li>
                                    @endforeach
                                </ul>

                            </li>

                           <li class="has-sub-menu">
                                <a href="#">
                                    <span><img src="{{ asset('frontcss') }}/img/citizen bank logo-03.png"></span>
                                    <span>Remitance</span>
                                </a>
                                <ul>
                                    <li><a href="{{ route('forexes') }}">Remitance</a></li>
                                </ul>
                            </li>
                            <li class="has-sub-menu">
                                <a href="home/others">
                                    <span><img src="{{ asset('frontcss') }}/img/citizens icon _green-48.png" alt="image"></span>
                                    <span>Other Services</span>
                                </a>
                                <ul>
                                    <li><a href="bancassurance">Bancassurance</a> </li>
                                    <li><a href="locker-facility">Locker</a> </li>       
                                    <li><a href="utility-payment">Utility Payment</a> </li>
                                    <li><a href="paybill-facility">Pay bill Facility</a> </li>
                                    <li><a href="privilege">Privilege Banking</a> </li>                                       
                                </ul>
                            </li>
                        </ul>
                    </div>
                </li>
                
                <li class="has-sub-menu rep-med active">
                    <a href="#">Rates</a>
                    <div class="ci-drop-area">
                        <ul>
                            <li><a href="interest_rate/index/ir">Interest Rates</a></li>
                            <li><a href="interest_rate/index/irs">Interest Rate Spread</a></li>
                            <li><a href="interest_rate/index/br">Base Rates</a></li>
                            <li><a href="interest_rate/index/ex">Exchange Rates</a></li>
                            <li><a href="interest_rate/index/fc">Fees and Commission</a></li>
                        </ul>
                    </div>
                </li>
                <li class="has-sub-menu rep-med active">
                    <a href="#">Reports & Disclosures</a>
                    <div class="ci-drop-area">
                        <ul>
                            @if(getannual_report_menu())
                                <li><a href="{{ getannual_report_menu()->slug_name }}">{{ getannual_report_menu()->page_title }}</a></li>
                            @endif
                            @if(getminutes_menu())
                                <li><a href="{{ getminutes_menu()->slug_name }}">{{ getminutes_menu()->page_title }}</a></li>
                            @endif
                            @if(getunaudited_menu())
                                <li><a href="{{ getunaudited_menu()->slug_name }}">{{ getunaudited_menu()->page_title }}</a></li>
                            @endif
                            @if(getdisclosure_menu())
                                <li><a href="{{ getunaudited_menu()->slug_name }}">{{ getunaudited_menu()->page_title }}</a></li>
                            @endif
                            <li><a href="uploads/wysiwyg/AOA_and_MOA_After_12th_AGM.pdf" target="_blank">MOA and AOA</a></li>
                            @if(getaml_compliance_menu())
                                <li><a href="{{ getaml_compliance_menu()->slug_name }}">{{ getaml_compliance_menu()->page_title }}</a></li>
                            @endif
                        </ul>
                    </div>
                </li>
               
                
                <li class="menu-int-bank">
                    <a href="https://www.ctznbank.com.np/">
                        eBank Login
                    </a>
                </li>                         
                <li class="nv-search"><a href="#" onclick="popup_search()"><i class="fa fa-search"></i> </a></li>
            </ul>
        </div>
    </div>
</div>
<!-------------- Bottom Menu End ------------->