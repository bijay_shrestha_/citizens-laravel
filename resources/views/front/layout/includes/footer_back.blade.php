                        

<div id="modal_vanancies" class="modal fade" role="dialog">
        <div class="modal-dialog">
    
             Modal content
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="section">
                        <div class="title">
                            <h1>Vacancy Reference</h1>
                        </div>
                        <div class="content">
                            @php $i=1; @endphp
                             @if(count(vacancies()) > 0 )
                            @foreach(vacancies() as $vacancies)
                                <div class="vacancy-item">
                                    <h3> {{ $i.'. '.$vacancies->preferred_post }}</h3>
                                     {!! $vacancies->description !!}
                                    <a href="{{url('home/online_vacancy/'.$vacancies->id.'/'.$vacancies->vacancy_code)}}" class="btn">Apply Now</a>
                                </div>
                                    @php $i++; @endphp 
                                @endforeach 
                            @else
                                <div>Currently We don't have any vacancies</div>
                            @endif
                        </div>
                            
                        
                    </div>
                </div>
            </div>
    
        </div>
    </div>

    <div class="modal fade" id="search_modal">
        <div class="modal-dialog modal-sm citizens-search">
            <div class="modal-content">
    
                <div class="modal-body">
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade active in" id="signin">
                            @php  
                                $csrf = array('name' => '_token','hash' => csrf_token());
                            @endphp
                            <form class="form-horizontal" action="{{url('/')}}/search" method="get" >
                                <fieldset>
                                    <!-- Sign In Form -->
                                    <!-- Text input-->
    
    
    
                                    <div class="control-group">
                                        <div class="controls">
    
    
                                            <input type="text" name="q" id="q" value="{{ ( Input::get('q') !='') ? Input::get('q'):'' }}" type="text" class="form-control" placeholder="Search for item" class="input-medium" required>
                                            <input type="hidden" name="{{$csrf['name']}}" value="{{$csrf['hash']}}" />
                                        </div>
                                    </div>
    
                                    <!-- Button -->
                                    <div class="control-group">
                                        <label class="control-label" for="search"></label>
                                        <div class="index-search">
    
                                            <button  id="search" name="search" class="btn btn-success btn-lg btn-block" id="form submit">Search</button>
                                        </div>
                                    </div>
    
                                    <!-- Button -->
                                    <div class="control-group">
                                        <label class="control-label" for="signin"></label>
                                        <center>
                                            <div class="controls">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </center>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
    
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div> <!--CONTENT WRAPPER ENDS-->

        <div class="footer-wrapper" id="footer-wrapper">
            <div class="ci-side-menu">
            <ul>
               <!-- <li>-->
               <!--       <a href="{{ url('bankings') }}">-->
               <!--        <div><i class="fa fa-bullhorn"></i></div>-->
               <!--        <div>Electronic Banking</div>-->
               <!--    </a>-->
               <!--</li>-->
             <li>
                       <a href="{{url('announcements')}}">
                    <div><i class="fa fa-bullhorn"></i></div>
                    <div>Notice</div>
                </a>
            </li>
            <li>
                <a href="{{url('interest_rates')}}">
                <div>
                    <img src="{{url('/')}}/assets/img/icons/citizens_rate_blue.png" class="tool-img">
                </div>
                <div>Rate</div>
                </a>
            </li>
            
            <li>
                <a href="{{url('home/calculator')}}">
                <div><i class="fa fa-calculator"></i></div>
                <div>Calculator</div>
                </a>
            </li>
            
            <li>
                       <a href="{{url('forexes')}}">
                <div><i class="fa fa-area-chart"></i></div>
                <div>Forex</div>
                </a>
            </li>
            
            <!--<li>-->
            <!--           <a href="{{url('branches/showLocation')}}">-->
            <!--    <div><i class="fa fa-map-marker"></i></div>-->
            <!--    <div>Location</div>-->
            <!--    </a>-->
            <!--</li>-->
            
            

            </ul>
            </div>
        </div>