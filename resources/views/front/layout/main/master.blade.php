<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Cache-control" content="public">
	    <meta http-equiv="Cache-control" content="private">
	    <meta http-equiv="Cache-control" content="no-cache">
	    <meta http-equiv="Cache-control" content="no-store">
		<script type="text/javascript">
		  	var nVer = navigator.appVersion;
			var nAgt = navigator.userAgent;
			var browserName  = navigator.appName;
			var fullVersion  = ''+parseFloat(navigator.appVersion); 
			var majorVersion = parseInt(navigator.appVersion,10);
			var nameOffset,verOffset,ix; 


			// In Opera 15+, the true version is after "OPR/" 
			if ((verOffset=nAgt.indexOf("OPR/"))!=-1) {
				browserName = "Opera";
				fullVersion = nAgt.substring(verOffset+4);
			}
			// In older Opera, the true version is after "Opera" or after "Version"
			else if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
				browserName = "Opera";
				fullVersion = nAgt.substring(verOffset+6);
				if ((verOffset=nAgt.indexOf("Version"))!=-1) 
					fullVersion = nAgt.substring(verOffset+8);
			}
			// In MSIE, the true version is after "MSIE" in userAgent
			else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
				browserName = "Microsoft Internet Explorer";
				fullVersion = nAgt.substring(verOffset+5);
			}
			// In Chrome, the true version is after "Chrome" 
			else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
				browserName = "Chrome";
				fullVersion = nAgt.substring(verOffset+7);
			}
			// In Safari, the true version is after "Safari" or after "Version" 
			else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
				browserName = "Safari";
				fullVersion = nAgt.substring(verOffset+7);
				if ((verOffset=nAgt.indexOf("Version"))!=-1) 
					fullVersion = nAgt.substring(verOffset+8);
			}
			// In Firefox, the true version is after "Firefox" 
			else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
				browserName = "Firefox";
				fullVersion = nAgt.substring(verOffset+8);
			}
			// In most other browsers, "name/version" is at the end of userAgent 
			else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) < 
				(verOffset=nAgt.lastIndexOf('/')) ) 
			{
				browserName = nAgt.substring(nameOffset,verOffset);
				fullVersion = nAgt.substring(verOffset+1);
				if (browserName.toLowerCase()==browserName.toUpperCase()) {
					browserName = navigator.appName;
				}
			}
			// trim the fullVersion string at semicolon/space if present
			if ((ix=fullVersion.indexOf(";"))!=-1)
			fullVersion=fullVersion.substring(0,ix);
			if ((ix=fullVersion.indexOf(" "))!=-1)
				fullVersion=fullVersion.substring(0,ix);

			majorVersion = parseInt(''+fullVersion,10);
			if (isNaN(majorVersion)) {
				fullVersion  = ''+parseFloat(navigator.appVersion); 
				majorVersion = parseInt(navigator.appVersion,10);
			}


		    // alert("Name:"+browserName+" Version "+majorVersion);
			if(browserName == 'Microsoft Internet Explorer' && majorVersion <= 8)
			{
				window.location = 'http://www.ctznback.com';
				//window.navigate("http://www.dignaj.com");
			}
			 
		</script>
	   	<title>{{ (isset($page) && $page['title'])?$page['title']:'Citizens ' }}</title>
	    	

			<!-- bootstrap -->
			<link rel="stylesheet" type="text/css" href="{{ asset('frontcss/') }}/css/bootstrap.css">

			<!-- Font awesome -->
			<link rel="stylesheet" href="{{ asset('frontcss/') }}/css/font-awesome.min.css">


			<link href='https://fonts.googleapis.com/css?family=Raleway:500,600,700,400,300' rel='stylesheet' type='text/css'>
			<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
			<link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
			<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900" rel="stylesheet">

			<!-- Components Start -->
			<link rel="stylesheet" type="text/css" href="{{ asset('frontcss/') }}/css/slick-theme.css">
			<link rel="stylesheet" type="text/css" href="{{ asset('frontcss/') }}/css/slick.css">
			<link rel="stylesheet" type="text/css" href="{{ asset('frontcss/') }}/css/jquery.mCustomScrollbar.min.css">
			<!-- Components Ends -->

			<!-- put at last -->
			<link rel="stylesheet" type="text/css" href="{{ asset('frontcss/') }}/css/global.css<?php echo '?ver='.mt_rand(); ?>">
			<link rel="stylesheet" href="{{ asset('frontcss/') }}/css/bootstrap-slider.css">
			<link rel="stylesheet" type="text/css" href="{{ asset('frontcss/') }}/css/theStyles.css<?php echo '?ver='.mt_rand(); ?>">
			<link rel="stylesheet" type="text/css" href="{{ asset('frontcss/') }}/css/responsive.css<?php echo '?ver='.mt_rand(); ?>">

			<!-- javascript -->


			<script src="{{ asset('frontcss/') }}/js/jquery.js"></script>
			<script src="{{ asset('frontcss/') }}/js/jquery-ui.js"></script>
			<!-- <script src="{{ asset('frontcss/') }}/js/jquery.upload.js" type="text/javascript"></script> -->
			<script src="{{ asset('frontcss/') }}/js/jquery.mCustomScrollbar.min.js"></script>
			<script src="{{ asset('frontcss/') }}assets/js/modernizr.custom.js"></script>


			<script src="{{ asset('frontcss/') }}/js/bootstrap-notify.min.js" type="text/javascript"></script>


			<script src="{{ asset('frontcss/') }}/js/canvasjs.min.js"></script>
	    



			<style>
				.blinking{
			    	animation:blinkingText 1.5s infinite;
				}
				@keyframes blinkingText{
				    0%{     color: #0a67b0;    }
				    49%{    color:transparent; }
				    50%{    color:#0a67b0; }
				    99%{    color:#0a67b0;  }
				    100%{   color:#0a67b0;    }
				}
				.e-bank a{
				  display:inline-block;
				}
				.e-bank span{
				  float: left;
				}

				.e-bank span:nth-of-type(2){
				  margin-top: 15px;
				}
				.wrapper .header-wrapper nav .container .collapse .navbar-nav .dropdown .reports li a,
				.wrapper .header-wrapper nav .container .collapse .navbar-nav .dropdown .abtus li a,
				.wrapper .header-wrapper nav .container .collapse .navbar-nav .dropdown .media li a,
				.wrapper .header-wrapper nav .container .collapse .navbar-nav .dropdown .ebanking li a{
				    padding: 0px;
				}

				/*.multi-bar-list {display:none; position: absolute; top:auto; left:auto;width: 500px;}
				.multi-bar-list li{float: left; display:block; width: 300px; margin: 0; border-bottom: 1px solid #a4aebb;  text-align: left !important;}
				.mutli-bar-list li:nth-child(last){
				    border-bottom: none;
				}




				.multi-bar-list-details {display:none; position: absolute; top:auto; left:auto;width: 500px ; }

				.multi-bar-list-details li{float: left; display:block; width: 400px; margin: 0; border-bottom: 1px solid #a4aebb;  text-align: left !important;}
				.mutli-bar-list-details li:nth-child(last){
				    border-bottom: none;
				}
				*/

				.btn-success{
				        background-color: #8dcfb7;
				    border-color: #8dcfb7;
				}
				.btn-success:hover{
				        background-color: #8dcfb7;
				    border-color: #8dcfb7;
				}
			</style>


	</head>

<body>

    <div class="wrapper" id="wrapper">
       
	    <!---------------------- Header Wrapper Start ---------------------->
	    <div class="header-wrapper" id="header-wrapper">

	    @include('front.layout.includes.navbar')

	    </div>
	    <!----------------------- Header Wrapper End ----------------------->
	 

		<div class="content-wrapper" id="content-wrapper">


			@yield('content')

			<div id="modal_vanancies" class="modal fade" role="dialog">
			    <div class="modal-dialog">

			         Modal content
			        <div class="modal-content">
			            <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal">&times;</button>
			            </div>
			            <div class="modal-body">
			                <div class="section">
			                    <div class="title">
			                        <h1>Vacancy Reference</h1>
			                    </div>
			                    
			                    <div class="content">
			                        
			                    </div>
			                        
			                    
			                </div>
			            </div>
			        </div>

			    </div>
			</div>

			<div class="modal fade" id="search_modal">
			    <div class="modal-dialog modal-sm citizens-search">
			        <div class="modal-content">

			            <div class="modal-body">
			                <div id="myTabContent" class="tab-content">
			                    <div class="tab-pane fade active in" id="signin">
			                        
			                        <form class="form-horizontal" action="search" method="get" >
			                            <fieldset>
			                                <!-- Sign In Form -->
			                                <!-- Text input-->



			                                <div class="control-group">
			                                    <div class="controls">


			                                        <input type="text" name="q" id="q" value="" type="text" class="form-control" placeholder="Search for item" class="input-medium" required>
			                                        <input type="hidden" name="" value="" />
			                                    </div>
			                                </div>

			                                <!-- Button -->
			                                <div class="control-group">
			                                    <label class="control-label" for="search"></label>
			                                    <div class="index-search">

			                                        <button  id="search" name="search" class="btn btn-success btn-lg btn-block" id="form submit">Search</button>
			                                    </div>
			                                </div>

			                                <!-- Button -->
			                                <div class="control-group">
			                                    <label class="control-label" for="signin"></label>
			                                    <center>
			                                        <div class="controls">
			                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			                                        </div>
			                                    </center>
			                                </div>
			                            </fieldset>
			                        </form>
			                    </div>

			                </div>
			            </div>
			        </div>
			    </div>
			</div>

		</div> <!--CONTENT WRAPPER ENDS-->
	</div> <!--WRAPPER ENDS-->

    @include('front.layout.includes.footer')

	<script src="{{ asset('frontcss') }}/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="{{ asset('frontcss') }}/js/bootstrap-scrollspy.js"></script>
	<script src="{{ asset('frontcss') }}/js/bootstrap-slider.js"></script>
	<script src="{{ asset('frontcss') }}/js/bootstrap-collapse.js"></script>
	<script type="text/javascript" src="{{ asset('frontcss') }}/js/slick.min.js" ></script>
	<script type="text/javascript" src="{{ asset('frontcss') }}/js/jquery.nepaliDatePicker.js"></script>

	<script type="text/javascript" src="{{ asset('frontcss') }}/js/thescripts.js"></script>
	<script src="{{ asset('frontcss') }}/js/bootstrap-tooltip.js"></script>

	<script>
    /*        $('.nav-tabs a').click(function(){
     $(this).tab('show');
     })

     $(".burger-nav").on("click",function(){
     $("header > nav > ul").toggleClass("open");
     });

     $(".mini-menu").on("click",function(){
     $(".topMenu > ul ").toggleClass("open");
     });

     $(".topHeader .dropdown").on("click",function(){
     $(this).children('ul').toggleClass("open");
     });

     $(".customDrop").on("click",function(){
     $(this).children('ul').toggleClass("open");
     });
     $(".burgerNav").on("click",function(){
     $("header > nav > ul").toggleClass("open");
     });
     $(".burgerNav").click(function(){
     $(".menu1").removeClass("open");
     });*/

    $(document).ready(function () {

        navInit();
        defaultPrevent();
    })

    var winWid;

    $(window).resize(function () {
        winWid = $(window).width();
        
        $(window).scroll(function() {
            if ($(this).scrollTop() === 100) { // this refers to window
                $('.ci-side-menu').removeClass('dim');
                console.log('yo');
            }
        });
    })



    function navInit() {
        winWid = $(window).width();

        
        $('.nav-container > ul > li').hover(
            function(){ $('.nav-container > ul > li').addClass('remove-back') },
            function(){ $('.nav-container > ul > li').removeClass('remove-back') }
        )
        
        /* Top Menu Script Starts */
        if(winWid >=1199){
            $('.ci-top-nav > ul > li > a').on('click', function (e) {
                e.stopPropagation();
                if($(this).parents('li').hasClass('custom-drop')){
                    $('#center-nav li').removeClass('hover');
                    $(this).parents('li').removeClass('custom-drop');
                    $('.ci-top-nav > ul > li > ul > li').removeClass('custom-drop-slevel');
                }else{
                    $('#center-nav li').removeClass('hover');
                    $('.ci-top-nav > ul > li > ul > li').removeClass('custom-drop-slevel');
                    $('.ci-top-nav > ul > li').removeClass('custom-drop');
                    $(this).parents('li').addClass('custom-drop');
                }
            });
            
            $('.ci-top-nav > ul > li > ul > li').on('click', function () {
                if($(this).hasClass('custom-drop-slevel')){
                    $(this).removeClass('custom-drop-slevel');
                }else{
                    $(this).parents('ul').parents('li').addClass('custom-drop');
                    $('.ci-top-nav > ul > li > ul > li').removeClass('custom-drop-slevel');
                    $(this).addClass('custom-drop-slevel');
                }
            });
        } 
        /* Top Menu Script Ends */
        
        
        /* Bottom Menu Script Starts */
        $('#center-nav li').on('click', function () {
            if($(this).hasClass('hover')){
                $('.ci-top-nav > ul > li').removeClass('custom-drop');
                $('.ci-top-nav > ul > li > ul > li').removeClass('custom-drop-slevel');
                $(this).removeClass('hover');
            }else{
                $('.ci-top-nav > ul > li').removeClass('custom-drop');
                $('.ci-top-nav > ul > li > ul > li').removeClass('custom-drop-slevel');
                $('#center-nav li').removeClass('hover');
                $(this).addClass('hover');
            }
        });
        /*Bottom Menu Script Ends */
            
        /* Main Class Romoval For botn Menus Starts */
        $('#content-wrapper').on('click', function () {
            $('#center-nav li').removeClass('hover');
            $('.ci-top-nav > ul > li').removeClass('custom-drop');
            $('.ci-top-nav > ul > li > ul > li').removeClass('custom-drop-slevel');
        });
        
        $('.bottom-navigation-container').on('click', function () {
            $('.ci-top-nav > ul > li').removeClass('custom-drop');
            $('.ci-top-nav > ul > li > ul > li').removeClass('custom-drop-slevel');
        });
        
        $('.top-navigation-container').on('click', function () {
            $('#center-nav li').removeClass('hover');
        });
        /* Main Class Romoval For botn Menus Ends */            
            
            
        $('.ci-burger').on('click', function () {
            $('.ci-top-nav').toggleClass('open');
        });

        $('.ci-bottom-burger').on('click', function () {
            $('.bottom-navigation-container .nav-container').toggleClass('open');
            $('.ci-side-menu').toggleClass('dim');
        });
        
        
        

        


        if(winWid <=1199){
            
             $('.ci-top-nav > ul > li').each(function(){
                $(this).appendTo('.bottom-navigation-container .center-nav');
            });
            
            $('.nav-container ul li').on('click', function () {
                if($(this).hasClass('main-menu-drop')){
                    $(this).removeClass('main-menu-drop');
                }else{
                    $('.nav-container ul li').removeClass('main-menu-drop');
                    $(this).addClass('main-menu-drop');
                }
            });

            $('.nav-container ul li ul li').on('click', function () {
                if($(this).hasClass('lemain-menu-drop')){
                    $(this).removeClass('lemain-menu-drop');
                }else{
                    $('.nav-container ul li ul li').removeClass('lemain-menu-drop');
                    $(this).addClass('lemain-menu-drop');
                }
            });
            
            $('.nav-container ul li ul li ul li').on('click', function () {
                if($(this).hasClass('memain-menu-drop')){
                    $(this).removeClass('memain-menu-drop');
                }else{
                    $('.nav-container ul li ul li ul li').removeClass('memain-menu-drop');
                    $(this).addClass('memain-menu-drop');
                }
            });
            
          
            /* Appended */
      
        }
        



    }

    function defaultPrevent() {
        $('.has-sub-menu > a').click(function (e) {
            e.preventDefault();
        });



    }

    function showvacancy(){
       
        $('#modal_vanancies').modal('show');
       
    }

    function popup_search(){
        $('#search_modal').modal('show');
    }
	</script>


	<!--Start of Tawk.to Script-->
	<script type="text/javascript">
	var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
	(function(){
	var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
	s1.async=true;
	s1.src='https://embed.tawk.to/58f7532530ab263079b6069f/default';
	s1.charset='UTF-8';
	s1.setAttribute('crossorigin','*');
	s0.parentNode.insertBefore(s1,s0);
	})();
	</script>
	<!--End of Tawk.to Script-->


	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-138151745-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-138151745-1');
	</script>

	<!--
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-101595708-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-101595708-1');
	</script>
	-->


	</body>
</html>