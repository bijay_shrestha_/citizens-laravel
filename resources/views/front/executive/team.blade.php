@extends('front.layout.main.master')
@section('content')


<div class="team-page list-all-page">


    @php 
       $management_teams= $data['management_teams'];
        $department_members= $data['department_members'];
             $executive_banners=$data['executive_banners'];
       $bod_banners=$data['bod_banners'];
    $departments=$data['departments'];
$active=$data['active'];
$bod_directors=$data['bod_directors'];
    @endphp
    <section class="home-slider">
        <div class="bannerSlider" id="carousel-inner">
           @if('http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] == url('executives/index/board'))
            
            <div class="bannerItem" style="background-image: url('{{  url('uploads/banner/'.$bod_banners[0]->image_name) }}')">
                    
            </div>
            
            @elseif('http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] == url('executives/index/depart'))
           
                <div class="bannerItem" style="background-image: url('{{ url('uploads/banner/'.$departments[0]->image_name) }}')">
                    
            </div>
            @else 
            
             <div class="bannerItem" style="background-image: url('{{ url('uploads/banner/'.$executive_banners[0]->image_name) }}')">
                    
            </div>
            @endif
        </div>
    </section>

    <section class="page-content-area">
        <div class="outer-custom-container">
            <div class="left-big-contianer">
                <div class="co-tab-container">
                    <!-- Nav Tabs Top-->
                    <ul class="nav nav-tabs" id="nav" role="tablist">
                        <li role="presentation" class="@if($active == 'board') active @endif">
                            <a  href="#board"  onclick="changeImage('bod')" role="tab" data-toggle="tab">
                                <span>BOARD MEMBERS</span>
                            </a>
                        </li>
                        <li role="presentation" class="@if($active == 'exec') active @endif">
                          <a href="#execute"  onclick="changeImage('executive')" role="tab" data-toggle="tab">
                                <span>EXECUTIVE TEAM</span>
                            </a>
                        </li>
                     
                        <li role="presentation" class="@if($active == 'depart') active @endif">
                        <a href="#department" onclick="changeImage('department')" role="tab" data-toggle="tab">
                        <span>DEPARTMENT HEADS
                        </a>
                        </li>
                    </ul>
                </div>

                <!-- Nav Tab Content -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in team-member-panel @if($active == 'exec') {{ "active" }} @endif> " id="execute">
                        <div class="team">
                            <div class="row">
                                @foreach($management_teams as $team)
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="team-col">
                                            <div class="team-members">
                                                <div class="team-img" style="background-image: url('{{ url('uploads/executive/'.$team->image_name) }}')"></div>
                                                <div class="name">
                                                    <h4> {{ $team->executive_name }}</h4>
                                                    <p>{{ $team->designation }}</p>
                                                </div>

                                                <div class="team-more">
                                                    <a href="#" class="profiles" data-toggle="modal" data-target="#profile" data-id='{{ json_encode($team) }}  '>View Profile</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane fade in team-member-panel @if($active == 'board') {{ "active" }} @endif" id="board">
                        <div class="team">
                            <div class="row">
                                 @foreach($bod_directors as $bod)
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="team-col">
                                            <div class="team-members">
                                                <div class="team-img" style="background-image: url('{{ url('uploads/executive/'.$bod->image_name) }}')"></div>
                                                <div class="name">
                                                    <h4> {{ $bod->executive_name }} </h4>
                                                    <p> {{ $bod->designation }} </p>
                                                </div>
                                                <div class="team-more">
                                                    <a href="#" class="profiles" data-toggle="modal" data-target="#profile" data-id='{{ json_encode($bod) }}  '>View Profile</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade in team-member-panel @if($active == 'depart') {{ "active" }} @endif" id="department">
                        <div class="team">
                            <div class="row">
                                @foreach($department_members as $department_member)
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="team-col">
                                            <div class="team-members">
                                                <div class="team-img" style="background-image: url('{{ url('uploads/executive/'.$department_member->image_name) }}')"></div>
                                                <div class="name">
                                                    <h4> {{ $department_member->executive_name }} </h4>
                                                    <p>{{ $department_member->designation }} </p>
                                                </div>
                                                <div class="team-more">
                                                    <a href="#" class="profiles" data-toggle="modal" data-target="#profile" data-id='{{ json_encode($department_member) }} '>View Profile</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Nav Tab Content End -->
            </div>
        </div>
    </section>
</div>






 <!-- Modal -->
 <div class="modal fade co-new-modal" id="profile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  aria-hidden="true">
 	<div class="modal-dialog">
 		<div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></button>
 			<div class="modal-body">
 			    <div class="modal-title">
 			        <h1 id="profile_detail_name"></h1>
 					<h2 id="profile_detail_post"></h2>
 			    </div>
 			    
 			    <div class="modal-content">
 			        <div class="block-container">
 			            <div class="left-block">
 			                <div id="profile_image"></div>
 			            </div>
 			            <div class="right-block">
 			                <h3 id="profile_detail_team"></h3>
 			                <div id="profile_detail_description"></div>
 			            </div>
 			        </div>
 			        
 			        <div class="modal-social" id="profile_facebook"></div>
 			    </div>
 		    </div>
 	    </div>
    </div> 
</div>
 <script type="text/javascript">

 	function changeImage(type){

 		
 		
 		if(type == 'executive'){
 		 var img_url = '{{ url('') }}';
 		 var img = '{{  $executive_banners[0]->image_name }}';
 		 var link  = '{{ $executive_banners[0]->link  }}';
 		 console.log(link);
 		 $('#carousel-inner').empty();
 		 $('#carousel-inner').html('<div class="bannerItem" style= \'background-image: url("'+img_url+'uploads/banner/'+img+'");\' ></div>');		
 		}
 		else if(type == 'department'){
 		   var img_url2 = '{{ url('') }}'; 
 		    var img2 = '{{ $departments[0]->image_name }}';
 		 var link2  = '{{$departments[0]->link  }}';
 		 //console.log(link);
 		 $('#carousel-inner').empty();
 		 $('#carousel-inner').html('<div class="bannerItem" style= \'background-image: url("'+img_url2+'uploads/banner/'+img2+'");\' ></div>');
 		}
 		else{
 		 $('#carousel-inner').empty();
 		 var img_url1 = '{{ url('') }}';
 		 var img1 = '{{ $bod_banners[0]->image_name }}';
 		 var link1  = '{{ $bod_banners[0]->link }}';
 		 $('#carousel-inner').empty();
 	
 		 $('#carousel-inner').html('<div class="bannerItem" style=\'background-image: url("'+img_url1+'uploads/banner/'+img1+'");\' ></div>');		

 		}

 		
 	}
 </script>

 <script type="text/javascript">

 	var base_url = '{{ url('') }} ';
 	var theme_url = '{{ url('img/frontend/') }}';
 	$(".profiles").on('click',function(){

 		var profileid = $(this).data('id');
 		console.log(profileid.image_name);
			// $("#profile_detail").append('here');
			$("#profile_image").html('<img src="'+base_url+'uploads/executive/'+profileid.image_name+'" width="200px">');
			$("#profile_title").html('<b>'+profileid.executive_name+'</b>');
			$("#profile_detail_name").html(profileid.executive_name);
			$("#profile_detail_post").html(profileid.designation);
// 			$("#profile_detail_team").html('Team Type: '+profileid.team_type);
			$("#profile_detail_description").html('<p> '+profileid.note+'</p>');
			$("#profile_facebook").html('<ul><li><a href="'+profileid.facebook+'" target="_blank"><i class="fa fa-facebook"></i></a></li></ul>');
		});
 	$('#profile').modal('show');


 </script>

@endsection

