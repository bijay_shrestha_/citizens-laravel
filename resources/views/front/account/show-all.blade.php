@extends('front.layout.main.master')
@section('content')

<div class="account-page common-page">

<section class="home-slider">
    <div class="bannerSlider">
        <?php foreach($banners as $banner):?>
        <?php if($banner['image_name']== NULL OR $banner['image_name']=='' ):?>
              <div class="bannerItem bannerItemInside <?php echo $banner['design']?>" style="background-image: url('{{asset('uploads/banner/'.$banner['background'])}}')">
                
                 <div class="text-container">
                    <div class="text-inner">
                        <div class="right-img">
                            <img src="<?php echo url('uploads/banner/'.$banner['logo'])?>">
                        </div>
                        <div class="right-text <?php echo $banner['color']?> bottom-<?php echo $banner['bottom-color']?> " >
                            <h1 ><?php echo $banner['top_box_description'] ?></h1>
                            <p><?php echo $banner['bottom_box_description'] ?></p>
                        </div>
                    </div>
                </div>
                <?php if($banner['type']==1){?>
                <div class="banner-water-mark">
                 
                    <a href="<?php echo $banner['freepik_link']?>" target="_blank">Link for image</a>                             
                </div>
                <?php }else{?>
                <?php }?>
            </div>
               
        <?php else:?>
       
                
                 <div class="cz_banner_item <?php echo $banner['design']?>" style="background-image: url('<?php echo url('uploads/banner/'.$banner['background'])?>)">
                <div class="image-container">
                    <img src="<?php echo url('uploads/banner/'.$banner['image_name'])?>">
                </div>
                
                <div class="text-container">
                    <div class="text-inner">
                        <div class="right-img">
                            <img src="<?php echo url('uploads/banner/'.$banner['logo'])?>">
                        </div>
                        <div class="right-text <?php echo $banner['color']?> bottom-<?php echo $banner['bottom-color']?> " >
                            <h1 ><?php echo $banner['top_box_description'] ?></h1>
                            <p><?php echo $banner['bottom_box_description'] ?></p>
                        </div>
                    </div>
                </div>
                
               
            
        </div>
                <?php endif;?>
        <?php endforeach;?>
        
    </div>
</section>

    
    


    <section class="page-content-area"> 
        <div class="outer-custom-container">
            <div class="left-big-contianer">
                
                <div class="section-title">
                    <div class="icon-container">
                        <img src="<?php echo url('uploads/account/'.$accounts['image_name'])?>">
                    </div>
                    <div class="text-container">
                        <h1><?php echo $accounts['name'];?></h1>
                    </div>
                </div>
                    
               <div class="section-content">
                        <div class="top-description-section table-responsive">
                            <p><?php echo $accounts['description'];?></p>
                        </div>
                        <?php if($accounts['fixed_rate']!= NULL OR $accounts['fixed_rate']!=""){?>
                        <div class="rate-section table-responsive">
                                 <p><b> Interest Rate : </b> <?php echo $accounts['fixed_rate'];?>  &nbsp;p.a &nbsp 
                                 <a href="<?php echo url('interest_rate/index/ir')?>">Click here to see more</a></p>
                               
                        </div>
                        <?php }else{?>
                        <?php }?>
                        <div class="eligibility-section">
                            <?php if ($accounts['facilities']!= NULL OR $accounts['facilities'] != ''){ ?>
                                <h2>Eligibility Criteria:</h2>
                                <p><?php echo $accounts['facilities'];?></p>
                            <?php } ?>
                        </div>
                    </div>
                
            </div>

            <div class="right-small-contianer">
                <div class="side-scroll-heading">
                        <h2>Other Deposits</h2>
                </div>
                
              
                <div class="sidebar-content sidebar-scroll">
                    <?php foreach($account_names as $account_name):?>
                    <div class="sidebar-item">
                        <a href="<?php echo url('account/detail/'.$account_name['slug_name'])?>">
                            <span><img  src="<?php echo url('uploads/account/'.$account_name['image_name'])?>">

                                </span>
                            <span><h1><?php echo $account_name['name']; ?></h1></span>
                        </a>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </section>

</div>



 </div>
 <!-- Content Wrapper Ends-->


<script>

        $(document).ready(function () {
            $(".sidebar-scroll").mCustomScrollbar({
                theme: "dark-thin"
                
            });
        });
        

</script>

@endsection