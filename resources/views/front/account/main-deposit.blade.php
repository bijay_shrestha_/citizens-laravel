@extends('front.layout.main.master')
@section('content')
<div class="loan-page-all list-all-page">
   

<section class="home-slider">
    <div class="bannerSlider">
        @foreach($banners as $banner)
        @if($banner['image_name']== NULL OR $banner['image_name']=='' )
              <div class="bannerItem bannerItemInside {{$banner['design']}}" style="background-image: url('<?php echo url('uploads/banner/'.$banner['background'])?>')">
                
                 <div class="text-container">
                    <div class="text-inner">
                        <div class="right-img">
                            <img src="{{url('/')}}/uploads/banner/{{$banner['logo']}}">
                        </div>
                        <div class="right-text{{$banner['color']}} bottom-{{$banner['bottom-color']}} " >
                            <h1 >{!! $banner['top_box_description'] !!}</h1>
                            <p>{!!$banner['bottom_box_description'] !!}</p>
                        </div>
                    </div>
                </div>
                @if($banner['type']==1){
                <div class="banner-water-mark">
                 
                    <a href="{{$banner['freepik_link']}}" target="_blank">Link for image</a>                          
                </div>
                @else

                @endif
            </div>
               
        @else
       
                
                 <div class="cz_banner_item {{ $banner['design']}}" style="background-image: url('{{asset('uploads/banner/'.$banner['background'])}}')">
                <div class="image-container">
                    <img src="{{asset('uploads/banner/'.$banner['image_name'])}}">
                </div>
                
                <div class="text-container">
                    <div class="text-inner">
                        <div class="right-img">
                            <img src="{{asset('uploads/banner/'.$banner['logo'])}}">
                        </div>
                        <div class="right-text {{$banner['color']}} bottom-{{$banner['bottom-color']}}" >
                            <h1 >{!!$banner['top_box_description']!!}</h1>
                            <p>{!! $banner['bottom_box_description']  !!}</p>
                        </div>
                    </div>
                </div>
                
               
            
        </div>
                @endif
        @endforeach
        
    </div>
</section>

    
<section class="page-content-area">
        <div class="outer-custom-container">
            <div class="left-big-contianer">
                <div class="section-title">
                    <div class="icon-container">
                       <img src="{{asset('frontcss/img/citizen icon design-21.jpg')}}">
                    </div>
                    <div class="text-container">
                        <h1>Deposits Schemes</h1>
                    </div>
                </div>

                <div class="content-container">
                    <div class="section-content">
                    @foreach($accounts as $account)
                            <div class="show-all-item">
                                <div class="title">
                                    <div class="image-container">
 
                                       <img src="{{asset('uploads/account/'.$account['image_name'])}}">
                                    </div>
                                    <div class="text-container">
                                        <h3>{{ $account['name'] }}</h3>
                                    </div>
                                </div>
                                <div class="content table-responsive">
                                    @if($account['fixed_rate'])
                                        <p><b>Interest Rate : </b>
                                        {{ $account['fixed_rate'] }}  &nbsp;p.a
                                    @endif


                                    <div class="show-all-btn">
                                        <span><a href="{{ url('account/detail/'.$account['slug_name'])}}">Know More</a></span>
                                        @if ($account['download_form_link']!= NULL OR $account['download_form_link'] != '')
                                            <span><a href="{{url('uploads/account/forms/'.$account['download_form_link'])}}">Download Form</a></span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                     <div class="aside-right">
                        <div class="sidebar-content ">
                            

                            <div class="sidebar-item side-title">
                                <span><h1>Tools</h1></span>
                            </div>

                            

                           <div class="sidebar-item">
                                <a href="{{url('feedback')}}">
                                    <span>
                                        <img src="{{asset('frontcss/img/citizens icon _green-35.png')}}">      
                                    </span>
                                    <span><h1>Grievance Handling</h1></span>
                                </a>
                            </div>


                            <div class="sidebar-item">
                                <a href="{{url('branch/showlocation/branch')}}">
                                    <span>
                                       
                                        <img  src="{{asset('frontcss/img/icons/hover-visit.png')}}">      
                                    </span>
                                    <span><h1>Visit Our Branch</h1></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                   
                
        </div>

            <!--<div class="right-small-contianer">

            </div>-->
        </div>
    </section>
</div>

@endsection



