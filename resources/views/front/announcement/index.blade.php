@extends('front.layout.main.master')
@section('content')
<style>
.section-title .text-container, .show-all-item .text-container {
    width: 70%;
display: inline-block;
    float: left;
}
</style>

<div class="rates-page list-all-page">
   <section class="home-slider">
        <div class="bannerSlider">
            @foreach($banners as $banner)
                <div class="bannerItem bannerItemInside {{$banner['design']}}" style="background-image: url('{{url('uploads/banner/'.$banner['background'])}}')">
                
                 <div class="text-container">
                    <div class="text-inner">
                        <div class="right-img">
                            <img src="{{url('/')}}/uploads/banner/{{$banner['logo']}}">
                        </div>
                       <div class="right-text {{$banner['color']}} bottom-<?php echo $banner['bottom-color']?> " >
                            <h1 >{!! $banner['top_box_description'] !!}</h1>
                            <p>{!! $banner['bottom_box_description'] !!}</p>
                        </div>
                    </div>
                </div>  
                @if($banner['type']==1)
                    <div class="banner-water-mark">
                     
                        <a href="{{$banner['freepik_link']}}" target="_blank">Link for image</a>                           
                    </div>
                @else
                
                @endif
            </div>
            @endforeach
        </div>
    </section>
 <section class="page-content-area">
        <div class="outer-custom-container">
            <div class="left-big-contianer">
                <div class="page-with-side-bar">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="page-with-side-header">
                                <h2>Categories</h2>
                            </div>
                            
                            <ul class="nav nav-tabs" role="tablist">
                                 
                                <li class="active">
                                    <a href="#all" role="tab" data-toggle="tab">All</a>
                                </li>
                                @if($notice_categories) 
                                    @foreach($notice_categories as $category)
                                        <li>
                                           <a href="#{{$category['id']}}" role="tab" data-toggle="tab">{{$category['category_name']}}</a>
                                        </li>
                                    @endforeach
                                @endif

                            </ul>
                        </div>
                        
                        <div class="col-sm-9">
                            <!-- Nav Tab Content -->
                            
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="all">
                                    <div class="page-with-side-header">
                                        <h2>All Notices</h2>
                                    </div>
                                    <div class="table-responsive">
                                        <div class="content-container">
                                            <div class="section-content">
                                                @if($announcements)
                                             @foreach ($announcements as $announcement)
                                                    <div class="show-all-item">
                                                        <div class="title" style="display: inline-block;">
                                                            
                                                            <div class="text-container">
                                                                <h4><strong>{{$announcement['title']}}</strong></h4>      
                                                                    @if($announcement['date'] == '0000-00-00' || $announcement['date'] == '' || $announcement['date'] == NULL )
                                                                    @else

                                                                    <h6 style="color: #159051">Posted Date: <?php echo date("jS F, Y", strtotime($announcement['date'])); ?></h6>@endif                                                                

                                                                    @if($announcement['ex_date'] == '0000-00-00' || $announcement['ex_date'] == '' || $announcement['ex_date'] == NULL )
                                                                  
                                                                    @else
                                                                        <h6 style="color: #159051">Expiry Date: <?php echo date("jS F, Y", strtotime($announcement['ex_date'])); ?></h6>
                                                                    @endif                                                            </div><div class="show-all-btn" style="margin-top: 0px !important;">
                                                               <a href="javascript:void(0);" onClick="newPopup('{{url('uploads/announcement/'.$announcement['image_name'])}}');">
                                                                
                                                                    Preview
                                                                
                                                            </a>
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="content">
                                                          {!! $announcement['content'] !!}
                                                           
                                                           
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                @else
                                                  <h1>0 Notice Found!!</h1>
                                                @endif
                                            </div>
                                    </div>
                                </div>
                            </div>
                            @foreach ($notice_categories as $cat)
                            @php $i = 0; @endphp
                            <div class="tab-pane fade in" id="<?php echo $cat['id'] ?>">
                                    <div class="page-with-side-header">
                                        <h2><?php echo $cat['category_name'] ?></h2>
                                    </div>
                                    <div class="table-responsive">
                                        <div class="content-container">
                                            <div class="section-content">
                                                @if($announcements)
                                                @foreach ($announcements as $announcement)
                                                    @if($cat['id'] == $announcement['cat_id'])
                                                        @php 
                                                            $i++;
                                                        @endphp
                                                    <div class="show-all-item">
                                                        <div class="title" style="display: inline-block;">
                                                            
                                                            <div class="text-container">
                                                                <h4><strong>{{$announcement['title'] }}</strong></h4>
                                                                @if($announcement['date'] == '0000-00-00' || $announcement['date'] == '' || $announcement['date'] == NULL )

                                                                @else

                                                                    <h6 style="color: #159051">Posted Date: {{date("jS F, Y", strtotime($announcement['date']))}}</h6>
                                                                @endif
                                                                
                                                                @if($announcement['ex_date'] == '0000-00-00' || $announcement['ex_date'] == '' || $announcement['ex_date'] == NULL 
                                                                )                                                                
                                                                 @else
                                                                <h6 style="color: #159051">Expiry Date: {{date("jS F, Y", strtotime($announcement['ex_date']))}}</h6>
                                                                @endif                                                           
                                                             </div>
 								                           <div class="show-all-btn" style="margin-top: 0px !important;">
                                                                    <a href="javascript:void(0);" onClick="newPopup('{{url('uploads/announcement/'.$announcement['image_name'])}}');">
                                                                
                                                                    Preview
                                                                </a>    
                                                            </div>
                                                        </div>
                                                        <div class="content">
                                                          {!! $announcement['content'] !!}
                                                           
                                                        </div>
                                                    </div>
                                                @endif
                                                @endforeach
                                                        @if($i == 0)
                                                            <h4>0 Notice Found!!</h4>
                                                        @endif
                                                 @endif   
                                            </div>
                                    </div>
                                </div>
                            </div>

                            @endforeach
                        </div>
                        
                        
                                                   
                        <!-- Tab panes -->
                            
                    <!-- Nav Tabs Top-->
                </div>
                </div>
            </div>
        </div>
    </section>    
</div>
       

                

<script>
    function newPopup(url) {
        popupWindow = window.open(
            url,'popUpWindow','height=700,width=800,left=10,top=10,resizable=yes,scrollbars=no,toolbar=no,menubar=no,location=no,directories=no,status=yes')
    }
</script>

@endsection