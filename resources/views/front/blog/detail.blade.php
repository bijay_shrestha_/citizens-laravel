@extends('front.layout.main.master')
@section('content')
<div class="blog-detail-page list-all-page">
    <section class="home-slider">
        <div class="bannerSlider">
            @foreach($banners as $banner)
                <div class="bannerItem" style="background-image: {{ url('uploads/banner/'.$banner->image_name) }}">
                    <div class="banner-content">
                        {{-- <!--<h1>{{ $banner['top_box_description'] }} </h1>-->
                        <!--<p>{{ $banner['bottom_box_description'] }}</p>-->
                        <!--<a href="{{ $banner['link'] }}"> See More</a>--> --}}
                    </div>
                </div>
            @endforeach
        </div>
    </section>

    <section class="page-content-area">
        <div class="outer-custom-container">
            <div class="left-big-contianer">

                <div class="section-content">
                    @foreach($blogs as $blog)
                        <div class="show-all-item">
                            <div class="title">
                                <div class="text-container">
                                    <h3>{{ $blog->blog_title }}</h3>
                                </div>
                            </div>

                           @if($blog->post_date != '0000-00-00')
                                <div class="date">
                                    <i class="fa fa-calendar" aria-hidden="true"> </i>
                                    <p>{{ $blog->post_date }}</p>
                                </div>
				            @endif
                            <div class="content">

            @if($blog->image_name=="" ||$blog->image_name==NULL)
            @else
            <div class="blog-image" style="background-image: {{ url('uploads/blog/'.$blog->image_name) }}')"></div>
            @endif
                                
                                {{ $blog->contents }}


                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="aside-right">
                    <div class="sidebar-content ">

                        <div class="sidebar-item side-title">
                            <span><h1>Related Blogs</h1></span>
                        </div>

                        @foreach ($news as $new)
                            <div class="sidebar-item">
                                <a href="{{ url('/'.$new['slug_name'] ) }}">
                                    <span><i class="fa fa-newspaper-o" aria-hidden="true"></i></span>
                                    <span><h1> {{ $new['blog_title'] }} </h1></span>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection





