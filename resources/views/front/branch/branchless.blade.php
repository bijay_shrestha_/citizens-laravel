@extends('front.layout.main.master')
@section('content')


<div class="card-page-all list-all-page">
    <section class="home-slider">
        <div class="bannerSlider">
            @foreach($banners as $banner)
                 <div class="bannerItem bannerItemInside" style="background-image: url('{{ url('uploads/banner/'.$banner['background'])}}')">
                
                 <div class="text-container">
                    <div class="text-inner">
                        
                        <div class="right-text {{ $banner['color'] }} bottom-{{ $banner['bottom-color'] }} " >
                            <h1 >{!! $banner['top_box_description'] !!} </h1>
                            {!! $banner['bottom_box_description'] !!}
                        </div>
                    </div>
                </div>  
                
            </div>
            @endforeach
        </div>
    </section>

    <section class="page-content-area">
        <div class="outer-custom-container">
            <div class="left-big-contianer">
                <div class="section-title">
                    <div class="icon-container">
                        <img src="{{asset('/frontcss/img/citizen icon design_Branchless Banking-02.png')}}">
                    </div>
                    <div class="text-container">
                       <h3 style="font-size: 26px;
    font-weight: 700;
    color: #0b67b1;">Branchless Banking</h3>
                    </div>
                </div>

                <div class="content-container">
                    <div class="section-content">
                        <div class="branchless">

          <p>   Branchless Banking (BLB) is means of providing banking services without setting up bank branch i.e. providing banking services by an agent of the bank.
            The bank has started its BLB operation from Jestha 30, 2069 at Matiyani of Mahottari District after taking approval from Nepal Rastra Bank.
            This service will help to penetrate the banking without the physical existence of bank. 
            In this area the bank has hired an agent which works on behalf of Bank and provides banking services as mentioned below:
          </p>

          <div class="blog-title">

            <h3>Financial Transactions</h3>
          </div>

          <ul >
            <li>Cash Deposit/Cash withdrawal</li>
            <li>Fund Transfer</li>
            <li>Mobile Top up</li>
            <li>Utility Payment</li>
            <li>Any other Services</li>
          </ul>

          <div class="blog-title">
            <h3> Non Financial Transactions</h3>
          </div>

          <ul>
            <li>Balance enquiry</li>
            <li>Mini statement</li>
            <li>Any other services</li>
          </ul>


          <p>
            The BLB service will help the remote area villagers by keeping the deposit safely and also providing interest on their unused fund and the people will also have the habit of saving.
            The BLB is operated through POS Machine which is biometric so it is secured as the concerned account holder has to be present
            along with VISA card at the BLB location for the withdrawal and deposit.
            Random selection of Fingerprint impression is made during the transaction.
            After the transaction is completed the customer listens to the voice prompt of the POS
            machine and understands what transaction they have done. 

          </p>

          <p><b>We have started BLB operation from following locations.</b> </p>
          <ul>
            <li>Narayanghat -> Meghauli,Gunjanagar</li>
            <li>Butwal -> Rani Bagiya,Kerwani</li>
            <li>Gorahi -> Dudrash</li>
            <li>Gaighat ->  Beltar, Katari</li>
            <li>Mahendra Nagar -> Beldandi, Dodhara,Bramhadev</li>
            <li>Janakpur -> Matiyani , Laxminiya Bazar</li>
            <li>Surkhet  -> Chinchu, Mehalkuna,Babiyachaur</li>
            <li>Nepalgunj ->  Bholagaudi,Maina Pokhar,Sitalbazar, Mahadevpuri</li>
            <li>Dhangadhi ->  Godavari,Pahalwanpur,Khodpe</li>
            <li>Biratnagar -> Katahari , Tankisinwari,Karsiya</li>
            <li>Bhaktapur  -> Sipadol, Chhaling, Sanga, Changu Narayan, Tathli, Nagarkot</li>
            <li>Kapan ->  Zhor</li>
            <li>Kirtipur -> Machhegaun, Chalnakhel</li>
            <li>Patandhoka -> Lele/Tika Bhairab,Tika Thali, Dhapakhel</li>
            <li>Birtamod ->Bahundagi, Gaurigunj,Shivgunj,Sanischare</li>
            <li>Pokhara -> Armala</li>
            <li>Kalanki -> Ramkot</li>
            <li>Nayabazar ->Tinpiple,Kavresthali</li>
            <li>Charikot -> Maina Pokhari, Namdu, Sahare, Sailungeshwor, Susma Chhyamawati</li>
            <li>Itahari -> Chatara, Khanar,Tarahara</li>
            <li>Dolpa -> Juphal</li>
            <li>Baglung -> Hatiya</li>
            <li>Gaushala -> Ramgopalpur, Aurahi</li>
            <li>Pathalaiya -> Piluwa</li>
            <li>Samakhusi -> Bidur, Bageshwori, Charghare, Chaughada, Fikuri, Gerkhu, Khadga Bhanjyang, Khanigaun, Laharepauwa, Manakamana, Thansing, Battar, Tupche, Budhsing, Duipipal, Gorsyang, Jiling, Taruka, Balkumari, Ganesthan, Rautbesi, Samundratar, Kabilashpur, Sundaradevi, Thaprek, Belkot, Kakani, Okharpauwa, Thanapati and Sunkhani</li>
            <li>Bhaisepati -> Khokana</li>
          </ul>
        </div>
                    </div>

                    <div class="aside-right">
                        <div class="sidebar-content ">
                            

                            <div class="sidebar-item side-title">
                                <span><h1>Tools</h1></span>
                            </div>

                            <div class="sidebar-item">
                                <a href="{{ url('faq') }}">
                                    <span> <img src="{{asset('/frontcss/img/hover-faq.png')}}">    </span>
                                    <span><h1>FAQ</h1></span>
                                </a>
                            </div>

                            <div class="sidebar-item">
                                <a href="{{ url('contact') }}">
                                    <span><img  src="{{asset('/frontcss/img/hover-call.png')}}">  
                                    
                                    </span>
                                    <span><h1>Call Us</h1></span>
                                </a>
                            </div>

                            <div class="sidebar-item">
                                <a href="{{ url('branch') }}">
                                   <span><img src="{{asset('/frontcss/img/hover-visit.png')}}">  </span>
                                    <span><h1>Visit Our Branch</h1></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

