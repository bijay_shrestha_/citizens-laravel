@extends('front.layout.main.master')
@section('content')

<div class="rates-page list-all-page">
    <section class="home-slider">
        <div class="bannerSlider">
		
            @foreach($banners as $banner)
                <div class="bannerItem" style="background-image: url('{{ url('uploads/banner/'.$banner->image_name) }} ')">            
                </div>
            @endforeach
        </div>
    </section>

    <section class="page-content-area">
        <div class="outer-custom-container">
            <div class="left-big-contianer">
                <!-- Nav Tabs Top-->
                	<div class="emergency">
		<div class="call">
			<div class="call-logo">
			 	<img src="{{asset('/frontcss/img/call.png')}}">
			</div>
			<div class="call-text">
				<h3>Branch / Emergency</h3>
			</div>
		</div>
		<div class="clearfix"></div>

		<div class="branch-tabs">

			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#inside"  role="tab" data-toggle="tab">Inside Valley</a></li>
				<li role="presentation"><a href="#outside" role="tab"   data-toggle="tab">Outside Valley</a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="inside">

					<div class="emergency-calls">
						<div class="row">
						 @foreach ($branches_inside as $inside)
							<div class="col-md-4">
								<div class="product1" style="background-image:url('{{asset('/frontcss/img/branch-img2.png')}}')">
									<div class="caption">
										
										<div class="position">
											<h5 class="b_name">{{ $inside->name }}</h5>
 											<h6 class="branch_manager">Branch Manager: {{ $inside->branch_manager }} </h6>
											<h6 class="b_add"> {{ $inside->address }} </h6>
											<div class="fa1">
												<i class="fa fa-phone" aria-hidden="true"> {{ $inside->contact_no }}</i>
											</div>
											@if($inside->fax!==NULL || $inside->fax!= '')
											<div class="fa3">
												<i class="fa fa-fax" aria-hidden="true"> {{ $inside->fax }} </i>
											</div>
											@endif
											<div class="fa2">
												<i class="fa fa-envelope" aria-hidden="true"> {{ $inside->email }} </i>
											</div>
										</div>
										<div class="dark-image">

										</div>
									</div>
								</div>
							</div>
							@endforeach
							
							
						</div>
					</div>  <!-- emergency-call 1 -->

					
					
				</div>

				<div role="tabpanel" class="tab-pane" id="outside">

					<div class="emergency-calls">
						<div class="row">
						@foreach($branches_outside as $outside)
							<div class="col-md-4">
								<div class="product1" style="background-image:url('{{asset('/frontcss/img/branch-img2.png')}}')">
									<div class="caption">
										<div class="dark-image">
											
										</div>
										<div class="position">
											<h5 class="b_name">{{ $outside->name }}</h5>
                                            	<h6 class="branch_manager">Branch Manager: {{ $outside->branch_manager }} </h6>
												<h6 class="b_add"> {{ $outside->address }}</h6>
											<div class="fa1">
												<i class="fa fa-phone" aria-hidden="true"> {{ $outside->contact_no }} </i>
											</div>
                                                                         @if($inside->fax!==NULL || $inside->fax!= '')
											<div class="fa3">
												<i class="fa fa-fax" aria-hidden="true"> {{ $outside->fax }}</i>
											</div>
											@endif
											<div class="fa2">
												<i class="fa fa-envelope" aria-hidden="true"> {{ $outside->email }} </i>
											</div>
										</div>
									</div>
								</div>
							</div>

							@endforeach

							
						</div>
					</div>  <!-- emergency-call 1 -->
				</div>
			</div>
		</div>
	</div>
                <!-- Nav Tab Content End -->
            </div>
        </div>
    </section>
</div>


@endsection




