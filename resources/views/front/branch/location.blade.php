@extends('front.layout.main.master')
@section('content')

<style>
 #map {
    height: 900px;
    width: 100%;
    margin: 0px;
    padding: 10px;
  }
  .clearfix{
  	padding-bottom: 70px;
  }


  </style>
<div class="branch-page list-all-page">
   
       <section class="home-slider" style="max-height:@if($type=='branch') {{"400"}}  @else {{"0"}} @endif ;overflow: hidden;" id="branch-banner-section">
    <div class="bannerSlider">
        @foreach($branch_banners as $banner)
       @if($banner['image_name']== NULL || $banner['image_name']=='' )

              <div class="bannerItem bannerItemInside {{ $banner['design'] }}" style="background-image: url('{{ url('uploads/banner/'.$banner['background']) }}')">
                
                 <div class="text-container">
                    <div class="text-inner">
                        <div class="right-img">
                            <img src="{{ url('/') }}/uploads/banner/{{ $banner['logo'] }}">
                        </div>
                        <div class="right-text {{ $banner['color'] }} bottom-{{ $banner['bottom-color'] }} " >
                            <h1 >{!! $banner['top_box_description'] !!} </h1>
                            <p>{{ $banner['bottom_box_description'] }}</p>
                        </div>
                    </div>
                </div>
                @if($banner['type']==1)
                <div class="banner-water-mark">
                   <a href="https://www.freepik.com/" target="_blank">Vectors graphics designed by Freepik</a>                              
                </div>
                @else

                @endif
            </div>
               
        @else
       
                
                 <div class="cz_banner_item {{ $banner['design'] }}" style="background-image: url('{{ url('') }}uploads/banner/{{ $banner['background'] }} ')">
                <div class="image-container">
                    <img src="{{ url('') }}uploads/banner/{{ $banner['image_name'] }}">
                </div>
                
                <div class="text-container">
                    <div class="text-inner">
                        <div class="right-img">
                        <img src="{{ url('') }}uploads/banner/{{ $banner['logo'] }}">
                        </div>
                        <div class="right-text {{  $banner['color'] }} bottom-{{ $banner['bottom-color'] }} " >
                            <h1 >{{ $banner['top_box_description']  }}</h1>
                            <p>{{ $banner['bottom_box_description']  }}</p>
                        </div>
                    </div>
                </div>
                
               
            
        </div>
                @endif
        @endforeach
        
    </div>
</section>

    <section class="home-slider" style="max-height:{{($type=='atm')?'400':'0'}};overflow: hidden;" id="atm-banner-section">
        <div class="bannerSlider">
        
        @foreach($atm_banners as $banner)
        @if($banner['image_name']== NULL || $banner['image_name']=='' )
              <div class="bannerItem bannerItemInside {{ $banner['design'] }}" style="background-image: url(' {{ url('uploads/banner/'.$banner['background']) }}')">
                
                 <div class="text-container">
                    <div class="text-inner">
                        <div class="right-img">
                        <img src="{{ url('') }}uploads/banner/{{ $banner['logo']  }}">
                        </div>
                        <div class="right-text {{ $banner['color'] }} bottom- {{ $banner['bottom-color'] }} " >
                            <h1> {!! $banner['top_box_description']  !!} </h1>
                            <p> {{ $banner['bottom_box_description']  }} </p>
                        </div>
                    </div> {{-- here i--}}
                </div>
                @if($banner['type']==1)
                <div class="banner-water-mark">
                   <a href="https://www.freepik.com/" target="_blank">Vectors graphics designed by Freepik</a>                              
                </div>
                @else
                @endif
            </div>
               
        @else
       
                
                 <div class="cz_banner_item {{ $banner['design'] }}" style="background-image: url(' {{ url('') }}uploads/banner/{{  $banner['background'] }}')">
                <div class="image-container">
                    <img src="{{ url() }} uploads/banner/{{ $banner['image_name'] }}">
                </div>
                
                <div class="text-container">
                    <div class="text-inner">
                        <div class="right-img">
                            <img src="{{ url('') }}uploads/banner/{{ $banner['logo'] }}">
                        </div>
                        <div class="right-text {{ $banner['color'] }} bottom-{{ $banner['bottom-color'] }} " >
                            <h1 >{{ $banner['top_box_description'] }}</h1>
                            <p> {{ $banner['bottom_box_description'] }}</p>
                        </div>
                    </div>
                </div>
                
               
            
        </div>
                @endif
        @endforeach
        
    </div>
</section>

    <section class="page-content-area">
        <div class="outer-custom-container">
            <div class="left-big-contianer">

                <div class="branch-map">
                    <div id="map"></div>
                </div>
                
                <div class="page-with-side-bar">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="page-with-side-header">
                                <h2>Branch and Location</h2>
                            </div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="dropdown @if($type=='branch') {{ 'active'}} @endif">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Branch <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#branch-inside" onclick="switch_image('branch')" role="tab" data-toggle="tab">Inside</a></li>
                                        <li><a href="#branch-outside" onclick="switch_image('branch')" role="tab" data-toggle="tab">Outside</a></li>
                                    </ul>
                                </li> 
                                
                                <li class="dropdown @if($type=='atm') {{'active'}} @endif">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">ATM <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#atm-inside" onclick="switch_image('atm')" role="tab" data-toggle="tab">Inside</a></li>
                                        <li><a href="#atm-outside" onclick="switch_image('atm')" role="tab" data-toggle="tab">Outside</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="#corporate" role="tab" data-toggle="tab">Corporate Location</a>
                                </li>
                            </ul>            
                        </div>
                        
                        
                        
                        <div class="col-sm-9">
                            <!-- Nav Tab Content -->
                            <div class="tab-content">
                                <div class="tab-pane fade in @if($type=='branch') {{ 'active' }}  @endif  " id="branch-inside">
                                    <div class="page-with-side-header">
                                        <h2>Inside</h2>
                                    </div>
                                    @foreach ($branches_inside as $inside)
                                        <div class="branch-item">
                                            <div class="banner-border">
                                                
                                                <div class="name">
                                                 <a href="#map" onclick="initMap('{{ $inside['id'] }}','{{ $inside['latitude'] }}','{{ $inside['longitude'] }}','{{ $inside['for_section'] }}','{{ $inside['name'] }}','{{ $inside['email'] }}','{{  $inside['branch_manager'] }}','{{ $inside['contact_no'] }}',)">
                                                    <h2>{{ $inside['name'] }}</h2>
                                                    </a>
                                                    <h2>{{ $inside['branch_manager'] }}</h2>
                                                </div>
                                                <div class="contact">
                                                    <div class="phone-email">
                                                        @if ($inside['fax']!= NULL || $inside['fax'] != '')
                                                        <div class="phone-number">
                                                            <i class="fa fa-fax" aria-hidden="true"> <span>{{ $inside['fax'] }} </span></i>
                                                            </div>
                                                        @else
                                                         
                                                        @endif
                                                            <div class="phone-number">
                                                           
                                                            <i class="fa fa-phone" aria-hidden="true"> <span> {{ $inside['contact_no'] }}</span></i>
                                                        </div>
                                                        <div class="email">
                                                            <i class="fa fa-envelope" aria-hidden="true">  <span>{{ $inside['email'] }}</span></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                   @endforeach 
                                </div>
                                
                                <div class="tab-pane fade in" id="branch-outside">
                                    <div class="page-with-side-header">
                                        <h2>Outisde</h2>
                                    </div>
                                    
                                    @foreach ($branches_outside as $inside)
                                        <div class="branch-item">
                                            <div class="banner-border">
                                               
                                                <div class="name">
                                                   <a href="#map" onclick="initMap('{{ $inside['id'] }}','{{ $inside['latitude'] }}','{{ $inside['longitude'] }}','{{ $inside['for_section'] }}','{{ $inside['name'] }}','{{ $inside['email'] }}','{{ $inside['branch_manager'] }}','{{ $inside['contact_no'] }}',)">
                                                   <h2>{{ $inside['name'] }}</h2></a>

                                                    <h2>{{ $inside['branch_manager'] }}</h2>
                                                </div>
                                                <div class="contact">
                                                    <div class="phone-email">
                                                       @if ($inside['fax']!= NULL || $inside['fax'] != '')
                                                        <div class="phone-number">
                                                            <i class="fa fa-fax" aria-hidden="true"> <span>{{ $inside['fax'] }}</span></i>
                                                            </div>
                                                        @else
                                                         
                                                        @endif                                                       
                                                            <div class="phone-number">
                                                           
                                                            <i class="fa fa-phone" aria-hidden="true"> <span> {{ $inside['contact_no'] }}</span></i>
                                                        </div>
                                                        <div class="email">
                                                            <i class="fa fa-envelope" aria-hidden="true">  <span>{{ $inside['email'] }}</span></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                  @endforeach
                                
                                </div>
                                
                                <div class="tab-pane fade in @if($type=='atm') {{ 'active'}} @endif" id="atm-inside">
                                    <div class="page-with-side-header">
                                        <h2>Inside</h2>
                                    </div>
                                    @foreach ($atms_inside as $inside)
                                        <div class="branch-item">
                                            <div class="banner-border">
                                                
                                                <div class="name">
                                                   <a href="#map" onclick="initMap('{{  $inside['id'] }}','{{ $inside['latitude'] }}','{{ $inside['longitude'] }}','{{ $inside['for_section'] }}','{{ $inside['name'] }}','{{ $inside['email'] }}','{{ $inside['branch_manager'] }}','{{ $inside['contact_no'] }}',)">
                                                   <h2>{{ $inside['name'] }}</h2></a>
                          
                                                </div>
                                                <div class="contact">
                                                    <div class="phone-email">
                                                       @if ($inside['fax']!= NULL || $inside['fax'] != '')
                                                         <div class="phone-number">
                                                          <h4>{{ $inside['address'] }}</h4>
                                                            </div>
                                                            @else                                                        
                                                       
                                                            @endif
                                                            <div class="phone-number">
                                                           
                                                            <i class="fa fa-phone" aria-hidden="true"> <span> {{ $inside['contact_no'] }}</span></i>
                                                        </div>
                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                  @endforeach
                                </div>
                                
                                <div class="tab-pane fade in" id="atm-outside">
                                    <div class="page-with-side-header">
                                        <h2>Outisde</h2>
                                    </div>
                                    @foreach ($atms_outside as $inside)
                                        <div class="branch-item">
                                            <div class="banner-border">
                                                
                                                <div class="name">
                                                    <a  href="#map"onclick="initMap('{{ $inside['id'] }}','{{ $inside['latitude'] }}','{{ $inside['longitude'] }}','{{ $inside['for_section'] }}','{{ $inside['name'] }}','{{ $inside['email'] }}','{{ $inside['branch_manager'] }}','{{ $inside['contact_no'] }}',)">
                                                    <h2>{{ $inside['name'] }} </h2></a>
                         

                                                </div>
                                                <div class="contact">
                                                            <div class="phone-email">
                                                       @if($inside['fax']!= NULL || $inside['fax'] != '')
                                                        <div class="phone-number">
                                                          <h4>{{ $inside['address'] }}</h4>
                                                            </div>
                                                        @else                                                       
                                                        @endif
                                                       
                                                            <div class="phone-number">
                                                           
                                                            <i class="fa fa-phone" aria-hidden="true"> <span> {{ $inside['contact_no'] }}</span></i>
                                                        </div>
                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                  @endforeach
                                </div>
                                
                                <div class="tab-pane fade in" id="corporate">
                                    <h2>CITIZENS BANK INTERNATIONAL LIMITED</h2>
                                    <ul>
                                        <li>P.O.BOX: 19681</li>
                                        <li>Narayanhiti, Kathmandu</li>
                                        <li>TEL:01-4427842/43/24</li>
                                        <li>FAX: 01-4427044</li>
                                        <li>EMAIL:info@ctznbank.com</li>
                                        <li>SWIFT: CTZNNPKA</li>
                                        <li>URL: www.ctznbank.com</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                            
                        
                    </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
 
                </div>
                
                
                <!-- Nav Tabs Top-->
                

                <!-- Nav Tab Content -->
                
            </div>
        </div>
    </section>
</div>





<script>

  var map;
  var infoWindow;

  function initMap(id=null, lat=null, long=null, section = null, name = null, email = null, branch_manager = null, contact = null)
   {
    console.log(lat);
    console.log(long);
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 7,
      center: new google.maps.LatLng(28.5866495,84.3710567),
      mapTypeId: 'roadmap'
    });

infoWindow = new google.maps.InfoWindow({
        maxWidth: 420
    });



  var icons = {
     
      branch: {
        icon: "{{ url('frontcss/img/branch_location.png')}} "
      },
       atm: {
        icon: "{{ url('frontcss/img/atm_location.png') }}"
      },
       corporate: {
        icon: "{{ url('frontcss/img/mainbranch.png') }}"
      },
      
    };


    function addMarker(feature) {
   
  var marker = new google.maps.Marker({
        position: feature.position,
        icon: icons[feature.type].icon,
         draggable:false,
    //title:"Drag me!",
       title :feature.title,
       // label: feature.title,
        map: map
      });
      var infowindow = new google.maps.InfoWindow();
       
                 
                             google.maps.event.addListener(marker, 'click', function(e) {

               
                                      // infowindow.setContent(this.html);
                                        // infowindow.setPosition(event.latLng);
                                        marker.setPosition(e.latLng);
                                       infowindow.setContent(feature.title );
                                       //label: feature.title;
                                infowindow.open(map, marker);

                                        infowindow.open(map, this);


                                    });
                        
                       
      
    }




// function HandleInfoWindow(latLng, content) {
//     infoWindow.setContent(content);
//     infoWindow.setPosition(latLng);
//     infoWindow.open(map);
// }

     if(id == null)
     {
        var features = [
    @foreach($geolocations as $geolocation)
      {
        position: new google.maps.LatLng( {{ $geolocation['latitude'] }}, {{ $geolocation['longitude'] }}),
        title:'{{ $geolocation['name'] }} ',
        label:'{{ $geolocation['name'] }} <br>Branch Manager: {{ $geolocation['branch_manager'] }} <br>{{ $geolocation['email'] }} <br> {{ $geolocation['contact_no'] }}',
     
      @if($geolocation['for_section']=='Bank Branches')
        type:'branch'
      @elseif($geolocation['for_section']=='ATM locations')
        type: 'atm'
      @else
          type: 'corporate'
          @endif
      },
      
  @endforeach
          ];

     }
     else
     {
      // console.log(section);
      if(section == 'Bank Branches'){
        var features = [{
        position: new google.maps.LatLng(lat,long),
        title:name,
        
        type:'branch'
       
          },
          ];
      }
     
      else{
          var features = [{
        position: new google.maps.LatLng(lat,long),
        title:name,
        
        type:'atm'
       
          },
          ]
          
      }
      
          
     }
        


    for (var i = 0, feature; feature = features[i]; i++) {
       addMarker(feature);
    }
}


</script>
<script>
    function switch_image(type)
    {
       if(type ==  'branch')
       {
           $('#branch-banner-section').css('max-height', '400px');
           $('#atm-banner-section').css('max-height', '0px');
       }
       else{
           $('#atm-banner-section').css('max-height', '400px');
           $('#branch-banner-section').css('max-height', '0px');
       }
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfh5Dkh6z8u_cpzZP6c3Dy3gTqiDPH-iM&callback=initMap"> </script>

@endsection