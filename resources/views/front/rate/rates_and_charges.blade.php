@extends('front.layout.main.master')

@section('content')
<div class="rates-page list-all-page">
    <section class="home-slider">
        <div class="bannerSlider">
            @foreach($banners as $banner)
                 <div class="bannerItem bannerItemInside {{$banner['design']}}" style="background-image: url('{{url('uploads/banner/'.$banner['image_name'])}}')">
                
                 <div class="text-container">
                    <div class="text-inner">
                         <div class="right-img">
                            
                            <img src="{{url('/')}}/uploads/banner/{{$banner['logo'] }}">
                        </div>
                         <div class="right-text {{$banner['color']}} bottom-{{$banner['bottom-color']}} " >
                            <h1 >{!! $banner['top_box_description'] !!}</h1>
                            <p>{!! $banner['bottom_box_description'] !!}</p>
                        </div>
                    </div>
                </div>  
                
            </div>
            @endforeach
        </div>
    </section>
 
    <section class="page-content-area">
        <div class="outer-custom-container">
            <div class="left-big-contianer">
                <div class="page-with-side-bar">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="page-with-side-header">
                                <h2>Rates</h2>
                            </div>
                            
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="dropdown {{($active == 'ir')? 'active':''}}">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Interest Rates <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#sub-deposit" role="tab" data-toggle="tab">Account & Deposit</a></li>
                                        <li><a href="#sub-loan" role="tab" data-toggle="tab">Loan</a></li>
                                    </ul>
                                </li>  
                                <li class="{{($active == 'irs')? 'active' :''}}">
                                    <a href="#rate-spread" role="tab" data-toggle="tab">Interest Rate Spread</a>
                                </li>
                                <li class="{{($active == 'br')? 'active' :''}}">
                                    <a href="#basse-rate" role="tab" data-toggle="tab">Base Rates</a>
                                </li>
                                
                                <li class="{{($active == 'ex')? 'active' :''}}">
                                    <a href="#exchange-rate" role="tab" data-toggle="tab">Exchange Rate</a>
                                </li>
                                <li class="dropdown {{($active == 'fc')? 'active' :''}}">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Fees and Commission <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        @foreach($feecommisions as $feecommision)
                                           
                                            @if ($feecommision['name'] =='Remittance')
                                            	<li class="active">
                                            @else
                                            	<li>
                                            @endif
                                                <a href="#{{$feecommision['id']}}_fees" role="tab" data-toggle="tab">{{$feecommision['name']}}</a>
                                            </li>
                                            
                                            @endforeach
                                            <li>
                                                    <a href="#charges" role="tab" data-toggle="tab">Fees and Charges</a>
                                            </li>
                                        </ul>
                                </li>  
                            </ul>
                        </div>
                        
                        <div class="col-sm-9">
                            <!-- Nav Tab Content -->
                            
                            <div class="tab-content">
                                <div class="tab-pane fade in {{($active == 'ir')?'active':''}}" id="sub-deposit">
                                    <div class="page-with-side-header">
                                        <h2>Account and Deposit   <span style="font-weight:normal;font-size:14px;">  - TO BE EFFECTIVE FROM  7th Baishakh, 2076  (20th April, 2019)</span></h2>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr>.
                                                <th>ACCOUNT NAME</th>
                                                <th>MINIMUM BALANCE</th>
                                                <th>INTEREST RATE (P.A.)</th>
                                               
                                            </tr>
                                          
                                           @foreach($counts as $count ) 
                                              @if($count['count'] >1 )
                                           
                                            
                                            
                                            <tr>
                                                <th>{{$count['name']}}</th>
                                                <th></th>
                                                <th></th>
                                                @foreach($accounts as $account)
                                                @if($account['name'] == $count['name'])
                                                <tr>
                                                    <td>{{$account['rate_name']}}</td>
                                                    <td>{{$account['per_annum_min']}}</td>
                                                    <td>{{$account['per_annum_max']}}</td>
                                                 </tr> 
                                                @endif
                                                 @endforeach 
                                            </tr>  
                                           
                                            @else
                                             <tr>
                                                <td><strong>{{$count['name']}}</strong></td>
                                          
                                                    <td>{{$count['fixed_rate']}}</td>
                                                 
                                            </tr> 
                                            @endif 
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                                
                                <div class="tab-pane fade in" id="sub-loan">
                                    <div class="page-with-side-header">
                                        <h2>Loan</h2>
                                    </div>
                                       <div class="co-site-accordian-container">
                                        <div class="panel-group" id="accordion">
                                            
                                              @php $i = 0; @endphp 
                                              @foreach($loan_parents as $parent)
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#{{$parent['id']}}">{{$parent['name']}}</a>
                                                </div>
                                                <div id="{{$parent['id']}}" class="panel-collapse {{($i==0)?'in':''}} @php $i=1; @endphp  collapse">
                                                   
                                                    <div class="panel-body">
                                                        <div class="table-responsive">
                                                            
                                                           <table class="table">
                                                                <tr>
                                                                    <th>LOAN NAME</th>
                                                                    
                                                                    <th>MIN RATE</th>
                                                                    <th>MAX RATE</th>
                                                                                                                                    </tr>

                                                                @foreach($loans as $loan)
                                                                <tr> 
                                                                  @if($loan['parent_id'] == $parent['id'])      
                                                                    <td>{{$loan['loan_name']}}</td>
                                                                    @foreach ($loan_rates as $interest)
	                                                                    @if($interest['loan_id'] == $loan['loan_id'])
	                                                                    <td>{{$interest['general_per_annum_min']}}</td>
	                                                                    <td>{{$interest['general_per_annum_max']}}</td>
	                                                                   
	                                                                    @endif
                                                                	@endforeach
                                                                  @endif
                                                                
                                                                </tr>  
                                                                @endforeach
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>    
                                         @endforeach
                           <div class="panel">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#other">Other Loans</a>
                                </div>
                                <div id="other" class="panel-collapse  collapse">
                                   
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            
                                           <table class="table">
                                                <tr>
                                                    <th>FIX RATE</th>
                                                    <th>MIN RATE</th>
                                                    <th>MAX RATE</th>
                                                    
                                                </tr>

                                                @foreach($loans as $loan)
                                                <tr> 
                                                   @if($loan['parent_id'] == '' || $loan['parent_id'] == null || $loan['parent_id'] == 0)     
                                                    <td>{{$loan['loan_name']}}</td>
                                                   	@foreach($loan_rates as $interest)
                                                    	@if($interest['loan_id'] == $loan['loan_id'])
		                                                    <td>{{$interest['general_per_annum_min']}}</td>
		                                                    <td>{{$interest['general_per_annum_max']}}</td>
                                                       @endif
                                                    @endforeach                                                              
                                                	@endif
                                                </tr>  
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="tab-pane fade in {{($active == 'irs')?'active':''}}" id="rate-spread">
                                    <div class="page-with-side-header">
                                        <h2>Interest Rate Spread</h2>
                                    </div>
                                     <div class="co-site-accordian-container">
                                        <div class="panel-group" id="accordion">
                                            @php $active_inside_spread = 0; @endphp
                                              @for($i = $max_spread_date+1; $i>=$min_spread_date; $i--)  
                                             
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#{{$i}}_spread">Fiscal Year {{($i-1)}}/{{($i)}}</a>
                                                </div>
                                                <div id="{{$i}}_spread" class="panel-collapse {{($active_inside_spread == 0)?'in':'' }} collapse">
                                                    @php $active_inside_spread++; @endphp
                                                    <div class="panel-body">
                                                        <div class="table-responsive">
                                                            
                                                            <table class="table">
                                                            <tr>
                                                                <th>INTEREST RATE SPREAD</th>
                                                                <th>RATE</th>
                                                                
                                                            </tr>
                                                            @foreach($interestratespreads as $rate)
                                                                @if(($rate['fiscal_rate'] == $i && $rate['fiscal_month'] < '04') || ($rate['fiscal_rate'] == $i-1 && $rate['fiscal_month'] > '03'))
                                                            
                                                            
                                                            <tr>
                                                                <td>{{$rate['name']}}</td>
                                                                <td>{{$rate['interest_rate_spread_rate']}}%</td>
                                                               
                                                            </tr>
                                                            	@endif
                                                            @endforeach
                                                          
                                                        </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>    
                                            @endfor
                                           
                                        </div>
                                    </div>
                                </div> 
                              
                                <div class="tab-pane fade in {{($active == 'br')?'active':''}}" id="basse-rate">
                                    <div class="page-with-side-header">
                                        <h2>Base Rates</h2>
                                    </div>
                                    @php $base_active = 0; @endphp
                                     <div class="co-site-accordian-container">
                                        <div class="panel-group" id="accordion1">
                                            @for($d=$max_date+1; $d>=$min_base_date; $d--)
                                            <div class="panel">
                                                <div class="panel-heading" >
                                                    <a data-toggle="collapse" data-parent="#accordion1" href="#{{$d}}_base">Fiscal Year {{($d-1)}}/{{$d}}</a>
                                                </div>
                                                <div id="{{$d}}_base" class="panel-collapse {{($base_active == 0)?'in':''}}  collapse">
                                                 @php $base_active = 1; @endphp
                                                    <div class="panel-body">
                                                        <div class="table-responsive">
                                                            
                                                            <table class="table">
                                                            <tr>
                                                                <th>BASE RATE</th>
                                                                <th>RATE</th>
                                                                
                                                            </tr>
                                                            @foreach($base_rates as $rate) 
                                                                @if(($rate['fiscal_rate'] == $d && $rate['fiscal_month'] < '04') || ($rate['fiscal_rate'] == $d-1 && $rate['fiscal_month'] > '03'))
	                                                            <tr>
	                                                                <td><a href="{{$rate['link']}}">{{$rate['name']}}</a></td>
	                                                                <td>{{$rate['base_rate']}}%</td>
	                                                               
	                                                            </tr>
	                                                            @endif
                                                            @endforeach
                                                          
                                                        </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>    
                                        @endfor
                                           
                                        </div>
                                    </div>
  
                                </div> 
                                
                                <div class="tab-pane fade in" id="charges">
                                    <div class="page-with-side-header">
                                        <h2>Fees and Charges</h2>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr>
                                                <th>Particulars</th>
                                                <th>Rupee Card</th>
                                                <th>USD card</th>
                                            </tr>
            
                                            @foreach($feecharges as $feecharge)
                                                <tr>
            
                                                    <td>$feecharge['fee_charge_name']}}</td>
                                                    <td>$feecharge['rupee']}}</td>
                                                    <td>$feecharge['usd']}}</td>
                                                </tr>
            
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                                
                                <div class="tab-pane fade in {{($active == 'ex')?'active':''}}" id="exchange-rate">
                                    <div class="page-with-side-header">
                                        <h2>Exchange Rate</h2>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr>
                                                <th>Currency</th>
                                                <th>Units</th>
                                                <th>Buying Cash Below Deno 50</th>
                                                <th>Buying</th>
                                                <th>Selling</th>
                                              
                                            </tr>
            
                                            @foreach($forexs as $forex)
                                                <tr>
                                                    <td>{{$forex['currency']}}</td>
                                                    <td>{{$forex['unit']}}</td>
                                                    <td>{{$forex['cash_buying']}}</td>
                                                    <td>{{$forex['buying']}}</td>
                                                    <td>{{$forex['selling']}}</td>
                                                   
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>    
                                </div>
                                
                                
                                
                                @foreach($feecommisions as $feecommision)
                                    @if ($active == 'fc' && $feecommision['name'] =='Remittance')
                                        <div class="tab-pane fade in active" id="{{$feecommision['id']}}_fees">
                                    @else
                                        <div class="tab-pane fade in" id="{{$feecommision['id']}}_fees">
                                    @endif       
                                            <div class="table-responsive">
                                                {{$feecommision['table']}}
                                            </div>
                                        </div>
                                @endforeach
                                </div>
                                
                        
                              
                            </div>
                        </div>
                        
                        
                                                   
                        <!-- Tab panes -->
                            
                    <!-- Nav Tabs Top-->
                </div>
                </div>
            </div>
        </div>
    </section>
        
                

                


@endsection