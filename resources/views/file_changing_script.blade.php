<script type="text/javascript">

  $( "#download_link" ).change(function() {
      //uploadReady();
      uploadfile(this);
  });

 $("#upload_image").change(function(){
    uploadimage(this);
 });

function uploadimage(thefile)
{
    var form_data = new FormData();
    form_data.append('upload_image', thefile.files[0]);
    form_data.append('directory', directory);
    form_data.append('_token', '{{csrf_token()}}');
    $('#image-status').html("Image is uploading....");
     $.ajax({
            url: "{{url('/admin/accounts/upload/image')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                  var err = '';
                  data.errors['upload_image'].forEach (function (element) {
                    err += element + '. ';
                  });
                   $('#image-status').css('color', 'red').html(err);
                } else {
                  $('#image-status').html('');
                  $('#upload_image').hide();
                  $('#image_name').val(data.filename);
                  $('#upload_image_name').html(data.originalname);
                  $('#upload_image_name').show();
                  $('#change-image').css('display', 'block');
                  //  $('#file_name').val(data);
                  //  $('#preview_image').attr('src', '{{asset('uploads')}}/' + data);
                }
               
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
              //  $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
            }
        });

}
function uploadfile(thefile)
{
	console.log(directory);
    var form_data = new FormData();
    form_data.append('download_link', thefile.files[0]);
    form_data.append('directory', directory);
    form_data.append('_token', '{{csrf_token()}}');
    $('#file-status').html("File is uploading....");
     $.ajax({
            url: "{{url('/admin/accounts/upload/file')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                   $('#file-status').css('color', 'red').html(data.errors['download_link']);
                } else {
                  $('#file-status').html('');
                  $('#download_link').hide();
                  $('#download_form_link').val(data.filename);
                  $('#upload_download_link').html(data.originalname);
                  $('#upload_download_link').show();
                  $('#change-file').css('display', 'block');
                  //  $('#file_name').val(data);
                  //  $('#preview_image').attr('src', '{{asset('uploads')}}/' + data);
                }
               
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
              //  $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
            }
        });

}

  $('#change-file').click(function () {
    var filename = $('#download_form_link').val();
    if (confirm('Are you sure you to want to delete this file?')) {
       $.post("{{url('/admin/accounts/delete/file')}}",
        {'filename': filename, 'directory' : directory, '_token':"{{csrf_token()}}"}, function (result) {
           if (result.success == "yes") {
             $('#download_form_link').val('');
             $('#download_link').val('');
             $('#upload_download_link').html('');
             $('#upload_download_link').hide();
             $('#change-file').hide();
             $('#download_link').show();
          } else {
            alert('Something went wrong.');
          }
        });
    }
  }); 

   $('#change-image').click(function () {
    var filename = $('#image_name').val();
    if (confirm('Are you sure you to want to delete this image?')) {
       $.post("{{url('/admin/accounts/delete/file')}}",
        {'filename': filename, 'directory' : directory, '_token':"{{csrf_token()}}"}, function (result) {
           if (result.success == "yes") {
             $('#image_name').val('');
             $('#upload_image').val('');
             $('#upload_image_name').html('');
             $('#upload_image_name').hide();
             $('#change-image').hide();
             $('#upload_image').show();
          } else {
            alert('Something went wrong.');
          }
        });
    }
  }); 
</script>