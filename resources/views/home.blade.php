<script src="{{ url('/vendors/Chart.js/dist/Chart.bundle.js') }}"></script>
<script src="{{ url('/img/frontend/js/Utils.js') }}"></script>

@include('layout.header')
@php 
$banners=$data['banners'];

$data_banners = [];
for ($i = 0; $i < count($banners); $i++) {
    foreach ($banners[$i] as $index_banner => $data_banner ) {
        $data_banners[$i][$index_banner] = $data_banner; 
    }
}
$banners = $data_banners;
$advertisements=$data['advertisements'];
$products=$data['products'];
$services=$data['services'];
$blog=$data['blogs'];
$forexs=$data['forexs'];
$stockvalue=$data['stockvalue'];
$forexdata=$data['forexdata'];
@endphp
<section class="home-slider">
    <div class="bannerSlider">
        @foreach($banners as $banner)
          @if(@$banner['image_name']== NULL || $banner['image_name']=='' )
            <div class="cz_banner_item  {{ $banner['design'] }}  homewatermark" style="background-image: url('{{url('uploads/banner/'.$banner['background']) }}')">
                 <div class="text-container">
                    <div class="text-inner">
                       @if($banner['logo'])
                       <div class="right-img">
                       <img src="{{ url('uploads/banner/'.$banner['logo']) }}">
                        </div>
                        @endif
                        
                        
                        <div class="right-text {{ $banner['color'] }}  bottom- {{ $banner['bottom-color'] }}  " >
                            <h1 >{!! $banner['top_box_description'] !!} </h1>
                            <p> {!! $banner['bottom_box_description'] !!} </p>
                        </div>
                    </div>
                </div>  
                
                <a class="home-ban-more" href="{{ $banner['link'] }}"> See More</a>
                @if($banner['type']==1)
                <div class="banner-water-mark">
                 
                    <a href="{{ $banner['freepik_link'] }}" target="_blank">Link for image</a>                              
                </div>
                @else
                @endif
            </div>
                @else
            <div class="cz_banner_item  {{ $banner['design'] }}" style="background-image: url('{{ url('uploads/banner/'.$banner['background']) }} ')">
                <div class="image-container">
                    <img src="{{ url('uploads/banner/'.$banner['image_name']) }}" class="@if($banner['type'] == 1) {{ 'make_image_top' }} @else {{ 'image'}} @endif ">
                  
                </div>
                
                <div class="text-container">
                    <div class="text-inner">
                        <div class="right-img">
                            
                            <img src="{{ url('uploads/banner/'.$banner['logo']) }} ">
                        </div>
                       <div class="right-text {{ $banner['color'] }} bottom-{{ $banner['bottom-color'] }} " >
                            <h1 >{!! $banner['top_box_description'] !!} </h1>
                            <p>{!!  $banner['bottom_box_description'] !!}</p>
                        </div>
                    </div>
                </div>
                
                <a class="home-ban-more" href="{{ $banner['link'] }}"> See More</a>
            
        </div>
        @endif
        
        @endforeach 
        @foreach($banners as $banner)
            <div class="bannerItem" style="background-image: url('{{ url('uploads/banner/'.$banner['image_name']) }} ')">
                <a href="{{ $banner['link'] }}"> See More</a>
            </div>
        @endforeach
    </div>
</section>

<section class="stock-info-section">
    <div class="outer-custom-container">
        <div class="ci-md-2">
            <div class="left-quick-links">
                <div class="side-item">
                        <span><i class="fa fa-product-hunt" aria-hidden="true"></i></span>
                        <span>Products</span>
                </div>
         
                <div class="side-item">
                    <a href=" {{ url('home/online') }}">
                    
                        {{--   http://pagodalabs.com.np/citizens/themes/citizens/ --}}
                        {{-- new url --}} {{-- url('img/frontend/icons') --}}

                      {{--   http://pagodalabs.com.np/citizens/themes/citizens/ --}}
                    {{-- <img src="{{url('/')}}/img/frontend/img/icons/menu/online_service.png" alt=""> --}}
                        <span><img src="{{url('/')}}/img/frontend/img/icons/menu/online_service.png" alt=""></span> {{-- remain --}}
                        <span>Online Services</span>
                    </a>
                </div>

                <div class="side-item">
                    <a href="{{ url('accounts') }}">
                        <span><img src="{{url('/')}}/img/frontend/img/icons/menu/simple_pig.png" alt=""></span> {{-- remain --}}
                        <span>Deposit</span>
                    </a>
                </div>

                <div class="side-item">
                    <a href="{{ url('loans') }}">
                        <span><img src="{{url('/')}}/img/frontend/img/icons/menu/hand.png" alt=""></span>
                        <span>Loan</span>
                    </a>
                </div>

                <div class="side-item">
                    <a href="{{ url('credit_cards') }}">
                        <span><img src="{{url('/')}}/img/frontend/img/icons/menu/card.png" alt=""></span>
                        <span>Card</span>
                    </a>
                </div>

                <div class="side-item">
                    <a href="{{ url('pages/remit') }}"> {{-- idk ke ho yo--}}
                        <span><img src="{{url('/')}}/img/frontend/img/icons/menu/remit.png" alt=""></span>
                        <span>Remittance</span>
                    </a>
                </div>

                <!--<div class="side-item">-->
                <!--    <a href="<?php //// echo site_url('banking')?>">-->
                <!--        <span><img src="<?php //echo theme_url()?>assets/img/icons/menu/int_pig.png" alt=""></span>-->
                <!--        <span>Electronic Banking</span>-->
                <!--    </a>-->
                <!--</div>-->

                <div class="side-item">
                    <a href=" {{ url('home/others') }} ">
                        <span><img src="{{url('/')}}/img/frontend/img/icons/menu/other.png" alt=""></span>
                        <span>Other Services</span>
                    </a>
                </div>
                
                
            </div>
        </div>

        <div class="ci-md-8">
            <div class="home-center-stock">
                <div class="ci-md-6 stock-image-container">
                   @foreach($advertisements as $advertisement)
                    <a href="{{ $advertisement['link'] }}">
                        <div class="offers-image" style="background-image: url('{{ @url('uploads/media/'.$advertisement['background']) }}')">

                             <div class="image-container">
                                 @if($advertisement['image']== NULL || $advertisement['image']=='' )
                                 @else
                                <img src="{{ url('uploads/media/'.$advertisement['image']) }}">
                                @endif
                            </div>
                           
        
                            <div class="text-container">
                                <div class="text-inner">
                                    <div class="right-img">
                                        <img src="{{ url('uploads/media/'.$advertisement['logo']) }}">
                                    </div>
                                    <div class="right-text">
                                        <h1>{{ $advertisement['title'] }} </h1>
                                        <p> {{ $advertisement['short_text'] }} </p>
                                    </div>
                                </div>
                            </div>
                        </div>
            
                    </a>
                  @endforeach
                    
              <!--      @foreach($advertisements as $advertisement)
                    <a href="{{ url($advertisement['link']) }}  "><div class="image-block" style="background: url('{{ url('uploads/media/'. $advertisement['image']) }}"></div></a>
                    
                    @endforeach
                </div>
                <div class="ci-md-6 stock-numbers-container">
                    <div class="stock-exchange">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#foreign-exchange" aria-controls="home" role="tab" data-toggle="tab">
                                    <span>FOREX</span>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#stock-exchange" aria-controls="profile" role="tab" data-toggle="tab">
                                    <span>STOCK</span>
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="foreign-exchange">
                                <div id="curve_chart" class="home-center-chart"></div>
                                <div class="fortex-rate table-responsive">
                                    <div class="table-wrapper-fortex">
                                        <table class="table">
                                        <tr>
                                            <td>Currency</td>
                                            <td>Buying</td>
                                            <td>Selling</td>
                                        </tr>
                                        {{-- Untouched stock and forex --}}
                                        @foreach($forexs as $forex)
                                            <tr>
                                                <td> <a href="{{ url('forexes') }}"> {{ $forex->currency }} </a></td>  
                                                <td>{{ $forex->buying }} </td>
                                                <td>{{ $forex->selling }} </td>
                                            </tr>
                                        @endforeach
                                       
                                    </table>
                                    </div>
                                    
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="stock-exchange">
                                <!--<div class="citizenstock home-center-chart"></div>-->
                                <div id="linechart" class="home-center-chart"></div>
                                <div class="fortex-rate table-responsive">
                                    <table class="table stock">
                                        <tr>
                                            <td>SYMBOL</td>
                                            <td>DATE</td>
                                            <td>CLOSING PRICE</td>
                                            
                                        </tr>

                                        <tr>
                                            @foreach($stockvalue as $stock)
                                            <td><a href="http://www.nepalstock.com/company/display/348">CZBIL</a></td>
                                            <td> {{ $stock['created_time'] }} </td>
                                            <td> {{ $stock['closing_price'] }}</td>
                                            @endforeach
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
       
        <div class="ci-md-1">
            <div class="right-tool-links">
                <div class="side-item">
                    <span>Online Services</span>
                </div>
                <!--<div class="side-item">-->
                <!--    <a href="<?php // echo site_url('home/application_form') ?>">-->
                <!--        <span><img src="<?php //echo theme_url() ?>assets/img/icons/onlineaccount.png"></span>-->
                <!--        <span>Online Account Form </span>-->
                <!--    </a>-->
                <!--</div>-->
                
                <!-- where am i ? -->
                <div class="side-item">
                    <a href="https://www.ctznbank.com/OnlineCDS/">
                        <span>
                            <img src="{{url('/')}}/img/frontend/img/citizens icon _green-32.png"> 
                        </span>
                        <span>Online DMAT</span>
                    </a>
                </div>
                
                <div class="side-item">
                    <a href="https://services.ctznbank.com/ASBA/Login.aspx?ReturnUrl=%2fasba%2f">
                        <span>
                             <img src="{{url('/')}}/img/frontend/img/form1_green.png"> 
                        </span>
                        <span>Guarantee Verification </span>
                    </a>
                </div>

                <div class="side-item">
                    <a href="https://www.ctznbank.com/kycform/" >
                        <span>
                             <img src="{{url('/')}}/img/frontend/img/form_green.png"> 
                        </span>
                        <span>Online KYC</span>
                    </a>
                </div>
                
                <div class="side-item">
                    <a href="https://meroshare.cdsc.com.np/" >
                        <span>
                           <img src="{{url('/')}}/img/frontend/img/linegraph_green.png">   
                        </span>
                        <span>Mero Share</span>
                    </a>
                </div>
                
               <!--<?php //if($this->banking_hour_menu){ ?>-->
               <!-- <div class="side-item">-->
               <!--     <a href="<?php // echo site_url($this->banking_hour_menu['slug_name'])?>">-->
               <!--         <span><i class="fa fa-hourglass-half" aria-hidden="true"></i></span>-->
               <!--         <span><?php //echo $this->banking_hour_menu['page_title'] ?></span>-->
               <!--     </a>-->
               <!-- </div>-->
               <!-- <?php //}  ?>-->
               
            </div>
        </div>
    </div>
</section>

<section class="services-section">
    <div class="outer-custom-container">
        <div class="section-title home-title">
            <h1>Services</h1>
        </div>

         <div class="section-content">
         
            <div class="service-item">
                <div class="service-hover-container">
                    <div class="image-container">
                          <img src="{{url('/')}}/img/frontend/img/locker.png" alt="image">
                    </div>
                    <div class="text-container">
                        <h2>Lockers Facility</h2>
                    </div>
                    <div class="curtain-effect"><a href="{{ url("locker-facility") }}">View More</a></div>
                </div>
            </div>
     
<div class="service-item">
                <div class="service-hover-container">
                    <div class="image-container">
                         <img src="{{url('/')}}/img/frontend/img/utility.png" alt="image">
                    </div>
                    <div class="text-container">
                        <h2>Utility Payment</h2>
                    </div>
                    <div class="curtain-effect"><a href="{{ url("utility-payment") }}">View More</a></div>
                </div>
            </div>
            <div class="service-item">
                <div class="service-hover-container">
                    <div class="image-container">
                           <img src="{{url('/')}}/img/frontend/img/pigg.png" alt="image">
                    </div>
                    <div class="text-container">
                        <h2>Privilege Banking</h2>
                    </div>
                    <div class="curtain-effect"><a href="{{ url("privilege") }}">View More</a></div>
                </div>
            </div>

           

            

            <div class="service-item">
                <div class="service-hover-container">
                    <div class="image-container">
                        <img src="{{url('/')}}/img/frontend/img/citizen icon design_Branchless Banking-02.png" alt="image">
                    </div>
                    <div class="text-container">
                        <h2>Branchless Banking</h2>
                    </div>
                    <div class="curtain-effect"><a href="{{ url('branchless-banking') }}">View More</a></div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="offers-section">
    <div class="outer-custom-container">
        
        @foreach($products as $product)
        <a href="{{ $product['link'] }}">
            <div class="offers-item">
                <div class="offers-image" style="background-image: url('{{ url('uploads/product'.$product['background']) }}')">
                    <div class="text-container">
                        <div class="text-inner">
                            <div class="right-img">
                                <img src="{{ url('uploads/product'.$product['logo']) }}">
                            </div>
                            <div class="right-text">
                                <h1> {{ $product['name'] }} </h1>
                                <p> {{ $product['short_text'] }}</p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="image-container">
                        <img src="{{ url('uploads/product/'.$product['image']) }}" class="@if($product['type'] == 0) {{ 'image' }} @else {{ 'card' }} @endif ">
                    </div>
                </div>
            </div>
        </a>

      @endforeach
        
   <!--     @foreach($products as $product)
            <a href="{{ $product['link'] }}">
                <div class="offers-item">
                    <div class="offers-image" style="background-image: url('{{ url('uploads/product/'.$product['image']) }}')"></div>
                </div>
            </a>
        @endforeach
    </div>
</section>

<section class="home-footer">
    <div class="outer-custom-container">

        <div class="ft-container">
            @foreach($blog as $key => $value)
                <div class="ft-md-1">
                    <div class="ft-image" style="background-image: url('{{ url('uploads/blog/'.$value['image_name']) }}')"></div>
                </div>
                <div class="ft-md-2">
                    <h5>{{ $value['blog_title'] }}></h5>
                    @php $str=$value['contents']; @endphp
                    <p>{{ substr($str, 0, 350) }}..... </p>
                    <a class="ft-view" href="{{ url('blog/index') }}">VIEW ALL <i class="fa fa-arrow-right"></i></a>
                </div>
            @endforeach
        </div>
        <!--    <div class="ft-right-container">
            </div>-->
        <div class="sq-slash">
            <div class="un-sq-slash">
                <h5> {{ get_preferences('title') }} </h5>
                <div class="ft-contact">
                    <ul>
                      <li><i class="fa fa-home"></i>{{ get_preferences('address').','.get_preferences('city').'-'.get_preferences('postalcode') }} </li>
                        <li>
                            <a href="tel:+01-4427842">
                                <i class="fa fa-phone"></i>{{ get_preferences('phone') }}
                            </a>
                        </li>
                        <li>
                            <i class="fa fa-mobile"></i>
                            <a href="tel:+16600166667">
                                {{ get_preferences('mobile') }}
                            </a>
                        </li>
                        <li>
                            <i class="fa fa-envelope"></i>
                            <a href="mailto:info@ctznbank.com">
                                {{ get_preferences('email') }}
                            </a>
                        </li>
                        <li>
                            <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                            <a href="{{ get_preferences('location') }}">Google Map</a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</section>

<div class="clearfix"></div>
<script type="text/javascript" src="{{ url('img/frontend/js/loader.js') }}"></script>
<script type="text/javascript">
 google.charts.load('current', {'packages':['line']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Closing Price'],
          
          @foreach($stockvalue as $stock)
          [' {{ $stock->created_time }}',  {{ $stock['closing_price'] }}],
            @endforeach
        ]);

       var options = {
           title:'Stock',
                width: 400,
                 height: 300,
                 legend: 'none',
                hAxis: { textPosition: 'none' },
                 
            };

        var chart = new google.visualization.LineChart(document.getElementById('linechart'));

        chart.draw(data, options);
      }
   
</script>



<script type="text/javascript">
 
    
     google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
           ['Currency', 'Buying', 'Selling'],
            @foreach($forexdata as $forex)
            ['{{ $forex->currency }}',  {{ $forex->buying }},{{ $forex->selling }}],
            @endforeach

        ]);

        var options = {
          title: 'US Dollar Rates',
          curveType: 'function',
          legend: 'none',
            width: 400,
                 height: 300,
                hAxis: { textPosition: 'none' },
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
</script>
<script>
    
    $(document).ready(function(){
        var sideImageHeight =$('.stock-image-container').height();
        var stockHeight = $('#curve_chart').height();
        var stockHeader = $('.stock-exchange ul').height();
        $(".fortex-rate").height(sideImageHeight - (stockHeight + stockHeader));
        $(".fortex-rate .table-wrapper-fortex").css('display', 'block');
        
        
    })
    
     $(function() {
         
         
        {{-- @foreach(get_asba()  as $asba) --}}
         var notice = '{{-- $asba->title --}}';
        $.notify({  
                className:'success',
                message: 'Notice:'+notice,
                url: "http://pagodalabs.com.np/citizens/announcement",
                target: "_blank",
                clickToHide: false,
                
            },
            
            {
                placement: {
                    from: 'bottom',
                    align: 'right'
                }
                
            });
              
            {{-- @endforeach --}}
     
  });
  
  
   
</script>
<script>
    $(document).ready(function(){
       $('#home-logo').css("width","80%"); 
    });
</script>

@include('layout.footer')