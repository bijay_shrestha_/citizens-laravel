<div id="modal_vanancies" class="modal fade" role="dialog">
    <div class="modal-dialog">

         Modal content
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="section">
                    <div class="title">
                        <h1>Vacancy Reference</h1>
                    </div>
                    <div class="content">
                        @php $i=1; @endphp
                         @if(count(vacancies()) > 0 )
                        @foreach(vacancies() as $vacancies)
                            <div class="vacancy-item">
                                <h3> {{ $i.'. '.$vacancies->preferred_post }}</h3>
                                 {!! $vacancies->description !!}
                                <a href="{{url('home/online_vacancy/'.$vacancies->id.'/'.$vacancies->vacancy_code)}}" class="btn">Apply Now</a>
                            </div>
                                @php $i++; @endphp 
                            @endforeach 
                        @else
                            <div>Currently We don't have any vacancies</div>
                        @endif
                    </div>
                        
                    
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="search_modal">
    <div class="modal-dialog modal-sm citizens-search">
        <div class="modal-content">

            <div class="modal-body">
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade active in" id="signin">
                        @php  
                            $csrf = array('name' => '_token','hash' => csrf_token());
                        @endphp
                        <form class="form-horizontal" action="{{url('/')}}/search" method="get" >
                            <fieldset>
                                <!-- Sign In Form -->
                                <!-- Text input-->



                                <div class="control-group">
                                    <div class="controls">


                                        <input type="text" name="q" id="q" value="{{ ( Input::get('q') !='') ? Input::get('q'):'' }}" type="text" class="form-control" placeholder="Search for item" class="input-medium" required>
                                        <input type="hidden" name="{{$csrf['name']}}" value="{{$csrf['hash']}}" />
                                    </div>
                                </div>

                                <!-- Button -->
                                <div class="control-group">
                                    <label class="control-label" for="search"></label>
                                    <div class="index-search">

                                        <button  id="search" name="search" class="btn btn-success btn-lg btn-block" id="form submit">Search</button>
                                    </div>
                                </div>

                                <!-- Button -->
                                <div class="control-group">
                                    <label class="control-label" for="signin"></label>
                                    <center>
                                        <div class="controls">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </center>
                                </div>
                            </fieldset>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
    </div> <!--CONTENT WRAPPER ENDS-->

    <div class="footer-wrapper" id="footer-wrapper">
        <div class="ci-side-menu">
        <ul>
           <!-- <li>-->
           <!--       <a href="{{ url('bankings') }}">-->
           <!--        <div><i class="fa fa-bullhorn"></i></div>-->
           <!--        <div>Electronic Banking</div>-->
           <!--    </a>-->
           <!--</li>-->
         <li>
                   <a href="{{('announcements')}}">
                <div><i class="fa fa-bullhorn"></i></div>
                <div>Notice</div>
            </a>
        </li>
        <li>
            <a href="{{url('interest_rates')}}">
            <div>
                <img src="{{url('/')}}/assets/img/icons/citizens_rate_blue.png" class="tool-img">
            </div>
            <div>Rate</div>
            </a>
        </li>
        
        <li>
            <a href="{{url('home/calculator')}}">
            <div><i class="fa fa-calculator"></i></div>
            <div>Calculator</div>
            </a>
        </li>
        
        <li>
                   <a href="{{url('forexes')}}">
            <div><i class="fa fa-area-chart"></i></div>
            <div>Forex</div>
            </a>
        </li>
        
        <!--<li>-->
        <!--           <a href="{{url('branches/showLocation')}}">-->
        <!--    <div><i class="fa fa-map-marker"></i></div>-->
        <!--    <div>Location</div>-->
        <!--    </a>-->
        <!--</li>-->
        
       

            @if(getbanking_hour_menu())
            <li>
            <a href="{{url(getbanking_hour_menu()->slug_name) }}">{{ getbanking_hour_menu()->page_title }}</a>
           </li>
           @else
           {{ "hello world" }}
            @endif
           
          
                <!--  <li>-->
                <!--           <a href="{{ url('contacts') }}">-->
                <!--    <div><i class="fa fa-commenting"></i></div>-->
                <!--    <div>Grievance Handling </div>-->
                <!--    </a>-->
                <!--</li>-->
            </ul>
        </div>
            <div class="top-footer">
                <div class="footer-custom-container">
                    <div class="footer-nav-block">
                        <h1>Products</h1>
                            <ul>
                                <li><a href="{{ url('home/online') }} ">Online Services</a></li>
                                <li><a href="{{url('account') }}">Deposit</a></li>
                                <li><a href="{{ url('loan')}} ">Loan</a></li>
                                <li><a href="{{url('credit_card')}}">Cards</a></li>
                                <li><a href="{{ url('page/remit')}} ">Remitance</a></li>
                                <!--<li><a href="{{ url('banking') }}">Electronic Banking</a></li>-->
                                <li><a href="{{ url('home/others')}} ">Other Services</a></li>
                                 
                            </ul>
                        </div>
                        <div class="footer-nav-block">
                            <h1>Rates</h1>
                            <ul>
                                 <li><a href="{{ url('interest_rate/index/ir')}}">Interest Rates</a></li>
                                                <li><a href="{{ url('interest_rate/index/irs') }}">Interest Rate Spread</a></li>
                                                <li><a href="{{ url('interest_rate/index/br')}}">Base Rates</a></li>
                                                <li><a href="{{ url('interest_rate/index/ex')}}">Exchange Rates</a></li>
                                                <li><a href="{{ url('interest_rate/index/fc') }}">Fees and Commission</a></li>
                            </ul>
                        </div>
                        <div class="footer-nav-block">
                            <h1>Reports & Disclosures</h1>
                            <ul>
                                <!--<li><a href="{{url('capital-plan') }}">Capital Plan</a></li>-->
                                
                                @if(getminutes_menu())
                                    <li><a href="{{ url(''.getminutes_menu()->slug_name) }}">{{ getminutes_menu()->page_title }}</a></li>
                                @endif

                                @if(getannual_report_menu())
                                    <li><a href="{{ url(''.getannual_report_menu()->slug_name) }}">{{ getannual_report_menu()->page_title }} </a></li>
                               @endif
                                
                                @if(getunaudited_menu())
                                    <li><a href="{{ url(''.getunaudited_menu()->slug_name) }}">{{ getunaudited_menu()->page_title}} </a></li>
                                @endif
                                @if(getunaudited_menu())
                                <li><a href="{{ url(''.getunaudited_menu()->slug_name) }} ">{{ getunaudited_menu()->page_title }} </a></li>
                                @endif
                                <li><a href="{{ url('uploads/wysiwyg/AOA_and_MOA_After_10th_AGM.pdf') }}" download>MOA and AOA</a></li>
                            </ul>
                        </div>
                        <div class="footer-nav-block">
                            <h1>News & Events</h1>
                            <ul>
                                <li><a href="{{ url('blog') }}">News/Press Releases</a></li>
                                <li><a href="{{ url('csr') }}">CSR</a></li>
                                <li><a href="{{ url('announcement') }}"> Notices</a></li>
                            </ul>
                        </div>

                        <div class="footer-nav-block">
                            <h1>Account Opening</h1>
                            <ul>
                                <li><a href="{{ url('account/online_Account_opening') }}">Online Account Opening</a></li>
                                <!--<li><a href="{{ url('home/tracking_form') }}">Track your Account </a></li>-->
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="bottom-footer">
                    <div class="footer-custom-container">
                        <div class="text">
                            Copyright © 2017 <b>Citizens Bank International Limited</b> All Rights Reserved
                        </div>

{{-- yeha problem --}}
                        <div class="bf-social">
                            <ul>
                                @php
                                $social_media=getLink();
                                 @endphp 
                                @foreach($social_media as $link)
                              
                                @if($link->name == 'Facebook')
                                 <li class="fb"><a href="{{ $link->link }}" target="_blank"><i class="fa fa-facebook"></i></a> </li>
                                @endif
                                  @if($link->name == 'Linked In')
                                <li class="ln"><a href="{{ $link->link }}" target="_blank"><i class="fa fa-linkedin"></i></a> </li>
                                @endif
                                  @if($link->name == 'Twitter')
                                <li class="tw"><a href="{{$link->link }}" target="_blank"><i class="fa fa-twitter"></i></a> </li>
                               @endif
                                  @if($link->name == 'Instagram')
                                <li class="in"><a href="{{ $link->link }}" target="_blank"><i class="fa fa-instagram"></i></a> </li>
                                @endif
                               
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
              {{-- @if(getannual_report_menu())
                //     <li><a href="{{ url(getannual_report_menu()->slug_name) }}"> {{ getannual_report_menu()->page_title }}</a></li>
                // @endif --}}
        </div> <!--WRAPPER ENDS-->
    
        <script src="{{ url('img/frontend/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('img/frontend/js/bootstrap-scrollspy.js') }}"></script>
        <script src="{{ url('img/frontend/js/bootstrap-slider.js') }}"></script>
        <script src="{{ url('img/frontend/js/bootstrap-collapse.js')}} "></script>
        <script type="text/javascript" src="{{ url('img/frontend/js/slick.min.js') }}" ></script>
        <script type="text/javascript" src="{{ url('nepalidatepicker/js/jquery.nepaliDatePicker.js') }}"></script>
        
        
        <script type="text/javascript" src="{{ url('img/frontend/js/thescripts.js') }}"></script>
        <script src="{{ url('img/frontend/js/bootstrap-tooltip.js') }}"></script>
        
        <script>
             $(document).ready(function () {
    
    navInit();
    defaultPrevent();
})

var winWid;

$(window).resize(function () {
    winWid = $(window).width();
    
    $(window).scroll(function() {
        if ($(this).scrollTop() === 100) { // this refers to window
            $('.ci-side-menu').removeClass('dim');
            console.log('yo');
        }
    });
})



function navInit() {
    winWid = $(window).width();

    
    $('.nav-container > ul > li').hover(
        function(){ $('.nav-container > ul > li').addClass('remove-back') },
        function(){ $('.nav-container > ul > li').removeClass('remove-back') }
    )
    
    /* Top Menu Script Starts */
    if(winWid >=1199){
        $('.ci-top-nav > ul > li > a').on('click', function (e) {
            e.stopPropagation();
            if($(this).parents('li').hasClass('custom-drop')){
                $('#center-nav li').removeClass('hover');
                $(this).parents('li').removeClass('custom-drop');
                $('.ci-top-nav > ul > li > ul > li').removeClass('custom-drop-slevel');
            }else{
                $('#center-nav li').removeClass('hover');
                $('.ci-top-nav > ul > li > ul > li').removeClass('custom-drop-slevel');
                $('.ci-top-nav > ul > li').removeClass('custom-drop');
                $(this).parents('li').addClass('custom-drop');
            }
        });
        $(document).ready(function () {
    
    navInit();
    defaultPrevent();
})

var winWid;

$(window).resize(function () {
    winWid = $(window).width();
    
    $(window).scroll(function() {
        if ($(this).scrollTop() === 100) { // this refers to window
            $('.ci-side-menu').removeClass('dim');
            console.log('yo');
        }
    });
})



function navInit() {
    winWid = $(window).width();

    
    $('.nav-container > ul > li').hover(
        function(){ $('.nav-container > ul > li').addClass('remove-back') },
        function(){ $('.nav-container > ul > li').removeClass('remove-back') }
    )
    
    /* Top Menu Script Starts */
    if(winWid >=1199){
        $('.ci-top-nav > ul > li > a').on('click', function (e) {
            e.stopPropagation();
            if($(this).parents('li').hasClass('custom-drop')){
                $('#center-nav li').removeClass('hover');
                $(this).parents('li').removeClass('custom-drop');
                $('.ci-top-nav > ul > li > ul > li').removeClass('custom-drop-slevel');
            }else{
                $('#center-nav li').removeClass('hover');
                $('.ci-top-nav > ul > li > ul > li').removeClass('custom-drop-slevel');
                $('.ci-top-nav > ul > li').removeClass('custom-drop');
                $(this).parents('li').addClass('custom-drop');
            }
        });
        
        $('.ci-top-nav > ul > li > ul > li').on('click', function () {
            if($(this).hasClass('custom-drop-slevel')){
                $(this).removeClass('custom-drop-slevel');
            }else{
                $(this).parents('ul').parents('li').addClass('custom-drop');
                $('.ci-top-nav > ul > li > ul > li').removeClass('custom-drop-slevel');
                $(this).addClass('custom-drop-slevel');
            }
        });
    } 
    /* Top Menu Script Ends */
    
    
    /* Bottom Menu Script Starts */
    $('#center-nav li').on('click', function () {
        if($(this).hasClass('hover')){
            $('.ci-top-nav > ul > li').removeClass('custom-drop');
            $('.ci-top-nav > ul > li > ul > li').removeClass('custom-drop-slevel');
            $(this).removeClass('hover');
        }else{
            $('.ci-top-nav > ul > li').removeClass('custom-drop');
            $('.ci-top-nav > ul > li > ul > li').removeClass('custom-drop-slevel');
            $('#center-nav li').removeClass('hover');
            $(this).addClass('hover');
        }
    });
    /*Bottom Menu Script Ends */
        
    /* Main Class Romoval For botn Menus Starts */
    $('#content-wrapper').on('click', function () {
        $('#center-nav li').removeClass('hover');
        $('.ci-top-nav > ul > li').removeClass('custom-drop');
        $('.ci-top-nav > ul > li > ul > li').removeClass('custom-drop-slevel');
    });
    
    $('.bottom-navigation-container').on('click', function () {
        $('.ci-top-nav > ul > li').removeClass('custom-drop');
        $('.ci-top-nav > ul > li > ul > li').removeClass('custom-drop-slevel');
    });
    
    $('.top-navigation-container').on('click', function () {
        $('#center-nav li').removeClass('hover');
    });
    /* Main Class Romoval For botn Menus Ends */            
        
        
    $('.ci-burger').on('click', function () {
        $('.ci-top-nav').toggleClass('open');
    });

    $('.ci-bottom-burger').on('click', function () {
        $('.bottom-navigation-container .nav-container').toggleClass('open');
        $('.ci-side-menu').toggleClass('dim');
    });
    
    
    

    


    if(winWid <=1199){
        
         $('.ci-top-nav > ul > li').each(function(){
            $(this).appendTo('.bottom-navigation-container .center-nav');
        });
        
        $('.nav-container ul li').on('click', function () {
            if($(this).hasClass('main-menu-drop')){
                $(this).removeClass('main-menu-drop');
            }else{
                $('.nav-container ul li').removeClass('main-menu-drop');
                $(this).addClass('main-menu-drop');
            }
        });

        $('.nav-container ul li ul li').on('click', function () {
            if($(this).hasClass('lemain-menu-drop')){
                $(this).removeClass('lemain-menu-drop');
            }else{
                $('.nav-container ul li ul li').removeClass('lemain-menu-drop');
                $(this).addClass('lemain-menu-drop');
            }
        });
        
        $('.nav-container ul li ul li ul li').on('click', function () {
            if($(this).hasClass('memain-menu-drop')){
                $(this).removeClass('memain-menu-drop');
            }else{
                $('.nav-container ul li ul li ul li').removeClass('memain-menu-drop');
                $(this).addClass('memain-menu-drop');
            }
        });
        
      
        /* Appended */
  
    }
    



}

function defaultPrevent() {
    $('.has-sub-menu > a').click(function (e) {
        e.preventDefault();
    });



}

function showvacancy(){   
    $('#modal_vanancies').modal('show');
   
}

function popup_search(){
    $('#search_modal').modal('show');
}
</script>


<!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/58f7532530ab263079b6069f/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
    
    
    
    
                                

       