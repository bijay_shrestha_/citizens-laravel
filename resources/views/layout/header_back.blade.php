<header id="header-wrapper">
    <div class="custom-container remove-top-bottom">
        <div class="logo-nav-wrapper">
            <div class="logo-container">
                <a class="logo-img" href="{{ route('home') }}">
                    <img src="{{ asset('public/img/logo/logo.png') }}">
                </a>
                {{--                <div class="second-nav">
                                    <div class="nav-toggle hamburger">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                    <nav>
                                        <ul>
                                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/shop')?'active':'' }}"><a href="{{ route('shop') }}">Products</a></li>
                                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/where_to_buy')?'active':'' }}"><a href="{{ route('whereToBuy') }}">Where to Buy</a></li>
                                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/press')?'active':'' }}"><a href="{{ route('press') }}">Press</a></li>
                                            <!--<li class="{{ ($_SERVER['REQUEST_URI'] == '/about-us')?'active':'' }}"><a href="{{ route('aboutData') }}">About</a></li>-->
                                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/about_cashmere')?'active':'' }}"><a href="{{ route('aboutCashmere') }}">About</a></li>
                                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/getProcess')?'active':'' }}"><a href="{{ route('processData') }}">The Process</a></li>
                                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/contactus')?'active':'' }}"><a href="{{ route('contactus') }}">Contact</a></li>
                                        </ul>
                                    </nav>
                                </div>--}}
            </div>

            <div class="nav-container">
                <div class="inner">
                    <div class="navigation-item">
                        <ul>
                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/shop')?'active':'' }}"><a href="{{ route('shop') }}">Products</a></li>
                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/where_to_buy')?'active':'' }}"><a href="{{ route('whereToBuy') }}">Where to Buy</a></li>
                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/press')?'active':'' }}"><a href="{{ route('press') }}">Press</a></li>
                        <!--<li class="{{ ($_SERVER['REQUEST_URI'] == '/about-us')?'active':'' }}"><a href="{{ route('aboutData') }}">About</a></li>-->
                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/about_cashmere')?'active':'' }}"><a href="{{ route('aboutCashmere') }}">About</a></li>
                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/getProcess')?'active':'' }}"><a href="{{ route('processData') }}">The Process</a></li>
                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/contactus')?'active':'' }}"><a href="{{ route('contactus') }}">Contact</a></li>
                        <!--@if (Session::get('guest') == NULL)-->
                        <!--<li class=""><a href="{{ route('member.login') }}">Login</a></li>-->
                        <!--@else-->
                        <!--<li class=""><a href="{{ route('member.logout') }}">Logout</a></li>-->
                        <!--@endif-->
                        </ul>
                    </div>

                    <div class="search-form-item">
                        <div class="close-search">
                            <i class="fa fa-times"></i>
                        </div>
                        <form class="search-bar" action="{{route('searchProduct')}}" method="get" id="searchBar">
                            <input type="search" name="search"  id="search" placeholder="Search">
                        </form>
                    </div>
                </div>
            </div>
            <div class="button-container">
                <ul>
                    <li id="search-button"><img src="{{ asset('public/img/icon/search.png') }}"></li>
                    <li>
                        @if (Session::get('guest') == NULL)
                            <a href="{{ route('member.login') }}" class="navigation-item"><img src="{{ asset('public/img/icon/account.png') }}"></a>
                            {{--<a href="{{ route('user.memeber.register.form') }}" class="navigation-item"><i class="fa fa-user-plus"></i>&nbsp;Register</a>--}}
                        @else
                            <a href="{{ route('member.logout') }}" class="navigation-item"><i class="fa fa-sign-out" aria-hidden="true"></i> <span>Logout</span></a>
                        @endif
                    </li>
                    <li id="cart-bag">
                        <a href="#"><img src="{{ asset('public/img/icon/cart.png') }}"></a>
                        <span class="cart-num" id="cart-num">{{Cart::count()}}</span>
                    </li>
                    <li id="nav-toggle">
                        <span></span>
                        <span></span>
                        <span></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

</header>

<div id="side-cart">
    <div class="inner">
        <div class="title">
            <div class="title-inner">
                <h1>Shopping Cart</h1>
                <div class="side-cart-close">
                    <p>Close</p>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="content-item product-container" >
                @foreach(Cart::content() as $row)
                    <div class="product-item sidebarCart-{{$row->rowId}}">
                        <div class="image-container">
                            <img src="{{asset('public/image/product/FeatureImage/'.$row->options->image)}}">
                        </div>
                        <div class="text-container">
                            <p>{{$row->name}}</p>
                            <p>Quantity: {{$row->qty}}</p>
                        </div>
                        <div class="remove-cart-item">
                            <a onclick="deleteItem('{{$row->rowId}}')">×</a>
                        </div>
                    </div>
                @endforeach
                <div id="content-item-product-container" ></div>
            </div>

            <div class="content-item checkout-container">
                <a href="{{route('get.from.cart')}}">
                    View Cart
                </a>
            </div>
        </div>
    </div>
</div>

<div class="main-overlay"></div>

<div id="side-nav">
    <div class="side-menu-close">
        <i class="fa fa-times x-close"></i>
    </div>
    <div class="custom-container">
        <div class="side-menu">
            <div class="side-menu-title">
                <h2>SHOPPING CART</h2>
            </div>
            @foreach(Cart::content() as $row)
            <div class="side-menu-wrapper sidebarCart-{{$row->rowId}}">
                <div class="product-img common-side">
                    <img src="{{asset('public/image/product/FeatureImage/'.$row->options->image)}}">
                </div>
                <div class="side-product-title common-side">
                    <label>{{$row->name}}</label>
                </div>
                <div class="item-delete-btn common-side">
                    <a onclick="deleteItem('{{$row->rowId}}')"><i class="fa fa-times"></i></a>
                </div>
            </div>
            @endforeach
            
        </div>
        <div class="go-to-cart-btn">
            <a href="{{route('get.from.cart')}}"><button type="button" class="btn btn-default">Go To Cart</button></a>
        </div>
    </div>
</div>
<!---------------------------------- Header Wrapper Ends ----------------------------------->




