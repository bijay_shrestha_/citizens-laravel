
<section class="content">
<div class="box box-primary">

                 <!-- form start -->
                <form role="form" action="<?php  site_url('page/admin/content_type/add')?>" method="post">
                    <div class="box-body">
                        <div class="form-group">
		              <label><?=lang('first_name')?></label><input name="first_name" id="first_name" class="form-control" value="<?php echo set_value('first_name')?>"/></div><div class="form-group">
		              <label><?=lang('midle_name')?></label><input name="midle_name" id="midle_name" class="form-control" value="<?php echo set_value('midle_name')?>"/></div><div class="form-group">
		              <label><?=lang('last_name')?></label><input name="last_name" id="last_name" class="form-control" value="<?php echo set_value('last_name')?>"/></div><div class="form-group">
		              <label><?=lang('permanent_address')?></label><input name="permanent_address" id="permanent_address" class="form-control" value="<?php echo set_value('permanent_address')?>"/></div><div class="form-group">
		              <label><?=lang('temporaray_address')?></label><input name="temporaray_address" id="temporaray_address" class="form-control" value="<?php echo set_value('temporaray_address')?>"/></div><div class="form-group">
		              <label><?=lang('dob')?></label><input name="dob" id="dob" type="date" class="form-control" value="<?php echo set_value('dob')?>"/></div><div class="form-group">
		              <label><?=lang('p_zone')?></label><input name="p_zone" id="p_zone" class="form-control" value="<?php echo set_value('p_zone')?>"/></div><div class="form-group">
		              <label><?=lang('p_district')?></label><input name="p_district" id="p_district" class="form-control" value="<?php echo set_value('p_district')?>"/></div><div class="form-group">
		              <label><?=lang('t_zone')?></label><input name="t_zone" id="t_zone" class="form-control" value="<?php echo set_value('t_zone')?>"/></div><div class="form-group">
		              <label><?=lang('t_district')?></label><input name="t_district" id="t_district" class="form-control" value="<?php echo set_value('t_district')?>"/></div><div class="form-group">
		              <label><?=lang('nationality')?></label><input name="nationality" id="nationality" class="form-control" value="<?php echo set_value('nationality')?>"/></div><div class="form-group">
		              <label><?=lang('cv')?></label><input name="cv" id="cv" class="form-control" value="<?php echo set_value('cv')?>"/></div><div class="form-group">
		              <label><?=lang('citizenship_no')?></label><input name="citizenship_no" id="citizenship_no" class="form-control" value="<?php echo set_value('citizenship_no')?>"/></div><div class="form-group">
		              <label><?=lang('language')?></label><input name="language" id="language" class="form-control" value="<?php echo set_value('language')?>"/></div><div class="form-group">
		              <label><?=lang('phone_no')?></label><input name="phone_no" id="phone_no" class="form-control" value="<?php echo set_value('phone_no')?>"/></div><div class="form-group">
		              <label><?=lang('email')?></label><input name="email" id="email" class="form-control" value="<?php echo set_value('email')?>"/></div><div class="form-group">
		              <label><?=lang('gender')?></label><input name="gender" id="gender" class="form-control" value="<?php echo set_value('gender')?>"/></div><div class="form-group">
		              <label><?=lang('marital_status')?></label><input name="marital_status" id="marital_status" class="form-control" value="<?php echo set_value('marital_status')?>"/></div><div class="form-group">
		              <label><?=lang('immediate_contact')?></label><input name="immediate_contact" id="immediate_contact" class="form-control" value="<?php echo set_value('immediate_contact')?>"/></div><div class="form-group">
		              <label><?=lang('mobile_no')?></label><input name="mobile_no" id="mobile_no" class="form-control" value="<?php echo set_value('mobile_no')?>"/></div><div class="form-group">
		              <label><?=lang('contact_no')?></label><input name="contact_no" id="contact_no" class="form-control" value="<?php echo set_value('contact_no')?>"/></div><div class="form-group">
		              <label><?=lang('status')?></label><div class="radio"><label><input type="radio" value="1" name="status" id="status1" <?php echo (set_value('status')=='1')?'checked="checked"':''?>/><?=lang("general_yes")?></label> 
						<label><input type="radio" value="0" name="status" id="status0" <?php echo (set_value('status')=='0')?'checked="checked"':''?>/><?=lang("general_no")?></label></div></div>
<input type="hidden" name="p_id" id="p_id"/>
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a href="<?php  echo site_url('personal_detail/admin/personal_detail')?>" class="btn btn-danger">Cancel</a>
                    </div>
                </form>
                            </div>    


</section> 