<?php

class Personal_detail extends Admin_Controller
{
    
    public function __construct(){
        parent::__construct();
           $this->load->module_model('personal_detail','personal_detail_model');
        $this->lang->module_load('personal_detail','personal_detail');
        $this->load->module_model('education','education_model');
        $this->load->module_model('preference_available','preference_available_model');
        $this->load->module_model('vacancydatum','vacancydatum_model');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
    public function index()
    {
        $search_query = ($this->input->get('search'))?http_build_query($this->input->get()):null;
        // Display Page
        $data['header'] = 'personal_detail';
        $data['page'] = $this->config->item('template_admin') . "personal_detail/index";
        $data['module'] = 'personal_detail';
        $this->load->library('pagination');
        $limit=5;
        $offset=($this->input->get('per_page'))?$this->input->get('per_page'):0;
        
        $where=array('del_flag'=>0);
        $config=$this->paginationStylesheet();
        $config['base_url'] = site_url('personal_detail/admin/personal_detail?'.$search_query);
        $config['total_rows'] = $this->personal_detail_model->count($where);
        $config['per_page'] = $limit; 
        $config['page_query_string']=TRUE;
        $this->pagination->initialize($config);    
        $this->_get_search_param();
        $this->personal_detail_model->joins= array('PREFERENCE_AVAILABLE');

        $data['personaldetails']=$this->personal_detail_model->getPersonalDetails($where,'personal_details.p_id asc',array('limit'=>$limit,'offset'=>$offset))->result_array();
        $data['vacancydatas']=$this->vacancydatum_model->getVacancydatas($where,'id desc')->result_array();
        $this->load->view($this->_container,$data);     
    }
    
    public function _get_search_param()
    {   
        $search = $this->input->get('search');
        
            if(!empty($search))
            {
                if($this->input->get('reset') == 'Clear')
                {
                    unset($search);
                }
                else{
                    ($search['specialized_area']!='')?$this->db->like('preference_available.specialized_area',$search['specialized_area']):'';
                    ($search['preferred_post']!='')?$this->db->where('preference_available.preferred_post',$search['preferred_post']):'';
                    if($search['date']!=''){
                        $date = explode(' - ',$search['date']);
                        $posed['from'] = date('Y-m-d', strtotime($date[0]));
                        $posed['to'] = date('Y-m-d', strtotime($date[1]));
                        $this->db->where('personal_details.posted_date >=', $posed['from']);
                        $this->db->where('personal_details.posted_date <=', $posed['to']);
                    }
                }
                // ($search['post_date']!=0)?$this->db->where('personal_details.post_date',$search['post_date']):'';
                // ($search['post_data']!='')?$this->db->where('personal_details.post_date',$search['post_data']):'';
            //   
                
            }     
    }
    public function detail($id){
        $data['header'] = 'personal_detail';
        $data['detail_id'] = $id;
        $data['page'] = $this->config->item('template_admin') . "personal_detail/detail";
        $data['module'] = 'personal_detail';
        $where=array('p_id'=>$id);
        $data['personal_details'] = $this->personal_detail_model->getPersonalDetails($where)->result_array();
        $data['educations']=$this->education_model->getEducations($where)->result_array();  
        $data['preferenceavailables']=$this->preference_available_model->getPreferenceAvailables($where)->result_array();
        $this->db->where('p_id',$id);
        $data['organizationreferences'] = $this->db->get('tbl_orgreference')->result_array();
        $this->db->where('p_id',$id);
        $data['workexperieces'] = $this->db->get('tbl_work_experience')->result_array();
        $this->db->where('p_id',$id);
        $data['extra_courses'] = $this->db->get('tbl_extra_course')->result_array();
        $this->load->view($this->_container,$data); 
    }
    
    public function form_view($id)
    {
        $data['header'] = 'personal_detail';
        $data['detail_id'] = $id;
        $data['page'] = $this->config->item('template_admin') . "personal_detail/form_view";
        $data['module'] = 'personal_detail';
        $where=array('p_id'=>$id);
        $data['personal_details'] = $this->personal_detail_model->getPersonalDetails($where)->result_array();
        $data['educations']=$this->education_model->getEducations($where)->result_array();  
        $data['preferenceavailables']=$this->preference_available_model->getPreferenceAvailables($where)->result_array();
        $this->db->where('p_id',$id);
        $data['organizationreferences'] = $this->db->get('tbl_orgreference')->result_array();
        $this->db->where('p_id',$id);
        $data['workexperieces'] = $this->db->get('tbl_work_experience')->result_array();
        $this->db->where('p_id',$id);
        $data['extra_courses'] = $this->db->get('tbl_extra_course')->result_array();
        $this->load->view($this->_container,$data);
    }
    public function add()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');    
        $data['header'] = 'Add PersonalDetails';
        $data['page'] = $this->config->item('template_admin') . "personal_detail/add";
        $data['module'] = 'personal_detail';
        //$this->form_validation->set_rules('name','Name','required|trim');
        //$this->form_validation->set_rules('sequence','Order','required|numeric');
        //$this->form_validation->set_rules('status','Status','required');
        
        if($this->form_validation->run() == false){
            $this->load->view($this->_container,$data);                 
        }
        else
        {
            $posted_data=$this->_get_posted_data();
            //$posted_data['added_date']=date('Y-m-d H:i:s');
            //$posted_data['added_by']=$this->session->userdata('id');
            $this->personal_detail_model->insert('PERSONAL_DETAILS',$posted_data);
            flashMsg('success','PersonalDetails Added Successfully');
            redirect(site_url('personal_detail/admin/personal_detail'));
        }
    } 
    
    public function edit($id=NULL)
    {
        if(is_null($id)){
            flashMsg('error','No Id matches to edit');
            redirect(site_url('personal_detail/admin/personal_detail'));            
        }    
        $this->load->helper('form');
        $this->load->library('form_validation');    
        $data['header'] = 'Edit PersonalDetails';
        $data['page'] = $this->config->item('template_admin') . "personal_detail/edit";
        $data['module'] = 'personal_detail';
        //$this->form_validation->set_rules('name','Name','required|trim');
        //$this->form_validation->set_rules('sequence','Order','required|numeric');
        //$this->form_validation->set_rules('status','Status','required');
        
        $data['personal_detail']=NULL;
        if($id){
            $data['personal_detail']=$this->personal_detail_model->getPersonalDetails(array('p_id'=>$id))->row_array();
        }        
        
        if($this->form_validation->run() == false){
            $this->load->view($this->_container,$data);                 
        }
        else
        {
            $posted_data=$this->_get_posted_data();
            //$posted_data['added_date']=date('Y-m-d H:i:s');
            //$posted_data['added_by']=$this->session->userdata('id');
            $this->personal_detail_model->update('PERSONAL_DETAILS',$posted_data,array('p_id'=>$posted_data['p_id']));
            flashMsg('success','Personal_detail Updated Successfully');
            redirect(site_url('personal_detail/admin/personal_detail'));
        }
    }        

    public function delete($id=NULL)
    {
        if(is_null($id)){
            flashMsg('error','No Id matches to Delete');
            redirect(site_url('personal_detail/admin/personal_detail'));            
        }
        $this->personal_detail_model->update('PERSONAL_DETAILS',array('del_flag'=>1),array('p_id'=>$id));
        flashMsg('success','Personal_detail Deleted Successfully');         
        redirect(site_url('personal_detail/admin/personal_detail'));            
    }   

    public function save()
    {
        
        $data=$this->_get_posted_data(); //Retrive Posted Data      

        if(!$this->input->post('p_id'))
        {
            $success=$this->personal_detail_model->insert('PERSONAL_DETAILS',$data);
        }
        else
        {
            $success=$this->personal_detail_model->update('PERSONAL_DETAILS',$data,array('p_id'=>$data['p_id']));
        }
        
        if($success)
        {
            $success = TRUE;
            $msg=lang('success_message'); 
        } 
        else
        {
            $success = FALSE;
            $msg=lang('failure_message');
        }
         
         echo json_encode(array('msg'=>$msg,'success'=>$success));      
        
    }
   
   private function _get_posted_data()
   {
        $data=array();
        $data['p_id'] = $this->input->post('p_id');
        $data['first_name'] = $this->input->post('first_name');
        $data['midle_name'] = $this->input->post('midle_name');
        $data['last_name'] = $this->input->post('last_name');
        $data['permanent_address'] = $this->input->post('permanent_address');
        $data['temporaray_address'] = $this->input->post('temporaray_address');
        $data['dob'] = $this->input->post('dob');
        $data['p_zone'] = $this->input->post('p_zone');
        $data['p_district'] = $this->input->post('p_district');
        $data['t_zone'] = $this->input->post('t_zone');
        $data['t_district'] = $this->input->post('t_district');
        $data['nationality'] = $this->input->post('nationality');
        $data['cv'] = $this->input->post('cv');
        $data['citizenship_no'] = $this->input->post('citizenship_no');
        $data['language'] = $this->input->post('language');
        $data['phone_no'] = $this->input->post('phone_no');
        $data['email'] = $this->input->post('email');
        $data['gender'] = $this->input->post('gender');
        $data['marital_status'] = $this->input->post('marital_status');
        $data['immediate_contact'] = $this->input->post('immediate_contact');
        $data['mobile_no'] = $this->input->post('mobile_no');
        $data['contact_no'] = $this->input->post('contact_no');
        $data['status'] = $this->input->post('status');

        return $data;
   }
   
    function export($detail_id){

   $query=$this->db->query('SELECT * FROM view_applicant_all WHERE p_id=?  ',array($detail_id));
       
        if(!$query)
            return false;

        // Starting the PHPExcel library
        $this->load->library('excel');
       // $this->load->library('IOFactory');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);
        // Field names in the first row

        $fields = $query->list_fields();
        
        $col = 0;
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $col++;
        }
        // Fetching the table data
        $row = 2;
        foreach($query->result() as $data)
        {
            $col = 0;
            foreach ($fields as $field)
            {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }
            $row++;
        }
        $objPHPExcel->setActiveSheetIndex(0);
         $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="applicants.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }
    
    public function get_highest($details)
    {
       foreach($details as $key => $detail){
            $this->db->where('p_id',$detail['p_id']);
            $experience[] = $this->db->get('tbl_work_experience')->result_array();
            
        }
       
        $max = 1;
        foreach($experience as $exp)
        {
            $size = count($exp);
            if($max < $size)
            {
                $max = $size;
            }
        }
        //  echo '<pre>'; print_r($max); exit;
        return $max;
    }

    public function get_highest_additional_course($details)
    {
        foreach($details as $key => $detail){
            $this->db->where('p_id',$detail['p_id']);
            $courses[] = $this->db->get('tbl_extra_course')->result_array();            
        }
        $max = 1;
        foreach($courses as $cor)
        {
            $size = count($cor);
            if($max < $size)
            {
                $max = $size;
            }
        }
        //  echo '<pre>'; print_r($max); exit;
        return $max;
    }

    public function get_highest_reference($details)
    {
        foreach($details as $key => $detail){
            $this->db->where('p_id',$detail['p_id']);
            $reference[] = $this->db->get('tbl_orgreference')->result_array();            
        }
        $max = 1;
        foreach($reference as $ref)
        {
            $size = count($ref);
            if($max < $size)
            {
                $max = $size;
            }
        }
        //  echo '<pre>'; print_r($max); exit;
        return $max;
    }
    
    public function export_all()
    {

        $this->load->library('Excel');
        ini_set('memory_limit','8000000M');
        
        $where = array('personal_details.del_flag'=>0);
        $this->_get_search_param();
        $this->personal_detail_model->joins= array('PREFERENCE_AVAILABLE','EDUCATION');
        $details=$this->personal_detail_model->getPersonalDetails($where)->result_array();
        
        
        // echo '<pre>'; print_r($details); exit;
        $highest = $this->get_highest($details);
        $highest_additional = $this->get_highest_additional_course($details);
        $highest_reference = $this->get_highest_reference($details);
        // $limit = 2000;
        // $offset = 0;

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->setTitle('All Persional Details');

        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Name')
        ->setCellValue('B1', 'Father Name')
        ->setCellValue('C1', 'Grand Father Name')
        ->setCellValue('D1', 'citizenship No.')
        ->setCellValue('E1', 'Permanent Address')
        ->setCellValue('F1', 'Permanent Zone')
        ->setCellValue('G1', 'Permanent District')
        ->setCellValue('H1', 'Temporary Address')
        ->setCellValue('I1', 'Temporary Zone')
        ->setCellValue('J1', 'Temporary District')
        ->setCellValue('K1', 'Date of Birth')
        ->setCellValue('L1', 'Gender')
        ->setCellValue('M1', 'Status')
        ->setCellValue('N1', 'Nationality')
        ->setCellValue('O1', 'Language')
        ->setCellValue('P1', 'Email')
        ->setCellValue('Q1', 'Landline')
        ->setCellValue('R1', 'Mobile')
        ->setCellValue('S1', 'Imidiate Contact Person')
        ->setCellValue('T1', 'Imidiate Contact Person No.')
        ->setCellValue('U1', 'School Board')
        ->setCellValue('V1', 'School Name')
        ->setCellValue('W1', 'School Obtained Percentage')
        ->setCellValue('X1', 'School Passed Year')
        ->setCellValue('Y1', '10+2 Board')
        ->setCellValue('Z1', '10+2 Name')
        ->setCellValue('AA1', '10+2 Obtained Percentage')
        ->setCellValue('AB1', '10+2 Passed Year')
        ->setCellValue('AC1', 'Bachelor Board')
        ->setCellValue('AD1', 'Bachelor Name')
        ->setCellValue('AE1', 'Bachelor College Degree')
        ->setCellValue('AF1', 'Bachelor Obtained Percentage/GPA')
        ->setCellValue('AG1', 'Bachelor Passed Year')
        ->setCellValue('AH1', 'Master Board')
        ->setCellValue('AI1', 'Master College Name')
        ->setCellValue('AJ1', 'Master College Degree')
        ->setCellValue('AK1', 'Master Obtained Percentage/GPA')
        ->setCellValue('AL1', 'Master Passed Year')
        ->setCellValue('AM1', 'Prefered Post')
        ->setCellValue('AN1', 'Specialized Area')
        ->setCellValue('AO1', 'Travel Option')
        ->setCellValue('AP1', 'Travel License')
        ->setCellValue('AQ1', 'Prefered Location Inside')
        ->setCellValue('AR1', 'Prefered Location Outside');
        
        $char = 'AS';    
        for($i = 1; $i <= $highest_additional; $i++){
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($char.'1','Course '.$i);
            $char++;
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($char.'1', 'Institute '.$i);
            $char++;          
        }

        for($i = 1; $i <= $highest; $i++){
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($char.'1','Organization '.$i);
            $char++;
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($char.'1', 'Position '.$i);
            $char++;
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($char.'1','Department '.$i);
            $char++;
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($char.'1', 'Experience From '.$i);
            $char++;
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($char.'1', 'Experience To '.$i);
            $char++;
        }

        for($i = 1; $i <= $highest_reference; $i++){
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($char.'1','Reference '.$i);
            $char++;
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($char.'1', 'Reference Post '.$i);
            $char++;
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($char.'1','Reference Organization '.$i);
            $char++;
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($char.'1', 'Reference Organization Address '.$i);
            $char++;
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($char.'1', 'Reference Organization Email'.$i);
            $char++;
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($char.'1', 'Reference Organization Telephone'.$i);
            $char++;
        }
        
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue($char++.'1', 'Submited Date and Time')
        ->setCellValue($char.'1', 'CV');
        
        
        $row = 2;
        $col = 0;  
        // echo '<pre>'; print_r($objPHPExcel); exit;

        foreach($details as $key => $detail){
            $this->db->where('p_id',$detail['p_id']);
            $details[$key]['work_experience'] = $this->db->get('tbl_work_experience')->result_array();
            $this->db->where('p_id',$detail['p_id']);
            $details[$key]['courses'] = $this->db->get('tbl_extra_course')->result_array();
            $this->db->where('p_id',$detail['p_id']);
            $details[$key]['references'] = $this->db->get('tbl_orgreference')->result_array();
        }

        foreach ($details as $k => $values) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['first_name'].' '.@$values['middle_name'].' '.$values['last_name']);
                $col++; 
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['father_name']);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['grand_father_name']);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['citizenship_no']);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['permanent_address']);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['p_zone']);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['p_district']);
                $col++; 
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['temporaray_address']);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['t_zone']);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['t_district']);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['dob']);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['gender']);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['marital_status']);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['nationality']);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['language']);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['email']);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['phone_no']);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['mobile_no']);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['immediate_contact']);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values['contact_no']);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['s_board']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['s_school']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['s_marks']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['s_passed_year']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['c_university']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['c_college']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['c_marks']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['c_passed_year']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['b_university']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['b_college']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['b_faculty']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['b_marks']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['b_passed_year']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['m_university']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['m_college']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['m_faculty']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['m_marks']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['m_passed_year']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['preferred_post']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['specialised_area']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['travelling_option']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['driving_license']);
                $locations = array();
                $locations = json_decode($values['preferred_location'],true);
                // print_r($locations); exit;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, @$locations['Inside']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, @$locations['Outside']);
                for($i=0; $i<$highest_additional; $i++ ){
                    if(array_key_exists($i,$values['courses'])){
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['courses'][$i]['course']);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['courses'][$i]['institude']);
                    }else{
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '');
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '');
                    }
                }
                // foreach($values['courses'] as $course){
                //     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $course['course']);
                //     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $course['institude']);                   
                // }
                for($i = 0; $i <$highest; $i++){
                    if(array_key_exists($i,$values['work_experience'])){
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['work_experience'][$i]['organization']);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['work_experience'][$i]['position']);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['work_experience'][$i]['department']);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['work_experience'][$i]['experience_from']);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['work_experience'][$i]['experience_to']);
                    }else{
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '');
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '');
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '');
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '');
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '');
                    }
                }

                // foreach($values['work_experience'] as $experience){
                //     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $experience['organization']);
                //     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $experience['position']);
                //     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $experience['department']);
                //     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $experience['experience_from']);
                //     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $experience['experience_to']);
                // }
                 
                for($i = 0; $i < $highest_reference; $i++){
                    if(array_key_exists($i,$values['references'])){
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['references'][$i]['reference']);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['references'][$i]['post']);                    
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['references'][$i]['organization']);                    
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['references'][$i]['address']);                    
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['references'][$i]['email']);                    
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['references'][$i]['telephone']); 
                    }else{
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '');
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '');                    
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '');                    
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '');                    
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '');                    
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '');  
                    }
                }    
                
                // foreach($values['references'] as $reference){
                //     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $reference['reference']);
                //     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $reference['post']);                    
                //     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $reference['organization']);                    
                //     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $reference['address']);                    
                //     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $reference['email']);                    
                //     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $reference['telephone']);                    
                // }

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['posted_date']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['cv']);
                $objPHPExcel->getActiveSheet()->getCell($char.$row)->getHyperlink()->setUrl('http://pagodalabs.com.np/citizens/uploads/cv/'.$values['cv']);
                $row++;
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $values['cv']);
                $col = 0;

            }

            header('Content-Type: application/vnd.ms-excel'); 
            header('Content-Disposition: attachment;filename="Applicants.xls"'); 
            header('Cache-Control: max-age=0'); 
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            // if (ob_get_length()) ob_end_clean();
            $objWriter->save('php://output');   

    }        

        
}