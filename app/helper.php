<?php

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\modules\page\Model\Page;
use App\modules\social_media_link\Model\Social_media_link;


function control($permission){
    $permissons_exist = 0;
    $user_id = Auth::user()->id;
    $permission_id      = DB::table('permissions')->where('name',$permission)->first();
    $role_id            = DB::table('role_user')->where('user_id',$user_id)->first();
    if(($permission_id != null || $permission_id != '') && ($role_id != '' || $role_id != null))
        $permissons_exist   = DB::table('permission_role')->where([
            ['permission_id','=',$permission_id->id],['role_id','=',$role_id->role_id]
        ])->count();
    if($user_id == 1){
        return true;
    }
    else if(isset($permissons_exist) && $permissons_exist > 0){
        return true;
    }
    else{
        return false;
    }
}

function just($ok) {
    return $ok;

}

function slugify($text) {
    // replace non letter or digits by -
    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
    // trim
    $text = trim($text, '-');
    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    // lowercase
    $text = strtolower($text);
    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);
    if (empty($text)) {
        return 'n-a';
    }
    return $text;
}

function validate_slug($slug, $id=false, $count=false)
{
    if($this->check_slug($slug.$count, $id))
    {
        if(!$count)
        {
            $count  = 1;
        }
        else
        {
            $count++;
        }
        return $this->validate_slug($slug, $id, $count);
    }
    else
    {
        return $slug.$count;
    }
}

function check_slug($slug, $id=FALSE)
{
    $slugdata = DB::table('slugs');
    if($id)
    {
        $slugdata = $slugdata->where('slug_id', '!=', $id);
    }
    $slugdata = $slugdata->where('slug_name', $slug)->get();
    
    return (bool) (count($slugdata));
}



function change_status($text, $status, $id, $tablename, $id_field, $deleted_by = null, $deleted_date = null) {
    $data = array();
    if ($text == "approved" || $text == "pending" || $text == "denied") {
            $data['approved'] = $text;
            /*if ($status == "delete") {
                $data['del_flag'] = 1;
            }*/            
            if ($text == "approved") {
                if ($status == "delete") {                    
                    $data['del_flag'] = 1;
                    if ($deleted_by) {
                        $data[$deleted_by] = Auth::user()->id;                        
                    }
                    if ($deleted_date) {
                        $data[$deleted_date] = new Carbon();
                    }                    
                } else {
                    if ($status == "edit") {
                        $data['approved'] = $text;
                    } else {
                        $data['approvel_status'] = $status;
                    }
                }
            } elseif ($text == "denied") {                
                if ($status == "delete") {
                    $data['del_flag'] = 0;
                } else {
                    if ($status == "edit") {
                        $data['approved'] = $text;
                    } else {
                        $data['approvel_status'] = $status;
                    }
                    //$data['del_flag'] = 0;
                }
            }
            /*if ($text == "approved") {
                if ($status == "delete") {
                    $data['del_flag'] = 0;
                } else {
                    $data['approvel_status'] = "ok";
                }
            } elseif ($text != "approved")  {
                if ($status == "delete") {
                    $data['del_flag'] = 0;
                }
            }
            */
        
        DB::table($tablename)->where($id_field, $id)->update($data);
        }
return $data;
}

function soft_delete($tablename, $id_field, $id, $deleted_by = null,  $deleted_date = null) {

    //$group_id = Check_user_id_if_admin($id);
    $group_id = true;
    if ($group_id) {
        //as of now , making group_id true for admininstrator and false for other type
        $data['del_flag'] = 1;
        if ($deleted_by) {
            $data[$deleted_by] = Auth::user()->id;                        
        }
        if ($deleted_date) {
            $data[$deleted_date] = new Carbon();
        }                    
    } else {
        //these are other than main administrator
        $data['del_flag'] = 0;
        $data['approvel_status'] = "delete";
        $data['approved'] = "pending";
    }
 

    DB::table($tablename)->where($id_field, $id)->update($data);
    return $data;
}

function form_dropdown_($fieldname, $options, $oldvalue)
{
    $data_to_print = "";
    $data_to_print .= "<select name='" . $fieldname . "' class='form-control'>"; 
/*    foreach ($options as $option) {
        $data_to_print . = "<option value='" . $option['page_id'] . "'></option";
    }*/
    return "to_be_remain";
}

//function check_group_or_maker_checker($data, $create_or_edit) 
function check_case($data, $create_or_edit) 
{
    if (Auth::user()->group == 2 || Auth::user()->control == 1) {
        $data['approved'] = 'approved';
        $data['approvel_status'] = 'ok';
    } else {
        $data['approved'] = 'pending';
        if ($create_or_edit) {
            if ($create_or_edit == "edit") {
               $data['approvel_status'] = 'edit';
            } else {
                $data['approvel_status'] = 'insert';
            }
        }
    }
    return $data;
}
function getabout_us_menu()
{
    $about_us = Page::where('page_id',35)->where('del_flag',0)->where('status',1)->first();
    return $about_us;
}

function getoverview_menu(){

    $about_us = DB::table('pages')->where('page_id',1)->where('del_flag',0)->where('status',1)->get();
    // $this->load->module_model('page','page_model');
    // $this->db->select('page_title,slug_name');
    // $about_us = $this->page_model->getPages(array('page_id'=>1,'pages.del_flag'=>0,'pages.status'=>1))->row_array();
    return $about_us;
}

 function getspokeperson_menu(){
    $about_us = DB::table('pages')->where('page_id',31)->where('del_flag',0)->where('status',1)->first();
    // $this->load->module_model('page','page_model');
    // $this->db->select('page_title,slug_name');
    // $about_us = $this->page_model->getPages(array('page_id'=>31,'pages.del_flag'=>0,'pages.status'=>1))->row_array();
    return $about_us;
}


function getBlogs(){
    $blogs = DB::table('tbl_blogs')->where('del_flag',0)->where('status',1)->first();
    // dd($blogs);
    return $blogs;
}


function getinformation_office_menu(){
    //$about_us = $this->page_model->getPages(array('page_id'=>29,'pages.del_flag'=>0,'pages.status'=>1))->row_array();
    $about_us = DB::table('pages')->where('page_id',29)->where('del_flag',0)->where('status',1)->first();
    return $about_us;
}

function getform_menu(){
    //$this->load->module_model('page','page_model');
    //$this->db->select('page_title,slug_name');
    //$about_us = $this->page_model->getPages(array('page_id'=>20,'pages.del_flag'=>0,'pages.status'=>1))->row_array();
    $about_us = DB::table('pages')->where('page_id',20)->where('del_flag',0)->where('status',1)->first();
    return $about_us; 
}

function other_menu()
{
   // $this->load->module_model('other_menu','other_menu_model');

    //$this->db->where("((approvel_status = 'ok' AND approved = 'approved') OR (approvel_status ='delete' And approved='denied')) AND del_flag = 0 AND status = 1");
    //$menu = $this->other_menu_model->getOtherMenus()->result_array();
    $menu = DB::table('tbl_other_menu')->where([['approvel_status', "ok"],['approved', "approved"]])->orWhere([['approvel_status', "delete"], ['approved', "denied"], ['del_flag', 0]])->get();
    return $menu;
}

 function getAccounts(){
    // $this->db->where("((approvel_status = 'ok' AND approved = 'approved') OR (approvel_status ='delete' And approved='denied')) AND del_flag = 0");
    // $accounts=$this->account_model->getAccounts()->result_array();
    $accounts = DB::table('tbl_accounts')->where([['approvel_status', "ok"],['approved', "approved"]])->orWhere([['approvel_status', "delete"], ['approved', "denied"], ['del_flag', 0]])->get();
    // return $accounts;
    return $accounts;
}

 function getLoans(){
    // $this->db->order_by("loan_name","asc");
    // $this->db->where("((approvel_status = 'ok' AND approved = 'approved') OR (approvel_status ='delete' And approved='denied')) AND del_flag = 0");
    // $loans=$this->loan_model->getLoans()->result_array();
    $loans = DB::table('tbl_loans')->where([['approvel_status', "ok"],['approved', "approved"]])->orWhere([['approvel_status', "delete"], ['approved', "denied"], ['del_flag', 0]])->get();
    //return "not done";
    return $loans;
}

function getCredit(){
    // $this->db->where("((approvel_status = 'ok' AND approved = 'approved') OR (approvel_status ='delete' And approved='denied')) AND del_flag = 0");
    // $credits=$this->credit_card_model->getCreditCards(null,'sequence asc')->result_array();
    $credits= DB::table('tbl_credit_cards')->where([['approvel_status', "ok"],['approved', "approved"]])->orWhere([['approvel_status', "delete"], ['approved', "denied"], ['del_flag', 0]])->get();
    return $credits;
}

 function getBanking(){
   // $this->db->where("((approvel_status = 'ok' AND approved = 'approved') OR (approvel_status ='delete' And approved='denied')) AND del_flag = 0");
    //$services=$this->banking_model->getBankings()->result_array();
    $services= DB::table('tbl_banking')->where([['approvel_status', "ok"],['approved', "approved"]])->orWhere([['approvel_status', "delete"], ['approved', "denied"], ['del_flag', 0]])->get();
    return $services;
}

 function getbranchless_menu(){
    //$this->load->module_model('page','page_model');
    //$this->db->select('page_title,slug_name');
    //$about_us = $this->page_model->getPages(array('page_id'=>34,'pages.del_flag'=>0,'pages.status'=>1))->row_array();
    $about_us = Page::where('page_id',34)->where('del_flag',0)->with('status',1)->first();

    return $about_us; 
}

 function getdemat_menu(){
    //$this->load->module_model('page','page_model');
    //$this->db->select('page_title,slug_name');
    //$about_us = $this->page_model->getPages(array('page_id'=>32,'pages.del_flag'=>0,'pages.status'=>1))->row_array();
    $about_us = Page::where('page_id',32)->where('del_flag',0)->with('status',1)->first();
    return $about_us;
}

 function getonlinefee_menu(){
    //$this->load->module_model('page','page_model');
    //$this->db->select('page_title,slug_name');
    //$about_us = $this->page_model->getPages(array('page_id'=>28,'pages.del_flag'=>0,'pages.status'=>1))->row_array();
    $about_us = Page::where('page_id',28)->where('del_flag',0)->with('status',1)->first();
    return $about_us; 
}

 function getasba(){
    //$this->load->module_model('page','page_model');
    //$this->db->select('page_title,slug_name');
    //$about_us = $this->page_model->getPages(array('page_id'=>36,'pages.del_flag'=>0,'pages.status'=>1))->row_array();
    $about_us = Page::where('page_id',36)->where('del_flag',0)->with('status',1)->first();
    return $about_us; 
}

 function getremit_menu()
{
    //$this->load->module_model('remit_catagory','remit_catagory_model');
    //$remit_menu =$this->remit_catagory_model->getRemitCatagories(array('del_flag'=>0))->result_array();
    //tbl_remit_catagories
    $services= DB::table('tbl_remit_catagories')->where([['del_flag', "0"]])->get();
    return $services;
  
}

 function getminutes_menu(){
    // $this->load->module_model('page','page_model');
    // $this->db->select('page_title,slug_name');
    // $about_us = $this->page_model->getPages(array('page_id'=>17,'pages.del_flag'=>0,'pages.status'=>1))->row_array();
    $about_us = Page::where('page_id',17)->where('del_flag',0)->with('status',1)->first();
    return $about_us;
}

 function getannual_report_menu(){
    // $this->load->module_model('page','page_model');
    // $this->db->select('page_title,slug_name');
    // $about_us = $this->page_model->getPages(array('page_id'=>12,'pages.del_flag'=>0,'pages.status'=>1))->row_array();
    $about_us = Page::where('page_id',12)->where('del_flag',0)->with('status',1)->first();
    return $about_us;
    //return "not done";
}
 function getunaudited_menu(){
    // $this->load->module_model('page','page_model');
    // $this->db->select('page_title,slug_name');
    // $about_us = $this->page_model->getPages(array('page_id'=>18,'pages.del_flag'=>0,'pages.status'=>1))->row_array();
    $about_us = Page::where('page_id',18)->where('del_flag',0)->with('status',1)->first();
    return $about_us; 
}
 function getdisclosure_menu(){
    // $this->load->module_model('page','page_model');
    // $this->db->select('page_title,slug_name');
    // $about_us = $this->page_model->getPages(array('page_id'=>19,'pages.del_flag'=>0,'pages.status'=>1))->row_array();
    $about_us = Page::where('page_id',19)->where('del_flag',0)->with('status',1)->first();
    return $about_us; 
    //return "not done";
}

 function getaml_compliance_menu(){
    // $this->load->module_model('page','page_model');
    // $this->db->select('page_title,slug_name');
    $about_us = Page::where('page_id',7)->where('del_flag',0)->with('status',1)->first();
    // $about_us = $this->page_model->getPages(array('page_id'=>7,'pages.del_flag'=>0,'pages.status'=>1))->row_array();
     return $about_us;


}

 function getmoa()
{
    //$this->load->module_model('page','page_model');
    //$moa = $this->page_model->getPages(array('page_id'=>37,'pages.del_flag'=>0,'pages.status'=>1))->row_array();
    $moa = Page::where('page_id',37)->where('del_flag',0)->with('status',1)->first();    
    return $moa;
}

function get_preferences($item)
{
    if($item==null)
    {
        return DB::select('SELECT * FROM `tbl_preferences`');
    }
    else
    {
        $data=DB::select("SELECT value FROM `tbl_preferences` where name='$item'");
        return $data[0]->value;
    }
        
}

//from manish
function vacancies() 
{
 /*   $this->load->module_model('vacancydatum','vacancydatum_model');
    $this->lang->module_load('vacancydatum','vacancydatum');
    date_default_timezone_set('Asia/Kathmandu');
    $date = date('Y-m-d H:i:s');  

    $where=array('del_flag'=>0,"vacancy_start_date <= '".$date."' "=>NULL,"vacancy_end_date >= '".$date."'"=>NULL); 
    
    $data['vanacncies']=$this->vacancydatum_model->getVacancydatas($where)->result_array();
*/
    $vanacncies = DB::table('tbl_vacancydata')->where([['del_flag',0], ['vacancy_start_date' , '<=' , new Carbon()], ['vacancy_end_date', '>=', new Carbon()]])->get();
    return $vanacncies;
}

//barsha

function getbanking_hour_menu() 
{
    // $this->load->module_model('page','page_model');
    // $this->db->select('page_title,slug_name');
    // $about_us = $this->page_model->getPages(array('page_id'=>23,'pages.del_flag'=>0,'pages.status'=>1))->row_array();
    // return $about_us; 
    $banking_hour_menu = Page::where('page_id',23)->where('del_flag',0)->where('status',1)->first();  
    return $banking_hour_menu;

}//yeha
function getLink()
{
    // $this->load->module_model('social_media_link','social_media_link_model');
    // $this->db->where("((approvel_status = 'ok' AND approved = 'approved') OR (approvel_status ='delete' And approved='denied')) AND del_flag = 0 AND status = 1");
    // $data=$this->social_media_link_model->getSocialMediaLinks(array('del_flag'=>0,'status'=>1))->result_array();
    //$data=Social_media_link::where("approvel_status",'ok')->where('approved','approved')->where('approvel_status','delete')->where('approved','denied')->where('del_flag',0)->where('status',1)->first();  
    $data= Social_media_link::where('del_flag',0)->where('status',1)->get();
    
    return $data;

}

function get_lists($table, $where = null, $orderBy = null, $ascdsc = null, $limit = null, $offset = null) {
    
    //forexample  $where = [['approvel_status', "ok"], ['approved', "approved"]];
    if ($where) {
        $whereval = $where;
    } else {
        $whereval = null;
    }

    $data = DB::table($table)
                ->where( function($query) use ($where, $whereval) {
                            if($where != null) {
                                foreach($where as $field => $v) {
                                    if (gettype($v) == "array") {
                                        if (count($v) == 3) {
                                            $query->where($v[0], $v[1], $v[2]);
                                        } else {
                                            $query->where($v[0], $v[1]);
                                        }
                                    } else {
                                        if (count($whereval) == 3) {
                                            $query->where($whereval[0], $whereval[1], $whereval[2]);
                                        } else {
                                            $query->where($whereval[0], $whereval[1]);
                                        }
                                        break;
                                    }
                                }
                            }
                            $query->where([['approvel_status', "ok"], ['approved', "approved"], ['del_flag', 0], ['status', 1]]);
                })
    
                ->orWhere( function($query) use ($where, $whereval) {
                            if($where != null) {
                                foreach($where as $field => $v) {
                                    if (gettype($v) == "array") {
                                        if (count($v) == 3) {
                                            $query->where($v[0], $v[1], $v[2]);
                                        } else {
                                            $query->where($v[0], $v[1]);
                                        }
                                    } else {
                                        if (count($whereval) == 3) {
                                            $query->where($whereval[0], $whereval[1], $whereval[2]);
                                        } else {
                                            $query->where($whereval[0], $whereval[1]);
                                        }
                                        break;
                                    }
                                    
                                }
                            }
                            $query->where([['approvel_status', "delete"], ['approved', "denied"], ['del_flag', 0], ['status', 1]]);
                });
    if ($orderBy && $ascdsc) {
        $data = $data->orderBy($orderBy, $ascdsc);
    }
    if ($limit) {
        $data = $data->limit($limit);
    }
    if ($offset) {
        $data = $data->offset($offset);
    }
    $data = $data->get()->toArray();
    $lists = [];
    if (count($data) > 0) {
        foreach ($data as $index => $value) {
            foreach ($value as $key => $val) {
                $lists[$index][$key] = $val;
            }
        }
    }
    return $lists;
}

function objToArray($obj)
{
    $array = [];
    for ($i = 0, $c = count($obj); $i < $c; ++$i) {
        $array[$i] = (array) $obj[$i];
    }

    return $array;

}

function preference($item)
{
    if($item==null)
    {
        return DB::select('SELECT * FROM `tbl_preferences`');
    }
    else
    {
        $data=DB::select("SELECT value FROM `tbl_preferences` where name='$item'");
        return $data[0]->value;
    }
        
}
