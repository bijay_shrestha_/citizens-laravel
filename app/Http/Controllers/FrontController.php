<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modules\banner_collection\Model\Banner_collection;
use App\modules\banner\Model\Banner;
use App\modules\advertisement\Model\Advertisement;
use App\modules\blog\Model\Blog;
use App\modules\product\Model\Product;
use App\modules\service\Model\Service;
use DB;
use App\modules\forex\Model\Forex;
use App\modules\stock\Model\Stock;
use App\modules\loan_interest_rate\Model\Loan_interest_rate;
use App\modules\loan_parent\Model\Loan_parent;
use App\modules\base_rate\Model\Base_rate;
use App\modules\interest_rate_spread\Model\Interest_rate_spread;
use App\modules\account\Model\Account;



class FrontController extends Controller
{
    public function index()
    {
		$page['title'] = 'Citizen | Home';
		$data['banners']= DB::select('select * from tbl_banners where status=1 and del_flag=0 and banner_collection_id = 12');
		// $data['banners']= Banner_collection::where('del_flag',0)->where('status',1)->get();
		$data['advertisements']=Advertisement::all()->take(2);
		$data['products']=Product::all()->take(6);
		$data['services']=Service::all();
		$data['blogs']=Blog::all()->take(1);
		$data['forexs']=Forex::where('status','1')->orderBy('sequence', 'ASC')->get();
		$page['class'] = 'home-page';
		
// 		dd($data['banners']);
	
		//stock
// 		$url = "http://nepalstock.com/todaysprice?stock-symbol=CZBIL";
// 		$ch = curl_init();
// 		curl_setopt ($ch, CURLOPT_URL, $url);
// 		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
// 		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
// 		$contents = curl_exec($ch);
	
	
// 		if (curl_errno($ch)) {
		  
// 		  $contents = '';
// 		} else {
// 		  curl_close($ch);
// 		}
	
// 	   if(!empty($contents) || $contents != ""  || $contents != null){
// 		$title_start = '<td class="alnright">';
// 		$parts = explode($title_start,$contents);
// 		 $data['price']= intval($parts[12]);
// 			$data['today_date'] = date('Y-m-d');
// 		 $data['closingPrice']= intval($parts[13]);
// 		$closing_price =$data['price'];
// 		  $turnover =$data['closingPrice'];
// 		 $created_time =date('Y-m-d');
// 		DB::update("UPDATE `tbl_stock` SET `trunover` = '$turnover', `closing_price` = '$closing_price' ,`created_time` = '$created_time' WHERE `tbl_stock`.`stock_id` = 1;");
// 			$stock=Stock::where('stock_id','1')->get();
// 		   $data['stockvalue']=$stock;
	
// 	}else{
			
		//	$data['stockvalue']= Stock::get('stock_id','1')->get();
			//$data['stockvalue']= $this->stock_model->getStocks()->result_array(); 
			$stock=Stock::where('stock_id','1')->get();
			$data['stockvalue']=$stock;
			$data['today_date'] = date('Y-m-d');
// 	}
		//stock
		//$this->db->where("((approvel_status = 'ok' AND approved = 'approved') OR (approvel_status ='delete' And approved='denied')) AND del_flag = 0 AND status = 1");
		//$data['forexs']=$this->forex_model->getForex(array('status'=>1),'sequence asc')->result_array();
		$data['forexs']=DB::table('tbl_forex')->where([['approvel_status', "ok"],['approved', "approved"]])->orWhere([['approvel_status', "delete"], ['approved', "denied"], ['del_flag', 0]])->get();

		//$this->db->where("((approvel_status = 'ok' AND approved = 'approved') OR (approvel_status ='delete' And approved='denied')) AND del_flag = 0 AND status = 1");

//US Dollar
 //$this->db->where('added_date >=', $time);
 //$this->db->where('added_date <=', $cur_date);
 //$data['forexdata'] = $this->forex_model->getForex(array('currency' =>'US Dollar'))->result_array();

//  $this->db->where('added_date >=', $time);
//  $this->db->where('added_date <=', $cur_date);
//  $data['forexdata'] = $this->forex_model->getForex(array('currency' =>'US Dollar'))->result_array();
 	$data['forexdata']=DB::table('tbl_forex')->where('currency', "US Dollar")->get();
    // dd($data['forexdata']);
		return view('front.home.index',compact('page','data'));
	}
	
	public function interest_rate($type = 'ir') 
	{
//		dd('up to here');
		$accounts = get_lists('view_accounts', ['rate_del_flag', 0]);
//		$counts = DB::table('view_accounts')->select(DB::raw('SELECT `name`,`fixed_rate`,count(account_id) as count FROM `view_accounts` WHERE `del_flag` = 0 AND `rate_del_flag` = 0 GROUP BY `name`'))->get();
		$counts = DB::table('view_accounts')->select(DB::raw('count(*) as count, name, fixed_rate'))->where([['del_flag', 0], ['rate_del_flag', 0], ['status', 1]])->groupBy('name')->get();
		$counts = objToArray($counts);
		$loans = get_lists('tbl_loans');
		$loan_rates = Loan_interest_rate::where([['del_flag', 0], ['status', 1]])->get()->toArray();
		$loan_parents = Loan_parent::where([['del_flag', 0], ['status', 1]])->get()->toArray();
		//dd($loans, $counts, $loan_rates, $loan_parents);
		$active = $type;
		$base_rate = get_lists('tbl_base_rates', null, 'base_rate_id', 'DSC');
		$i=0;
		foreach($base_rate as $rate)
		{
		    $base_rate[$i]['fiscal_rate'] = date('Y',strtotime($rate['date']));
		    $base_rate[$i]['fiscal_month'] = date('m',strtotime($rate['date']));
		    $i++;
		}
		$max_date = date('Y', strtotime(Base_rate::max('date')));
		$min_base_date = date('Y', strtotime(Base_rate::min('date')));
		$banners = Banner::where([['del_flag', 0], ['status', 1],['banner_collection_id', 138]])->get()->toArray();
		$feecharges = get_lists('tbl_fee_charges');
		$feecommisions = get_lists('tbl_fees_commisions');
		$interestratespreads = get_lists('tbl_interest_rate_spread');
		$i=0;
		foreach($interestratespreads as $rate)
		{
		    $interestratespreads[$i]['fiscal_rate'] = date('Y',strtotime($rate['date']));
		     $interestratespreads[$i]['fiscal_month'] = date('m',strtotime($rate['date']));
		  $i++;  
		}
		$max_spread_date = date('Y', strtotime(Interest_rate_spread::max('date')));
		$min_spread_date = date('Y',strtotime(Interest_rate_spread::min('date')));
		$forexs = get_lists('tbl_forex', null, 'sequence', 'asc');
		$base_rates = $base_rate;

		return view('front.rate.rates_and_charges', compact('accounts', 'counts', 'loans', 'loan_rates', 'loan_parents', 'active', 'base_rates', 'max_date', 'min_base_date', 'banners', 'feecharges', 'feecommisions', 'interestratespreads', 'max_spread_date', 'min_spread_date', 'forexs'));

	}

	public function calculator()
	{
		$accounts = get_lists('tbl_accounts', ['business_type', 'PERSONAL']);
		$banners = Banner::where([['del_flag', 0], ['status', 1],['banner_collection_id', 108]])->get()->toArray();		
		return view('front.home.calculator', compact('accounts', 'banners'));
	}

}
