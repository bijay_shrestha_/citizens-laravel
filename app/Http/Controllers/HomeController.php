<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\User;
use Illuminate\Support\Facades\Hash;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Admin | Dashboard';
        
        return view('admin.home',compact('page'));
    }

    // private static function GetAccessToken($client_id, $redirect_uri, $client_secret, $code) {      
    //     $url = 'https://api.instagram.com/oauth/access_token';
        
    //     $curlPost = 'client_id='. $client_id . '&redirect_uri=' . $redirect_uri . '&client_secret=' . $client_secret . '&code='. $code . '&grant_type=authorization_code';
    //     $ch = curl_init();      
    //     curl_setopt($ch, CURLOPT_URL, $url);        
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //     curl_setopt($ch, CURLOPT_POST, 1);      
    //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);            
    //     $data = json_decode(curl_exec($ch), true);  
    //     $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
    //     curl_close($ch);        
    //     if($http_code != '200')         
    //         throw new Exception('Error : Failed to receieve access token');
        
    //     return $data['access_token'];   
    // }
     
    // private static function GetUserProfileInfo($access_token) { 
    //     $url = 'https://api.instagram.com/v1/users/self/?access_token=' . $access_token;    

    //     $ch = curl_init();      
    //     curl_setopt($ch, CURLOPT_URL, $url);        
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);    
    //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    //     $data = json_decode(curl_exec($ch), true);
    //     $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
    //     curl_close($ch); 
    //     if($data['meta']['code'] != 200 || $http_code != 200)
    //         throw new Exception('Error : Failed to get user information');

    //     return $data['data'];
    // }


    // private static function GetUserMedia($access_token) { 
    //     $url = 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' . $access_token;    

    //     $ch = curl_init();      
    //     curl_setopt($ch, CURLOPT_URL, $url);        
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);    
    //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    //     $data = json_decode(curl_exec($ch), true);
    //     $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
    //     curl_close($ch); 
    //     if($data['meta']['code'] != 200 || $http_code != 200)
    //         throw new Exception('Error : Failed to get user information');

    //     return $data['data'];
    // }

    // public function storeUser(Request $request){
    //     if(empty($request->id)){
    //         $this->validate($request,[
    //             'username'=> 'required',
    //             'email'=> 'email |unique:users,email',
    //             'control'=> 'required',
    //             'status'=> 'required',
    //             'password'=>'required|confirmed|min:6',
    //         ]);
    //         $user =DB::table('users')->insertGetId([
    //             'username'              =>$request->username,
    //             'email'                 =>$request->email,
    //             'password'              =>Hash::make($request->password),
    //             'remember_token'        =>$request->_token,
    //             'control'               =>$request->control,
    //             'active'                =>$request->status,
    //             'activation_key'        =>rand(000000000,999999999),
    //             'last_visit'             =>date('Y-m-d H:m:s'),
    //             'created_at'            =>date('Y-m-d H:m:s'),
    //             'updated_at'            =>date('Y-m-d H:m:s'),
    //         ]);
    //         $return = DB::table('role_user')->insert(["user_id"=>$user,"role_id"=>$request->role_id]);
    //     }
    //     else{
    //         $user = User::find($request->id);
    //         if(empty($request->password)){
    //             $this->validate($request,[
    //                 'username'=> 'required',
    //                 'email'=> 'email',
    //                 'control'=> 'required',
    //                 'status'=> 'required',
    //             ]);
    //             $user->username         = $request->username;
    //             $user->email            = $request->email;
    //             $user->control          = $request->control;
    //             $user->active           = $request->status;
    //             $user->remember_token   =$request->_token;
    //             $user->last_visit       =date('Y-m-d H:m:s');
    //             $user->updated_at       =date('Y-m-d H:m:s');
    //             $user->save();
    //         }
    //         else{
    //             $this->validate($request,[
    //                 'name'=> 'required',
    //                 'email'=> 'email',
    //                 'password'=>'required|confirmed|min:6',
    //                 'control'=> 'required',
    //                 'status'=> 'required',
    //             ]);
    //             $user->name             = $request->name;
    //             $user->email            = $request->email;
    //             $user->password         = Hash::make($request->password);
    //             $user->remember_token   =$request->_token;
    //             $user->last_visit       =date('Y-m-d H:m:s');
    //             $user->updated_at       =date('Y-m-d H:m:s');
    //             $user->save();
    //         }
    //         $check =DB::table('role_user')->where('user_id',$user->id)->get();
    //         if(count($check)==0){
    //             $return = DB::table('role_user')->insert(["user_id"=>$request->id,"role_id"=>$request->role_id]);
    //         }
    //         else{
    //             $return = DB::table('role_user')->where('user_id',$request->id)->update(["user_id"=>$request->id,"role_id"=>$request->role_id]);
    //         }
    //     }
    //     if($return)
    //     {
    //         $message = "User Data Saved";
    //         return back()->with(compact('message'));
    //     }
    //     else{
    //         $alert = "User Data Not Saved";
    //         return back()->with(compact('alert'));
    //     }
    // }

}



// function SendRequest($url, $post, $post_data, $user_agent, $cookies) {
//     $ch = curl_init();
//     curl_setopt($ch, CURLOPT_URL, 'https://i.instagram.com/api/v1/'.$url);
//     curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
//     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//     curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

//     if($post) {
//         curl_setopt($ch, CURLOPT_POST, true);
//         curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
//     }

//     if($cookies) {
//         curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookies.txt');            
//     } else {
//         curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookies.txt');
//     }

//     $response = curl_exec($ch);
//     $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//     curl_close($ch);

//    return array($http, $response);
// }

// function GenerateGuid() {
//      return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', 
//             mt_rand(0, 65535), 
//             mt_rand(0, 65535), 
//             mt_rand(0, 65535), 
//             mt_rand(16384, 20479), 
//             mt_rand(32768, 49151), 
//             mt_rand(0, 65535), 
//             mt_rand(0, 65535), 
//             mt_rand(0, 65535));
// }

// function GenerateUserAgent() {  
//      $resolutions = array('720x1280', '320x480', '480x800', '1024x768', '1280x720', '768x1024', '480x320');
//      $versions = array('GT-N7000', 'SM-N9000', 'GT-I9220', 'GT-I9100');
//      $dpis = array('120', '160', '320', '240');

//      $ver = $versions[array_rand($versions)];
//      $dpi = $dpis[array_rand($dpis)];
//      $res = $resolutions[array_rand($resolutions)];

//      return 'Instagram 4.'.mt_rand(1,2).'.'.mt_rand(0,2).' Android ('.mt_rand(10,11).'/'.mt_rand(1,3).'.'.mt_rand(3,5).'.'.mt_rand(0,5).'; '.$dpi.'; '.$res.'; samsung; '.$ver.'; '.$ver.'; smdkc210; en_US)';
//  }

// function GenerateSignature($data) {
//      return hash_hmac('sha256', $data, 'b4a23f5e39b5929e0666ac5de94c89d1618a2916');
// }

// function GetPostData($filename) {
//     if(!$filename) {
//         echo "The image doesn't exist ".$filename;
//     } else {
//         $post_data = array('device_timestamp' => time(), 
//                         'photo' => '@'.$filename);
//         return $post_data;
//     }
// }


// // Set the username and password of the account that you wish to post a photo to
// $username = 'ig_username';
// $password = 'ig_password';

// // Set the path to the file that you wish to post.
// // This must be jpeg format and it must be a perfect square
// $filename = 'pictures/test.jpg';

// // Set the caption for the photo
// $caption = "Test caption";

// // Define the user agent
// $agent = GenerateUserAgent();

// // Define the GuID
// $guid = GenerateGuid();

// // Set the devide ID
// $device_id = "android-".$guid;

// /* LOG IN */
// // You must be logged in to the account that you wish to post a photo too
// // Set all of the parameters in the string, and then sign it with their API key using SHA-256
// $data ='{"device_id":"'.$device_id.'","guid":"'.$guid.'","username":"'.$username.'","password":"'.$password.'","Content-Type":"application/x-www-form-urlencoded; charset=UTF-8"}';
// $sig = GenerateSignature($data);
// $data = 'signed_body='.$sig.'.'.urlencode($data).'&ig_sig_key_version=4';
// $login = SendRequest('accounts/login/', true, $data, $agent, false);

// if(strpos($login[1], "Sorry, an error occurred while processing this request.")) {
//     echo "Request failed, there's a chance that this proxy/ip is blocked";
// } else {            
//     if(empty($login[1])) {
//         echo "Empty response received from the server while trying to login";
//     } else {            
//         // Decode the array that is returned
//         $obj = @json_decode($login[1], true);

//         if(empty($obj)) {
//             echo "Could not decode the response: ".$body;
//         } else {
//             // Post the picture
//             $data = GetPostData($filename);
//             $post = SendRequest('media/upload/', true, $data, $agent, true);    

//             if(empty($post[1])) {
//                  echo "Empty response received from the server while trying to post the image";
//             } else {
//                 // Decode the response 
//                 $obj = @json_decode($post[1], true);

//                 if(empty($obj)) {
//                     echo "Could not decode the response";
//                 } else {
//                     $status = $obj['status'];

//                     if($status == 'ok') {
//                         // Remove and line breaks from the caption
//                         $caption = preg_replace("/\r|\n/", "", $caption);

//                         $media_id = $obj['media_id'];
//                         $device_id = "android-".$guid;
//                         $data = '{"device_id":"'.$device_id.'","guid":"'.$guid.'","media_id":"'.$media_id.'","caption":"'.trim($caption).'","device_timestamp":"'.time().'","source_type":"5","filter_type":"0","extra":"{}","Content-Type":"application/x-www-form-urlencoded; charset=UTF-8"}';   
//                         $sig = GenerateSignature($data);
//                         $new_data = 'signed_body='.$sig.'.'.urlencode($data).'&ig_sig_key_version=4';

//                        // Now, configure the photo
//                        $conf = SendRequest('media/configure/', true, $new_data, $agent, true);

//                        if(empty($conf[1])) {
//                            echo "Empty response received from the server while trying to configure the image";
//                        } else {
//                            if(strpos($conf[1], "login_required")) {
//                                 echo "You are not logged in. There's a chance that the account is banned";
//                             } else {
//                                 $obj = @json_decode($conf[1], true);
//                                 $status = $obj['status'];

//                                 if($status != 'fail') {
//                                     echo "Success";
//                                 } else {
//                                     echo 'Fail';
//                                 }
//                             }
//                         }
//                     } else {
//                         echo "Status isn't okay";
//                     }
//                 }
//             }
//         }
//     }
// }


