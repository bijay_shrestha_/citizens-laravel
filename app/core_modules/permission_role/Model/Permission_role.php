<?php

namespace App\Core_modules\Permission_role\Model;


use Illuminate\Database\Eloquent\Model;

class Permission_role extends Model
{
    public  $table = 'permission_role';

    protected $fillable = ['permission_id','role_id',];
}
