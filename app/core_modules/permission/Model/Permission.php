<?php

namespace App\Core_modules\Permission\Model;


use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public  $table = 'permissions';

    protected $fillable = ['id','name','display_name','description','created_at','updated_at',];
}
