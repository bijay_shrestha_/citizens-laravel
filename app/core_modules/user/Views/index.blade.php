@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Users		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Users</a></li>

		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.users.create') }}" class="btn bg-green waves-effect"  title="create">Create</a>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="user-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								<th >Username</th>
								<!-- <th >Control</th> -->
								<!-- <th >Last_visit</th> -->
								<!-- <th >Status</th> -->
								<th >Email</th>
								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#user-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.users.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
		            { data: "username",name: "username"},
		            // { data: "last_visit",name: "last_visit"},
		        //     { data:  function (data, type, row, meta) {
			       // 		if(data.status == 1){
			       // 			return 'Enabled'
			       // 		}else{
			       // 			return 'Disabled'
			       // 		}
			      	// },name: "status"},
		            { data: "email",name: "email"},            
					{ data: function(data,b,c,table) { 
					var buttons = '';
					if(data.id != 1 && data.id != 2){
						buttons += "<a class='btn btn-default  waves-effect' href='"+site_url+"/edit/"+data.id+"' type='button' ><i class='fa fa-pencil'></i></a>&nbsp"; 

						buttons += "<a href='"+site_url+"/delete/"+data.id+"' class='btn btn-default  waves-effect' ><i class='fa fa-trash'></i></a>";
						
					}


					return buttons;
					}, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
