<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\modules\account_detail\Model\Personal_detail;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Excel;

class individual_personal_details implements FromView, ShouldAutoSize
{

	public function __construct($view, $data = "")
	{
	    $this->view = $view;
	    $this->data = $data;
	    // $this->remaining = $remaining;
	}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
	{
		// $data= $this->data;
	    return view($this->view,[
	        'data'=>$this->data
	    ]);
	}

}
