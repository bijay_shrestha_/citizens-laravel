<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\modules\account_detail\Model\Account_detail;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class Account_details_list implements FromView, ShouldAutoSize
{

	public function __construct($view, $data = "")
	{
	    $this->view = $view;
	    $this->data = $data;
	    // $this->remaining = $remaining;
	}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
	{
		// $data= $this->data;
	    return view($this->view,[
	        'data'=>$this->data
	    ]);
	}

}



