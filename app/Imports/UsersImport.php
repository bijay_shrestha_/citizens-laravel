<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

//use Maatwebsite\Excel\Concerns\FromCollection;
use App\modules\interest_rate_spread\Model\Interest_rate_spread;
use Auth;
use Carbon\Carbon;


class UsersImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
/*        foreach ($collection as $collec) {
        	$success = Interest_rate_spread::Insert($collec);
        }
*/
        foreach ($rows as $index => $row) {
        	if ($index > 0) {
        		$datafor_ratespread['name'] = $row[0];
        		$datafor_ratespread['interest_rate_spread_rate'] = $row[1];
        		$datafor_ratespread['date'] = date("Y-m-d", strtotime($row[2]));
        		$datafor_ratespread['status'] = 1;
        		$datafor_ratespread['added_by'] = Auth::user()->id;
        		$datafor_ratespread['added_date'] = new Carbon();
        		$datafor_ratespread['del_flag'] = 0;
        		$datafor_ratespread = check_case($datafor_ratespread, 'create');
        		$success = Interest_rate_spread::Insert($datafor_ratespread);
        	}
        }
        
//        return Interest_rate_spread::all();
    }
}
