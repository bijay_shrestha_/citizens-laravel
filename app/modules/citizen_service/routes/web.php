<?php



Route::group(array('prefix'=>'admin/','module'=>'citizen_service','middleware' => ['web','auth'], 'namespace' => 'App\modules\citizen_service\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('citizen_services/','AdminCitizen_serviceController@index')->name('admin.citizen_services');
    Route::post('citizen_services/getcitizen_servicesJson','AdminCitizen_serviceController@getcitizen_servicesJson')->name('admin.citizen_services.getdatajson');
    Route::get('citizen_services/create','AdminCitizen_serviceController@create')->name('admin.citizen_services.create');
    Route::post('citizen_services/store','AdminCitizen_serviceController@store')->name('admin.citizen_services.store');
    Route::get('citizen_services/show/{id}','AdminCitizen_serviceController@show')->name('admin.citizen_services.show');
    Route::get('citizen_services/edit/{id}','AdminCitizen_serviceController@edit')->name('admin.citizen_services.edit');
    Route::match(['put', 'patch'], 'citizen_services/update/{id}','AdminCitizen_serviceController@update')->name('admin.citizen_services.update');
    Route::get('citizen_services/delete/{id}', 'AdminCitizen_serviceController@destroy')->name('admin.citizen_services.edit');
});




Route::group(array('module'=>'citizen_service','namespace' => 'App\modules\citizen_service\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('citizen_services/','Citizen_serviceController@index')->name('citizen_services');
    
});