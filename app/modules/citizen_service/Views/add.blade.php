@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Citizen_services   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.citizen_services') }}">tbl_citizen_service</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.citizen_services.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                                    <label for="s_id">S_id</label><input type="text" name="s_id" id="s_id" class="form-control" ></div><div class="form-group">
                                    <label for="a_id">A_id</label><input type="text" name="a_id" id="a_id" class="form-control" ></div><div class="form-group">
                                    <label for="visa">Visa</label><input type="text" name="visa" id="visa" class="form-control" ></div><div class="form-group">
                                    <label for="mobile_banking">Mobile_banking</label><input type="text" name="mobile_banking" id="mobile_banking" class="form-control" ></div><div class="form-group">
                                    <label for="internet_banking">Internet_banking</label><input type="text" name="internet_banking" id="internet_banking" class="form-control" ></div><div class="form-group">
                                    <label for="locker_facilities">Locker_facilities</label><input type="text" name="locker_facilities" id="locker_facilities" class="form-control" ></div><div class="form-group">
                                    <label for="ntc">Ntc</label><input type="text" name="ntc" id="ntc" class="form-control" ></div><div class="form-group">
                                    <label for="e-statement">E-statement</label><input type="text" name="e-statement" id="e-statement" class="form-control" ></div><div class="form-group">
                                    <label for="standing_instruction">Standing_instruction</label><input type="text" name="standing_instruction" id="standing_instruction" class="form-control" ></div><div class="form-group">
                                    <label for="other_service">Other_service</label><input type="text" name="other_service" id="other_service" class="form-control" ></div>
<input type="hidden" name="id" id="id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.citizen_services') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
