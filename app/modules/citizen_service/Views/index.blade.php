@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Citizen_services		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Citizen_services</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.citizen_services.create') }}" class="btn bg-green waves-effect"  title="create">Create</a>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="citizen_service-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								<th >S_id</th>
<th >A_id</th>
<th >Visa</th>
<th >Mobile_banking</th>
<th >Internet_banking</th>
<th >Locker_facilities</th>
<th >Ntc</th>
<th >E-statement</th>
<th >Standing_instruction</th>
<th >Other_service</th>

								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#citizen_service-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.citizen_services.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
					{ data: "s_id",name: "s_id"},
            { data: "a_id",name: "a_id"},
            { data: "visa",name: "visa"},
            { data: "mobile_banking",name: "mobile_banking"},
            { data: "internet_banking",name: "internet_banking"},
            { data: "locker_facilities",name: "locker_facilities"},
            { data: "ntc",name: "ntc"},
            { data: "e-statement",name: "e-statement"},
            { data: "standing_instruction",name: "standing_instruction"},
            { data: "other_service",name: "other_service"},
            
					{ data: function(data,b,c,table) { 
					var buttons = '';

					buttons += "<a class='btn bg-red waves-effect' href='"+site_url+"/edit/"+data.id+"' type='button' >Edit</a>&nbsp"; 

					buttons += "<a href='"+site_url+"/delete/"+data.id+"' class='btn bg-red waves-effect' >Delete</a>";

					return buttons;
					}, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
