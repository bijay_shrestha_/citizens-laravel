<?php

namespace App\Modules\Citizen_service\Model;


use Illuminate\Database\Eloquent\Model;

class Citizen_service extends Model
{
    public  $table = 'tbl_citizen_service';

    protected $fillable = ['s_id','a_id','visa','mobile_banking','internet_banking','locker_facilities','ntc','e-statement','standing_instruction','other_service',];
}
