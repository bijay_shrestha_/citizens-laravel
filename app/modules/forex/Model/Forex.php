<?php

namespace App\Modules\Forex\Model;


use Illuminate\Database\Eloquent\Model;

class Forex extends Model
{
    public  $table = 'tbl_forex';

    protected $fillable = ['ex_rate_id','currency','unit','cash_buying','buying','selling','ex_date','sequence','approvel_status','approved','added_date','added_by','modified_date','deleted_date','deleted_by','del_flag','status',];
}
