<?php

namespace App\modules\forex\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\banner\Model\Banner;


class ForexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'forexes';
        $forexs = get_lists('tbl_forex');
        $banners = Banner::where([['del_flag', 0], ['status', 1],['banner_collection_id', 109]])->get()->toArray();        
         return view('front.forex.forex', compact('forexs', 'banners'));
        //
    }

}
