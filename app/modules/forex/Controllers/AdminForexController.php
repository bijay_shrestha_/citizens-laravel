<?php

namespace App\modules\forex\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\forex\Model\Forex;
use Carbon\Carbon;

class AdminForexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Forex';
        return view("forex::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getforexesJson(Request $request)
    {
        $forex = new Forex;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $forex->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->orderBy('ex_rate_id', "DSC")->where('del_flag',0)->get();

        // Display limited list        
        $rows = $forex->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->orderBy('ex_rate_id', "DSC")->where('del_flag',0)->get();

        //To count the total values present
        $total = $forex->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Forex | Create';
        return view("forex::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $datafor_forex = $data;

        if ($datafor_forex['ex_date']) {
            $datafor_forex['ex_date'] = date('Y-m-d H:i:s',strtotime($data['ex_date']));
        }
        $datafor_forex['sequence'] = $datafor_forex['sequence'] ? $datafor_forex['sequence']: 0;
        $datafor_forex['added_by'] = Auth::user()->id;
        $datafor_forex['added_date'] = new Carbon();
        $datafor_forex['del_flag'] = 0;
        $datafor_forex = check_case($datafor_forex, 'create');
        $success = Forex::Insert($datafor_forex);
        return redirect()->route('admin.forexes')->with('success', "Data inserted successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $forex = Forex::where('ex_rate_id',$id)->firstOrFail();
        $page['title'] = 'Forex | Update';
        return view("forex::edit",compact('page','forex'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $vacancydata = Forex::where('ex_rate_id',$id)->firstOrFail();
        $datafor_forex = $data;

        if ($datafor_forex['ex_date']) {
            $datafor_forex['ex_date'] = date('Y-m-d H:i:s',strtotime($data['ex_date']));
        }
        $datafor_forex['sequence'] = $datafor_forex['sequence'] ? $datafor_forex['sequence']: 0;
        $datafor_forex['modified_date'] = new Carbon();
        $datafor_forex = check_case($datafor_forex, 'edit');
        DB::table('tbl_forex')->where('ex_rate_id', $id)->update($datafor_forex);
        return redirect()->route('admin.forexes')->with('success', "Data updated successfully.");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $data = soft_delete('tbl_forex', 'ex_rate_id', $id, 'deleted_by', 'deleted_date');
        return redirect()->route('admin.forexes')->with('success', "Data deleted successfully.");
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_forex', 'ex_rate_id', 'deleted_by', 'deleted_date');
        return redirect()->route('admin.forexes')->with('success', "Data updated successfully.");
    }

    public function uploader() {
        $upload_lists = DB::table('tbl_forex_uploads')->where('del_flag', 0)->get()->toArray();
        $data = []; 
        for ($i = 0; $i < count($upload_lists); $i++) {
            foreach ($upload_lists[$i] as $index => $u) {
                $data[$i][$index] = $u;
            }
        }
        $upload_list = $data;
        $page['title'] = 'Forex | Update';
        return view("forex::uploader",compact('page','upload_list'));

    }

    public function upload_file(Request $request) 
    {
        $this->validate($request, ['filename' => 'required|mimes:xls,xlsx,xlsm']);
        $upload_list = DB::table('tbl_forex_uploads')->get()->toArray();
        if (count($upload_list) > 0) {
            $soft_delete = DB::table('tbl_forex_uploads')->where('del_flag', '!=', 1)->update(array('del_flag' => 1, 'deleted_date' => new Carbon(), 'deleted_by' => Auth::user()->id));
        }
        $thefile = $request->file('filename');
        $new_name = time() . '.' . $request->filename->getClientOriginalExtension();
        $thefile->move(public_path('uploads/forex'), $new_name);
        $data['filename'] = $new_name;
        $data['added_by'] = Auth::user()->id;
        $data['added_date'] = new Carbon();
        $data['del_flag'] = 0;
        $data['status'] = "imported";
        $data['imported_date'] = new Carbon();
        DB::table('tbl_forex_uploads')->insert($data);

        return redirect()->route('admin.forexes.uploader')->with('success', "Data added successfully.");
    }

    public function delete_file($id) 
    {
        $data = DB::table('tbl_forex_uploads')->where('forex_upload_id', $id)->first();
        if ($data) {
            DB::table('tbl_forex_uploads')->where('forex_upload_id', $id)->update(array('del_flag' => 1, 'deleted_date' => new Carbon(), 'deleted_by' => Auth::user()->id));
            return redirect()->route('admin.forexes.uploader')->with('success', "Data deleted successfully.");

        } else {
            return redirect()->route('admin.forexes.uploader');
        }
    }

}
