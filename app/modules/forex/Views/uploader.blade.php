@extends('admin.layout.main')
@section('content')
<section class="content-header">
    <h1>
        Upload Forex
        <small></small>                    
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Upload Forex</li>
    </ol>
</section>

<div class="pad margin no-print">
<div class="pull-right">
   

					<form action="{{ route('admin.forexes.upload_file') }}" method="post" enctype="multipart/form-data">
						<table>
						<tr>
							{{csrf_field()}}
						<td><input type="file" id="file_name" name="filename"></td>
						<td><button type="submit" class="btn btn-primary btn-sm">
								 <i class="fa fa-plus-circle"></i>  AddNewFile
							</button>

						</td>
						<!-- <td><a href="{{url('/')}}?>uploads/csv/sample1.xlsx" target="_blank" class="btn btn-primary btn-sm">
								<span class="glyphicon glyphicon-download"> DownloadSample</span> 
							</a>
						</td> -->
						</tr>
						<tr>
							<td>
								{!! ($errors->first('filename','<font color="red"> :message</font>')) !!}
							</td>
						</tr>
						</table>
     				</form>
    </div>
</div> 				
<br>
<br>
<br>
@if ($message = Session::get('success'))
<div class="btn btn-success">
	{{$message}}
</div>
@endif

<br>
					@if(count($upload_list)>0)
					<table class="table table-striped">
						<tr>
							<th>File Title</th>
							<th>Imported Date</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						
						
							@foreach($upload_list as $list)
							<tr>
								<td><a href="{{@url('/uploads/forex', $list['filename'])}}" target="_blank" >{{$list['filename']}}</a></td>
								<td>{{$list['imported_date']}}</td>
								<td>{{$list['status']}}</td>
								<td>
									@if($list['status'] == 'pending')
										<button type="button" class="btn btn-primary btn-sm" onClick="importFile({{$list['forex_upload_id']}})" style="float:left;margin-left:4px;">
											<i class="fa fa-cloud-download"></i> Import</span> 
										</button>
										@endif
										<!-- <button type="button" class="btn btn-danger btn-sm" onClick="deleteFile({{$list['forex_upload_id']}})" style="float:left;margin-left:4px;">
											 <i class="fa fa-trash-o"></i> Delete</span>
										</button> -->
										<a href="{{route('admin.forexes.delete_file', $list['forex_upload_id'])}}" class="btn btn-danger btn-sm" style="float:left;margin-left:4px;">
											 <i class="fa fa-trash-o"></i> Delete</span>
										</button>
								</td>
								</tr>
							@endforeach
						
					</table>
					@endif
					</div>
            </div>
			
        </div>
    </div>
</div>  <!--Content Wrapper--> 
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>-->
<script>
$(document).ready(function(){
		$('#checkall').on('click',function(){
			var status = false;
			if($(this).is(':checked'))
			{
				status = true;
			}
			$('.get_checked').prop('checked',status);
		});
	});
	
function deleteSelected()
{
	console.log($('.get_checked:checked').length);
	if($('.get_checked:checked').length > 0)
	{
		if(confirm('Are you sure to delete the files?')){	
			var selected = [];
			$(".get_checked:checked").each(function(){
					selected.push(this.value);
				});
	
				$.post('{{url("forex/admin/forex/delete_file")}}',{checked:selected},function(data){
					if(data.success == 'true')
					{
						
						window.location.href = '{{url("forex/admin/forex/uploader")}}'; 
					}
					else
					{
						alert('Unable to delete');
						//window.location.href = '{{url("csv")}}'; 
					}
					
				});
		}
			return false;
	}
	else
	{
		alert('Please select at least one to Delete');
		return false;
	}
}


function importFile(id)
{
	if(confirm('Are you sure to import the file?')){
		$.post('{{url("forex/admin/forex/importer")}}',{id:id},function(data){
			console.log(data);
			if(data.success)
			{
				window.location.href = '{{url("forex/admin/forex/uploader")}}'; 
			}
			else
			{
				alert('There was some problem when importing the file'); 
			}
		},'JSON');
		
	}
	return false;
}

function deleteFile(id)
{
	if(confirm('Are you sure to delete the file?')){
		$.post('{{url("forex/admin/forex/delete_file")}}',{checked:[id]},function(data){
			if(data.success)
			{
				alert('Item has been removed');
				window.location.href = '{{url("forex/admin/forex/uploader")}}'; 
			}
			else
			{	
				alert('Sorry there was some error while deleting');
				// window.location.href = '{{url("forex/admin/forex/uploader")}}'; 
			}
			
		});
	}
	return false;
	
}

</script>

@endsection