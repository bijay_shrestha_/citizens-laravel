@extends('admin.layout.main')
@section('content')
<link rel="stylesheet" href="{{ asset('datetimepicker/css/jquery.datetimepicker.min.css') }}">

    <section class="content-header">
        <h1>
            Edit Forexes   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.forexes') }}">tbl_forex</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.forexes.update', $forex->ex_rate_id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                    <div class="form-group">
                        <label for="currency">Currency (*)</label><input type="text" value = "{{$forex->currency}}"  name="currency" id="currency" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="unit">Unit (*)</label><input type="text" value = "{{$forex->unit}}"  name="unit" id="unit" class="form-control" required>
                    </div>

                    
                    <div class="form-group">
                        <label for="ex_date">Exchange Rate and Time (*)</label><input type="text" value = "{{$forex->ex_date}}"  name="ex_date" id="ex_date" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="cash_buying">Cash Buying</label><input type="text" value = "{{$forex->cash_buying}}"  name="cash_buying" id="cash_buying" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="selling">Selling</label><input type="text" value = "{{$forex->selling}}"  name="selling" id="selling" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="buying">Buying</label><input type="text" value = "{{$forex->buying}}"  name="buying" id="buying" class="form-control" >
                    </div>
                                        
                    <div class="form-group">
                        <label for="sequence">Sequence</label><input type="number" value = "{{$forex->sequence}}"  name="sequence" id="sequence" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="status" id="status1" @if($forex->status == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="status" id="status0" @if($forex->status == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.forexes') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection

@section('custom_scripts')
<script type="text/javascript">
    // $(document).ready(function(){
    //     $('#remove-zone-option').attr('disabled',true);
    //     $('.district-select').multiSelect();
    // });
    // var globel = 1;
    $(document).ready(function(){
        $('#ex_date').datetimepicker();
    })
</script>
<script src="{{asset('datetimepicker/js/jquery.datetimepicker.js')}}"></script>

@endsection

