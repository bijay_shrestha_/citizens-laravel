<?php



Route::group(array('prefix'=>'admin/','module'=>'forex','middleware' => ['web','auth'], 'namespace' => 'App\modules\forex\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('forexes/','AdminForexController@index')->name('admin.forexes');
    Route::post('forexes/getforexesJson','AdminForexController@getforexesJson')->name('admin.forexes.getdatajson');
    Route::get('forexes/create','AdminForexController@create')->name('admin.forexes.create');
    Route::post('forexes/store','AdminForexController@store')->name('admin.forexes.store');
    Route::get('forexes/show/{id}','AdminForexController@show')->name('admin.forexes.show');
    Route::get('forexes/edit/{id}','AdminForexController@edit')->name('admin.forexes.edit');
    Route::match(['put', 'patch'], 'forexes/update/{id}','AdminForexController@update')->name('admin.forexes.update');
    Route::get('forexes/delete/{id}', 'AdminForexController@destroy')->name('admin.forexes.edit');
    Route::get('forexes/change/{text}/{status}/{id}', 'AdminForexController@change');
    Route::get('forexes/uploader', 'AdminForexController@uploader')->name('admin.forexes.uploader');
    Route::post('forexes/upload_file', 'AdminForexController@upload_file')->name('admin.forexes.upload_file');
    Route::get('forexes/delete_file/{id}', 'AdminForexController@delete_file')->name('admin.forexes.delete_file');

});




Route::group(array('module'=>'forex','namespace' => 'App\modules\forex\Controllers', 'middleware' => 'web'), function() {
    //Your routes belong to this module.
    Route::get('forexes/','ForexController@index')->name('forexes');
    
});