<?php

namespace App\modules\blog\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\blog\Model\Blog;
use App\modules\account\Controllers\AdminAccountController;
use Carbon\Carbon;


class AdminBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Blog';
        return view("blog::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getblogsJson(Request $request)
    {
        $blog = new Blog;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $blog->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('blog_id', "DSC")->get();

        // Display limited list        
        $rows = $blog->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->orderBy('blog_id', "DSC")->get();

        //To count the total values present
        $total = $blog->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Blog | Create';
        return view("blog::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $admin_account_controller = new AdminAccountController;
        $name_slug = $admin_account_controller->slugify($data['blog_title']);
        $slug_name = $admin_account_controller->validate_slug($name_slug);
        $slug_data = DB::table('slugs')->insert(['slug_name' => $slug_name]); //create slug
        $allslug =  DB::table('slugs')->get();
        $dataforblog = $data;
        unset($dataforblog['upload_image']);
        unset($dataforblog['tags']);

        $dataforblog['slug_id'] = $allslug[count($allslug)-1]->slug_id;
        $dataforblog['slug_name'] = $slug_name;
        $dataforblog['del_flag'] = 0;
        $dataforblog['added_by'] = Auth::user()->id;
        $dataforblog['sequence'] = ($dataforblog['sequence'] == null) ? "0":$dataforblog['sequence'];
        $dataforblog['added_date'] = new Carbon();
        $dataforblog = check_case($dataforblog, 'create');
        $success = Blog::Insert($dataforblog); //create Blog
        $blogs = Blog::all();
        $blog_id = $blogs[count($blogs)-1]['blog_id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "blog", 'file_id' => $blog_id]); //create activity log
        $slug['route'] = 'blog/detail/' . $blog_id;
        DB::table('slugs')->where('slug_id', $dataforblog['slug_id'])->update(['route' => $slug['route']]);

        $tags = explode(',', $data['tags']);
        foreach($tags as $tag)
        {       
            DB::table('tbl_tags')->insert(['section' => "blog", 'section_id' => $blog_id, 'tag_title' => $tag]);
        }
        return redirect()->route('admin.blogs')->with('success', "Data inserted successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::where('blog_id',$id)->firstOrFail();
        $tags = DB::table('tbl_tags')->where([['section_id',$id], ['section', 'blog']])->get();
        $blog_tags = '';
        if ($tags) {
            foreach($tags as $tag) {
                $blog_tag[] = $tag->tag_title; 
            }
            $blog_tags = implode(',', $blog_tag);
        }
        $page['title'] = 'Blog | Update';
        return view("blog::edit",compact('page','blog', 'blog_tags'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        //dd($data);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "blog", 'file_id' => $id]); //create activity log
        $blog = Blog::where('blog_id', $id)->firstOrFail();
        $admin_account_controller = new AdminAccountController;
        $name_slug = $admin_account_controller->slugify($data['blog_title']);
        $slug['slug_name'] = $admin_account_controller->validate_slug($name_slug, $blog->slug_id);
        $slug['route'] = 'blog/detail/'.$id;         //$data['loan_id'] = $id
        DB::table('slugs')->where('slug_id', $blog->slug_id)->update(['slug_name' =>  $slug['slug_name'], 'route' => $slug['route']]);                
        $dataforblog = $data;
        unset($dataforblog['upload_image']);
        unset($dataforblog['tags']);

        $dataforblog['slug_name'] = $slug['slug_name'];
        
        if (array_key_exists('delete_old_image', $data)) {
            if ((file_exists(public_path().'/uploads/blog/'. $blog->image_name))  && $blog->image_name) {
                unlink(public_path().'/uploads/blog/'. $blog->image_name);
                if (file_exists(public_path().'/uploads/blog/thumb/'. $blog->image_name)) {
                    unlink(public_path().'/uploads/blog/thumb/'. $blog->image_name);
                } 
            }
            if (!$data['image_name']) {
                $data['image_name'] = '';
                $dataforblog['image_name'] = '';
            }
            unset($dataforblog['delete_old_image']);
        } else {
            if (!$data['image_name']) {
                $data['image_name'] = $blog->image_name;
                $dataforblog['image_name'] = $blog->image_name;
            }
        }
        $dataforblog['modified_date'] = new Carbon();
        $dataforblog = check_case($dataforblog, 'edit');
        DB::table('tbl_blogs')->where('blog_id', $id)->update($dataforblog);

        $tags_data = DB::table('tbl_tags')->where([['section_id',$id], ['section', 'blog']])->delete();
        if (array_key_exists('tags', $data)) {
            $tags = explode(',', $data['tags']);
            foreach($tags as $tag)
            {       
                DB::table('tbl_tags')->insert(['section' => "blog", 'section_id' => $id, 'tag_title' => $tag]);
            }
            
        }        
        //for tags table        

        return redirect()->route('admin.blogs')->with('success', "Data updated successfully.");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_blogs', 'blog_id', $id, 'deleted_by', 'deleted_date');
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "blog", 'file_id' => $id]); //create acrivity log
        return redirect()->route('admin.blogs')->with('success', "Data deleted successfully.");
        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_blogs', 'blog_id', 'deleted_by', 'deleted_date');
        return redirect()->route('admin.blogs')->with('success', "Data updated successfully.");
    }

}
