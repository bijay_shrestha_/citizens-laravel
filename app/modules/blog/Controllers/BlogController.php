<?php

namespace App\modules\blog\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\modules\blog\Model\Blog;
use Illuminate\Support\Facades\Schema;


class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $page['title'] = 'Blog';
        // $blogs = DB::table('tbl_blogs')->where([['approvel_status', "ok"],['approved', "approved"]])
        //                                     ->orWhere([['approvel_status', "delete"], ['approved', "denied"]])
        //                                     ->where('del_flag', 0)->where('status',1)->get();
        // $news=DB::table('tbl_blogs')->where([['approvel_status', "ok"],['approved', "approved"]])
        //                                     ->orWhere([['approvel_status', "delete"], ['approved', "denied"]])
        //                                     ->where('del_flag', 0)->where('status',1)
        //                                     ->orderBy('blog_id', 'DSC')
        //                                     ->limit(4)->get();  
        $news = get_lists('tbl_blogs', null, 'blog_id', 'DSC', 4);
        $tags = DB::table('tbl_tags')
             ->select('tag_title', DB::raw('count(*) as total'))
             ->groupBy('tag_title')
             ->get();
        $banners=DB::table('tbl_banners')->where('banner_collection_id',11)->get();
        $header="BLog";
        $view_page="blog/news";
        $blogs= get_lists('tbl_blogs');
        return view('front.blog.news',compact('news','tags','banners','header','view_page','blogs')); 
    }
    function detail($slug_name)
{       
        $tags = DB::table('tbl_tags')->where('section',"blog")->get();
        $blogs= DB::table('tbl_blogs')->where('slug_name', $slug_name)->get();
        $banners=DB::table('tbl_banners')->where('banner_collection_id',11)->get();
        $news = Blog::where([['status', 1], ['blog_id', '!=', $blogs[0]->blog_id]])->orderBy('blog_id','desc')->get();
        $header="BLog";
        $view_page="blog/detail";
      
        return view('front.blog.detail',compact('news','tags','banners','header','view_page','blogs')); 
    }  
    function tagFilter()
    {

      $title = 'Blog';
    //   $this->blog_model->joins = array('TAGS');
    //   $data['blogs'] = $this->blog_model->getBlogs(array('tags.tag_title'=>$title,'tags.section'=>'blog','blogs.status'=>1,'blogs.del_flag'=>0))->result_array();
    //   $this->load->view('blog/content',$data);
    $blogs=DB::table('tbl_blogs')
    ->leftjoin('tbl_tags', 'tbl_blogs.blog_id', '=', 'tbl_tags.section_id' )
    ->select('tbl_blogs.*', 'tbl_tags.*')
    ->where([['tbl_tags.tag_title',$title],['tbl_tags.section',"blog"],['tbl_blogs.status',1],['tbl_blogs.del_flag',0]])
    ->get()->toArray();// baki to join table
    return $blogs;
     }  
}
