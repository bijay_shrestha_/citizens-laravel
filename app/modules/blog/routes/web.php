<?php



Route::group(array('prefix'=>'admin/','module'=>'blog','middleware' => ['web','auth'], 'namespace' => 'App\modules\blog\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('blogs/','AdminBlogController@index')->name('admin.blogs');
    Route::post('blogs/getblogsJson','AdminBlogController@getblogsJson')->name('admin.blogs.getdatajson');
    Route::get('blogs/create','AdminBlogController@create')->name('admin.blogs.create');
    Route::post('blogs/store','AdminBlogController@store')->name('admin.blogs.store');
    Route::get('blogs/show/{id}','AdminBlogController@show')->name('admin.blogs.show');
    Route::get('blogs/edit/{id}','AdminBlogController@edit')->name('admin.blogs.edit');
    Route::match(['put', 'patch'], 'blogs/update/{id}','AdminBlogController@update')->name('admin.blogs.update');
    Route::get('blogs/delete/{id}', 'AdminBlogController@destroy')->name('admin.blogs.edit');
    Route::get('blogs/change/{text}/{status}/{id}', 'AdminBlogController@change');

});





Route::group(array('module'=>'blog','namespace' => 'App\modules\blog\Controllers','middleware' => 'web'), function() {
    //Your routes belong to this module.
    Route::get('blogs/','BlogController@index')->name('blogs');
    Route::get('blog/{slug_name}','BlogController@detail')->name('details');
    Route::get('tagFilter/','BlogController@tagFilter')->name('tagFilters');
});