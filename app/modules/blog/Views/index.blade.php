@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Blogs		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Blogs</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.blogs.create') }}" class="btn bg-green waves-effect"  title="create">Create</a>
					</div>
					<br>
					@if ($message = Session::get('success'))
					<div class="btn btn-success">
						{{$message}}
					</div>
					@endif
					<!-- /.box-header -->
					<div class="box-body">
						<table id="blog-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								<th >Blog Title</th>
								<th >Image </th>
								<th >Added Date</th>
								<th >Post Date</th>
								<th >Approvel Status</th>
								<th >Approved</th>
								<th >Status</th>
								<th >Sequence</th>
								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#blog-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.blogs.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
				    { data: "blog_title",name: "blog_title"},
		            { data: function (data, b,c, table){
		            	var imagefile = '';
		            	if (data.image_name) {
		            		imagefile += '<img src="{{url("/uploads/blog/thumb/")}}/'+data.image_name+'">';
		            	}
		            	return imagefile;

		            }, name:'image_name', searchable: false},
		            { data: "added_date",name: "added_date"},
		            { data: "post_date",name: "post_date"},
		            { data: "approvel_status",name: "approvel_status"},
		           { data: function (data)  {
		           	var content = "";

		           	content += "<a href='{{url('/admin/blogs/change/')}}/approved/" + data.approvel_status + "/" + data.blog_id + "'style='color:";
		           	if (data.approved == "approved") { content += "green"; }
		           	content += "'>Approved</a><br>";
		           	
		           	content += "<a href='{{url('/admin/blogs/change/')}}/pending/" + data.approvel_status + "/" + data.blog_id + "'style='color:";
		           	if (data.approved == "pending") { content += "green"; }
		           	content += "'>Pending</a><br>";

		           	content += "<a href='{{url('/admin/blogs/change/')}}/denied/" + data.approvel_status + "/" + data.blog_id + "'style='color:";
		           	if (data.approved == "denied") { content += "green"; }
		           	content += "'>Denied</a><br>";
		           	
		           	return content;
		           },name:'approved', searchable: false},
		          { data: function(data){
		          	if (data.status == 1) {
		          		return "<img src='{{url('/images/logo/accept.png')}}'>";
		          	} else {
		          		return "<img src='{{url('/images/logo/cancel.png')}}'>";
		          	}
		          	;
		          }, name:'status', searchable: false},
		            { data: "sequence",name: "sequence"},
		                   
		            { data: function(data,b,c,table) { 
		            var buttons = '';

		            buttons += "<a class='btn bg-green waves-effect' href='"+site_url+"/edit/"+data.blog_id+"' type='button' ><i class='fa fa-edit'></i> Edit</a>&nbsp"; 

		            buttons += "<a href='"+site_url+"/delete/"+data.blog_id+"' class='btn bg-red waves-effect' ><i class='fa fa-trash'></i>  Delete</a>";

		            return buttons;
		            }, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
