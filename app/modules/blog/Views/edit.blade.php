@extends('admin.layout.main')
@section('content')
<style type="text/css">
  div.tagsinput { border:1px solid #CCC; background: #FFF; padding:5px; width:300px; height:100px; overflow-y: auto;}
div.tagsinput span.tag { border: 1px solid #a5d24a; -moz-border-radius:2px; -webkit-border-radius:2px; display: block; float: left; padding: 5px; text-decoration:none; background: #cde69c; color: #638421; margin-right: 5px; margin-bottom:5px;font-family: helvetica;  font-size:13px;}
div.tagsinput span.tag a { font-weight: bold; color: #82ad2b; text-decoration:none; font-size: 11px;  }
div.tagsinput input { width:80px; margin:0px; font-family: helvetica; font-size: 13px; border:1px solid transparent; padding:5px; background: transparent; color: #000; outline:0px;  margin-right:5px; margin-bottom:5px; }
div.tagsinput div { display:block; float: left; }
.tags_clear { clear: both; width: 100%; height: 0px; }
.not_valid { background: #FBD8DB !important; color: #90111A !important;}
</style>

    <section class="content-header">
        <h1>
            Edit Blogs   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.blogs') }}">blogs</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.blogs.update', $blog->blog_id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                
                <div class="form-group">
                    <label for="blog_title">Blog Title</label><input type="text" value = "{{$blog->blog_title}}"  name="blog_title" id="blog_title" class="form-control" >
                </div>

                <div class="form-group">
                    <label for="tags">Tags</label><input type="text" value = "{{$blog_tags}}"  name="tags" id="tags" class="form-control" >
                </div>

                <div class="form-group">
                    <label for="image_name">Image </label><br>
                    <label id="upload_image_name" style="display:none"></label>
                     <div id="delete_old_image_requested"></div>
                    <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                    <div id="image-status"></div>
                    <input type="file" id="upload_image" name="upload_image" @if (file_exists(public_path().'/uploads/blog/thumb/'. $blog->image_name) && $blog->image_name) style="display:none" @else style="display:block" @endif />
                    <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                    <div id="imagedata">
                    @if (file_exists(public_path().'/uploads/blog/'. $blog->image_name) && $blog->image_name)                                   
                        <img src="{{url('uploads/blog/thumb/'.$blog->image_name)}}">
                        <a href="javascript:void(0)" id="change-image_option" title="Change Image">&nbsp; &nbsp; <img src="{{('/images/logo/cancel.png')}}" border="0"/> Change the old image</a>
                    @endif                                
                    </div>
                </div>


                <div class="form-group">
                    <label for="contents">Contents</label><textarea name="contents" id="contents" class="form-control  my-editor" >{!! $blog->contents !!}</textarea>
                </div>

                <div class="form-group">
                    <label for="post_date">Post Date</label><input type="date" value = "{{$blog->post_date}}"  name="post_date" id="post_date" class="form-control" >
                </div>


                <div class="form-group">
                    <label for="status">Status</label>
                    <div class="radio">
                    <label>
                        <input type="radio" value="1" name="status" id="status1" @if($blog->status == 1) checked @endif  />Yes
                    </label> 
                    <label>
                        <input type="radio" value="0" name="status" id="status0" @if($blog->status == 1) @else  checked @endif  />No
                    </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="sequence">Sequence</label><input type="number" value = "{{$blog->sequence}}"  name="sequence" id="sequence" class="form-control" >
                </div>

                <input type="hidden" name="blog_id" id="blog_id" value = "{{$blog->blog_id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" id="btn-submit"  class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.blogs') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
@section('custom_scripts')
<script src="{{asset('js/jquery.tagsinput.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function() {
  /*  $('#post_date').datepicker({
       format: 'yyyy-mm-dd',
      pickTime: false
    });*/
    $('#tags').tagsInput({width:'100%'});

   
  }); 
</script>
<script type="text/javascript">
    var directory = 'blog';
    $('#btn-submit').on('click',function(){
      var filename = $('#image_name').val();

      var name        = $('#blog_title').val();
//      var image     = $('#upload_image').val();
      var tags     = $('#tags').val();
      var post_date     = $('#post_date').val();
       var content     = $('#contents').val();
      
      if(name =='')
      {   
          
         alert('Please Enter Blog title');
          return false;
      }

      if(tags =='')
      {
          alert('Please Enter tags for Blog');
         return false;
      }
      if(post_date =='')
      {
          alert('Please Enter date to posted');
         return false;
      }
      if(content =='')
      {
          alert('Please write content for blog');
         return false;
      }
     });
</script>
@include('data_changing_script_edit')
@include('file_changing_script')
@endsection
