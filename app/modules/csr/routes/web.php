<?php



Route::group(array('prefix'=>'admin/','module'=>'csr','middleware' => ['web','auth'], 'namespace' => 'App\modules\csr\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('csrs/','AdminCsrController@index')->name('admin.csrs');
    Route::post('csrs/getcsrsJson','AdminCsrController@getcsrsJson')->name('admin.csrs.getdatajson');
    Route::get('csrs/create','AdminCsrController@create')->name('admin.csrs.create');
    Route::post('csrs/store','AdminCsrController@store')->name('admin.csrs.store');
    Route::get('csrs/show/{id}','AdminCsrController@show')->name('admin.csrs.show');
    Route::get('csrs/edit/{id}','AdminCsrController@edit')->name('admin.csrs.edit');
    Route::match(['put', 'patch'], 'csrs/update/{id}','AdminCsrController@update')->name('admin.csrs.update');
    Route::get('csrs/delete/{id}', 'AdminCsrController@destroy')->name('admin.csrs.edit');
    Route::get('csrs/change/{text}/{status}/{id}', 'AdminCsrController@change');
});




Route::group(array('module'=>'Csr','namespace' => 'App\modules\csr\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('csrs/','CsrController@index')->name('csrs');
    
});