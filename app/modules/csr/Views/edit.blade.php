@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Csrs   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.csrs') }}">tbl_csr</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.csrs.update', $csr->id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                    <label for="title">Title</label><input type="text" value = "{{$csr->title}}"  name="title" id="title" class="form-control" >
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea name="description" id="description" class="form-control my-editor" >{!! $csr->description !!}</textarea>
                </div>
                   <!-- in this (csr) case image field in db table is 'image' not 'image_name'  -->
                <div class="form-group">
                    <label for="image_name">Image </label><br>
                    <label id="upload_image_name" style="display:none"></label>
                     <div id="delete_old_image_requested"></div>
                    <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                    <div id="image-status"></div>
                    <input type="file" id="upload_image" name="upload_image" @if (file_exists(public_path().'/uploads/csr/thumb/'. $csr->image) && $csr->image) style="display:none" @else style="display:block" @endif />
                    <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                    <div id="imagedata">
                    @if (file_exists(public_path().'/uploads/csr/'. $csr->image) && $csr->image)                                   
                        <img src="{{url('uploads/csr/thumb/'.$csr->image)}}">
                        <a href="javascript:void(0)" id="change-image_option" title="Change Image">&nbsp; &nbsp; <img src="{{('/images/logo/cancel.png')}}" border="0"/> Change the old image</a>
                    @endif                                
                    </div>
                </div>

                <div class="form-group">
                    <label for="date">Date</label><input type="date" value = "{{$csr->date}}"  name="date" id="date" class="form-control" >
                </div>

                <div class="form-group">
                    <label for="status">Status</label>
                    <div class="radio">
                    <label>
                        <input type="radio" value="1" name="status" id="status1" @if($csr->status == 1) checked @endif  />Yes
                    </label> 
                    <label>
                        <input type="radio" value="0" name="status" id="status0" @if($csr->status == 1) @else  checked @endif  />No
                    </label>
                    </div>
                </div>

                <input type="hidden" name="id" id="id" value = "{{$csr->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" id="btn-submit"  class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.csrs') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection

@section('custom_scripts')
<script type="text/javascript">
    var directory = 'csr';
    $('#btn-submit').on('click',function(){
      var filename = $('#image_name').val();

      var name        = $('#title').val();
      var image     = $('#upload_image').val();
      var post_date     = $('#date').val();
      
      if(name =='')
      {   
          
         alert('Please Enter CSR title');
          return false;
      }

/*      if (filename == "") {
       alert('Please Upload Image');
       return false;
      }*/
     });
</script>

    @include('data_changing_script_edit')
    @include('file_changing_script')

@endsection

