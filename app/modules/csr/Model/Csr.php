<?php

namespace App\Modules\Csr\Model;


use Illuminate\Database\Eloquent\Model;

class Csr extends Model
{
    public  $table = 'tbl_csr';

    protected $fillable = ['id','title','description','image','date','approvel_status','approved','status','added_by','added_date','modified_by','modified_date','del_flag',];
}
