<?php

namespace App\modules\csr\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\csr\Model\Csr;
use Carbon\Carbon;

class AdminCsrController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Csr';
        return view("csr::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getcsrsJson(Request $request)
    {
        $csr = new Csr;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $csr->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->get();

        // Display limited list        
        $rows = $csr->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->get();

        //To count the total values present
        $total = $csr->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Csr | Create';
        return view("csr::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        //dd($data);
        //$success = Csr::Create($data);
        $dataforcsr = $data;
        if ($dataforcsr['date']) {
            $dataforcsr['date'] = date('Y-m-d H:i:s',strtotime($data['date']));
        }
        unset($dataforcsr['upload_image']);
        $dataforcsr['del_flag'] = 0;
        $dataforcsr['added_by'] = Auth::user()->id;
        $dataforcsr['added_date'] = new Carbon();
        $dataforcsr['image'] = $dataforcsr['image_name'];
        unset($dataforcsr['image_name']);
        $dataforcsr = check_case($dataforcsr, 'create');
        $success = Csr::Insert($dataforcsr);
        $csrs = Csr::all();
        $csr_id = $csrs[count($csrs)-1]['id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "csr", 'file_id' => $csr_id]); //create acrivity log     

        return redirect()->route('admin.csrs')->with('success', "Data inserted successfully");

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $csr = Csr::findOrFail($id);
        $page['title'] = 'Csr | Update';
        return view("csr::edit",compact('page','csr'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "csr", 'file_id' => $id]); //create activity log
        $csr = Csr::where('id', $id)->firstOrFail();
        $dataforcsr = $data;
        unset($dataforcsr['upload_image']);
        
        if (array_key_exists('delete_old_image', $data)) {
            if ((file_exists(public_path().'/uploads/csr/'. $csr->image))  && $csr->image) {
                unlink(public_path().'/uploads/csr/'. $csr->image);
                if (file_exists(public_path().'/uploads/csr/thumb/'. $csr->image)) {
                    unlink(public_path().'/uploads/csr/thumb/'. $csr->image);
                } 
            }
            if (!$data['image_name']) {
                $data['image_name'] = '';
                $dataforcsr['image_name'] = '';
            }
            unset($dataforcsr['delete_old_image']);
        } else {
            if (!$data['image_name']) {
                $data['image_name'] = $csr->image;
                $dataforcsr['image_name'] = $csr->image;
            }
        }
        $dataforcsr['image'] = $dataforcsr['image_name'];
        $dataforcsr['modified_by'] = Auth::user()->id;
        $dataforcsr['modified_date'] = new Carbon();
        unset($dataforcsr['image_name']);
        $dataforcsr = check_case($dataforcsr, 'edit');
        $success = DB::table('tbl_csr')->where('id', $id)->update($dataforcsr);
        return redirect()->route('admin.csrs')->with('success', "Data updated Successfully.");
        

        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_csr', 'id', $id);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "csr", 'file_id' => $id]); //create activity log
        return redirect()->route('admin.csrs')->with('success', "Data deleted Successfully.");
        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_csr', 'id');
        return redirect()->route('admin.csrs')->with('success', "Data updated Successfully.");
    }


}
