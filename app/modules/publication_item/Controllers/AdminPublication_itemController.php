<?php

namespace App\modules\publication_item\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\publication_item\Model\Publication_item;
use App\modules\publication_category\Model\Publication_category;
use Carbon\Carbon;

class AdminPublication_itemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Publication Item';
        return view("publication_item::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getpublication_itemsJson(Request $request)
    {
       // $publication_item = DB::table('tbl_publication_items');
        $publication_item = new Publication_item;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $publication_item->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )
        ->where('del_flag',0)->orderBy('item_id', "DSC")->get();
        //->leftjoin('tbl_publication_categories', 'tbl_publication_items.category_id', '=', 'tbl_publication_categories.category_id')
        //->where('tbl_publication_items.del_flag',0)->select('tbl_publication_items.*', 'tbl_publication_categories.category_name')->orderBy('tbl_publication_items.item_id', "DSC")->get();


        // Display limited list        
        $rows = $publication_item->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)
        //->where('tbl_publication_items.del_flag',0)->orderBy('tbl_publication_items.item_id', "DSC")->get();
        ->where('del_flag',0)->orderBy('item_id', "DSC")->get();

        //To count the total values present
        //$total = $publication_item->where('tbl_publication_items.del_flag',0)->get();
        $total = $publication_item->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Publication Item | Create';
        $categories = Publication_category::all();
        return view("publication_item::add",compact('page', 'categories'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $datafor_publication = $data;
        $datafor_publication['del_flag'] = 0;
        $datafor_publication['show_homepage'] = 0; 
        $datafor_publication['sequence'] = ($datafor_publication['sequence'] == null) ? "0":$datafor_publication['sequence'];
        $datafor_publication['added_by'] = Auth::user()->id;
        $datafor_publication['added_date'] = new Carbon();
        $datafor_publication['file_name'] = $datafor_publication['download_form_link'];
        unset($datafor_publication['download_form_link']);
        unset($datafor_publication['download_link']);

        $datafor_publication = check_case($datafor_publication, 'create');
        $success = Publication_item::Insert($datafor_publication);
        $publication_items = Publication_item::all();
        $publication_id = $publication_items[count($publication_items)-1]['item_id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "PUBLICATION_ITEMS", 'file_id' => $publication_id]);
        return redirect()->route('admin.publication_items')->with('success', "Data added successfully");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $publication_item = Publication_item::where('item_id',$id)->firstOrFail();
        $page['title'] = 'Publication Item | Update';
        $categories = Publication_category::all();
        return view("publication_item::edit",compact('page','publication_item', 'categories'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $publication_item = Publication_item::where('item_id',$id)->firstOrFail();
        

        $datafor_publication = $data;

        /*if ($datafor_publication['ex_date']) {
            $datafor_publication['date'] = date('Y-m-d H:i:s',strtotime($data['date']));
        }*/
        unset($datafor_publication['download_link']);
        $datafor_publication['sequence'] = ($datafor_publication['sequence'] == null) ? "0":$datafor_publication['sequence'];

        if (array_key_exists('delete_old_file', $data)) {
            if ((file_exists(public_path().'/uploads/publication/form/'. $publication_item->file_name)) && $publication_item->file_name) {
                unlink(public_path().'/uploads/publication/form/'. $publication_item->file_name);                
            }
            if (!$data['download_form_link']) {
                $data['download_form_link'] = '';
                $datafor_publication['download_form_link'] = '';
            }
            unset($datafor_publication['delete_old_file']);
        } else {
            if (!$data['download_link']) {
                $data['download_form_link'] = $publication_item->file_name;
                $datafor_publication['download_form_link'] = $publication_item->file_name;
            }            
        }
        $datafor_publication['file_name'] = $datafor_publication['download_form_link'];
        unset($datafor_publication['download_form_link']);
        $datafor_publication['modified_date'] = new Carbon();
        $datafor_publication = check_case($datafor_publication, 'edit');
        DB::table('tbl_publication_items')->where('item_id', $id)->update($datafor_publication);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "PUBLICATION_ITEMS", 'file_id' => $id]);

        return redirect()->route('admin.publication_items')->with('success', "Data added successfully");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_publication_items', 'item_id', $id, 'deleted_by', 'deleted_date');
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "PUBLICATION_ITEMS", 'file_id' => $id]); //create acrivity log             
        return redirect()->route('admin.publication_items')->with('success', "Data deleted successfully.");
        //
    }


    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_publication_items', 'item_id', 'deleted_by', 'deleted_date');
        return redirect()->route('admin.publication_items')->with('success', "Data updated successfully.");
    }
}
