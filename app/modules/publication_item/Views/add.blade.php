@extends('admin.layout.main')
@section('content')
<style type="text/css">
    
</style>
    <section class="content-header">
        <h1>
            Add Publication Items   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.publication_items') }}">Publication Items</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.publication_items.store') }}"  method="post">
                <div class="box-body">                
                        <div class="form-group">
                            <label for="category_id">Category</label>
                            <select name="category_id" class="form-control" required>
                                <option value="">Select Category </option> 
                                @foreach ($categories as $category)
                                    <option value="{{$category['category_id']}}"
                                      @if (old('category_id') == $category['category_id']) selected @endif
                                    >{{$category['category_name']}}</option>
                                @endforeach

                            </select>
                        </div>

                        <div class="form-group">
                            <label for="item_name">Item Name (*)</label><input type="text" name="item_name" id="item_name" class="form-control" required>
                        </div>

                        <div class="form-group">
                                  <label>File Name</label><br/><label id="upload_download_link" style="display:none"></label>
                                  <!-- <input type="text" name="download_form_link" id="download_form_link"  value="" class="form-control"> -->
                                  <input type="file" name="download_link" id="download_link" >
                                  <div id="file-status"></div>
                                  <input type="hidden" name="download_form_link" id="download_form_link"/>
                                  <a href="javascript:void(0)" id="change-file" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>

                        </div>
                        
                        <div class="form-group">
                            <label for="start_date">Start Date &nbsp; &nbsp;</label> <input type="date" name="start_date" id="start_date" class="" >
                        </div>

                        <div class="form-group">
                            <label for="end_date">End Date &nbsp; &nbsp;</label> <input type="date" name="end_date" id="end_date" class="" >
                        </div>
                        
                        <div class="form-group">
                            <label for="contain">Contain</label><textarea name="contain" id="contain" class="form-control my-editor" ></textarea>
                        </div>

                        <div class="form-group">
                            <label for="sequence">Sequence</label><input type="number" name="sequence" id="sequence" class="form-control" >
                        </div>

                            <div class="form-group">
                                <label for="status">Status</label>
                                <div class="radio">
                                <label>
                                    <input type="radio" value="1" name="status" id="status1" @if(old('status') == 1) checked @endif  />Yes
                                </label> 
                                <label>
                                    <input type="radio" value="0" name="status" id="status0" @if(old('status') == 1) @else  checked @endif  />No
                                </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="status">Allow Download</label>
                                <div class="radio">
                                <label>
                                    <input type="radio" value="1" name="allow_download" id="allow_download1" @if(old('allow_download') == 1) checked @endif  />Yes
                                </label> 
                                <label>
                                    <input type="radio" value="0" name="allow_download" id="allow_download0" @if(old('allow_download') == 1) @else  checked @endif  />No
                                </label>
                                </div>
                            </div>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.publication_items') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
    <script type="text/javascript">
          var directory = 'publication';
    </script>
    @include('data_changing_script_create')
    @include('file_changing_script')

@endsection
