@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Publication Items		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Publication Items</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.publication_items.create') }}" class="btn bg-green waves-effect"  title="create">Create</a>
					</div>
					<!-- /.box-header -->
					<br>
					@if ($message = Session::get('success'))
					<div class="btn btn-success">
						{{$message}}
					</div>
					@endif

					<div class="box-body">
						<table id="publication_item-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								<th >Item Name</th>
								<th >File </th>
								<th >Sequence</th>
								<th >Approvel Status</th>
								<th >Approved</th>
								<th >Status</th>
								<th >Allow Download</th>
								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		var config_url = "{{url('/')}}";
		$(function(){
			dataTable = $('#publication_item-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.publication_items.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
		            { data: "item_name",name: "item_name"},
		           { data: function(data){
		           	if (data.file_name) {
		           		return "<a target='_blank' href='" + config_url + "/uploads/publication/form/" + data.file_name+"'> " +data.file_name +" </a> ";
		           	} else {
		           		return "";
		           	}
		           	;
		           }, name:'file_name', searchable: false},
		           
		            { data: "sequence",name: "sequence"},
		            { data: "approvel_status",name: "approvel_status"},
		           { data: function (data)  {
		           	var content = "";

		           	content += "<a href='{{url('/admin/publication_items/change/')}}/approved/" + data.approvel_status + "/" + data.item_id + "'style='color:";
		           	if (data.approved == "approved") { content += "green"; }
		           	content += "'>Approved</a><br>";
		           	
		           	content += "<a href='{{url('/admin/publication_items/change/')}}/pending/" + data.approvel_status + "/" + data.item_id + "'style='color:";
		           	if (data.approved == "pending") { content += "green"; }
		           	content += "'>Pending</a><br>";

		           	content += "<a href='{{url('/admin/publication_items/change/')}}/denied/" + data.approvel_status + "/" + data.item_id + "'style='color:";
		           	if (data.approved == "denied") { content += "green"; }
		           	content += "'>Denied</a><br>";
		           	
		           	return content;
		           },name:'approved', searchable: false},
		           { data: function(data){
		           	if (data.status == 1) {
		           		return "<img src='{{url('/images/logo/accept.png')}}'>";
		           	} else {
		           		return "<img src='{{url('/images/logo/cancel.png')}}'>";
		           	}
		           	;
		           }, name:'status', searchable: false},
		           { data: function(data){
		           	if (data.allow_download == 1) {
		           		return "<img src='{{url('/images/logo/accept.png')}}'>";
		           	} else {
		           		return "<img src='{{url('/images/logo/cancel.png')}}'>";
		           	}
		           	;
		           }, name:'allow_download', searchable: false},
		            
		           { data: function(data,b,c,table) { 
		           var buttons = '';

		           buttons += "<a class='btn bg-green waves-effect' href='"+site_url+"/edit/"+data.item_id+"' type='button' ><i class='fa fa-view'></i> Edit</a>&nbsp"; 

		           buttons += "<a href='"+site_url+"/delete/"+data.item_id+"' class='btn bg-red waves-effect' ><i class='fa fa-trash'></i>  Delete</a>";

		           return buttons;
		           }, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
