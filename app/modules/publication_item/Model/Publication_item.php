<?php

namespace App\Modules\Publication_item\Model;


use Illuminate\Database\Eloquent\Model;

class Publication_item extends Model
{
    public  $table = 'tbl_publication_items';

    protected $fillable = ['item_id','category_id','item_name','file_name','start_date','end_date','contain','show_homepage','sequence','approvel_status','approved','added_date','added_by','modified_date','deleted_date','deleted_by','del_flag','status','allow_download',];
}
