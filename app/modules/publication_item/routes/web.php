<?php



Route::group(array('prefix'=>'admin/','module'=>'publication_item','middleware' => ['web','auth'], 'namespace' => 'App\modules\publication_item\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('publication_items/','AdminPublication_itemController@index')->name('admin.publication_items');
    Route::post('publication_items/getpublication_itemsJson','AdminPublication_itemController@getpublication_itemsJson')->name('admin.publication_items.getdatajson');
    Route::get('publication_items/create','AdminPublication_itemController@create')->name('admin.publication_items.create');
    Route::post('publication_items/store','AdminPublication_itemController@store')->name('admin.publication_items.store');
    Route::get('publication_items/show/{id}','AdminPublication_itemController@show')->name('admin.publication_items.show');
    Route::get('publication_items/edit/{id}','AdminPublication_itemController@edit')->name('admin.publication_items.edit');
    Route::match(['put', 'patch'], 'publication_items/update/{id}','AdminPublication_itemController@update')->name('admin.publication_items.update');
    Route::get('publication_items/delete/{id}', 'AdminPublication_itemController@destroy')->name('admin.publication_items.edit');
    Route::get('publication_items/change/{text}/{status}/{id}', 'AdminPublication_itemController@change');
});




Route::group(array('module'=>'publication_item','namespace' => 'App\modules\publication_item\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('publication_items/','Publication_itemController@index')->name('publication_items');
    
});