<?php



Route::group(array('prefix'=>'admin/','module'=>'Extra_course','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Extra_course\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('extra_courses/','AdminExtra_courseController@index')->name('admin.extra_courses');
    Route::post('extra_courses/getextra_coursesJson','AdminExtra_courseController@getextra_coursesJson')->name('admin.extra_courses.getdatajson');
    Route::get('extra_courses/create','AdminExtra_courseController@create')->name('admin.extra_courses.create');
    Route::post('extra_courses/store','AdminExtra_courseController@store')->name('admin.extra_courses.store');
    Route::get('extra_courses/show/{id}','AdminExtra_courseController@show')->name('admin.extra_courses.show');
    Route::get('extra_courses/edit/{id}','AdminExtra_courseController@edit')->name('admin.extra_courses.edit');
    Route::match(['put', 'patch'], 'extra_courses/update/{id}','AdminExtra_courseController@update')->name('admin.extra_courses.update');
    Route::get('extra_courses/delete/{id}', 'AdminExtra_courseController@destroy')->name('admin.extra_courses.edit');
});




Route::group(array('module'=>'Extra_course','namespace' => 'App\Modules\Extra_course\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('extra_courses/','Extra_courseController@index')->name('extra_courses');
    
});