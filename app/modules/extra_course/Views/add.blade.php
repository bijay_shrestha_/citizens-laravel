@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Extra_courses   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.extra_courses') }}">tbl_extra_course</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.extra_courses.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                                    <label for="p_id">P_id</label><input type="text" name="p_id" id="p_id" class="form-control" ></div><div class="form-group">
                                    <label for="course">Course</label><input type="text" name="course" id="course" class="form-control" ></div><div class="form-group">
                                    <label for="institude">Institude</label><input type="text" name="institude" id="institude" class="form-control" ></div>
<input type="hidden" name="id" id="id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.extra_courses') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
