<?php

namespace App\Modules\Extra_course\Model;


use Illuminate\Database\Eloquent\Model;

class Extra_course extends Model
{
    public  $table = 'tbl_extra_course';

    protected $fillable = ['id','p_id','course','institude',];
}
