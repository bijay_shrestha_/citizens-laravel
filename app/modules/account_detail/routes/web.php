<?php



Route::group(array('prefix'=>'admin/','module'=>'account_detail','middleware' => ['web','auth'], 'namespace' => 'App\modules\account_detail\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('account_details/index/{status?}','AdminAccount_detailController@index')->name('admin.account_details');
    Route::post('account_details/getaccount_detailsJson','AdminAccount_detailController@getaccount_detailsJson')->name('admin.account_details.getdatajson');
    Route::get('account_details/create','AdminAccount_detailController@create')->name('admin.account_details.create');
    Route::post('account_details/store','AdminAccount_detailController@store')->name('admin.account_details.store');
    Route::get('account_details/show/{id}','AdminAccount_detailController@show')->name('admin.account_details.show');
    Route::get('account_details/edit/{id}','AdminAccount_detailController@edit')->name('admin.account_details.edit');
    Route::match(['put', 'patch'], 'account_details/update/{id}','AdminAccount_detailController@update')->name('admin.account_details.update');
    Route::get('account_details/delete/{id}', 'AdminAccount_detailController@destroy')->name('admin.account_details.edit');
    Route::get('account_details/detail/{id}','AdminAccount_detailController@detail')->name('admin.account_details.detail');
    Route::post('account_details/change_status','AdminAccount_detailController@change_status')->name('admin.account_details.change_status');
    Route::get('account_details/form_view/{id}','AdminAccount_detailController@form_view')->name('admin.account_details.form_view');
    Route::get('account_details/kyc/{id}','AdminAccount_detailController@kyc')->name('admin.account_details.form_view');
    Route::get('account_details/export_lists', 'AdminAccount_detailController@exportAccountDetails');
    Route::get('account_details/account_details_lists', 'AdminAccount_detailController@get_account_details')->name('admin.account_detail');
    Route::get('account_details/account_individual_detail_list/{id}', 'AdminAccount_detailController@get_account_individual_details')->name('admin.account_individual_details_list');


});




Route::group(array('module'=>'account_detail','namespace' => 'App\modules\account_detail\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('account_details/','Account_detailController@index')->name('account_details');
    Route::get('account/online_Account_opening','Account_detailController@index')->name('account_details.index');
    Route::get('account/application_form','Account_detailController@form')->name('account_details.form');
    Route::get('account/tracking_form','Account_detailController@trackingform')->name('account_details.tracking_form');
    
});
