<?php

namespace App\Modules\Account_detail\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;

class Account_detailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Citizens Bank International Limited';
        // $datas = get_lists('tbl_accounts');
        // $banners = Banner::where('banner_collection_id', 3)->get();

        return view('front.account_details.index', compact('page'));
        //
    }

    public function form()
    {
        $page['title'] = '';
        return view('front.account_details.form', compact('page'));
    }

}
