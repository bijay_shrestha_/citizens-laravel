<?php

namespace App\modules\account_detail\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\account_detail\Model\Account_detail;
use App\modules\branch\Model\Branch;
use Mail;
use App\modules\kyc\Model\Kyc;
use App\Exports\Account_Details_Export;
use App\Exports\Account_details_list;
use App\Exports\account_individual_detail_list;
use Excel;

class AdminAccount_detailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $account_detail;

    public function __construct() 
    {
        $this->account_detail = DB::table('tbl_account_details')
                       ->leftjoin('tbl_authorization', 'tbl_account_details.a_id', '=', 'tbl_authorization.a_id')
                       ->leftjoin('tbl_citizen_service', 'tbl_account_details.a_id', '=', 'tbl_citizen_service.a_id')
                       ->leftjoin('tbl_depositer', 'tbl_account_details.a_id', '=', 'tbl_depositer.a_id')
                       ->leftjoin('tbl_kyc', 'tbl_account_details.a_id', '=', 'tbl_kyc.a_id')
                       ->orderBy('tbl_account_details.a_id', "ASC");
                  //     ->select('*', 'tbl_depositer.dob as depositerdob')

    }


    public function index($status = null)
    {
        $page['title'] = 'Account Detail';
        return view("account_detail::index",compact('page', 'status'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getaccount_detailsJson(Request $request)
    {
        $account_detail = new Account_detail;
        $where = $this->_get_search_param($request);
        // For pagination
        $filterTotal = $account_detail->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->orderBy('a_id', "DSC")->where('del_flag',0)->where('account_status', $request->status)->get();

        // Display limited list        
        $rows = $account_detail->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->orderBy('a_id', "DSC")->where('del_flag',0)->where('account_status', $request->status)->get();

        //To count the total values present
        $total = $account_detail->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Account_detail | Create';
        return view("account_detail::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $success = Account_detail::Create($data);
        return redirect()->route('admin.account_details');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $account_detail = Account_detail::findOrFail($id);
        $page['title'] = 'Account_detail | Update';
        return view("account_detail::edit",compact('page','account_detail'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token');
        $success = Account_detail::where('id', $id)->update($data);
        return redirect()->route('admin.account_details');
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_account_details', 'a_id', $id);
        return redirect()->route('admin.account_details')->with('success', "Data deleted successfully.");
        //
    }

    public function detail($id) 
    {
        $account_details = DB::table('tbl_account_details')
                       ->leftjoin('tbl_authorization', 'tbl_account_details.a_id', '=', 'tbl_authorization.a_id')
                       ->leftjoin('tbl_citizen_service', 'tbl_account_details.a_id', '=', 'tbl_citizen_service.a_id')
                       ->leftjoin('tbl_depositer', 'tbl_account_details.a_id', '=', 'tbl_depositer.a_id')
                       ->where('tbl_account_details.a_id', $id)
                       ->orderBy('tbl_account_details.a_id', "ASC")
                  //     ->select('*', 'tbl_depositer.dob as depositerdob')
                       ->first();
//                       dd($account_details['branch']);
        $account_details = $this->account_detail->where('tbl_account_details.a_id', $id)->first();

        $account_detail = [];

        foreach ($account_details as $key => $detail_ac) {
            $account_detail[$key] = $detail_ac;
        }
        $page['title'] = 'Account Detail | Data';
        $account_id = $id;
        return view("account_detail::detail",compact('page','account_detail', 'account_id'));    
    }


    public function form_view($id)
    {
        $account_details = DB::table('tbl_account_details')
                       ->leftjoin('tbl_authorization', 'tbl_account_details.a_id', '=', 'tbl_authorization.a_id')
                       ->leftjoin('tbl_citizen_service', 'tbl_account_details.a_id', '=', 'tbl_citizen_service.a_id')
                       ->leftjoin('tbl_depositer', 'tbl_account_details.a_id', '=', 'tbl_depositer.a_id')
                       ->where('tbl_account_details.a_id', $id)
                       ->orderBy('tbl_account_details.a_id', "ASC")
                  //     ->select('*', 'tbl_depositer.dob as depositerdob')
                       ->first();
//                       dd($account_details['branch']);
        $account_detail = [];
        foreach ($account_details as $key => $detail_ac) {
            $account_detail[$key] = $detail_ac;
        }
        $page['title'] = 'Account Detail | Data';
        $detail_id = $id;


       
        return view("account_detail::form_view",compact('page','form_view', 'detail_id','account_detail')); 
    }

    public function kyc($id)
    {
        $account_details = Account_detail::where('a_id', $id)->firstOrfail();
        $account_details = $this->account_detail->where('tbl_account_details.a_id', $id)->first();
                       
        $account_detail = [];
        foreach ($account_details as $key => $detail_ac) {
            $account_detail[$key] = $detail_ac;
        }
        $page['title'] = 'Account Detail | Data';
        $account_id = $id;
        $kyc = Kyc::where('a_id', $id)->first()->toArray();
        return view("account_detail::kyc",compact('page','account_detail', 'account_id', 'kyc'));    
    }

    public function change_status(Request $request) 
    {
        //dd(config('citizen.main_id'));
        $data_form = $request->except('_token');
        $value =  $data_form['status'];
        $id =  $data_form['id'];
        $email =  $data_form['email'];
        $email = json_decode($email,true);
        $name =  $data_form['account_name'];
        $gender= $data_form['gender'];
        $branch= $data_form['branch'];
        $account_number= $data_form['account_number'];
        $remark =  $data_form['remark'];
        if(strtolower($gender)=='male'){
            $prifix="Mr.";
        } else {
            $prifix="Ms.";
        }
        $manager = Branch::where('name', $branch)->get();
        $branch_manager=$manager[0]->branch_manager;
        $branch_contact=$manager[0]->contact_no;

        $update_data=array('account_status'=>$value,
            'account_no'=>$account_number,
            'remark'=>$remark);

        $success = DB::table('tbl_account_details')->where('a_id', $id)->update($update_data);
        $success = 1;
        if ($success) {
            $subject="Contact from :: " ."Citizen Bank";
            $from_email =  "subi.maharjan@ctznbank.com";            
            $data = array(
                'prefix'=>$prifix,
                'lastname'=>ucwords(strtolower($name)),
                'accountnumber'=>$account_number,
                'branchmanager'=>$branch_manager,
                'branchcontact'=>$branch_contact,
                'branch'=>$branch,
                'remark'=>$remark
            );
            if($value == 'Approved'){
                $file = 'account_detail::mail_approved';
            }
            else{
                $file = 'account_detail::mail_denied';
            }
            //$email_admin1="niroj@pagodalabs.com";
            $email_admin2="ebanking@ctznbank.com";
            $email_admin3="subi.maharjan@ctznbank.com";

            foreach ($email['email'] as $e) {
                $email_to[] = $e;
            //    $email_to[]=$email_admin1;
                $email_to[]=$email_admin2;
                $email_to[]=$email_admin3;
            }

            foreach ($email_to as $email_id) {
                Mail::send($file, $data, function($message) use ($email_id, $name, $subject) {
                   $message->to($email_id, $name)->subject
                      ($subject);
                   $message->from(config('citizen.main_id'),'Citizen Bank');
                });
            }

            $email_toadmin=NULL;
        //    $email_toadmin[]=$email_admin1;
            $email_toadmin[]=$email_admin2;
            $email_toadmin[]=$email_admin3;
            
            $data = array(
            'prefix'=>$prifix,
            'lastname'=>ucwords(strtolower($name)),
            'accountnumber'=>$account_number,
            'branchmanager'=>$branch_manager,
            'branchcontact'=>$branch_contact,
            'branch'=>$branch,
            'remark'=>$remark
                );

                Mail::send('account_detail::mail_notification', $data, function($message) use ($name, $subject) {
                   $message->to('ebanking@ctznbank.com', $name)->subject
                      ($subject)->cc($email_toadmin);
                   $message->from(config('citizen.main_id'),'Citizen Bank');
                });

            return redirect()->route('admin.account_details', "New")->with('success', "Account Status Changes and mail was send.");

        } else {
            return redirect()->route('admin.account_details', "New")->with('success', "Account Status Changes but failed to send mail.");
        }

    }



    public function exportAccountDetails()
    {
        $data = Account_detail::where('del_flag',0)->get();

        return Excel::download(new Account_Details_Export("account_detail::account_details",$data), 'Account Details.xlsx');
    }

    public function get_account_details()
    {
        // $final_data=array();
        // $account_details= Account_detail::where('del_flag',0)->get();
        // foreach($account_details as $ad)
        // {
        //     $depositer=Depositer::where('a_id',$ad->a_id)->get();
        //     $data =array(
        //         'account_details'=>$account_details,
        //         'depositor'=>$depositer,
        //     );
        //     array_push($final_data,$data);
        // }
        // dd($depositer)
       
        $account_details = DB::table('tbl_account_details')->where('del_flag',0)
        ->leftjoin('tbl_depositer','tbl_account_details.a_id','=','tbl_depositer.a_id')
        ->leftjoin('tbl_authorization','tbl_account_details.a_id','=','tbl_authorization.a_id')
        ->leftjoin('tbl_kyc','tbl_account_details.a_id','=','tbl_kyc.a_id')
        ->leftjoin('tbl_citizen_service','tbl_account_details.a_id','=','tbl_citizen_service.a_id')
        ->get();
            //return $account_details->toJson();
            // return $account_details;
        //        return view('export.account_details_list');
        //return $account_details;
        // return view("account_detail::account_details_list")->with('data',$account_details);
        return Excel::download(new Account_details_list("account_detail::account_details_list",$account_details), 'Account Details.xlsx');
    }



    public function get_account_individual_details($id)
    {
        $data = DB::table('tbl_account_details')
                       ->leftjoin('tbl_authorization', 'tbl_account_details.a_id', '=', 'tbl_authorization.a_id')
                       ->leftjoin('tbl_citizen_service', 'tbl_account_details.a_id', '=', 'tbl_citizen_service.a_id')
                       ->leftjoin('tbl_depositer', 'tbl_account_details.a_id', '=', 'tbl_depositer.a_id')
                       ->leftjoin('tbl_kyc', 'tbl_account_details.a_id', '=', 'tbl_kyc.a_id')
                       ->where('tbl_account_details.a_id', $id)
                       ->orderBy('tbl_account_details.a_id', "ASC")
                  //     ->select('*', 'tbl_depositer.dob as depositerdob')
                       ->get();
       // return $data;
        return view('account_detail::account_individual_details_list')->with('data',$data);
        // return Excel::download(new account_individual_detail_list("account_detail::account_individual_details_list",$data), 'Account Individual Details.xlsx');
    }


}
