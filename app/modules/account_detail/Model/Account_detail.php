<?php

namespace App\Modules\Account_detail\Model;


use Illuminate\Database\Eloquent\Model;

class Account_detail extends Model
{
    public  $table = 'tbl_account_details';

    protected $fillable = ['a_id','reference_code','applying_from','branch','photo','type','account_category','types_of_individual','account_type','currency','account_name','father_name','grandfather_name','spouse_name','existing_relationship','account_number','dob','dob_nepali','gender','occupation','nationality','citizenship_no','passport_no','nrn_card','birth_certificate','other_id','place_of_issue','date_of_issue','date_of_issue_bs','age_minor','name_of_guardian','relationship_minor','marital_status','email','pan_number','phone_number','mobile','fax_number','pox_box','permanent_address_house_no','permanent_address_vdc','permanent_address_ward_number','permanent_address_street','permanent_address_district','correspondence_address_house_no','correspondence_address_vdc','correspondence_address_ward_number','correspondence_address_street','correspondence_address_district','account_status','company_name','company_address','com_position','expiry_date','foreign_address','country','contact_no','type_of_visa','visa_expiry_date','city_state','account_no','remark','applied_date','del_flag',];
}
