<style>
    
    #map {
            margin: auto;
            height: 400px;
            width: 800px;
        }

        /* label {
            padding-right: 30px;
        } */
        img {
            width: 300px;
        }
        input {
            border: 2px solid #8bceb6;
            
        }
        input[type=text] {
            box-shadow: 3px 3px #8bceb6;
        }
        .box-width {
            width: 100%;
        }
        .space-for-td{
            padding-top: 10px;
            padding-bottom: 10px;
        }
        .pp-photo {
            border: 2px solid #8bceb6;
            width: 92px;
            text-align: center;
            font-weight: bold;
        }
        .ac-book-instruct {
            border-bottom: 2px solid #8bceb6;
            padding-bottom: 10px;
        }
        .ac-book-instruct label {
            padding-left: 5px;
            padding-right: 106px;
        }
        
        .four-i label{
            padding-right: 20px;
        }
        .four-i::after {
            content: "";
            border: 1px solid #8bceb6;
        }
        .four-ii label {
            padding-right: 20px;
        }
        .four-ii {
            padding-bottom: 10px;
        }
        .four-i {
            padding-bottom: 10px;
        }
        .acct-name {
            padding-bottom: 10px;
        }
        .address-info {
            width: 100%;
            border-collapse: collapse;
            border-top: 2px solid #8bceb6;
            border-bottom: 2px solid #8bceb6;

        }
        .address-info-td {
            height: 75px;
            vertical-align: top;
            border-left: 2px solid #8bceb6;
        }
        .table-inner {
            border-bottom: 2px solid #8bceb6;
            width: 100%;
        }
        .date-part {
            padding-right: 100px;
            text-align:right;
        }
        .singly-jointly {
            padding-right: 15px;
        }
        .dob-occ-nat {
            text-align: center;
        }
        .dob {
            padding-right: 10px;
        }
        .occupation {
            padding-right: 10px;
        }
        .down-border {
            border-bottom: 2px solid #8bceb6;
            padding-bottom: 10px;
        }
        .correspondence {
            padding-bottom: 7px;
            padding-top: 3px;

        }
        .accnt-op {
            padding-top: 3px;
            padding-bottom: 3px;
        }
        
        input {
            border: 2px solid #8bceb6;
        }
        .ac-holder-minor {
            padding-bottom: 10px;
        }
        .minor-space{
            padding: 10px 0px;
        }
        .acct-name label{
            margin-bottom: 5px;
            display: block;
        }
        .statement {
            border-top: 2px solid #8bceb6;
            /* border-bottom: 1px solid #000; */
            padding-top: 6px;
        }

        
        .company-and-office-info {
            width: 100%;
            border-top: 2px solid #8bceb6;
            border-bottom: 2px solid #8bceb6;
        }
        .company-and-office-td {
            border-left: 2px solid #8bceb6;
            height: 30px;
            border-top: 2px solid #8bceb6;
            
        }
        .table {
            width: 100%;
            /* border-bottom: 1px solid #8bceb6; */
        }
        .ind-acc-details {
            width: 100%;
            border: 2px solid #8bceb6;
            
            

        }
        .ind-acc-details th {
            font-weight: normal;
            /* border-bottom: 1px solid #000; */
           
        }
        .ind-acc-details td{
            /* border-bottom: 1px solid #8bceb6; */
            height: 20px;
        }
        .table-head {
            width: 100%;
    background: #040444;
    color: #fff;
    font-weight: bold;
    text-align: center;
        }
        .table-box-border {
            border: 2px solid #8bceb6;
            border-collapse: collapse;
        }
        .table-box-border .bank {
            border: 2px solid #8bceb6;
        }
        .table-box-border >td {
            border: 2px solid #8bceb6;
        }

        .table-detail {
            border-collapse: collapse;
          
        }
        .table-detail th {
            font-weight: normal;
            height: 40px;
        }
        .table-detail td {
            height: 40px;
        }
        .table-detail-td {
            border-left: 2px solid #8bceb6 !important;;
            
        }

        

        .table-firm {
            border-collapse: collapse;
            width: 100%;
        }
        .table-firm th,td {
            font-weight: normal;
            height: 30px;
            
        }
        .table-firm td {
            height: 40px;
        }
        .table-firm th {
            height: 40px;
            font-weight: normal;
        }
        .table-firm-td {
            border-right: 2px solid #8bceb6 !important;
        }
        .firm {
            border: 2px solid #8bceb6;
            border-left: 0;
            border-right: 0;
        }
        
        .input-height{
            height: 26px;
        }
        .head-space {
            padding-top: 10px;
        }
        .info {
            padding-left: 40px;
            line-height: 1.6em;
        }
        .internet {
            padding-left: 27px;
        }
        .transaction {
            margin-left: 70px;
        }
        .table-width {
            width: 100%;
        }
        .table-sign {
            border-collapse: collapse;
            border-left: 0;
            border-right: 0;
        }
        .table-sign th {
            font-weight: normal;
            height: 30px;
        }
        .table-sign td {
            height: 40px;
        }
        .table-sign-td {
            border-left: 2px solid #8bceb6 !important;
            padding-right: 100px;
        }
        .sign {
            border: 2px solid #8bceb6;
            border-left: 0;
            border-right: 0;
        }
        .map{
            border-collapse: collapse;
            height: 300px;
        }
        .notes {
            padding-right: 13px;
        }
        .space-td {
            padding-bottom: 10px;
        }
        .addr-space{
            padding-left: 10px;
        }
        .head-align {
            text-align: center;
            font-weight: normal;
        }
        h3 {
            padding-right: 35px; 
        }
        .for-font {
            font-weight: bold;
        }

        .td-left{
            padding-right: 20px;
        }

        .age {
            width: 20%;
        }
        .guardian-name {
            width: 50%;
        }
        .relationship {
            text-align: right;
        }
        .checkbox-space {
            padding-bottom: 10px;
            
        }
        .checkbox-align {
            float: left;
        }
        .div-wrap {
            display: inline-block;
        }
        .table-inner-space {
            padding-bottom: 3px;
        }
        .account-no-td {
            padding-bottom: 3px;
        }
        .top-space {
            padding-top: 4px; 
        }
        .opened-date-bottom {
            border-bottom: none !important;
        }
        .opened-date-top {
            border-top: none !important;
        }
        .documentation-bottom {
            border-bottom: none !important;
        }
        .documentation-top {
            border-top: none !important;
        }
        .account-number {
            padding-bottom: 10px;
            width: 100%;
        }
        .ac-no {
            letter-spacing: 0.2em;
            padding: 0px 5px;
        }
    </style>
    <section class="content-header">


            <html>

                    <head>
                        <style>
                        
                        #map {
                                margin: auto;
                                height: 400px;
                                width: 800px;
                            }
                    
                            /* label {
                                padding-right: 30px;
                            } */
                            img {
                                width: 300px;
                            }
                            input {
                                border: 2px solid #8bceb6;
                                
                            }
                            input[type=text] {
                                box-shadow: 3px 3px #8bceb6;
                            }
                            .box-width {
                                width: 100%;
                            }
                            .space-for-td{
                                padding-top: 10px;
                                padding-bottom: 10px;
                            }
                            .pp-photo {
                                border: 2px solid #8bceb6;
                                width: 92px;
                                text-align: center;
                                font-weight: bold;
                            }
                            .ac-book-instruct {
                                border-bottom: 2px solid #8bceb6;
                                padding-bottom: 10px;
                            }
                            .ac-book-instruct label {
                                padding-left: 5px;
                                padding-right: 106px;
                            }
                            
                            .four-i label{
                                padding-right: 20px;
                            }
                            .four-i::after {
                                content: "";
                                border: 1px solid #8bceb6;
                            }
                            .four-ii label {
                                padding-right: 20px;
                            }
                            .four-ii {
                                padding-bottom: 10px;
                            }
                            .four-i {
                                padding-bottom: 10px;
                            }
                            .acct-name {
                                padding-bottom: 10px;
                            }
                            .address-info {
                                width: 100%;
                                border-collapse: collapse;
                                border-top: 2px solid #8bceb6;
                                border-bottom: 2px solid #8bceb6;
                    
                            }
                            .address-info-td {
                                height: 75px;
                                vertical-align: top;
                                border-left: 2px solid #8bceb6;
                            }
                            .table-inner {
                                border-bottom: 2px solid #8bceb6;
                                width: 100%;
                            }
                            .date-part {
                                padding-right: 100px;
                                text-align:right;
                            }
                            .singly-jointly {
                                padding-right: 15px;
                            }
                            .dob-occ-nat {
                                text-align: center;
                            }
                            .dob {
                                padding-right: 10px;
                            }
                            .occupation {
                                padding-right: 10px;
                            }
                            .down-border {
                                border-bottom: 2px solid #8bceb6;
                                padding-bottom: 10px;
                            }
                            .correspondence {
                                padding-bottom: 7px;
                                padding-top: 3px;
                    
                            }
                            .accnt-op {
                                padding-top: 3px;
                                padding-bottom: 3px;
                            }
                            
                            input {
                                border: 2px solid #8bceb6;
                            }
                            .ac-holder-minor {
                                padding-bottom: 10px;
                            }
                            .minor-space{
                                padding: 10px 0px;
                            }
                            .acct-name label{
                                margin-bottom: 5px;
                                display: block;
                            }
                            .statement {
                                border-top: 2px solid #8bceb6;
                                /* border-bottom: 1px solid #000; */
                                padding-top: 6px;
                            }
                    
                            
                            .company-and-office-info {
                                width: 100%;
                                border-top: 2px solid #8bceb6;
                                border-bottom: 2px solid #8bceb6;
                            }
                            .company-and-office-td {
                                border-left: 2px solid #8bceb6;
                                height: 30px;
                                border-top: 2px solid #8bceb6;
                                
                            }
                            .table {
                                width: 100%;
                                /* border-bottom: 1px solid #8bceb6; */
                            }
                            .ind-acc-details {
                                width: 100%;
                                border: 2px solid #8bceb6;
                                
                                
                    
                            }
                            .ind-acc-details th {
                                font-weight: normal;
                                /* border-bottom: 1px solid #000; */
                               
                            }
                            .ind-acc-details td{
                                /* border-bottom: 1px solid #8bceb6; */
                                height: 20px;
                            }
                            .table-head {
                                width: 100%;
                        background: #040444;
                        color: #fff;
                        font-weight: bold;
                        text-align: center;
                            }
                            .table-box-border {
                                border: 2px solid #8bceb6;
                                border-collapse: collapse;
                            }
                            .table-box-border .bank {
                                border: 2px solid #8bceb6;
                            }
                            .table-box-border >td {
                                border: 2px solid #8bceb6;
                            }
                    
                            .table-detail {
                                border-collapse: collapse;
                              
                            }
                            .table-detail th {
                                font-weight: normal;
                                height: 40px;
                            }
                            .table-detail td {
                                height: 40px;
                            }
                            .table-detail-td {
                                border-left: 2px solid #8bceb6 !important;;
                                
                            }
                    
                            
                    
                            .table-firm {
                                border-collapse: collapse;
                                width: 100%;
                            }
                            .table-firm th,td {
                                font-weight: normal;
                                height: 30px;
                                
                            }
                            .table-firm td {
                                height: 40px;
                            }
                            .table-firm th {
                                height: 40px;
                                font-weight: normal;
                            }
                            .table-firm-td {
                                border-right: 2px solid #8bceb6 !important;
                            }
                            .firm {
                                border: 2px solid #8bceb6;
                                border-left: 0;
                                border-right: 0;
                            }
                            
                            .input-height{
                                height: 26px;
                            }
                            .head-space {
                                padding-top: 10px;
                            }
                            .info {
                                padding-left: 40px;
                                line-height: 1.6em;
                            }
                            .internet {
                                padding-left: 27px;
                            }
                            .transaction {
                                margin-left: 70px;
                            }
                            .table-width {
                                width: 100%;
                            }
                            .table-sign {
                                border-collapse: collapse;
                                border-left: 0;
                                border-right: 0;
                            }
                            .table-sign th {
                                font-weight: normal;
                                height: 30px;
                            }
                            .table-sign td {
                                height: 40px;
                            }
                            .table-sign-td {
                                border-left: 2px solid #8bceb6 !important;
                                padding-right: 100px;
                            }
                            .sign {
                                border: 2px solid #8bceb6;
                                border-left: 0;
                                border-right: 0;
                            }
                            .map{
                                border-collapse: collapse;
                                height: 300px;
                            }
                            .notes {
                                padding-right: 13px;
                            }
                            .space-td {
                                padding-bottom: 10px;
                            }
                            .addr-space{
                                padding-left: 10px;
                            }
                            .head-align {
                                text-align: center;
                                font-weight: normal;
                            }
                            h3 {
                                padding-right: 35px; 
                            }
                            .for-font {
                                font-weight: bold;
                            }
                    
                            .td-left{
                                padding-right: 20px;
                            }
                    
                            .age {
                                width: 20%;
                            }
                            .guardian-name {
                                width: 50%;
                            }
                            .relationship {
                                text-align: right;
                            }
                            .checkbox-space {
                                padding-bottom: 10px;
                                
                            }
                            .checkbox-align {
                                float: left;
                            }
                            .div-wrap {
                                display: inline-block;
                            }
                            .table-inner-space {
                                padding-bottom: 3px;
                            }
                            .account-no-td {
                                padding-bottom: 3px;
                            }
                            .top-space {
                                padding-top: 4px; 
                            }
                            .opened-date-bottom {
                                border-bottom: none !important;
                            }
                            .opened-date-top {
                                border-top: none !important;
                            }
                            .documentation-bottom {
                                border-bottom: none !important;
                            }
                            .documentation-top {
                                border-top: none !important;
                            }
                            .account-number {
                                padding-bottom: 10px;
                                width: 100%;
                            }
                            .ac-no {
                                letter-spacing: 0.2em;
                                padding: 0px 5px;
                            }
                        </style>
                            @php
                                $longitude=json_decode($account_detail['longitude'],true);
                                $latitude=json_decode($account_detail['latitude'],true);
                                $account_name = json_decode($account_detail['account_name'],true);
                                $father_name = json_decode($account_detail['father_name'],true);
                                $grandfather_name = json_decode($account_detail['grandfather_name'],true);
                                $spouse_name = json_decode($account_detail['spouse_name'],true);
                                $account_number = json_decode($account_detail['account_number'],true);
                                $dob = json_decode($account_detail['dob'],true);
                                $dob_nepali = json_decode($account_detail['dob_nepali'],true);
                                $occupation = json_decode($account_detail['occupation'],true);
                                $gender = json_decode($account_detail['gender'],true);
                                $nationality = json_decode($account_detail['nationality'],true);
                                $passport_no = json_decode($account_detail['passport_no'],true);
                                $citizenship_no = json_decode($account_detail['citizenship_no'],true);
                                $nrn_card = json_decode($account_detail['nrn_card'],true);
                                $expiry_date = json_decode($account_detail['expiry_date'],true);
                                $foreign_address = json_decode($account_detail['foreign_address'],true);
                                $country = json_decode($account_detail['country'],true);
                                $contact_no = json_decode($account_detail['contact_no'],true);
                                $type_of_visa = json_decode($account_detail['type_of_visa'],true);
                                $visa_expiry_date = json_decode($account_detail['visa_expiry_date'],true);
                                $city_state = json_decode($account_detail['city_state'],true);
                                $birth_certificate = json_decode($account_detail['birth_certificate'],true);
                                $other_id = json_decode($account_detail['other_id'],true);
                                $place_of_issue = json_decode($account_detail['place_of_issue'],true);
                                $date_of_issue = json_decode($account_detail['date_of_issue'],true);
                                $date_of_issue_bs = json_decode($account_detail['date_of_issue_bs'],true);
                                $marital_status = json_decode($account_detail['marital_status'],true);
                                $email = json_decode($account_detail['email'],true);
                                $pan_number = json_decode($account_detail['pan_number'],true);
                                $phone_number = json_decode($account_detail['phone_number'],true);
                                $mobile = json_decode($account_detail['mobile'],true);
                                $fax_number = json_decode($account_detail['fax_number'],true);
                                $pox_box = json_decode($account_detail['pox_box'],true);
                                $permanent_address_house_no = json_decode($account_detail['permanent_address_house_no'],true);
                                $permanent_address_vdc = json_decode($account_detail['permanent_address_vdc'],true);
                                $permanent_address_ward_number = json_decode($account_detail['permanent_address_ward_number'],true);
                                $permanent_address_street = json_decode($account_detail['permanent_address_street'],true);
                                $permanent_address_district = json_decode($account_detail['permanent_address_district'],true);
                                $correspondence_address_house_no = json_decode($account_detail['correspondence_address_house_no'],true);
                                $correspondence_address_vdc = json_decode($account_detail['correspondence_address_vdc'],true);
                                $correspondence_address_ward_number = json_decode($account_detail['correspondence_address_ward_number'],true);
                                $correspondence_address_ward_number = json_decode($account_detail['correspondence_address_ward_number'],true);
                                $correspondence_address_district = json_decode($account_detail['correspondence_address_district'],true);
                                $correspondence_address_street = json_decode($account_detail['correspondence_address_street'],true);   
                            @endphp
                        <section style="width: 720px;margin:0;margin: auto">
                            <table style="width: 100%;">
                                <tr>
                                    <td>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td><img src="{{ url('images/CB_logo.png') }}" alt="logo"></td>
                                                <td>
                                                    <h3>ACCOUNT OPENING FORM</h3>
                                                </td>
                                                <td class="pp-photo"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td class="for-font">
                                                    <p>Citizens Bank International Ltd.</p>
                                                    <p><u>&nbsp;&nbsp;&nbsp; {{ $account_detail['branch'] }} &nbsp;&nbsp;&nbsp;</u>BRANCH</p>
                                                </td>
                                                <td class="for-font" style="vertical-align:top;">
                                                    <label>A/C No.<input type="text" disabled value="{{ $account_number['account_number'][0] }}" class="input-height ac-no"></label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width:100%">
                                            <tr>
                                                <td class="date-part for-font">Date:</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width:100%">
                                            <tr>
                                                <td>Dear Sir</td>
                                            </tr>
                                            <tr>
                                                <td class="ac-book-instruct">Please open in your book an account with undermentioned title.</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width:100%">
                                            <tr>
                                                <td class="ac-book-instruct">
                                                    <input type="checkbox" disabled><label for="current">Current</label>
                                                    <input type="checkbox" disabled><label for="call">Call</label>
                                                    <input type="checkbox" disabled checked><label for="savings">Savings</label>
                                                    <input type="checkbox" disabled><label for="others">Others</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="ac-book-instruct">
                                                    <div class="div-wrap">
                                                        <div class="checkbox-space checkbox-align">
                                                            <input disabled checked  type="checkbox"><label for="individual" style="padding-right:38px;">Individual
                                                                Account</label>
                                                        </div>
                                                        <div class="checkbox-space checkbox-align">
                                                            <input disabled type="checkbox"><label for="sole proprietorship" style="padding-right:44px;">Sole
                                                                Proprietorship Account</label>
                                                        </div>
                                                        <div class="checkbox-space checkbox-align">
                                                            <input disabled type="checkbox"><label for="partnership" style="padding-right: 103px;">Partnership
                                                                Account</label>
                                                        </div>
                                                        <div class="checkbox-align">
                                                            <input disabled type="checkbox"><label for="company" style="padding-right: 42px;">Company Account</label>
                                                        </div>
                                                       <div class="checkbox-align">
                                                        <input disabled type="checkbox"><label for="Others">Others
                                                            __________________________________________</label>
                                                       </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="table-inner">
                                            <tr>
                                                <td class="four-i ">
                                                    <label>Account Type</label>
                                                    <input type="checkbox" disabled @if($account_detail['account_type'] == 'Single') {{ 'checked' }} @endif > <label for="single">Single</label>
                                                    <input type="checkbox" disabled @if($account_detail['account_type'] == 'Joint') {{ 'checked' }} @endif ><label for="joint">Joint</label>
                                                    <input type="checkbox" disabled @if($account_detail['account_type'] == 'Minor') {{ 'checked' }} @endif ><label for="minor">Minor</label>
                                                </td>
                                                <td colspan="2" class="four-ii ">
                                                    Currency
                                                    <input type="checkbox" disabled checked><label for="NPR">NPR</label>
                                                    <input type="checkbox" disabled><label for="USD">USD</label>
                                                    <input type="checkbox" disabled><label for="Other">Other</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                               @php $i=0;  @endphp
                                @foreach( $account_name['account_name'] as $name) 
                                    <tr>
                                        <td>
                                            <table style="width:100%">
                                                <tr class="account-det-part ">
                                                    <td class="acct-name td-left">
                                                        <label>Account Name</label>
                                                        <input type="text" readonly value="&nbsp;&nbsp;{{ strtoupper($name) }} " style="width:100%" class="input-height">
                                                    </td>
                                                    <td class="acct-name">
                                                        <label>Father's Name</label>
                                                        <input type="text" readonly value="&nbsp;&nbsp;{{ strtoupper($father_name['father_name'][$i]) }} " style="width:100%" class="input-height">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            <table style="width:100%">
                                                <tr class="account-det-part">
                                                    <td class="acct-name td-left">
                                                        <label>Grandfather's Name</label>
                                                        <input type="text" readonly value="&nbsp;&nbsp;{{ strtoupper($grandfather_name['grandfather_name'][$i]) }}" style="width:100%" class="input-height">
                                                    </td>
                                                    <td class="acct-name">
                                                        <label>Spouse's Name</label>
                                                        <input type="text" readonly value="&nbsp;&nbsp;{{ strtoupper($spouse_name['spouse_name'][$i]) }}" style="width:100%" class="input-height">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            <table style="width:100%">
                                                <tr>
                                                    <td class="dob-occ-nat ">
                                                        <label>DOB</label>
                                                    </td>
                                                    <td class="dob-occ-nat ">
                                                        <label>Occupation</label>
                                                    </td>
                                                    <td class="dob-occ-nat">
                                                        <label>Nationality</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="dob">
                                                        <input type="text"  readonly value="&nbsp;&nbsp;{{ $dob['dob'][$i] }} &nbsp;/&nbsp; {{ $dob_nepali['dob_nepali'][$i] }}" class="input-height box-width" >
                                                    </td>
                                                    <td class="occupation">
                                                        @if((is_string($occupation['occupation'][$i])) && (is_array(json_decode($occupation['occupation'][$i], true))) && (json_last_error() == JSON_ERROR_NONE))
                                                     @php $occupation_array  = json_decode($occupation['occupation'][$i],true); @endphp
                                                            <input type="text" readonly value="&nbsp;&nbsp;{{ $occupation_array['company_position'] }}" class="input-height box-width" >
                                                        @else
                                                            <input type="text" readonly value="&nbsp;&nbsp;{{ strtoupper($occupation['occupation'][$i]) }}" class="input-height box-width">
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <input type="text" readonly value="&nbsp;&nbsp;{{ strtoupper($nationality['nationality'][$i]) }}" class="input-height box-width">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="box-width down-border">
                                                <tr>
                                                    <td class="acct-name">
                                                        <label>Passport/Citizenship No.</label>
                                                    </td>
                                                    <td class="acct-name">
                                                        @if($passport_no['passport_no'][$i]!="")
                                                            <input type="text" readonly value="&nbsp;&nbsp; {{ $passport_no['passport_no'][$i] }} "  class="input-height">
                                                        @endif
                                                        @if($citizenship_no['citizenship_no'][$i]!="")
                                                            <input type="text" readonly value="&nbsp;&nbsp; {{ $citizenship_no['citizenship_no'][$i] }}"  class="input-height">
                                                         @endif
                                                    </td>
                                                    <td class="acct-name">
                                                        <label>Date of Issue</label>
                        
                                                    </td>
                                                    <td>
                                                        <input type="text" readonly value="&nbsp;&nbsp;{{ $date_of_issue['date_of_issue'][$i] }}" class="input-height box-width">
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>
                                                        <label>Marital Status</label>
                                                    </td>
                                                    <td>
                                                        <input type="text" readonly value="&nbsp;&nbsp;{{ $marital_status['marital_status'][$i] }} " class="input-height">
                                                    </td>
                                                    <td class="acct-name">
                                                        <label>Place of Issue</label>
                        
                                                    </td>
                                                    <td>
                                                        <input type="text" readonly value="&nbsp;&nbsp;{{ $place_of_issue['place_of_issue'][$i] }}" class="input-height box-width">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table style="width:100%" class="ac-holder-minor ">
                                                <tr>
                                                    <td colspan="3">If account holder(s) is/are minor</td>
                                                </tr>
                                                <tr>
                                                    <td class="age">
                                                        <label>Age of minor</label>
                                                        <input type="text" style="width:25%" readonly value="&nbsp;&nbsp; @if(isset($account_detail['age_minor'])) {{ $account_detail['age_minor'] }} @endif" class="input-height">
                                                    </td>
                                                    <td class="guardian-name">
                                                        <label>Name of guardian</label>
                                                        <input type="text" style="width:60%" readonly value="&nbsp;&nbsp; @if(isset($account_detail['age_minor'])) {{ $account_detail['name_of_guardian'] }} @endif" class="input-height">
                                                    </td>
                                                    <td class="relationship">
                                                        <label>Relationship</label>
                                                        <input type="text" style="width:57%" readonly value="&nbsp;&nbsp; @if(isset($account_detail['age_minor'])) {{ $account_detail['relationship_minor']}} @endif" class="input-height">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="address-info">
                                                <tr>
                                                    <td class="addr-space" style="vertical-align:top;">Permanent Address
                                                        <br>
                                                        House No.: {{ $permanent_address_house_no['permanent_address_house_no'][$i] }} <br>
                                                        VDC/Muncipality: {{ $permanent_address_vdc['permanent_address_vdc'][$i]  }} <br>
                                                        Ward No.: {{ $permanent_address_ward_number['permanent_address_ward_number'][$i] }} <br>
                                                        Street: {{ $permanent_address_street['permanent_address_street'][$i] }} <br>
                                                        District: {{ $permanent_address_district['permanent_address_district'][$i] }}
                                                    </td>
                                                    <td class="address-info-td addr-space">Present Address</td>
                                                    <td class="address-info-td addr-space">
                                                        Communication Address
                                                         @if(isset($correspondence_address_house_no))
                                                            <br>
                                                            House No.: {{ $correspondence_address_house_no['correspondence_address_house_no'][$i] }} <br>
                                                            VDC/Muncipality: {{ $correspondence_address_vdc['correspondence_address_vdc'][$i] }} <br>
                                                            Ward No.: {{ $correspondence_address_ward_number['correspondence_address_ward_number'][$i] }} <br>
                                                            Street: {{ $correspondence_address_street['correspondence_address_street'][$i] }} <br>
                                                            District: {{ $correspondence_address_district['correspondence_address_district'][$i] }}
                                                        @endif
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="table-inner  table-inner-space">
                                                <tr>
                                                    <td>
                                                        <label>Ph. number</label> : &nbsp;&nbsp;{{ $phone_number['phone_number'][$i] }}
                                                    </td>
                                                    <td>
                                                        <label>Mobile Number</label> : &nbsp;&nbsp;{{ $mobile['mobile'][$i] }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label>Telex/Fax number</label> : &nbsp;&nbsp; @if(isset($fax_number) && $fax_number['fax_number'][$i] != null) {{ $fax_number['fax_number'][$i]  }} @endif
                                                    </td>
                                                    <td>
                                                        <label>Email Address</label> : &nbsp;&nbsp;{{ $email['email'][$i] }}
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="account-no-td">
                                            <table class="table-inner">
                                                <tr>
                                                    <td class="correspondence">
                                                        <label>Correspondence Address</label>
                                                        <!-- </td>
                                                        <td>    -->
                                                        <label>( ) Business</label>
                                                    </td>
                                                    <td class="correspondence">
                                                        <label>( ) Residential</label>
                                                    </td>
                                                    <td class="correspondence">
                                                        <label>Post Box No.</label> : &nbsp;&nbsp; @if(isset($pox_box) && $pox_box['pox_box'][$i] != null) {{ $pox_box['pox_box'][$i]  }} @endif
                                                    </td>
                                                </tr>
                                
                                            </table>
                                        </td>
                                    </tr>
                                    @php $i++;  @endphp 
                                @endforeach 
                            </table>
                            <table style="width: 100%;">
                                <tr>
                                    <td class="head-space">
                                        <table class="table-head">
                                            <tr>
                                                <td>
                                                    <label>DEPOSITOR'S NOMINATION (TO BE FILLED BY INDIVIDUAL/JOINT DEPOSITORS ONLY</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                @php            
                    $mr_miss = json_decode($account_detail['mr_miss'],true);
                    $depositer_name = json_decode($account_detail['depositer_name'],true);
                    $relationship = json_decode($account_detail['relationship'],true);
                    $nominee_images = json_decode($account_detail['nominee_images'],true);
                    $images = json_decode($account_detail['images'],true);
                    $nominee_citizenship_fronts = json_decode($account_detail['nominee_citizenship_fronts'],true);
                    $citizenship_fronts = json_decode($account_detail['citizenship_fronts'],true);
                    $nominee_citizenship_backs = json_decode($account_detail['nominee_citizenship_backs'],true);
                    $citizenship_backs = json_decode($account_detail['citizenship_backs'],true);
                    $i=0; 
                @endphp
                @if(isset($depositer_name))
                @foreach($depositer_name['depositer_name'] as $dname)   
                <tr>
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <td>Mr./Mrs/Miss<u>&nbsp;&nbsp;&nbsp;{{ strtoupper($dname) }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>
                                </td>
                                <td colspan="2">Son/Wife/daughter of
                                <u>&nbsp;&nbsp;&nbsp;{{ strtoupper($account_name['account_name'][0]) }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>
                                </td>
                            </tr>
                            <tr>
                                <td>Date of Birth...................................................</td>
                                <td>Age.........................</td>
                                <td>Realtionship<u>&nbsp;&nbsp;&nbsp;{{ strtoupper($relationship['relationship'][$i]) }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td>
                            </tr>
                            <tr>
                                <td colspan="3">Permanent Address.................................................................................................................................................
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">Contact Addrress.............................................................................</td>
                                <td>Tel. No...................................................</td>
                            </tr>
                            </table>
                            </td>
                        </tr>
                        @if($i == 1)
                            <tr>
                                <td style="border-bottom: 2px solid #8bceb6;">And In the event of the death of above
                                                nominee(s), I appoint the following alternate nominee(s)</td>
                            </tr>
                        @endif
                        @php  $i++; @endphp
                        @endforeach
                        @endif
                                <tr>
                                    <td style="padding-top: 50px;">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td>Introduction:</td>
                        
                                                <td style="text-align:right;"><span style="border-top: 2px solid #8bceb6;">Authorized
                                                        Signature(s)</span></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width:100%">
                                            <tr>
                                                <td>
                                                    <label>Name:</label>
                                                </td>
                                                <td>
                                                    <input type="text" style="width: 306px;" class="input-height">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="account-number">
                                            <tr>
                                                <td>
                                                    <label>Account Number</label>
                                                </td>
                                                <td>
                                                    <input type="text" style="width: 235px;" class="input-height">
                                                </td>
                                                <td style="padding-right: 80px;">
                                                    <label>Signature</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 100%;" class="table-box-border">
                                            <tr>
                                                <th colspan="5" class="bank head-align">For Bank's use only</th>
                        
                                            </tr>
                                            <tr>
                                                <td class="bank opened-date-bottom"></td>
                                                <td class="bank">Initial</td>
                                                <td class="bank documentation-bottom"></td>
                                                <td class="bank ">Initial</td>
                                                <td>Account opening approved by</td>
                                            </tr>
                                            <tr>
                                                <td class="bank opened-date-top">Opened Date:</td>
                                                <td class="bank"></td>
                                                <td class="bank documentation-top">Documentation Completed</td>
                                                <td class="bank"></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td class="bank">Specimen Signature Card</td>
                                                <td class="bank"></td>
                                                <td class="bank">Introduction confirmed</td>
                                                <td class="bank"></td>
                                                <td>Designation..............................</td>
                                            </tr>
                                            <tr>
                                                <td class="bank">Identification copies verified against originals</td>
                                                <td class="bank"></td>
                                                <td class="bank">K.Y.C Form</td>
                                                <td class="bank"></td>
                                                <td>Date.....................................</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 100%;">
                                <tr>
                                    <td>Dear Sir/Madam</td>
                                </tr>
                                <tr>
                                    <td>Please,arrange to provide me with following services availed by your Bank.</td>
                                </tr>
                                <tr>
                                    <td class="space-td">
                                        <table class="table-width">
                                            <tr>
                                                <td>
                                                    <label>1.</label>
                                                    <input disabled @if($account_detail['debit_card'] == 1) {{ 'checked' }} @endif type="checkbox">
                                                    <label>Visa Debit Card</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="space-td">
                                        <table class="table-width">
                                            <tr>
                                                <td>
                                                    <label>2.</label>
                                                    <input disabled @if($account_detail['mobile_banking_services'] == 1) {{'checked'}} @endif type="checkbox">
                                                    <label>Mobile Banking</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label class="info">*(Balance Inquiry, Deposit/Withdrawal Alert, Statement, Fund Transfer,
                                                        Utility/Merchant Payments, Load E-Sewa, etc)</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="space-td">
                                        <table>
                                            <tr>
                                                <td>
                                                    <label>3.</label>
                                                    <label class="internet">Internet Banking</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label class="info">*(Account information, Statement, Fund Transfer, Utility Payments, Load
                                                        E-Sewa,
                                                        etc)</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="info">
                                                    <input disabled @if($account_detail['is_internet_banking_password']==1){{ 'checked' }} @endif type="checkbox">
                                                    <label>Viewing Rights only</label>
                        
                                                    <input disabled @if($account_detail['is_internet_banking_password']==1) {{ 'checked' }} @endif type="checkbox" class="transaction">
                                                    <label>Transaction Rights</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="space-td">
                                        <table class="table-width">
                                            <tr>
                                                <td>
                                                    <label>4.</label>
                                                    <input type="checkbox">
                                                    <label>Locker Facilities</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label class="info">*(Availed from selected branches, Charge based on Locker Size)</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                        
                                <tr>
                                    <td class="space-td">
                                        <table class="table-width">
                                            <tr>
                                                <td>
                                                    <label>5.</label>
                                                    <input disabled @if($account_detail['ntc'] == 1) {{ 'checked' }} @endif  type="checkbox">
                                                    <label>NTC(Prepaid/Postpaid/ADSL/PSTN)</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="info">
                                                    <label>(Bill Payment Service)</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="space-td">
                                        <table class="table-width">
                                            <tr>
                                                <td>
                                                    <label>6.</label>
                                                    <input disabled @if($account_detail['e_statement'] == 1){{ 'checked'}} @endif type="checkbox">
                                                    <label>E-Statement</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="space-td">
                                        <table class="table-width">
                                            <tr>
                                                <td>
                                                    <label>7.</label>
                                                    <input disabled @if($account_detail['standing_instruction'] == 1){{ 'checked' }} @endif   type="checkbox">
                                                    <label>Standing Instruction Service</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label class="info">*(Monthly NTC PSTN, NTC Postpaid and NTC ADSL bill payment Facility by
                                                        Bank)</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="space-td">
                                        <table class="table-width">
                                            <tr>
                                                <td>
                                                    <label>8.</label>
                                                    <input disabled @if($account_detail['other_service'] == 1){{ 'checked' }} @endif type="checkbox">
                                                    <label>Other Services</label>
                                                </td>
                                            </tr>
                                            @if($account_detail['other_service'] == 1)
                                                <tr>
                                                    <td class="info">
                                                        <label><u>&nbsp;&nbsp;&nbsp;{{ strtoupper($account_detail['otherservice_name']) }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></label>
                                                    </td>
                                                </tr>
                                             @endif
                                        </table>
                                    </td>
                                </tr>
                                <!------------------ Location Map ------------------>
                                <tr>
                                    <td class="space-td">
                                        <table border="1" class="table-width map">
                                            <tr>
                                                <td>
                                                    <div id="map"></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="notes">-</label>
                                        <label>I/We have read your Bank's conditions for conduct of Account and I/We agree to abide by the
                                            Bank's rules.</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="notes">-</label>
                                        <label>I/We agree to comply with the prevalent rules of the Bank in force from time to time regarding
                                            conduct of<br> the Account and agree to abide by them.</label>
                        
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="notes">-</label>
                                        <label>All required documents are enclosed herewith.</label>
                                    </td>
                        
                                </tr>
                                <tr>
                                    <td>
                                        <table border="1" class="table-width table-sign sign">
                                            <tr>
                                                <th class="sign">Authorised Signature</th>
                                                <th class="table-sign-td sign">Name</th>
                                            </tr>
                                            <tr>
                                                <td class="sign">1.</td>
                                                <td class="table-sign-td sign"></td>
                                            </tr>
                                            <tr>
                                                <td class="sign">2.</td>
                                                <td class="table-sign-td sign"></td>
                                            </tr>
                                            <tr>
                                                <td class="sign">3.</td>
                                                <td class="table-sign-td sign"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:right; padding-top: 70px;"><span style="border-top: 2px solid #8bceb6; ">Authorized
                                            Signature(s)</span></td>
                        
                                </tr>
                            </table>
                        </section>
                    <script src="{{ URL::to('js/JqueryUI.js') }}"></script>
                    <script>
                        var lati_s = '{{ $account_detail['latitude'] }}';
                        var longi_s = '{{ $account_detail['longitude'] }}';
                        console.log(lati_s, longi_s, typeof lati_s, typeof longi_s);
                        var lati= parseFloat(lati_s);
                        var longi=parseFloat(longi_s);
                        console.log(lati, longi, typeof lati, typeof longi);
                          function initMap(){
                          
                            uluru = {lat: lati, lng: longi};
                            var map = new google.maps.Map(document.getElementById('map'), {
                              zoom: 19,
                              center: uluru
                              
                              
                            });
                            var marker = new google.maps.Marker({
                              position: uluru,
                              map: map
                            });
                        }
                        $(function(){
        google.maps.event.trigger(map, "resize");
    });    
    
                    </script>
                <script async defer  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8xUqFu-Te4JAwIsNP25J6b59cfxbjgyQ&callback=initMap&libraries=places">
                    </script>
                 