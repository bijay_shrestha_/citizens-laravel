@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Account Details		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Account Details</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						{{-- <a href="{{ route('admin.account_details.create') }}" class="btn bg-green waves-effect"  title="create">Create</a> --}}
					</div>
                              @if ($message = Session::get('success'))
                                    <div class="btn btn-success">{{$message}}</div>
                              @endif
					<!-- /.box-header -->
					<div class="box-body">
						<table id="account_detail-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
                                                <th>Photo</th>
                                                <th>Reference Number</th>
                                                <th>Account Name</th>
                                                <th>Type</th>
                                                <th>Account Category</th>
                                                <th>Account Type</th>
                                                <th>Currency</th>
                                                <th>Account Status</th>
								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
            var account_detail_status = '{{$status}}';
		var dataTable; 
		//var site_url = window.location.href;
            var site_url = "{{url('/admin/account_details/')}}";
		$(function(){
			dataTable = $('#account_detail-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.account_details.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}', 'status': account_detail_status } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
                              { data: function (data, b,c, table){
                                    var imagefile = '';
                                    if (data.photo) {
                                          imagefile += '<img src="{{url("/uploads/account_details/")}}/'+data.photo+'">';
                                    }
                                    return imagefile;

                              }, name:'photo', searchable: false},
                              { data: "reference_code",name: "reference_code"},                             

                              {data:function(data){
                                    if (data.account_name) {
                                        var names = '';
                                        var JSONString = data.account_name;
                                        var JSONObject = JSON.parse(JSONString);
                                        if (JSONObject["account_name"].length > 0) {
                                        for (var i =0; i< JSONObject["account_name"].length; i++) {
                                            names += JSONObject["account_name"][i] + ' ';
                                        }
                                          return names;
                                        } else {
                                          return '';
                                        }
                                    } else {
                                          return '';
                                    }
                              }, name:'account_name',},

                              { data: "type",name: "type"},
                              { data: "account_category",name: "account_category"},
                              { data: "account_type",name: "account_type"},
                              { data: "currency",name: "currency"},
                              { data: "account_status",name: "account_status"},
					{ data: function(data,b,c,table) { 
					var buttons = '';
					buttons += "<a class='btn btn-primary waves-effect' href='"+site_url+"/form_view/"+data.a_id+"' type='button' ><i class='fa fa-file-text-o'></i>form</a>&nbsp"; 

					buttons += "<a class='btn bg-green waves-effect' href='"+site_url+"/detail/"+data.a_id+"' type='button' ><i class='fa fa-eye'></i> View</a>&nbsp"; 

					
					buttons += "<a href='"+site_url+"/delete/"+data.a_id+"' class='btn bg-red waves-effect' ><i class='fa fa-trash'></i>  Delete</a>";
					
					buttons += "<a href='"+site_url+"/kyc/"+data.a_id+"' class='btn btn-primary' ><i class='fa fa-file-text-o'></i>KYC</a> ";
					
					return buttons;
					}, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
