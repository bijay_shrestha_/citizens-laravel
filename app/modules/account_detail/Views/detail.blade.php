@extends('admin.layout.main')
@section('content')
<style>
                               #map {
                                   margin: auto;
                                height: 400px;
                                width: 800px;
                               }
                            </style>
    <section class="content-header">
        <h1>
           Account Details   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.account_details') }}">Account Details</a></li>
        </ol>
       
    </section>

    <section class="content">
        <div class="box box-primary">
            <div class="box-header">
                <a href="{{ route('admin.accounts.create') }}" class="btn bg-green waves-effect"  title="create">Excel
            </a>
            </div>
        </div>
                  <div class="row">
            <div class="col-xs-12">
              <div class="box">
                   <div class="box-body overflow-horizontal">
                       
                       <table id="example2" class="table table-bordered table-striped table-hover" >
                           <tbody>
                                 
                                 
                                  <tr>
                                      <th>Branch</th>
                                    <td>{{$account_detail['branch']}}</td>
                                  </tr>
                                  <tr>
                                      <th>Type</th>
                                    <td>{{$account_detail['type']}}</td>
                                  </tr>
                                    <tr>
                                      <th>Account Category</th>
                                    <td>{{$account_detail['account_category']}}</td>
                                  </tr>
                                  
                                  <tr>
                                      <th>Types of Individual</th>
                                    <td>{{$account_detail['types_of_individual']}}</td>
                                  </tr>
                                  <tr>
                                      <th>Currency</th>
                                    <td>{{$account_detail['currency']}}</td>
                                  </tr>
                                  
                                   <tr>
                                      <th>Account Type</th>
                                      <td>{{$account_detail['account_type']}}</td>
                                  </tr>
                                  @php
                                        $account_name = json_decode($account_detail['account_name'],true);
                                          
                                        $father_name = json_decode($account_detail['father_name'],true);
                                        $grandfather_name = json_decode($account_detail['grandfather_name'],true);
                                        $spouse_name = json_decode($account_detail['spouse_name'],true);
                                        
                                        $account_number = json_decode($account_detail['account_number'],true);
                                        
                                        $dob = json_decode($account_detail['dob'],true);
                                        $dob_nepali = json_decode($account_detail['dob_nepali'],true);
                                        $occupation = json_decode($account_detail['occupation'],true);

                                        $gender = json_decode($account_detail['gender'],true);
                                        $nationality = json_decode($account_detail['nationality'],true);
                                        $passport_no = json_decode($account_detail['passport_no'],true);
                                        $citizenship_no = json_decode($account_detail['citizenship_no'],true);
                                        $nrn_card = json_decode($account_detail['nrn_card'],true);
                                        $expiry_date = json_decode($account_detail['expiry_date'],true);
                                        $foreign_address = json_decode($account_detail['foreign_address'],true);
                                        $country = json_decode($account_detail['country'],true);
                                        $contact_no = json_decode($account_detail['contact_no'],true);
                                        $type_of_visa = json_decode($account_detail['type_of_visa'],true);
                                        $visa_expiry_date = json_decode($account_detail['visa_expiry_date'],true);
                                        $city_state = json_decode($account_detail['city_state'],true);
                                        $birth_certificate = json_decode($account_detail['birth_certificate'],true);
                                        $other_id = json_decode($account_detail['other_id'],true);
                                        
                                        $place_of_issue = json_decode($account_detail['place_of_issue'],true);
                                        $date_of_issue = json_decode($account_detail['date_of_issue'],true);
                                        $date_of_issue_bs = json_decode($account_detail['date_of_issue_bs'],true);
                                        $marital_status = json_decode($account_detail['marital_status'],true);
                                        $email = json_decode($account_detail['email'],true);
                                        $pan_number = json_decode($account_detail['pan_number'],true);
                                        $phone_number = json_decode($account_detail['phone_number'],true);
                                        $mobile = json_decode($account_detail['mobile'],true);
                                        $fax_number = json_decode($account_detail['fax_number'],true);
                                        $pox_box = json_decode($account_detail['pox_box'],true);
                                        $permanent_address_house_no = json_decode($account_detail['permanent_address_house_no'],true);
                                        $permanent_address_vdc = json_decode($account_detail['permanent_address_vdc'],true);
                                        $permanent_address_ward_number = json_decode($account_detail['permanent_address_ward_number'],true);
                                        $permanent_address_street = json_decode($account_detail['permanent_address_street'],true);
                                        $permanent_address_district = json_decode($account_detail['permanent_address_district'],true);
                                        $correspondence_address_house_no = json_decode($account_detail['correspondence_address_house_no'],true);
                                        $correspondence_address_vdc = json_decode($account_detail['correspondence_address_vdc'],true);
                                        $correspondence_address_ward_number = json_decode($account_detail['correspondence_address_ward_number'],true);
                                        $correspondence_address_ward_number = json_decode($account_detail['correspondence_address_ward_number'],true);
                                        $correspondence_address_district = json_decode($account_detail['correspondence_address_district'],true);
                                        $correspondence_address_street = json_decode($account_detail['correspondence_address_street'],true);

                                       $i=0; 

                                  @endphp

                                  @foreach ($account_name['account_name'] as $name) 

                                   <tr>
                                      <th colspan="2" style="background: gray; color: white; font-size: 20px"><center>Account Requester Personal Details  {{$a = $i+1}}</center></th>
                                  </tr>
                                    <tr>
                                        <th>Account Requester Name</th>
                                        <td>{{ strtoupper($name) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Account Requester Father Name</th>
                                        <td>{{ strtoupper($father_name['father_name'][$i]) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Account Requester Grand Father Name</th>
                                        <td>{{ strtoupper($grandfather_name['grandfather_name'][$i]) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Account Requester Grand Spouse Name</th>
                                        <td>{{ strtoupper($spouse_name['spouse_name'][$i]) }}</td>
                                    </tr>
                                    
                                    <tr>
                                      <th>Account Number</th>
                                    <td>{{ $account_number['account_number'][$i] }}</td>
                                    </tr>
                                    
                                    <tr>
                                        <th>Account Requester DOB</th>
                                        <td>{{ $dob['dob'][$i] }}</td>
                                    </tr>
                                 
                                    <tr>
                                        <th>Account Requester DOB Nepali</th>
                                        <td>{{$dob_nepali['dob_nepali'][$i]}}</td>
                                    </tr>
                                                                      <tr>
                                        <th>Account Requester Occupation</th>
                                        @if(is_string($occupation['occupation'][$i]) && is_array(json_decode($occupation['occupation'][$i], true)) &&  (json_last_error() == JSON_ERROR_NONE))
                                          @php 
                                            $occupation_array = json_decode($occupation['occupation'][$i],true);
                                            
                                          @endphp
                                          <td>
                                            <table >
                                                <th>Company Name</th>
                                                <th>Company Address</th>
                                                <th>Company Position</th>
                                                    <tr>
                                                        <td>{{ $occupation_array['company_name'] }}</td>
                                                        <td>{{ $occupation_array['company_address'] }}</td>
                                                        <td>{{ $occupation_array['company_position'] }}</td>
                                                    </tr>
                                            </table>        
                                          </td>   
                                        @else

                                        <td>{{ strtoupper($occupation['occupation'][$i]) }}</td>
                                        @endif
                                    </tr>

                                    <tr>
                                        <th>Account Requester Nationality</th>
                                        <td>{{$nationality['nationality'][$i] }}</td>
                                    </tr>
                                   
                                   @if($passport_no['passport_no'][$i]!="")
                                    <tr>
                                        <th>Account Requester Passport No</th>
                                        <td>{{$passport_no['passport_no'][$i] }}</td>
                                    </tr>
                                    @endif
                                    @if($citizenship_no['citizenship_no'][$i]!="")
                                    <tr>
                                        <th>Account Requester Citizen No</th>
                                        <td>{{$citizenship_no['citizenship_no'][$i] }}</td>
                                    </tr>
                                    @endif
                                    
                                   
                                    @if($expiry_date['expiry_date'][$i]!="")
                                      <tr>
                                        <th>Account Requester Expiry Date</th>
                                        <td>{{$expiry_date['expiry_date'][$i] }}</td>
                                    </tr>
                                    @endif
                                    @if($nrn_card['nrn_card'][$i]!="")
                                     <tr>
                                        <th>Account Requester NRN Card</th>
                                        <td>{{$nrn_card['nrn_card'][$i] }}</td>
                                    </tr>
                                    @endif
                                   
                                    
                                    @if($country['country'][$i]!="")
                                    <tr>
                                        <th>Account Requester Country</th>
                                        <td>{{$country['country'][$i] }}</td>
                                    </tr>
                                    @endif

                                    @if($foreign_address['foreign_address'][$i]!="")
                                    <tr>
                                        <th>Account Requester Foreign Address</th>
                                        <td>{{$foreign_address['foreign_address'][$i]}}</td>
                                    </tr>
                                    @endif
                                    
                                    @if($contact_no['contact_no'][$i]!="")
                                    <tr>
                                        <th>Account Requester Contact No.</th>
                                        <td>{{$contact_no['contact_no'][$i]}}</td>
                                    </tr>
                                    @endif
                                    
                                    @if($type_of_visa['type_of_visa'][$i]!="")
                                    <tr>
                                        <th>Account Requester Type of Visa</th>
                                        <td>{{$type_of_visa['type_of_visa'][$i]}}></td>
                                    </tr>
                                    @endif
                                    
                                    @if($visa_expiry_date['visa_expiry_date'][$i]!="")
                                    <tr>
                                        <th>Account Requester Visa Expiry Date</th>
                                        <td>{{$visa_expiry_date['visa_expiry_date'][$i]}}</td>
                                    </tr>
                                     @endif
                                    @if($city_state['city_state'][$i]!="")
                                     <tr>
                                        <th>Account Requester City Date</th>
                                        <td>{{$city_state['city_state'][$i]}}</td>
                                    </tr>
                                     @endif
                                    <tr>
                                        <th>Account Requester Place of Issue</th>
                                        <td>{{ $place_of_issue['place_of_issue'][$i] }}</td>
                                    </tr>
                                    <tr>
                                        <th>Account Requester Date of Issue(AD)</th>
                                        <td>{{ $date_of_issue['date_of_issue'][$i] }}</td>
                                    </tr>
                                    <tr>
                                        <th>Account Requester Date of Issue(BS)</th>
                                        <td>{{ $date_of_issue_bs['date_of_issue_bs'][$i] }}</td>
                                    </tr>
                                    <tr>
                                        <th>Account Requester Marital Issue</th>
                                        <td>{{ $marital_status['marital_status'][$i] }}</td>
                                    </tr><tr>
                                        <th>Account Requester Email</th>
                                        <td>{{ $email['email'][$i] }}</td>
                                    </tr><tr>
                                    </tr><tr>
                                        <th>Account Requester PAN Number</th>
                                        <td>{{ $pan_number['pan_number'][$i] }}</td>
                                    </tr><tr>
                                        <th>Account Requester Phone</th>
                                        <td>{{ $phone_number['phone_number'][$i] }}</td>
                                    </tr><tr>
                                        <th>Account Requester Mobile</th>
                                        <td>{{ $mobile['mobile'][$i] }}</td>
                                    </tr><tr>
                                        <th>Account Requester Permanent Address House No.</th>
                                        <td>{{ $permanent_address_house_no['permanent_address_house_no'][$i] }}</td>
                                    </tr><tr>
                                        <th>Account Requester VDC/Muncipality</th>
                                        <td>{{ $permanent_address_vdc['permanent_address_vdc'][$i] }}</td>
                                    </tr><tr>
                                        <th>Account Requester Ward No.</th>
                                        <td>{{ $permanent_address_ward_number['permanent_address_ward_number'][$i] }}</td>
                                    </tr><tr>
                                        <th>Account Requester Street</th>
                                        <td>{{ $permanent_address_street['permanent_address_street'][$i] }}</td>
                                    </tr>
                                    <tr>
                                        <th>Account Requester District</th>
                                        <td>{{ $permanent_address_district['permanent_address_district'][$i] }}</td>
                                    </tr>

                                    @if(isset($correspondence_address_house_no))
                                    <tr>
                                        <th>Account Requester Corresponding House No</th>
                                        <td>{{ $correspondence_address_house_no['correspondence_address_house_no'][$i] }}</td>
                                    </tr>
                                    <tr>
                                        <th>Account Requester Corresponding  VDC/Municipality</th>
                                        <td>{{ $correspondence_address_vdc['correspondence_address_vdc'][$i] }}</td>
                                    </tr>
                                    <tr>
                                        <th>Account Requester Corresponding Ward No.</th>
                                        <td>{{ $correspondence_address_ward_number['correspondence_address_ward_number'][$i] }}</td>
                                    </tr>
                                    <tr>
                                        <th>Account Requester Corresponding Street</th>
                                        <td>{{ $correspondence_address_street['correspondence_address_street'][$i] }}</td>
                                    </tr>
                                    <tr>
                                        <th>Account Requester Corresponding District</th>
                                        <td>{{ $correspondence_address_district['correspondence_address_district'][$i] }}</td>
                                    </tr>
                                    @endif

                                    @php 
                                      $i++; 
                                    @endphp
                                  @endforeach
                                  @if(isset($account_detail['age_minor']))
                                            
                                    <tr></tr>
                            </tbody>
                        </table>
                        <table id="example3" class="table table-bordered table-hover">
                           <tbody>
                                <tr><th colspan="2" style="background: gray; color: white; font-size: 20px">Minor Information</th></tr>
                                    <tr>
                                        <td>Age of Minor</td>
                                        <td>{{ $account_detail['age_minor'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>Name of Guardian</td>
                                        <td>{{ $account_detail['name_of_guardian'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>Minor Relation</td>
                                        <td>{{ $account_detail['relationship_minor'] }}</td>
                                    </tr>
                                  @endif
                                  
                                  <tr></tr><br>
                                  
                           </tbody>
                        </table>           
                        <table  id="example3" class="table table-bordered table-hover">
                          <tbody>
                            
                            @php
                              $images = json_decode($account_detail['images'],true);
                              $citizenship_fronts = json_decode($account_detail['citizenship_fronts'],true);
                              $citizenship_backs = json_decode($account_detail['citizenship_backs'],true);
                              $i=0;
                            @endphp

                            @foreach ($account_name['account_name'] as $name)
                                  <tr>
                                       <th colspan="2">Image {{$a = $i+1}}</th>
                                       @if($images['images'][$i] != "" && file_exists( public_path() . '/uploads/account_details/' . $images['images'][$i]))
                                           @php
                                            $url= url("uploads/account_details/{$images['images'][$i]}");
                                           @endphp
                                        @else
                                          @php
                                            $url= url("uploads/account_details/default.png");
                                          @endphp
                                        @endif
                                       <td><a href="{{$url}}" target="_blank"><img src="{{$url}}" width="200px;" alt='no image'></a></td>
                                   </tr>
                                   <tr>
                                           <th >Identity Image {{$a = $i+1 }}</th>
                               
                                      @if(isset($citizenship_fronts['citizenship_fronts'][$i]) && !empty($citizenship_fronts['citizenship_fronts'][$i]) && file_exists( public_path() . '/uploads/account_details/' . $citizenship_fronts['citizenship_fronts'][$i]))
                                          @php
                                            $file = array();
                                           
                                            $file = explode('.',$citizenship_fronts['citizenship_fronts'][$i]);
                                            
                                             $front_len = count($file);
                                          @endphp  
                                            @if($file[$front_len-1] == 'jpg' || $file[$front_len-1] == 'jpeg' || $file[$front_len-1] == 'png' || $file[$front_len-1] == 'JPG' || $file[$front_len-1] == 'JPEG' || $file[$front_len-1] == 'PNG')
                                              @php
                                                $url_fronts= url("uploads/account_details/{$citizenship_fronts['citizenship_fronts'][$i]}");
                                              @endphp  
                                                <td>
                                                 <a href="{{ $url_fronts}}" target="_blank">    <img src="{{ $url_fronts}}"  width="200px;" alt='no image'></a>
                                               </td>
                                            @else
                                              @php
                                                $url_fronts=url("uploads/account_details/{$citizenship_fronts['citizenship_fronts'][$i]}");
                                              @endphp
                                                <td>
                                                 <a href="{{ $url_fronts}}" target="_blank"> <img src="{{ url('assets/images/pdf.png') }}" width="200px;" alt='no image'>
                                                   </a> 
                                               </td>
                                           @endif
                                     
                                      @else
                                          @php 
                                            $url_fronts= url("uploads/account_details/default.png");
                                          @endphp
                                             <td><a href="{{ $url_fronts}}" target="_blank"> 
                                           <img src="{{ $url_fronts}}"  width="200px;" alt='no image'></a>
                                        </td> 
                                      @endif
                                        
                                      @if(isset($citizenship_backs['citizenship_backs'][$i])  && !empty($citizenship_backs['citizenship_backs'][$i]) && file_exists( public_path() . '/uploads/account_details/' . $citizenship_backs['citizenship_backs'][$i]))
                                       
                                           
                                           @php
                                            $back = array();
                                            $back = explode('.',$citizenship_backs['citizenship_backs'][$i]);
                                            $back_len = count($back);
                                            @endphp
                                          @if($back[$back_len-1] == 'jpg' || $back[$back_len-1] == 'jpeg' || $back[$back_len-1] == 'png' || $back[$back_len-1] == 'JPG' || $back[$back_len-1] == 'JPEG' || $back[$back_len-1] == 'PNG')
                                                @php
                                                 $url_backs = url("uploads/account_details/{$citizenship_backs['citizenship_backs'][$i]}"); 
                                                @endphp
                                                
                                                <td><a href="{{ $url_backs}}" target="_blank"> 
                                                   <img src="{{ $url_backs }}" width="200px;" alt='no image'>
                                                   </a>
                                               </td> 
                                          @else 
                                               @php
                                             //$url_backs = base_url("assets/images/pdf.png");
                                                $url_backs = url("uploads/account_details/{$citizenship_backs['citizenship_backs'][$i]}");
                                                @endphp
                                                 <td>
                                                 <a href="{{$url_backs}}" target="blank"> <img src="{{ url('assets/images/pdf.png') }}" width="200px;" alt='no image'>
                                                   </a> 
                                               </td>
                                           @endif
                                      @else
                                            @php
                                              $url_backs = url("uploads/account_details/default.png");
                                            @endphp
                                             <td> <a href="{{$url_backs}}" target="blank">
                                           <img src="{{ $url_backs }}" width="200px;" alt='no image'>
                                           </a>
                                       </td>
                                      @endif
                                   </tr>
                                   @php 
                                     $i++; 
                                   @endphp
                            @endforeach
                          </tbody>

                        </table>

                        <table id="example3" class="table table-bordered table-hover">
                           <tbody>
                                <tr>
                                      <th colspan="2" style="background: gray; color: white; font-size: 20px"><CENTER>Requester Location Detail </CENTER></th>
                                </tr>
                                <tr colspan="2">
                                    <div id="map"></div>
                                </tr>
                               
                            </tbody>
                        </table>    

                       <table id="example3" class="table table-bordered table-hover">
                           <tbody>
                                @php   
                                 $mr_miss = json_decode($account_detail['mr_miss'],true);
                                  $depositer_name = json_decode($account_detail['depositer_name'],true);
                                  $relationship = json_decode($account_detail['relationship'],true);
                                  $nominee_images = json_decode($account_detail['nominee_images'],true);
                                  $images = json_decode($account_detail['images'],true);
                                  $nominee_citizenship_fronts = json_decode($account_detail['nominee_citizenship_fronts'],true);
                                  $citizenship_fronts = json_decode($account_detail['citizenship_fronts'],true);
                                  $nominee_citizenship_backs = json_decode($account_detail['nominee_citizenship_backs'],true);
                                  $citizenship_backs = json_decode($account_detail['citizenship_backs'],true);
                                  $i=0; 
                                @endphp
                          @if (isset($depositer_name['depositer_name']))
                            @foreach($depositer_name['depositer_name'] as $name)   
                               <tr>
                                      <th colspan="3" style="background: gray; color: white; font-size: 20px"><center>Additional Information and Images for Verification {{ $a = $i+1 }}</center> </th>
                                      
                               </tr>
                               <tr>
                                   <th colspan="2">Depositer Name </th>
                                   <td>{{ strtoupper($mr_miss['mr_miss'][$i].' '.$name) }}</td>
                               </tr>
                               <tr>
                                   <th colspan="2">Relation</th>
                                   <td>{{ $relationship['relationship'][$i] }}</td>
                               </tr>
                               
                               
                               
                               
                               
                               <tr>
                                   <th colspan="2">Image(Nominee) </th>
                                    @if($nominee_images['nominee_images'][$i]!="" && file_exists( public_path() . '/uploads/account_details/' . $nominee_images['nominee_images'][$i]))
                                      @php
                                        $url= url("uploads/account_details/{$nominee_images['nominee_images'][$i]}");
                                      @endphp
                                    @else
                                      @php
                                        $url= url("uploads/account_details/default.png");
                                      @endphp
                                    @endif
                                    
                                   <td><a href="{{$url}}" target="_blank"><img src="{{ $url }}" width="200px;" alt='no image'><td></td>
                               </tr>
                               <tr>
                                   <th >Identity Image(Nominee) </th>
                                   @if(isset($nominee_citizenship_fronts['nominee_citizenship_fronts'][$i]) && !empty($nominee_citizenship_fronts['nominee_citizenship_fronts'][$i]) && file_exists( public_path() . '/uploads/account_details/' . $nominee_citizenship_fronts['nominee_citizenship_fronts'][$i]))
                                   
                                      @php
                                        $file = array();
                                        $file = explode('.',$nominee_citizenship_fronts['nominee_citizenship_fronts'][$i]);
                                        $len = count($file);
                                      @endphp 

                                        @if($file[$len-1] == 'jpg' || $file[$len-1] == 'jpeg' || $file[$len-1] == 'png' || $file[$len-1] == 'JPG' || $file[$len-1] == 'JPEG' || $file[$len-1] == 'PNG')
                                          @php
                                            $url_fronts= url("uploads/account_details/{$nominee_citizenship_fronts['nominee_citizenship_fronts'][$i]}");
                                          @endphp
                                        @else
                                          @php
                                            $url_fronts = base_url("assets/images/pdf.png");
                                          @endphp
                                        @endif
                                    @else
                                      @php
                                        $url_fronts= url("uploads/account_details/default.png");
                                      @endphp
                                    @endif
                                   <td><a href="{{ $url_fronts}}" target="_blank"><img src="{{ $url_fronts }}" width="200px;" alt='no image'></a></td>  
                                       
                                   @if(isset($nominee_citizenship_backs['nominee_citizenship_backs'][$i])  && !empty($nominee_citizenship_backs['nominee_citizenship_backs'][$i])  && file_exists( public_path() . '/uploads/account_details/' . $nominee_citizenship_backs['nominee_citizenship_backs'][$i]) )
                                      @php
                                       $back = array();
                                        $back = explode('.',$nominee_citizenship_backs['nominee_citizenship_backs'][$i]);
                                        $back_len = count($back);
                                        // print_r($back);
                                        // exit;
                                      @endphp  
                                        @if($back[$back_len-1] == 'jpg' || $back[$back_len-1] == 'jpeg' || $back[$back_len-1] == 'png' || $back[$back_len-1] == 'JPG' || $back[$back_len-1] == 'JPEG' || $back[$back_len-1] == 'PNG')
                                          @php
                                            $url_backs= url("uploads/account_details/{$nominee_citizenship_backs['nominee_citizenship_backs'][$i]}");
                                          @endphp
                                        @else
                                          @php
                                            $url_backs =  url("assets/images/pdf.png");
                                          @endphp
                                        @endif
                                    @else
                                      @php
                                        $url_backs= url("uploads/account_details/default.png");
                                      @endphp
                                    @endif
                                   <td><a href="{{$url_backs}}" target="_blank"><img src="{{ $url_backs }}" width="200px;" alt='no image'></a></td>
                               </tr>
                                  @php 
                                    $i++; 
                                  @endphp                      
                            @endforeach 
                          @endif
                            </tbody>
                            <!-- endforeach -->
                          </table>
                       <br>

                       <table id="example3" class="table table-bordered table-hover">
                           <tbody>
                                 <tr>
                                      <th colspan="2" style="background: gray; color: white; font-size: 20px"><center>Services Selected</center> </th>
                                      
                                  </tr>
                                  <tr>
                                      <th>Debit Card Service</th>
                                      <td>{{ ($account_detail['debit_card'] == 1)?'Yes':'No' }}</td>
                                  </tr>
                                  <tr>
                                      <th>Mobile Banking Service</th>
                                      <td>{{ ($account_detail['mobile_banking_services'] == 1)?'Yes':'No' }}</td>
                                  </tr>
                                  @if($account_detail['mobile_banking_services'] == 1)
                                  <tr>
                                      <th>Do you want transaction password or not?</th>
                                      @if($account_detail['is_banking_password']==1)
                                        <td>Yes</td>
                                      @else
                                        <td>No</td>
                                      @endif
                                     
                                  </tr>
                                  @endif
                                  
                                  <tr>
                                      <th>Internet Banking Service</th>
                                      <td>{{ ($account_detail['internet_banking_services'] == 1)?'Yes':'No' }}</td>
                                  </tr>
                                  
                                   @if($account_detail['internet_banking_services'] == 1)
                                  <tr>
                                      <th>Do you want transaction password or not?</th>
                                      @if($account_detail['is_internet_banking_password']==1)
                                        <td>Yes</td>
                                      @else
                                        <td>No</td>
                                      @endif
                                     
                                  </tr>
                                    @endif
                                  <tr>
                                      <th>NTC(Prepaid/Postpaid/ADSL/PSTN)</th>
                                      <td>{{ ($account_detail['ntc'] == 1)?'Yes':'No' }}</td>
                                  </tr>
                                  <tr>
                                      <th>E-Statement Service</th>
                                      <td>{{ ($account_detail['e_statement'] == 1)?'Yes':'No' }}</td>
                                  </tr><tr>
                                      <th>Standing Instruction Service</th>
                                      <td>{{ ($account_detail['standing_instruction'] == 1)?'Yes':'No' }}</td>
                                  </tr>
                                  <tr>
                                      <th>Bancassurance</th>
                                      <td>{{ ($account_detail['bancassurance'] == 1)?'Yes':'No' }}</td>
                                  </tr>
                                  <tr>
                                      <th>Other Service</th>
                                      <td>{{ ($account_detail['other_service'] == 1)?'Yes':'No' }}</td>
                                  </tr>
                                @if($account_detail['other_service'] == 1)
                                  <tr>
                                      <th>Other Service</th>
                                     <td>{{ strtoupper($account_detail['otherservice_name']) }}</td>
                                </tr>
                                @endif
                               
                           </tbody>
                           
                       </table>
                       
                       <table id="kyc" class="table table-bordered table-hover">
                           <tbody>
                                 <tr>
                                      <th colspan="2" style="background: gray; color: white; font-size: 20px"><center>KYC</center> </th>
                                      
                                  </tr>
                                  <tr>
                                      <th>High Risk Customer</th>
                                        @if($account_detail['is_high_risk_customer']==1)
                                          <td>Yes</td>
                                        @else
                                          <td>No</td>
                                        @endif
                                  </tr>
                                  
                                  {{--
                                  <tr>
                                    <th>Are you NRN?</th>
                                      @if($account_detail['if_nrn']==1)
                                        <td>Yes</td>
                                      @else
                                        <td>No</td>
                                      @endif
                                 </tr>
                                 --}}

                                @if($account_detail['if_beneficial_owner']==1)
                                   <tr>
                                      <th>Beneficial Owner</th>
                                        <td>Yes</td>
                                   </tr>
                                  
                                   <tr>
                                      <th>Beneficial Owner Name</th>
                                      
                                      <td>{{ strtoupper($account_detail['mother_name']) }}</td>
                                     
                                    
                                      
                                  </tr>
                                  
                                  <tr>
                                      <th>Beneficial Relation</th>
                                      
                                      <td>{{ strtoupper($account_detail['beneficial_relation']) }}</td>
                                     
                                  </tr>
                                  
                                  
                                   <tr>
                                      <th>Beneficial Citizenship No.</th>
                                      
                                      <td>{{ strtoupper($account_detail['beneficial_citizen']) }}</td>
                                     
                                  </tr>
                                  
                                  <tr>
                                      <th>Beneficial Address</th>
                                      
                                      <td>{{ strtoupper($account_detail['beneficial_address']) }}</td>
                                     
                                  </tr>
                                  
                                  <tr>
                                      <th>Beneficial Contact No.</th>
                                      
                                      <td>{{ strtoupper($account_detail['beneficial_contact']) }}</td>
                                     
                                  </tr>

                                @else
                                    <tr>
                                      <th>Beneficial Owner</th>
                                      
                                      <td>No</td>
                                      
                                  </tr>
                                @endif
                                  <tr>
                                      <th>Mother's Name</th>
                                      <td>{{ strtoupper($account_detail['mother_name']) }}</td>
                                  </tr>
                                  <tr>
                                      <th>Grandmother's Name</th>
                                      <td>{{ strtoupper($account_detail['grandmother_name']) }}</td>
                                  </tr>
                                  
                                  @php   
                                      $daughter = json_decode($account_detail['daughter_name'],true);
                                      $i=0;
                                  @endphp 
                                    @if(!empty($daughter['daughter_name']))
                                      @foreach ($daughter['daughter_name'] as $name)    
                                    
                                    
                                        <tr>
                                            <th>Daughter's Name</th>
                                            <td>{{ strtoupper($daughter['daughter_name'][$i])}}</td>
                                        </tr>
                                        @php 
                                          $i++;   
                                        @endphp 
                                      @endforeach 
                                    @endif                    
                                  
                                  @php 
                                      $son = json_decode($account_detail['son_name'],true);
                                      $i=0; 
                                  @endphp
                                    @if(!empty($son['son_name']))
                                      @foreach($son['son_name'] as $name)    
                                  
                                        <tr>
                                            <th>Son's Name</th>
                                            <td>{{strtoupper($son['son_name'][$i]) }}</td>
                                        </tr>
                                        @php 
                                          $i++;   
                                        @endphp 
                                      @endforeach  
                                    @endif 
                                    
                                    
                                  @php  
                                      $daughter_in_law = json_decode($account_detail['daughter_in_law_name'],true);
                                      $i=0;
                                  @endphp
                                    @if(!empty($daughter_in_law['daughter_in_law_name']))
                                      @foreach($daughter_in_law['daughter_in_law_name'] as $name)   
                                  
                                          <tr>
                                              <th>Daughter-in-law's Name</th>
                                              <td>{{ strtoupper($daughter_in_law['daughter_in_law_name'][$i]) }}</td>
                                          </tr>
                                              
                                        @php 
                                          $i++;   
                                        @endphp 
                                      @endforeach  
                                    @endif 
                                    <tr>
                                      <th>Father-in-law's Name</th>
                                      <td>{{ strtoupper($account_detail['father_in_law_name']) }}</td>
                                  </tr><tr>
                                      <th>Expected Monthly Turnover</th>
                                      <td>{{ ($account_detail['expected_monthly_turnover']) }}</td>
                                  </tr>
                                  </tr><tr>
                                      <th>Expected Monthly Transaction</th>
                                      <td>{{ ($account_detail['expected_monthly_transaction']) }}</td>
                                  </tr>
                                  <tr>
                                      <th>Purpose of Account</th>
                                      <td>{{($account_detail['purpose_of_account']) }}s</td>
                                  </tr>
                                  
                                  @if($account_detail['purpose_of_account']=='Others')
                                  <tr>
                                      <th>Purpose of Action</th>
                                      <td>{{($account_detail['purpose_of_fund_input']) }}s</td>
                                  </tr>
                                  @endif
                                  
                                  <tr>
                                      <th>Source of Fund</th>
                                      <td>{{($account_detail['source_of_fund']) }}s</td>
                                  </tr>
                                  
                                  @if($account_detail['source_of_fund']=='Others')
                                  <tr>
                                      <th>Source of Fund</th>
                                      <td>{{($account_detail['source_of_fund_input']) }}s</td>
                                  </tr>
                                  @endif


                        </tbody>
                      </table>

                       
                       <br>
                             

                    </div>
              </div>
    </section>
        <section>
            <form  action="{{route('admin.account_details.change_status')}}" id="form_data" method="post" >
                            {{ csrf_field() }}

                <input type="hidden" name="id" value="{{ $account_id }}">
                <input type="hidden" name="branch" value="{{ $account_detail['branch'] }}">
                <input type="hidden" name="account_name" value="{{ $account_name['account_name'][0] }}">
                <input type="hidden" name="gender" value=" {{$gender['gender'][0] }}">
                <input type="hidden" name="email" value='{{$account_detail['email'] }}'>
                <input type="hidden" name="status" id="status">
                 <div class="form-group row">
                    <div class="col-xs-4">  
                        <label for="account_number">Account Number</label>
                        <input class="form-control" name="account_number" id="account_number" type="text">
                    </div>
                  </div>
                  
                  <div class="form-group row" id="if_status">
                    <div class="col-xs-4">  
                        <label for="account_number">Remarks</label>
                        <input class="form-control" name="remark" id="remark" type="textarea">
                    </div>
                  </div>
                  
                  
                  
                  
                    <div class="row all-round-border  check_options">
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <input type="radio"  name="status_radio" id="status1" value="Approved" onclick="status_case('Approved')" />Approved
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="radio"   name="status_radio" id="status2" value="Denied" onclick="status_case('Denied')" />Deny
                                        </div>
                                        <div class="col-sm-4 no-padding">
                                            <button class="btn btn-success" type="button" id="btn-submit">Send</button>
                                        </div>
                                    </div>
                                        
                                </div>
                            </div>
                        </div>
            </form>
        </section>
      <script type="text/javascript">
            function status_case(TheCase) {
                  $("#account_number").prop('required',true);
                  $("#status").prop('value', TheCase);
            }
        
        $(document).ready(function(){
            $("#if_status").show();
            
/*            $('input').on('ifClicked',function(event){
                 $(event.target).trigger('click');
                 var a = $(this).val()
                console.log(a);
                if($(this).val()=='Approved'){
                    $("#if_status").show();
                    $("#account_number").prop('required',true);
                    $("#status").prop('value','Approved');
                }
                else{
                    $("#if_status").show();
                    $("#remark").prop('required',true);
                    $("#status").prop('value','Denied');
                }
            })*/

       
      });
        $('#btn-submit').click(function () {
          if ($('#status').val() == '' || $('#status').val() == null) {
            alert('Please select either Approved or Denied.');
            return false;
          } else {
            if ($('#remark').val() == '' || $('#remark').val() == null) {
              alert('Please fill remark fill');
              return false;
            } else {
              if ($('#account_number').val() == '' || $('#account_number').val() == null) {
                alert('Please fill account number');
                return false;
              } else {
                $('#form_data').submit();
              }
            }
          }
        });
     </script>
          
     <script>
     $(function(){
         google.maps.event.trigger(map, "resize");
     });
        var lati = {{$account_detail['latitude']}};
        var longi = {{$account_detail['longitude']}};
          function initMap(){
          
            uluru = {lat: lati, lng: longi};
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 19,
              center: uluru
              
              
            });
            var marker = new google.maps.Marker({
              position: uluru,
              map: map
            });
          }
    </script>
    <script async defer  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8xUqFu-Te4JAwIsNP25J6b59cfxbjgyQ&callback=initMap&libraries=places">
    </script>



@endsection