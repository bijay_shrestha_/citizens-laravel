
      <table border="1">
        <tbody>
            <tr>
            <td>Branch</td>
            <td>Type</td>
            <td>Account Category</td>
            <td>Account Type</td>
            <td>Currency</td>
            <td>Account Name</td>
            <td>Applicant Father Name</td>
            <td>Applicant Grandfather Name</td>
            <td>Spouse Name</td>
            <td>Existing Relationship</td>
            <td>Account Number</td>
            <td>DOB</td>
            <td>DOB Nepali</td>
            <td>Occupation</td>
            <td>Nationality</td>
            <td>Citizenship No.</td>
            <td>Passport No.</td>
            <td>Place of Issue</td>
            <td>Date of Issue</td>
            <td>Age of Minor</td>
            <td>Name of Guardian</td>
            <td>Relationship Minor</td>
            <td>Marital Status</td>
            <td>Email</td>
            <td>Phone No.</td>
            <td>Mobile No.</td>
            <td>Fax No</td>
            <td>Pox Box Corresponding Address</td>
            <td>Permanent Address House No.</td>
            <td>Permanent Address VDC/Municipality</td>
            <td>Permanent Address Ward No.</td>
            <td>Permanent Address District</td>
            <td>Corresponding Address House No.</td>
            <td>Corresponding Address VDC/Municipality</td>
            <td>Corresponding Address Ward No.</td>

            <td>Depositers Nomination name</td>
            <td>Relationship</td>
            <td>Debit Card</td>
            <td>Mobile Banking</td>
            <td>Internet Banking Services</td>
            <td>Longitude</td>
            <td>Latitude</td>
            <td>Mothers Name</td>
            <td>Grand Mothers Name</td>
            <td>Daughters Name</td>
            <td>Sons Name</td>
            <td>Daughter in laws Name</td>
            <td>Father in laws Name</td>
            <td>Expected Monthly Turnover</td>
            <td>Expected Monthly Transaction</td>
            <td>Purpose Of Account</td>
            <td>Source Of Fund</td>
            <td>Image</td>
            <td>Citizen Front</td>
            <td>Citizen Back</td>
            <td>Nominee Image</td>
            <td>Nominee Citizen Front</td>
            <td>Nominee Citizen Back</td>
            </tr>
            @php 
                $account_details=$data;
                
             $data_details=json_decode($account_details, true);
            
           @endphp
           
            @foreach ($data_details as $d)
                  
            @php
                $grandfather_name=json_decode($d['grandfather_name'], true);
                $father_name=json_decode($d['father_name'], true);
                $account_name=json_decode($d['account_name'], true);
                $spouse_name=json_decode($d['spouse_name'],true);
                $existing_relationship=json_decode($d['existing_relationship'],true);
                $account_number=json_decode($d['account_number'],true);
                $dob=json_decode($d['dob'],true);
                $dob_nepali=json_decode($d['dob_nepali'],true);
                $occupation=json_decode($d['occupation'],true);
                $nationality=json_decode($d['nationality'],true);
                $citizenship_no=json_decode($d['citizenship_no'],true);
                $passport_no=json_decode($d['passport_no'],true);
                $place_of_issue=json_decode($d['place_of_issue'],true);
                $date_of_issue=json_decode($d['date_of_issue'],true);
                $age_minor=json_decode($d['age_minor'],true);
                $name_of_guardian=json_decode($d['name_of_guardian'],true);
                $relationship_minor=json_decode($d['relationship_minor'],true);
                $marital_status=json_decode($d['marital_status'],true);
                $email=json_decode($d['email'],true);
                $phone_number=json_decode($d['phone_number'],true);
                $mobile=json_decode($d['mobile'],true);
                $fax_number=json_decode($d['fax_number'],true);
                $pox_box=json_decode($d['pox_box'],true);
                $permanent_address_house_no=json_decode($d['permanent_address_house_no'],true);
                $permanent_address_vdc=json_decode($d['permanent_address_vdc'],true);
                $permanent_address_ward_number=json_decode($d['permanent_address_ward_number'],true);
                $permanent_address_district=json_decode($d['permanent_address_district'],true);            
                $correspondence_address_house_no=json_decode($d['correspondence_address_house_no'],true);
                $correspondence_address_vdc=json_decode($d['correspondence_address_vdc'],true); 
                $correspondence_address_ward_number=json_decode($d['correspondence_address_ward_number'],true); 
                $depositer_name=json_decode($d['depositer_name'],true);
                $relationship=json_decode($d['relationship'],true);
                $longitude=json_decode($d['longitude'],true);
                $latitude=json_decode($d['latitude'],true);
                $grandmother_name=json_decode($d['grandmother_name'],true);
                $daughter_name=json_decode($d['daughter_name'],true);
                $son_name=json_decode($d['son_name'],true);
                $daughter_in_law_name=json_decode($d['daughter_in_law_name'],true);
                $father_in_law_name=json_decode($d['father_in_law_name'],true);
                $expected_monthly_turnover=json_decode($d['expected_monthly_turnover'],true);
                $expected_monthly_transaction=json_decode($d['expected_monthly_transaction'],true);
                $purpose_of_account=json_decode($d['purpose_of_account'],true);
                $source_of_fund=json_decode($d['source_of_fund'],true);
                $citizenship_fronts=json_decode($d['citizenship_fronts'],true);
                $citizenship_backs=json_decode($d['citizenship_backs'],true);
                $nominee_citizenship_fronts=json_decode($d['nominee_citizenship_fronts'],true);
                $nominee_citizenship_backs=json_decode($d['nominee_citizenship_backs'],true);
                $nominee_images=json_decode($d['nominee_images'],true);

            
            @endphp
          
            <tr>
            <td> {{ @$d['branch']  }} </td>
            <td> {{ @$d['type']}} </td>
            <td> {{ @$d['account_category']}} </td>
            <td> {{ @$d['account_type']}} </td>
            <td> {{ @$d['currency']}} </td>
            <td> {{  @$account_name['account_name'][0] }} </td>
            <td> {{ @$father_name['father_name'][0]}} </td>
            <td> {{ @$grandfather_name['grandfather_name'][0] }} </td>
            <td> {{ @$spouse_name['spouse_name'][0] }} </td>
            <td> {{ @$existing_relationship['existing_relationship'][0] }} </td>
            <td> {{ @$account_number['account_number'][0]}} </td>
            <td> {{ @$dob['dob'][0] }} </td>
            <td> {{ @$dob_nepali['dob_nepali'][0]}} </td>
            <td> {{ @$occupation['occupation'][0]}} </td>
            <td> {{ @$nationality['nationality'][0]}} </td>
            <td> {{ @$citizenship_no['citizenship_no'][0]}} </td>
            <td> {{ @$passport_no['passport_no'][0]}} </td>
            <td> {{ @$place_of_issue['place_of_issue'][0]}} </td>
            <td> {{ @$date_of_issue['date_of_issue'][0]}} </td>
            <td> {{ @$age_minor['age_minor'][0]}} </td>
            <td> {{ @$name_of_guardian['name_of_guardian'][0] }} </td>
            <td> {{ @$relationship_minor['relationship_minor'][0] }} </td>
            <td> {{ @$marital_status['marital_status'][0]}} </td>
            <td> {{ @$email['email'][0]}} </td>
            <td> {{ @$phone_number['phone_number'][0]}} </td>
            <td>{{ @$mobile['mobile'][0] }}</td>
            <td> {{ @$fax_number['fax_number'][0]}} </td>
            <td> {{ @$pox_box['pox_box'][0] }} </td>
            <td> {{ @$permanent_address_house_no['permanent_address_house_no'][0] }} </td>
            <td> {{ @$permanent_address_vdc['permanent_address_vdc'][0] }} </td>
            <td> {{ @$permanent_address_ward_number['permanent_address_ward_number'][0]}} </td>
            <td> {{ @$permanent_address_district['permanent_address_district'][0]}} </td>
            <td> {{ @$correspondence_address_house_no['correspondence_address_house_no'][0]}} </td>
            <td> {{ @$correspondence_address_vdc['correspondence_address_vdc'][0] }} </td>
            <td> {{ @$correspondence_address_ward_number['correspondence_address_ward_number'][0] }} </td>
            <td>  {{ @$depositer_name['depositer_name'][0]}}  </td>
            <td>  {{ @$relationship['relationship'][0] }}  </td>
            <td>  {{ @$d['debit_card'] }} </td>
            <td>  {{ @$d['mobile_banking_services'] }} </td>
            <td>  {{ @$d['internet_banking_services'] }} </td>
            <td>  {{ @$longitude['longitude'][0]}} </td>
            <td>  {{ @$latitude['latitude'][0]}} </td>
            <td>   {{$d['mother_name']}} </td>
            <td>  {{ @$grandmother_name['grandmother_name'][0]}} </td>
            <td>  {{ @$daughter_name['daughter_name'][0]}}</td>
            <td>  {{ @$son_name['son_name'][0] }} </td>
            <td>{{ @$daughter_in_law_name['daughter_in_law_name'][0]}}</td>
            <td>{{ @$father_in_law_name['father_in_law_name'][0]}}</td>
            <td>{{ @$d['expected_monthly_turnover']}}</td>
            <td>{{ @$d['expected_monthly_transaction']}}</td>
            <td>{{ @$d['purpose_of_account']}}</td>
            <td>{{ @$d['source_of_fund']}}</td>
            <td>Image</td>
            <td>{{ @$citizenship_fronts['citizenship_fronts'][0] }} </td>
            <td> {{ @$citizenship_backs['citizenship_backs'][0] }} </td>
            <td>{{ @$nominee_images['nominee_images'][0] }}</td>
            <td>{{ @$nominee_citizenship_fronts['nominee_citizenship_fronts'][0] }}</td>
            <td>{{ @$nominee_citizenship_backs['nominee_citizenship_backs'][0] }}</td>
            </tr>
             @endforeach

        </tbody>
    </table>      
    