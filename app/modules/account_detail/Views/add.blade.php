@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Account_details   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.account_details') }}">tbl_account_details</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.account_details.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                                    <label for="a_id">A_id</label><input type="text" name="a_id" id="a_id" class="form-control" ></div><div class="form-group">
                                    <label for="reference_code">Reference_code</label><input type="text" name="reference_code" id="reference_code" class="form-control" ></div><div class="form-group">
                                    <label for="applying_from">Applying_from</label><input type="text" name="applying_from" id="applying_from" class="form-control" ></div><div class="form-group">
                                    <label for="branch">Branch</label><input type="text" name="branch" id="branch" class="form-control" ></div><div class="form-group">
                                    <label for="photo">Photo</label><input type="text" name="photo" id="photo" class="form-control" ></div><div class="form-group">
                                    <label for="type">Type</label><input type="text" name="type" id="type" class="form-control" ></div><div class="form-group">
                                    <label for="account_category">Account_category</label><input type="text" name="account_category" id="account_category" class="form-control" ></div><div class="form-group">
                                    <label for="types_of_individual">Types_of_individual</label><input type="text" name="types_of_individual" id="types_of_individual" class="form-control" ></div><div class="form-group">
                                    <label for="account_type">Account_type</label><input type="text" name="account_type" id="account_type" class="form-control" ></div><div class="form-group">
                                    <label for="currency">Currency</label><input type="text" name="currency" id="currency" class="form-control" ></div><div class="form-group">
                                    <label for="account_name">Account_name</label><input type="text" name="account_name" id="account_name" class="form-control" ></div><div class="form-group">
                                    <label for="father_name">Father_name</label><input type="text" name="father_name" id="father_name" class="form-control" ></div><div class="form-group">
                                    <label for="grandfather_name">Grandfather_name</label><input type="text" name="grandfather_name" id="grandfather_name" class="form-control" ></div><div class="form-group">
                                    <label for="spouse_name">Spouse_name</label><input type="text" name="spouse_name" id="spouse_name" class="form-control" ></div><div class="form-group">
                                    <label for="existing_relationship">Existing_relationship</label><input type="text" name="existing_relationship" id="existing_relationship" class="form-control" ></div><div class="form-group">
                                    <label for="account_number">Account_number</label><input type="text" name="account_number" id="account_number" class="form-control" ></div><div class="form-group">
                                    <label for="dob">Dob</label><input type="text" name="dob" id="dob" class="form-control" ></div><div class="form-group">
                                    <label for="dob_nepali">Dob_nepali</label><input type="text" name="dob_nepali" id="dob_nepali" class="form-control" ></div><div class="form-group">
                                    <label for="gender">Gender</label><input type="text" name="gender" id="gender" class="form-control" ></div><div class="form-group">
                                    <label for="occupation">Occupation</label><input type="text" name="occupation" id="occupation" class="form-control" ></div><div class="form-group">
                                    <label for="nationality">Nationality</label><input type="text" name="nationality" id="nationality" class="form-control" ></div><div class="form-group">
                                    <label for="citizenship_no">Citizenship_no</label><input type="text" name="citizenship_no" id="citizenship_no" class="form-control" ></div><div class="form-group">
                                    <label for="passport_no">Passport_no</label><input type="text" name="passport_no" id="passport_no" class="form-control" ></div><div class="form-group">
                                    <label for="nrn_card">Nrn_card</label><input type="text" name="nrn_card" id="nrn_card" class="form-control" ></div><div class="form-group">
                                    <label for="birth_certificate">Birth_certificate</label><input type="text" name="birth_certificate" id="birth_certificate" class="form-control" ></div><div class="form-group">
                                    <label for="other_id">Other_id</label><input type="text" name="other_id" id="other_id" class="form-control" ></div><div class="form-group">
                                    <label for="place_of_issue">Place_of_issue</label><input type="text" name="place_of_issue" id="place_of_issue" class="form-control" ></div><div class="form-group">
                                    <label for="date_of_issue">Date_of_issue</label><input type="text" name="date_of_issue" id="date_of_issue" class="form-control" ></div><div class="form-group">
                                    <label for="date_of_issue_bs">Date_of_issue_bs</label><input type="text" name="date_of_issue_bs" id="date_of_issue_bs" class="form-control" ></div><div class="form-group">
                                    <label for="age_minor">Age_minor</label><input type="text" name="age_minor" id="age_minor" class="form-control" ></div><div class="form-group">
                                    <label for="name_of_guardian">Name_of_guardian</label><input type="text" name="name_of_guardian" id="name_of_guardian" class="form-control" ></div><div class="form-group">
                                    <label for="relationship_minor">Relationship_minor</label><input type="text" name="relationship_minor" id="relationship_minor" class="form-control" ></div><div class="form-group">
                                    <label for="marital_status">Marital_status</label><input type="text" name="marital_status" id="marital_status" class="form-control" ></div><div class="form-group">
                                    <label for="email">Email</label><input type="text" name="email" id="email" class="form-control" ></div><div class="form-group">
                                    <label for="pan_number">Pan_number</label><input type="text" name="pan_number" id="pan_number" class="form-control" ></div><div class="form-group">
                                    <label for="phone_number">Phone_number</label><input type="text" name="phone_number" id="phone_number" class="form-control" ></div><div class="form-group">
                                    <label for="mobile">Mobile</label><input type="text" name="mobile" id="mobile" class="form-control" ></div><div class="form-group">
                                    <label for="fax_number">Fax_number</label><input type="text" name="fax_number" id="fax_number" class="form-control" ></div><div class="form-group">
                                    <label for="pox_box">Pox_box</label><input type="text" name="pox_box" id="pox_box" class="form-control" ></div><div class="form-group">
                                    <label for="permanent_address_house_no">Permanent_address_house_no</label><input type="text" name="permanent_address_house_no" id="permanent_address_house_no" class="form-control" ></div><div class="form-group">
                                    <label for="permanent_address_vdc">Permanent_address_vdc</label><input type="text" name="permanent_address_vdc" id="permanent_address_vdc" class="form-control" ></div><div class="form-group">
                                    <label for="permanent_address_ward_number">Permanent_address_ward_number</label><input type="text" name="permanent_address_ward_number" id="permanent_address_ward_number" class="form-control" ></div><div class="form-group">
                                    <label for="permanent_address_street">Permanent_address_street</label><input type="text" name="permanent_address_street" id="permanent_address_street" class="form-control" ></div><div class="form-group">
                                    <label for="permanent_address_district">Permanent_address_district</label><input type="text" name="permanent_address_district" id="permanent_address_district" class="form-control" ></div><div class="form-group">
                                    <label for="correspondence_address_house_no">Correspondence_address_house_no</label><input type="text" name="correspondence_address_house_no" id="correspondence_address_house_no" class="form-control" ></div><div class="form-group">
                                    <label for="correspondence_address_vdc">Correspondence_address_vdc</label><input type="text" name="correspondence_address_vdc" id="correspondence_address_vdc" class="form-control" ></div><div class="form-group">
                                    <label for="correspondence_address_ward_number">Correspondence_address_ward_number</label><input type="text" name="correspondence_address_ward_number" id="correspondence_address_ward_number" class="form-control" ></div><div class="form-group">
                                    <label for="correspondence_address_street">Correspondence_address_street</label><input type="text" name="correspondence_address_street" id="correspondence_address_street" class="form-control" ></div><div class="form-group">
                                    <label for="correspondence_address_district">Correspondence_address_district</label><input type="text" name="correspondence_address_district" id="correspondence_address_district" class="form-control" ></div><div class="form-group">
                                    <label for="account_status">Account_status</label><input type="text" name="account_status" id="account_status" class="form-control" ></div><div class="form-group">
                                    <label for="company_name">Company_name</label><input type="text" name="company_name" id="company_name" class="form-control" ></div><div class="form-group">
                                    <label for="company_address">Company_address</label><input type="text" name="company_address" id="company_address" class="form-control" ></div><div class="form-group">
                                    <label for="com_position">Com_position</label><input type="text" name="com_position" id="com_position" class="form-control" ></div><div class="form-group">
                                    <label for="expiry_date">Expiry_date</label><input type="text" name="expiry_date" id="expiry_date" class="form-control" ></div><div class="form-group">
                                    <label for="foreign_address">Foreign_address</label><input type="text" name="foreign_address" id="foreign_address" class="form-control" ></div><div class="form-group">
                                    <label for="country">Country</label><input type="text" name="country" id="country" class="form-control" ></div><div class="form-group">
                                    <label for="contact_no">Contact_no</label><input type="text" name="contact_no" id="contact_no" class="form-control" ></div><div class="form-group">
                                    <label for="type_of_visa">Type_of_visa</label><input type="text" name="type_of_visa" id="type_of_visa" class="form-control" ></div><div class="form-group">
                                    <label for="visa_expiry_date">Visa_expiry_date</label><input type="text" name="visa_expiry_date" id="visa_expiry_date" class="form-control" ></div><div class="form-group">
                                    <label for="city_state">City_state</label><input type="text" name="city_state" id="city_state" class="form-control" ></div><div class="form-group">
                                    <label for="account_no">Account_no</label><input type="text" name="account_no" id="account_no" class="form-control" ></div><div class="form-group">
                                    <label for="remark">Remark</label><input type="text" name="remark" id="remark" class="form-control" ></div><div class="form-group">
                                    <label for="applied_date">Applied_date</label><input type="text" name="applied_date" id="applied_date" class="form-control" ></div><div class="form-group">
                                    <label for="del_flag">Del_flag</label><input type="text" name="del_flag" id="del_flag" class="form-control" ></div>
<input type="hidden" name="id" id="id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.account_details') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
