<table border="1">
    <tbody>
        <thead>
        <td>a_id</td>
        <td>reference_code</td>
        <td>applying_from</td>
        <td>branch</td>
        <td>photo</td>
        <td>type</td>
        <td>types_of_individual</td>
        <td>account_category</td>
        <td>account_type</td>
        <td>currency</td>

        <td>account_name</td>
        <td>father_name</td>
        <td>grandfather_name</td>
        <td>spouse_name</td>
        <td>existing_relationship</td>
        <td>account_number</td>
        <td>dob</td>
        <td>dob_nepali</td>
        <td>gender</td>
        <td>occupation</td>
        <td>nationality</td>
        <td>citizenship_no</td>
        <td>passport_no</td>
        <td>nrn_card</td>
        <td>birth_certificate</td>
        <td>other_id</td>
        <td>expiry_date</td>
        <td>foreign_address</td>
        <td>country</td>
        <td>contact_no</td>
        <td>type_of_visa</td>
        <td>visa_expiry_date</td>
        <td>place_of_issue</td>
        <td>date_of_issue</td>
        <td>date_of_issue_bs</td>
        <td>age_minor</td>
        <td>name_of_guardian</td>
        <td>relationship_minor</td>
        <td>marital_status</td>
        <td>email</td>
        <td>pan_number</td>
        <td>phone_number</td>
        <td>mobile</td>
        <td>fax_number</td>
        <td>pox_box</td>
        <td>permanent_address_house_no</td>
        <td>permanent_address_vdc</td>
        <td>permanent_address_ward_number</td>
        <td>permanent_address_street</td>
        <td>permanent_address_district</td>
        <td>correspondence_address_house_no</td>
        <td>correspondence_address_vdc</td>
        <td>correspondence_address_ward_number</td>
        <td>correspondence_address_street</td>
        <td>correspondence_address_district</td>
        <td>	account_status</td>
        <td>del_flag</td>
        <td>kyc_id</td>
        <td>high_reason</td>
        <td>mother_name</td>
        <td>grandmother_name</td>
        <td>daughter_name</td>
        <td>father_in_law_name</td>	
        <td>son_name</td>
        <td>daughter_in_law_name</td>
        <td>expected_monthly_turnover</td>
        <td>expected_monthly_transaction</td>
        <td>purpose_of_account</td>
        <td>source_of_fund</td>
        <td>source_of_fund_input</td>
        <td>purpose_of_fund_input</td>
        <td>beneficial_owner_name</td>
        <td>beneficial_citizen</td>
        <td>beneficial_address</td>
        <td>beneficial_relation</td>
        <td>beneficial_contact</td>
        <td>auth_id</td>
        <td>longitude</td>
        <td>latitude</td>
        <td>name1</td>
        <td>name2</td>
        <td>name3</td>
        <td>s_id</td>
        <td>is_high_risk_customer</td>
        <td>if_beneficial_owner</td>
        <td>debit_card</td>
        <td>mobile_banking_services</td>
        <td>internet_banking_services</td>
        <td>locker_facilities</td>
        <td>ntc</td>
        <td>e_statement</td>
        <td>standing_instruction</td>
        <td>bancassurance</td>
        <td>is_banking_password</td>
        <td>is_internet_banking_password</td>
        <td>other_service</td>
<td>d_id</td>
        <td>mr_miss</td>	
        <td>depositer_name</td>
        <td>relationship</td>
        <td>images</td>
        <td>citizenship_fronts</td>
        <tD>citizenship_backs</td>
        <td>nominee_images</td>	
        <td>nominee_citizenship_fronts</td>
        <td>nominee_citizenship_backs</td>
        </thead>
        <tbody>
            @php
        // dd($data);
                $account_detail=$data[0];         
                  
        //   dd($account_detail);
        //   $ac=json_decode($account_detail,true);
        //       $a=json_decode($account_detail['a_id'],true);
        //       echo $a['a_id'];

     //   echo($account_detail->a_id) 
            @endphp
            <tr>
         
         
                 <td>{{ @$account_detail->a_id }}</td>
                <td>{{ @$account_detail->reference_code }}</td>
                <td>{{ @$account_detail->applying_from }}</td>
                <td>{{ @$account_detail->branch }}</td> 
                <td>{{ @$account_detail->photo }}</td>
                <td>{{ @$account_detail->type }}</td>
                <td>{{ @$account_detail->types_of_individual }}</td>
                <td>{{ @$account_detail->account_category }}</td>
                <td>{{ @$account_detail->account_type }}</td>
                <td>{{ @$account_detail->currency }}</td>
                @php
   
                $account_name=json_decode($account_detail->account_name);
                $father_name=json_decode($account_detail->father_name);
                $grandfather_name=json_decode($account_detail->grandfather_name);
                $spouse_name=json_decode($account_detail->spouse_name);
                $existing_relationship=json_decode($account_detail->existing_relationship);
                $account_number=json_decode($account_detail->account_number);
                $existing_relationship=json_decode($account_detail->existing_relationship);
                $dob=json_decode($account_detail->dob);
                $dob_nepali=json_decode($account_detail->dob_nepali);
                $gender=json_decode($account_detail->gender);
                $occupation=json_decode($account_detail->occupation);
                $nationality=json_decode($account_detail->nationality);
                $citizenship_no=json_decode($account_detail->citizenship_no);
                $passport_no=json_decode($account_detail->passport_no);
                $nrn_card=json_decode($account_detail->nrn_card);
                $birth_certificate=json_decode($account_detail->birth_certificate);
                $other_id=json_decode($account_detail->other_id);
                // $expiry_date=json_decode($account_detail->expiry_date);
                $foreign_address=json_decode($account_detail->foreign_address);
                $country=json_decode($account_detail->country);
                $contact_no=json_decode($account_detail->contact_no);
                $type_of_visa=json_decode($account_detail->type_of_visa);
                // $visa_expiry_date=json_decode($account_detail->visa_expiry_date);
                $place_of_issue=json_decode($account_detail->place_of_issue);
                $date_of_issue=json_decode($account_detail->date_of_issue);
                $age_minor=json_decode($account_detail->age_minor);
                $marital_status=json_decode($account_detail->marital_status);
                $email=json_decode($account_detail->email);
                $pan_number=json_decode($account_detail->pan_number);
                $phone_number=json_decode($account_detail->phone_number);
                $mobile=json_decode($account_detail->mobile);
                $fax_number=json_decode($account_detail->fax_number);
                $pox_box=json_decode($account_detail->pox_box);
                $permanent_address_house_no=json_decode($account_detail->permanent_address_house_no);
                $permanent_address_vdc=json_decode($account_detail->permanent_address_vdc);
                $permanent_address_ward_number=json_decode($account_detail->permanent_address_ward_number);
                $permanent_address_street=json_decode($account_detail->permanent_address_street);
                $permanent_address_district=json_decode($account_detail->permanent_address_district);
                $correspondence_address_house_no=json_decode($account_detail->correspondence_address_house_no);
                $correspondence_address_vdc=json_decode($account_detail->correspondence_address_vdc);
                $correspondence_address_ward_number=json_decode($account_detail->correspondence_address_ward_number);
                $correspondence_address_street=json_decode($account_detail->correspondence_address_street);
                $correspondence_address_district=json_decode($account_detail->correspondence_address_district);
                $account_status=json_decode($account_detail->account_status);
                $del_flag=json_decode($account_detail->del_flag);
                $kyc_id=json_decode($account_detail->kyc_id);
                $high_reason=json_decode($account_detail->high_reason);
                //dd($account_detail->mother_name);
                //$mother_name=json_decode($account_detail->mother_name);
                //$grandmother_name=json_decode($account_detail->grandmother_name);
                $daughter_name=json_decode($account_detail->daughter_name);
                $father_in_law_name=json_decode($account_detail->father_in_law_name);
                $son_name=json_decode($account_detail->son_name);
                $daughter_in_law_name=json_decode($account_detail->daughter_in_law_name);
                $name_of_guardian=json_decode($account_detail->name_of_guardian);
                $relationship_minor=json_decode($account_detail->relationship_minor);
                // $expected_monthly_turnover=json_decode($account_detail->daughter_in_law_name);
                // $expected_monthly_transaction=json_decode($account_detail->expected_monthly_transaction);
                $purpose_of_account=json_decode($account_detail->purpose_of_account);
                $source_of_fund=json_decode($account_detail->source_of_fund);
                $source_of_fund_input=json_decode($account_detail->source_of_fund_input);
                $date_of_issue_bs=json_decode($account_detail->date_of_issue_bs);
                $purpose_of_fund_input=json_decode($account_detail->purpose_of_fund_input);
                $beneficial_owner_name=json_decode($account_detail->beneficial_owner_name);
                $beneficial_citizen=json_decode($account_detail->beneficial_citizen);
                $beneficial_address=json_decode($account_detail->beneficial_address);
                $beneficial_relation=json_decode($account_detail->beneficial_relation);
                $beneficial_contact=json_decode($account_detail->beneficial_contact);
                // $auth_id=json_decode($account_detail->auth_id);
                // $longitude=json_decode($account_detail->longitude);
                // $latitude=json_decode($account_detail->latitude);
                $name1=json_decode($account_detail->name1);
                $name2=json_decode($account_detail->name2);
                $name3=json_decode($account_detail->name3);

                $s_id=json_decode($account_detail->s_id);
                $is_high_risk_customer=json_decode($account_detail->is_high_risk_customer);
                $if_beneficial_owner=json_decode($account_detail->if_beneficial_owner);
                $debit_card=json_decode($account_detail->debit_card);

                $mobile_banking_services=json_decode($account_detail->mobile_banking_services);
                $internet_banking_services=json_decode($account_detail->internet_banking_services);
                $locker_facilities=json_decode($account_detail->locker_facilities);
                $ntc=json_decode($account_detail->ntc);
                $e_statement=json_decode($account_detail->e_statement);

                $standing_instruction=json_decode($account_detail->standing_instruction);
                $bancassurance=json_decode($account_detail->bancassurance);
                $is_banking_password=json_decode($account_detail->is_banking_password);
                $is_internet_banking_password=json_decode($account_detail->is_internet_banking_password);
                $other_service=json_decode($account_detail->other_service);
                $d_id=json_decode($account_detail->d_id);
                $mr_miss=json_decode($account_detail->mr_miss);
                $depositer_name=json_decode($account_detail->depositer_name);
                $relationship=json_decode($account_detail->relationship);
                $images=json_decode($account_detail->images);
                $citizenship_fronts=json_decode($account_detail->citizenship_fronts);
                $citizenship_backs=json_decode($account_detail->citizenship_backs);
                $nominee_images=json_decode($account_detail->nominee_images);
                $nominee_citizenship_fronts=json_decode($account_detail->nominee_citizenship_fronts);
                $nominee_citizenship_backs=json_decode($account_detail->nominee_citizenship_backs);







               @endphp
                <td>{{ @$account_name->account_name[0] }}</td>
                 <td>{{ @$father_name->father_name[0] }}</td>
                 <td>{{ @$grandfather_name->grandfather_name[0] }}</td>
                 <td>{{ @$spouse_name->spouse_name[0] }}</td>
                 <td>{{ @$existing_relationship->existing_relationship[0] }}</td>
                 <td>{{ @$account_number->account_number[0] }}</td>
                 <td>{{ @$dob->dob[0] }}</td>
                 <td>{{ @$dob_nepali->dob_nepali[0] }}</td>
                 <td>{{ @$gender->gender[0] }}</td>
                 <td>{{ @$occupation->occupation[0] }}</td>
                 <td>{{ @$nationality->nationality[0] }}</td>
                 <td>{{ @$citizenship_no->citizenship_no[0] }}</td>
                 <td>{{ @$passport_no->passport_no[0] }}</td>
                 <td>{{ @$nrn_card->nrn_card[0] }}</td>
                 <td>{{ @$birth_certificate->birth_certificate[0] }}</td>
                 <td>{{ @$other_id->other_id[0] }}</td>
                 <td>{{ @$account_detail->expiry_date }}</td>

                 <td>{{ @$foreign_address->foreign_address[0] }}</td>
                 <td>{{ @$country->country[0] }}</td>
                 <td>{{ @$contact_no->contact_no[0] }}</td>
                 <td>{{ @$type_of_visa->type_of_visa[0] }}</td>

                 <td>{{ @$account_detail->visa_expiry_date}}</td>
                 <td>{{ @$place_of_issue->place_of_issue[0] }}</td>
                 <td>{{ @$date_of_issue->date_of_issue[0] }}</td>
                 <td> {{ @$date_of_issue_bs->date_of_issue_bs[0]  }} </td>
                 <td>{{ @$age_minor->age_minor[0] }}</td>
                 <td>{{ @$name_of_guardian->name_of_guardian[0] }}</td>
                 <td>{{ @$relationship_minor->relationship_minor[0] }}</td>

                 <td>{{ @$marital_status->marital_status[0] }}</td>
                 <td>{{ @$email->email[0] }}</td>
                 <td>{{ @$pan_number->pan_number[0] }}</td>
                 <td>{{ @$phone_number->phone_number[0] }}</td>
                 <td>{{ @$mobile->mobile[0] }}</td>
                
                 <td>{{ @$fax_number->fax_number[0] }}</td>
                 <td>{{ @$pox_box->pox_box[0] }}</td>
                 <td>{{ @$permanent_address_house_no->permanent_address_house_no[0] }}</td>
                 <td>{{ @$permanent_address_vdc->permanent_address_vdc[0] }}</td>
                 <td>{{ @$permanent_address_ward_number->permanent_address_ward_number[0] }}</td>

                 <td>{{ @$permanent_address_street->permanent_address_street[0] }}</td>
                 <td>{{ @$permanent_address_district->permanent_address_district[0] }}</td>
                 <td>{{ @$correspondence_address_house_no->correspondence_address_house_no[0] }}</td>
                 <td>{{ @$correspondence_address_vdc->correspondence_address_vdc[0] }}</td>
                 <td>{{ @$correspondence_address_ward_number->correspondence_address_ward_number[0] }}</td>
                 <td>{{ @$correspondence_address_street->correspondence_address_street[0] }}</td>

                 <td>{{ @$correspondence_address_district->correspondence_address_district[0] }}</td>
                 <td>{{ @$account_detail->account_status }}</td>
                 <td>{{ @$account_detail->del_flag }}</td>
                 <td>{{ @$account_detail->kyc_id}}</td>
                 <td>{{ @$account_detail->high_reason}}</td>

                 <td>{{ @$account_detail->mother_name }}</td>
                 <td>{{ @$account_detail->grandmother_name }}</td>
                 <td>
                 @foreach($daughter_name->daughter_name as $daughter)
                 {{ @$daughter }}
                 @endforeach
                </td>
                 <td>{{ @$father_in_law_name->father_in_law_name[0] }}</td>
                <td>
                    @foreach($son_name->son_name as $son)
                        {{ @$son }}
                    @endforeach
                </td>
                
                <td>{{ @$daughter_in_law_name->daughter_in_law_name[0] }}</td>

                 <td>{{ @$account_detail->expected_monthly_turnover }}</td>
                 <td>{{ @$account_detail->expected_monthly_transaction }}</td>
                 <td>{{ @$account_detail->purpose_of_account }}</td>
                 <td>{{ @$account_detail->source_of_fund }}</td>
                 <td>{{ @$account_detail->source_of_fund_input }}</td>

                 <td>{{ @$account_detail->purpose_of_fund_input }}</td>
                 <td>{{ @$account_detail->beneficial_owner_name }}</td>
                 <td>{{ @$account_detail->beneficial_citizen }}</td>
                 <td>{{ @$account_detail->beneficial_address}}</td>
                 <td>{{ @$account_detail->beneficial_relation }}</td>

                 <td>{{ @$account_detail->beneficial_contact }}</td>
                 <td>{{ @$account_detail->auth_id }}</td>
                 <td>{{ @$account_detail->longitude}}</td>
                 <td>{{ @$account_detail->latitude}}</td>
                 <td>{{ @$account_detail->name1 }}</td>
                 <td>{{ @$account_detail->name2 }}</td>

                 <td>{{ @$account_detail->name3 }}</td>
                
                 <td>{{ @$account_detail->s_id }}</td>
                 <td>{{ @$account_detail->is_high_risk_customer }}</td>
                 <td>{{ @$account_detail->if_beneficial_owner}}</td>
                 <td>{{ @$account_detail->debit_card}}</td>

                 <td>{{ @$account_detail->mobile_banking_services}}</td>
                 <td>{{ @$account_detail->internet_banking_services}}</td>
                 <td>{{ @$account_detail->locker_facilities}}</td>
                 <td>{{ @$account_detail->ntc }}</td>
                 <td>{{ @$account_detail->e_statement }}</td>

                 <td>{{ @$account_detail->standing_instruction }}</td>
                 <td>{{ @$account_detail->bancassurance }}</td>
                 <td>{{ @$account_detail->is_banking_password }}</td>
                 <td>{{ @$account_detail->is_internet_banking_password }}</td>
                 <td>{{ @$account_detail->other_service}}</td>
                 <td>{{ @$account_detail->d_id }}</td>

                 <td>{{ @$mr_miss->mr_miss[0] }}</td>
                 <td>{{ @$depositer_name->depositer_name[0] }}</td>
                 <td>{{ @$relationship->relationship[0] }}</td>
                 <td>{{ @$images->images[0] }}</td>
                 <td>{{ @$citizenship_fronts->citizenship_fronts[0] }}</td>
                 <td>{{ @$citizenship_backs->citizenship_backs[0] }}</td>
                 
                 <td>{{ @$nominee_images->nominee_images[0] }}</td>
                 <td>{{ @$nominee_citizenship_fronts->nominee_citizenship_fronts[0] }}</td>
                 <td>{{ @$nominee_citizenship_backs->nominee_citizenship_backs[0] }}</td>
                
            </tr>
        </tbody>
    </table>
    