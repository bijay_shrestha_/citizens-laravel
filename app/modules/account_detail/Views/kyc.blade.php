<!DOCTYPE html>
<html>

<head>
    <style>
    img {
        width: 300px;
    }
    .table-width {
        width: 100%;
    }
    .date-top {
        text-align: left;
    }
    .kyc-title {
        text-align: right;
    }
    input {
        border: 2px solid #8bceb6;

    }
    .client-id {
        text-align: right;
    }
    .in-table-one {
        padding-bottom: 10px;
    }
    .table-wrap {
        border-collapse: collapse;
    }
    .table-head {
        text-align: left;
    }
    .address-td {
        border-right: 1px solid #8bceb6;
    }
    .thead-font {
        font-weight: normal;
    }
        /* .source-of-fund-label {
            width: 100%;
            float: left;
            } */
            .source-of-fund {
                width: 100%;
                display: inline-block;
            }
            .source-of-fund-align {
                float: left;
                width: 25%;
                padding-bottom: 4px;
            }
            .site-map {
                height: 300px;
            }
            .thumb-width {
                width: 35%;
            }
            .thumb-row {
                height: 145px;
            }
            .thumb-class {
                border: 2px solid #8bceb6;
            }
            .left {
                width: 50%;
            }
            .thumb-impression {
                padding-left: 60px
            }
            .signature {
                border-top: 2px solid #8bceb6;

            }
            .ac-holder-signature {
                text-align: right;
            }
            .bank-head {
                text-align: center;
            }
            .bank-use td{
                border: none;
            }
            .verifying-doc-wrap {
                width: 100%;
                display: inline-block;
            }
            .verifying-doc {
                float: left;
                width: 33.33%;
            }
            .bank-td {
                border-right: 2px solid #8bceb6 !important;
                width: 50%;
            }
            .ac-risk-grading {
                font-weight: bold;
            }
            .remarks {
                padding-bottom: 42px;
            }
        /* .remarks2 {
            padding-bottom: 42px;
            padding-top: 30px;
            } */
            .main {
                border: 2px solid #8bceb6;
            }
            .table-first {
                border: 0;
            }
        /* .table-first td{
            border: 1px solid #8bceb6;
            } */
            td {
                height: 40px;
            }
            .bank-second {
                padding-top: 20px;
            }
            .input-height {
                height: 26px;
            }
            .table-first tr {
                border-bottom: 1px solid #8bceb6;
            }
            .table-first td {
                border-right: 1px solid #8bceb6;
            }
            .table-first td:last-child {
                border-bottom: 0;
            }
            .last-row {
                border-bottom: 0 !important;
            }
            .last-td {
                border-right: 0 !important;
                width: 400px;
            }
            .tr-align {
                width: 100%;
                display: block;
            }
            .table-first label {
                padding-left: 5px;
            }
            .space td{
                padding-left: 5px;
            }
            .vert-align {
                vertical-align:top;
            }
            .no {
                padding-left: 30px !important;
            }
            .kyc-id {
                width: 44%;
                padding-left: 16px;
            }
            .table-height th{
                height: 30px;
            }
            .table-height td {
                height: 40px;
            }
            .sn-align {
                text-align: center;
            }
             #map0 {
               margin: auto;
                height: 400px;
                width: 800px;
            }
             #map2 {
               margin: auto;
                height: 400px;
                width: 800px;
            }

        </style>
      
    </head>


<body>

    @php
         $account_name = json_decode($account_detail['account_name'],true);

         $father_name = json_decode($account_detail['father_name'],true);
         $grandfather_name = json_decode($account_detail['grandfather_name'],true);
         $spouse_name = json_decode($account_detail['spouse_name'],true);

         $account_number = json_decode($account_detail['account_number'],true);

         $dob = json_decode($account_detail['dob'],true);
         $dob_nepali = json_decode($account_detail['dob_nepali'],true);
         $occupation = json_decode($account_detail['occupation'],true);
         $gender = json_decode($account_detail['gender'],true);
         $nrn_card = json_decode($account_detail['nrn_card'],true);
         $foreign_address = json_decode($account_detail['foreign_address'],true);
         $country = json_decode($account_detail['country'],true);
         $contact_no = json_decode($account_detail['contact_no'],true);
         $type_of_visa = json_decode($account_detail['type_of_visa'],true);
         $visa_expiry_date = json_decode($account_detail['visa_expiry_date'],true);
         $city_state = json_decode($account_detail['city_state'],true);
         $nationality = json_decode($account_detail['nationality'],true);
         $passport_no = json_decode($account_detail['passport_no'],true);
         $expiry_date = json_decode($account_detail['expiry_date'],true);
         $citizenship_no = json_decode($account_detail['citizenship_no'],true);
         $place_of_issue = json_decode($account_detail['place_of_issue'],true);
         $date_of_issue = json_decode($account_detail['date_of_issue'],true);
         $date_of_issue_bs = json_decode($account_detail['date_of_issue_bs'],true);
         $marital_status = json_decode($account_detail['marital_status'],true);
         $email = json_decode($account_detail['email'],true);
         $pan_number = json_decode($account_detail['pan_number'],true);
         $phone_number = json_decode($account_detail['phone_number'],true);
         $mobile = json_decode($account_detail['mobile'],true);
         $fax_number = json_decode($account_detail['fax_number'],true);
         $pox_box = json_decode($account_detail['pox_box'],true);
         $permanent_address_house_no = json_decode($account_detail['permanent_address_house_no'],true);
         $permanent_address_vdc = json_decode($account_detail['permanent_address_vdc'],true);
         $permanent_address_ward_number = json_decode($account_detail['permanent_address_ward_number'],true);
         $permanent_address_street = json_decode($account_detail['permanent_address_street'],true);
         $permanent_address_district = json_decode($account_detail['permanent_address_district'],true);
         $correspondence_address_house_no = json_decode($account_detail['correspondence_address_house_no'],true);
         $correspondence_address_vdc = json_decode($account_detail['correspondence_address_vdc'],true);
         $correspondence_address_ward_number = json_decode($account_detail['correspondence_address_ward_number'],true);
         $correspondence_address_ward_number = json_decode($account_detail['correspondence_address_ward_number'],true);
         $correspondence_address_district = json_decode($account_detail['correspondence_address_district'],true);
         $correspondence_address_street = json_decode($account_detail['correspondence_address_street'],true);
        
         $i=0; 

    @endphp
    @foreach ($account_name['account_name'] as $index => $name)
        <style>
             #map{{$index}} {
               margin: auto;
                height: 400px;
                width: 800px;
            }
         </style>
         <section>
            <table style="width: 700px;">
                <tr>
                    <td>
                        <table class="table-width">
                            <tr>
                                <td><img src= "{{url('images/CB_logo.png')}}" alt="logo"></td>
                                <td class="kyc-title">
                                    <h4>KYC FORM FOR INDIVIDUAL CUSTOMER</h4>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="table-width in-table-one">
                            <tr class="tr-align">
                                <td style="width: 44%;">
                                    <label>Screening ID</label>
                                    <input type="text" disabled class="input-height">
                                </td>
                                <td class="kyc-id">
                                    <label>KYC ID</label>
                                    <input type="text" disabled class="input-height">
                                </td>
                                <td class="date-top">
                                    <label>Date:</label>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="table-width">
                            <tr>
                                <td>
                                    <label>Account Number</label>
                                    <input type="text" disabled value="{{$account_number['account_number'][0]}}" class="input-height">
                                </td>
                                <td class="client-id">
                                    <label>Client ID</label>
                                    <input type="text" disabled class="input-height">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <!-- first main inner table(below ac no & client id) -->
                        <table border="1" class="table-width table-wrap main">
                            <tr>

                                <td colspan="2">
                                    <table border="1" class="table-width table-wrap table-first">
                                        <tr>
                                            <td colspan="2"><label>Account Holder's Name</label> : {{strtoupper($name)}} </td>
                                            <td class="last-td"><label>PAN No.</label>: {{@$pan_number['pan_number'][$i]}}</td>
                                        </tr>
                                        <tr>
                                            <td><label>Date of Birth</label>: {{@$dob['dob'][$i]}}</td>
                                            <td><label>Citizenship / ID No.</label>: {{@$citizenship_no['citizenship_no'][$i]}}</td>
                                            <td class="last-td"><label>Issuing Office & Date:</label></td>
                                        </tr>
                                        <tr>
                                            <td><label>Gender</label> : {{@$gender['gender'] [$i] }}</td>
                                            <td rowspan="2"><label>Passport No.</label>: {{@$passport_no['passport_no'][$i]}}</td>
                                            <td class="last-td"><label>Issuing Office & Date:</label></td>
                                        </tr>
                                        <tr>
                                            <td><label>Nationality</label> : {{@$nationality['nationality'][$i]}} </td>
                                            <td class="last-td"><label>Passport Expiry Date:</label> {{@$expiry_date['expiry_date'][$i]}}</td>
                                        </tr>
                                        <tr>
                                            <td><label>Phone No.</label> : {{@$phone_number['phone_number'][$i] }}</td>
                                            <td colspan="2" style="border-right: 0;">
                                                <table class="table-width">
                                                    <tr>

                                                        <td><label>Marital Status</label> : {{@$marital_status['marital_status'][$i]}}</td>
                                                        <td><label>Mobile No.</label> : {{@$mobile['mobile'][$i]}}</td>
                                                        <td class="last-td"><label>Occupation</label> : {{strtoupper(@$occupation['occupation'][$i])}}</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr class="last-row">
                                            <td colspan="2"><label>Email</label> : {{@$email['email'][$i]}}</td>
                                            <td class="last-td"><label>PO Box </label> : {{@$pox_box['pox_box'][$i]}} </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            {{-- yaha bata --}}
                            <tr>
                                <td class="address-td">
                                    <table class="table-width">
                                        <tr>
                                            <th colspan="2">Present Address</th>
                                        </tr>

                                        <tr>
                                            <td colspan="2">
                                                <label>Ward No.:</label> {{ @$correspondence_address_ward_number['correspondence_address_ward_number'][$i] }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <label>Tole.:</label> {{ @$correspondence_address_street['correspondence_address_street'][$i] }}

                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <label>House No.:</label> {{ @$correspondence_address_house_no['correspondence_address_house_no'][$i] }}

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>District:</label> {{ @$correspondence_address_district['correspondence_address_district'][$i] }}

                                            </td>
                                            <td>
                                                <label>Province No.:</label>
                                                <label>_________</label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table class="table-width">
                                        <tr>
                                            <th colspan="2">Permanent Address</th>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <label>Ward No.:</label> {{ @$permanent_address_ward_number['permanent_address_ward_number'][$i] }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <label>Tole.:</label>{{ @$permanent_address_street['permanent_address_street'][$i] }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <label>House No.:</label>{{ @$permanent_address_house_no['permanent_address_house_no'][$i] }}

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>District:</label> {{ @$permanent_address_district['permanent_address_district'][$i] }}
                                            </td>
                                            <td>
                                                <label>Province No.:</label>
                                                <label>___________</label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <!-- third row of first main table -->
                        <table border="1" class="table-width table-wrap main">
                            <tr>
                                <td>
                                    @if($account_detail['nrn_card'])

                                        <table class="table-width">
                                            <tr>
                                                <th colspan="2" class="table-head">In case of Non Residence</th>

                                            </tr>
                                            <tr>
                                                <td>NRN ID (If applicable):</td>
                                                <td>{{@$nrn_card['nrn_card'][$i]}}  </td>
                                            </tr>
                                            <tr>

                                                <td>Foreign Address:</td>
                                                <td>{{ @$foreign_address['foreign_address'][$i] }}  </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>Country: </label> {{ @$country['country'][$i] }}  

                                                </td>
                                                <td>
                                                    <label>City/state: </label> {{ @$city_state['city_state'][$i] }}

                                                </td>

                                            </tr>
                                            <tr>
                                                <td>Contact No.:</td>
                                                <td>{{ @$contact_no['contact_no'][$i] }}  </td>

                                            </tr>
                                            <tr>
                                                <td>Type of Visa:</td>
                                                <td>{{ @$type_of_visa['type_of_visa'][$i]  }}  </td>

                                            </tr>
                                            <tr>

                                                <td>Visa Expiry Date:</td>
                                                <td>{{ @$visa_expiry_date['visa_expiry_date'][$i] }} </td>

                                            </tr>
                                        </table>
                                  @endif
                                </td>
                                <td>

                                    <table class="table-width">
                                        <tr>

                                            <th colspan="2" class="table-head">Beneficial Owner</th>
                                            <td>
                                                <input type="checkbox" disabled @if(@$kyc['if_beneficial_owner'] == 1) {{ "checked" }} @endif >
                                                <label>Yes</label>
                                            </td>
                                            <td>
                                                <input type="checkbox" disabled @if(@$kyc['if_beneficial_owner'] == 0) {{"checked" }} >
                                                <label>No</label>
                                            </td>

                                        </tr>
                                        @if(@$kyc['if_beneficial_owner'] == 1 )
                                            <tr>

                                                <td colspan="4">If Yes,</td>
                                            </tr>
                                            <tr>

                                                <td colspan="2">Beneficial Owner Name:</td>
                                                <td colspan="2">
                                                    <label><span>{{ strtoupper(@$kyc['mother_name']) }}</span></label>
                                                </td>

                                            </tr>
                                            <tr>

                                                <td colspan="2">Citizenship No.:</td>
                                                <td colspan="2">
                                                    <label><span>{{ strtoupper(@$kyc['beneficial_citizen']) }} </span></label>
                                                </td>
                                            </tr>
                                            <tr>

                                                <td colspan="2">Address:</td>
                                                <td colspan="2">
                                                    <label><span>{{ strtoupper(@$kyc['beneficial_address']) }} </span></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                    <td colspan="2">Citizenship No.:</td>
                                                    <td colspan="2">
                                                        <label><span>{{  strtoupper(@$kyc['beneficial_citizen']) }}</span></label>
                                                    </td>
                                                </tr>
                                                <tr>
    
                                                    <td colspan="2">Address:</td>
                                                    <td colspan="2">
                                                        <label><span>{{ strtoupper(@$kyc['beneficial_address']) }} </span></label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                        <td colspan="2">Relation:</td>
                                                        <td colspan="2">
                                                            <label><span>{{ strtoupper(@$kyc['beneficial_relation']) }} </span></label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">Contact No.:</td>
                                                        <td colspan="2">
                                                            <label><span>{{ strtoupper(@$kyc['beneficial_contact']) }} </span></label>
                                                        </td>
                                                    </tr>
                                                     @else
                                                     <tr>

                                                            <td colspan="2">Beneficial Owner Name:</td>
                                                            <td colspan="2">
                                                                <label>_______________________</label>
                                                            </td>
        
                                                        </tr>
                                                        <tr>
        
                                                            <td colspan="2">Citizenship No.:</td>
                                                            <td colspan="2">
                                                                <label>_______________________</label>
                                                            </td>
                                                        </tr>
                                                        <tr>
        
                                                            <td colspan="2">Address:</td>
                                                            <td colspan="2">
                                                                <label>_______________________</label>
                                                            </td>
                                                        </tr>
                                                        <tr>
        
                                                            <td colspan="2">Relation:</td>
                                                            <td colspan="2">
                                                                <label>_______________________</label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">Contact No.:</td>
                                                            <td colspan="2">
                                                                <label>_______________________</label>
                                                            </td>
        
                                                        </tr>
                                                     @endif
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
            
            
                                    </td>
                                </tr>
                                <tr>
                                    <td>Family Members</td>
                                </tr>
            
                                <tr>
                                    <td>
                                        <table border="1" class="table-width table-wrap main space table-height">
                                            <tr>
                                                <th class="thead-font">SN</th>
                                                <th class="thead-font">Relation</th>
                                                <th class="thead-font">Name & Surname</th>
                                                <th class="thead-font">Citizenship No.</th>
                                                <th class="thead-font">Issuing Office</th>
                                                <th class="thead-font">Date of Issue</th>
                                            </tr>
                                            <tr>
                                                <td class="sn-align">1</td>
                                                <td>Spouse</td>
                                                <td>{{ strtoupper(@$spouse_name['spouse_name'][$i]) }}</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td class="sn-align">2</td>
                                                <td>Father</td>
                                                <td>{{ strtoupper(@$father_name['father_name'][$i]) }}</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td class="sn-align">3</td>
                                                <td>Mother</td>
                                                <td>{{ strtoupper(@$kyc['mother_name']) }}</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td class="sn-align">4</td>
                                                <td>Grandfather</td>
                                                <td>{{ strtoupper(@$grandfather_name['grandfather_name'][$i]) }}</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td class="sn-align">5</td>
                                                <td>Grandmother</td>
                                                <td>{{ strtoupper(@$kyc['grandmother_name']) }}</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>

                                            @php
                                            $son = json_decode($account_detail['son_name'],true);
                                            $i=0; 
                                            @endphp
                                            @foreach($son['son_name'] as $name)  
                                            
                                              <tr>
                                                <td class="sn-align">6</td>
                                                <td>Son @if(count(@$son['son_name']) > 1) {{ $i+1 }} @endif</td>
                                                <td>{{  strtoupper(@$son['son_name'][$i]) }} </td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
            
                                            </tr>
                                            @php $i++;  @endphp
                                            @endforeach
            
                                            @php   
                                            $daughter = json_decode($account_detail['daughter_name'],true);
                                            $i=0; 
                                            @endphp
                                            @foreach($daughter['daughter_name'] as $name)   
                                              <tr>
                                                <td class="sn-align">7</td>
                                                <td>Daughter @if(count(@$daughter['daughter_name']) > 1) {{ $i+1 }} @endif</td>
                                                <td>{{  strtoupper(@$daughter['daughter_name'][$i]) }}</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
            
                                            </tr>
                                            @php $i++;   @endphp
                                            @endforeach
            
            
                                            @php  
                                            $daughter_in_law = json_decode($account_detail['daughter_in_law_name'],true);
                                            $i=0;
                                            @endphp
                                            @foreach($daughter_in_law['daughter_in_law_name'] as $name)  
                                              <tr>
                                                <td class="sn-align">8</td>
                                                <td>Daughter in Law @if(@count($daughter_in_law['daughter_in_law_name']) > 1) {{ $i+1 }} @endif</td>
                                                <td>{{ strtoupper(@$daughter_in_law['daughter_in_law_name'][$i]) }} </td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            @php $i++; @endphp
                                            @endforeach 
                                            <tr>
                                                <td class="sn-align">9</td>
                                                <td>Father in Law</td>
                                                <td>{{ strtoupper(@$account_detail['father_in_law_name']) }}</td>
                                                <td></td>                                                            <td></td>
                                                <td></td>
                                            </tr>
                                            </table>
                                            </td>
                                            </tr>
                                            <tr>
                                                    <td>Occupation/Business</td>
                                            </tr>           
                                            <tr>
                                               <td>                           
                                                <table border="1" class="table-width table-wrap main space table-height">
                                                    <tr>
                                                        <th class="thead-font">SN</th>
                                                        <th class="thead-font">Name of Firm/Company/Office</th>
                                                        <th class="thead-font">Address</th>
                                                        <th class="thead-font">Web Site</th>
                                                        <th class="thead-font">Post</th>
                                                        <th class="thead-font">Expected Annual</th>
                                                    </tr>
                                                    @php
                                                    $i = 0 @endphp
                                                    @if(is_string($occupation['occupation'][$i]) && is_array(json_decode($occupation['occupation'][$i], true)) &&  (json_last_error() == JSON_ERROR_NONE))
                                                        $occupation_array = json_decode($occupation['occupation'][$i],true);   
                                                    <tr>
                                                            <td class="sn-align">1</td>
                                                            <td>{{ @$occupation_array['company_name'] }}</td>
                                                            <td>{{@$occupation_array['company_address'] }}</td>
                                                            <td></td>
                                                            <td>{{ @$occupation_array['company_position'] }}</td>
                                                            <td></td>
                                                        </tr>
                                                        @endif
                                                        <tr>
                                                            <td class="sn-align">2</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                                <td class="sn-align">3</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="sn-align">4</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="6">
                                                                    <table>
                                                                        <tr>
                                                                            <td>Are you civil servant/high position/politican/Relatives of politician</td>
                                                                            <td>
                                                                                <input type="checkbox" disabled>
                                                                                <label>Yes</label>
                                                                            </td>
                                                                            <td class="no">
                                                                                <input type="checkbox" disabled>
                                                                                <label>No</label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="6">
                                                                    <table class="table-width">
                                                                        <tr>
                                                                            <td>Expected Monthly Turnover:</td>
                                                                            <td>
                                                                                <input type="checkbox" disabled @if(@$kyc['expected_monthly_turnover'] == "Above Rs.100.00 Lac"){{ "checked" }} @endif>
                                                                                <label>Above Rs.100.00 Lakh</label>
                                                                            </td>
                                                                            <td>
                                                                                <input type="checkbox" disabled @if(@$kyc['expected_monthly_turnover'] == "Between Rs.50.00 Lac to Below Rs.100.00 Lac"){{ "checked" }} @endif  >
                                                                                <label>Between Rs.50.00 Lac to Below Rs.100.00 Lac</label>
                                                                            </td>
                                                                            <td>
                                                                                <input type="checkbox" disabled @if(@$kyc['expected_monthly_turnover'] == "Between Rs.20.00 Lac to Below Rs.50.00 Lac"){{ "checked"}} @endif  >
                                                                                <label>Between Rs.20.00 Lac to Below Rs.50.00 Lac</label>
                                                                            </td>
                                                                            <td>
                                                                                <input type="checkbox"disabled @if(@$kyc['expected_monthly_turnover'] == "Below Rs.20.00 Lac") {{ "checked" }} @endif >
                                                                                <label>Below Rs.20.00 Lac</label>
                                                                            </td>                
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>

                                       
                                                                    <td colspan="6">
                                                                     <table class="table-width">
                                                                         <tr>
                                                                             <td>Expected Monthly No. of Transaction:</td>
                                                                             <td>
                                                                                 <input type="checkbox" disabled  @if(trim(@$kyc['expected_monthly_transaction']) == "Below 25 Transactions"){{ "checked" }} @endif >
                                                                                 <label>Below 25 Transactions</label>
                                                                             </td>
                                                                             <td>
                                                                                 <input type="checkbox" disabled @if(trim(@$kyc['expected_monthly_transaction']) == "Between 25 to Below 50 Transactions"){{ "checked" }} @endif>
                                                                                 <label>Between 25 to Below 50 Transactions</label>
                                                                             </td>
                                                                             <td>
                                                                                 <input type="checkbox" disabled @if(trim(@$kyc['expected_monthly_transaction']) == "Between 50 to Below 100 Transactions"){{ "checked" }} @endif  >
                                                                                 <label> Between 50 to Below 100 Transactions</label>
                                                                             </td>
                                                                             <td>
                                                                                 <input type="checkbox" disabled @if(trim(@$kyc['expected_monthly_transaction']) == "Above 100 Transactions"){{ "checked" }} @endif>
                                                                                 <label> Above 100 Transactions</label>
                                                                             </td>
                                                                         </tr>
                                                                     </table>
                                                                 </td>
                                                             </tr>

                                <tr>
                                        <td colspan="6">
                                            <table class="table-width">
                                                <tr>
                                                    <td>Purpose of Account:</td>
                                                    <td>
                                                        <input type="checkbox" disabled @if(@$kyc['purpose_of_account']== "Remittance"){{"checked"    }} @endif>
                                                        <label>Remittance</label>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" disabled @if(@$kyc['purpose_of_account']== "Saving"){{"checked"}} @endif  >
    
                                                        <label>Savings</label>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" disabled @if(@$kyc['purpose_of_account']== "Business"){{"checked"    }} @endif>
                                                        <label>Business</label>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" disabled @if(@$kyc['purpose_of_account']== "Others"){{"checked"    }} @endif>
                                                        <label>Others</label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <table class="table-width">
                                                <tr>
                                                    <td>
                                                        <div class="source-of-fund-label">
                                                            <label>Source of Fund</label>
                                                        </div>
                                                    </td>
                                                    <td style="padding-top: 9px;">
                                                        <div class="source-of-fund">    
                                                            <div class="source-of-fund-align">
                                                                <input type="checkbox" disabled @if(@$kyc['source_of_fund']== "Business Income"){{ "checked"}} @endif >
                                                                <label>Business Income</label>
                                                            </div>
                                                            <div class="source-of-fund-align">
                                                                <input type="checkbox" disabled @if(@$kyc['source_of_fund']== "Rental Income"){{ "checked"}}@endif >
                                                                <label>Rental Income</label>
                                                            </div>
                                                            <div class="source-of-fund-align">
                                                                <input type="checkbox" disabled @if(@$kyc['source_of_fund']== "Salary Income"){{ "checked" }}@endif >
                                                                <label>Salary Income</label>
                                                            </div>
                                                            <div class="source-of-fund-align">
                                                                <input type="checkbox" disabled @if(@$kyc['source_of_fund']== "Others"){{ "checked" }} @endif>
                                                                <label>Others</label>
                                                            </div> 
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <table>
                                                <tr>
                                                    <td>Punished or charged for any criminal activities in the past?</td>
                                                    <td>
                                                        <input type="checkbox" disabled >
                                                        <label>Yes</label>
                                                    </td>
                                                    <td class="no">
                                                        <input type="checkbox" disabled >
                                                        <label>No</label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>                      
                                </table>
                            </td>
                        </tr>
    
                    <!------------Site Map  ------------------>
                    <tr>
                        <td>
                            <table class="table-width">
                                <tr>
                                    <td>Site Map</td>
                                    <td>
                                        <input type="checkbox" disabled>
                                        <label>Permanent Address</label>
                                    </td>
                                    <td>
                                        <input type="checkbox" disabled >
                                        <label>Present Address</label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table border="1" class="table-width table-wrap main">
                                <tr>
                                    <td class="site-map">
                                        <div id=" map{{ $index }}"></div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="table-width">
                                <tr>
                                    <td>I/We hereby declare that all the information & documents provided to the bank are true &
                                    Correct.</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                      <!-- thumb print -->
                      <tr>
                            <td>
                                <table class="table-width">
                                    <tr>
                                        <td>
                                            <table class="thumb-width">
                                                <tr>
                                                    <td>Right</td>
                                                    <td>Left</td>
                                                </tr>
    
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table border="1" class="thumb-width table-wrap main">
                                                <tr class="thumb-row">
                                                    <td class="left"></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                            <td>
                                                    <table class="table-width">
                                                        <tr>
                                                            <td class="thumb-impression">Thumb Impression</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="ac-holder-signature">
                                                                <span class="signature">Account Holder's Signature</span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="table-width">
                                            <tr>
                                                <td>Note: Any document/information if not exists, shall be declared an N/A.</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bank-head">
                                        <h2>Bank's Use Only</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table border="1" class="table-width table-wrap bank-use main">
                                            <tr>
                                                <td colspan="4">Supporting Documents (Provided by the customer)</td>
                                            </tr>
                                            <tr>
                                                <td>Photo of account holder</td>
                                                <td>
                                                    <input type="checkbox" disabled>
                                                    <label>Obtained</label>
                                                </td>
                                                <td colspan="2">
                                                    <input type="checkbox" disabled>
                                                    <label>Not obtained</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Photo of beneficial owner</td>
                                                <td>
                                                    <input type="checkbox" disabled>
                                                    <label>Obtained</label>
                                                </td>
                                                <td colspan="2">
                                                    <input type="checkbox" disabled>
                                                    <label>Not obtained</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Identificaion document</td>
                                                <td class="vert-align">
                                                    <input type="checkbox" disabled>
                                                    <label>Citizenship</label>
                                                </td>
                                                <td class="vert-align">
                                                    <input type="checkbox" disabled>
                                                    <label>Passport</label>
                                                </td>
                                                <td>
                                                    <input type="checkbox" disabled>
                                                    <label>Others ____________</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Address verifying document(Any one)</td>
                                                <td class="vert-align">
            
                                                    <input type="checkbox" disabled>
                                                    <label>Utility Bill (Water/Electricity/Telephone Bill)</label>
                                                </td>
            
            
                                                <td class="vert-align">
                                                        <input type="checkbox" disabled>
                                                        <label>Driving License</label>
                                                    </td>
                
                
                
                                                    <td>
                                                        <input type="checkbox" disabled>
                                                        <label>Land ownership document</label>
                                                    </td>
                
                
                
                
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td class="vert-align">
                
                                                        <input type="checkbox" disabled>
                                                        <label>Rental Agreement</label>
                
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" disabled>
                                                        <label>letter from local authority</label>
                                                    </td>
                
                
                                                    <td class="vert-align">
                                                        <input type="checkbox" disabled>
                                                        <label>Voter ID</label>
                                                    </td>
                
                                                </tr>
                                                <tr>
                                                    <td>Employee ID (Mandatory for Govt. Officials)</td>
                                                    <td>
                                                        <input type="checkbox" disabled>
                                                        <label>N/A</label>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" disabled>
                                                        <label>Yes</label>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" disabled>
                                                        <label>No</label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                               
                                <tr>
                                        <td class="bank-second">
                                            <table border="1" class="table-width table-wrap bank-use main">
                                                <tr>
                                                    <td class="bank-td">
                                                        <table class="table-width">
                                                            <tr>
                                                                <td colspan="3" class="ac-risk-grading">Account Risk Grading</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <input type="checkbox" @if(@$kyc['is_high_risk_customer'] == 1 ){{ "checked" }} @endif>
                                                                    <label>High Risk</label>
                                                                </td>
                                                                <td>
                                                                    <input type="checkbox" disabled>
                                                                    <label>Medium Risk</label>
                                                                </td>
                                                                <td>
                                                                    <input type="checkbox"disabled >
                                                                    <label>Low Risk</label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <input type="checkbox" disabled>
                                                                    <label>HPP</label>
                                                                </td>
                                                                <td colspan="2">
                                                                    <input type="checkbox" disabled>
                                                                    <label>PEP</label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">Name list in Sanction</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <input type="checkbox" disabled>
                                                                    <label>Yes</label>
                                                                </td>
                                                                <td colspan="2">
                                                                    <input type="checkbox" disabled>
                                                                    <label>No</label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" class="remarks">Remarks / information if any:</td>
                                                            </tr>
                                                            <tr></tr>
                                                            <tr>
                                                                <td colspan="3">Branch Manager</td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">Date:</td>
                                                            </tr>
                
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <table class="table-width">
                                                            <tr>
                                                                <td colspan="2">Information Update in Core Banking System & accuity Check:</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <input type="checkbox" disabled>
                                                                    <label>Yes</label>
                                                                </td>
                                                                <td>
                                                                    <input type="checkbox" disabled>
                                                                    <label>No</label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" class="date-update">Date Updated on: _______________________</td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                            </tr>
                
                                                            <td colspan="2" class="remarks">Remarks if any:</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">_____________</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">CSD Staff</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">Date:</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                </table>
        @endif
@endforeach
         </section>

<script src="{{ URL::to('js/jquery.js') }}"></script>
<script>
    var lati_s = '{{ $account_detail['latitude'] }}';
    var longi_s = '{{ $account_detail['longitude'] }}';
    var lati= parseFloat(lati_s);
    var longi=parseFloat(longi_s);
    function initMap(){

        @foreach($account_name['account_name'] as $index => $value) 
        var uluru = {lat: lati, lng: longi};
        
        var map{{ $index }}  = new google.maps.Map(document.getElementById(' map{{ $index }} '), {
          zoom: 19,
          center: uluru 
        });
        
        var marker = new google.maps.Marker({
            position: uluru,
            map : map{{ $index }}
        });

       @endforeach
        $(function(){
        google.maps.event.trigger(map, "resize");
    });  
    }
    
</script>
<script async defer  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8xUqFu-Te4JAwIsNP25J6b59cfxbjgyQ&callback=initMap&libraries=places">
</script>
