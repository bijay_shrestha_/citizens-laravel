<?php

namespace App\Modules\Faq\Model;


use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    public  $table = 'tbl_faq';

    protected $fillable = ['id','collection_id','for_type','heading','contain','redirect','approvel_status','approved','added_by','added_date','modified_date','del_flag','deleted_by','deleted_date','status',];
}
