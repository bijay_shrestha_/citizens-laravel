<?php



Route::group(array('prefix'=>'admin/','module'=>'faq','middleware' => ['web','auth'], 'namespace' => 'App\modules\faq\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('faqs/','AdminFaqController@index')->name('admin.faqs');
    Route::post('faqs/getfaqsJson','AdminFaqController@getfaqsJson')->name('admin.faqs.getdatajson');
    Route::get('faqs/create','AdminFaqController@create')->name('admin.faqs.create');
    Route::post('faqs/store','AdminFaqController@store')->name('admin.faqs.store');
    Route::get('faqs/show/{id}','AdminFaqController@show')->name('admin.faqs.show');
    Route::get('faqs/edit/{id}','AdminFaqController@edit')->name('admin.faqs.edit');
    Route::match(['put', 'patch'], 'faqs/update/{id}','AdminFaqController@update')->name('admin.faqs.update');
    Route::get('faqs/delete/{id}', 'AdminFaqController@destroy')->name('admin.faqs.edit');
    Route::get('faqs/change/{text}/{status}/{id}', 'AdminFaqController@change');

});




Route::group(array('module'=>'faq','namespace' => 'App\modules\faq\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('faqs/','FaqController@index')->name('faqs');
    
});