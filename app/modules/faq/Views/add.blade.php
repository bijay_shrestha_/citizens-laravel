@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Faqs   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.faqs') }}">tbl_faq</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.faqs.store') }}"  method="post">
                <div class="box-body">                
         
                    <div class="form-group">
                        <label for="collection_id">Type</label>
                        <select name="collection_id" class="form-control">
                        @foreach($collections as $collection) 
                                <option value="{{$collection['faq_collection_id']}}"
                                 @if (old('collection_id') == $collection['faq_collection_id']) selected @endif
                                >{{$collection['faq_collection_name']}}</option>
                        @endforeach
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label for="heading">Heading (*)</label><textarea name="heading" id="heading" class="form-control my-editor"></textarea>
                    </div>
                    
                    <div class="form-group">
                        <label for="contain">Contain (*)</label><textarea name="contain" id="contain" class="form-control my-editor" ></textarea>
                    </div>
                    
                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="status" id="status1" @if(old('status') == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="status" id="status0" @if(old('status') == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>

                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.faqs') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
