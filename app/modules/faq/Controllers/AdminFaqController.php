<?php

namespace App\modules\faq\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\faq\Model\Faq;
use Carbon\Carbon;
use App\modules\faq_collection\Model\Faq_collection;

class AdminFaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Faq';
        return view("faq::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getfaqsJson(Request $request)
    {
        $faq = new Faq;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $faq->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('id', "DSC")->get();

        // Display limited list        
        $rows = $faq->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->orderBy('id', "DSC")->where('del_flag',0)->get();

        //To count the total values present
        $total = $faq->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Faq | Create';
        $collections = Faq_collection::all();
        return view("faq::add",compact('page', 'collections'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $dataforfaq = $data;
        $dataforfaq['added_by'] = Auth::user()->id;
        $dataforfaq['added_date'] = new Carbon();
        $dataforfaq['del_flag'] = 0;
        $dataforfaq = check_case($dataforfaq, 'create');
        $success = Faq::Insert($dataforfaq);
        $faqs = Faq::all();
        $faq_id = $faqs[count($faqs)-1]['id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "FAQ", 'file_id' => $faq_id]); //create acrivity log     
        return redirect()->route('admin.faqs')->with('success', "Data added Successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faq = Faq::findOrFail($id);
        $page['title'] = 'Faq | Update';
        $collections = Faq_collection::all();
        return view("faq::edit",compact('page','faq', 'collections'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $executive = Faq::where('id', $id)->firstOrFail();
        $dataforfaq = $data;
        $dataforfaq['modified_date'] = new Carbon();
        $dataforfaq = check_case($dataforfaq, 'edit');
        DB::table('tbl_faq')->where('id', $id)->update($dataforfaq);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "FAQ", 'file_id' => $id]); //create acrivity log     
        return redirect()->route('admin.faqs')->with('success', "Data updated Successfully.");
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_faq', 'id', $id, 'deleted_by', 'deleted_date');
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "FAQ", 'file_id' => $id]); //create acrivity log     
        return redirect()->route('admin.faqs')->with('success', "Data deleted successfully.");
        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_faq', 'id', 'deleted_by', 'deleted_date');
        return redirect()->route('admin.faqs')->with('success', "Data updated successfully.");
    }

}
