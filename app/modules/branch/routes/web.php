<?php



Route::group(array('prefix'=>'admin/','module'=>'branch','middleware' => ['web','auth'], 'namespace' => 'App\modules\branch\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('branches/','AdminBranchController@index')->name('admin.branches');
    Route::post('branches/getbranchesJson','AdminBranchController@getbranchesJson')->name('admin.branches.getdatajson');
    Route::get('branches/create','AdminBranchController@create')->name('admin.branches.create');
    Route::post('branches/store','AdminBranchController@store')->name('admin.branches.store');
    Route::get('branches/show/{id}','AdminBranchController@show')->name('admin.branches.show');
    Route::get('branches/edit/{id}','AdminBranchController@edit')->name('admin.branches.edit');
    Route::match(['put', 'patch'], 'branches/update/{id}','AdminBranchController@update')->name('admin.branches.update');
    Route::get('branches/delete/{id}', 'AdminBranchController@destroy')->name('admin.branches.edit');
    Route::get('branches/change/{text}/{status}/{id}', 'AdminBranchController@change');
});




Route::group(array('module'=>'branch','namespace' => 'App\modules\branch\Controllers', 'middleware' => 'web'), function() {
    //Your routes belong to this module.
    //Your routes belong to this module.
    Route::get('branches/','BranchController@index')->name('branches');

    Route::get('branchless/','BranchController@branchless')->name('branchless');
    Route::get('location/','BranchController@showLocation')->name('location');

    
});