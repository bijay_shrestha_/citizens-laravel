<?php

namespace App\Modules\Branch\Model;


use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    public  $table = 'tbl_branches';

    protected $fillable = ['id','name','branch_manager','address','email','contact_no','fax','longitude','latitude','for_section','location','city','state','wheelchair_access','withdrawal_limit','redirect','approvel_status','approved','added_by','added_date','modified_date','del_flag','deleted_by','deleted_date','status','district_id',];
}
