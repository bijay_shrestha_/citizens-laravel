<?php

namespace App\modules\branch\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\branch\Model\Branch;
use Carbon\Carbon;

class AdminBranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Branch';
        return view("branch::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getbranchesJson(Request $request)
    {
        $branch = new Branch;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $branch->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('id', "DSC")->get();

        // Display limited list        
        $rows = $branch->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->orderBy('id', "DSC")->get();

        //To count the total values present
        $total = $branch->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Branch | Create';
        $districts = DB::table('tbl_district')->get()->toArray();
        return view("branch::add",compact('page', 'districts'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $data['state'] = "Nepal";

        $data['del_flag'] = 0;
        $data['added_by'] = Auth::user()->id;
        $data['added_date'] = new Carbon();
        $data['wheelchair_access'] = 0;
        $data['withdrawal_limit'] = 1;
        $data['redirect'] = 'branch';        
        $data = check_case($data, 'create');
        $success = Branch::Insert($data);
        return redirect()->route('admin.branches')->with('success', "Data added successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branch = Branch::findOrFail($id);
        $page['title'] = 'Branch | Update';
        $districts = DB::table('tbl_district')->get()->toArray();
        return view("branch::edit",compact('page','branch', 'districts'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $data['state'] = "Nepal";
        $data['added_by'] = Auth::user()->id;
        $data['modified_date'] = new Carbon();
        $data = check_case($data, 'edit');
        DB::table('tbl_branches')->where('id', $id)->update($data);
        return redirect()->route('admin.branches')->with('success', "Data updated successfully.");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_branches', 'id', $id, 'deleted_by', 'deleted_date');
        return redirect()->route('admin.branches')->with('success', "Data deleted successfully.");

    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_branches', 'id', 'deleted_by', 'deleted_date');
        return redirect()->route('admin.branches')->with('success', "Data updated successfully.");
    }
}
