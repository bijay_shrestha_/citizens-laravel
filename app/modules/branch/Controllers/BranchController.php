<?php

namespace App\modules\branch\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\banner\Model\Banner;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function index()
    {
        $page['title'] = 'branches';
        // if($this->session->userdata('activeMenu')!='CONTACTUS')
        //  {   
        //      $this->session->unset_userdata('activeMenu');
        //      $this->session->set_userdata('activeMenu','CONTACTUS');
        //      redirect(site_url('branch'));
        //  }
        $table="tbl_branches";

        //$data['atm_locations']=$this->branch_model->getBranches(array('del_flag'=>0,'status'=>1,'for_section'=>'ATM locations','location'=>'INSIDE'),'name asc')->result_array();
        $atm_locations=DB::table('tbl_branches')->where([['del_flag', "0"],['status', "1"],['for_section','ATM locations'],['location','INSIDE']])->orderBy('name','asc')->get();
        $atms_outside=DB::table('tbl_branches')->where([['del_flag', "0"],['status', "1"],['for_section','ATM locations'],['location','OUTSIDE']])->orderBy('name','asc')->get();

        //$data['atms_outside']=$this->branch_model->getBranches(array('del_flag'=>0,'status'=>1,'for_section'=>'ATM locations','location'=>'OUTSIDE'),'name asc')->result_array();
        //$data['all_branches']=$this->branch_model->getBranches(array('del_flag'=>0,'status'=>1,'for_section'=>'Bank Branches'),'name asc')->result_array();
        
        $all_branches=DB::table('tbl_branches')->where([['del_flag', "0"],['status', "1"],['for_section','Bank Branches']])->orderBy('name','asc')->get()->toArray();

        //$data['branches_inside']=$this->branch_model->getBranches(array('del_flag'=>0,'status'=>1,'for_section'=>'Bank Branches'),'name asc')->result_array();

        $branches_inside=DB::table('tbl_branches')->where([['del_flag', "0"],['status', "1"],['for_section','Bank Branches'],['location','INSIDE']])->orderBy('name','asc')->get()->toArray();
        
        
        //$data['banners']=$this->banner_model->getBanners(array('banner_collection_id'=>7))->result_array();
        
        $branches_outside=DB::table('tbl_branches')->where([['del_flag', "0"],['status', "1"],['for_section','Bank Branches'],['location','OUTSIDE']])->orderBy('name','asc')->get()->toArray();

        //$data['branches_outside']=$this->branch_model->getBranches(array('del_flag'=>0,'status'=>1,'for_section'=>'Bank Branches','location'=>'OUTSIDE'),'name asc')->result_array();
        //$data['remit_locations']=$this->branch_model->getBranches(array('del_flag'=>0,'status'=>1,'for_section'=>'Himal Remit'),'name asc')->result_array();
        
        $dataremit_locations=DB::table('tbl_branches')->where([['del_flag', "0"],['status', "1"],['for_section','Himal Remit']])->orderBy('name','asc')->get()->toArray();
        
        //$data['banners']=DB::table('')
        $banners=DB::table('tbl_banners')->where('banner_collection_id',7)->where('del_flag',0)->get();

        //$data['banners']=$this->banner_model->getBanners(array('banner_collection_id'=>7))->result_array();
        $header = "Home";
//        dd($branches_outside, $branches_inside);
        //$data['view_page'] = 'branch/branch';
        
        return view('front.branch.branch',compact('page','atm_locations','atms_outside','all_branches','branches_inside','branches_outside','dataremit_locations','banners','header'));
    }


    public function showLocation($type = 'branch'){
       //$this->db->where("((approvel_status = 'ok' AND approved = 'approved') OR (approvel_status ='delete' And approved='denied'))");
        //$data['atms_outside']=$this->branch_model->getBranches(array('del_flag'=>0,'status'=>1,'for_section'=>'ATM locations','location'=>'OUTSIDE'),'name asc')->result_array();
        $atms_outside=DB::table('tbl_branches')->where([['del_flag', "0"],['status', "1"],['for_section','ATM locations'],['location','OUTSIDE']])->orderBy('name','asc')->get()->toArray();
        $atms_outside=objToArray($atms_outside);
       //$this->db->where("((approvel_status = 'ok' AND approved = 'approved') OR (approvel_status ='delete' And approved='denied'))");
        //$data['atms_inside']=$this->branch_model->getBranches(array('del_flag'=>0,'status'=>1,'for_section'=>'ATM locations','location'=>'INSIDE'),'name asc')->result_array();
        $atms_inside=DB::table('tbl_branches')->where([['del_flag', "0"],['status', "1"],['for_section','ATM locations'],['location','INSIDE']])->orderBy('name','asc')->get()->toArray();
        $atms_inside = objToArray($atms_inside);

        //$this->db->where("((approvel_status = 'ok' AND approved = 'approved') OR (approvel_status ='delete' And approved='denied'))");
        //$data['branches_outside']=$this->branch_model->getBranches(array('del_flag'=>0,'status'=>1,'for_section'=>'Bank Branches','location'=>'OUTSIDE'),'name asc')->result_array();
        $branches_outside=DB::table('tbl_branches')->where([['del_flag', "0"],['status', "1"],['for_section','Bank Branches'],['location','OUTSIDE']])->orderBy('name','asc')->get()->toArray();
        $branches_outside= objToArray($branches_outside);
        //$this->db->where("((approvel_status = 'ok' AND approved = 'approved') OR (approvel_status ='delete' And approved='denied'))");
        //$data['branches_inside']=$this->branch_model->getBranches(array('del_flag'=>0,'status'=>1,'for_section'=>'Bank Branches','location'=>'INSIDE'),'name asc')->result_array();
        $branches_inside=DB::table('tbl_branches')->where([['del_flag', "0"],['status', "1"],['for_section','Bank Branches'],['location','INSIDE']])->orderBy('name','asc')->get()->toArray();
        $branches_inside = objtoArray($branches_inside);
        $branch_banners= Banner::where('banner_collection_id',7)->where('del_flag',0)->get()->toArray();

        //$data['banners']=DB::table('tbl_banners')->where('banner_collection_id',7)->where('del_flag',0)->get()->toArray();
        // $data['branch_banners']=$this->banner_model->getBanners(array('banner_collection_id'=>7,'del_flag'=>0))->result_array();

        $atm_banners=DB::table('tbl_banners')->where('banner_collection_id',141)->where('del_flag',0)->get()->toArray();
        $atm_banners = objToArray($atm_banners);

        //$data['atm_banners']=$this->banner_model->getBanners(array('banner_collection_id'=>141,'del_flag'=>0))->result_array();
        //$this->db->where("((approvel_status = 'ok' AND approved = 'approved') OR (approvel_status ='delete' And approved='denied')) AND del_flag = 0");
        //$data['geolocations'] = $this->branch_model->getBranches()->result_array();
        $geolocations = DB::table('tbl_branches')->get()->toArray();
        $geolocations = objToArray($geolocations);
        // echo "<pre>";
        // print_r($data['geolocations'] );
        // exit;
        $type = $type;
        $header = "Home";
        $view_page = 'branch/location';
        return view('front.branch.location',compact('header','banners','view_page','type','geolocations','atm_banners','branches_inside','branches_outside','atms_inside','atms_outside','branch_banners'));
        //$this->load->view($this->_container,$data);
    }
    
    public function branchless(){
        $header = "Home";
        $banners= Banner::where('banner_collection_id',134)->where('del_flag',0)->get()->toArray();
        

        //$data['banners']=$this->banner_model->getBanners(array('banner_collection_id'=>))->result_array();
        $view_page = 'branch/branchless';
        return view('front.branch.branchless',compact('header','banners','view_page'));
        //$this->load->view($this->_container,$data);
    }
    public function saveContact()
    {
            
    //  $this->load->module_model('contact','contact_model');

        
        if(isset($_POST['g-recaptcha-response']) && $_POST['g-recaptcha-response'])
        {
            
            $secret="6Lcz0yMTAAAAADWMT86R_Yb8WSgKonTt0JlS6zhm";
            $ip = $_SERVER['REMOTE_ADDR'];
            $captcha = $_POST['g-recaptcha-response'];
            $rsp = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$captcha&remoteip=$ip");
            $arr = json_decode($rsp,TRUE);
            if($arr['success'])
            {
                $data=array();
                    // $data['contact_id']      = $this->input->post('booking_id');
                $data['name']           = $this->input->post('name');
                $data['email']          = $this->input->post('email');
                $data['date']           = date('Y-m-d');
                $data['message']        = $this->input->post('message');
                $data['branch']         = $this->input->post('branch');
                
                $result = $this->contact_model->insert('CONTACTS',$data);
                
                if($result)
                {
                    $this->_send_mail();
                    flashMsg('success','Thank you for contacting us. We will response you as soon as possible.');
                }
                else
                {   
                    flashMsg('error','Sorry there was some error while sending your message.');         
                }
            
            }
            else
            {
                
                flashMsg('error','you are a bot.');
                
            }
            
            
        }
        else
        {
                flashMsg('error','Please insert captcha');
                
        }
        redirect(site_url('branch'));
        
        
        
    
    }

    function _send_mail()
    {
            $this->load->library('email');

            
                                    
            // $config['protocol'] = 'smtp';
            // $config['smtp_host'] = 'smtp.vianet.com.np';     
            
            $this->email->set_mailtype('html');
            // $this->email->initialize($config);
            $this->email->initialize($this->mail_config());
            $this->email->clear(TRUE);
                

            $id = $this->input->post('branch');

            $branch = $this->branch_model->getBranches(array('id'=>$id,'del_flag'=>0))->row_array(); 
            

            $emails = explode(',',$branch['email']);

            foreach ($emails as $email) {
                $email_to[] = $email;
            }
            
            $subject="Contact from :: " .$this->input->post('name');

            $message="The following person has contacted";
            $message.="<br/>=====================================";
            $message.="<br/>Sender Name: " . $this->input->post('name');
            $message.="<br/>Sender Email: " . $this->input->post('email');
            $message .="</br>Branch: ".$branch['name'];         
            $message.="<br/>Sender Message: " . $this->input->post('message');
            
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->from($this->preference->item('automated_from_email'), $this->preference->item('automated_from_name'));

            if(!empty($email_to))
            {
                //$this->email->to($email_to);
                $this->email->to('crazy.san8@gmail.com');   
            }   
            else
            {
                //$this->email->to($this->preference->item('automated_from_email'));
                    $this->email->to('crazy.san8@gmail.com');
            }
            
            $success = $this->email->send();    
            // echo $success;
            // print_r($email_to);
            // exit;    
    }

}
