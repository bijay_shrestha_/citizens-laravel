@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Branches 
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.branches') }}">branches</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.branches.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                        <label for="name">Name (*)</label><input type="text" name="name" id="name" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="branch_manager">Branch Manager (*)</label><input type="text" name="branch_manager" id="branch_manager" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="address">Address (*)</label><input type="text" name="address" id="address" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="email">Email Address (*)</label><input type="text" name="email" id="email" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="district_id">Select District (*)</label>
                        <select name="district_id" id="district_id" class="form-control" required>
                            @foreach ($districts as $district)
                            <option value="{{$district->d_id}}">{{$district->district}}</option>
                            @endforeach
                        </select>                           
                    </div>
                    
                    <div class="form-group">
                        <select name="location" class="form-control" required>
                            <option value="INSIDE">INSIDE VALLEY</option>
                            <option value="OUTSIDE">OUTSIDE VALLEY</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="contact_no">Contact Number</label><input type="text" name="contact_no" id="contact_no" class="form-control" required>
                    </div>
                    
                    <div class="form-group">
                        <label for="fax">Fax</label><input type="text" name="fax" id="fax" class="form-control" >
                    </div>
                    
                    <div class="form-group">
                        <label for="longitude">Longitude (*)</label><input type="text" name="longitude" id="longitude" class="form-control" required>
                    </div>
                    
                    <div class="form-group">
                        <label for="latitude">Latitude (*)</label><input type="text" name="latitude" id="latitude" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="for_section">For Section (*)</label>
                            <select name="for_section" id="for_section" class="form-control" required>
                              <option value="">Select Section </option>
                              <option value="ATM locations" >ATM locations</option>
                              <option value="Bank Branches" >Bank Branches</option>                                     
                            </select>
                    </div>
                                        
                    <div class="form-group">
                        <label for="city">City</label><input type="text" name="city" id="city" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                            <div class="radio">
                                <label><input type="radio" value="1" name="status" id="status1" checked="checked"/>Yes</label> 
                                <label><input type="radio" value="0" name="status" id="status0" />No</label>
                            </div>
                    </div>
                        <input type="hidden" name="id" id="id"/>
                    
                    </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.branches') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
