<?php

namespace App\modules\loan_parent\Model;


use Illuminate\Database\Eloquent\Model;

class Loan_parent extends Model
{
    public  $table = 'tbl_loan_parent';

    protected $fillable = ['id','name','status','del_flag'];
}
