<?php



Route::group(array('prefix'=>'admin/','module'=>'loan_parent','middleware' => ['web','auth'], 'namespace' => 'App\modules\loan_parent\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('loan_parents/','AdminLoan_parentController@index')->name('admin.loan_parents');
    Route::post('loan_parents/getloan_parentsJson','AdminLoan_parentController@getloan_parentsJson')->name('admin.loan_parents.getdatajson');
    Route::get('loan_parents/create','AdminLoan_parentController@create')->name('admin.loan_parents.create');
    Route::post('loan_parents/store','AdminLoan_parentController@store')->name('admin.loan_parents.store');
    Route::get('loan_parents/show/{id}','AdminLoan_parentController@show')->name('admin.loan_parents.show');
    Route::get('loan_parents/edit/{id}','AdminLoan_parentController@edit')->name('admin.loan_parents.edit');
    Route::match(['put', 'patch'], 'loan_parents/update/{id}','AdminLoan_parentController@update')->name('admin.loan_parents.update');
    Route::get('loan_parents/delete/{id}', 'AdminLoan_parentController@destroy')->name('admin.loan_parents.edit');
});




Route::group(array('module'=>'loan_parent','namespace' => 'App\modules\loan_parent\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('loan_parents/','Loan_parentController@index')->name('loan_parents');
    
});