<?php



Route::group(array('prefix'=>'admin/','module'=>'base_rate','middleware' => ['web','auth'], 'namespace' => 'App\modules\base_rate\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('base_rates/','AdminBase_rateController@index')->name('admin.base_rates');
    Route::post('base_rates/getbase_ratesJson','AdminBase_rateController@getbase_ratesJson')->name('admin.base_rates.getdatajson');
    Route::get('base_rates/create','AdminBase_rateController@create')->name('admin.base_rates.create');
    Route::post('base_rates/store','AdminBase_rateController@store')->name('admin.base_rates.store');
    Route::get('base_rates/show/{id}','AdminBase_rateController@show')->name('admin.base_rates.show');
    Route::get('base_rates/edit/{id}','AdminBase_rateController@edit')->name('admin.base_rates.edit');
    Route::match(['put', 'patch'], 'base_rates/update/{id}','AdminBase_rateController@update')->name('admin.base_rates.update');
    Route::get('base_rates/delete/{id}', 'AdminBase_rateController@destroy')->name('admin.base_rates.edit');
    Route::get('base_rates/change/{text}/{status}/{id}', 'AdminBase_rateController@change');
});




Route::group(array('module'=>'base_rate','namespace' => 'App\modules\base_rate\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('base_rates/','Base_rateController@index')->name('base_rates');
    
});