<?php

namespace App\modules\base_rate\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\base_rate\Model\Base_rate;
use Carbon\Carbon;

class AdminBase_rateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Base Rate';
        return view("base_rate::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getbase_ratesJson(Request $request)
    {
        $base_rate = new Base_rate;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $base_rate->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('base_rate_id', "DSC")->get();

        // Display limited list        
        $rows = $base_rate->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->orderBy('base_rate_id', "DSC")->where('del_flag',0)->get();

        //To count the total values present
        $total = $base_rate->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Base Rate | Create';
        return view("base_rate::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $datafor_base = $data;

        /*if ($datafor_base['date']) {
            $datafor_base['date'] = date('Y-m-d H:i:s',strtotime($data['date']));
        }*/
        $datafor_base['added_by'] = Auth::user()->id;
        $datafor_base['added_date'] = new Carbon();
        $datafor_base['del_flag'] = 0;
        $datafor_base = check_case($datafor_base, 'create');
        unset($datafor_base['download_link']);
        $datafor_base['link'] = $datafor_base['download_form_link'];
        unset($datafor_base['download_form_link']);
        $success = Base_rate::Insert($datafor_base);
        return redirect()->route('admin.base_rates')->with('success', "Data inserted successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $base_rate = Base_rate::where('base_rate_id',$id)->firstOrFail();
        $page['title'] = 'Base Rate | Update';
        return view("base_rate::edit",compact('page','base_rate'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $base_rate = Base_rate::where('base_rate_id',$id)->firstOrFail();
        $datafor_base = $data;

        /*if ($datafor_base['ex_date']) {
            $datafor_base['date'] = date('Y-m-d H:i:s',strtotime($data['date']));
        }*/
        unset($datafor_base['download_link']);

        if (array_key_exists('delete_old_file', $data)) {
            if ((file_exists(public_path().'/uploads/brate/form/'. $base_rate->link)) && $base_rate->link) {
                unlink(public_path().'/uploads/brate/form/'. $base_rate->link);                
            }
            if (!$data['download_form_link']) {
                $data['download_form_link'] = '';
                $datafor_base['download_form_link'] = '';
            }
            unset($datafor_base['delete_old_file']);
        } else {
            if (!$data['download_link']) {
                $data['download_form_link'] = $base_rate->link;
                $datafor_base['download_form_link'] = $base_rate->link;
            }            
        }
        $datafor_base['link'] = $datafor_base['download_form_link'];
        unset($datafor_base['download_form_link']);
        $datafor_base['modified_date'] = new Carbon();
        $datafor_base = check_case($datafor_base, 'edit');
        DB::table('tbl_base_rates')->where('base_rate_id', $id)->update($datafor_base);
        return redirect()->route('admin.base_rates')->with('success', "Data updated successfully.");        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_base_rates', 'base_rate_id', $id, 'deleted_by', 'deleted_date');
        return redirect()->route('admin.base_rates')->with('success', "Data deleted successfully.");
        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_base_rates', 'base_rate_id', 'deleted_by', 'deleted_date');
        return redirect()->route('admin.base_rates')->with('success', "Data updated successfully.");
    }

}
