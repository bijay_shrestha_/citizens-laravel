<?php

namespace App\Modules\Base_rate\Model;


use Illuminate\Database\Eloquent\Model;

class Base_rate extends Model
{
    public  $table = 'tbl_base_rates';

    protected $fillable = ['base_rate_id','base_rate','name','date','link','approvel_status','approved','added_by','added_date','modified_date','deleted_date','deleted_by','del_flag','status',];
}
