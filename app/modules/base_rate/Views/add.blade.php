@extends('admin.layout.main')
@section('content')
        <link rel="stylesheet" href="{{ asset('nepalidatepicker/css/nepali.datepicker.css') }}">

    <section class="content-header">
        <h1>
            Add Base Rates
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.base_rates') }}">Base Rates</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.base_rates.store') }}"  method="post">
                <div class="box-body">    

                    <div class="form-group">
                        <label for="date">Date</label><input type="text" name="date" id="date" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="name">Name</label><input type="text" name="name" id="name" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="base_rate">Base Rate</label><input type="text" name="base_rate" id="base_rate" class="form-control" >
                    </div>
                    
                    <div class="form-group">
                              <label>Link</label><br/><label id="upload_download_link" style="display:none"></label>
                              <!-- <input type="text" name="download_form_link" id="download_form_link"  value="" class="form-control"> -->
                              <input type="file" name="download_link" id="download_link" >
                              <div id="file-status"></div>
                              <input type="hidden" name="download_form_link" id="download_form_link"/>
                              <a href="javascript:void(0)" id="change-file" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>

                    </div>

                    <div class="form-group">
                         <label for="status">Status</label>
                         <div class="radio">
                         <label>
                             <input type="radio" value="1" name="status" id="status1" @if(old('status') == 1) checked @endif  />Yes
                         </label> 
                         <label>
                             <input type="radio" value="0" name="status" id="status0" @if(old('status') == 1) @else  checked @endif  />No
                         </label>
                         </div>
                     </div>

                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.base_rates') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection


@section('custom_scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $('#date').nepaliDatePicker();

    }); 
</script>
    <script type="text/javascript">
    var directory = 'brate';
</script>
<script src="{{asset('nepalidatepicker/js/nepali.datepicker.js')}}"></script>
@include('data_changing_script_create')
@include('file_changing_script')

@endsection
