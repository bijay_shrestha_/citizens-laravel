<?php



Route::group(array('prefix'=>'admin/','module'=>'account_interest_rate','middleware' => ['web','auth'], 'namespace' => 'App\modules\account_interest_rate\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('account_interest_rates/','AdminAccount_interest_rateController@index')->name('admin.account_interest_rates');
    Route::post('account_interest_rates/getaccount_interest_ratesJson','AdminAccount_interest_rateController@getaccount_interest_ratesJson')->name('admin.account_interest_rates.getdatajson');
    Route::get('account_interest_rates/create','AdminAccount_interest_rateController@create')->name('admin.account_interest_rates.create');
    Route::post('account_interest_rates/store','AdminAccount_interest_rateController@store')->name('admin.account_interest_rates.store');
    Route::get('account_interest_rates/show/{id}','AdminAccount_interest_rateController@show')->name('admin.account_interest_rates.show');
    Route::get('account_interest_rates/edit/{id}','AdminAccount_interest_rateController@edit')->name('admin.account_interest_rates.edit');
    Route::match(['put', 'patch'], 'account_interest_rates/update/{id}','AdminAccount_interest_rateController@update')->name('admin.account_interest_rates.update');
    Route::get('account_interest_rates/delete/{id}', 'AdminAccount_interest_rateController@destroy')->name('admin.account_interest_rates.edit');
});




Route::group(array('module'=>'account_interest_rate','namespace' => 'App\modules\account_interest_rate\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('account_interest_rates/','Account_interest_rateController@index')->name('account_interest_rates');
    
});