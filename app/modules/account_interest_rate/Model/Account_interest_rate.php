<?php

namespace App\Modules\Account_interest_rate\Model;


use Illuminate\Database\Eloquent\Model;

class Account_interest_rate extends Model
{
    public  $table = 'tbl_account_interest_rates';

    protected $fillable = ['rate_id','account_id','rate_name','per_annum_min','per_annum_max','status','added_by','deleted_by','deleted_date','del_flag','added_date','modified_date',];
}
