@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Account_interest_rates   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.account_interest_rates') }}">tbl_account_interest_rates</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.account_interest_rates.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                                    <label for="rate_id">Rate_id</label><input type="text" name="rate_id" id="rate_id" class="form-control" ></div><div class="form-group">
                                    <label for="account_id">Account_id</label><input type="text" name="account_id" id="account_id" class="form-control" ></div><div class="form-group">
                                    <label for="rate_name">Rate_name</label><input type="text" name="rate_name" id="rate_name" class="form-control" ></div><div class="form-group">
                                    <label for="per_annum_min">Per_annum_min</label><input type="text" name="per_annum_min" id="per_annum_min" class="form-control" ></div><div class="form-group">
                                    <label for="per_annum_max">Per_annum_max</label><input type="text" name="per_annum_max" id="per_annum_max" class="form-control" ></div><div class="form-group">
                                    <label for="status">Status</label><input type="text" name="status" id="status" class="form-control" ></div><div class="form-group">
                                    <label for="created_by">Created_by</label><input type="text" name="created_by" id="created_by" class="form-control" ></div><div class="form-group">
                                    <label for="deleted_by">Deleted_by</label><input type="text" name="deleted_by" id="deleted_by" class="form-control" ></div><div class="form-group">
                                    <label for="deleted_at">Deleted_at</label><input type="text" name="deleted_at" id="deleted_at" class="form-control" ></div><div class="form-group">
                                    <label for="del_flag">Del_flag</label><input type="text" name="del_flag" id="del_flag" class="form-control" ></div><div class="form-group">
                                    <label for="created_at">Created_at</label><input type="text" name="created_at" id="created_at" class="form-control" ></div><div class="form-group">
                                    <label for="updated_at">Updated_at</label><input type="text" name="updated_at" id="updated_at" class="form-control" ></div>
<input type="hidden" name="id" id="id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.account_interest_rates') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
