<?php

namespace App\modules\banner\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\banner\Model\Banner;
use Carbon\Carbon;

class AdminBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Banner';
        return view("banner::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getbannersJson(Request $request)
    {
        $banner = new Banner;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $banner->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->get();

        // Display limited list        
        $rows = $banner->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->get();

        //To count the total values present
        $total = $banner->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($banner_collection_id)
    {
        $page['title'] = 'Banner | Create';
        return view("banner::add",compact('page', 'banner_collection_id'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
//        dd($data, "up to here");
        $data_for_banner = $data;
        unset($data_for_banner["upload_image"]);
        unset($data_for_banner["upload_logo"]);
        unset($data_for_banner["upload_background"]);
        $data_for_banner['new_window'] = 0;
        $data_for_banner['sort_order'] = ($data_for_banner['sort_order'] == null) ? "0":$data_for_banner['sort_order'];
        $data_for_banner['added_date'] = new Carbon();
        $data_for_banner['position'] = 0;
        $data_for_banner['del_flag'] = 0;
        $data_for_banner['hide_description'] = 0;
        $success = Banner::Insert($data_for_banner);
        return redirect()->route('admin.banner_collections')->with('success', "Data added successfully");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banner::where('banner_id', $id)->firstOrFail();
        $banner = Banner::where('banner_id', $id)->first()->get()->toArray();
        $page['title'] = 'Banner | Update';
        $data = [];
        foreach ($banner as $index => $value) {
            $data[$index] = $value;
        }
        $banner = $data[0];
        return view("banner::edit",compact('page','banner'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $banner = Banner::where('banner_id', $id)->firstOrFail();
        $data_for_banner = $data;
        unset($data_for_banner['upload_image']);
        unset($data_for_banner['upload_logo']);
        unset($data_for_banner['upload_background']);
        
        //for image case
        if (array_key_exists('delete_old_image', $data)) {
            if ((file_exists(public_path().'/uploads/banner/'. $banner->image_name))  && $banner->image_name) {
                unlink(public_path().'/uploads/banner/'. $banner->image_name);
                if (file_exists(public_path().'/uploads/banner/thumb/'. $banner->image_name)) {
                    unlink(public_path().'/uploads/banner/thumb/'. $banner->image_name);
                } 
            }
            if (!$data['image_name']) {
                $data['image_name'] = '';
                $data_for_banner['image_name'] = '';
            }
            unset($data_for_banner['delete_old_image']);
        } else {
            if (!$data['image_name']) {
                $data['image_name'] = $banner->image_name;
                $data_for_banner['image_name'] = $banner->image_name;
            }
        }
       // $data_for_banner['image'] = $data_for_banner['image_name']; //no needed for banner
       // unset($data_for_banner['image_name']); // no needed for banner

        //for logo case
        if (array_key_exists('delete_old_logo', $data)) {

            if ((file_exists(public_path().'/uploads/banner/'. $banner->logo))  && $banner->logo) {
                unlink(public_path().'/uploads/banner/'. $banner->logo);
                if (file_exists(public_path().'/uploads/banner/thumb/'. $banner->logo)) {
                    unlink(public_path().'/uploads/banner/thumb/'. $banner->logo);
                } 
            }
            if (!$data['logo']) {
                $data['logo'] = '';
                $data_for_banner['logo'] = '';
            }
            unset($data_for_banner['delete_old_logo']);
        } else {
            if (!$data['logo']) {
                $data['logo'] = $banner->logo;
                $data_for_banner['logo'] = $banner->logo;
            }
        }

        //for background case
        if (array_key_exists('delete_old_background', $data)) {
            if ((file_exists(public_path().'/uploads/banner/'. $banner->background))  && $banner->background) {
                unlink(public_path().'/uploads/banner/'. $banner->background);
                if (file_exists(public_path().'/uploads/banner/thumb/'. $banner->background)) {
                    unlink(public_path().'/uploads/banner/thumb/'. $banner->background);
                } 
            }
            if (!$data['background']) {
                $data['background'] = '';
                $data_for_banner['background'] = '';
            }
            unset($data_for_banner['delete_old_background']);
        } else {
            if (!$data['background']) {
                $data['background'] = $banner->background;
                $data_for_banner['background'] = $banner->background;
            }
        }

        $data_for_banner['modified_date'] = new Carbon();
        $data_for_banner['sort_order'] = ($data_for_banner['sort_order'] == null) ? "0":$data_for_banner['sort_order'];
        $success = DB::table('tbl_banners')->where('banner_id', $id)->update($data_for_banner);

        return redirect()->route('admin.banner_collections')->with('success', "Data added successfully");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_banners', 'banner_id', $id, 'deleted_by', 'deleted_date');
        return redirect()->route('admin.banner_collections')->with('success', "Data deleted successfully");

        //
    }
}
