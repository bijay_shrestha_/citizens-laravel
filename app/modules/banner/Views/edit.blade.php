@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Banners   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.banners') }}">tbl_banners</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.banners.update', $banner['banner_id']) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                    <input type="hidden" value = "{{$banner['banner_collection_id']}}"  name="banner_collection_id" id="banner_collection_id" class="form-control" >
                    

                    <div class="form-group">
                        <label for="image_name">Image </label><br>
                        <label id="upload_image_name" style="display:none"></label>
                         <div id="delete_old_image_requested"></div>
                        <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                        <div id="image-status"></div>
                        <input type="file" id="upload_image" name="upload_image" @if (file_exists(public_path().'/uploads/banner/thumb/'. $banner['image_name']) && $banner['image_name']) style="display:none" @else style="display:block" @endif />
                        <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                        <div id="imagedata">
                        @if (file_exists(public_path().'/uploads/banner/'. $banner['image_name']) && $banner['image_name'])                                   
                            <img src="{{url('uploads/banner/thumb/'.$banner['image_name'])}}" height="100" width="100">
                            <a href="javascript:void(0)" id="change-image_option" title="Change Image">&nbsp; &nbsp; <img src="{{('/images/logo/cancel.png')}}" border="0"/> Change the old image</a>
                        @endif                                
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="logo">Logo </label><br>
                        <label id="upload_logo_name" style="display:none"></label>
                         <div id="delete_old_logo_requested"></div>
                        <input type="text" name="logo" id="logo" class="form-control" style="display:none" />
                        <div id="logo-status"></div>
                        <input type="file" id="upload_logo" name="upload_logo" @if (file_exists(public_path().'/uploads/banner/thumb/'. $banner['logo']) && $banner['logo']) style="display:none" @else style="display:block" @endif />
                        <a href="javascript:void(0)" id="change-logo" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                        <div id="logodata">
                        @if (file_exists(public_path().'/uploads/banner/'. $banner['logo']) && $banner['logo'])                                   
                            <img src="{{url('uploads/banner/thumb/'.$banner['logo'])}}" height="100" width="100">
                            <a href="javascript:void(0)" id="change-logo_option" title="Change logo">&nbsp; &nbsp; <img src="{{('/images/logo/cancel.png')}}" border="0"/> Change the old logo</a>
                        @endif                                
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="logo">Background </label><br>
                        <label id="upload_background_name" style="display:none"></label>
                         <div id="delete_old_background_requested"></div>
                        <input type="text" name="background" id="background" class="form-control" style="display:none" />
                        <div id="background-status"></div>
                        <input type="file" id="upload_background" name="upload_background" @if (file_exists(public_path().'/uploads/banner/thumb/'. $banner['background']) && $banner['background']) style="display:none" @else style="display:block" @endif />
                        <a href="javascript:void(0)" id="change-background" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                        <div id="backgrounddata">
                        @if (file_exists(public_path().'/uploads/banner/'. $banner['background']) && $banner['background'])                                   
                            <img src="{{url('uploads/banner/thumb/'.$banner['background'])}}" height="100" width="100">
                            <a href="javascript:void(0)" id="change-background_option" title="Change background">&nbsp; &nbsp; <img src="{{('/images/logo/cancel.png')}}" border="0"/> Change the old background</a>
                        @endif                                
                        </div>
                    </div>

                    <div class="form-group">
                                  <label>IS_Free Pik</label><div class="radio"><label><input type="radio" value="1" name="type" id="type1" @if($banner['type'] == 1) checked="checked" @endif />Yes</label> 
                              <label><input type="radio" value="0" name="type" id="type2" @if($banner['type'] == 1) @else  checked="checked" @endif  />No</label></div>
                              </div>
                              <div class="form-group">
                                  <label>Link Of Image</label>
                                  <input type="text" id="freepik_link" name="freepik_link" class="form-control" value="{{$banner['freepik_link']}}">
                    </div>

                     <div class="form-group">
                        <label for="design">Design Align (*)</label>
                            <select class="form-control" id="design" name="design" required>
                            <option value="">Select Design </option>
                            <option value="Left" @if($banner['design']== "Left") selected @endif>Left</option>
                            <option value="Middle" @if($banner['design']== "Middle") selected @endif>Middle</option>
                            <option value="Right" @if($banner['design']== "Right") selected @endif>Right</option>
                            <option value="Mid-Left" @if($banner['design']== "Mid-Left") selected @endif>Mid-Left</option>
                            
                        </select>
                                            
                                                
                      </div>

                    <div class="form-group">
                        <label for="top_box_description">Top Box Description</label><textarea name="top_box_description" id="top_box_description" class="form-control my-editor" >{!! $banner['top_box_description'] !!}</textarea>
                    </div>

                        <div class="form-group">
                            <label for="color">Text color (*)</label>
                                <select class="form-control" id="color" name="color">
                                <option value="">Select Text Color</option>
                                <option value="White"  @if($banner['color']== "White") selected @endif>White</option>
                                <option value="Black" @if($banner['color']== "Black") selected @endif>Black</option>
                                <option value="Red" @if($banner['color']== "Red") selected @endif>Red</option>
                                <option value="Blue" @if($banner['color']== "Blue") selected @endif>Blue</option>
                                <option value="Green" @if($banner['color']== "Green") selected @endif>Green</option>
                            </select>
                        </div>
                    
                    <div class="form-group">
                        <label for="bottom_box_description">Bottom Box Description</label><textarea name="bottom_box_description" id="bottom_box_description" class="form-control my-editor" >{!! $banner['bottom_box_description'] !!}</textarea>
                    </div>
                    
                    <div class="form-group">
                        <label for="bottom-color">Text color (*)</label>
                            <select class="form-control" id="bottom-color" name="bottom-color" required>
                            <option value="">Select Text Color</option>
                                <option value="White"  @if($banner['bottom-color']== "White") selected @endif>White</option>
                                <option value="Black" @if($banner['bottom-color']== "Black") selected @endif>Black</option>
                                <option value="Red" @if($banner['bottom-color']== "Red") selected @endif>Red</option>
                                <option value="Blue" @if($banner['bottom-color']== "Blue") selected @endif>Blue</option>
                                <option value="Green" @if($banner['bottom-color']== "Green") selected @endif>Green</option>
                        </select>
                    </div>

                    <div class="form-group">
                            <label for="link">Link</label><input type="text" value = "{{$banner['link']}}"  name="link" id="link" class="form-control" >
                    </div>
                    
                    <div class="form-group">
                        <label for="link_title">Link Title</label><input type="text" value = "{{$banner['link_title']}}"  name="link_title" id="link_title" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="start_date">Start Date</label><input type="date" value = "{{$banner['start_date']}}"  name="start_date" id="start_date" class="form-control" >
                    </div>
                    
                    <div class="form-group">
                        <label for="end_date">End Date</label><input type="date" value = "{{$banner['end_date']}}"  name="end_date" id="end_date" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="sort_order">Sort Order</label><input type="text" value = "{{$banner['sort_order']}}"  name="sort_order" id="sort_order" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="status" id="status1" @if($banner['status'] == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="status" id="status0" @if($banner['status'] == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>                
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.banners') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
        <script type="text/javascript">
        var directory = 'banner';
    </script>
    @include('data_changing_script_edit')
    @include('file_changing_script')
    @include('multiple_file_changing_script')  {{-- For more than one image or more than one file fields   --}}

@endsection
