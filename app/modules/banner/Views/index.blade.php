@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Banners		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Banners</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.banners.create') }}" class="btn bg-green waves-effect"  title="create">Create</a>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="banner-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								<th >Banner_id</th>
<th >Banner_collection_id</th>
<th >Image_name</th>
<th >Business_type</th>
<th >Top_box_description</th>
<th >Bottom_box_description</th>
<th >Hide_description</th>
<th >Logo</th>
<th >Background</th>
<th >Link</th>
<th >Link_title</th>
<th >New_window</th>
<th >Added_date</th>
<th >Modified_date</th>
<th >Start_date</th>
<th >End_date</th>
<th >Sort_order</th>
<th >Position</th>
<th >Deleted_by</th>
<th >Deleted_date</th>
<th >Del_flag</th>
<th >Status</th>
<th >Type</th>
<th >Color</th>
<th >Bottom-color</th>
<th >Design</th>
<th >Freepik_link</th>

								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#banner-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.banners.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
					{ data: "banner_id",name: "banner_id"},
            { data: "banner_collection_id",name: "banner_collection_id"},
            { data: "image_name",name: "image_name"},
            { data: "business_type",name: "business_type"},
            { data: "top_box_description",name: "top_box_description"},
            { data: "bottom_box_description",name: "bottom_box_description"},
            { data: "hide_description",name: "hide_description"},
            { data: "logo",name: "logo"},
            { data: "background",name: "background"},
            { data: "link",name: "link"},
            { data: "link_title",name: "link_title"},
            { data: "new_window",name: "new_window"},
            { data: "added_date",name: "added_date"},
            { data: "modified_date",name: "modified_date"},
            { data: "start_date",name: "start_date"},
            { data: "end_date",name: "end_date"},
            { data: "sort_order",name: "sort_order"},
            { data: "position",name: "position"},
            { data: "deleted_by",name: "deleted_by"},
            { data: "deleted_date",name: "deleted_date"},
            { data: "del_flag",name: "del_flag"},
            { data: "status",name: "status"},
            { data: "type",name: "type"},
            { data: "color",name: "color"},
            { data: "bottom-color",name: "bottom-color"},
            { data: "design",name: "design"},
            { data: "freepik_link",name: "freepik_link"},
            
					{ data: function(data,b,c,table) { 
					var buttons = '';

					buttons += "<a class='btn bg-red waves-effect' href='"+site_url+"/edit/"+data.id+"' type='button' >Edit</a>&nbsp"; 

					buttons += "<a href='"+site_url+"/delete/"+data.id+"' class='btn bg-red waves-effect' >Delete</a>";

					return buttons;
					}, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
