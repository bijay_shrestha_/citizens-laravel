@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Banners   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.banners') }}">tbl_banners</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.banners.store') }}"  method="post">
                <div class="box-body">                
                        <input type="hidden" name="banner_collection_id" id="banner_collection_id" value="{{$banner_collection_id}}" class="form-control">
                    
                    <div class="form-group">
                        <label for="image_name">Image </label><br>
                        <label id="upload_image_name" style="display:none"></label>
                        <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                        <div id="image-status"></div>
                        <input type="file" id="upload_image" name="upload_image" style="display:block"/>
                        <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>

                    </div>

                    <div class="form-group">
                        <label for="logo">Logo </label><br>
                        <label id="upload_logo_name" style="display:none"></label>
                        <input type="text" name="logo" id="logo" class="form-control" style="display:none" />
                        <div id="logo-status"></div>
                        <input type="file" id="upload_logo" name="upload_logo" style="display:block"/>
                        <a href="javascript:void(0)" id="change-logo" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>

                    </div>

                    <div class="form-group">
                        <label for="background">Background</label><br>
                        <label id="upload_background_name" style="display:none"></label>
                        <input type="text" name="background" id="background" class="form-control" style="display:none" />
                        <div id="background-status"></div>
                        <input type="file" id="upload_background" name="upload_background" style="display:block"/>
                        <a href="javascript:void(0)" id="change-background" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>

                    </div>

                    <div class="form-group">
                                <label>IS_Free Pik</label><div class="radio"><label><input type="radio" value="1" name="type" id="type1" />Yes</label> 
                            <label><input type="radio" value="0" name="type" id="type2" checked />No</label></div>
                    </div>
                  
                    <div class="form-group">
                                <label>Link Of Image</label>
                                <input type="text" id="freepik_link" name="freepik_link" class="form-control">
                    </div>

                     <div class="form-group">
                        <label for="design">Design Align (*)</label>
                            <select class="form-control" id="design" name="design" required>
                            <option value="">Select Design </option>
                            <option value="Left">Left</option>
                            <option value="Middle">Middle</option>
                            <option value="Right">Right</option>
                            <option value="Mid-Left">Mid-Left</option>
                            
                        </select>
                                            
                                                
                      </div>

                        <div class="form-group">
                            <label for="top_box_description">Top Box Description</label><textarea name="top_box_description" id="top_box_description" class="form-control my-editor" ></textarea>
                        </div>
                        <div class="form-group">
                            <label for="color">Text color (*)</label>
                                <select class="form-control" id="color" name="color">
                                <option value="">Select Text Color</option>
                                <option value="White">White</option>
                                <option value="Black">Black</option>
                                <option value="Red">Red</option>
                                <option value="Blue">Blue</option>
                                <option value="Green">Green</option>
                            </select>
                        </div>
                     
                    <div class="form-group">
                        <label for="bottom_box_description">Bottom Box Description</label><textarea name="bottom_box_description" id="bottom_box_description" class="form-control my-editor" ></textarea>
                    </div>

                    <div class="form-group">
                        <label for="bottom-color">Text color (*)</label>
                            <select class="form-control" id="bottom-color" name="bottom-color" required>
                            <option value="">Select Text Color</option>
                            <option value="White">White</option>
                            <option value="Black">Black</option>
                            <option value="Red">Red</option>
                            <option value="Blue">Blue</option>
                            <option value="Green">Green</option>
                        </select>
                    </div>

                        {{--
                    <div class="form-group">
                        <label for="business_type">Business_type</label><input type="text" name="business_type" id="business_type" class="form-control" >
                    </div> --}}
                
                    <div class="form-group">
                        <label for="link">Link</label><input type="text" name="link" id="link" class="form-control" >
                    </div>
                
                    <div class="form-group">
                        <label for="link_title">Link Title</label><input type="text" name="link_title" id="link_title" class="form-control" >
                    </div>
                
                    <div class="form-group">
                        <label for="start_date">Start Date</label><input type="date" name="start_date" id="start_date" class="form-control" >
                    </div>
                
                    <div class="form-group">
                        <label for="end_date">End Date</label><input type="date" name="end_date" id="end_date" class="form-control">
                    </div>
                
                    <div class="form-group">
                        <label for="sort_order">Sort Order</label><input type="text" name="sort_order" id="sort_order" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="status" id="status1" @if(old('status') == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="status" id="status0" @if(old('status') == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>
                    </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary"  id="btn-submit" >Save</button>
                    <a href="{{ route('admin.banners') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
    <script type="text/javascript">
          var directory = 'banner';
        $('#btn-submit').on('click',function(){
          var filename = $('#image_name').val();

          
          if (filename == "") {
           alert('Please Upload Image');
           return false;
          }
         });

    </script>
    @include('data_changing_script_create')
    @include('file_changing_script') 
    @include('multiple_file_changing_script')  {{-- For more than one image or more than one file fields   --}}

@endsection
