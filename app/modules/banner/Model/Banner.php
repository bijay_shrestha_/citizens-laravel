<?php

namespace App\Modules\Banner\Model;


use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    public  $table = 'tbl_banners';

    protected $fillable = ['banner_id','banner_collection_id','image_name','business_type','top_box_description','bottom_box_description','hide_description','logo','background','link','link_title','new_window','added_date','modified_date','start_date','end_date','sort_order','position','deleted_by','deleted_date','del_flag','status','type','color','bottom-color','design','freepik_link',];
}
