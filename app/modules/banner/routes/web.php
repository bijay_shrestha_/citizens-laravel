<?php



Route::group(array('prefix'=>'admin/','module'=>'banner','middleware' => ['web','auth'], 'namespace' => 'App\modules\banner\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('banners/','AdminBannerController@index')->name('admin.banners');
    Route::post('banners/getbannersJson','AdminBannerController@getbannersJson')->name('admin.banners.getdatajson');
    Route::get('banners/create/{banner_collection_id}','AdminBannerController@create')->name('admin.banners.create');
    Route::post('banners/store','AdminBannerController@store')->name('admin.banners.store');
    Route::get('banners/show/{id}','AdminBannerController@show')->name('admin.banners.show');
    Route::get('banners/edit/{id}','AdminBannerController@edit')->name('admin.banners.edit');
    Route::match(['put', 'patch'], 'banners/update/{id}','AdminBannerController@update')->name('admin.banners.update');
    Route::get('banners/delete/{id}', 'AdminBannerController@destroy')->name('admin.banners.delete');
});




Route::group(array('module'=>'banner','namespace' => 'App\modules\banner\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('banners/','BannerController@index')->name('banners');
    
});