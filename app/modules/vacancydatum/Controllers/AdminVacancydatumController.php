<?php

namespace App\modules\vacancydatum\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\vacancydatum\Model\Vacancydatum;
use App\modules\branch\Model\Branch;
use Carbon\Carbon;

class AdminVacancydatumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Vacancydatum';
        return view("vacancydatum::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getvacancydataJson(Request $request)
    {
        $vacancydatum = new Vacancydatum;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $vacancydatum->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('id', "DSC")->get();

        // Display limited list        
        $rows = $vacancydatum->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->orderBy('id', "DSC")->get();

        //To count the total values present
        $total = $vacancydatum->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Vacancydatum | Create';
        $branches = Branch::where('for_section', "Bank Branches")->get()->toArray();
        return view("vacancydatum::add",compact('page', 'branches'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $dataforvacancy = $data;
        /*if (array_key_exists('branch_id', $dataforvacancy)) 
        */

        //checking if start and end date have null value or not; only to avoid converting datatime format to null value in order not to get datetime like 1970s
        if ($dataforvacancy['vacancy_start_date']) {
            $dataforvacancy['vacancy_start_date'] = date('Y-m-d H:i:s',strtotime($data['vacancy_start_date']));
        }
        if ($dataforvacancy['vacancy_end_date']) {
            $dataforvacancy['vacancy_end_date'] =  date('Y-m-d H:i:s',strtotime($data['vacancy_end_date']));
        }
        unset($dataforvacancy['branch_id']);      
        $dataforvacancy['created_by'] = Auth::user()->id;
        $dataforvacancy['created_date'] = new Carbon();
        $dataforvacancy['del_flag'] = 0;
        $dataforvacancy = check_case($dataforvacancy, 'create');        
        $success = Vacancydatum::Insert($dataforvacancy);
        $vacancydatum = Vacancydatum::all();
        $vacancydata_id = $vacancydatum[count($vacancydatum)-1]['id'];

        //checking if branches are selected, even if it is not or at least one
        if (array_key_exists('branch_id', $data)) {
            if (count($data['branch_id']) > 0) {
                foreach ($data['branch_id'] as $branch) {                               
                    DB::table('tbl_vacancy_branches')->insert(['vacancy_id' => $vacancydata_id, 'branch_id' => $branch]); 
                }
            }
        }
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "VACANCYDATA", 'file_id' => $vacancydata_id]); //create activity log     
        return redirect()->route('admin.vacancydata')->with('success', "Data inserted successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vacancydatum = Vacancydatum::findOrFail($id);
        $page['title'] = 'Vacancydatum | Update';
        $branches = Branch::where('for_section', "Bank Branches")->get()->toArray();
        $chosen_branches = DB::table('tbl_vacancy_branches')->where('vacancy_id', $id)->get();
        return view("vacancydatum::edit",compact('page','vacancydatum', 'branches', 'chosen_branches'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $vacancydata = Vacancydatum::findOrFail($id);
        $dataforvacancy = $data;
        /*if (array_key_exists('branch_id', $dataforvacancy)) 
        */

        //checking if start and end date have null value or not; only to avoid converting datatime format to null value in order not to get datetime like 1970s
        if ($dataforvacancy['vacancy_start_date']) {
            $dataforvacancy['vacancy_start_date'] = date('Y-m-d H:i:s',strtotime($data['vacancy_start_date']));
        }
        if ($dataforvacancy['vacancy_end_date']) {
            $dataforvacancy['vacancy_end_date'] =  date('Y-m-d H:i:s',strtotime($data['vacancy_end_date']));
        }
        unset($dataforvacancy['branch_id']);      
        $dataforvacancy['modified_by'] = Auth::user()->id;
        $dataforvacancy['modified_date'] = new Carbon();
        $dataforvacancy = check_case($dataforvacancy, 'edit');        
        DB::table('tbl_vacancydata')->where('id', $dataforvacancy['id'])->update($dataforvacancy);

        $vacancydata_id = $id;

        DB::table('tbl_vacancy_branches')->where('vacancy_id', $id)->delete();
        //checking if branches are selected, even if it is not or at least one
        if (array_key_exists('branch_id', $data)) {
            if (count($data['branch_id']) > 0) {
                foreach ($data['branch_id'] as $branch) {                               
                    DB::table('tbl_vacancy_branches')->insert(['vacancy_id' => $vacancydata_id, 'branch_id' => $branch]); 
                }
            }
        }
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "VACANCYDATA", 'file_id' => $vacancydata_id]); //create activity log     

//        $success = Vacancydatum::where('id', $id)->update($data);
        return redirect()->route('admin.vacancydata')->with('success', "Data updated successfully.");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_vacancydata', 'id', $id);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "VACANCYDATA", 'file_id' => $id]); //create activity log     

        return redirect()->route('admin.vacancydata')->with('success', "Data deleted successfully.");

        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_vacancydata', 'id');
        return redirect()->route('admin.vacancydata')->with('success', "Data updated successfully.");
    }

}
