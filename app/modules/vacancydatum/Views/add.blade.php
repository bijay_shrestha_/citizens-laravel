@extends('admin.layout.main')
@section('content')
        <link rel="stylesheet" href="{{ asset('datetimepicker/css/jquery.datetimepicker.min.css') }}">

    <section class="content-header">
        <h1>
            Add Vacancydata   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.vacancydata') }}">Vacancydata</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
              <button class="btn btn-primary" onclick="toogleoption('post')">Vacancy</button>
              <button class="btn btn-primary" onclick="toogleoption('form_option')">Form Option</button>
              <button class="btn btn-primary" onclick="toogleoption('scope')">Scope Option</button>
            
            <form role="form" action="{{ route('admin.vacancydata.store') }}"  method="post">
                <div class="box-body">                
                    <div id="vacancy_data">
                        <div class="form-group">
                            <label for="preferred_post">Preferred Post</label><input type="text" name="preferred_post" id="preferred_post" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="specialized_area">Specialized Area</label><input type="text" name="specialized_area" id="specialized_area" class="form-control" >
                        </div>

                        <div class="form-group">
                            <label for="vacancy_code">Vacancy Code</label><input type="text" name="vacancy_code" id="vacancy_code" class="form-control" >
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label><textarea  name="description" id="description" class="form-control  my-editor" ></textarea>
                        </div>
                        <div class="form-group">
                            <label for="vacancy_start_date">Vacancy Start Date</label><input name="vacancy_start_date" id="vacancy_start_date" class="form-control" >
                        </div>

                        <div class="form-group">
                            <label for="vacancy_end_date">Vacancy End Date</label><input name="vacancy_end_date" id="vacancy_end_date" class="form-control" >
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div id="form_data" style="display: none;">
                        
                        <div class="form-group">
                            <label for="master">Master</label>
                            <div class="radio">
                                <label><input type="radio" value="1" name="master" />Required</label> 
                                <label><input type="radio" value="0" name="master" checked />Not Required</label>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="bachelor">Bachelor</label>
                            <div class="radio">
                                <label><input type="radio" value="1" name="bachelor"  />Required</label> 
                                <label><input type="radio" value="0" name="bachelor" checked />Not Required</label>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="experience">Experience</label>
                            <div class="radio"><label><input type="radio" value="1" name="experience" />Required</label> 
                            <label><input type="radio" value="0" name="experience" checked />Not Required</label></div>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div id="scope_data" style="display: none;">
                        <h4>Add As Many Branches</h4>
                        <div id="input">
                           <select name="branch_id[]" size=10 class="form-control" multiple="multiple">
                               @foreach ($branches as $branch)
                                    <option value="{{$branch['id']}}">{{$branch['name']}}</option>
                               @endforeach
                           </select>
                    
                        </div>                        
                    </div>                        
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.vacancydata') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection

@section('custom_scripts')
<script type="text/javascript">
    // $(document).ready(function(){
    //     $('#remove-zone-option').attr('disabled',true);
    //     $('.district-select').multiSelect();
    // });
    // var globel = 1;
    $(document).ready(function(){
        $('#vacancy_start_date').datetimepicker();
        $('#vacancy_end_date').datetimepicker();
    })
    /*$(document).ready(function () {
        
    //DatePicker Example
        $('#datetimepicker').datetimepicker();
        $('#datetimepicker1').datetimepicker();
       }); */
    function toogleoption(option)
    {
        if(option == 'post')
        {
            $('#vacancy_data').show();
            $('#scope_data').hide();                            
            $('#form_data').hide();

        }
        else if(option == 'scope')
        {
            $('#vacancy_data').hide();
            $('#form_data').hide();
            $('#scope_data').show();
        }
        else
        {
            $('#scope_data').hide();
            $('#vacancy_data').hide();
            $('#form_data').show();

        }
    }
</script>
<script src="{{asset('datetimepicker/js/jquery.datetimepicker.js')}}"></script>

@endsection