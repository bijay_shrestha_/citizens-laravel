<?php



Route::group(array('prefix'=>'admin/','module'=>'vacancydatum','middleware' => ['web','auth'], 'namespace' => 'App\modules\vacancydatum\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('vacancydata/','AdminVacancydatumController@index')->name('admin.vacancydata');
    Route::post('vacancydata/getvacancydataJson','AdminVacancydatumController@getvacancydataJson')->name('admin.vacancydata.getdatajson');
    Route::get('vacancydata/create','AdminVacancydatumController@create')->name('admin.vacancydata.create');
    Route::post('vacancydata/store','AdminVacancydatumController@store')->name('admin.vacancydata.store');
    Route::get('vacancydata/show/{id}','AdminVacancydatumController@show')->name('admin.vacancydata.show');
    Route::get('vacancydata/edit/{id}','AdminVacancydatumController@edit')->name('admin.vacancydata.edit');
    Route::match(['put', 'patch'], 'vacancydata/update/{id}','AdminVacancydatumController@update')->name('admin.vacancydata.update');
    Route::get('vacancydata/delete/{id}', 'AdminVacancydatumController@destroy')->name('admin.vacancydata.edit');
    Route::get('vacancydata/change/{text}/{status}/{id}', 'AdminVacancydatumController@change');
});




Route::group(array('module'=>'vacancydatum','namespace' => 'App\modules\vacancydatum\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('vacancydata/','VacancydatumController@index')->name('vacancydata');
    
});