<?php

namespace App\Modules\Vacancydatum\Model;


use Illuminate\Database\Eloquent\Model;

class Vacancydatum extends Model
{
    public  $table = 'tbl_vacancydata';

    protected $fillable = ['id','preferred_post','specialized_area','vacancy_code','description','approvel_status','approved','master','bachelor','experience','del_flag','created_by','created_date','modified_by','modified_date','vacancy_start_date','vacancy_end_date',];
}
