@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Faq Collections   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.faq_collections') }}">Faq Collections</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.faq_collections.update', $faq_collection->faq_collection_id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                                    <label for="faq_collection_name">Faq Collection Name</label><input type="text" value = "{{$faq_collection->faq_collection_name}}"  name="faq_collection_name" id="faq_collection_name" class="form-control" required></div><div class="form-group">
                                    <label for="sequence">Sequence</label><input type="text" value = "{{$faq_collection->sequence}}"  name="sequence" id="sequence" class="form-control" ></div><div class="form-group">
                                    <label for="status">Status</label>
                                    <div class="radio">
                                        <label>
                                        <input type="radio" value="1" name="status" id="status1" @if($faq_collection->status == 1) checked="checked" @endif/>Yes</label> 
                                        <label><input type="radio" value="0" name="status" id="status0" @if($faq_collection->status == 0) checked="checked" @endif/>No</label>
                                    </div>
                                    </div><div class="form-group">
                                    </div>
<input type="hidden" name="faq_collection_id" id="faq_collection_id" value = "{{$faq_collection->faq_collection_id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.faq_collections') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
