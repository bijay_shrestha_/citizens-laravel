@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Faq Collecion   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.faq_collections') }}">Faq Collection</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.faq_collections.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                                    <label for="faq_collection_name">Faq Collection Name (*)</label><input type="text" name="faq_collection_name" id="faq_collection_name" class="form-control" required></div><div class="form-group">
                                    <label for="sequence">Sequence</label><input type="text" name="sequence" id="sequence" class="form-control" ></div><div class="form-group">
                                    <label for="status">Status</label><div class="radio">
                            <label>
                            <input type="radio" value="1" name="status" id="status1" checked="checked" />Yes</label> 
                            <label><input type="radio" value="0" name="status" id="status0" />No</label>
                        </div></div>
<input type="hidden" name="faq_collection_id" id="faq_collection_id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.faq_collections') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
