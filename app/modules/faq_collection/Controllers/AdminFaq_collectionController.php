<?php

namespace App\modules\faq_collection\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\faq_collection\Model\Faq_collection;
use Carbon\Carbon;

class AdminFaq_collectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Faq Collection';
        return view("faq_collection::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getfaq_collectionsJson(Request $request)
    {
        $faq_collection = new Faq_collection;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $faq_collection->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('faq_collection_id', "DSC")->get();

        // Display limited list        
        $rows = $faq_collection->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->orderBy('faq_collection_id', "DSC")->get();

        //To count the total values present
        $total = $faq_collection->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Faq Category | Create';
        return view("faq_collection::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        if(!$data['sequence']) {
            $data['sequence'] = 0;
        }
        $data['del_flag'] = 0;
        $data['added_by'] = Auth::user()->id;
        $data['added_date'] = new Carbon();
        $data = check_case($data, 'create');
        $success = Faq_collection::Insert($data);
        $faq_collections = Faq_collection::all();
        $faq_collection_id = $faq_collections[count($faq_collections)-1]['faq_collection_id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "FAQ_COLLECTIONS", 'file_id' => $faq_collection_id]); //create acrivity log     
        return redirect()->route('admin.faq_collections')->with('success', "Date added successfully");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faq_collection = Faq_collection::where('faq_collection_id', $id)->firstOrFail();
        $page['title'] = 'Faq_collection | Update';
        return view("faq_collection::edit",compact('page','faq_collection'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        if(!$data['sequence']) {
            $data['sequence'] = 0;
        }
        $data['modified_date'] = new Carbon();
        $data = check_case($data, 'edit');
         DB::table('tbl_faq_collections')->where('faq_collection_id', $id)->update($data);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "FAQ_COLLECTIONS", 'file_id' => $id]); //create acrivity log     

        return redirect()->route('admin.faq_collections')->with('success', "Date updated successfully");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_faq_collections', 'faq_collection_id', $id, 'deleted_by', 'deleted_date');
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "FAQ_COLLECTIONS", 'file_id' => $id]); //create acrivity log     
        return redirect()->route('admin.faq_collections')->with('success', "Date deleted successfully");

        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_faq_collections', 'faq_collection_id', 'deleted_by', 'deleted_date');
        return redirect()->route('admin.faq_collections')->with('success', "Date status changed");
    }

}
