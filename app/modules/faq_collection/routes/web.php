<?php



Route::group(array('prefix'=>'admin/','module'=>'faq_collection','middleware' => ['web','auth'], 'namespace' => 'App\modules\faq_collection\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('faq_collections/','AdminFaq_collectionController@index')->name('admin.faq_collections');
    Route::post('faq_collections/getfaq_collectionsJson','AdminFaq_collectionController@getfaq_collectionsJson')->name('admin.faq_collections.getdatajson');
    Route::get('faq_collections/create','AdminFaq_collectionController@create')->name('admin.faq_collections.create');
    Route::post('faq_collections/store','AdminFaq_collectionController@store')->name('admin.faq_collections.store');
    Route::get('faq_collections/show/{id}','AdminFaq_collectionController@show')->name('admin.faq_collections.show');
    Route::get('faq_collections/edit/{id}','AdminFaq_collectionController@edit')->name('admin.faq_collections.edit');
    Route::match(['put', 'patch'], 'faq_collections/update/{id}','AdminFaq_collectionController@update')->name('admin.faq_collections.update');
    Route::get('faq_collections/delete/{id}', 'AdminFaq_collectionController@destroy')->name('admin.faq_collections.edit');
    Route::get('faq_collections/change/{text}/{status}/{id}', 'AdminFaq_collectionController@change');

});




Route::group(array('module'=>'faq_collection','namespace' => 'App\modules\faq_collection\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('faq_collections/','Faq_collectionController@index')->name('faq_collections');
    
});