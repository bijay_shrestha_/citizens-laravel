<?php

namespace App\Modules\Faq_collection\Model;


use Illuminate\Database\Eloquent\Model;

class Faq_collection extends Model
{
    public  $table = 'tbl_faq_collections';

    protected $fillable = ['faq_collection_id','faq_collection_name','sequence','approvel_status','approved','added_by','deleted_by','deleted_date','del_flag','status','added_date','modified_date',];
}
