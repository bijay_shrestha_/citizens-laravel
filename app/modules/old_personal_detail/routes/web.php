<?php



Route::group(array('prefix'=>'admin/','module'=>'personal_detail','middleware' => ['web','auth'], 'namespace' => 'App\modules\personal_detail\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('personal_details/','AdminPersonal_detailController@index')->name('admin.personal_details');
    Route::post('personal_details/getpersonal_detailsJson','AdminPersonal_detailController@getpersonal_detailsJson')->name('admin.personal_details.getdatajson');
    Route::get('personal_details/create','AdminPersonal_detailController@create')->name('admin.personal_details.create');
    Route::post('personal_details/store','AdminPersonal_detailController@store')->name('admin.personal_details.store');
    Route::get('personal_details/show/{id}','AdminPersonal_detailController@show')->name('admin.personal_details.show');
    Route::get('personal_details/edit/{id}','AdminPersonal_detailController@edit')->name('admin.personal_details.edit');
    Route::match(['put', 'patch'], 'personal_details/update/{id}','AdminPersonal_detailController@update')->name('admin.personal_details.update');
    Route::get('personal_details/delete/{id}', 'AdminPersonal_detailController@destroy')->name('admin.personal_details.edit');
});




Route::group(array('module'=>'personal_detail','namespace' => 'App\modules\personal_detail\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('personal_details/','Personal_detailController@index')->name('personal_details');
    
});