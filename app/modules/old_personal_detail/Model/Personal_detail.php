<?php

namespace App\modules\personal_detail\Model;


use Illuminate\Database\Eloquent\Model;

class Personal_detail extends Model
{
    public  $table = 'tbl_personal_details';

    protected $fillable = ['p_id','first_name','midle_name','last_name','permanent_address','temporary_address','dob','p_zone','p_district','t_zone','t_district','nationality','cv','citizenship_no','language','phone_no','email','gender','marital_status','immediate_contact','mobile_no','contact_no','status','del_flag','father_name','grand_father_name','posted_date',];
}
