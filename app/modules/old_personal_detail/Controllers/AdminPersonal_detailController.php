<?php

namespace App\modules\personal_detail\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\personal_detail\Model\Personal_detail;
use App\modules\education\Model\Education;
use App\modules\extra_course\Model\Extra_course;
use App\modules\preference_available\Model\Preference_available;
use App\modules\orgreference\Model\Orgreference;
use App\modules\work_experience\Model\Work_experience;

use Carbon\Carbon;

class AdminPersonal_detailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Personal_detail';
        return view("personal_detail::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getpersonal_detailsJson(Request $request)
    {
        $personal_detail = DB::table('tbl_personal_details');
        
        $where = $this->_get_search_param($request);
        //$where[2][0]="email";
        //return $where;
        
        // return $request;
        // For pagination
        $filterTotal = $personal_detail->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )
        ->leftjoin('tbl_education', 'tbl_personal_details.p_id', '=', 'tbl_education.p_id')
        ->leftjoin('tbl_work_experience', 'tbl_personal_details.p_id', '=', 'tbl_work_experience.p_id')
        ->leftjoin('tbl_extra_course', 'tbl_personal_details.p_id', '=', 'tbl_extra_course.p_id')
        ->leftjoin('tbl_preference_available', 'tbl_personal_details.p_id', '=', 'tbl_preference_available.p_id')
        ->leftjoin('tbl_reference_organization', 'tbl_personal_details.p_id', '=', 'tbl_reference_organization.p_id')
        ->leftjoin('tbl_orgreference', 'tbl_personal_details.p_id', '=', 'tbl_orgreference.p_id')
        ->leftjoin('tbl_working_experience', 'tbl_personal_details.p_id', '=', 'tbl_working_experience.p_id')
        ->select('tbl_personal_details.*','tbl_personal_details.email as p_email','tbl_preference_available.specialised_area as specialised_area','tbl_preference_available.preferred_post as preferred_post')
        ->where('del_flag',0)
        ->orderBy('tbl_personal_details.p_id', 'DSC')
        ->get();
        // Display limited list        
        $rows = $personal_detail->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('tbl_personal_details.del_flag',0)->orderBy('tbl_personal_details.p_id', 'DSC')->get();

        //To count the total values present
        $total = $personal_detail->where('tbl_personal_details.del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);
    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Personal_detail | Create';
        return view("personal_detail::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');

        // $datafor_personalDetail = $data;
        // $datafor_personalDetail['first_name'];
        // $datafor_personalDetail['middle_name'];
        // $datafor_personalDetail['last_name'];
        // $datafor_personalDetail['permanent_address'];
        // $datafor_personalDetail['temporary_address'];
        // $datafor_personalDetail['dob'];
        // $datafor_personalDetail['p_zone'];
        // $datafor_personalDetail['p_district'];
        // $datafor_personalDetail['t_zone'];
        // $datafor_personalDetail['t_district'];
        // $datafor_personalDetail['nationality'];
        
        // $datafor_personalDetail['citizenship_no'];
        // $datafor_personalDetail['language'];
        // $datafor_personalDetail['phone_no'];
        // $datafor_personalDetail['email'];
        // $datafor_personalDetail['gender'];
        // $datafor_personalDetail['mobile_no'];
        // $datafor_personalDetail['contact_no'];
        // $datafor_personalDetail['status'];
        // unset( $datafor_personalDetail['del_flag']);
        // unset($datafor_personalDetail['father_name']);
        // unset($datafor_personalDetail['grand_father_name']);
        // unset($datafor_personalDetail['posted_date']);
        // unset($datafor_personalDetail['download_link']);
        // unset($datafor_personalDetail['upload_image']);
        // unset($datafor_personalDetail['p_id']);
        // $success = Personal_detail::Insert($datafor_personalDetail);

        // $personal_detail = Personal_detail::all();
        // $personal_detail_id = $personal_detail[count($personal_detail) - 1]['p_id'];

        // DB::table('activity_log')->insert(['user_id' => Auth::user()->p_id, 'action' => "add", 'date' => new Carbon(), 'file' => "PERSONAL_DETAILS", 'file_id' => $personal_detail_id]); //create acrivity log     


        // dd($data);
        // return redirect()->route('admin.personal_details');
        // //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page['title'] = 'Personal_detail';
       // $personal_detail = Personal_detail::all();
        $personal_detail = Personal_detail::where('p_id',$id)->get();
        $education = Education::where('p_id',$id)->get();
        $extra_course=Extra_course::where('p_id',$id)->get();
        $preference_available=Preference_available::where('p_id',$id)->get();
        $orgreference=Orgreference::where('p_id',$id)->get();
        $work_experience=Work_experience::where('p_id',$id)->get();
        //return $personal_detail;
        return view("personal_detail::detail",compact('page','personal_detail','education','extra_course','preference_available','orgreference','work_experience'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $personal_detail = Personal_detail::findOrFail($id);
        $page['title'] = 'Personal_detail | Update';
        return view("personal_detail::edit",compact('page','personal_detail'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token');
        $success = Personal_detail::where('id', $id)->update($data);
        return redirect()->route('admin.personal_details');
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      
        $data = soft_delete('tbl_personal_details', 'p_id', $id);
       // DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "personal_details", 'file_id' => $id]); //create acrivity log
        return redirect()->route('admin.personal_details')->with('success', "Data deleted for trash.");
        //
    }
}
