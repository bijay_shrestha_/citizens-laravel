@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Personal_details		
		</h1>
                <div class="breadcrumb">
                        <div class="pull-right"><a href="" class="btn btn-success "></i> Excel</a></div>
                        </div>
	</section>
	<section class="content">
            @php
            $personal_detail=$personal_detail[0];
            $education=$education[0];
            $extra_course=$extra_course[0]; 
            $preference_available=$preference_available[0];
            $orgreference=$orgreference[0];
            $work_experience=$work_experience[0];
        @endphp
		<div class="box box-primary">
			<div class="box-body">		
					<!-- /.box-header -->
				<div class="row">
                    <div class="col-xs-12">
						<div class="box">
					        <div class="box-body">
                            <table id="personal_detail-datatable" class="table table-striped table-bordered table-hover">
                          
                            <tbody>
                                <tr>
                                    <th>Name</th>
                                        <td>{{ $personal_detail->first_name }} @if($personal_detail->midle_name!=null) {{ $personal_detail->midle_name }} @endif {{ $personal_detail->last_name}}</td>
                                </tr>
                                <tr>
                                        <th>Permanent Address</th>
                                            <td>{{ $personal_detail->permanent_address }}</td>
                                </tr>
                                <tr>
                                        <th>Zone</th>
                                            <td>{{ $personal_detail->p_zone }}</td>
                                </tr>
                                <tr>
                                        <th>District</th>
                                            <td>{{ $personal_detail->p_district }}</td>
                                </tr>
                                <tr>
                                        <th>Temporary Address</th>
                                            <td>{{ $personal_detail->temporary_address }}</td>
                                </tr>
                                <tr>
                                        <th>Zone</th>
                                            <td>{{ $personal_detail->t_zone }}</td>
                                </tr>
                                <tr>
                                        <th>District</th>
                                            <td>{{ $personal_detail->t_district }}</td>
                                </tr>
                                <tr>
                                        <th>Date of Birth</th>
                                            <td>{{ $personal_detail->dob }}</td>
                                </tr>
                                <tr>
                                        <th>Nationality</th>
                                            <td>{{ $personal_detail->nationality }}</td>
                                </tr>
                                
                                <tr>
                                        <th>curriculum vitae</th>
                                            <td><ul class="mailbox-attachments clearfix">
                                                <li>
                                                  <span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>
                                                  <div class="mailbox-attachment-info">
                                                    <a class="iframe" href="{{ url('uploads/cv/'.$personal_detail->cv)}}"><i class="fa fa-paperclip"></i>Curriculum Vitae</a>
                                                    
                                                  </div>
                                                </li>
                                              
                                                
                                              </ul></td>
                            
                                </tr>
                                <tr>
                                        <th>Citizenship Number</th>
                                            <td>{{ $personal_detail->citizenship_no }}</td>
                                </tr>
                                <tr>
                                        <th>Langauge </th>
                                            <td>{{ $personal_detail->language }}</td>
                                </tr>
                                <tr>
                                        <th>Phone Number </th>
                                            <td>{{ $personal_detail->citizenship_no }}</td>
                                </tr>
                                <tr>
                                        <th>Phone Number </th>
                                            <td>{{ $personal_detail->phone_no }}</td>
                                </tr>
                                <tr>
                                        <th>Email Address </th>
                                            <td>{{ $personal_detail->email }}</td>
                                </tr>
                                <tr>
                                        <th>Gender</th>
                                            <td>{{ $personal_detail->gender }}</td>
                                </tr>
                                <tr>
                                        <th>Marital Status</th>
                                            <td>{{ $personal_detail->marital_status }}</td>
                                </tr>
                                <tr>
                                        <th>Immediate Contact</th>
                                            <td>{{ $personal_detail->immediate_contact }}</td>
                                </tr>
                                <tr>
                                        <th>Mobile Number</th>
                                            <td>{{ $personal_detail->mobile_no }}</td>
                                </tr>
                                <tr>
                                        <th>Contact Number</th>
                                            <td>{{ $personal_detail->contact_no }}</td>
                                </tr>
                            </tbody>
                        </table>
        <div class="row" style="margin-top:10px;">
            <div class="box">
                <div class="box-body overflow-horizontal">
                        <h3>Academic Detail</h3>
                        <table id="educations-datatable" class="table table-striped table-bordered  table-hover ">
                                <thead>
                                    <th>Academic Level</th>  
                                    <th>Board/Univeristy</th>
                                    <th>School/College</th>
                                    <th>Degree/ Faculty</th>
                                    <th>Obtained % or GPA</th>
                                    <th>Division</th>
                                    <th>Passed Year</th>
                                </thead>
                            <tbody>
                                <tr>
                                    <th>SLC or Equivalent</th>
                                    <td>{{ $education->s_board }}</td>
                                    <td>{{ $education->s_school }}</td>
                                    <td>{{ $education->s_degree }}</td>
                                    <td>{{ $education->s_marks }}</td>
                                    <td>{{ $education->s_division }}</td>
                                    <td>{{ $education->s_passed_year }}</td>
                                </tr>
                                <tr>
                                    <th>10+2 or Equivalent </th>
                                    <td>{{ $education->c_university }}</td>
                                    <td>{{ $education->c_college }}</td>
                                    <td>{{ $education->c_degree }}</td>
                                    <td>{{ $education->c_marks }}</td>
                                    <td>{{ $education->c_division }}</td>
                                    <td>{{ $education->c_passed_year }}</td>
                                    
                                </tr>
                                <tr>
                                    <th>Bachelors Degree</th>
                                    <td>{{ $education->c_university }}</td>
                                    <td>{{ $education->b_college }}</td>
                                    <td>{{ $education->b_faculty }}</td>
                                    <td>{{ $education->b_marks }}</td>
                                    <td>{{ $education->b_division }}</td>
                                    <td>{{ $education->b_passed_year }}</td>
                                </tr>
                                <tr>
                                    <th>Master Degree</th>
                                    <td>{{ $education->m_university }}</td>
                                    <td>{{ $education->m_college }}</td>
                                    <td>{{ $education->m_faculty }}</td>
                                    <td>{{ $education->m_marks }}</td>
                                    <td>{{ $education->m_division }}</td>
                                    <td>{{ $education->m_passed_year }}</td>
                                    
                                </tr>

                            </tbody>
                            </table>
                </div>
            </div>
        </div>
					       
        {{-- academic div close --}}
        <div class="row" style="margin-top:10px;">
                <div class="box">
                    <div class="box-body overflow-horizontal">
                            <h3>Short-term Course/Training</h3>
                            <table id="extra_course-datatable" class="table table-striped table-bordered  table-hover ">
                                    <thead>
                                        <th>Institute Name</th>  
                                        <th>Course Name</th>
                                    </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $extra_course->institude }}</td>
                                        <td>{{ $extra_course->course }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                 {{-- Short-term Course/Training div close --}}
                 <div class="row" style="margin-top:10px;">
                    <div class="box">
                        <div class="box-body overflow-horizontal">
                                <h3>Working Experience</h3>
                                <table id="extra_course-datatable" class="table table-striped table-bordered  table-hover ">
                                        <thead>
                                            <th>Organization</th>  
                                            <th>Position</th>
                                            <th>Department</th>
                                            <th>Experience</th>
                                            <th>Experience From</th>
                                            <th>Experience To</th>
                                        </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{ $work_experience->organization }}</td>
                                            <td>{{ $work_experience->position }}</td>
                                            <td>{{ $work_experience->department }}</td>
                                            <td>{{ $work_experience->experience }}</td>
                                            <td>{{ $work_experience->experience_from}}</td>
                                            <td>{{ $work_experience->experience_to }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- working experience div close --}}
                 <div class="row" style="margin-top:10px;">
                        <div class="box">
                                <div class="col-xs-12">
                            <div class="box-body overflow-horizontal">
                                    <h3>Availability and Preferences</h3>
                                    <table id="preference_available-datatable" class="table table-striped table-bordered  table-hover ">
                                        <tbody>
                                            <tr>
                                                <th>Preferred Post</th>
                                                <td>{{ $preference_available->preferred_post }}</td>
                                            </tr>
                                            <tr>
                                                    <th>Specialised Area</th>
                                                    <td>{{ $preference_available->specialised_area }}</td>
                                            </tr>
                                            <tr>
                                                    <th>Preferred Location</th>
                                                    <td>{{ $preference_available->preferred_location }}</td>
                                            </tr>
                                            <tr>
                                                    <th>Expected Salary</th>
                                                    <td>{{ $preference_available->expected_salary }}</td>
                                            </tr>
                                            <tr>
                                                    <th>Travelling Option</th>
                                                    <td>{{ $preference_available->travelling_option }}</td>
                                            </tr>
                                            <tr>
                                                    <th>Driving License for</th>
                                                    <td>{{ $preference_available->driving_license }}</td>
                                            </tr>
                                            <tr>
                                                    <th>Vacancy Code</th>
                                                    <td>{{ $preference_available->vacancy_code }}</td>
                                            </tr>
                                            <tr>
                                                    <th>Reference 1</th>
                                                    <td>{{ $orgreference->reference }}</td>
                                            </tr>
                                            <tr>
                                                    <th>Post</th>
                                                    <td>{{ $orgreference->post }}</td>
                                            </tr>
                                            <tr>
                                                <th>Organization</th>
                                                <td>{{ $orgreference->organization }}</td>
                                            </tr>
                                            <tr>
                                                <th>Address</th>
                                                <td>{{ $orgreference->address }}</td>
                                            </tr>
                                            <tr>
                                                <th>Email</th>
                                                <td>{{ $orgreference->email }}</td>
                                            </tr>
                                            <tr>
                                                <th>Telephone</th>
                                                <td>{{ $orgreference->telephone }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        {{--Availability and Preferences div ended--}}
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	
@endsection
