<?php

namespace App\modules\remittance_content\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\remittance_content\Model\Remittance_content;
use Carbon\Carbon;

class AdminRemittance_contentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Remittance Content';
        return view("remittance_content::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getremittance_contentsJson(Request $request)
    {
        $remittance_content = new Remittance_content;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $remittance_content->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('id', "DSC")->get();

        // Display limited list        
        $rows = $remittance_content->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->orderBy('id', "DSC")->get();

        //To count the total values present
        $total = $remittance_content->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Remittance Content | Create';
        return view("remittance_content::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $datafor_r_content = $data;
        $datafor_r_content['added_date'] = new Carbon();
        $datafor_r_content['added_by'] = Auth::user()->id;
        $datafor_r_content['del_flag'] = 0;
        $success = Remittance_content::Insert($datafor_r_content);
        return redirect()->route('admin.remittance_contents')->with('success', "Data inserted successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $remittance_content = Remittance_content::findOrFail($id);
        $page['title'] = 'Remittance Content | Update';
        return view("remittance_content::edit",compact('page','remittance_content'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $datafor_r_content = $data;
        $datafor_r_content['modified_date'] = new Carbon();
        $datafor_r_content['modified_by'] = Auth::user()->id;
        DB::table('tbl_remittance_contents')->where('id', $id)->update($datafor_r_content);
        return redirect()->route('admin.remittance_contents')->with('success', "Data updated successfully.");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_remittance_contents', 'id', $id);
        return redirect()->route('admin.remittance_contents')->with('success', "Data deleted for trash.");

        //
    }
}
