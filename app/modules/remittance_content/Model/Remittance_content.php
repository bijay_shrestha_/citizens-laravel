<?php

namespace App\Modules\Remittance_content\Model;


use Illuminate\Database\Eloquent\Model;

class Remittance_content extends Model
{
    public  $table = 'tbl_remittance_contents';

    protected $fillable = ['id','title','content','status','del_flag','added_date','added_by','modified_date','modified_by',];
}
