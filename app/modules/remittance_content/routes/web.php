<?php



Route::group(array('prefix'=>'admin/','module'=>'remittance_content','middleware' => ['web','auth'], 'namespace' => 'App\modules\remittance_content\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('remittance_contents/','AdminRemittance_contentController@index')->name('admin.remittance_contents');
    Route::post('remittance_contents/getremittance_contentsJson','AdminRemittance_contentController@getremittance_contentsJson')->name('admin.remittance_contents.getdatajson');
    Route::get('remittance_contents/create','AdminRemittance_contentController@create')->name('admin.remittance_contents.create');
    Route::post('remittance_contents/store','AdminRemittance_contentController@store')->name('admin.remittance_contents.store');
    Route::get('remittance_contents/show/{id}','AdminRemittance_contentController@show')->name('admin.remittance_contents.show');
    Route::get('remittance_contents/edit/{id}','AdminRemittance_contentController@edit')->name('admin.remittance_contents.edit');
    Route::match(['put', 'patch'], 'remittance_contents/update/{id}','AdminRemittance_contentController@update')->name('admin.remittance_contents.update');
    Route::get('remittance_contents/delete/{id}', 'AdminRemittance_contentController@destroy')->name('admin.remittance_contents.edit');
});




Route::group(array('module'=>'remittance_content','namespace' => 'App\modules\remittance_content\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('remittance_contents/','Remittance_contentController@index')->name('remittance_contents');
    
});