@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Remittance Contents   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.remittance_contents') }}">Remittance Contents</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.remittance_contents.update', $remittance_content->id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                
                    <div class="form-group">
                        <label for="title">Title</label><input type="text" value = "{{$remittance_content->title}}"  name="title" id="title" class="form-control" required >
                    </div>
                    
                    {{--
                    <div class="form-group">
                        <label for="content">Content</label><texarea name="content" id="content" class="form-control" >{!! $remittance_content->content !!}</texarea>
                    </div> 
                    --}}

                    <div class="form-group">
                        <label for="content">Content</label><textarea name="content" id="content" class="form-control my-editor" >{!! $remittance_content->content !!}</textarea>
                    </div>


                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="status" id="status1" @if($remittance_content->status == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="status" id="status0" @if($remittance_content->status == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>

<input type="hidden" name="id" id="id" value = "{{$remittance_content->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.remittance_contents') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
