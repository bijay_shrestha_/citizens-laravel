@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Remittance Contents   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.remittance_contents') }}">Remittance Contents</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.remittance_contents.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                        <label for="title">Title</label><input type="text" name="title" id="title" class="form-control" required>
                    </div>
                    
                    <div class="form-group">
                        <label for="content">Content</label><textarea name="content" id="content" class="form-control my-editor" ></textarea>
                    </div>
                    
                     <div class="form-group">
                         <label for="status">Status</label>
                         <div class="radio">
                         <label>
                             <input type="radio" value="1" name="status" id="status1" @if(old('status') == 1) checked @endif  />Yes
                         </label> 
                         <label>
                             <input type="radio" value="0" name="status" id="status0" @if(old('status') == 1) @else  checked @endif  />No
                         </label>
                         </div>
                     </div>
                   
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.remittance_contents') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
