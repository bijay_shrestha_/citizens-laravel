<?php

namespace App\Modules\Account\Model;


use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public  $table = 'tbl_accounts';

    protected $fillable = ['account_id','parent_id','template_id','business_type','name','slug_id','slug_name','image_name','banner_collection_id','faq_collection_id','description','facilities','account','interest_rate','fixed_rate','min_balance','download_form_link','apply_online_link','submit_form_link','instant_cash','approvel_status','approved','sequence','added_date','added_by','modified_date','deleted_date','del_flag','deleted_by','status','is_online',];
}
