<?php

namespace App\modules\account\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\account\Model\Account;
use App\modules\banner_collection\Model\Banner_collection;
use App\modules\faq_collection\Model\Faq_collection;
use App\modules\account_parent\Model\Account_parent;
use App\modules\account_interest_rate\Model\Account_interest_rate;
use Carbon\Carbon;
use Validator;
use File;
use Image;

class AdminAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Account';
        return view("account::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getaccountsJson(Request $request)
    {
        //$account = new Account;
        $account = DB::table('tbl_accounts');
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $account->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )
        ->leftjoin('tbl_account_parent', 'tbl_account_parent.id', '=', 'tbl_accounts.parent_id')
        ->leftjoin('tbl_banner_collections', 'tbl_banner_collections.banner_collection_id', '=', 'tbl_accounts.banner_collection_id')
        ->select('tbl_accounts.*', 'tbl_account_parent.name as parentname', 'tbl_banner_collections.name as banner_name')
        ->where('tbl_accounts.del_flag',0)
        ->orderBy('tbl_accounts.account_id', 'DSC')
        ->get();


        //->select('tbl_accounts.*', 'tbl_banner_collections.name as banner_name')
        //->select('tbl_accounts.*')
        
        // Display limited list        
        $rows = $account->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('tbl_accounts.del_flag',0)->orderBy('tbl_accounts.account_id', 'DSC')->get();

        //To count the total values present
        $total = $account->where('tbl_accounts.del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }
    public function slugify($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        // trim
        $text = trim($text, '-');
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        // lowercase
        $text = strtolower($text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        if (empty($text)) {
            return 'n-a';
        }
        return $text;
    }
    public function validate_slug($slug, $id=false, $count=false)
    {
        if($this->check_slug($slug.$count, $id))
        {
            if(!$count)
            {
                $count  = 1;
            }
            else
            {
                $count++;
            }
            return $this->validate_slug($slug, $id, $count);
        }
        else
        {
            return $slug.$count;
        }
    }
    
    public function check_slug($slug, $id=FALSE)
    {
        $slugdata = DB::table('slugs');
        if($id)
        {
            $slugdata = $slugdata->where('slug_id', '!=', $id);
        }
        $slugdata = $slugdata->where('slug_name', $slug)->get();
        
        return (bool) (count($slugdata));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Account | Create';
        $banner_collections = Banner_collection::all();
        $faq_collections = Faq_collection::all();
        $account_parents = Account_parent::all();
        return view("account::add",compact('page', 'banner_collections', 'faq_collections', 'account_parents'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $name_slug = $this->slugify($data['name']);
        $slug_name = $this->validate_slug($name_slug);
        $slug_data = DB::table('slugs')->insert(['slug_name' => $slug_name]); //create slug
        $allslug =  DB::table('slugs')->get();
        $dataforaccount = $data;
        $rating_clones = count($dataforaccount['rate_name']);
        
        unset($dataforaccount['rate_name']);
        unset($dataforaccount['rate_id']);
        unset($dataforaccount['rate_max_rate']);
        unset($dataforaccount['rate_min_rate']);
        unset($dataforaccount['rate_status']);
        unset($dataforaccount['id']);
        unset($dataforaccount['download_link']);
        unset($dataforaccount['upload_image']);
        
        $dataforaccount['slug_id'] = $allslug[count($allslug)-1]->slug_id;
        $dataforaccount['slug_name'] = $slug_name;
        $dataforaccount['del_flag'] = 0;
        $dataforaccount['added_by'] = Auth::user()->id;
        $dataforaccount['added_date'] = new Carbon();
        //dd($dataforaccount);
        $dataforaccount = check_case($dataforaccount, 'create');
        $success = Account::Insert($dataforaccount); //create account
        $accounts = Account::all();
        $account_id = $accounts[count($accounts)-1]['account_id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "account", 'file_id' => $account_id]); //create acrivity log
        $slug['route'] = 'account/detail/' . $account_id;
        DB::table('slugs')->where('slug_id', $dataforaccount['slug_id'])->update(['route' => $slug['route']]);
        $interest_rates = [];
        for ($i = 0; $i < $rating_clones; $i++) {
            $interest_rate = [];
            $interest_rate['account_id'] = $account_id;
            $interest_rate['rate_name'] = $request->rate_name[$i];
            $interest_rate['per_annum_min'] = $request->rate_min_rate[$i];
            $interest_rate['per_annum_max'] = $request->rate_max_rate[$i];
            $interest_rate['status'] = $request->rate_status[$i];
            $interest_rate['added_by'] = Auth::user()->id;
            $interest_rate['added_date'] = new Carbon();
            $success_interest_rate = Account_interest_rate::Insert($interest_rate); //create interest rate
            $interest_rates["data".($i +1)] = $interest_rate; 
        }

        return redirect()->route('admin.accounts')->with('success', "Data added Successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $account = Account::where('account_id',$id)->firstOrFail();
        $page['title'] = 'Account | Update';
        $banner_collections = Banner_collection::all();
        $faq_collections = Faq_collection::all();
        $account_parents = Account_parent::all();
        $account_interest_rates = Account_interest_rate::where('account_id', $id)->where('del_flag', 0)->get();
        return view("account::edit",compact('page','account', 'banner_collections', 'faq_collections', 'account_parents', 'account_interest_rates'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "account", 'file_id' => $id]); //create activity log
        $account = Account::where('account_id', $data['account_id'])->firstOrFail();
        $name_slug = $this->slugify($data['name']);
        $slug['slug_name'] = $this->validate_slug($name_slug, $account->slug_id);
        $slug['route'] = 'account/detail/'.$data['account_id'];
        $data['sequence'] = ($data['sequence'] == null) ? "0":$data['sequence'];
        DB::table('slugs')->where('slug_id', $account->slug_id)->update(['slug_name' =>  $slug['slug_name'], 'route' => $slug['route']]);
        $dataforaccount = $data;
        $rating_clones = count($dataforaccount['rate_name']);
        unset($dataforaccount['rate_name']);
        unset($dataforaccount['rate_id']);
        unset($dataforaccount['rate_max_rate']);
        unset($dataforaccount['rate_min_rate']);
        unset($dataforaccount['rate_status']);
        unset($dataforaccount['id']);
        unset($dataforaccount['download_link']);
        unset($dataforaccount['upload_image']);
        $dataforaccount['slug_name'] = $slug['slug_name'];
        
        /*if ($data['image_name']) {
            if ((file_exists(public_path().'/uploads/account/'. $account->image_name)) && $account->image_name) {
                unlink(public_path().'/uploads/account/'. $account->image_name);
                if (file_exists(public_path().'/uploads/account/thumb/'. $account->image_name)) {
                    unlink(public_path().'/uploads/account/thumb/'. $account->image_name);
                } 
            }
        } else {
            $data['image_name'] = $account->image_name;
            $dataforaccount['image_name'] = $account->image_name;
        }
        if ($data['download_form_link']) {
            if ((file_exists(public_path().'/uploads/account/form/'. $account->download_form_link)) && $account->download_form_link) {
                unlink(public_path().'/uploads/account/form/'. $account->download_form_link);                
            }
        } else {
            $data['download_form_link'] = $account->download_form_link;
            $dataforaccount['download_form_link'] = $account->download_form_link;
        }*/

        if (array_key_exists('delete_old_image', $data)) {
            if ((file_exists(public_path().'/uploads/account/'. $account->image_name))  && $account->image_name) {
                unlink(public_path().'/uploads/account/'. $account->image_name);
                if (file_exists(public_path().'/uploads/account/thumb/'. $account->image_name)) {
                    unlink(public_path().'/uploads/account/thumb/'. $account->image_name);
                } 
            }
            if (!$data['image_name']) {
                $data['image_name'] = '';
                $dataforaccount['image_name'] = '';
            }
            unset($dataforaccount['delete_old_image']);
        } else {
            if (!$data['image_name']) {
                $data['image_name'] = $account->image_name;
                $dataforaccount['image_name'] = $account->image_name;
            }
        }

        if (array_key_exists('delete_old_file', $data)) {
            if ((file_exists(public_path().'/uploads/account/form/'. $account->download_form_link)) && $account->download_form_link) {
                unlink(public_path().'/uploads/account/form/'. $account->download_form_link);                
            }
            if (!$data['download_form_link']) {
                $data['download_form_link'] = '';
                $dataforaccount['download_form_link'] = '';
            }
            unset($dataforaccount['delete_old_file']);
        } else {
            if (!$data['download_link']) {
                $data['download_form_link'] = $account->download_form_link;
                $dataforaccount['download_form_link'] = $account->download_form_link;
            }            
        }
        $dataforaccount['modified_date'] = new Carbon();
        $dataforaccount = check_case($dataforaccount, 'edit');        
        $success = DB::table('tbl_accounts')->where('account_id', $id)->update($dataforaccount);

        //for updating rates
        $account_interest_rates = Account_interest_rate::where('account_id', $id)->where('del_flag', 0)->get();
        $ids = [];
        for ($i = 0 ; $i < count($account_interest_rates); $i++) {
            $thecount = 0;
            for ($j = 0; $j < count($request->rate_name); $j++) {
                
                if (($request->rate_id[$j] == $account_interest_rates[$i]->rate_id)) {
                    $thecount++;
                } 
            }
            if ($thecount == 0) {
                //$interest_rate['del_flag'] = 1;
                //$interest_rate['deleted_by'] = Auth::user()->id;
                //$interest_rate['deleted_at'] = new Carbon();
                //$success = Account_interest_rate::where('rate_id', $account_interest_rates[$i]->rate_id)->update($interest_rate);
                $data_delete = soft_delete('tbl_account_interest_rates', 'rate_id', $account_interest_rates[$i]->rate_id, 'deleted_by', 'deleted_date');
            }
        }
        for ($k = 0; $k < count($request->rate_name); $k++) {
                $interest_rate = [];
                $interest_rate['rate_name'] = $request->rate_name[$k];
                $interest_rate['per_annum_min'] = $request->rate_min_rate[$k];
                $interest_rate['per_annum_max'] = $request->rate_max_rate[$k];
                $interest_rate['status'] = $request->rate_status[$k];
            
            if ($request->rate_id[$k] == null || $request->rate_id[$k] == '') {
                $interest_rate['account_id'] = $id;
                $interest_rate['added_by'] = Auth::user()->id;
                $interest_rate['added_date'] = new Carbon();
                $success_interest_rate = Account_interest_rate::Insert($interest_rate); //create interest rate
            } else {
                for ($l =0; $l <count($account_interest_rates); $l++) {
                    if (($request->rate_id[$k] == $account_interest_rates[$l]->rate_id)) {
                        //$success = Account_interest_rate::where('rate_id', $request->rate_id[$k])->update($interest_rate);
                        $interest_rate['modified_date'] = new Carbon();
                        $success = DB::table('tbl_account_interest_rates')->where('rate_id', $request->rate_id[$k]  )->update($interest_rate);
                    }
                }
            }
        }
        return redirect()->route('admin.accounts')->with('success', "Data updated Successfully.");
        
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_accounts', 'account_id', $id, 'deleted_by', 'deleted_date');
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "account", 'file_id' => $id]); //create acrivity log
        return redirect()->route('admin.accounts')->with('success', "Data deleted for trash.");

        //
    }

    public function uploadFile(Request $request) {
        $validator = Validator::make($request->all(),
           [
           'download_link' => 'required|max:10240', 
           //'download_link' => 'required'
           ], 
           [
            'download_link.required' => 'Please upload a file',
            'download_link.max' => 'The filesize is too large'
            ]);

        if ($validator->fails()) {
            return response()->json(['fail'=> true, 'errors' =>$validator->errors(), 'directory' => $request->directory]);
        } else {
            $thefile = $request->file('download_link');
            $new_name = time() . '.' . $request->download_link->getClientOriginalExtension();
            $thefile->move(public_path('uploads/'.$request->directory.'/form/'), $new_name);
            return response()->json(['fail'=> false, 'filename' => $new_name, 'originalname' => $request->download_link->getClientOriginalName(), 'directory' => $request->directory]);
        }
        //echo json_encode(['error' => null, 'filename' => "hello"]);
    }

    public function deleteFile (Request $request) {
        if (file_exists(public_path().'/uploads/'.$request->directory.'/thumb/'. $request->filename)) {
            unlink(public_path().'/uploads/'.$request->directory.'/thumb/'. $request->filename);
            if (file_exists(public_path().'/uploads/'.$request->directory.'/'. $request->filename)) {
                unlink(public_path().'/uploads/'.$request->directory.'/'. $request->filename);              
                return response()->json(['success' => "yes", 'name' => $request->filename]);  
            } else {
                return response()->json(['success' => "yes", 'name' => $request->filename]);  
            }
        } else {
            if (file_exists(public_path().'/uploads/'.$request->directory.'/form/'. $request->filename)) {
                unlink(public_path().'/uploads/'.$request->directory.'/form/'. $request->filename);              
                return response()->json(['success' => "yes", 'name' => $request->filename]);  
            } else {
                return response()->json(['success' => "no"]);
            }
        }
    }

    public function uploadImage (Request $request) 
    {
        $validator = Validator::make($request->all(),
           [
           'upload_image' => 'required|image|max:10240', 
           //'download_link' => 'required'
           ], 
           [
            'upload_image.required' => 'Please upload a file',
            'upload_image.image' => 'Please upload an image',
            //'upload_image.mimes' => 'Only  jpeg, png,jpg,gif,svg are valid',
            'upload_image.max' => 'The image size is too large'
            ]);
        if ($validator->fails()) {
            return response()->json(['fail' => true, 'errors' => $validator->errors()]);
        } else {
            $image = $request->file('upload_image');
            $thumbnail = $image;
            $name = time();
            $filename = $name . '.' . $image->getClientOriginalExtension();
            $thumbname = $name . '.' . $thumbnail->getClientOriginalExtension();
            
            $location = public_path('/uploads/'.$request->directory.'/' . $filename);
            $thumblocation = public_path('/uploads/'.$request->directory.'/thumb/'. $thumbname);

            Image::make($image)->save($location);
            Image::make($thumbnail)->resize(100, 100)->save($thumblocation);
            

            return response()->json(['fail'=> false, 'filename' => $filename, 'originalname' => $request->upload_image->getClientOriginalName()]);
        }
    }

        public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_accounts', 'account_id', 'deleted_by', 'deleted_date');
        return redirect()->route('admin.accounts')->with('success', "Data Updated");
    }


}
