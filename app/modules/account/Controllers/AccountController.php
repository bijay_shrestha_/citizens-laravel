<?php

namespace App\Modules\Account\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\account\Model\Account;
use App\modules\banner\Model\Banner;
use App\Modules\Account_interest_rate\Model\Account_interest_rate;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'accounts';
        $accounts = get_lists('tbl_accounts');
        $banners = Banner::where('banner_collection_id', 3)->get();

        return view('front.account.main-deposit', compact('page', 'accounts', 'banners'));
    }

    public function detail($slug)
    {
        $accounts = Account::where('slug_name',$slug)->where('del_flag',0)->first();
        $account_names = Account::where('slug_name','!=',$slug)->where('del_flag',0)->get();
        $interest_rates = Account_interest_rate::where('account_id',$accounts->account_id)->where('del_flag',0)->first();
        // dd($account->banner_collection_id);
        $banners = Banner::where('banner_collection_id', $accounts->banner_collection_id)->get();
        // dd($banners);

        return view('front.account.show-all',compact('accounts','interest_rates','banners','account_names'));
    }

}
