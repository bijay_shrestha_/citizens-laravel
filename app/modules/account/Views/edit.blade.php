@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Accounts   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.accounts') }}">accounts</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.accounts.update', $account->account_id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}
                <div class="form-group">
                        <label for="name">Name (*)</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{$account->name}}" required>
                    </div>

                    <div class="form-group">
                        <label for="image_name">Image </label><br>
                        <label id="upload_image_name" style="display:none"></label>
                        <div id="delete_old_image_requested"></div>
                        <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                        <div id="image-status"></div>
                        <input type="file" id="upload_image" name="upload_image" @if (file_exists(public_path().'/uploads/account/thumb/'. $account->image_name) && $account->image_name) style="display:none" @else style="display:block" @endif />
                        <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                        <div id="imagedata">
                        @if (file_exists(public_path().'/uploads/account/'. $account->image_name) && $account->image_name)
       
                            <img src="{{url('uploads/account/thumb/'.$account->image_name)}}">
                            <a href="javascript:void(0)" id="change-image_option" title="Change Image">&nbsp; &nbsp; <img src="{{('/images/logo/cancel.png')}}" border="0"/> Change the old image</a>
                        @endif                                
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="description">Description (*)</label>
                        <textarea name="description" id="description" class="form-control my-editor" >{!! $account->description !!}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="banner_collection_id">Banner Collection </label>
                        <select name="banner_collection_id" class="form-control">
                            <option value="0">Select Banner </option> 
                            @foreach ($banner_collections as $banner_collection)
                                <option value="{{$banner_collection['banner_collection_id']}}" 
                                 @if ($account->banner_collection_id == $banner_collection['banner_collection_id']) selected @endif
                                > {{$banner_collection['name']}} </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="parent_id">Parent</label>
                        <select name="parent_id" class="form-control">
                            <option value="0">Select Parent </option> 
                            @foreach ($account_parents as $account_parent)
                                <option value="{{$account_parent['id']}}"
                                  @if ($account->parent_id == $account_parent['id']) selected @endif
                                >{{$account_parent['name']}}</option>
                            @endforeach

                        </select>
                    </div>

                    <div class="form-group">
                        <label for="faq_collection_id">Faq  type</label>
                        <select name="faq_collection_id" class="form-control">
                        @foreach($faq_collections as $faq_collection) 
                                <option value="{{$faq_collection['faq_collection_id']}}"
                                 @if ($account->faq_collection_id == $faq_collection['faq_collection_id']) selected @endif
                                >{{$faq_collection['faq_collection_name']}}</option>
                        @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="facilities">Facilities</label>
                        <textarea name="facilities" id="facilities" class="form-control my-editor" >{!! $account->facilities !!}</textarea>
                    </div>

                   <div>                          
                      <label>Add Interest Rate</label>
                    </div>
                    @if (count($account_interest_rates) == 0)

                         <div id="input1" style="margin-bottom:4px; border:1px #CCC solid; padding:10px" class="clonedInput">

                            <div class="form-group">
                            <input type="hidden" name="rate_id[]" value="{{''}}">
                                <div id="label_rt"><label for="rate_name">Rate name</label></div>
                                <div id="input_rt"><input type="text" class="form-control" name="rate_name[]" id="rate_name" /></div>
                            </div>
                            <div class="form-group">
                                <div id="label_max_rt"><label for="rate_max">Max Rate (Or normal rate)</label></div>
                                <div id="input_max_rt"><input type="text" class="form-control" name="rate_max_rate[]" id="rate_max" /></div>
                            </div>
                            <div class="form-group">
                                <div id="label_min_rt"><label for="rate_min">Min Rate </label></div>
                                <div id="input_min_rt"><input type="text" class="form-control" name="rate_min_rate[]" id="rate_min" /></div>
                            </div>
                            <div class="form-group">
                                <div id="label_status"><label for="rate_status">Status</label></div>
                                <div id="input_status">
                                <select name="rate_status[]" class="form-control" id="rate-status" >
                                  <option value="1">ON</option>
                                  <option value="0">OFF</option>
                                </select>
                               
                                </div>
                            </div>
                            <input type="button" id="btnDel1" value="remove Rate" onclick="removeRate('1')" />
                          </div>
                    @else
                        @foreach ($account_interest_rates as $index => $account_interest_rate)
                            <div id="input{{($index+1)}}" style="margin-bottom:4px; border:1px #CCC solid; padding:10px" class="clonedInput">

                                <div class="form-group">
                                <input type="hidden" name="rate_id[]" value="{{$account_interest_rate->rate_id}}">
                                    <div id="label_rt"><label for="rate_name">Rate name</label></div>
                                    <div id="input_rt"><input type="text" class="form-control" name="rate_name[]" id="rate_name" value="{{$account_interest_rate->rate_name}}" /></div>
                                </div>
                                <div class="form-group">
                                    <div id="label_max_rt"><label for="rate_max">Max Rate (Or normal rate)</label></div>
                                    <div id="input_max_rt"><input type="text" class="form-control" name="rate_max_rate[]" id="rate_max" value="{{$account_interest_rate->per_annum_max}}" /></div>
                                </div>
                                <div class="form-group">
                                    <div id="label_min_rt"><label for="rate_min">Min Rate </label></div>
                                    <div id="input_min_rt"><input type="text" class="form-control" name="rate_min_rate[]" id="rate_min" value="{{$account_interest_rate->per_annum_min}}" /></div>
                                </div>
                                <div class="form-group">
                                    <div id="label_status"><label for="rate_status">Status</label></div>
                                    <div id="input_status">
                                    <select name="rate_status[]" class="form-control" id="rate-status">
                                      <option value="1" @if ($account_interest_rate->status == 1) selected @endif >ON</option>
                                      <option value="0" @if ($account_interest_rate->status == 0) selected @endif >OFF</option>
                                    </select>
                                   
                                    </div>
                                </div>
                                <input type="button" id="btnDel{{($index + 1)}}" value="remove Rate" onclick="removeRate('{{($index + 1)}}')" />

                              </div>
                        @endforeach
                    @endif
                                 <div>
                                  <input type="button" id="btnAdd" value="Add more Rates" />
                                  {{-- <input type="button" id="btnDel" value="remove Rate"  @if (count($account_interest_rates) == 0 ||count($account_interest_rates) == 1 ) disabled="true" @endif /> --}}
                               </div>
                           
                           

                    {{--
                    <div class="form-group">
                        <label for="account">Account</label><input type="text" name="account" id="account" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="interest_rate">Interest Rate</label><input type="text" name="interest_rate" id="interest_rate" class="form-control" >
                    </div> 
                    --}}

                    <div class="form-group"> 
                    <label for="fixed_rate">Fixed Rate</label><input type="text" name="fixed_rate" id="fixed_rate" class="form-control" value="{{$account->fixed_rate}}" required></div>

                    <div class="form-group">
                        <label for="instant_cash">Instant Cash</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="instant_cash" id="instant_cash1" @if($account->instant_cash == 1) checked @endif/>Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="instant_cash" id="instant_cash0" @if($account->instant_cash == 1) @else checked @endif />No</label>
                        </div>
                    </div>
                    <div class="form-group">
                              <label>Download Form file</label><br/><label id="upload_download_link" style="display:none"></label>
                               <div id="delete_old_file_requested"></div>
                              <input type="file" name="download_link" id="download_link" @if (file_exists(public_path().'/uploads/account/form/'. $account->download_form_link) && $account->download_form_link) style="display:none" @else style="display:block" @endif >
                              <div id="file-status"></div>
                              <input type="hidden" name="download_form_link" id="download_form_link"/>
                              <a href="javascript:void(0)" id="change-file" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                              <div id="filedata">
                              @if (file_exists(public_path().'/uploads/account/form/'. $account->download_form_link) && $account->download_form_link)
                              
                                  <a  href="{{url('uploads/account/form/'.$account->download_form_link)}}" target="_blank">{{$account->download_form_link}}</a>
                                  <a href="javascript:void(0)" id="change-file_option" title="Change file">&nbsp; &nbsp; <img src="{{('/images/logo/cancel.png')}}" border="0"/> Change the old file</a>
                              @endif                                
                              </div>
                    </div>
            
                    <div class="form-group">
                    <label for="sequence">Sequence</label><input type="text" name="sequence" id="sequence" class="form-control" value="{{old('sequence')}}"></div>
                    
                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="status" id="status1" @if($account->status == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="status" id="status0" @if($account->status == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="is_online">Show in Online Form</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="is_online" id="is_online1" @if($account->is_online == 1) checked @endif />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="is_online" id="is_online0" @if($account->is_online == 1) @else checked @endif />No
                        </label>
                        </div>
                    </div>
                            
                <input type="hidden" name="account_id" id="account_id" value = "{{$account->account_id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.accounts') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
     <script type="text/javascript">
    var directory = 'account';
</script>
@include('data_changing_script_edit')
@include('file_changing_script')

@endsection
