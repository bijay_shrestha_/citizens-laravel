@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Accounts   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.accounts') }}">accounts</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" enctype="multipart/form-data" action="{{ route('admin.accounts.store') }}"  method="post">
                <div class="box-body">                
                    
                    <div class="form-group">
                        <label for="name">Name (*)</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{old('name')}}" required>
                    </div>

                    <div class="form-group">
                        <label for="image_name">Image </label><br>
                        <label id="upload_image_name" style="display:none"></label>
                        <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                        <div id="image-status"></div>
                        <input type="file" id="upload_image" name="upload_image" style="display:block"/>
                        <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>

                    </div>

                    <div class="form-group">
                        <label for="description">Description (*)</label>
                        <textarea name="description" id="description" class="form-control my-editor" >{!! old('description') !!}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="banner_collection_id">Banner Collection </label>
                        <select name="banner_collection_id" class="form-control">
                            <option value="0">Select Banner </option> 
                            @foreach ($banner_collections as $banner_collection)
                                <option value="{{$banner_collection['banner_collection_id']}}" 
                                 @if (old('banner_collection_id') == $banner_collection['banner_collection_id']) selected @endif
                                > {{$banner_collection['name']}} </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="parent_id">Parent</label>
                        <select name="parent_id" class="form-control">
                            <option value="0">Select Parent </option> 
                            @foreach ($account_parents as $account_parent)
                                <option value="{{$account_parent['id']}}"
                                  @if (old('parent_id') == $account_parent['id']) selected @endif
                                >{{$account_parent['name']}}</option>
                            @endforeach

                        </select>
                    </div>

                    <div class="form-group">
                        <label for="faq_collection_id">Faq  type</label>
                        <select name="faq_collection_id" class="form-control">
                        @foreach($faq_collections as $faq_collection) 
                                <option value="{{$faq_collection['faq_collection_id']}}"
                                 @if (old('faq_collection_id') == $faq_collection['faq_collection_id']) selected @endif
                                >{{$faq_collection['faq_collection_name']}}</option>
                        @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="facilities">Facilities</label>
                        <textarea name="facilities" id="facilities" class="form-control my-editor" >{!! old('facilities') !!}</textarea>
                    </div>

                   <div>                          
                      <label>Add Interest Rate</label>
                    </div>
                        <div id="input1" style="margin-bottom:4px; border:1px #CCC solid; padding:10px" class="clonedInput">

                            <div class="form-group">
                            <input type="hidden" name="rate_id[]" value="{{''}}">
                                <div id="label_rt"><label for="rate_name">Rate name</label></div>
                                <div id="input_rt"><input type="text" class="form-control" name="rate_name[]" id="rate_name" required /></div>
                            </div>
                            <div class="form-group">
                                <div id="label_max_rt"><label for="rate_max">Max Rate (Or normal rate)</label></div>
                                <div id="input_max_rt"><input type="text" class="form-control" name="rate_max_rate[]" id="rate_max" required /></div>
                            </div>
                            <div class="form-group">
                                <div id="label_min_rt"><label for="rate_min">Min Rate </label></div>
                                <div id="input_min_rt"><input type="text" class="form-control" name="rate_min_rate[]" id="rate_min"  required/></div>
                            </div>
                            <div class="form-group">
                                <div id="label_status"><label for="rate_status">Status</label></div>
                                <div id="input_status">
                                <select name="rate_status[]" class="form-control" id="rate-status" required>
                                  <option value="1">ON</option>
                                  <option value="0">OFF</option>
                                </select>
                               
                                </div>
                            </div>
                          </div>
                                 <div>
                                  <input type="button" id="btnAdd" value="Add more Rates" />
                                  <input type="button" id="btnDel" value="remove Rate" />
                               </div>
                           
                           

                    {{--
                    <div class="form-group">
                        <label for="account">Account</label><input type="text" name="account" id="account" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="interest_rate">Interest Rate</label><input type="text" name="interest_rate" id="interest_rate" class="form-control" >
                    </div> 
                    --}}

                    <div class="form-group"> 
                    <label for="fixed_rate">Fixed Rate</label><input type="text" name="fixed_rate" id="fixed_rate" class="form-control" value="{{old('fixed_rate')}}"  required></div>

                    <div class="form-group">
                        <label for="instant_cash">Instant Cash</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="instant_cash" id="instant_cash1" @if(old('instant_cash') == 1) checked @endif/>Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="instant_cash" id="instant_cash0" @if(old('instant_cash') == 1) @else checked @endif />No</label>
                        </div>
                    </div>
                    <div class="form-group">
                              <label>Download Form file</label><br/><label id="upload_download_link" style="display:none"></label>
                              <!-- <input type="text" name="download_form_link" id="download_form_link"  value="" class="form-control"> -->
                              <input type="file" name="download_link" id="download_link" >
                              <div id="file-status"></div>
                              <input type="hidden" name="download_form_link" id="download_form_link"/>
                              <a href="javascript:void(0)" id="change-file" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>

                    </div>
            
                    <div class="form-group">
                    <label for="sequence">Sequence</label><input type="text" name="sequence" id="sequence" class="form-control" value="{{old('sequence')}}"></div>
                    
                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="status" id="status1" @if(old('status') == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="status" id="status0" @if(old('status') == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="is_online">Show in Online Form</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="is_online" id="is_online1" @if(old('is_online') == 1) checked @endif />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="is_online" id="is_online0" @if(old('is_online') == 1) @else checked @endif />No
                        </label>
                        </div>
                    </div>
                    <input type="hidden" name="account_id" id="account_id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.accounts') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
    <script type="text/javascript">
          var directory = 'account';

  /*      $(document).ready(function() {
   
            $('#btnAdd').click(function(){  
              //$('#options_tagsinput').remove(); 
              var num   = $('.clonedInput').length; // how many "duplicatable" input fields we currently have
              var newNum  = new Number(num + 1);    // the numeric ID of the new input field being added
              

              // create the new element via clone(), and manipulate it's ID using newNum value
              var newElem = $('#input' + num).clone().attr('id', 'input' + newNum);
              
              // manipulate the name/id values of the input inside the new element
              //newElem.find('#option_row').attr('id', 'option_row' + newNum);
              // newElem.find('.troptions').attr('id', 'option_row' + newNum);
             
              newElem.find('#label_rt label').attr('for', 'rate_name' + newNum);
              // newElem.find('#input_rt input').attr('name', 'rate[name' + newNum+']').val('');
              // newElem.find('#option_row' + newNum).hide();

              newElem.find('#label_max_rt label').attr('for', 'rate_max' + newNum);
              // newElem.find('#input_max_rt input').attr('name', 'rate[max_rate' + newNum+']').val('');

              newElem.find('#label_min_rt label').attr('for', 'rate_min' + newNum);
              // newElem.find('#input_min_rt input').attr('name', 'rate[min_rate' + newNum+']').val('');

              newElem.find('#label_status label').attr('for', 'rate_status' + newNum);
             
              // newElem.find('#input_status select').attr('name', 'rate[status' + newNum+']');
              // newElem.find('#input_status select').attr('name', 'rate[status' + newNum+']');
             
             
             
              // insert the new element after the last "duplicatable" input field
              $('#input' + num).after(newElem);
              $('#input' + newNum).find('input').val('').end();
              // newElem.find('#options'+(newNum-1)+'_tagsinput').remove();
              $('#btnDel').attr('disabled',false);
            });

          $('#btnDel').click(function(){
                var num = $('.clonedInput').length; // how many "duplicatable" input fields we currently have
                $('#input' + num).remove();   // remove the last element
              
                // enable the "add" button
                $('#btnAdd').attr('disabled',false);
                
                // if only one element remains, disable the "remove" button
                if (num-1 == 1)
                $('#btnDel').attr('disabled',true);
              });

            
            $('#btnDel').attr('disabled',true);

        });
  $( "#download_link" ).change(function() {
      //uploadReady();
      uploadfile(this);
  });

 $("#upload_image").change(function(){
    uploadimage(this);
 });

function uploadimage(thefile)
{
    var form_data = new FormData();
    form_data.append('upload_image', thefile.files[0]);
    form_data.append('_token', '{{csrf_token()}}');
    $('#image-status').html("Image is uploading....");
     $.ajax({
            url: "{{url('/admin/accounts/upload/image')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                  console.log(data.errors);
                  var err = '';
                  data.errors['upload_image'].forEach (function (element) {
                    err += element + '. ';
                  });
                   $('#image-status').css('color', 'red').html(err);
                } else {
                  $('#image-status').html('');
                  $('#upload_image').hide();
                  $('#image_name').val(data.filename);
                  $('#upload_image_name').html(data.originalname);
                  $('#upload_image_name').show();
                  $('#change-image').css('display', 'block');
                  //  $('#file_name').val(data);
                  //  $('#preview_image').attr('src', '{{asset('uploads')}}/' + data);
                }
               
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
              //  $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
            }
        });

}
function uploadfile(thefile)
{
    var form_data = new FormData();
    form_data.append('download_link', thefile.files[0]);
    form_data.append('_token', '{{csrf_token()}}');
    $('#file-status').html("File is uploading....");
     $.ajax({
            url: "{{url('/admin/accounts/upload/file')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                   $('#file-status').css('color', 'red').html(data.errors['download_link']);
                } else {
                  $('#file-status').html('');
                  $('#download_link').hide();
                  $('#download_form_link').val(data.filename);
                  $('#upload_download_link').html(data.originalname);
                  $('#upload_download_link').show();
                  $('#change-file').css('display', 'block');
                  //  $('#file_name').val(data);
                  //  $('#preview_image').attr('src', '{{asset('uploads')}}/' + data);
                }
               
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
              //  $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
            }
        });

}

  $('#change-file').click(function () {
    var filename = $('#download_form_link').val();
    console.log(filename);
    if (confirm('Are you sure you to want to delete this file?')) {
       $.post("{{url('/admin/accounts/delete/file')}}",
        {'filename': filename,'_token':"{{csrf_token()}}"}, function (result) {
           if (result.success == "yes") {
             $('#download_form_link').val('');
             $('#download_link').val('');
             $('#upload_download_link').html('');
             $('#upload_download_link').hide();
             $('#change-file').hide();
             $('#download_link').show();
          } else {
            alert('Something went wrong.');
          }
        });
    }
  }); 

   $('#change-image').click(function () {
    var filename = $('#image_name').val();
    if (confirm('Are you sure you to want to delete this image?')) {
       $.post("{{url('/admin/accounts/delete/file')}}",
        {'filename': filename,'_token':"{{csrf_token()}}"}, function (result) {
           if (result.success == "yes") {
             $('#image_name').val('');
             $('#upload_image').val('');
             $('#upload_image_name').html('');
             $('#upload_image_name').hide();
             $('#change-image').hide();
             $('#upload_image').show();
          } else {
            alert('Something went wrong.');
          }
        });
    }
  }); */
    </script>
    @include('data_changing_script_create')
    @include('file_changing_script')

@endsection


