<?php



Route::group(array('prefix'=>'admin/','module'=>'account','middleware' => ['web','auth'], 'namespace' => 'App\modules\account\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('accounts/','AdminAccountController@index')->name('admin.accounts');
    Route::post('accounts/getaccountsJson','AdminAccountController@getaccountsJson')->name('admin.accounts.getdatajson');
    Route::get('accounts/create','AdminAccountController@create')->name('admin.accounts.create');
    Route::post('accounts/store','AdminAccountController@store')->name('admin.accounts.store');
    Route::get('accounts/show/{id}','AdminAccountController@show')->name('admin.accounts.show');
    Route::get('accounts/edit/{id}','AdminAccountController@edit')->name('admin.accounts.edit');
    Route::match(['put', 'patch'], 'accounts/update/{id}','AdminAccountController@update')->name('admin.accounts.update');
    Route::get('accounts/delete/{id}', 'AdminAccountController@destroy')->name('admin.accounts.edit');
    Route::any('accounts/upload/file', 'AdminAccountController@uploadFile')->name('admin.accounts.upload.file');
    Route::any('accounts/upload/image', 'AdminAccountController@uploadImage')->name('admin.accounts.upload.image');
    Route::post('accounts/delete/file', 'AdminAccountController@deleteFile')->name('admin.accounts.delete.file');
    Route::get('accounts/change/{text}/{status}/{id}', 'AdminAccountController@change');

});




Route::group(array('module'=>'account','namespace' => 'App\modules\account\Controllers', 'middleware' => 'web'), function() {
    //Your routes belong to this module.
    Route::get('accounts/','AccountController@index')->name('accounts');
    Route::get('account/detail/{param}','AccountController@detail')->name('account.detail');
    
});
