<?php



Route::group(array('prefix'=>'admin/','module'=>'other_menu','middleware' => ['web','auth'], 'namespace' => 'App\modules\other_menu\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('other_menus/','AdminOther_menuController@index')->name('admin.other_menus');
    Route::post('other_menus/getother_menusJson','AdminOther_menuController@getother_menusJson')->name('admin.other_menus.getdatajson');
    Route::get('other_menus/create','AdminOther_menuController@create')->name('admin.other_menus.create');
    Route::any('other_menus/store','AdminOther_menuController@store')->name('admin.other_menus.store');
    Route::get('other_menus/show/{id}','AdminOther_menuController@show')->name('admin.other_menus.show');
    Route::get('other_menus/edit/{id}','AdminOther_menuController@edit')->name('admin.other_menus.edit');
    Route::match(['put', 'patch'], 'other_menus/update/{id}','AdminOther_menuController@update')->name('admin.other_menus.update');
    Route::get('other_menus/delete/{id}', 'AdminOther_menuController@destroy')->name('admin.other_menus.edit');
    Route::get('other_menus/change/{text}/{status}/{id}', 'AdminOther_menuController@change');
});




Route::group(array('module'=>'other_menu','namespace' => 'App\modules\other_menu\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('other_menus/','Other_menuController@index')->name('other_menus');
    
});