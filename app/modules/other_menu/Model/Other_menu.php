<?php

namespace App\Modules\Other_menu\Model;


use Illuminate\Database\Eloquent\Model;

class Other_menu extends Model
{
    public  $table = 'tbl_other_menu';

    protected $fillable = ['id','title','link','approvel_status','approved','status','created_date','created_by','del_flag','type','is_download',];
}
