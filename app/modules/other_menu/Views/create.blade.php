@extends('admin.layout.main')
@section('content')

<section class="content-header">
    <h1>
        Add Other Menus
        <small></small>                    
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.other_menus') }}">Other Menu</a></li>
        <li class="active">Add</li>
    </ol>
</section>
<section class="content">
<div class="box box-primary">
            <button class="btn btn-primary" onclick="toogleoption('select')">Select Option</button>
            <button class="btn btn-primary" onclick="toogleoption('type')">Type Option</button>
                 <!-- form start -->
                <form role="form" action="{{ route('admin.other_menus.store') }}" method="post">
                    <div class="box-body" id="selected" >
                        <div class="form-group" style="display:block;">
		              <label>Title</label>
                        <select name="title" class="form-control">
                        <option value="">Select Page</option>
                            <?php foreach ($page_datas as $page_data) { ?>
                               <option value="<?php echo $page_data['page_id'] ?>"><?php echo $page_data['page_title'] ?></option>
                            <?php } ?>
                        </select>
                   <!--  -->
                   </div>
                   </div>
                    <div class="box-body" id="typed" style="display:none;">
                        <div class="form-group">
                      <label>Title</label><input name="title_t" class="form-control" value="{{old('title_t')}}"/></div>

                      <div class="form-group">
                      <label>Link</label><input name="link_t"  class="form-control" value="{{old('link_t')}}"/></div>
                        <div class="form-group">
                      <label>Is_Download</label><div class="radio"><label><input type="radio" value="1" name="is_download" 
                      @if (old('is_download') == '1') checked="checked" @endif /> Yes </label> 
                      <label><input type="radio" value="0" name="is_download"  @if (old('is_download') == '1') @else checked="checked" @endif  /> No</label></div>
                        <p><strong>Note:</strong> Only filename is required in link field for downloadable links</p>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-body"  >
                    <div class="form-group">
                        {!! $errors->first('title', "<li style='color:#FF0000'> :message </li>") !!}
                        {!! $errors->first('title_t', "<li style='color:#FF0000'> :message </li>") !!}
                        {!! $errors->first('link_t', "<li style='color:#FF0000'> :message </li>") !!}

                    </div>

                    <div class="form-group">
                      <label>Status</label><div class="radio"><label><input type="radio" value="1" name="status" @if (old('status') == '1') checked="checked" @endif />Yes</label> 
                        <label><input type="radio" value="0" name="status" @if(old('status') == '1') @else  checked="checked" @endif /> No</label></div></div>
                        </div>
                        <input type="hidden" id="typ" value="select" name="type"/>

                    <div class="box-footer">
                    {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a href="{{ route('admin.other_menus') }}" class="btn btn-danger">Cancel</a>
                    </div>
                </form>
                            </div>    


</section> 
<script type="text/javascript">
    function toogleoption(option)
    {
        if(option == 'select')
        {
            $('#selected').show();
            $('#typ').val('select');

            $('#typed').hide();

        }
        else
        {
               $('#selected').hide();
            $('#typ').val('type');

            $('#typed').show();
        }
    }
</script>

@endsection