@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Other_menus   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.other_menus') }}">tbl_other_menu</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.other_menus.update') }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                                    <label for="title">Title</label><input type="text" value = "{{$other_menu->title}}"  name="title" id="title" class="form-control" ></div><div class="form-group">
                                    <label for="link">Link</label><input type="text" value = "{{$other_menu->link}}"  name="link" id="link" class="form-control" ></div><div class="form-group">
                                    <label for="approvel_status">Approvel_status</label><input type="text" value = "{{$other_menu->approvel_status}}"  name="approvel_status" id="approvel_status" class="form-control" ></div><div class="form-group">
                                    <label for="approved">Approved</label><input type="text" value = "{{$other_menu->approved}}"  name="approved" id="approved" class="form-control" ></div><div class="form-group">
                                    <label for="status">Status</label><input type="text" value = "{{$other_menu->status}}"  name="status" id="status" class="form-control" ></div><div class="form-group">
                                    <label for="created_date">Created_date</label><input type="text" value = "{{$other_menu->created_date}}"  name="created_date" id="created_date" class="form-control" ></div><div class="form-group">
                                    <label for="created_by">Created_by</label><input type="text" value = "{{$other_menu->created_by}}"  name="created_by" id="created_by" class="form-control" ></div><div class="form-group">
                                    <label for="del_flag">Del_flag</label><input type="text" value = "{{$other_menu->del_flag}}"  name="del_flag" id="del_flag" class="form-control" ></div><div class="form-group">
                                    <label for="type">Type</label><input type="text" value = "{{$other_menu->type}}"  name="type" id="type" class="form-control" ></div><div class="form-group">
                                    <label for="is_download">Is_download</label><input type="text" value = "{{$other_menu->is_download}}"  name="is_download" id="is_download" class="form-control" ></div>
<input type="hidden" name="id" id="id" value = "{{$other_menu->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.other_menus') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
