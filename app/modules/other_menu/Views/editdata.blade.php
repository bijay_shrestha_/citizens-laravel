@extends('admin.layout.main')
@section('content')

<section class="content-header">
    <h1>
        Edit Other Menus
        <small></small>                    
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.other_menus') }}">Other Menu</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<section class="content">
<div class="box box-primary">
        @if($other_menu->type == "select")
            <button class="btn btn-primary" onclick="toogleoption('select')">Select Option</button>
        @elseif($other_menu->type == "type")
            <button class="btn btn-primary" onclick="toogleoption('type')">Type Option</button> 
        @endif
                 <!-- form start -->
                <form role="form" action="{{ route('admin.other_menus.update', $other_menu->id) }}" method="post">
                {{method_field('PATCH')}}            
                    @if ($other_menu->type == "select")
                    <div class="box-body" id="selected" >
                        <div class="form-group" style="display:block;">
		              <label>Title</label>
                        <select name="title" class="form-control">
                        <option value="">Select Page</option>
                            @foreach ($page_datas as $page_data) 
                               <option value="{{$page_data['page_id']}}" @if($page_data['slug_name'] == $other_menu->link) selected= "selected"
                                @endif >{{$page_data['page_title']}}</option>
                            @endforeach
                        </select>
                   <!--  -->
                   </div>
                   </div>
                   @endif
                   @if ($other_menu->type == "type")
                    <div class="box-body" id="typed" >
                        <div class="form-group">
                      <label>Title</label><input name="title_t" class="form-control" value="{{$other_menu->title}}"/></div>

                      <div class="form-group">
                      <label>Link</label><input name="link_t"  class="form-control" value="{{$other_menu->link}}"/></div>
                        <div class="form-group">
                      <label>Is_Download</label><div class="radio"><label><input type="radio" value="1" name="is_download" 
                      @if ($other_menu->is_download == '1') checked="checked" @endif /> Yes </label> 
                      <label><input type="radio" value="0" name="is_download"  @if ($other_menu->is_download == '1') @else checked="checked" @endif  /> No</label></div>
                        <p><strong>Note:</strong> Only filename is required in link field for downloadable links</p>
                        </div>
                    </div><!-- /.box-body -->
                    @endif
                    <div class="box-body"  >
                    <div class="form-group">
                        @if ($other_menu->type == "select")
                        {!! $errors->first('title', "<li style='color:#FF0000'> :message </li>") !!}
                        @else ($other_menu->type == "type")
                        {!! $errors->first('title_t', "<li style='color:#FF0000'> :message </li>") !!}
                        {!! $errors->first('link_t', "<li style='color:#FF0000'> :message </li>") !!}
                        @endif
                    </div>

                    <div class="form-group">
                      <label>Status</label><div class="radio"><label><input type="radio" value="1" name="status" @if ($other_menu->status == '1') checked="checked" @endif />Yes</label> 
                        <label><input type="radio" value="0" name="status" @if($other_menu->status == '1') @else  checked="checked" @endif /> No</label></div></div>
                        </div>
                        <input type="hidden" id="typ" value="{{$other_menu->type}}" name="type"/>

                    <div class="box-footer">
                    {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a href="{{ route('admin.other_menus') }}" class="btn btn-danger">Cancel</a>
                    </div>
                </form>
                            </div>    


</section> 
<script type="text/javascript">
    function toogleoption(option)
    {
        if(option == 'select')
        {
            $('#selected').show();
            $('#typ').val('select');

            $('#typed').hide();

        }
        else
        {
               $('#selected').hide();
            $('#typ').val('type');

            $('#typed').show();
        }
    }
</script>

@endsection