<?php

namespace App\modules\other_menu\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\other_menu\Model\Other_menu;
use App\modules\page\Model\Page;
use Carbon\Carbon;

class AdminOther_menuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Other_menu';
        return view("other_menu::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getother_menusJson(Request $request)
    {
        $other_menu = new Other_menu;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $other_menu->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('id', "DSC")->get();

        // Display limited list        
        $rows = $other_menu->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->orderBy('id', "DSC")->get();

        //To count the total values present
        $total = $other_menu->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Other_menu | Create';
        $page_datas = Page::where('del_flag', 0)->orderBy('page_id', 'DSC')->get()->toArray();
        return view("other_menu::create",compact('page', 'page_datas'));
        //
    }

    protected function validate_and_modify($request, $datafor_om)
    {
        if ($datafor_om['type'] == "select") {
            $this->validate($request, array(
              'title' => 'required',
            ), ['title.required' => "Please select Title while choosing 'Select Option' Category"]);
            $pagedata = Page::where('page_id', $datafor_om['title'])->firstOrFail();
            $datafor_om['title'] = $pagedata->page_title;
            $datafor_om['link'] = $pagedata->slug_name;
        } elseif ($datafor_om['type'] == "type") {
            $this->validate($request, array(
              'title_t' => 'required',
              'link_t' => 'required',
            ), [
            'title_t.required' => "Please type on the title field while choosing 'Type Option' Category",
            'link_t.required' => "Please type on the link field while choosing 'Type Option' Category",
            ]);
            $datafor_om['title'] = $datafor_om['title_t'];
            $datafor_om['link'] = $datafor_om['link_t'];
        }
        unset($datafor_om['title_t']);
        unset($datafor_om['link_t']);
        return $datafor_om;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $datafor_om = $this->validate_and_modify($request, $data);
        //unsetting non-needed fields
        $datafor_om['created_date'] = new Carbon();
        $datafor_om['created_by'] = Auth::user()->id;
        $datafor_om['del_flag'] = 0;
        $datafor_om = check_case($datafor_om, 'create');        
        $success = Other_menu::Insert($datafor_om);
        $other_menus = Other_menu::all();
        $other_menu_id = $other_menus[count($other_menus)-1]['id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "OTHER_MENU", 'file_id' => $other_menu_id]); //create acrivity log     
        return redirect()->route('admin.other_menus')->with('success', "Data inserted successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $other_menu = Other_menu::findOrFail($id);
        $page['title'] = 'Other_menu | Update';
        $page_datas = Page::where('del_flag', 0)->orderBy('page_id', 'DSC')->get()->toArray();
        return view("other_menu::editdata",compact('page','other_menu', 'page_datas'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $datafor_om = $this->validate_and_modify($request, $data);
//        $success = Other_menu::where('id', $id)->update($data);
        $datafor_om = check_case($datafor_om, 'edit');        
        DB::table('tbl_other_menu')->where('id', $id)->update($datafor_om);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "OTHER_MENU", 'file_id' => $id]); //create acrivity log     
        return redirect()->route('admin.other_menus')->with('success', "Data updated successfully.");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$data['del_flag'] = 0;
        //$success = Other_menu::where('id', $id)->update($data);
        $data = soft_delete('tbl_other_menu', 'id', $id);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "OTHER_MENU", 'file_id' => $id]); //create acrivity log     
        return redirect()->route('admin.other_menus')->with('success', "Data deleted successfully.");

        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_other_menu', 'id');
        return redirect()->route('admin.other_menus')->with('success', "Data updated successfully.");
    }

}
