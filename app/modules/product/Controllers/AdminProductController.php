<?php

namespace App\modules\product\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\product\Model\Product;
use App\modules\account\Model\Account;
use App\modules\loan\Model\Loan;
use App\modules\credit_card\Model\Credit_card;
use App\modules\banking\Model\Banking;
use App\modules\service\Model\Service;
use Carbon\Carbon;

class AdminProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Product';
        return view("product::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getproductsJson(Request $request)
    {
        $product = new Product;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $product->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('p_id', "DSC")->get();

        // Display limited list        
        $rows = $product->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->orderBy('p_id', "DSC")->get();

        //To count the total values present
        $total = $product->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Product | Create';
        $accounts = Account::all();
        $loans = Loan::all();
        $card_name = Credit_card::all();
        $bankings = Banking::all();
        $services = Service::all();
        return view("product::add",compact('page', 'accounts', 'loans', 'card_name', 'bankings', 'services'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $datafor_product = $data;
        unset($datafor_product['upload_image']);
        unset($datafor_product['upload_logo']);
        unset($datafor_product['upload_background']);

        $datafor_product['del_flag'] = 0;
        $datafor_product['image'] = $datafor_product['image_name'];
        unset($datafor_product['image_name']);
        $datafor_product = check_case($datafor_product, 'create');        

        $success = Product::Insert($datafor_product);

        $products = Product::all();
        $product_id = $products[count($products)-1]['p_id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "product", 'file_id' => $product_id]); 
        
        return redirect()->route('admin.products')->with('success', "Data added successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::where('p_id',$id)->firstOrFail();
        $page['title'] = 'Product | Update';
        $accounts = Account::all();
        $loans = Loan::all();
        $card_name = Credit_card::all();
        $bankings = Banking::all();
        $services = Service::all();
        return view("product::edit",compact('page','product', 'accounts', 'loans', 'card_name', 'bankings', 'services'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $product = Product::where('p_id', $id)->firstOrFail();
        $datafor_product = $data;
        unset($datafor_product['upload_image']);
        unset($datafor_product['upload_logo']);
        unset($datafor_product['upload_background']);

        //for image case
        if (array_key_exists('delete_old_image', $data)) {
            if ((file_exists(public_path().'/uploads/product/'. $product->image))  && $product->image) {
                unlink(public_path().'/uploads/product/'. $product->image);
                if (file_exists(public_path().'/uploads/product/thumb/'. $product->image)) {
                    unlink(public_path().'/uploads/product/thumb/'. $product->image);
                } 
            }
            if (!$data['image_name']) {
                $data['image_name'] = '';
                $datafor_product['image_name'] = '';
            }
            unset($datafor_product['delete_old_image']);
        } else {
            if (!$data['image_name']) {
                $data['image_name'] = $product->image;
                $datafor_product['image_name'] = $product->image;
            }
        }
        $datafor_product['image'] = $datafor_product['image_name'];
        unset($datafor_product['image_name']);

        //for logo case
        if (array_key_exists('delete_old_logo', $data)) {

            if ((file_exists(public_path().'/uploads/product/'. $product->logo))  && $product->logo) {
                unlink(public_path().'/uploads/product/'. $product->logo);
                if (file_exists(public_path().'/uploads/product/thumb/'. $product->logo)) {
                    unlink(public_path().'/uploads/product/thumb/'. $product->logo);
                } 
            }
            if (!$data['logo']) {
                $data['logo'] = '';
                $datafor_product['logo'] = '';
            }
            unset($datafor_product['delete_old_logo']);
        } else {
            if (!$data['logo']) {
                $data['logo'] = $product->logo;
                $datafor_product['logo'] = $product->logo;
            }
        }

        //for background case
        if (array_key_exists('delete_old_background', $data)) {
            if ((file_exists(public_path().'/uploads/product/'. $product->background))  && $product->background) {
                unlink(public_path().'/uploads/product/'. $product->background);
                if (file_exists(public_path().'/uploads/product/thumb/'. $product->background)) {
                    unlink(public_path().'/uploads/product/thumb/'. $product->background);
                } 
            }
            if (!$data['background']) {
                $data['background'] = '';
                $datafor_product['background'] = '';
            }
            unset($datafor_product['delete_old_background']);
        } else {
            if (!$data['background']) {
                $data['background'] = $product->background;
                $datafor_product['background'] = $product->background;
            }
        }
        $datafor_product = check_case($datafor_product, 'edit');        
        $success = DB::table('tbl_product')->where('p_id', $id)->update($datafor_product);

        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "product", 'file_id' => $id]);      
        return redirect()->route('admin.products')->with('success', "Data updated successfully.");//
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_product', 'p_id', $id);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "product", 'file_id' => $id]); //create acrivity log             
        return redirect()->route('admin.products')->with('success', "Data deleted successfully.");

        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_product', 'p_id');
        return redirect()->route('admin.products')->with('success', "Data updated successfully.");
    }

}
