<?php



Route::group(array('prefix'=>'admin/','module'=>'product','middleware' => ['web','auth'], 'namespace' => 'App\modules\product\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('products/','AdminProductController@index')->name('admin.products');
    Route::post('products/getproductsJson','AdminProductController@getproductsJson')->name('admin.products.getdatajson');
    Route::get('products/create','AdminProductController@create')->name('admin.products.create');
    Route::post('products/store','AdminProductController@store')->name('admin.products.store');
    Route::get('products/show/{id}','AdminProductController@show')->name('admin.products.show');
    Route::get('products/edit/{id}','AdminProductController@edit')->name('admin.products.edit');
    Route::match(['put', 'patch'], 'products/update/{id}','AdminProductController@update')->name('admin.products.update');
    Route::get('products/delete/{id}', 'AdminProductController@destroy')->name('admin.products.edit');
    Route::get('products/change/{text}/{status}/{id}', 'AdminProductController@change');

});




Route::group(array('module'=>'product','namespace' => 'App\modules\product\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('products/','ProductController@index')->name('products');
    
});