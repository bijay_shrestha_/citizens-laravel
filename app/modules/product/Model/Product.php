<?php

namespace App\Modules\Product\Model;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public  $table = 'tbl_product';

    protected $fillable = ['p_id','image','link','logo','background','short_text','name','type','approvel_status','approved','status','del_flag',];
}
