<?php



Route::group(array('prefix'=>'admin/','module'=>'Password_reset','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Password_reset\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('password_resets/','AdminPassword_resetController@index')->name('admin.password_resets');
    Route::post('password_resets/getpassword_resetsJson','AdminPassword_resetController@getpassword_resetsJson')->name('admin.password_resets.getdatajson');
    Route::get('password_resets/create','AdminPassword_resetController@create')->name('admin.password_resets.create');
    Route::post('password_resets/store','AdminPassword_resetController@store')->name('admin.password_resets.store');
    Route::get('password_resets/show/{id}','AdminPassword_resetController@show')->name('admin.password_resets.show');
    Route::get('password_resets/edit/{id}','AdminPassword_resetController@edit')->name('admin.password_resets.edit');
    Route::match(['put', 'patch'], 'password_resets/update/{id}','AdminPassword_resetController@update')->name('admin.password_resets.edit');
    Route::get('password_resets/delete/{id}', 'AdminPassword_resetController@destroy')->name('admin.password_resets.edit');
});




Route::group(array('module'=>'Password_reset','namespace' => 'App\Modules\Password_reset\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('password_resets/','Password_resetController@index')->name('password_resets');
    
});