@extends('admin.layout.main')
@section('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Add</h3>
            </div>
        </div>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.password_resets') }}">password_resets</a></li>
            <li class="active">Add</li>
        </ol>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                    <!-- form start -->
                        <form role="form" action="{{ route('admin.password_resets.store') }}"  method="post" class="form-horizontal form-label-left">
                            <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="email">Email</label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line"><input type="text" name="email" id="email" class="form-control" ></div>
                        </div>
                    </div>
                </div><!-- close row clearfix -->
                <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="token">Token</label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line"><input type="text" name="token" id="token" class="form-control" ></div>
                        </div>
                    </div>
                </div><!-- close row clearfix -->
                <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="created_at">Created_at</label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line"><input type="text" name="created_at" id="created_at" class="form-control" ></div>
                        </div>
                    </div>
                </div><!-- close row clearfix -->
                
<input type="hidden" name="id" id="id"/>
                            {{ csrf_field() }}
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="{{ route('admin.password_resets') }}" class="btn btn-danger">Cancel</a>
                            </div>
                        </form>
                    </div>   
                </div> 
            </div>
        </div>
    </div>
@endsection
