@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Depositers		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Depositers</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.depositers.create') }}" class="btn bg-green waves-effect"  title="create">Create</a>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="depositer-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								<th >D_id</th>
<th >A_id</th>
<th >Mr_miss</th>
<th >Family</th>
<th >Dob</th>
<th >Age</th>
<th >Relationship</th>
<th >Permanent_address</th>
<th >Contact_address</th>
<th >Tel_no</th>
<th >Authorized_signature</th>

								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#depositer-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.depositers.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
					{ data: "d_id",name: "d_id"},
            { data: "a_id",name: "a_id"},
            { data: "mr_miss",name: "mr_miss"},
            { data: "family",name: "family"},
            { data: "dob",name: "dob"},
            { data: "age",name: "age"},
            { data: "relationship",name: "relationship"},
            { data: "permanent_address",name: "permanent_address"},
            { data: "contact_address",name: "contact_address"},
            { data: "tel_no",name: "tel_no"},
            { data: "authorized_signature",name: "authorized_signature"},
            
					{ data: function(data,b,c,table) { 
					var buttons = '';

					buttons += "<a class='btn bg-red waves-effect' href='"+site_url+"/edit/"+data.id+"' type='button' >Edit</a>&nbsp"; 

					buttons += "<a href='"+site_url+"/delete/"+data.id+"' class='btn bg-red waves-effect' >Delete</a>";

					return buttons;
					}, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
