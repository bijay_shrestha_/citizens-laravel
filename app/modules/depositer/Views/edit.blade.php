@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Depositers   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.depositers') }}">tbl_depositer</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.depositers.update') }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                                    <label for="d_id">D_id</label><input type="text" value = "{{$depositer->d_id}}"  name="d_id" id="d_id" class="form-control" ></div><div class="form-group">
                                    <label for="a_id">A_id</label><input type="text" value = "{{$depositer->a_id}}"  name="a_id" id="a_id" class="form-control" ></div><div class="form-group">
                                    <label for="mr_miss">Mr_miss</label><input type="text" value = "{{$depositer->mr_miss}}"  name="mr_miss" id="mr_miss" class="form-control" ></div><div class="form-group">
                                    <label for="family">Family</label><input type="text" value = "{{$depositer->family}}"  name="family" id="family" class="form-control" ></div><div class="form-group">
                                    <label for="dob">Dob</label><input type="text" value = "{{$depositer->dob}}"  name="dob" id="dob" class="form-control" ></div><div class="form-group">
                                    <label for="age">Age</label><input type="text" value = "{{$depositer->age}}"  name="age" id="age" class="form-control" ></div><div class="form-group">
                                    <label for="relationship">Relationship</label><input type="text" value = "{{$depositer->relationship}}"  name="relationship" id="relationship" class="form-control" ></div><div class="form-group">
                                    <label for="permanent_address">Permanent_address</label><input type="text" value = "{{$depositer->permanent_address}}"  name="permanent_address" id="permanent_address" class="form-control" ></div><div class="form-group">
                                    <label for="contact_address">Contact_address</label><input type="text" value = "{{$depositer->contact_address}}"  name="contact_address" id="contact_address" class="form-control" ></div><div class="form-group">
                                    <label for="tel_no">Tel_no</label><input type="text" value = "{{$depositer->tel_no}}"  name="tel_no" id="tel_no" class="form-control" ></div><div class="form-group">
                                    <label for="authorized_signature">Authorized_signature</label><input type="text" value = "{{$depositer->authorized_signature}}"  name="authorized_signature" id="authorized_signature" class="form-control" ></div>
<input type="hidden" name="id" id="id" value = "{{$depositer->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.depositers') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
