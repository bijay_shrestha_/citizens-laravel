<?php



Route::group(array('prefix'=>'admin/','module'=>'depositer','middleware' => ['web','auth'], 'namespace' => 'App\modules\depositer\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('depositers/','AdminDepositerController@index')->name('admin.depositers');
    Route::post('depositers/getdepositersJson','AdminDepositerController@getdepositersJson')->name('admin.depositers.getdatajson');
    Route::get('depositers/create','AdminDepositerController@create')->name('admin.depositers.create');
    Route::post('depositers/store','AdminDepositerController@store')->name('admin.depositers.store');
    Route::get('depositers/show/{id}','AdminDepositerController@show')->name('admin.depositers.show');
    Route::get('depositers/edit/{id}','AdminDepositerController@edit')->name('admin.depositers.edit');
    Route::match(['put', 'patch'], 'depositers/update/{id}','AdminDepositerController@update')->name('admin.depositers.update');
    Route::get('depositers/delete/{id}', 'AdminDepositerController@destroy')->name('admin.depositers.edit');
});




Route::group(array('module'=>'depositer','namespace' => 'App\modules\depositer\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('depositers/','DepositerController@index')->name('depositers');
    
});