<?php

namespace App\Modules\Depositer\Model;


use Illuminate\Database\Eloquent\Model;

class Depositer extends Model
{
    public  $table = 'tbl_depositer';

    protected $fillable = ['d_id','a_id','mr_miss','family','dob','age','relationship','permanent_address','contact_address','tel_no','authorized_signature',];
}
