<?php



Route::group(array('prefix'=>'admin/','module'=>'expected_transaction','middleware' => ['web','auth'], 'namespace' => 'App\modules\expected_transaction\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('expected_transactions/','AdminExpected_transactionController@index')->name('admin.expected_transactions');
    Route::post('expected_transactions/getexpected_transactionsJson','AdminExpected_transactionController@getexpected_transactionsJson')->name('admin.expected_transactions.getdatajson');
    Route::get('expected_transactions/create','AdminExpected_transactionController@create')->name('admin.expected_transactions.create');
    Route::post('expected_transactions/store','AdminExpected_transactionController@store')->name('admin.expected_transactions.store');
    Route::get('expected_transactions/show/{id}','AdminExpected_transactionController@show')->name('admin.expected_transactions.show');
    Route::get('expected_transactions/edit/{id}','AdminExpected_transactionController@edit')->name('admin.expected_transactions.edit');
    Route::match(['put', 'patch'], 'expected_transactions/update/{id}','AdminExpected_transactionController@update')->name('admin.expected_transactions.update');
    Route::get('expected_transactions/delete/{id}', 'AdminExpected_transactionController@destroy')->name('admin.expected_transactions.edit');
});




Route::group(array('module'=>'expected_transaction','namespace' => 'App\modules\expected_transaction\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('expected_transactions/','Expected_transactionController@index')->name('expected_transactions');
    
});