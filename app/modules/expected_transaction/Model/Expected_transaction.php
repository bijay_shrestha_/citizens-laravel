<?php

namespace App\Modules\Expected_transaction\Model;


use Illuminate\Database\Eloquent\Model;

class Expected_transaction extends Model
{
    public  $table = 'tbl_expected_transaction';

    protected $fillable = ['id','expected_transaction','show_or_hide','del_flag',];
}
