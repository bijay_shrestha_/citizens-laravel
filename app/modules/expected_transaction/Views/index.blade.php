@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Expected Transactions		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Expected Transactions</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.expected_transactions.create') }}" class="btn bg-green waves-effect"  title="create">Create</a>
					</div>
					<!-- /.box-header -->
					<br>
					@if ($message = Session::get('success'))
					<div class="btn btn-success">
						{{$message}}
					</div>
					@endif

					<div class="box-body">
						<table id="expected_transaction-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								<th >Expected Transaction</th>
								<th >Show Or Hide</th>
								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#expected_transaction-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.expected_transactions.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
		  { data: "expected_transaction",name: "expected_transaction"},
            { data: function(data){
            	if (data.show_or_hide == 1) {
            		return "Show";
            	} else {
            		return "Hide";
            	}
            	;
            }, name:'show_or_hide', searchable: false},

            
            	{ data: function(data,b,c,table) { 
            var buttons = '';

            buttons += "<a class='btn bg-green waves-effect' href='"+site_url+"/edit/"+data.id+"' type='button' ><i class='fa fa-edit'></i> Edit</a>&nbsp"; 

            buttons += "<a href='"+site_url+"/delete/"+data.id+"' class='btn bg-red waves-effect' ><i class='fa fa-trash'></i>  Delete</a>";

            return buttons;
            }, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
