@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Expected Transactions   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.expected_transactions') }}">Expected Transaction</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.expected_transactions.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                        <label for="expected_transaction">Expected Transaction</label><input type="text" name="expected_transaction" id="expected_transaction" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="show_or_hide">Show Or Hide</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="show_or_hide" id="show_or_hide1" @if(old('show_or_hide') == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="show_or_hide" id="show_or_hide0" @if(old('show_or_hide') == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>


                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.expected_transactions') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
