<?php



Route::group(array('prefix'=>'admin/','module'=>'fees_commision','middleware' => ['web','auth'], 'namespace' => 'App\modules\fees_commision\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('fees_commisions/','AdminFees_commisionController@index')->name('admin.fees_commisions');
    Route::post('fees_commisions/getfees_commisionsJson','AdminFees_commisionController@getfees_commisionsJson')->name('admin.fees_commisions.getdatajson');
    Route::get('fees_commisions/create','AdminFees_commisionController@create')->name('admin.fees_commisions.create');
    Route::post('fees_commisions/store','AdminFees_commisionController@store')->name('admin.fees_commisions.store');
    Route::get('fees_commisions/show/{id}','AdminFees_commisionController@show')->name('admin.fees_commisions.show');
    Route::get('fees_commisions/edit/{id}','AdminFees_commisionController@edit')->name('admin.fees_commisions.edit');
    Route::match(['put', 'patch'], 'fees_commisions/update/{id}','AdminFees_commisionController@update')->name('admin.fees_commisions.update');
    Route::get('fees_commisions/delete/{id}', 'AdminFees_commisionController@destroy')->name('admin.fees_commisions.edit');
    Route::get('fees_commisions/change/{text}/{status}/{id}', 'AdminFees_commisionController@change');

});




Route::group(array('module'=>'fees_commision','namespace' => 'App\modules\fees_commision\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('fees_commisions/','Fees_commisionController@index')->name('fees_commisions');
    
});