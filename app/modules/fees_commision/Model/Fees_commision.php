<?php

namespace App\Modules\Fees_commision\Model;


use Illuminate\Database\Eloquent\Model;

class Fees_commision extends Model
{
    public  $table = 'tbl_fees_commisions';

    protected $fillable = ['id','name','table','approvel_status','approved','status','del_flag','added_date','added_by','modified_date','modified_by',];
}
