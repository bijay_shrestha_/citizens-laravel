<?php

namespace App\modules\fees_commision\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\fees_commision\Model\Fees_commision;
use Carbon\Carbon;

class AdminFees_commisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Fees_commision';
        return view("fees_commision::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getfees_commisionsJson(Request $request)
    {
        $fees_commision = new Fees_commision;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $fees_commision->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('id', "DSC")->get();

        // Display limited list        
        $rows = $fees_commision->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->orderBy('id', "DSC")->where('del_flag',0)->get();

        //To count the total values present
        $total = $fees_commision->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Fees_commision | Create';
        return view("fees_commision::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $datafor_fee_commision = $data;
        $datafor_fee_commision['added_by'] = Auth::user()->id;
        $datafor_fee_commision['added_date'] = new Carbon();
        $datafor_fee_commision['del_flag'] = 0;
        $datafor_fee_commision = check_case($datafor_fee_commision, 'create');
        $success = Fees_commision::Insert($datafor_fee_commision);

        $fee_commisions = Fees_commision::all();
        $fee_commision_id = $fee_commisions[count($fee_commisions) - 1]['id'];

        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "FEES_COMMISIONS", 'file_id' => $fee_commision_id]); //create acrivity log     


        return redirect()->route('admin.fees_commisions');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fees_commision = Fees_commision::where('id',$id)->firstOrFail();
        $page['title'] = 'Fees_commision | Update';
        return view("fees_commision::edit",compact('page','fees_commision'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $datafor_fee_commision = $data;
        $datafor_fee_commision['modified_date'] = new Carbon();
        $datafor_fee_commision['modified_by'] = Auth::user()->id;
        $datafor_fee_commision = check_case($datafor_fee_commision, 'edit');
        $success = DB::table('tbl_fees_commisions')->where('id', $id)->update($datafor_fee_commision);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "FEES_COMMISIONS", 'file_id' => $id]); //create acrivity log     
        return redirect()->route('admin.fees_commisions')->with('success', "Data updated successfully.");        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_fees_commisions', 'id', $id);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "FEES_COMMISIONS", 'file_id' => $id]); //create acrivity log     
        return redirect()->route('admin.fees_commisions')->with('success', "Data deleted for trash.");
        //
    }


    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_fees_commisions', 'id');
        return redirect()->route('admin.fees_commisions')->with('success', "Data updated ");
    }

}
