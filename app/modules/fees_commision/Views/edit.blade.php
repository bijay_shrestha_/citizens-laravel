@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Fees_commisions   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.fees_commisions') }}">tbl_fees_commisions</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.fees_commisions.update', $fees_commision->id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                    <div class="form-group">
                        <label for="name">Name</label><input type="text" value = "{{$fees_commision->name}}"  name="name" id="name" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="table">Table</label><textarea  name="table" id="table" class="form-control my-editor" >{!! $fees_commision->table !!} </textarea>
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="status" id="status1" @if($fees_commision->status == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="status" id="status0" @if($fees_commision->status == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.fees_commisions') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
