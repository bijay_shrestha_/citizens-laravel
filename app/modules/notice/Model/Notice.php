<?php

namespace App\Modules\Notice\Model;


use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    public  $table = 'tbl_notice';

    protected $fillable = ['n_id','image','approvel_status','approved','status','del_flag',];
}
