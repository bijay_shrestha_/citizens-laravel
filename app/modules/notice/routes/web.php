<?php



Route::group(array('prefix'=>'admin/','module'=>'notice','middleware' => ['web','auth'], 'namespace' => 'App\modules\notice\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('notices/','AdminNoticeController@index')->name('admin.notices');
    Route::post('notices/getnoticesJson','AdminNoticeController@getnoticesJson')->name('admin.notices.getdatajson');
    Route::get('notices/create','AdminNoticeController@create')->name('admin.notices.create');
    Route::post('notices/store','AdminNoticeController@store')->name('admin.notices.store');
    Route::get('notices/show/{id}','AdminNoticeController@show')->name('admin.notices.show');
    Route::get('notices/edit/{id}','AdminNoticeController@edit')->name('admin.notices.edit');
    Route::match(['put', 'patch'], 'notices/update/{id}','AdminNoticeController@update')->name('admin.notices.update');
    Route::get('notices/delete/{id}', 'AdminNoticeController@destroy')->name('admin.notices.edit');
    Route::get('notices/change/{text}/{status}/{id}', 'AdminNoticeController@change');

});




Route::group(array('module'=>'notice','namespace' => 'App\modules\notice\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('notices/','NoticeController@index')->name('notices');
    
});