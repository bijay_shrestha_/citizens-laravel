<?php

namespace App\modules\notice\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\notice\Model\Notice;
use Carbon\Carbon;

class AdminNoticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Notice';
        return view("notice::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getnoticesJson(Request $request)
    {
        $notice = new Notice;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $notice->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('n_id', "DSC")->get();

        // Display limited list        
        $rows = $notice->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->orderBy('n_id', "DSC")->where('del_flag',0)->get();

        //To count the total values present
        $total = $notice->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Notice | Create';
        return view("notice::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $data = $request->except('_token');
        $datafor_notice = $data;
        unset($datafor_notice['upload_image']);
        $datafor_notice['del_flag'] = 0;
        $datafor_notice['image'] = $datafor_notice['image_name'];
        unset($datafor_notice['image_name']);
        $datafor_notice = check_case($datafor_notice, 'create');        
        $success = Notice::Insert($datafor_notice);
        $notices = Notice::all();
        $notice_id = $notices[count($notices)-1]['n_id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "NOTICE", 'file_id' => $notice_id]); //create acrivity log     
        return redirect()->route('admin.notices')->with('success', "Data added Successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notice = Notice::where('n_id', $id)->firstOrFail();
        $page['title'] = 'Notice | Update';
        return view("notice::edit",compact('page','notice'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "NOTICE", 'file_id' => $id]); //create activity log
        $notice = Notice::where('n_id', $id)->firstOrFail();
        $datafor_notice = $data;
        unset($datafor_notice['upload_image']);
        
        if (array_key_exists('delete_old_image', $data)) {
            if ((file_exists(public_path().'/uploads/notice/'. $notice->image))  && $notice->image) {
                unlink(public_path().'/uploads/notice/'. $notice->image);
                if (file_exists(public_path().'/uploads/notice/thumb/'. $notice->image)) {
                    unlink(public_path().'/uploads/notice/thumb/'. $notice->image);
                } 
            }
            if (!$data['image_name']) {
                $data['image_name'] = '';
                $datafor_notice['image_name'] = '';
            }
            unset($datafor_notice['delete_old_image']);
        } else {
            if (!$data['image_name']) {
                $data['image_name'] = $notice->image;
                $datafor_notice['image_name'] = $notice->image;
            }
        }
        $datafor_notice['image'] = $datafor_notice['image_name'];
        unset($datafor_notice['image_name']);
        $datafor_notice = check_case($datafor_notice, 'edit');        
        $success = DB::table('tbl_notice')->where('n_id', $id)->update($datafor_notice);
        return redirect()->route('admin.notices')->with('success', "Data updated Successfully.");
        

        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_notice', 'n_id', $id);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "NOTICE", 'file_id' => $id]); //create activity log
        return redirect()->route('admin.notices')->with('success', "Data deleted Successfully.");
        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_notice', 'n_id');
        return redirect()->route('admin.notices')->with('success', "Data updated Successfully.");
    }

}
