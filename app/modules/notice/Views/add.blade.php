@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Notices   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.notices') }}">Notice</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.notices.store') }}"  method="post">
                <div class="box-body">                
                   
                    <!-- in this (notice) case image field in db table is 'image' not 'image_name'  -->
                    <div class="form-group">
                        <label for="image_name">Image </label><br>
                        <label id="upload_image_name" style="display:none"></label>
                        <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                        <div id="image-status"></div>
                        <input type="file" id="upload_image" name="upload_image" style="display:block"/>
                        <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="status" id="status1" @if(old('status') == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="status" id="status0" @if(old('status') == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>

                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" id="btn-submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.notices') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection

@section('custom_scripts')
    <script type="text/javascript">
          var directory = 'notice';
         $('#btn-submit').on('click',function(){
           var filename = $('#image_name').val();
           if (!filename || filename == "") {
            alert('Please Upload Image');
            return false;
           }
          });
    </script>

    @include('data_changing_script_create')
    @include('file_changing_script')

@endsection