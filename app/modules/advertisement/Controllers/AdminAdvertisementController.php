<?php

namespace App\modules\advertisement\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\advertisement\Model\Advertisement;
use Carbon\Carbon;

class AdminAdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Advertisement';
        return view("advertisement::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getadvertisementsJson(Request $request)
    {
        $advertisement = new Advertisement;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $advertisement->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->orderBy('id', "DSC")->where('del_flag',0)->get();

        // Display limited list        
        $rows = $advertisement->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->orderBy('id', "DSC")->where('del_flag',0)->get();

        //To count the total values present
        $total = $advertisement->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Advertisement | Create';
        return view("advertisement::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $datafor_ad = $data;
        unset($datafor_ad['upload_image']);
        unset($datafor_ad['upload_logo']);
        unset($datafor_ad['upload_background']);

        $datafor_ad['created_date'] = new Carbon();
        $datafor_ad['created_by'] = Auth::user()->id;
        $datafor_ad['del_flag'] = 0;
        $datafor_ad['image'] = $datafor_ad['image_name'];
        unset($datafor_ad['image_name']);
        $datafor_ad = check_case($datafor_ad, 'create');
        $success = Advertisement::Insert($datafor_ad);

        $advertisements = Advertisement::all();
        $advertisement_id = $advertisements[count($advertisements)-1]['id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "advertisement", 'file_id' => $advertisement_id]); //create acrivity log     
        
        return redirect()->route('admin.advertisements')->with('success', "Data added successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $advertisement = Advertisement::findOrFail($id);
        $page['title'] = 'Advertisement | Update';
        return view("advertisement::edit",compact('page','advertisement'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $advertisement = Advertisement::where('id', $id)->firstOrFail();
        $datafor_ad = $data;
        unset($datafor_ad['upload_image']);
        unset($datafor_ad['upload_logo']);
        unset($datafor_ad['upload_background']);
        
        //for image case
        if (array_key_exists('delete_old_image', $data)) {
            if ((file_exists(public_path().'/uploads/media/'. $advertisement->image))  && $advertisement->image) {
                unlink(public_path().'/uploads/media/'. $advertisement->image);
                if (file_exists(public_path().'/uploads/media/thumb/'. $advertisement->image)) {
                    unlink(public_path().'/uploads/media/thumb/'. $advertisement->image);
                } 
            }
            if (!$data['image_name']) {
                $data['image_name'] = '';
                $datafor_ad['image_name'] = '';
            }
            unset($datafor_ad['delete_old_image']);
        } else {
            if (!$data['image_name']) {
                $data['image_name'] = $advertisement->image;
                $datafor_ad['image_name'] = $advertisement->image;
            }
        }
        $datafor_ad['image'] = $datafor_ad['image_name'];
        unset($datafor_ad['image_name']);

        //for logo case
        if (array_key_exists('delete_old_logo', $data)) {

            if ((file_exists(public_path().'/uploads/media/'. $advertisement->logo))  && $advertisement->logo) {
                unlink(public_path().'/uploads/media/'. $advertisement->logo);
                if (file_exists(public_path().'/uploads/media/thumb/'. $advertisement->logo)) {
                    unlink(public_path().'/uploads/media/thumb/'. $advertisement->logo);
                } 
            }
            if (!$data['logo']) {
                $data['logo'] = '';
                $datafor_ad['logo'] = '';
            }
            unset($datafor_ad['delete_old_logo']);
        } else {
            if (!$data['logo']) {
                $data['logo'] = $advertisement->logo;
                $datafor_ad['logo'] = $advertisement->logo;
            }
        }

        //for background case
        if (array_key_exists('delete_old_background', $data)) {
            if ((file_exists(public_path().'/uploads/media/'. $advertisement->background))  && $advertisement->background) {
                unlink(public_path().'/uploads/media/'. $advertisement->background);
                if (file_exists(public_path().'/uploads/media/thumb/'. $advertisement->background)) {
                    unlink(public_path().'/uploads/media/thumb/'. $advertisement->background);
                } 
            }
            if (!$data['background']) {
                $data['background'] = '';
                $datafor_ad['background'] = '';
            }
            unset($datafor_ad['delete_old_background']);
        } else {
            if (!$data['background']) {
                $data['background'] = $advertisement->background;
                $datafor_ad['background'] = $advertisement->background;
            }
        }

        $datafor_ad['modified_date'] = new Carbon();
        $datafor_ad['modified_by'] = Auth::user()->id;
        $datafor_ad = check_case($datafor_ad, 'edit');
        $success = DB::table('tbl_advertisement')->where('id', $id)->update($datafor_ad);

        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "advertisement", 'file_id' => $id]);      
        return redirect()->route('admin.advertisements')->with('success', "Data updated successfully.");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_advertisement', 'id', $id);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "advertisement", 'file_id' => $id]); //create activity log
        return redirect()->route('admin.advertisements')->with('success', "Data deleted successfully.");

        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_advertisement', 'id');
        return redirect()->route('admin.advertisements')->with('success', "Data updated successfully.");
    }


}
