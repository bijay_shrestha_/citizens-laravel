<?php



Route::group(array('prefix'=>'admin/','module'=>'advertisement','middleware' => ['web','auth'], 'namespace' => 'App\modules\advertisement\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('advertisements/','AdminAdvertisementController@index')->name('admin.advertisements');
    Route::post('advertisements/getadvertisementsJson','AdminAdvertisementController@getadvertisementsJson')->name('admin.advertisements.getdatajson');
    Route::get('advertisements/create','AdminAdvertisementController@create')->name('admin.advertisements.create');
    Route::post('advertisements/store','AdminAdvertisementController@store')->name('admin.advertisements.store');
    Route::get('advertisements/show/{id}','AdminAdvertisementController@show')->name('admin.advertisements.show');
    Route::get('advertisements/edit/{id}','AdminAdvertisementController@edit')->name('admin.advertisements.edit');
    Route::match(['put', 'patch'], 'advertisements/update/{id}','AdminAdvertisementController@update')->name('admin.advertisements.update');
    Route::get('advertisements/delete/{id}', 'AdminAdvertisementController@destroy')->name('admin.advertisements.edit');
    Route::get('advertisements/change/{text}/{status}/{id}', 'AdminAdvertisementController@change');
    Route::get('advertisements/change/{text}/{status}/{id}', 'AdminAdvertisementController@change');

});




Route::group(array('module'=>'advertisement','namespace' => 'App\modules\advertisement\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('advertisements/','AdvertisementController@index')->name('advertisements');
    
});