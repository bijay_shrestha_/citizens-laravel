@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Advertisements   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.advertisements') }}">Advertisement</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.advertisements.update', $advertisement->id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                    <div class="form-group">
                        <label for="title">Title</label><input type="text" value = "{{$advertisement->title}}"  name="title" id="title" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="description">Description</label><textarea name="description" id="description" class="form-control my-editor" >{!! $advertisement->description !!}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="short_text">Short Text</label><textarea name="short_text" id="short_text" class="form-control my-editor" >{!! $advertisement->short_text !!}</textarea>
                    </div>


                    <div class="form-group">
                        <label for="image_name">Image </label><br>
                        <label id="upload_image_name" style="display:none"></label>
                         <div id="delete_old_image_requested"></div>
                        <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                        <div id="image-status"></div>
                        <input type="file" id="upload_image" name="upload_image" @if (file_exists(public_path().'/uploads/media/thumb/'. $advertisement->image) && $advertisement->image) style="display:none" @else style="display:block" @endif />
                        <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                        <div id="imagedata">
                        @if (file_exists(public_path().'/uploads/media/'. $advertisement->image) && $advertisement->image)                                   
                            <img src="{{url('uploads/media/thumb/'.$advertisement->image)}}">
                            <a href="javascript:void(0)" id="change-image_option" title="Change Image">&nbsp; &nbsp; <img src="{{('/images/logo/cancel.png')}}" border="0"/> Change the old image</a>
                        @endif                                
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="logo">Logo </label><br>
                        <label id="upload_logo_name" style="display:none"></label>
                         <div id="delete_old_logo_requested"></div>
                        <input type="text" name="logo" id="logo" class="form-control" style="display:none" />
                        <div id="logo-status"></div>
                        <input type="file" id="upload_logo" name="upload_logo" @if (file_exists(public_path().'/uploads/media/thumb/'. $advertisement->logo) && $advertisement->logo) style="display:none" @else style="display:block" @endif />
                        <a href="javascript:void(0)" id="change-logo" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                        <div id="logodata">
                        @if (file_exists(public_path().'/uploads/media/'. $advertisement->logo) && $advertisement->logo)                                   
                            <img src="{{url('uploads/media/thumb/'.$advertisement->logo)}}">
                            <a href="javascript:void(0)" id="change-logo_option" title="Change logo">&nbsp; &nbsp; <img src="{{('/images/logo/cancel.png')}}" border="0"/> Change the old logo</a>
                        @endif                                
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="logo">Background </label><br>
                        <label id="upload_background_name" style="display:none"></label>
                         <div id="delete_old_background_requested"></div>
                        <input type="text" name="background" id="background" class="form-control" style="display:none" />
                        <div id="background-status"></div>
                        <input type="file" id="upload_background" name="upload_background" @if (file_exists(public_path().'/uploads/media/thumb/'. $advertisement->background) && $advertisement->background) style="display:none" @else style="display:block" @endif />
                        <a href="javascript:void(0)" id="change-background" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                        <div id="backgrounddata">
                        @if (file_exists(public_path().'/uploads/media/'. $advertisement->background) && $advertisement->background)                                   
                            <img src="{{url('uploads/media/thumb/'.$advertisement->background)}}">
                            <a href="javascript:void(0)" id="change-background_option" title="Change background">&nbsp; &nbsp; <img src="{{('/images/logo/cancel.png')}}" border="0"/> Change the old background</a>
                        @endif                                
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="status" id="status1" @if($advertisement->status == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="status" id="status0" @if($advertisement->status == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>                
<input type="hidden" name="id" id="id" value = "{{$advertisement->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.advertisements') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
    <script type="text/javascript">
        var directory = 'media';
    </script>
    @include('data_changing_script_edit')
    @include('file_changing_script')
    @include('multiple_file_changing_script')  {{-- For more than one image or more than one file fields   --}}
@endsection
