@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Advertisements   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.advertisements') }}">Advertisement</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.advertisements.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                        <label for="title">Title</label><input type="text" name="title" id="title" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="description">Description</label><textarea name="description" id="description" class="form-control my-editor" ></textarea>
                    </div>

                    <div class="form-group">
                        <label for="short_text">Short Text</label><textarea name="short_text" id="short_text" class="form-control my-editor" ></textarea>
                    </div>

                    <div class="form-group">
                        <label for="image_name">Image </label><br>
                        <label id="upload_image_name" style="display:none"></label>
                        <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                        <div id="image-status"></div>
                        <input type="file" id="upload_image" name="upload_image" style="display:block"/>
                        <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>

                    </div>

                    <div class="form-group">
                        <label for="logo">Logo </label><br>
                        <label id="upload_logo_name" style="display:none"></label>
                        <input type="text" name="logo" id="logo" class="form-control" style="display:none" />
                        <div id="logo-status"></div>
                        <input type="file" id="upload_logo" name="upload_logo" style="display:block"/>
                        <a href="javascript:void(0)" id="change-logo" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>

                    </div>

                    <div class="form-group">
                        <label for="background">Background</label><br>
                        <label id="upload_background_name" style="display:none"></label>
                        <input type="text" name="background" id="background" class="form-control" style="display:none" />
                        <div id="background-status"></div>
                        <input type="file" id="upload_background" name="upload_background" style="display:block"/>
                        <a href="javascript:void(0)" id="change-background" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>

                    </div>

                    <div class="form-group">
                        <label for="link">Link</label><input type="text" name="link" id="link" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="status" id="status1" @if(old('status') == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="status" id="status0" @if(old('status') == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.advertisements') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
    <script type="text/javascript">
          var directory = 'media';
    </script>
    @include('data_changing_script_create')
    @include('file_changing_script') 
    @include('multiple_file_changing_script')  {{-- For more than one image or more than one file fields   --}}

@endsection
