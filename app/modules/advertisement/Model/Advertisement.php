<?php

namespace App\Modules\Advertisement\Model;


use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    public  $table = 'tbl_advertisement';

    protected $fillable = ['id','title','description','image','logo','background','short_text','link','approvel_status','approved','del_flag','status','created_by','created_date','modified_by','modified_date',];
}
