<?php

namespace App\Modules\Account_parent\Model;


use Illuminate\Database\Eloquent\Model;

class Account_parent extends Model
{
    public  $table = 'tbl_account_parent';

    protected $fillable = ['id','name','status','del_flag'];
}
