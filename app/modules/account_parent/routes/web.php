<?php



Route::group(array('prefix'=>'admin/','module'=>'account_parent','middleware' => ['web','auth'], 'namespace' => 'App\modules\account_parent\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('account_parents/','AdminAccount_parentController@index')->name('admin.account_parents');
    Route::post('account_parents/getaccount_parentsJson','AdminAccount_parentController@getaccount_parentsJson')->name('admin.account_parents.getdatajson');
    Route::get('account_parents/create','AdminAccount_parentController@create')->name('admin.account_parents.create');
    Route::post('account_parents/store','AdminAccount_parentController@store')->name('admin.account_parents.store');
    Route::get('account_parents/show/{id}','AdminAccount_parentController@show')->name('admin.account_parents.show');
    Route::get('account_parents/edit/{id}','AdminAccount_parentController@edit')->name('admin.account_parents.edit');
    Route::match(['put', 'patch'], 'account_parents/update/{id}','AdminAccount_parentController@update')->name('admin.account_parents.update');
    Route::get('account_parents/delete/{id}', 'AdminAccount_parentController@destroy')->name('admin.account_parents.edit');
});




Route::group(array('module'=>'account_parent','namespace' => 'App\modules\account_parent\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('account_parents/','Account_parentController@index')->name('account_parents');
    
});