@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Account Parents   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.account_parents') }}">Account Parent</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.account_parents.update', $account_parent->id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                    <label for="name">Name</label><input type="text" value = "{{$account_parent->name}}"  name="name" id="name" class="form-control" >
                </div>
                <div class="form-group">
                    <label for="status">Status</label>
                    <div class="radio">
                        <label>
                        <input type="radio" value="1" name="status" id="status1" @if($account_parent->status == 1) checked="checked" @endif/>Yes</label> 
                        <label><input type="radio" value="0" name="status" id="status0" @if($account_parent->status == 0) checked="checked" @endif/>No</label>
                    </div>
                </div>
<input type="hidden" name="id" id="id" value = "{{$account_parent->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.account_parents') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
