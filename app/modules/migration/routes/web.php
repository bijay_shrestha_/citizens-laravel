<?php



Route::group(array('prefix'=>'admin/','module'=>'Migration','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Migration\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('migrations/','AdminMigrationController@index')->name('admin.migrations');
    Route::post('migrations/getmigrationsJson','AdminMigrationController@getmigrationsJson')->name('admin.migrations.getdatajson');
    Route::get('migrations/create','AdminMigrationController@create')->name('admin.migrations.create');
    Route::post('migrations/store','AdminMigrationController@store')->name('admin.migrations.store');
    Route::get('migrations/show/{id}','AdminMigrationController@show')->name('admin.migrations.show');
    Route::get('migrations/edit/{id}','AdminMigrationController@edit')->name('admin.migrations.edit');
    Route::match(['put', 'patch'], 'migrations/update/{id}','AdminMigrationController@update')->name('admin.migrations.edit');
    Route::get('migrations/delete/{id}', 'AdminMigrationController@destroy')->name('admin.migrations.edit');
});




Route::group(array('module'=>'Migration','namespace' => 'App\Modules\Migration\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('migrations/','MigrationController@index')->name('migrations');
    
});