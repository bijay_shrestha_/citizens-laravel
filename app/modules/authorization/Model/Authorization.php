<?php

namespace App\Modules\Authorization\Model;


use Illuminate\Database\Eloquent\Model;

class Authorization extends Model
{
    public  $table = 'tbl_authorization';

    protected $fillable = ['auth_id','a_id','longitude','latitude','signature1','signature2','signature3','name1','name2','name3',];
}
