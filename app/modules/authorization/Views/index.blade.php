@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Authorizations		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Authorizations</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.authorizations.create') }}" class="btn bg-green waves-effect"  title="create">Create</a>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="authorization-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								<th >Auth_id</th>
<th >A_id</th>
<th >Longitude</th>
<th >Latitude</th>
<th >Signature1</th>
<th >Signature2</th>
<th >Signature3</th>
<th >Name1</th>
<th >Name2</th>
<th >Name3</th>

								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#authorization-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.authorizations.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
					{ data: "auth_id",name: "auth_id"},
            { data: "a_id",name: "a_id"},
            { data: "longitude",name: "longitude"},
            { data: "latitude",name: "latitude"},
            { data: "signature1",name: "signature1"},
            { data: "signature2",name: "signature2"},
            { data: "signature3",name: "signature3"},
            { data: "name1",name: "name1"},
            { data: "name2",name: "name2"},
            { data: "name3",name: "name3"},
            
					{ data: function(data,b,c,table) { 
					var buttons = '';

					buttons += "<a class='btn bg-red waves-effect' href='"+site_url+"/edit/"+data.id+"' type='button' >Edit</a>&nbsp"; 

					buttons += "<a href='"+site_url+"/delete/"+data.id+"' class='btn bg-red waves-effect' >Delete</a>";

					return buttons;
					}, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
