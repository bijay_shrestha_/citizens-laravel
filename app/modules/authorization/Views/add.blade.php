@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Authorizations   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.authorizations') }}">tbl_authorization</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.authorizations.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                                    <label for="auth_id">Auth_id</label><input type="text" name="auth_id" id="auth_id" class="form-control" ></div><div class="form-group">
                                    <label for="a_id">A_id</label><input type="text" name="a_id" id="a_id" class="form-control" ></div><div class="form-group">
                                    <label for="longitude">Longitude</label><input type="text" name="longitude" id="longitude" class="form-control" ></div><div class="form-group">
                                    <label for="latitude">Latitude</label><input type="text" name="latitude" id="latitude" class="form-control" ></div><div class="form-group">
                                    <label for="signature1">Signature1</label><input type="text" name="signature1" id="signature1" class="form-control" ></div><div class="form-group">
                                    <label for="signature2">Signature2</label><input type="text" name="signature2" id="signature2" class="form-control" ></div><div class="form-group">
                                    <label for="signature3">Signature3</label><input type="text" name="signature3" id="signature3" class="form-control" ></div><div class="form-group">
                                    <label for="name1">Name1</label><input type="text" name="name1" id="name1" class="form-control" ></div><div class="form-group">
                                    <label for="name2">Name2</label><input type="text" name="name2" id="name2" class="form-control" ></div><div class="form-group">
                                    <label for="name3">Name3</label><input type="text" name="name3" id="name3" class="form-control" ></div>
<input type="hidden" name="id" id="id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.authorizations') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
