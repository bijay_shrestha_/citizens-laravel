<?php



Route::group(array('prefix'=>'admin/','module'=>'authorization','middleware' => ['web','auth'], 'namespace' => 'App\modules\authorization\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('authorizations/','AdminAuthorizationController@index')->name('admin.authorizations');
    Route::post('authorizations/getauthorizationsJson','AdminAuthorizationController@getauthorizationsJson')->name('admin.authorizations.getdatajson');
    Route::get('authorizations/create','AdminAuthorizationController@create')->name('admin.authorizations.create');
    Route::post('authorizations/store','AdminAuthorizationController@store')->name('admin.authorizations.store');
    Route::get('authorizations/show/{id}','AdminAuthorizationController@show')->name('admin.authorizations.show');
    Route::get('authorizations/edit/{id}','AdminAuthorizationController@edit')->name('admin.authorizations.edit');
    Route::match(['put', 'patch'], 'authorizations/update/{id}','AdminAuthorizationController@update')->name('admin.authorizations.update');
    Route::get('authorizations/delete/{id}', 'AdminAuthorizationController@destroy')->name('admin.authorizations.edit');
});




Route::group(array('module'=>'authorization','namespace' => 'App\modules\authorization\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('authorizations/','AuthorizationController@index')->name('authorizations');
    
});