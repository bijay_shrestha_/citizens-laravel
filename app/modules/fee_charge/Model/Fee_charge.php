<?php

namespace App\modules\fee_charge\Model;


use Illuminate\Database\Eloquent\Model;

class Fee_charge extends Model
{
    public  $table = 'tbl_fee_charges';

    protected $fillable = ['fee_charge_id','parent_id','fee_charge_name','file_name','rupee','usd','fee_and_charges','approvel_status','approved','added_by','added_date','modified_date','deleted_by','deleted_date','del_flag','status',];
}
