<?php



Route::group(array('prefix'=>'admin/','module'=>'fee_charge','middleware' => ['web','auth'], 'namespace' => 'App\modules\fee_charge\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('fee_charges/','AdminFee_chargeController@index')->name('admin.fee_charges');
    Route::post('fee_charges/getfee_chargesJson','AdminFee_chargeController@getfee_chargesJson')->name('admin.fee_charges.getdatajson');
    Route::get('fee_charges/create','AdminFee_chargeController@create')->name('admin.fee_charges.create');
    Route::post('fee_charges/store','AdminFee_chargeController@store')->name('admin.fee_charges.store');
    Route::get('fee_charges/show/{id}','AdminFee_chargeController@show')->name('admin.fee_charges.show');
    Route::get('fee_charges/edit/{id}','AdminFee_chargeController@edit')->name('admin.fee_charges.edit');
    Route::match(['put', 'patch'], 'fee_charges/update/{id}','AdminFee_chargeController@update')->name('admin.fee_charges.update');
    Route::get('fee_charges/delete/{id}', 'AdminFee_chargeController@destroy')->name('admin.fee_charges.edit');
    Route::get('fee_charges/change/{text}/{status}/{id}', 'AdminFee_chargeController@change');
});




Route::group(array('module'=>'fee_charge','namespace' => 'App\modules\fee_charge\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('fee_charges/','Fee_chargeController@index')->name('fee_charges');
    
});