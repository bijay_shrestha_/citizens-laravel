@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Fee Charges   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.fee_charges') }}">Fee Charges</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.fee_charges.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                        <label for="fee_charge_name">Fee Charge Name</label><input type="text" name="fee_charge_name" id="fee_charge_name" class="form-control" required >
                    </div>

                    <div class="form-group">
                        <label for="rupee">Rupee</label><input type="text" name="rupee" id="rupee" class="form-control" required >
                    </div>

                    <div class="form-group">
                        <label for="usd">Usd</label><input type="text" name="usd" id="usd" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="fee_and_charges">Fee And Charges</label><textarea name="fee_and_charges" id="fee_and_charges" class="form-control my-editor" > </textarea>
                    </div>
                    
                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="status" id="status1" @if(old('status') == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="status" id="status0" @if(old('status') == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.fee_charges') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
