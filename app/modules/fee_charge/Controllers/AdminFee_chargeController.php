<?php

namespace App\modules\fee_charge\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\fee_charge\Model\Fee_charge;
use Carbon\Carbon;

class AdminFee_chargeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Fee Charge';
        return view("fee_charge::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getfee_chargesJson(Request $request)
    {
        $fee_charge = new Fee_charge;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $fee_charge->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('fee_charge_id', "DSC")->get();

        // Display limited list        
        $rows = $fee_charge->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->orderBy('fee_charge_id', "DSC")->where('del_flag',0)->get();

        //To count the total values present
        $total = $fee_charge->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Fee Charge | Create';
        return view("fee_charge::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');

        $datafor_freecharges = $data;
        $datafor_freecharges['added_by'] = Auth::user()->id;
        $datafor_freecharges['added_date'] = new Carbon();
        $datafor_freecharges['del_flag'] = 0;
        $datafor_freecharges['parent_id'] = 0;
        $datafor_freecharges['file_name'] = '';
        $datafor_freecharges = check_case($datafor_freecharges, 'create');
        $success = Fee_charge::Insert($datafor_freecharges);
        $fee_charges = Fee_charge::all();
        $fee_charge_id = $fee_charges[count($fee_charges) - 1]['fee_charge_id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "FEE_CHARGES", 'file_id' => $fee_charge_id]); //create acrivity log     
        return redirect()->route('admin.fee_charges')->with('success', "Data inserted successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fee_charge = Fee_charge::where('fee_charge_id', $id)->firstOrFail();
        $page['title'] = 'Fee Charge | Update';
        return view("fee_charge::edit",compact('page','fee_charge'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');

        $datafor_freecharges = $data;
        $datafor_freecharges['modified_date'] = new Carbon();
        $datafor_freecharges = check_case($datafor_freecharges, 'edit');
        $success = DB::table('tbl_fee_charges')->where('fee_charge_id', $id)->update($datafor_freecharges);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "FEE_CHARGES", 'file_id' => $id]); //create acrivity log     

        return redirect()->route('admin.fee_charges')->with('success', "Data updated successfully.");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_fee_charges', 'fee_charge_id', $id, 'deleted_by', 'deleted_date');
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "FEE_CHARGES", 'file_id' => $id]); //create acrivity log     
        return redirect()->route('admin.fee_charges')->with('success', "Data deleted for trash.");
        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_fee_charges', 'fee_charge_id', 'deleted_by', 'deleted_date');
        return redirect()->route('admin.fee_charges')->with('success', "Data updated ");
    }

}
