<?php

namespace App\Modules\Page\Model;


use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public  $table = 'pages';

    protected $fillable = ['page_id','page_title','description','image_name','business_type','content_type_id','parent_id','template_id','default','sequence','meta_keywords','meta_description','created_date','modified_date','slug_id','slug_name','download_form_link','apply_form_link','submit_form_link','url','deleted_by','deleted_date','new_window','is_menu','del_flag','status'];
}
