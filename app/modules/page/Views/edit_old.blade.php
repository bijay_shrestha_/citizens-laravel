@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Pages   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.pages') }}">pages</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.pages.update') }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                                    <label for="page_id">Page_id</label><input type="text" value = "{{$page->page_id}}"  name="page_id" id="page_id" class="form-control" ></div><div class="form-group">
                                    <label for="page_title">Page_title</label><input type="text" value = "{{$page->page_title}}"  name="page_title" id="page_title" class="form-control" ></div><div class="form-group">
                                    <label for="description">Description</label><input type="text" value = "{{$page->description}}"  name="description" id="description" class="form-control" ></div><div class="form-group">
                                    <label for="image_name">Image_name</label><input type="text" value = "{{$page->image_name}}"  name="image_name" id="image_name" class="form-control" ></div><div class="form-group">
                                    <label for="business_type">Business_type</label><input type="text" value = "{{$page->business_type}}"  name="business_type" id="business_type" class="form-control" ></div><div class="form-group">
                                    <label for="content_type_id">Content_type_id</label><input type="text" value = "{{$page->content_type_id}}"  name="content_type_id" id="content_type_id" class="form-control" ></div><div class="form-group">
                                    <label for="parent_id">Parent_id</label><input type="text" value = "{{$page->parent_id}}"  name="parent_id" id="parent_id" class="form-control" ></div><div class="form-group">
                                    <label for="template_id">Template_id</label><input type="text" value = "{{$page->template_id}}"  name="template_id" id="template_id" class="form-control" ></div><div class="form-group">
                                    <label for="default">Default</label><input type="text" value = "{{$page->default}}"  name="default" id="default" class="form-control" ></div><div class="form-group">
                                    <label for="sequence">Sequence</label><input type="text" value = "{{$page->sequence}}"  name="sequence" id="sequence" class="form-control" ></div><div class="form-group">
                                    <label for="meta_keywords">Meta_keywords</label><input type="text" value = "{{$page->meta_keywords}}"  name="meta_keywords" id="meta_keywords" class="form-control" ></div><div class="form-group">
                                    <label for="meta_description">Meta_description</label><input type="text" value = "{{$page->meta_description}}"  name="meta_description" id="meta_description" class="form-control" ></div><div class="form-group">
                                    <label for="created_date">Created_date</label><input type="text" value = "{{$page->created_date}}"  name="created_date" id="created_date" class="form-control" ></div><div class="form-group">
                                    <label for="modified_date">Modified_date</label><input type="text" value = "{{$page->modified_date}}"  name="modified_date" id="modified_date" class="form-control" ></div><div class="form-group">
                                    <label for="slug_id">Slug_id</label><input type="text" value = "{{$page->slug_id}}"  name="slug_id" id="slug_id" class="form-control" ></div><div class="form-group">
                                    <label for="slug_name">Slug_name</label><input type="text" value = "{{$page->slug_name}}"  name="slug_name" id="slug_name" class="form-control" ></div><div class="form-group">
                                    <label for="download_form_link">Download_form_link</label><input type="text" value = "{{$page->download_form_link}}"  name="download_form_link" id="download_form_link" class="form-control" ></div><div class="form-group">
                                    <label for="apply_form_link">Apply_form_link</label><input type="text" value = "{{$page->apply_form_link}}"  name="apply_form_link" id="apply_form_link" class="form-control" ></div><div class="form-group">
                                    <label for="submit_form_link">Submit_form_link</label><input type="text" value = "{{$page->submit_form_link}}"  name="submit_form_link" id="submit_form_link" class="form-control" ></div><div class="form-group">
                                    <label for="url">Url</label><input type="text" value = "{{$page->url}}"  name="url" id="url" class="form-control" ></div><div class="form-group">
                                    <label for="deleted_by">Deleted_by</label><input type="text" value = "{{$page->deleted_by}}"  name="deleted_by" id="deleted_by" class="form-control" ></div><div class="form-group">
                                    <label for="deleted_date">Deleted_date</label><input type="text" value = "{{$page->deleted_date}}"  name="deleted_date" id="deleted_date" class="form-control" ></div><div class="form-group">
                                    <label for="new_window">New_window</label><input type="text" value = "{{$page->new_window}}"  name="new_window" id="new_window" class="form-control" ></div><div class="form-group">
                                    <label for="is_menu">Is_menu</label><input type="text" value = "{{$page->is_menu}}"  name="is_menu" id="is_menu" class="form-control" ></div><div class="form-group">
                                    <label for="del_flag">Del_flag</label><input type="text" value = "{{$page->del_flag}}"  name="del_flag" id="del_flag" class="form-control" ></div><div class="form-group">
                                    <label for="status">Status</label><input type="text" value = "{{$page->status}}"  name="status" id="status" class="form-control" ></div>
<input type="hidden" name="id" id="id" value = "{{$page->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.pages') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
