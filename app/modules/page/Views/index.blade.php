@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Pages		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Pages</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.pages.create') }}" class="btn bg-green waves-effect"  title="create">Create Content Link</a> &nbsp; &nbsp;<a href="{{ route('admin.pages.createlink') }}" class="btn bg-green waves-effect"  title="create">Add Content link</a>
					</div>
					<!-- /.box-header -->
					<br>
					
					@if ($message = Session::get('success'))
					<div class="btn btn-success">
						{{$message}}
					</div>
					@endif

					<div class="box-body">
						<table id="page-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								<th >Page Title</th>
								<th >Content Type </th>
								<th >Business Type</th>
								<th >Created Date</th>
								<th >Status</th>
								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		var config_url = "{{url('/')}}";
		$(function(){
			dataTable = $('#page-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.pages.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
		    { data: function (data,b,c,table){
		    	var links = "";
		    	links += "<a href='" + config_url + "/page/index/" + data.page_id + "' target='_blank'>" + data.page_title + "</a>"
		    	return links;
		    }, name: 'page_title', searchable: false},
            { data: "contentname",name: "contentname"},
            { data: "business_type",name: "business_type"},
            { data: "created_date",name: "created_date"},
            { data: "status",name: "status"},
					{ data: function(data,b,c,table) { 
					var buttons = '';
					if (data.url) {
						buttons += "<a class='btn bg-green waves-effect' href='"+site_url+"/editlink/"+data.page_id+"' type='button' ><i class='fa fa-edit'></i> Edit</a>&nbsp"; 

						buttons += "<a class='btn bg-yellow waves-effect' href='" + data.url + "' target='_blank'><i class='icon-play-circle'></i> Follow</a>&nbsp;";
					} else {
						buttons += "<a class='btn bg-green waves-effect' href='"+site_url+"/edit/"+data.page_id+"' type='button' ><i class='fa fa-edit'></i> Edit</a>&nbsp"; 

						buttons += "<a class='btn bg-yellow waves-effect' href='" + config_url + "/" + data.slug_name + "' target='_blank'><i class='icon-play-circle'></i> Visit</a>&nbsp;";
					}
					buttons += "<a href='"+site_url+"/delete/"+data.page_id+"' class='btn bg-red waves-effect'  ><i class='fa fa-trash'></i>  Delete</a>";

					return buttons;
					}, name:'action',searchable: false},
					
				]
			});
		});

		
	</script>
@endsection
