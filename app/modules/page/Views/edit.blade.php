@extends('admin.layout.main')
@section('content')
<link rel="stylesheet" href="{{ asset('css/navpanel.css') }}">
        <!-- jvectormap -->
    <section class="content-header">
        <h1>
            Edit Pages   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.pages') }}">Pages</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
        
              <form role="form" action="{{ route('admin.pages.update', $pagedata->page_id) }}"  method="post">
                <div class="box-body">                
                    <div><h2><!-- --></h2></div>
                     {{method_field('PATCH')}}
                     <input type="hidden" value = "{{$pagedata->page_id}}"  name="page_id" id="page_id" class="form-control" >
                    <div id="exTab2"> 
                    <ul class="nav nav-tabs">
                                <li class="active">
                                <a  href="#1" data-toggle="tab">Content</a>
                                </li>
                                <li><a href="#2" data-toggle="tab">Attributes</a>
                                </li>
                                <li><a href="#3" data-toggle="tab">SEO</a>
                                </li>
                            </ul>

                            <div class="tab-content ">
                              <div class="tab-pane active" id="1">
                              
                              <div class="form-group">
                                  <label for="page_title">Page Title (*)</label><input type="text" name="page_title" id="page_title" class="form-control" value="{{$pagedata->page_title}}" required>
                              </div>

                              <div class="form-group">
                                  <label>Template</label>
                                  <select name="template_id" class="form-control">
                                  <option value="0"></option>
                                  @foreach($page_templates as $page_template)
                                    <option value="{{$page_template->page_template_id}}" @if($page_template->page_template_id == $pagedata->template_id) selected @endif>{{$page_template->template_name}}</option> 
                                  @endforeach
                                  </select>
                              </div>
                              <div class="form-group">
                                  <label for="image_name">Image </label><br>
                                  <label id="upload_image_name" style="display:none"></label>
                                  <div id="delete_old_image_requested"></div>
                                  <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                                  <div id="image-status"></div>
                                  <input type="file" id="upload_image" name="upload_image" @if (file_exists(public_path().'/uploads/page/thumb/'. $pagedata->image_name) && $pagedata->image_name) style="display:none" @else style="display:block" @endif />
                                  <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                                  <div id="imagedata">
                                  @if (file_exists(public_path().'/uploads/page/'. $pagedata->image_name) && $pagedata->image_name)
                              
                                      <img src="{{url('uploads/page/thumb/'.$pagedata->image_name)}}">
                                      <a href="javascript:void(0)" id="change-image_option" title="Change Image">&nbsp; &nbsp; <img src="{{('/images/logo/cancel.png')}}" border="0"/> Change the old image</a>
                                  @endif                                
                                  </div>
                              </div>


                              <label><input type="checkbox" name="is_menu" id="is_menu" value="1"  @if($pagedata->is_menu == 1) checked @endif/>Is Menu</label>

                              <div class="form-group">                                    
                                  <label for="description">Description</label><textarea name="description" id="description" class="form-control my-editor">{!! $pagedata->description !!}</textarea>
                              </div>
                              
                              <div class="form-group">
                                  <label for="status">Status</label>
                                  <div class="radio">
                                  <label>
                                      <input type="radio" value="1" name="status" id="status1" @if($pagedata->status == 1) checked @endif  />Yes
                                  </label> 
                                  <label>
                                      <input type="radio" value="0" name="status" id="status0" @if($pagedata->status == 1) @else  checked @endif  />No
                                  </label>
                                  </div>
                              </div>
                              
                              <div class="form-group">
                                  <label for="business_type">Business Type</label>
                                    <select class="form-control" id="business_type" name="business_type">
                                      <option value="BUSINESS" @if($pagedata->business_type == "BUSINESS") selected @endif >BUSINESS</option>
                                      <option value="PERSONAL" @if($pagedata->business_type == "PERSONAL") selected @endif >PERSONAL</option>
                                      <option value="CORPORATE" @if($pagedata->business_type == "CORPORATE") selected @endif >CORPORATE</option>
                                    </select>
                              </div>
                              
                              <div class="form-group">
                                  <label for="content_type_id">Content Type</label><br>
                                  <select name="content_type_id" id="content_type_id" class="form-control">
                                    <option value="0">Select Content Type</option>
                                    @foreach($content_types as $content_type)
                                      <option value="{{$content_type->content_type_id}}" @if($content_type->content_type_id == $pagedata->content_type_id) selected @endif >{{$content_type->name}}</option>
                                    @endforeach
                                  </select>
                              </div>

                            </div>
                            <div class="tab-pane" id="2">
                            
                           <div class="form-group">
                                <label for="parent">Parent</label>
                                <select class="form-control" id="parent_id" name="parent_id"/>
                                     <option value="0" @if($pagedata->parent_id == 0) selected  @endif >Top level</option>
                                        @foreach($parents as $parent) 
                                            @if($parent['parent_id'] == 0)
                                                @php $dash = ""; @endphp
                                            @else
                                                @php $dash= "-"; @endphp
                                            @endif
                                    <option value="{{$parent['page_id']}}" @if($parent['page_id'] == $pagedata->parent_id) selected  @endif> {{$dash.$parent['page_title']}}</option>
                                        @endforeach
                                </select>    
                            </div>
                              
                              <div class="form-group">
                                  <label for="sequence">Sequence</label><input type="number" name="sequence" id="sequence" class="form-control"  value="{{$pagedata->sequence}}" >
                              </div>
                            </div>

                            <div class="tab-pane" id="3">
                      
                              <div class="form-group">
                                  <label for="meta_keywords">Meta Keywords</label><input type="text" name="meta_keywords" id="meta_keywords" class="form-control" value="{{$pagedata->meta_keywords}}" >
                              </div>
                              
                              <div class="form-group">
                                  <label for="meta_description">Meta Description</label><input type="text" name="meta_description" id="meta_description" class="form-control"  value="{{$pagedata->meta_description}}" >
                              </div>
                            </div>
                            
                            
                      </div>                   
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.pages') }}" class="btn btn-danger">Cancel</a>
                </div>
                </div>
            </form>
        </div>
    </section>
       <script type="text/javascript">
          var directory = 'page';
</script>
    @include('data_changing_script_edit')
    @include('file_changing_script')


@endsection
