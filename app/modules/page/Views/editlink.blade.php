@extends('admin.layout.main')
@section('content')
<link rel="stylesheet" href="{{ asset('css/navpanel.css') }}">
        <!-- jvectormap -->
    <section class="content-header">
        <h1>
            Add Pages   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.pages') }}">pages</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
        
            <form role="form" action="{{ route('admin.pages.updatelink', $pagedata->page_id) }}"  method="post">
                <div class="box-body">                
                    <div><h2><!-- --></h2></div>
                    {{method_field('PATCH')}}
                   
                              
                              <div class="form-group">
                                  <label for="page_title">Page Title (*)</label><input type="text" name="page_title" id="page_title" class="form-control" value="{{$pagedata->page_title}}" required>
                              </div>
                              <div class="form-group">
                                  <label><input type="checkbox" name="is_menu" id="is_menu" value="1"  @if($pagedata->is_menu == 1) checked @endif />Is Menu</label>
                            
                              </div>

                              <div class="form-group">
                                  <label for="page_title">URL (*)</label><input type="text" name="url" id="url" class="form-control" value="{{$pagedata->url}}" required>
                              </div>

                              <div class="form-group">
                                <label><input type="checkbox" name="new_window" id="new_window" value="1" @if($pagedata->new_window == 1)  checked="checked" @endif />New Window</label>
                              </div>

                               <div class="form-group">
                                <label for="business_type">Business Type</label>
                                    <select class="form-control" id="business_type" name="business_type">
                                    <option value="BUSINESS" @if($pagedata->business_type == "BUSINESS") selected @endif >BUSINESS</option>
                                    <option value="PERSONAL" @if($pagedata->business_type == "PERSONAL") selected @endif >PERSONAL</option>
                                    <option value="CORPORATE"  @if($pagedata->business_type == "CORPORATE") selected @endif >CORPORATE</option>
                                </select>
                              </div>

                              <div class="form-group">
                                  <label for="content_type_id">Content Type</label><br>
                                  <select name="content_type_id" id="content_type_id" class="form-control">
                                    <option value="0">Select Content Type</option>
                                    @foreach($content_types as $content_type)
                                      <option value="{{$content_type->content_type_id}}">{{$content_type->name}}</option>
                                    @endforeach
                                  </select>
                              </div>
                            
                              <div class="form-group">
                                  <label for="name">Sequence</label> <small>(Enter 0 for default Order)</small>
                                  <input type="number" class="form-control" id="sequence" name="sequence" value="{{$content_type->sequence}}" required />
                              </div>  
                              
                              <div class="form-group">
                                  <label for="status">Status</label>
                                  <div class="radio">
                                  <label>
                                      <input type="radio" value="1" name="status" id="status1" @if($content_type->status == 1) checked @endif  />Yes
                                  </label> 
                                  <label>
                                      <input type="radio" value="0" name="status" id="status0" @if($content_type->status == 1) @else  checked @endif  />No
                                  </label>
                                  </div>
                              </div>          
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.pages') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
       <script type="text/javascript">
          var directory = 'page';
</script>
    @include('data_changing_script_create')
    @include('file_changing_script')

@endsection
