<?php



Route::group(array('prefix'=>'admin/','module'=>'page','middleware' => ['web','auth'], 'namespace' => 'App\modules\page\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('pages/','AdminPageController@index')->name('admin.pages');
    Route::post('pages/getpagesJson','AdminPageController@getpagesJson')->name('admin.pages.getdatajson');
    Route::get('pages/create','AdminPageController@create')->name('admin.pages.create');
    Route::get('pages/createlink','AdminPageController@createlink')->name('admin.pages.createlink');
    Route::post('pages/store','AdminPageController@store')->name('admin.pages.store');
    Route::post('pages/storelink','AdminPageController@storelink')->name('admin.pages.storelink');
    Route::get('pages/show/{id}','AdminPageController@show')->name('admin.pages.show');
    Route::get('pages/edit/{id}','AdminPageController@edit')->name('admin.pages.edit');
    Route::get('pages/editlink/{id}','AdminPageController@editlink')->name('admin.pages.editlink');
    Route::match(['put', 'patch'], 'pages/update/{id}','AdminPageController@update')->name('admin.pages.update');
    Route::match(['put', 'patch'], 'pages/updatelink/{id}','AdminPageController@updatelink')->name('admin.pages.updatelink');
    Route::get('pages/delete/{id}', 'AdminPageController@destroy')->name('admin.pages.edit');
});




Route::group(array('module'=>'page','namespace' => 'App\modules\page\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('pages/','PageController@index')->name('pages');
    
});