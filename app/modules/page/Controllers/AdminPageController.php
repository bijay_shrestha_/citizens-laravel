<?php

namespace App\modules\page\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\page\Model\Page;
use Carbon\Carbon;
use App\modules\account\Controllers\AdminAccountController;
use File;

class AdminPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Page';
        return view("page::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getpagesJson(Request $request)
    {
        $page = DB::table('pages');

        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $page->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('pages.del_flag',0)
        ->leftjoin('content_types', 'content_types.content_type_id', '=', 'pages.content_type_id')
        ->select('pages.*', 'content_types.name as contentname')
        ->orderBy('pages.page_id', 'DSC')
        ->get();
        

        // Display limited list        
        $rows = $page->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('pages.del_flag',0)->orderBy('pages.page_id', 'DSC')->get();

        //To count the total values present
        $total = $page->where('pages.del_flag',0)->orderBy('pages.page_id', 'DSC')->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Page | Create';
        $content_types = DB::table('content_types')->get();
        $page_templates = DB::table('tbl_page_templates')->get();
        return view("page::add",compact('page', 'content_types', 'page_templates'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $admin_account_controller = new AdminAccountController;
        $name_slug = $admin_account_controller->slugify($data['page_title']);
        $slug_name = $admin_account_controller->validate_slug($name_slug);        
        $slug_data = DB::table('slugs')->insert(['slug_name' => $slug_name]); //create slug
        $allslug =  DB::table('slugs')->get();
        $dataforpage = $data;
        unset($dataforpage['upload_image']);
        $slug_id = $allslug[count($allslug)-1]->slug_id;
        $dataforpage['slug_id'] = $slug_id;
        $dataforpage['slug_name'] = $slug_name;
        $dataforpage['del_flag'] = 0;
        $dataforpage['created_date'] = new Carbon();
        $success = Page::Insert($dataforpage);
        $pages = Page::all();
        $page_id = $pages[count($pages)-1]['pages_id'];
        $slug['route'] = 'page/index/' . $page_id;
        DB::table('slugs')->where('slug_id', $page_id)->update(['route' => $slug['route']]);
        return redirect()->route('admin.pages')->with('success', "Data created successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pagedata = Page::where('page_id',$id)->firstOrFail();
        $content_types = DB::table('content_types')->get();
        $page_templates = DB::table('tbl_page_templates')->get();
        $parents = Page::where('page_id', '!=', $id)->get()->toArray();
        $page['title'] = 'Page | Update';
        return view("page::edit",compact('page','pagedata', 'content_types', 'page_templates', 'parents'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $admin_account_controller = new AdminAccountController;
        $name_slug = $admin_account_controller->slugify($data['page_title']);
        $page = Page::where('page_id', $id)->firstOrFail();
        $slug['slug_name'] = $admin_account_controller->validate_slug($name_slug, $page->slug_id);
        $slug['route'] = 'page/index/'.$data['page_id'];
        DB::table('slugs')->where('slug_id', $page->slug_id)->update(['slug_name' =>  $slug['slug_name'], 'route' => $slug['route']]);
        $dataforpage = $data;
        $dataforpage['slug_name'] = $slug['slug_name'];
        unset($dataforpage['upload_image']);
        
        if (array_key_exists('delete_old_image', $data)) {
            if ((file_exists(public_path().'/uploads/page/'. $page->image_name))  && $page->image_name) {
                unlink(public_path().'/uploads/page/'. $page->image_name);
                if (file_exists(public_path().'/uploads/page/thumb/'. $page->image_name)) {
                    unlink(public_path().'/uploads/page/thumb/'. $page->image_name);
                } 
            }
            if (!$data['image_name']) {
                $data['image_name'] = '';
                $dataforpage['image_name'] = '';
            }
            unset($dataforpage['delete_old_image']);
            
        } else {
            if (!$data['image_name']) {
                $data['image_name'] = $page->image_name;
                $dataforpage['image_name'] = $page->image_name;
            }
        }
        if (array_key_exists('is_menu', $dataforpage)) {
            $dataforpage['is_menu'] = 1;
        } else {
            $dataforpage['is_menu'] = 0;
        }
        $dataforpage['modified_date'] = new Carbon();                    
  //      $success = Page::where('page_id', $id)->update($dataforpage);
        DB::table('pages')->where('page_id', $id)->update($dataforpage);
        return redirect()->route('admin.pages')->with('success', 'Data updated successfully.');                
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('pages', 'page_id', $id, 'deleted_by', 'deleted_date');
        return redirect()->route('admin.pages')->with('success, "data deleted successfully.');


        //
    }

    public function createlink() {
        $page['title'] = 'PageLink | Create';
        $content_types = DB::table('content_types')->get();
        $page_templates = DB::table('tbl_page_templates')->get();
        $parent_id = '';
        $pages = Page::all();
        return view("page::addlink",compact('page', 'content_types', 'page_templates', 'pages', 'parent_id'));
        //
    }

    public function storelink(Request $request) 
    {
        $data = $request->except('_token');
        $dataforpage = $data;
        $dataforpage['created_date'] = new Carbon();
        $dataforpage['del_flag'] = 0;
        $success = Page::Insert($dataforpage);
        return redirect()->route('admin.pages')->with('success', "Data created successfully.");
    }

    public function editlink($id) 
    {
        $pagedata = Page::where('page_id',$id)->firstOrFail();
        $content_types = DB::table('content_types')->get();
        $page_templates = DB::table('tbl_page_templates')->get();
        $parents = Page::where('page_id', '!=', $id)->get()->toArray();
        $page['title'] = 'Page | Edit';
        return view("page::editlink",compact('page','pagedata', 'content_types', 'page_templates', 'parents'));
    }

    public function updatelink(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $dataforpage = $data;
        $dataforpage['modified_date'] = new Carbon();
        if (array_key_exists('is_menu', $dataforpage)) {
            $dataforpage['is_menu'] = 1;
        } else {
            $dataforpage['is_menu'] = 0;
        }
        if (array_key_exists('new_window', $dataforpage)) {
            $dataforpage['new_window'] = 1;
        } else {
            $dataforpage['new_window'] = 0;
        }
        DB::table('pages')->where('page_id', $id)->update($dataforpage);
        return redirect()->route('admin.pages')->with('success', 'Data updated successfully.');           
    }
}
