<?php

namespace App\modules\banking\Model;


use Illuminate\Database\Eloquent\Model;

class Banking extends Model
{
    public  $table = 'tbl_banking';

    protected $fillable = ['banking_id','name','description','short_description','image_name','banner_collection_id','download_form','approvel_status','approved','slug_name','status','del_flag',];
}
