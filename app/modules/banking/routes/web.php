<?php



Route::group(array('prefix'=>'admin/','module'=>'banking','middleware' => ['web','auth'], 'namespace' => 'App\modules\banking\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('bankings/','AdminBankingController@index')->name('admin.bankings');
    Route::post('bankings/getbankingsJson','AdminBankingController@getbankingsJson')->name('admin.bankings.getdatajson');
    Route::get('bankings/create','AdminBankingController@create')->name('admin.bankings.create');
    Route::post('bankings/store','AdminBankingController@store')->name('admin.bankings.store');
    Route::get('bankings/show/{id}','AdminBankingController@show')->name('admin.bankings.show');
    Route::get('bankings/edit/{id}','AdminBankingController@edit')->name('admin.bankings.edit');
    Route::match(['put', 'patch'], 'bankings/update/{id}','AdminBankingController@update')->name('admin.bankings.update');
    Route::get('bankings/delete/{id}', 'AdminBankingController@destroy')->name('admin.bankings.edit');
    Route::get('bankings/change/{text}/{status}/{id}', 'AdminBankingController@change');

});




Route::group(array('module'=>'banking','namespace' => 'App\modules\banking\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('bankings/','BankingController@index')->name('bankings');
    
});