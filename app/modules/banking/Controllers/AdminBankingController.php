<?php

namespace App\modules\banking\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\banking\Model\Banking;
use App\modules\banner_collection\Model\Banner_collection;
use App\modules\account\Controllers\AdminAccountController;
use Carbon\Carbon;

class AdminBankingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Banking';
        return view("banking::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getbankingsJson(Request $request)
    {
        $banking = new Banking;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $banking->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('banking_id', 'DSC')->get();

        // Display limited list        
        $rows = $banking->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->orderBy('banking_id', 'DSC')->get();

        //To count the total values present
        $total = $banking->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Banking | Create';
        $banners_collection = Banner_collection::all();
        return view("banking::add",compact('page', 'banners_collection'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $admin_account_controller = new AdminAccountController;
        $name_slug = $admin_account_controller->slugify($data['name']);
        $slug_name = $admin_account_controller->validate_slug($name_slug);
        $slug_data = DB::table('slugs')->insert(['slug_name' => $slug_name]); //create slug
        $allslug =  DB::table('slugs')->get();
        $dataforbanking = $data;

        $slug_id = $allslug[count($allslug)-1]->slug_id;
        $dataforbanking['slug_name'] = $slug_name;        
        $dataforbanking['slug_id'] = $slug_id;        
        unset($dataforbanking['upload_image']);
        $dataforbanking['del_flag'] = 0;
        $dataforbanking = check_case($dataforbanking, 'create');
        $success = Banking::Insert($dataforbanking);
        $bankings = Banking::all();
        $banking_id = $bankings[count($bankings)-1]['banking_id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "banking", 'file_id' => $banking_id]); //create activity log
        $slug['route'] = 'banking/details/' . $banking_id;
        DB::table('slugs')->where('slug_id', $slug_id)->update(['route' => $slug['route']]);
        return redirect()->route('admin.bankings')->with('success', "Data inserted successfully");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banking = Banking::where('banking_id', $id)->firstOrFail();
        $page['title'] = 'Banking | Update';
        $banners_collection = Banner_collection::all();
        return view("banking::edit",compact('page','banking', 'banners_collection'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $banking = Banking::where('banking_id', $id)->firstOrFail();
        $admin_account_controller = new AdminAccountController;
        $name_slug = $admin_account_controller->slugify($data['name']);
        $slug['slug_name'] = $admin_account_controller->validate_slug($name_slug, $banking->slug_id); //$banking->slug_id        
        $slug['route'] = 'banking/details/'.$id;         //$data['loan_id'] = $id
        DB::table('slugs')->where('slug_id', $banking->slug_id)->update(['slug_name' =>  $slug['slug_name'], 'route' => $slug['route']]);
        $dataforbanking = $data;
        unset($dataforbanking['upload_image']);
        
        if (array_key_exists('delete_old_image', $data)) {
            if ((file_exists(public_path().'/uploads/banking/'. $banking->image_name))  && $banking->image_name) {
                unlink(public_path().'/uploads/banking/'. $banking->image_name);
                if (file_exists(public_path().'/uploads/banking/thumb/'. $banking->image_name)) {
                    unlink(public_path().'/uploads/banking/thumb/'. $banking->image_name);
                } 
            }
            if (!$data['image_name']) {
                $data['image_name'] = '';
                $dataforbanking['image_name'] = '';
            }
            unset($dataforbanking['delete_old_image']);
        } else {
            if (!$data['image_name']) {
                $data['image_name'] = $banking->image_name;
                $dataforbanking['image_name'] = $banking->image_name;
            }
        }
        $dataforbanking = check_case($dataforbanking, 'edit');
        DB::table('tbl_banking')->where('banking_id', $id)->update($dataforbanking);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "banking", 'file_id' => $id]); //create activity log
        return redirect()->route('admin.bankings')->with('success', "Data updated successfully");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_banking', 'banking_id', $id);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "banking", 'file_id' => $id]); //create activity log
        return redirect()->route('admin.bankings')->with('success', "Data updated successfully");
        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_banking', 'banking_id');
        return redirect()->route('admin.bankings')->with('success', "Data updated successfully.");
    }

}
