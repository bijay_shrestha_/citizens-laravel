@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Bankings   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.bankings') }}">banking</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.bankings.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                        <label for="name">Name </label><input type="text" name="name" id="name" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="description">Description</label><textarea name="description" id="description" class="form-control my-editor" >{!! old('description') !!}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="short_description">Short Description</label><textarea name="short_description" id="short_description" class="form-control my-editor" >{!! old('short_description') !!}</textarea>
                    </div>
                    
                    <div class="form-group">
                        <label for="image_name">Image</label><br>
                        <label id="upload_image_name" style="display:none"></label>
                        <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                        <div id="image-status"></div>
                        <input type="file" id="upload_image" name="upload_image" style="display:block"/>
                        <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                    </div>



                    <div class="form-group">
                        <label for="banner_collection_id">Banner Collection </label>
                        <select name="banner_collection_id" class="form-control">
                            <option value="0">Select Banner </option> 
                            @foreach ($banners_collection as $banner_collection)
                                <option value="{{$banner_collection['banner_collection_id']}}" 
                                 @if (old('banner_collection_id') == $banner_collection['banner_collection_id']) selected @endif
                                > {{$banner_collection['name']}} </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Status</label><div class="radio"><label><input type="radio" value="1" name="status" id="status1" @if(old('status') == '1') checked="checked" @endif/> Yes</label> 
                    <label><input type="radio" value="0" name="status" id="status0" @if(old('status') == '1') @else checked="checked" @endif  /> No</label></div></div>                               
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.bankings') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
        <script type="text/javascript">
        var directory = 'banking';
    </script>
    @include('data_changing_script_create')
    @include('file_changing_script')

@endsection
