@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Bankings   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.bankings') }}">Banking</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.bankings.update', $banking->banking_id) }}"  method="post">
                <div class="box-body">      
                {{method_field('PATCH')}}   
                    <input type="hidden" name="banking_id" value="{{$banking->banking_id}}">           
                    <div class="form-group">
                        <label for="name">Name </label><input type="text" name="name" id="name" class="form-control" value="{{$banking->name}}" required>
                    </div>

                    <div class="form-group">
                        <label for="description">Description</label><textarea name="description" id="description" class="form-control my-editor" >{!! $banking->description !!}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="short_description">Short Description</label><textarea name="short_description" id="short_description my-editor" class="form-control" >{!! $banking->short_description !!}</textarea>
                    </div>
                    
                    <div class="form-group">
                        <label for="image_name">Image </label><br>
                        <label id="upload_image_name" style="display:none"></label>
                        <div id="delete_old_image_requested"></div>
                        <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                        <div id="image-status"></div>
                        <input type="file" id="upload_image" name="upload_image" @if (file_exists(public_path().'/uploads/banking/thumb/'. $banking->image_name) && $banking->image_name) style="display:none" @else style="display:block" @endif />
                        <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                        <div id="imagedata">
                        @if (file_exists(public_path().'/uploads/banking/'. $banking->image_name) && $banking->image_name)
                    
                            <img src="{{url('uploads/banking/thumb/'.$banking->image_name)}}">
                            <a href="javascript:void(0)" id="change-image_option" title="Change Image">&nbsp; &nbsp; <img src="{{('/images/logo/cancel.png')}}" border="0"/> Change the old image</a>
                        @endif                                
                        </div>
                    </div>



                    <div class="form-group">
                        <label for="banner_collection_id">Banner Collection </label>
                        <select name="banner_collection_id" class="form-control">
                            <option value="0">Select Banner </option> 
                            @foreach ($banners_collection as $banner_collection)
                                <option value="{{$banner_collection['banner_collection_id']}}" 
                                 @if ($banking->banner_collection_id == $banner_collection['banner_collection_id']) selected @endif
                                > {{$banner_collection['name']}} </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Status</label><div class="radio"><label><input type="radio" value="1" name="status" id="status1" @if($banking->status == '1') checked="checked" @endif/> Yes</label> 
                    <label><input type="radio" value="0" name="status" id="status0" @if($banking->status == '1') @else checked="checked" @endif  /> No</label></div></div>                               
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.bankings') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
        <script type="text/javascript">
        var directory = 'banking';
    </script>
    @include('data_changing_script_edit')
    @include('file_changing_script')

@endsection