@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Bankings		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Bankings</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.bankings.create') }}" class="btn bg-green waves-effect"  title="create">Create</a>
					</div>
					<!-- /.box-header -->
					<br>
					@if ($message = Session::get('success'))
					<div class="btn btn-success">
						{{$message}}
					</div>
					@endif
					
					<div class="box-body">
						<table id="banking-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
							<th >Name</th>
							<th >Short Description</th>
							<th >Image </th>
							<th >Approvel Status</th>
							<th >Approved</th>
							<th >Status</th>

								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#banking-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.bankings.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
		    { data: "name",name: "name"},
            { data: "short_description",name: "short_description"},
            { data: function (data, b,c, table){
            	var imagefile = '';
            	if (data.image_name) {
            		imagefile += '<img src="{{url("/uploads/banking/thumb/")}}/'+data.image_name+'">';
            	}
            	return imagefile;

            }, name:'image_name', searchable: false},
            { data: "approvel_status",name: "approvel_status"},
            { data: function (data)  {
            	var content = "";

            	content += "<a href='{{url('/admin/bankings/change/')}}/approved/" + data.approvel_status + "/" + data.banking_id + "'style='color:";
            	if (data.approved == "approved") { content += "green"; }
            	content += "'>Approved</a><br>";
            	
            	content += "<a href='{{url('/admin/bankings/change/')}}/pending/" + data.approvel_status + "/" + data.banking_id + "'style='color:";
            	if (data.approved == "pending") { content += "green"; }
            	content += "'>Pending</a><br>";

            	content += "<a href='{{url('/admin/bankings/change/')}}/denied/" + data.approvel_status + "/" + data.banking_id + "'style='color:";
            	if (data.approved == "denied") { content += "green"; }
            	content += "'>Denied</a><br>";
            	
            	return content;
            },name:'approved', searchable: false},                                    
            { data: "status",name: "status"},

			{ data: function(data,b,c,table) { 
			var buttons = '';

			buttons += "<a class='btn bg-green waves-effect' href='"+site_url+"/edit/"+data.banking_id+"' type='button' ><i class='fa fa-edit'></i> Edit</a>&nbsp"; 

			buttons += "<a href='"+site_url+"/delete/"+data.banking_id+"' class='btn bg-red waves-effect' ><i class='fa fa-trash'></i>  Delete</a>";

			return buttons;
			}, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
