@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Departments   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.department_contacts') }}">departments</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.department_contacts.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                        <label for="name">Name</label><input type="text" name="name" id="name" class="form-control" required >
                    </div>
                    <div class="form-group">
                        <label for="contact_no">Contact Number</label><input type="text" name="contact_no" id="contact_no" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label><input type="email" name="email" id="email" class="form-control" required >
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label> <br>
                        <select name="status">
                            <option value="1">Enabled</option>
                            <option value="0">Disabled</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="branch_id">Branch</label> <br>
                        <select name="branch_id">
                            @foreach ($branches as $branch)
                            <option value="{{$branch->id}}">{{$branch->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                                    <!-- <label for="created_at">Created_at</label><input type="text" name="created_at" id="created_at" class="form-control" ></div><div class="form-group">
                                    <label for="updated_at">Updated_at</label><input type="text" name="updated_at" id="updated_at" class="form-control" ></div> -->
                    <input type="hidden" name="id" id="id"/>
                    </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.department_contacts') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
