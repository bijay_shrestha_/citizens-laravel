@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Departments   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.department_contacts') }}">departments</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.department_contacts.update', $department->id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                    <label for="name">Name</label><input type="text" value = "{{$department->name}}"  name="name" id="name" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="contact_no">Contact Number</label><input type="text" name="contact_no" id="contact_no" class="form-control" value="{{$department->contact_no}}">
                </div>
                <div class="form-group">
                    <label for="email">Email</label><input type="text" value = "{{$department->email}}"  name="email" id="email" class="form-control" required>
                </div>
                <div class="form-group">
                <label for="status">Status</label> <br>
                <select name="status">
                    <option value="1" @if ($department->status == 1) selected @endif>Enabled</option>
                    <option value="0" @if ($department->status == 0) selected @endif>Disabled</option>
                </select>
                </div>
                <div class="form-group">
                    <label for="branch_id">Branch</label> <br>
                    <select name="branch_id">
                        @foreach ($branches as $branch)                        
                        <option value="{{$branch->id}}" @if($branch->id == $department->branch_id) selected @endif>{{$branch->name}}</option>
                        @endforeach
                    </select>
                </div>
                <input type="hidden" name="id" id="id" value = "{{$department->id}}" /> 
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.department_contacts') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
