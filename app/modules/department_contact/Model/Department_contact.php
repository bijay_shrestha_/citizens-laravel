<?php

namespace App\Modules\Department_contact\Model;


use Illuminate\Database\Eloquent\Model;

class Department_contact extends Model
{
    public  $table = 'tbl_department_contacts';

    protected $fillable = ['id','name','contact_no','email','status','branch_id','approvel_status','approved','created_by','created_date','modified_by','modifie_date','del_flag',];
}
