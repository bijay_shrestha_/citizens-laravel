<?php



Route::group(array('prefix'=>'admin/','module'=>'department_contact','middleware' => ['web','auth'], 'namespace' => 'App\modules\department_contact\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('department_contacts/','AdminDepartment_contactController@index')->name('admin.department_contacts');
    Route::post('department_contacts/getdepartment_contactsJson','AdminDepartment_contactController@getdepartment_contactsJson')->name('admin.department_contacts.getdatajson');
    Route::get('department_contacts/create','AdminDepartment_contactController@create')->name('admin.department_contacts.create');
    Route::post('department_contacts/store','AdminDepartment_contactController@store')->name('admin.department_contacts.store');
    Route::get('department_contacts/show/{id}','AdminDepartment_contactController@show')->name('admin.department_contacts.show');
    Route::get('department_contacts/edit/{id}','AdminDepartment_contactController@edit')->name('admin.department_contacts.edit');
    Route::match(['put', 'patch'], 'department_contacts/update/{id}','AdminDepartment_contactController@update')->name('admin.department_contacts.update');
    Route::get('department_contacts/delete/{id}', 'AdminDepartment_contactController@destroy')->name('admin.department_contacts.edit');
    Route::get('department_contacts/change/{text}/{status}/{id}', 'AdminDepartment_contactController@change');

});




Route::group(array('module'=>'department_contact','namespace' => 'App\Modules\department_contact\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('department_contacts/','Department_contactController@index')->name('department_contacts');
    
});