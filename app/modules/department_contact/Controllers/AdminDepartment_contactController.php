<?php

namespace App\modules\department_contact\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\department_contact\Model\Department_contact;
use Carbon\Carbon;

class AdminDepartment_contactController extends Controller
{
    /**
     * Display a listing of the resource. //DEPARTMENT_CONTACTS
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Department_contact';
        return view("department_contact::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getdepartment_contactsJson(Request $request)
    {
        $department_contact = new Department_contact;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $department_contact->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('id', "DSC")->get();

        // Display limited list        
        $rows = $department_contact->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->orderBy('id', "DSC")->get();

        //To count the total values present
        $total = $department_contact->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Department_contact | Create';
        $branches = DB::table('tbl_branches')->get()->toArray();
        return view("department_contact::add",compact('page', 'branches'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $datafordepartment = $data;
        $datafordepartment['created_by'] = Auth::user()->id;
        $datafordepartment['created_date'] = new Carbon();
        $datafordepartment['del_flag'] = 0;
        $datafordepartment = check_case($datafordepartment, 'create');


        $success = Department_contact::Insert($datafordepartment);
        $department_contacts = Department_contact::all();
        $department_contact_id = $department_contacts[count($department_contacts)-1]['id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "DEPARTMENT_CONTACTS", 'file_id' => $department_contact_id]); //create acrivity log     

        return redirect()->route('admin.department_contacts')->with('success', "Data inserted successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department_contact::findOrFail($id);
        $page['title'] = 'Department_contact | Update';
        $branches = DB::table('tbl_branches')->get()->toArray();
        return view("department_contact::edit",compact('page','department', 'branches'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $datafordepartment = $data;
        $datafordepartment['modified_by'] = Auth::user()->id;
        $datafordepartment['modifie_date'] = new Carbon;
        $datafordepartment = check_case($datafordepartment, 'edit');
        DB::table('tbl_department_contacts')->where('id', $id)->update($datafordepartment);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "DEPARTMENT_CONTACTS", 'file_id' => $id]); //create acrivity log     

        return redirect()->route('admin.department_contacts')->with('success', "Data updated successfully.");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_department_contacts', 'id', $id);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "DEPARTMENT_CONTACTS", 'file_id' => $id]); //create acrivity log     
        return redirect()->route('admin.department_contacts')->with('success', "Data deleted successfully.");
        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_department_contacts', 'id');
        return redirect()->route('admin.department_contacts')->with('success', "Data updated successfully.");
    }
}
