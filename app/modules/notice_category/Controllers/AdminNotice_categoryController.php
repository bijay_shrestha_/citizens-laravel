<?php

namespace App\modules\notice_category\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\notice_category\Model\Notice_category;
use Carbon\Carbon;

class AdminNotice_categoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Notice Category';
        return view("notice_category::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getnotice_categoriesJson(Request $request)
    {
        $notice_category = new Notice_category;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $notice_category->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('id', "DSC")->get();

        // Display limited list        
        $rows = $notice_category->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->orderBy('id', "DSC")->where('del_flag',0)->get();

        //To count the total values present
        $total = $notice_category->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Notice Category | Create';
        return view("notice_category::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $datafor_categories = $data;
        $datafor_categories['sequence'] = $datafor_categories['sequence'] ? $datafor_categories['sequence']: 0;
        $datafor_categories['del_flag'] = 0;
        $datafor_categories['added_by'] = Auth::user()->id;
        $datafor_categories['added_date'] = new Carbon();
        $datafor_categories = check_case($datafor_categories, 'create');        
        $success = Notice_category::Insert($datafor_categories);        
        return redirect()->route('admin.notice_categories')->with('success', "Data added successfully.");



        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notice_category = Notice_category::findOrFail($id);
        $page['title'] = 'Notice Category | Update';
        return view("notice_category::edit",compact('page','notice_category'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $datafor_categories = $data;
        $datafor_categories['sequence'] = $datafor_categories['sequence'] ? $datafor_categories['sequence']: 0;
        $datafor_categories['modified_by'] = Auth::user()->id;
        $datafor_categories['modified_date'] = new Carbon();
        $datafor_categories = check_case($datafor_categories, 'edit');        
        $success = DB::table('tbl_notice_categories')->where('id', $id)->update($datafor_categories);        
        return redirect()->route('admin.notice_categories')->with('success', "Data updated successfully.");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_notice_categories', 'id', $id, 'deleted_by', 'deleted_date');
        return redirect()->route('admin.notice_categories')->with('success', "Data updated successfully.");

        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_notice_categories', 'id', 'deleted_by', 'deleted_date');
        return redirect()->route('admin.notice_categories')->with('success', "Data updated Successfully.");
    }

}
