<?php

namespace App\Modules\Notice_category\Model;


use Illuminate\Database\Eloquent\Model;

class Notice_category extends Model
{
    public  $table = 'tbl_notice_categories';

    protected $fillable = ['id','category_name','sequence','approvel_status','approved','status','added_date','added_by','modified_date','modified_by','deleted_date','deleted_by','del_flag',];
}
