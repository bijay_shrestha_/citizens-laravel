<?php



Route::group(array('prefix'=>'admin/','module'=>'notice_category','middleware' => ['web','auth'], 'namespace' => 'App\modules\notice_category\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('notice_categories/','AdminNotice_categoryController@index')->name('admin.notice_categories');
    Route::post('notice_categories/getnotice_categoriesJson','AdminNotice_categoryController@getnotice_categoriesJson')->name('admin.notice_categories.getdatajson');
    Route::get('notice_categories/create','AdminNotice_categoryController@create')->name('admin.notice_categories.create');
    Route::post('notice_categories/store','AdminNotice_categoryController@store')->name('admin.notice_categories.store');
    Route::get('notice_categories/show/{id}','AdminNotice_categoryController@show')->name('admin.notice_categories.show');
    Route::get('notice_categories/edit/{id}','AdminNotice_categoryController@edit')->name('admin.notice_categories.edit');
    Route::match(['put', 'patch'], 'notice_categories/update/{id}','AdminNotice_categoryController@update')->name('admin.notice_categories.update');
    Route::get('notice_categories/delete/{id}', 'AdminNotice_categoryController@destroy')->name('admin.notice_categories.edit');
    Route::get('notice_categories/change/{text}/{status}/{id}', 'AdminNotice_categoryController@change');

});




Route::group(array('module'=>'notice_category','namespace' => 'App\modules\notice_category\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('notice_categories/','Notice_categoryController@index')->name('notice_categories');
    
});