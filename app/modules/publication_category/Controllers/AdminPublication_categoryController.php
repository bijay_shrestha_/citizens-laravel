<?php

namespace App\modules\publication_category\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\publication_category\Model\Publication_category;
use Carbon\Carbon;

class AdminPublication_categoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Publication Category';
        return view("publication_category::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getpublication_categoriesJson(Request $request)
    {
        $publication_category = new Publication_category;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $publication_category->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('category_id', "DSC")->get();

        // Display limited list        
        $rows = $publication_category->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->orderBy('category_id', "DSC")->get();

        //To count the total values present
        $total = $publication_category->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Publication Category | Create';
        return view("publication_category::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $datafor_publication_cat = $data;
        $datafor_publication_cat['del_flag'] = 0;
        $datafor_publication_cat['parent_id'] = 0; 
        $datafor_publication_cat['sequence'] = ($datafor_publication_cat['sequence'] == null) ? "0":$datafor_publication_cat['sequence'];
        $datafor_publication_cat['added_by'] = Auth::user()->id;
        $datafor_publication_cat['added_date'] = new Carbon();
        $datafor_publication_cat = check_case($datafor_publication_cat, 'create');
        $success = Publication_category::Insert($datafor_publication_cat);
        $publication_categories = Publication_category::all();
        $publication_category_id = $publication_categories[count($publication_categories)-1]['category_id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "PUBLICATION_CATEGORIES", 'file_id' => $publication_category_id]); //create acrivity log     
        return redirect()->route('admin.publication_categories')->with('success', "Data added successfully");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $publication_category = Publication_category::where('category_id', $id)->firstOrFail();
        $page['title'] = 'Publication Category | Update';
        return view("publication_category::edit",compact('page','publication_category'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $datafor_publication_cat = $data;
        $datafor_publication_cat['sequence'] = ($datafor_publication_cat['sequence'] == null) ? "0":$datafor_publication_cat['sequence'];
        $datafor_publication_cat['modified_date'] = new Carbon();
        $datafor_publication_cat = check_case($datafor_publication_cat, 'edit');
//        dd($datafor_publication_cat);
        DB::table('tbl_publication_categories')->where('category_id', $id)->update($datafor_publication_cat);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "PUBLICATION_CATEGORIES", 'file_id' => $id]); //create activity log
        return redirect()->route('admin.publication_categories')->with('success', "Data updated successfully");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_publication_categories', 'category_id', $id, 'deleted_by', 'deleted_date');
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "PUBLICATION_CATEGORIES", 'file_id' => $id]); //create acrivity log             
        return redirect()->route('admin.publication_categories')->with('success', "Data deleted successfully.");
        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_publication_categories', 'category_id', 'deleted_by', 'deleted_date');
        return redirect()->route('admin.publication_categories')->with('success', "Data updated successfully.");
    }

}
