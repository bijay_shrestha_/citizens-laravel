<?php

namespace App\Modules\Publication_category\Model;


use Illuminate\Database\Eloquent\Model;

class Publication_category extends Model
{
    public  $table = 'tbl_publication_categories';

    protected $fillable = ['category_id','parent_id','category_name','sequence','approvel_status','approved','added_date','added_by','modified_date','deleted_date','deleted_by','del_flag','status',];
}
