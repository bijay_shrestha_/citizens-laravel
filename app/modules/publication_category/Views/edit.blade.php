@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Publication Categories   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.publication_categories') }}">Publication Categories</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.publication_categories.update', $publication_category->category_id) }}"  method="post">
                <div class="box-body">    
                    {{method_field('PATCH')}}            
                        <input type="hidden" value = "{{$publication_category->category_id}}"  name="category_id" id="category_id" class="form-control" >
                    
                    <div class="form-group">
                        <label for="category_name">Category Name (*)</label><input type="text" value = "{{$publication_category->category_name}}"  name="category_name" required id="category_name" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="sequence">Sequence</label><input type="number" value = "{{$publication_category->sequence}}"  name="sequence" id="sequence" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="status" id="status1" @if($publication_category->status == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="status" id="status0" @if($publication_category->status == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.publication_categories') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
