<?php



Route::group(array('prefix'=>'admin/','module'=>'publication_category','middleware' => ['web','auth'], 'namespace' => 'App\modules\publication_category\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('publication_categories/','AdminPublication_categoryController@index')->name('admin.publication_categories');
    Route::post('publication_categories/getpublication_categoriesJson','AdminPublication_categoryController@getpublication_categoriesJson')->name('admin.publication_categories.getdatajson');
    Route::get('publication_categories/create','AdminPublication_categoryController@create')->name('admin.publication_categories.create');
    Route::post('publication_categories/store','AdminPublication_categoryController@store')->name('admin.publication_categories.store');
    Route::get('publication_categories/show/{id}','AdminPublication_categoryController@show')->name('admin.publication_categories.show');
    Route::get('publication_categories/edit/{id}','AdminPublication_categoryController@edit')->name('admin.publication_categories.edit');
    Route::match(['put', 'patch'], 'publication_categories/update/{id}','AdminPublication_categoryController@update')->name('admin.publication_categories.update');
    Route::get('publication_categories/delete/{id}', 'AdminPublication_categoryController@destroy')->name('admin.publication_categories.edit');
    Route::get('publication_categories/change/{text}/{status}/{id}', 'AdminPublication_categoryController@change');
});




Route::group(array('module'=>'publication_category','namespace' => 'App\modules\publication_category\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('publication_categories/','Publication_categoryController@index')->name('publication_categories');
    
});