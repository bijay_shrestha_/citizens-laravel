<?php

namespace App\Modules\Loan_interest_rate\Model;


use Illuminate\Database\Eloquent\Model;

class Loan_interest_rate extends Model
{
    public  $table = 'tbl_loan_interest_rates';

    protected $fillable = ['rate_id','loan_id','rate_name','general_per_annum_min','general_per_annum_max','prime_a_category_min','prime_a_category_max','prime_b_category_min','prime_b_category_max','notes','status','added_by','deleted_by','deleted_date','del_flag','added_date','modified_date',];
}
