<?php



Route::group(array('prefix'=>'admin/','module'=>'loan_interest_rate','middleware' => ['web','auth'], 'namespace' => 'App\modules\loan_interest_rate\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('loan_interest_rates/','AdminLoan_interest_rateController@index')->name('admin.loan_interest_rates');
    Route::post('loan_interest_rates/getloan_interest_ratesJson','AdminLoan_interest_rateController@getloan_interest_ratesJson')->name('admin.loan_interest_rates.getdatajson');
    Route::get('loan_interest_rates/create','AdminLoan_interest_rateController@create')->name('admin.loan_interest_rates.create');
    Route::post('loan_interest_rates/store','AdminLoan_interest_rateController@store')->name('admin.loan_interest_rates.store');
    Route::get('loan_interest_rates/show/{id}','AdminLoan_interest_rateController@show')->name('admin.loan_interest_rates.show');
    Route::get('loan_interest_rates/edit/{id}','AdminLoan_interest_rateController@edit')->name('admin.loan_interest_rates.edit');
    Route::match(['put', 'patch'], 'loan_interest_rates/update/{id}','AdminLoan_interest_rateController@update')->name('admin.loan_interest_rates.update');
    Route::get('loan_interest_rates/delete/{id}', 'AdminLoan_interest_rateController@destroy')->name('admin.loan_interest_rates.edit');
});




Route::group(array('module'=>'loan_interest_rate','namespace' => 'App\modules\loan_interest_rate\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('loan_interest_rates/','Loan_interest_rateController@index')->name('loan_interest_rates');
    
});