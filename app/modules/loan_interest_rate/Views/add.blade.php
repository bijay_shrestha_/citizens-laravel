@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Loan_interest_rates   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.loan_interest_rates') }}">tbl_loan_interest_rates</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.loan_interest_rates.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                                    <label for="rate_id">Rate_id</label><input type="text" name="rate_id" id="rate_id" class="form-control" ></div><div class="form-group">
                                    <label for="loan_id">Loan_id</label><input type="text" name="loan_id" id="loan_id" class="form-control" ></div><div class="form-group">
                                    <label for="rate_name">Rate_name</label><input type="text" name="rate_name" id="rate_name" class="form-control" ></div><div class="form-group">
                                    <label for="general_per_annum_min">General_per_annum_min</label><input type="text" name="general_per_annum_min" id="general_per_annum_min" class="form-control" ></div><div class="form-group">
                                    <label for="general_per_annum_max">General_per_annum_max</label><input type="text" name="general_per_annum_max" id="general_per_annum_max" class="form-control" ></div><div class="form-group">
                                    <label for="prime_a_category_min">Prime_a_category_min</label><input type="text" name="prime_a_category_min" id="prime_a_category_min" class="form-control" ></div><div class="form-group">
                                    <label for="prime_a_category_max">Prime_a_category_max</label><input type="text" name="prime_a_category_max" id="prime_a_category_max" class="form-control" ></div><div class="form-group">
                                    <label for="prime_b_category_min">Prime_b_category_min</label><input type="text" name="prime_b_category_min" id="prime_b_category_min" class="form-control" ></div><div class="form-group">
                                    <label for="prime_b_category_max">Prime_b_category_max</label><input type="text" name="prime_b_category_max" id="prime_b_category_max" class="form-control" ></div><div class="form-group">
                                    <label for="notes">Notes</label><input type="text" name="notes" id="notes" class="form-control" ></div><div class="form-group">
                                    <label for="rate_status">Rate_status</label><input type="text" name="rate_status" id="rate_status" class="form-control" ></div><div class="form-group">
                                    <label for="created_by">Created_by</label><input type="text" name="created_by" id="created_by" class="form-control" ></div><div class="form-group">
                                    <label for="deleted_by">Deleted_by</label><input type="text" name="deleted_by" id="deleted_by" class="form-control" ></div><div class="form-group">
                                    <label for="deleted_at">Deleted_at</label><input type="text" name="deleted_at" id="deleted_at" class="form-control" ></div><div class="form-group">
                                    <label for="del_flag">Del_flag</label><input type="text" name="del_flag" id="del_flag" class="form-control" ></div><div class="form-group">
                                    <label for="created_at">Created_at</label><input type="text" name="created_at" id="created_at" class="form-control" ></div><div class="form-group">
                                    <label for="updated_at">Updated_at</label><input type="text" name="updated_at" id="updated_at" class="form-control" ></div>
<input type="hidden" name="id" id="id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.loan_interest_rates') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
