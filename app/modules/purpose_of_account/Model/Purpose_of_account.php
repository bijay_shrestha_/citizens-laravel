<?php

namespace App\Modules\Purpose_of_account\Model;


use Illuminate\Database\Eloquent\Model;

class Purpose_of_account extends Model
{
    public  $table = 'tbl_purpose_of_account';

    protected $fillable = ['id','purpose_of_account','show_or_hide','del_flag',];
}
