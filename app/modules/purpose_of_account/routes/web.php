<?php



Route::group(array('prefix'=>'admin/','module'=>'purpose_of_account','middleware' => ['web','auth'], 'namespace' => 'App\modules\purpose_of_account\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('purpose_of_accounts/','AdminPurpose_of_accountController@index')->name('admin.purpose_of_accounts');
    Route::post('purpose_of_accounts/getpurpose_of_accountsJson','AdminPurpose_of_accountController@getpurpose_of_accountsJson')->name('admin.purpose_of_accounts.getdatajson');
    Route::get('purpose_of_accounts/create','AdminPurpose_of_accountController@create')->name('admin.purpose_of_accounts.create');
    Route::post('purpose_of_accounts/store','AdminPurpose_of_accountController@store')->name('admin.purpose_of_accounts.store');
    Route::get('purpose_of_accounts/show/{id}','AdminPurpose_of_accountController@show')->name('admin.purpose_of_accounts.show');
    Route::get('purpose_of_accounts/edit/{id}','AdminPurpose_of_accountController@edit')->name('admin.purpose_of_accounts.edit');
    Route::match(['put', 'patch'], 'purpose_of_accounts/update/{id}','AdminPurpose_of_accountController@update')->name('admin.purpose_of_accounts.update');
    Route::get('purpose_of_accounts/delete/{id}', 'AdminPurpose_of_accountController@destroy')->name('admin.purpose_of_accounts.edit');
});




Route::group(array('module'=>'purpose_of_account','namespace' => 'App\modules\purpose_of_account\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('purpose_of_accounts/','Purpose_of_accountController@index')->name('purpose_of_accounts');
    
});