@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Purpose Of Accounts   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.purpose_of_accounts') }}">Purpose Of Account</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.purpose_of_accounts.update', $purpose_of_account->id) }}"  method="post">
                <div class="box-body">    
                    {{method_field('PATCH')}}            
                
                    <div class="form-group">
                        <label for="purpose_of_account">Purpose Of Account</label><input type="text" value = "{{$purpose_of_account->purpose_of_account}}"  name="purpose_of_account" id="purpose_of_account" class="form-control" required >
                    </div>

                    <div class="form-group">
                        <label for="show_or_hide">Show Or Hide</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="show_or_hide" id="show_or_hide1" @if($purpose_of_account->show_or_hide == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="show_or_hide" id="show_or_hide0" @if($purpose_of_account->show_or_hide == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>
<input type="hidden" name="id" id="id" value = "{{$purpose_of_account->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.purpose_of_accounts') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
