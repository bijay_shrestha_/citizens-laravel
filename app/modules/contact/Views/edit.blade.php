@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Contacts   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.contacts') }}">tbl_contacts</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.contacts.update') }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                                    <label for="contact_id">Contact_id</label><input type="text" value = "{{$contact->contact_id}}"  name="contact_id" id="contact_id" class="form-control" ></div><div class="form-group">
                                    <label for="sector">Sector</label><input type="text" value = "{{$contact->sector}}"  name="sector" id="sector" class="form-control" ></div><div class="form-group">
                                    <label for="branch">Branch</label><input type="text" value = "{{$contact->branch}}"  name="branch" id="branch" class="form-control" ></div><div class="form-group">
                                    <label for="name">Name</label><input type="text" value = "{{$contact->name}}"  name="name" id="name" class="form-control" ></div><div class="form-group">
                                    <label for="email">Email</label><input type="text" value = "{{$contact->email}}"  name="email" id="email" class="form-control" ></div><div class="form-group">
                                    <label for="contact_no">Contact_no</label><input type="text" value = "{{$contact->contact_no}}"  name="contact_no" id="contact_no" class="form-control" ></div><div class="form-group">
                                    <label for="message">Message</label><input type="text" value = "{{$contact->message}}"  name="message" id="message" class="form-control" ></div><div class="form-group">
                                    <label for="reason">Reason</label><input type="text" value = "{{$contact->reason}}"  name="reason" id="reason" class="form-control" ></div><div class="form-group">
                                    <label for="date">Date</label><input type="text" value = "{{$contact->date}}"  name="date" id="date" class="form-control" ></div><div class="form-group">
                                    <label for="country">Country</label><input type="text" value = "{{$contact->country}}"  name="country" id="country" class="form-control" ></div><div class="form-group">
                                    <label for="address">Address</label><input type="text" value = "{{$contact->address}}"  name="address" id="address" class="form-control" ></div><div class="form-group">
                                    <label for="post_code">Post_code</label><input type="text" value = "{{$contact->post_code}}"  name="post_code" id="post_code" class="form-control" ></div><div class="form-group">
                                    <label for="added_by">Added_by</label><input type="text" value = "{{$contact->added_by}}"  name="added_by" id="added_by" class="form-control" ></div><div class="form-group">
                                    <label for="added_date">Added_date</label><input type="text" value = "{{$contact->added_date}}"  name="added_date" id="added_date" class="form-control" ></div><div class="form-group">
                                    <label for="modified_date">Modified_date</label><input type="text" value = "{{$contact->modified_date}}"  name="modified_date" id="modified_date" class="form-control" ></div><div class="form-group">
                                    <label for="del_flag">Del_flag</label><input type="text" value = "{{$contact->del_flag}}"  name="del_flag" id="del_flag" class="form-control" ></div><div class="form-group">
                                    <label for="deleted_by">Deleted_by</label><input type="text" value = "{{$contact->deleted_by}}"  name="deleted_by" id="deleted_by" class="form-control" ></div><div class="form-group">
                                    <label for="deleted_date">Deleted_date</label><input type="text" value = "{{$contact->deleted_date}}"  name="deleted_date" id="deleted_date" class="form-control" ></div><div class="form-group">
                                    <label for="status">Status</label><input type="text" value = "{{$contact->status}}"  name="status" id="status" class="form-control" ></div>
<input type="hidden" name="id" id="id" value = "{{$contact->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.contacts') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
