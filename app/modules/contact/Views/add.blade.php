@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Contacts   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.contacts') }}">tbl_contacts</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.contacts.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                                    <label for="contact_id">Contact_id</label><input type="text" name="contact_id" id="contact_id" class="form-control" ></div><div class="form-group">
                                    <label for="sector">Sector</label><input type="text" name="sector" id="sector" class="form-control" ></div><div class="form-group">
                                    <label for="branch">Branch</label><input type="text" name="branch" id="branch" class="form-control" ></div><div class="form-group">
                                    <label for="name">Name</label><input type="text" name="name" id="name" class="form-control" ></div><div class="form-group">
                                    <label for="email">Email</label><input type="text" name="email" id="email" class="form-control" ></div><div class="form-group">
                                    <label for="contact_no">Contact_no</label><input type="text" name="contact_no" id="contact_no" class="form-control" ></div><div class="form-group">
                                    <label for="message">Message</label><input type="text" name="message" id="message" class="form-control" ></div><div class="form-group">
                                    <label for="reason">Reason</label><input type="text" name="reason" id="reason" class="form-control" ></div><div class="form-group">
                                    <label for="date">Date</label><input type="text" name="date" id="date" class="form-control" ></div><div class="form-group">
                                    <label for="country">Country</label><input type="text" name="country" id="country" class="form-control" ></div><div class="form-group">
                                    <label for="address">Address</label><input type="text" name="address" id="address" class="form-control" ></div><div class="form-group">
                                    <label for="post_code">Post_code</label><input type="text" name="post_code" id="post_code" class="form-control" ></div><div class="form-group">
                                    <label for="added_by">Added_by</label><input type="text" name="added_by" id="added_by" class="form-control" ></div><div class="form-group">
                                    <label for="added_date">Added_date</label><input type="text" name="added_date" id="added_date" class="form-control" ></div><div class="form-group">
                                    <label for="modified_date">Modified_date</label><input type="text" name="modified_date" id="modified_date" class="form-control" ></div><div class="form-group">
                                    <label for="del_flag">Del_flag</label><input type="text" name="del_flag" id="del_flag" class="form-control" ></div><div class="form-group">
                                    <label for="deleted_by">Deleted_by</label><input type="text" name="deleted_by" id="deleted_by" class="form-control" ></div><div class="form-group">
                                    <label for="deleted_date">Deleted_date</label><input type="text" name="deleted_date" id="deleted_date" class="form-control" ></div><div class="form-group">
                                    <label for="status">Status</label><input type="text" name="status" id="status" class="form-control" ></div>
<input type="hidden" name="id" id="id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.contacts') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
