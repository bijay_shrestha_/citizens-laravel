@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Contacts		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Contacts</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.contacts.create') }}" class="btn bg-green waves-effect"  title="create">Create</a>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="contact-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								<th >Contact_id</th>
<th >Sector</th>
<th >Branch</th>
<th >Name</th>
<th >Email</th>
<th >Contact_no</th>
<th >Message</th>
<th >Reason</th>
<th >Date</th>
<th >Country</th>
<th >Address</th>
<th >Post_code</th>
<th >Added_by</th>
<th >Added_date</th>
<th >Modified_date</th>
<th >Del_flag</th>
<th >Deleted_by</th>
<th >Deleted_date</th>
<th >Status</th>

								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#contact-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.contacts.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
					{ data: "contact_id",name: "contact_id"},
            { data: "sector",name: "sector"},
            { data: "branch",name: "branch"},
            { data: "name",name: "name"},
            { data: "email",name: "email"},
            { data: "contact_no",name: "contact_no"},
            { data: "message",name: "message"},
            { data: "reason",name: "reason"},
            { data: "date",name: "date"},
            { data: "country",name: "country"},
            { data: "address",name: "address"},
            { data: "post_code",name: "post_code"},
            { data: "added_by",name: "added_by"},
            { data: "added_date",name: "added_date"},
            { data: "modified_date",name: "modified_date"},
            { data: "del_flag",name: "del_flag"},
            { data: "deleted_by",name: "deleted_by"},
            { data: "deleted_date",name: "deleted_date"},
            { data: "status",name: "status"},
            
					{ data: function(data,b,c,table) { 
					var buttons = '';

					buttons += "<a class='btn bg-red waves-effect' href='"+site_url+"/edit/"+data.id+"' type='button' >Edit</a>&nbsp"; 

					buttons += "<a href='"+site_url+"/delete/"+data.id+"' class='btn bg-red waves-effect' >Delete</a>";

					return buttons;
					}, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
