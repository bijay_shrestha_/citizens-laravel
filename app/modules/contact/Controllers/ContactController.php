<?php

namespace App\modules\contact\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\modules\contact\Model\Contact;
use Illuminate\Support\Facades\Schema;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'contacts';
        $banners=DB::table('tbl_banners')->where('banner_collection_id',137)->get();
        $banners = objToArray($banners);
        $faqs = get_lists('tbl_faq');
        // $news = get_lists('tbl_inquiries',null, 'sequence', 'ASC', 4);
        $branches_inside=DB::table('tbl_branches')->where([['del_flag',0],['for_section','Bank Branches'],['location','INSIDE']])->orderBy('name','ASC')->get();
        $branches_outside=DB::table('tbl_branches')->where([['del_flag',0],['for_section','Bank Branches'],['location','OUTSIDE']])->orderBy('name','ASC')->get();
		$inquiries =DB::table('tbl_inquiries')->where([['status',1],['del_flag',0]])->orderBy('sequence','ASC')->get();
        return view("front.contact.contact",compact('page','banners','faqs','branches_inside','branches_outside','inquiries'));
       
    // 	if($this->form_validation->run()===FALSE)
	// 	{
	// 	    	$data['header'] 	= "Contact Us";
    //     		$data['view_page'] 	= 'contact/contact';
    //     		$this->load->view($this->_container,$data);
	// 	}
	// 	else
	// 	{	
		    	    
		    
    //     	$capta = $this->input->post('g-recaptcha-response');
    // // 		print_r($capta); exit;
    // 		$google_secret = '6LfkgCgUAAAAAACvfH_gc2S4lYfWOqwR98_vvGj7';
    // // 		$verifiedRecaptcha = curl -d "param1=value1&param2=value2" -X POST "http://localhost:3000/data" ;
    // 		$verifiedRecaptcha = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$google_secret.'&response='.$capta);
    
    //         $verResponseData = json_decode($verifiedRecaptcha);
        
    //         if(!$verResponseData->success)
    //         {
    //             $this->session->set_userdata('msg','Please Verify You Are Not A Bot');
    //             redirect($_SERVER['HTTP_REFERER']);
    //         }
    //         else
    //         {
                
          
		    
	// 		$data = $this->_get_posted_data();
	// 		$result = $this->contact_model->insert('CONTACTS',$data);
			
	// 		if($result){
			
	// 		$this->_send_mail();
	// 		flashMsg('success','<div class="alert alert-danger">Thank you for writing in to us. Your query has been forwarded to the concerned Unit.</div>');			
			
	// 		}
	// 		else{
				
	// 			flashMsg('failure','Sorry there was some error while sending your message.');			
	// 		}
	// 		redirect(site_url('contact'));			
    //         }
        }
        
        function chief_information(){
                $banners=DB::table('tbl_banners')->where('banner_collection_id',110)->get();
                $view_page='contact/chief';
                return $banners;
        }


        function _send_mail()
        {
                $this->load->library('email');
    
                $subject="Contact from :: " .$this->input->post('name');
        $from_email =  $this->input->post('email');
                $message="The following person has contacted";
                $message.="<br/>=====================================";
                $message.="<br/>Sender Name: " . $this->input->post('name');
                $message.="<br/>Sender Email: " . $this->input->post('email');
                $message.="<br/>Sender Message: " . $this->input->post('reason');			
                $message.="<br/>Sender Message: " . $this->input->post('message');
                                        
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = TRUE;
                $config['mailtype'] = 'html';
                // $config['protocol'] = 'smtp';
                // $config['smtp_host'] = 'smtp.vianet.com.np';
                $this->email->initialize($config);
                $this->email->clear(TRUE);
    
                $id = $this->input->post('sector');
    
                $inquiry = $this->inquiry_model->getInquiries(array('id'=>$id,'del_flag'=>0))->row_array(); 
                
                $emails = explode(',',$inquiry['sector_email']);
    
                foreach ($emails as $email) {
                    $email_to[] = $email;
                }
                
                $this->email->subject($subject);
                $this->email->message($message);
                $this->email->from($from_email);
    
                if(!empty($email_to))
                {
                    $this->email->to($email_to);	
                }	
                else
                {
                    $this->email->to($this->preference->item('automated_from_email'));
    
                }
                
                $this->email->send();	
                    
        }
        public function career()
        {
                $this->load->library('email');
                     $img=$this->do_upload();
                $files=$img['file_name'];
                $fpath=$img['file_path'].$files;
                $subject="Contact from :: " .$this->input->post('name');
                $from_email=$this->input->post('email');
                $message="The following person has contacted";
                $message.="<br/>=====================================";
                $message.="<br/>Sender Name: " . $this->input->post('name');
                $message.="<br/>Sender Email: " . $this->input->post('email');
                        
                $message.="<br/>Sender Message: " . $this->input->post('message');
                
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = TRUE;
                $config['mailtype'] = 'html';
    // 			 $config['protocol'] = 'smtp';
    // 			$config['smtp_host'] = 'smtp.vianet.com.np';
                $this->email->initialize($config);
                $this->email->clear();
    
              $this->email->attach($fpath);
        
                $this->email->from($from_email);
                 
                $this->email->to($this->preference->item('automated_from_email'));
                $this->email->subject($subject);
                $this->email->message($message);
                
                
                if($this->email->send()){
                
                
                flashMsg('success','<div class="alert alert-danger">Thank you for writing in to us. Your query has been forwarded to the concerned Unit.</div>');			
                
                }
                else{
                    
                    flashMsg('failure','Sorry there was some error while sending your message.');			
                }
                redirect(site_url('contact'));			
                
            
                
                // $this->email->send();
        }
    
        
    

	  //
    }
   


