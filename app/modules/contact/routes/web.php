<?php



Route::group(array('prefix'=>'admin/','module'=>'Contact','middleware' => ['web','auth'], 'namespace' => 'App\modules\contact\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('contacts/','AdminContactController@index')->name('admin.contacts');
    Route::post('contacts/getcontactsJson','AdminContactController@getcontactsJson')->name('admin.contacts.getdatajson');
    Route::get('contacts/create','AdminContactController@create')->name('admin.contacts.create');
    Route::post('contacts/store','AdminContactController@store')->name('admin.contacts.store');
    Route::get('contacts/show/{id}','AdminContactController@show')->name('admin.contacts.show');
    Route::get('contacts/edit/{id}','AdminContactController@edit')->name('admin.contacts.edit');
    Route::match(['put', 'patch'], 'contacts/update/{id}','AdminContactController@update')->name('admin.contacts.update');
    Route::get('contacts/delete/{id}', 'AdminContactController@destroy')->name('admin.contacts.edit');
});

Route::group(array('module'=>'Contact','namespace' => 'App\modules\contact\Controllers','middleware'=>'web'), function() {
    //Your routes belong to this module.
    Route::get('contacts/','ContactController@index')->name('contacts');
    Route::get('chief_information/','ContactController@chief_information')->name('chief_information');
    
});