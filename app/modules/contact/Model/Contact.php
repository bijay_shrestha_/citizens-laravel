<?php

namespace App\modules\contact\Model;


use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public  $table = 'tbl_contacts';

    protected $fillable = ['contact_id','sector','branch','name','email','contact_no','message','reason','date','country','address','post_code','added_by','added_date','modified_date','del_flag','deleted_by','deleted_date','status',];
}
