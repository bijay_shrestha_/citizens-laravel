<?php

namespace App\modules\executive\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\executive\Model\Executive;
use Carbon\Carbon;

class AdminExecutiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Executive';
        return view("executive::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getexecutivesJson(Request $request)
    {
        $executive = new Executive;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $executive->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('executive_id', "DSC")->get();

        // Display limited list        
        $rows = $executive->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->orderBy('executive_id', "DSC")->get();

        //To count the total values present
        $total = $executive->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Executive | Create';
        return view("executive::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        
        $data['added_by'] = Auth::user()->id;
        $data['added_date'] = new Carbon();
        $data['del_flag'] = 0;

        $datafor_e = $data;
        unset($datafor_e['upload_image']);
        $datafor_e = check_case($datafor_e, 'create');
        
        $success = Executive::Insert($datafor_e);
        $executives = Executive::all();
        $executive_id = $executives[count($executives)-1]['executive_id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "executive", 'file_id' => $executive_id]); //create acrivity log     
        
        return redirect()->route('admin.executives');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $executive = Executive::where('executive_id', $id)->firstOrFail();
        $page['title'] = 'Executive | Update';
        return view("executive::edit",compact('page','executive'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $executive = Executive::where('executive_id', $id)->firstOrFail();
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "executive", 'file_id' => $id]); //create activity log
        
        $datafor_e = $data;
        unset($datafor_e['upload_image']);      
       
        if (array_key_exists('delete_old_image', $data)) {
            if ((file_exists(public_path().'/uploads/executive/'. $executive->image_name))  && $executive->image_name) {
                unlink(public_path().'/uploads/executive/'. $executive->image_name);
                if (file_exists(public_path().'/uploads/executive/thumb/'. $executive->image_name)) {
                    unlink(public_path().'/uploads/executive/thumb/'. $executive->image_name);
                } 
            }
            if (!$data['image_name']) {
                $data['image_name'] = '';
                $datafor_e['image_name'] = '';
            }
            unset($datafor_e['delete_old_image']);
        } else {
            if (!$data['image_name']) {
                $data['image_name'] = $executive->image_name;
                $datafor_e['image_name'] = $executive->image_name;
            }
        }


        $datafor_e['modified_date'] = new Carbon();
        $datafor_e = check_case($datafor_e, 'edit');

        DB::table('tbl_executives')->where('executive_id', $id)->update($datafor_e);
        return redirect()->route('admin.executives')->with('success', "Data updated successfully.");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_executives', 'executive_id', $id, 'deleted_by', 'deleted_date');
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "executive", 'file_id' => $id]); //create acrivity log     

        return redirect()->route('admin.executives')->with('success', "Data deleted successfully.");

    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_executives', 'executive_id', 'deleted_by', 'deleted_date');
        return redirect()->route('admin.executives')->with('success', "Data updated successfully.");
    }
}
