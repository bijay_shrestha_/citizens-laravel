<?php

namespace App\modules\executive\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;

class ExecutiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($parameter)
    {
        
       $data = DB::table('pages')->where('page_title',"Like",$parameter)->get();
        $page['title'] = 'executives';

     
        $data['module'] = 'executive';
//      $this->db->where("((approvel_status = 'ok' AND approved = 'approved') OR (approvel_status ='delete' And approved='denied')) AND del_flag = 0");
//      $data['bod_directors'] = $this->executive_model->getExecutives(array('team_type'=>'BOD Committe'))->result_array();

        $data['bod_directors']=DB::table('tbl_executives')->where('team_type','BOD Committe')->get();

        //$this->db->where("((approvel_status = 'ok' AND approved = 'approved') OR (approvel_status ='delete' And approved='denied')) AND del_flag = 0");
        //$data['management_teams'] = $this->executive_model->getExecutives(array('team_type'=>'Management Team'))->result_array();
        
        $data['management_teams']=DB::table('tbl_executives')->where('team_type','Management Team')->get();
        //$this->db->where("((approvel_status = 'ok' AND approved = 'approved') OR (approvel_status ='delete' And approved='denied')) AND del_flag = 0");
       // $data['department_members'] = $this->executive_model->getExecutives(array('team_type'=>'Department'))->result_array();
       
        $data['department_members']=DB::table('tbl_executives')->where('team_type','Department')->get();

        $data['active'] = $parameter;
        
            //$data['executive_banners']=$this->banner_model->getBanners(array('banner_collection_id'=>10,'del_flag'=>0))->result_array();
            $data['executive_banners']=DB::table('tbl_banners')->where('banner_collection_id',10)->where('del_flag',0)->get();

            //$data['bod_banners']=$this->banner_model->getBanners(array('banner_collection_id'=>29,'del_flag'=>0))->result_array();
           
            $data['bod_banners']=DB::table('tbl_banners')->where('banner_collection_id',29)->where('del_flag',0)->get();

            //$data['departments']=$this->banner_model->getBanners(array('banner_collection_id'=>106,'del_flag'=>0))->result_array();
            $data['departments']=DB::table('tbl_banners')->where('banner_collection_id',106)->where('del_flag',0)->get();
        //$data['banners']=$this->banner_model->getBanners(array('banner_collection_id'=>10))->result_array();
        $data['banners']=DB::table('tbl_banners')->where('banner_collection_id',10)->where('del_flag',0)->get();
        $data['header']= "Home";
        return view("front.executive.team",compact('page','data'));

    }


}
