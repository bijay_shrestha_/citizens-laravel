@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Executives   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.executives') }}">Executives</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.executives.store') }}"  method="post">
                <div class="box-body">                
                                    <div class="form-group">
                                      <label>Team type (*)</label>
                                      <select name="team_type" id="team_type" class="form-control" required>
                                        <option value="">Select Team type</option>
                                        <option value="BOD Committe">BOD Committe</option>
                                        <option value="Management Team">Management Team</option>
                                        <option value="Senior Executive">Senior Executive</option>
                                        <option value="Department">Department</option>
                                      </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="salutation">Salutation (*)</label><input type="text" name="salutation" id="salutation" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="executive_name">Executive Name (*)</label><input type="text" name="executive_name" id="executive_name" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="designation">Designation</label>
                                        <input type="text" name="designation" id="designation" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="branch">Branch</label>
                                        <input type="text" name="branch" id="branch" class="form-control" >
                                    </div>

                                    <div class="form-group">
                                        <label for="image_name">Image </label><br>
                                        <label id="upload_image_name" style="display:none"></label>
                                        <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                                        <div id="image-status"></div>
                                        <input type="file" id="upload_image" name="upload_image" style="display:block"/>
                                        <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                                    </div>

                                    <div class="form-group">
                                        <label for="committe">Committe</label><input type="text" name="committe" id="committe" class="form-control" >
                                    </div>

                                    <div class="form-group">
                                        <label for="note">Note</label><textarea name="note" id="note" class="form-control my-editor" ></textarea>
                                    </div>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.executives') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
        <script type="text/javascript">
          var directory = 'executive';
    </script>
    @include('data_changing_script_create')
    @include('file_changing_script')

@endsection
