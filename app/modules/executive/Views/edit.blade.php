@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Executives   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.executives') }}">tbl_executives</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.executives.update', $executive->executive_id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                                    <input type="hidden" value = "{{$executive->executive_id}}"  name="executive_id" id="executive_id" class="form-control">
                                    
                                    <div class="form-group">
                                          <label>Team Type(*)</label>
                                            <select name="team_type" id="team_type" class="form-control" required>
                                              <option value="">Select Team type</option>
                                              <option value="BOD Committe" <?php if($executive->team_type == "BOD Committe"){echo "selected";}?>>BOD Committe</option>
                                              <option value="Management Team" <?php if($executive->team_type == "Management Team"){echo "selected";}?>>Management Team</option>
                                              <option value="Senior Executive" <?php if($executive->team_type == "Senior Executive"){echo "selected";}?>>Senior Executive</option>
                                              <option value="Department" <?php if($executive->team_type == "Department"){echo "selected";}?>>Department</option>
                                          </select>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="salutation">Salutation (*)</label>
                                        <input type="text" value = "{{$executive->salutation}}"  name="salutation" id="salutation" class="form-control" required>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="executive_name">Executive Name (*)</label>
                                        <input type="text" value = "{{$executive->executive_name}}"  name="executive_name" id="executive_name" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="designation">Designation</label>
                                        <input type="text" value = "{{$executive->designation}}"  name="designation" id="designation" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="branch">Branch</label><input type="text" value = "{{$executive->branch}}"  name="branch" id="branch" class="form-control" >
                                    </div>
                                    <div class="form-group">
                                        <label for="image_name">Image </label><br>
                                        <label id="upload_image_name" style="display:none"></label>
                                        <div id="delete_old_image_requested"></div>
                                        <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                                        <div id="image-status"></div>
                                        <input type="file" id="upload_image" name="upload_image" @if (file_exists(public_path().'/uploads/executive/thumb/'. $executive->image_name) && $executive->image_name) style="display:none" @else style="display:block" @endif />
                                        <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                                        <div id="imagedata">
                                        @if (file_exists(public_path().'/uploads/executive/'. $executive->image_name) && $executive->image_name)                                   
                                            <img src="{{url('uploads/executive/thumb/'.$executive->image_name)}}">
                                            <a href="javascript:void(0)" id="change-image_option" title="Change Image">&nbsp; &nbsp; <img src="{{('/images/logo/cancel.png')}}" border="0"/> Change the old image</a>
                                        @endif                                
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="facebook">Facebook</label><input type="text" value = "{{$executive->facebook}}"  name="facebook" id="facebook" class="form-control" >
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="committe">Committe</label>
                                        <input type="text" value = "{{$executive->committe}}"  name="committe" id="committe" class="form-control" >
                                    </div>

                                    <div class="form-group">
                                        <label for="note">Note</label>
                                        <textarea name="note" id="note" class="form-control my-editor" >{!! $executive->note !!}</textarea>
                                    </div>

                                    
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.executives') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
     <script type="text/javascript">
        var directory = 'executive';
    </script>
@include('data_changing_script_edit')
@include('file_changing_script')

@endsection
