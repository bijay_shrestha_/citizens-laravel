<?php



Route::group(array('prefix'=>'admin/','module'=>'executive','middleware' => ['web','auth'], 'namespace' => 'App\modules\executive\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('executives/','AdminExecutiveController@index')->name('admin.executives');
    Route::post('executives/getexecutivesJson','AdminExecutiveController@getexecutivesJson')->name('admin.executives.getdatajson');
    Route::get('executives/create','AdminExecutiveController@create')->name('admin.executives.create');
    Route::post('executives/store','AdminExecutiveController@store')->name('admin.executives.store');
    Route::get('executives/show/{id}','AdminExecutiveController@show')->name('admin.executives.show');
    Route::get('executives/edit/{id}','AdminExecutiveController@edit')->name('admin.executives.edit');
    Route::match(['put', 'patch'], 'executives/update/{id}','AdminExecutiveController@update')->name('admin.executives.update');
    Route::get('executives/delete/{id}', 'AdminExecutiveController@destroy')->name('admin.executives.edit');
    Route::get('executives/change/{text}/{status}/{id}', 'AdminExecutiveController@change');
});




Route::group(array('module'=>'executive','namespace' => 'App\modules\executive\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('executives/index/{parameter}','ExecutiveController@index')->name('executives');
    
});