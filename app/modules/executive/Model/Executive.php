<?php

namespace App\Modules\Executive\Model;


use Illuminate\Database\Eloquent\Model;

class Executive extends Model
{
    public  $table = 'tbl_executives';

    protected $fillable = ['executive_id','team_type','salutation','executive_name','designation','branch','image_name','facebook','committe','note','approvel_status','approved','added_by','added_date','modified_date','del_flag','deleted_by','deleted_date',];
}
