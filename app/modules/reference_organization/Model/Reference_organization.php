<?php

namespace App\Modules\Reference_organization\Model;


use Illuminate\Database\Eloquent\Model;

class Reference_organization extends Model
{
    public  $table = 'tbl_reference_organization';

    protected $fillable = ['r_id','p_id','reference_1','post_1','organization_1','address_1','email_1','telephone_1','reference_2','post_2','organization_2','address_2','email_2','telephone',];
}
