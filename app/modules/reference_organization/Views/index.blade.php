@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Reference_organizations		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Reference_organizations</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.reference_organizations.create') }}" class="btn bg-green waves-effect"  title="create">Create</a>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="reference_organization-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								<th >R_id</th>
<th >P_id</th>
<th >Reference_1</th>
<th >Post_1</th>
<th >Organization_1</th>
<th >Address_1</th>
<th >Email_1</th>
<th >Telephone_1</th>
<th >Reference_2</th>
<th >Post_2</th>
<th >Organization_2</th>
<th >Address_2</th>
<th >Email_2</th>
<th >Telephone</th>

								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#reference_organization-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.reference_organizations.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
					{ data: "r_id",name: "r_id"},
            { data: "p_id",name: "p_id"},
            { data: "reference_1",name: "reference_1"},
            { data: "post_1",name: "post_1"},
            { data: "organization_1",name: "organization_1"},
            { data: "address_1",name: "address_1"},
            { data: "email_1",name: "email_1"},
            { data: "telephone_1",name: "telephone_1"},
            { data: "reference_2",name: "reference_2"},
            { data: "post_2",name: "post_2"},
            { data: "organization_2",name: "organization_2"},
            { data: "address_2",name: "address_2"},
            { data: "email_2",name: "email_2"},
            { data: "telephone",name: "telephone"},
            
					{ data: function(data,b,c,table) { 
					var buttons = '';

					buttons += "<a class='btn bg-red waves-effect' href='"+site_url+"/edit/"+data.id+"' type='button' >Edit</a>&nbsp"; 

					buttons += "<a href='"+site_url+"/delete/"+data.id+"' class='btn bg-red waves-effect' >Delete</a>";

					return buttons;
					}, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
