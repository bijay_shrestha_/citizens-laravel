@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Reference_organizations   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.reference_organizations') }}">tbl_reference_organization</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.reference_organizations.update') }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                                    <label for="r_id">R_id</label><input type="text" value = "{{$reference_organization->r_id}}"  name="r_id" id="r_id" class="form-control" ></div><div class="form-group">
                                    <label for="p_id">P_id</label><input type="text" value = "{{$reference_organization->p_id}}"  name="p_id" id="p_id" class="form-control" ></div><div class="form-group">
                                    <label for="reference_1">Reference_1</label><input type="text" value = "{{$reference_organization->reference_1}}"  name="reference_1" id="reference_1" class="form-control" ></div><div class="form-group">
                                    <label for="post_1">Post_1</label><input type="text" value = "{{$reference_organization->post_1}}"  name="post_1" id="post_1" class="form-control" ></div><div class="form-group">
                                    <label for="organization_1">Organization_1</label><input type="text" value = "{{$reference_organization->organization_1}}"  name="organization_1" id="organization_1" class="form-control" ></div><div class="form-group">
                                    <label for="address_1">Address_1</label><input type="text" value = "{{$reference_organization->address_1}}"  name="address_1" id="address_1" class="form-control" ></div><div class="form-group">
                                    <label for="email_1">Email_1</label><input type="text" value = "{{$reference_organization->email_1}}"  name="email_1" id="email_1" class="form-control" ></div><div class="form-group">
                                    <label for="telephone_1">Telephone_1</label><input type="text" value = "{{$reference_organization->telephone_1}}"  name="telephone_1" id="telephone_1" class="form-control" ></div><div class="form-group">
                                    <label for="reference_2">Reference_2</label><input type="text" value = "{{$reference_organization->reference_2}}"  name="reference_2" id="reference_2" class="form-control" ></div><div class="form-group">
                                    <label for="post_2">Post_2</label><input type="text" value = "{{$reference_organization->post_2}}"  name="post_2" id="post_2" class="form-control" ></div><div class="form-group">
                                    <label for="organization_2">Organization_2</label><input type="text" value = "{{$reference_organization->organization_2}}"  name="organization_2" id="organization_2" class="form-control" ></div><div class="form-group">
                                    <label for="address_2">Address_2</label><input type="text" value = "{{$reference_organization->address_2}}"  name="address_2" id="address_2" class="form-control" ></div><div class="form-group">
                                    <label for="email_2">Email_2</label><input type="text" value = "{{$reference_organization->email_2}}"  name="email_2" id="email_2" class="form-control" ></div><div class="form-group">
                                    <label for="telephone">Telephone</label><input type="text" value = "{{$reference_organization->telephone}}"  name="telephone" id="telephone" class="form-control" ></div>
<input type="hidden" name="id" id="id" value = "{{$reference_organization->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.reference_organizations') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
