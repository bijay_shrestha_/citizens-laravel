<?php



Route::group(array('prefix'=>'admin/','module'=>'Reference_organization','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Reference_organization\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('reference_organizations/','AdminReference_organizationController@index')->name('admin.reference_organizations');
    Route::post('reference_organizations/getreference_organizationsJson','AdminReference_organizationController@getreference_organizationsJson')->name('admin.reference_organizations.getdatajson');
    Route::get('reference_organizations/create','AdminReference_organizationController@create')->name('admin.reference_organizations.create');
    Route::post('reference_organizations/store','AdminReference_organizationController@store')->name('admin.reference_organizations.store');
    Route::get('reference_organizations/show/{id}','AdminReference_organizationController@show')->name('admin.reference_organizations.show');
    Route::get('reference_organizations/edit/{id}','AdminReference_organizationController@edit')->name('admin.reference_organizations.edit');
    Route::match(['put', 'patch'], 'reference_organizations/update/{id}','AdminReference_organizationController@update')->name('admin.reference_organizations.update');
    Route::get('reference_organizations/delete/{id}', 'AdminReference_organizationController@destroy')->name('admin.reference_organizations.edit');
});




Route::group(array('module'=>'Reference_organization','namespace' => 'App\Modules\Reference_organization\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('reference_organizations/','Reference_organizationController@index')->name('reference_organizations');
    
});