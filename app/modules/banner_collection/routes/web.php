<?php



Route::group(array('prefix'=>'admin/','module'=>'banner_collection','middleware' => ['web','auth'], 'namespace' => 'App\modules\banner_collection\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('banner_collections/','AdminBanner_collectionController@index')->name('admin.banner_collections');
    Route::post('banner_collections/getbanner_collectionsJson','AdminBanner_collectionController@getbanner_collectionsJson')->name('admin.banner_collections.getdatajson');
    Route::get('banner_collections/create','AdminBanner_collectionController@create')->name('admin.banner_collections.create');
    Route::post('banner_collections/store','AdminBanner_collectionController@store')->name('admin.banner_collections.store');
    Route::get('banner_collections/show/{id}','AdminBanner_collectionController@show')->name('admin.banner_collections.show');
    Route::get('banner_collections/edit/{id}','AdminBanner_collectionController@edit')->name('admin.banner_collections.edit');
    Route::match(['put', 'patch'], 'banner_collections/update/{id}','AdminBanner_collectionController@update')->name('admin.banner_collections.update');
    Route::get('banner_collections/delete/{id}', 'AdminBanner_collectionController@destroy')->name('admin.banner_collections.edit');
});




Route::group(array('module'=>'banner_collection','namespace' => 'App\modules\banner_collection\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('banner_collections/','Banner_collectionController@index')->name('banner_collections');
    
});