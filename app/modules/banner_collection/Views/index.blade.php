@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Banner Collections		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Banner Collections</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.banner_collections.create') }}" class="btn bg-green waves-effect"  title="create">Add Banner Collection</a>
					</div>
					<br>
					@if ($message = Session::get('success'))
					<div class="btn btn-success">
						{{$message}}
					</div>
					@endif
					<!-- /.box-header -->
					<div class="box-body">
						<table id="banner_collection-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
<th >Name</th>

								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		var config_url = "{{url('/')}}";
		$(function(){
			dataTable = $('#banner_collection-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.banner_collections.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
		    { data: "name",name: "name"},
            
					{ data: function(data,b,c,table) { 
					var buttons = '';

					buttons += "<a class='btn bg-blue waves-effect' href='"+config_url+"/admin/banners/create/"+data.banner_collection_id+"' type='button' ><i class='fa fa-plus-circle '></i> Add Banner</a>&nbsp;"; 

					buttons += "<a class='btn bg-blue waves-effect' href='"+site_url+"/show/"+data.banner_collection_id+"' type='button' ><i class='fa fa-align-justify'></i> My Banners</a>&nbsp;"; 

					buttons += "<a class='btn bg-green waves-effect' href='"+site_url+"/edit/"+data.banner_collection_id+"' type='button' ><i class='fa fa-edit'></i> Edit</a>&nbsp;"; 

					buttons += "<a href='"+site_url+"/delete/"+data.banner_collection_id+"' class='btn bg-red waves-effect' ><i class='fa fa-trash'></i>  Delete</a>";

					return buttons;
					}, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
