@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Banner Collections   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.banner_collections') }}">Banner Collections</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.banner_collections.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                        <label for="name">Name</label><input type="text" name="name" id="name" class="form-control" required >
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                            <label>
                            <input type="radio" value="1" name="status" id="status1" checked="checked" />Yes</label> 
                            <label><input type="radio" value="0" name="status" id="status0" />No</label>
                        </div>
                    </div>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.banner_collections') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
