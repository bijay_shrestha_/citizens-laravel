@extends('admin.layout.main')
@section('content')
 <style>
            #customers {
              font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
              border-collapse: collapse;
              width: 100%;
            }

            #customers td, #customers th {
              border: 1px solid #ddd;
              padding: 8px;
            }

            #customers tr:nth-child(even){background-color: #f2f2f2;}

            #customers tr:hover {background-color: #ddd;}

            #customers th {
              padding-top: 12px;
              padding-bottom: 12px;
              text-align: left;
              background-color: #3c8dbc;
              color: white;
            }
</style>
    <section class="content-header">
        <h1>
           Banner Collections
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.banner_collections') }}">Banner Collections</a></li>
            <li class="active">Show</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            @if (count($banners) > 0)
                <br><br><h3> {{$banner_collection->name}} &nbsp; &nbsp; &nbsp; <a class='btn bg-blue waves-effect' 
                    href='{{route("admin.banners.create",$banner_collection->banner_collection_id)}}' type='button' ><i class='fa fa-plus-circle '></i> Add Banner</a>
                </h3>
                <table id="customers">
                  <tr>
                    <th>Banner Image</th>
                    <th>Banner Logo</th>
                    <th>Banner Background</th>
                    <th>Action</th>
                  </tr>
                    @foreach ($banners as $banner)
                    <tr>

                        <td><img src="{{ url('/uploads/banner/'.$banner->image_name )}}" height="100" width="100"></td>
                        <td><img src="{{ url('/uploads/banner/'.$banner->logo )}}"  height="100" width="100"></</td>
                        <td><img src="{{ url('/uploads/banner/'.$banner->background )}}"  height="100" width="100"></</td>
                        <td><a class='btn bg-green waves-effect' href='{{route("admin.banners.edit", $banner->banner_id)}}' type='button' ><i class='fa fa-edit'></i> Edit</a> &nbsp;
                            <a class='btn bg-red waves-effect' href='{{url("admin/banners/delete", $banner->banner_id)}}' type='button' ><i class='fa fa-trash'></i> Delete</a> &nbsp;</td>

                  </tr>
                    @endforeach
                </table>
            @else 
                <br><br><h3> No Banner Images for "{{$banner_collection->name}}"  &nbsp; &nbsp; &nbsp;  <a class='btn bg-blue waves-effect' 
                    href='{{route("admin.banners.create",$banner_collection->banner_collection_id)}}' type='button' ><i class='fa fa-plus-circle '></i> Add Banner</a>
                </h3>
                <br><br>
            @endif

          
        </div>
    </section>
@endsection
