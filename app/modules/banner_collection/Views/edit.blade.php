@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Banner Collections   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.banner_collections') }}">Banner Collections</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.banner_collections.update', $banner_collection->banner_collection_id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                    <div class="form-group">
                        <label for="name">Name</label><input type="text" name="name" id="name" class="form-control" value="{{$banner_collection->name}}" required >
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                            <label>
                            <input type="radio" value="1" name="status" id="status1" @if($banner_collection->status == 1) checked="checked" @endif />Yes</label> 
                            <label><input type="radio" value="0" name="status" id="status0" @if($banner_collection->status == 0) checked="checked" @endif />No</label>
                        </div>
                    </div>
<input type="hidden" name="banner_collection_id" id="banner_collection_id" value = "{{$banner_collection->banner_collection_id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.banner_collections') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
