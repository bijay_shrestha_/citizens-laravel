<?php

namespace App\Modules\Banner_collection\Model;


use Illuminate\Database\Eloquent\Model;

class Banner_collection extends Model
{
    public  $table = 'tbl_banner_collections';

    protected $fillable = ['banner_collection_id','name','default','added_by','deleted_by','deleted_date','del_flag','status','added_date','modified_date' ];
}
