<?php

namespace App\modules\banner_collection\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\banner_collection\Model\Banner_collection;
use App\modules\banner\Model\Banner;
use Carbon\Carbon;

class AdminBanner_collectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Banner_collection';
        return view("banner_collection::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getbanner_collectionsJson(Request $request)
    {
        $banner_collection = new Banner_collection;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $banner_collection->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('banner_collection_id', 'DSC')->get();

        // Display limited list        
        $rows = $banner_collection->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->orderBy('banner_collection_id', 'DSC')->get();

        //To count the total values present
        $total = $banner_collection->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Banner Collection | Create';
        return view("banner_collection::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $data['added_by'] = Auth::user()->id;
        $data['added_date'] = new Carbon();
        $data['del_flag'] = 0;
        $success = Banner_collection::Insert($data);
        return redirect()->route('admin.banner_collections')->with('success', "Data added successfully");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $banner_collection = Banner_collection::where('banner_collection_id', $id)->firstOrfail();
        $banners = Banner::where('banner_collection_id', $id)->where('del_flag', 0)->get();
        $page['title'] = 'Banner Collection | Show';
        return view("banner_collection::show",compact('page','banner_collection', 'banners'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner_collection = Banner_collection::where('banner_collection_id', $id)->firstOrfail();
        $page['title'] = 'Banner Collection | Update';
        return view("banner_collection::edit",compact('page','banner_collection'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
//        $success = Banner_collection::where('banner_collection_id', $id)->update($data);
        $data['modified_date'] = new Carbon();
        DB::table('tbl_banner_collections')->where('banner_collection_id', $id)->update($data);
        return redirect()->route('admin.banner_collections')->with('success', "Data updated successfully");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_banner_collections', 'banner_collection_id', $id, 'deleted_by', 'deleted_date');
        return redirect()->route('admin.banner_collections')->with('success', "Data deleted successfully");

        //
    }
}
