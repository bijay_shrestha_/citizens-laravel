<?php

namespace App\modules\personal_detail\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\personal_detail\Model\Personal_detail;
use App\modules\education\Model\Education;
use App\modules\extra_course\Model\Extra_course;
use App\modules\preference_available\Model\Preference_available;
use App\modules\orgreference\Model\Orgreference;
use App\modules\reference_organization\Model\Reference_organization;
use App\modules\work_experience\Model\Work_experience;
use App\modules\working_experience\Model\Working_experience;
use App\Exports\personal_detail_info;
use App\Exports\individual_personal_details;
use Excel;
use Carbon\Carbon;

class AdminPersonal_detailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Personal_detail';
        return view("personal_detail::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getpersonal_detailsJson(Request $request)
    {
        $personal_detail = DB::table('tbl_personal_details');
        
        $where = $this->_get_search_param($request);
        //$where[2][0]="email";
        //return $where;
        
        // return $request;
        // For pagination
        $filterTotal = $personal_detail->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )
       ->leftjoin('tbl_preference_available', 'tbl_personal_details.p_id', '=', 'tbl_preference_available.p_id')
        ->select('tbl_personal_details.*','tbl_personal_details.email as p_email','tbl_preference_available.specialised_area as specialised_area','tbl_preference_available.preferred_post as preferred_post')
        ->where('del_flag',0)
        ->orderBy('tbl_personal_details.p_id', 'DSC')
        ->get();
        // Display limited list        
        $rows = $personal_detail->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('tbl_personal_details.del_flag',0)->orderBy('tbl_personal_details.p_id', 'DSC')->get();

        //To count the total values present
        $total = $personal_detail->where('tbl_personal_details.del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);
    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Personal_detail | Create';
        return view("personal_detail::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
    }

    public function get_highest_additional_course($details)
    {
        foreach($details as $key => $detail){
            $courses[] = DB::table('tbl_extra_course')->where('p_id', $detail['p_id'])->get();
//            $this->db->where('p_id',$detail['p_id']);
//            $courses[] = $this->db->get('tbl_extra_course')->result_array();            
        }
        $max = 1;
        foreach($courses as $cor)
        {
            $size = count($cor);
            if($max < $size)
            {
                $max = $size;
            }
        }
        //  echo '<pre>'; print_r($max); exit;
        return $max;
    }
    public function get_highest_work_experience($details)
    {
        foreach($details as $key => $detail){
            $workexp[] = DB::table('tbl_work_experience')->where('p_id', $detail['p_id'])->get();      
        }
        $max = 1;
        foreach($workexp as $cor)
        {
            $size = count($cor);
            if($max < $size)
            {
                $max = $size;
            }
        }
        //  echo '<pre>'; print_r($max); exit;
        return $max;
    }
    public function get_highest_reference($details)
    {
        foreach($details as $key => $detail){
            $reference[] = DB::table('tbl_orgreference')->where('p_id', $detail['p_id'])->get();      
        }
        $max = 1;
        foreach($reference as $cor)
        {
            $size = count($cor);
            if($max < $size)
            {
                $max = $size;
            }
        }
        //  echo '<pre>'; print_r($max); exit;
        return $max;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page['title'] = 'Personal_detail';
       // $personal_detail = Personal_detail::all();
        $personal_detail = Personal_detail::where('del_flag', 0)->where('p_id',$id)->get(); 
    
        $final_data=array(); 
        $education = Education::where('p_id',$id)->get();
        $extra_course=Extra_course::where('p_id',$id)->get();
        $preference_available=Preference_available::where('p_id',$id)->get();
        $reference_organization=Reference_organization::where('p_id',$id)->get();
        $orgreference=Orgreference::where('p_id',$id)->get();
        $work_experience=Work_experience::where('p_id',$id)->get();
       
        $data = array(
            'personal_detail'=>$personal_detail,
            'education'=>$education,
            'extra_course'=>$extra_course,
            'preference_available'=>$preference_available,
            'orgreference'=>$orgreference,
            'reference_organization'=>$reference_organization,
            'work_experience'=>$work_experience,
           
        );

        $page = array(
            'title'=>'individual information'
        );
         array_push($final_data,$data);
               
            //  return $data;
           return view("personal_detail::detail")->with('data',$final_data)->with('page',$page);
        // return view("personal_detail::detail")->with('data',$data)->with('page',$page);
        // return view("personal_detail::detail",compact('page','personal_detail','education','extra_course','preference_available','orgreference','work_experience'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $personal_detail = Personal_detail::findOrFail($id);
        $page['title'] = 'Personal_detail | Update';
        return view("personal_detail::edit",compact('page','personal_detail'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token');
        $success = Personal_detail::where('id', $id)->update($data);
        return redirect()->route('admin.personal_details');
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      
        $data = soft_delete('tbl_personal_details', 'p_id', $id);
       // DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "personal_details", 'file_id' => $id]); //create acrivity log
        return redirect()->route('admin.personal_details')->with('success', "Data deleted for trash.");
        //
    }

    public function get_personal_details()
    {
           $personal_detail = Personal_detail::where('del_flag', 0)->get(); // when one ko needed
           $highest_additional = $this->get_highest_additional_course($personal_detail);
           $highest = $this->get_highest_work_experience($personal_detail);
           $high_reference = $this->get_highest_reference($personal_detail);
            $final_data=array(); 
                $max_count=0;
            foreach($personal_detail as $pd)
            {
                $p_id= $pd->p_id;
                $education=Education::where('p_id',$p_id)->get();
                $extra_course=Extra_course::where('p_id',$p_id)->get();
                $preference_available=Preference_available::where('p_id',$p_id)->get();
                $orgreference=Orgreference::where('p_id',$p_id)->get();
                $reference_organization=Reference_organization::where('p_id',$p_id)->get();
                $work_experience=Work_experience::where('p_id',$p_id)->get();
                $working_experience=Working_experience::where('p_id',$p_id)->get();
                
                $data = array(
                    'personal_details'=>$pd,
                    'education'=>$education,
                    'extra_course'=>$extra_course,
                    'preference_available'=>$preference_available,
                    'orgreference'=>$orgreference,
                    'reference_organization'=>$reference_organization,
                    'work_experience'=>$work_experience,
                    'working_experience'=>$working_experience,
                );
                
               
                //array_push($data,$pd,$education,$extra_course,$preference_available,$orgreference,$reference_organization,$work_experience,$working_experience);  
                array_push($final_data,$data);

            
            //  $total_count=0;
            //     foreach($extra_course as $d)
            //     {   
            //         $total_count++;
            //     }
            //     if($max_count<$total_count)
            //     {
            //         $max_count=$total_count;
            //     }
            
    
            }
            $final_data['total_count']= $highest_additional;
            $final_data['total_work_experience']=$highest;
            $final_data['orgreference']=$high_reference;
            
            //return view("personal_detail::personal_details_info")->with('data',$final_data);

            return Excel::download(new personal_detail_info("personal_detail::personal_details_info",$final_data), 'Personal Details.xlsx');
    }
    public function get_individual_detail($id)
    {
            
            $personal_detail = Personal_detail::where('del_flag', 0)->where('p_id',$id)->get(); // when one ko needed
            $highest = $this->get_highest_work_experience($personal_detail);
            $high_reference = $this->get_highest_reference($personal_detail);
            $final_data=array(); 
            $education=Education::where('p_id',$id)->get();
            $extra_course=Extra_course::where('p_id',$id)->get();
            $preference_available=Preference_available::where('p_id',$id)->get();
            $orgreference=Orgreference::where('p_id',$id)->get();
            $reference_organization=Reference_organization::where('p_id',$id)->get();
            $work_experience=Work_experience::where('p_id',$id)->get();
            $working_experience=Working_experience::where('p_id',$id)->get();
                $data = array(
                    'personal_details'=>$personal_detail,
                    'education'=>$education,
                    'extra_course'=>$extra_course,
                    'preference_available'=>$preference_available,
                    'orgreference'=>$orgreference,
                    'reference_organization'=>$reference_organization,
                    'work_experience'=>$work_experience,
                    'working_experience'=>$working_experience,
                );
                array_push($final_data,$data);
                $final_data['total_work_experience']=$highest;
                $final_data['orgreference']=$high_reference;
               
            //  return $data;
          // return view("personal_detail::individual_info")->with('data',$final_data);
    	return Excel::download(new individual_personal_details("personal_detail::individual_info",$final_data), 'Personal Details.xlsx');
    }


   
}
