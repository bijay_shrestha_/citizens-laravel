
	<table border="1">
            @php  
            $highest=$data['total_work_experience'];
            $reference=$data['orgreference'];
            @endphp
        <thead>
        <tr>

            <td>p_id</td>
            <td>first_name</td>
            <td>midle_name</td>
            <td>last_name</td>
            <td>permanent_address</td>
            <td>temporaray_address</td>
            <td>dob	</td>
            <td>p_zone</td>
            <td>p_district</td>
            <td>t_zone</td>
            <td>t_district</td>
            <td>nationality</td>
            <td>cv</td>
            <td>citizenship_no</td>
            <td>language</td>
            <td>email</td>
            <td>marital_status</td>
            <td>immediate_contact</td>
            <td>mobile_no</td>
            <td>status</td>
            <td>del_flag</td>

            @for($i=1;$i<$reference+1;$i++)
            <td>reference {{ $i }} </td>
            <td>post{{ $i }} </td>
            <td>organization{{ $i }} </td>
            <td>address{{ $i }} </td>
            <td>org_email{{ $i }} </td>
            <td>telephone{{ $i }} </td>
            @endfor

            <td>preferred_post</td>
            <td>specialised_area</td>	
            <td>preferred_location</td>	
            <td>expected_salary</td>	
            <td>driving_license</td>	
            <td>travelling_option</td>														
            <td>vacancy_code</td>	

  @for($i=1;$i<$highest+1;$i++)
            <td>experience_organization  {{ $i }}</td>	
            <td>position {{ $i }}</td>	
            <td>department {{ $i }}</td>	
            <td>experience {{ $i }}</td>
            @endfor

            <td>School Board</td>
            <td>School Name</td>
            <td>School Obtained Percentage</td>
            <td>School Passed Year</td>
            <td>10+2 Board</td>
            <td>10+2 Name</td>
            <td>10+2 Obtained Percentage</td>
            <td>10+2 Passed Year</td>
            <td>Bachelor Board</td>
            <td>Bachelor college Name</td>
            <td>Bachelor College Degree</td>
            <td>Bachelor Obtained Percentage/GPA</td>
            <td>Bachelor Passed Year</td>
            <td>Master Board</td>
            <td>Master College Name</td>
            <td>Master College Degree/faculty</td>
            <td>Master Obtained Percentage/GPA</td>
            <td>Master Passed Year</td>
            <td>course</td>	
            <td>institute</td>	
        </tr>
        </thead>
        <tbody>
               @foreach($data as $individual_data) 
                @php $personal_detail=$individual_data['personal_details']; 
                     $education= $individual_data['education'];
                     $extra_course= $individual_data['extra_course'];
                     $preference_available= $individual_data['preference_available'];
                     $work_experience= $individual_data['work_experience'];
                     $orgreference= $individual_data['orgreference'];
                     $reference_organization= $individual_data['reference_organization'];
                     $working_experience= $individual_data['working_experience'];
                    //  dd($preference_available)
                @endphp
                
            <tr>
                <td>{{ $personal_detail[0]['p_id'] }}</td>
                <td>{{ $personal_detail[0]['first_name'] }}</td>
                <td>{{ $personal_detail[0]['midle_name'] }}</td>
                <td>{{ $personal_detail[0]['last_name'] }}</td>
                <td>{{ $personal_detail[0]['permanent_address'] }}</td>
                <td>{{ $personal_detail[0]['temporaray_address'] }}</td>
                <td>{{ $personal_detail[0]['dob'] }}</td>
                <td>{{ $personal_detail[0]['p_zone'] }}</td>
                <td>{{ $personal_detail[0]['p_district'] }}</td>
                <td>{{ $personal_detail[0]['t_zone'] }}</td>
                <td>{{ $personal_detail[0]['t_district'] }}</td>
                <td>{{ $personal_detail[0]['nationality'] }}</td>
                <td>@if(isset($personal_detail[0]['cv']) && (!empty($personal_detail[0]['cv'])) && ($personal_detail[0]['cv']!="")){{  url('/uploads/cv/'.$personal_detail[0]['cv']) }} @else {{" "}} @endif</td>
                <td>{{ $personal_detail[0]['citizenship_no'] }}</td>
                <td>{{ $personal_detail[0]['language'] }}</td>
                <td>{{ $personal_detail[0]['email'] }}</td>
                <td>{{ $personal_detail[0]['marital_status'] }}</td>
                <td>{{ $personal_detail[0]['immediate_contact'] }}</td>
                <td>{{ $personal_detail[0]['mobile_no'] }}</td>
                <td>{{ $personal_detail[0]['status'] }}</td>
                <td>{{ $personal_detail[0]['del_flag'] }}</td>


                @php
                if ($orgreference) {
                    $count_reference = count($orgreference);
                } else {
                    $count_reference = 0;
                }
                @endphp
                @for ($i = 0; $i < $reference; $i++)              
                @if ($count_reference > 0)
                    @if ($count_reference - 1  >= $i )
                        <td>{{ @$orgreference[$i]['reference']}}</td>
                        <td>{{ @$orgreference[$i]['post']}}</td>
                        <td>{{ @$orgreference[$i]['organization']}}</td>
                        <td>{{ @$orgreference[$i]['address']}}</td>
                        <td>{{ @$orgreference[$i]['email']}}</td>
                        <td>{{ @$orgreference[$i]['telephone']}}</td>
                    @else
                    <td>empty</td>             
                    @endif
                @else 
                <td>{{-- empty1 --}} </td>
                <td>{{-- empty2 --}} </td> 
                
                      
                @endif
            @endfor

                <td>{{ @$preference_available[0]['preferred_post'] }}</td>
                <td>{{ @$preference_available[0]['specialised_area'] }}</td>
                <td>{{ @$preference_available[0]['preferred_location'] }}</td>
                <td>{{ @$preference_available[0]['expected_salary'] }}</td>
                <td>{{ @$preference_available[0]['driving_license'] }}</td>
                <td>{{ @$preference_available[0]['travelling_option'] }}</td>
                <td>{{ @$preference_available[0]['vacancy_code'] }}</td>
                
                @php
                if ($work_experience) {
                    $count_experience = count($work_experience);
                } else {
                    $count_experience = 0;
                }
                @endphp
        @for ($i = 0; $i < $highest; $i++)
          
        @if ($count_experience > 0)
            @if ($count_experience - 1  >= $i )
                <td>{{ @$work_experience[$i]['organization']}}</td>
                <td>{{ @$work_experience[$i]['position']}}</td>
                <td>{{ @$work_experience[$i]['department']}}</td>
                <td>{{ @$work_experience[$i]['experience']}}</td>
              
            @else
                <td>{{-- empty1 --}} </td>
                <td>{{-- empty2 --}} </td> 
                <td></td>
                <td></td>
                <td></td>                
            @endif
        @else 
            <td>{{-- empty3 --}} </td>
            <td>{{-- empty4 --}}</td>
            <td></td>
            <td></td>
            <td></td>  
        @endif
    @endfor
                {{-- <td>{{ @$work_experience[0]['organization'] }}</td>
                <td>{{ @$work_experience[0]['position'] }}</td>
                <td>{{ @$work_experience[0]['department'] }}</td>
                <td>{{ @$work_experience[0]['experience'] }}</td>
                 --}}
            
                
                <td>{{ @$education[0]['s_board'] }}</td>
                <td>{{ @$education[0]['s_school'] }}</td>
                <td>{{ @$education[0]['s_marks'] }}</td>
                <td>{{ @$education[0]['s_passed_year'] }}</td>
                <td>{{ @$education[0]['c_university'] }}</td>
                <td>{{ @$education[0]['c_college'] }}</td>
                <td>{{ @$education[0]['c_marks'] }}</td>
                <td>{{ @$education[0]['c_passed_year'] }}</td>
                <td>{{ @$education[0]['b_university'] }}</td>
                <td>{{ @$education[0]['b_college'] }}</td>
                <td>{{ @$education[0]['b_faculty']  }}</td>
                <td>{{ @$education[0]['b_marks']  }}</td>
                <td>{{ @$education[0]['b_passed_year'] }}</td>
                <td>{{ @$education[0]['m_university'] }}</td>
                <td>{{ @$education[0]['m_college'] }}</td>
                <td>{{ @$education[0]['m_faculty']  }}</td>
                <td>{{ @$education[0]['m_marks']}}</td>
                <td>{{ @$education[0]['m_passed_year'] }}</td>
                <td>{{ @$education[0]['course_name'] }}</td>
                <td>{{ @$education[0]['institude_name'] }}</td> 

            </tr>
         
            @endforeach
        </tbody>
	</table>
