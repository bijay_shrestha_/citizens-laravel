@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Personal_details   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.personal_details') }}">tbl_personal_details</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.personal_details.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                        <input type="hidden" name="p_id" id="p_id" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="first_name">First_name</label><input type="text" name="first_name" id="first_name" class="form-control" ></div><div class="form-group">
                        <label for="middle_name">Middle_name</label><input type="text" name="midle_name" id="middle_name" class="form-control" ></div><div class="form-group">
                        <label for="last_name">Last_name</label><input type="text" name="last_name" id="last_name" class="form-control" ></div><div class="form-group">
                        <label for="permanent_address">Permanent_address</label><input type="text" name="permanent_address" id="permanent_address" class="form-control" ></div><div class="form-group">
                        <label for="temporary_address">Temporary_address</label><input type="text" name="temporary_address" id="temporary_address" class="form-control" ></div><div class="form-group">
                        <label for="dob">Dob</label><input type="text" name="dob" id="dob" class="form-control" ></div><div class="form-group">
                        <label for="p_zone">P_zone</label><input type="text" name="p_zone" id="p_zone" class="form-control" ></div><div class="form-group">
                        <label for="p_district">P_district</label><input type="text" name="p_district" id="p_district" class="form-control" ></div><div class="form-group">
                        <label for="t_zone">T_zone</label><input type="text" name="t_zone" id="t_zone" class="form-control" ></div><div class="form-group">
                        <label for="t_district">T_district</label><input type="text" name="t_district" id="t_district" class="form-control" ></div><div class="form-group">
                        <label for="nationality">Nationality</label><input type="text" name="nationality" id="nationality" class="form-control" ></div><div class="form-group">
                        <label for="cv">Cv</label>

                        <br/>
                        <div class="radio"><label id="upload_download_link" style="display:none"></label>
                           <input type="file" name="download_link" id="download_link" >
                            <div id="file-status"></div>
                            <input type="hidden" name="download_form_link" id="download_form_link"/>
                            <a href="javascript:void(0)" id="change-file" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                  </div></div><div class="form-group">
<label for="citizenship_no">Citizenship_no</label><input type="text" name="citizenship_no" id="citizenship_no" class="form-control" ></div><div class="form-group">
                        <label for="language">Language</label><input type="text" name="language" id="language" class="form-control" ></div><div class="form-group">
                        <label for="phone_no">Phone_no</label><input type="text" name="phone_no" id="phone_no" class="form-control" ></div><div class="form-group">
                        <label for="email">Email</label><input type="text" name="email" id="email" class="form-control" ></div><div class="form-group">
                        <label for="gender">Gender </label><br>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="gender" id="gender1" @if(old('gender') == 1) checked @endif  />Male
                        </label> 
                        <label>
                            <input type="radio" value="0" name="gender" id="gender0" @if(old('gender') == 1) @else  checked @endif  />Female
                        </label></div>
                    </div><div class="form-group">
                        <label for="marital_status">Marital_status </label><br><div class="radio">
                        <label><input type="radio" name="marital_status" id="marital_status1" @if(old('marital_status') == 1) checked @endif  />Married</label>
                        <label><input type="radio" name="marital_status" id="marital_status0" @if(old('marital_status') == 1) @else  checked @endif  />Unmarried</label>
                    </div></div>
                        <div class="form-group">
                        <label for="immediate_contact">Immediate_contact</label><input type="text" name="immediate_contact" id="immediate_contact" class="form-control" ></div><div class="form-group">
                        <label for="mobile_no">Mobile_no</label><input type="text" name="mobile_no" id="mobile_no" class="form-control" ></div><div class="form-group">
                        <label for="contact_no">Contact_no</label><input type="text" name="contact_no" id="contact_no" class="form-control" ></div>
                                    <div class="form-group">
                                        <label for="status">Status</label><br>
                                        <div class="radio">
                                        <label>
                                            <input type="radio" value="1" name="status" id="status1" @if(old('status') == 1) checked @endif  />Yes
                                        </label> 
                                        <label>
                                            <input type="radio" value="0" name="status" id="status0" @if(old('status') == 1) @else  checked @endif  />No
                                        </label>
                                        </div>
                                    </div>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.personal_details') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
