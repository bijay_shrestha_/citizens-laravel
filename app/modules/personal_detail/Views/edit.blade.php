@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Personal_details   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.personal_details') }}">tbl_personal_details</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.personal_details.update') }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                                    <label for="p_id">P_id</label><input type="text" value = "{{$personal_detail->p_id}}"  name="p_id" id="p_id" class="form-control" ></div><div class="form-group">
                                    <label for="first_name">First_name</label><input type="text" value = "{{$personal_detail->first_name}}"  name="first_name" id="first_name" class="form-control" ></div><div class="form-group">
                                    <label for="middle_name">Middle_name</label><input type="text" value = "{{$personal_detail->middle_name}}"  name="midle_name" id="middle_name" class="form-control" ></div><div class="form-group">
                                    <label for="last_name">Last_name</label><input type="text" value = "{{$personal_detail->last_name}}"  name="last_name" id="last_name" class="form-control" ></div><div class="form-group">
                                    <label for="permanent_address">Permanent_address</label><input type="text" value = "{{$personal_detail->permanent_address}}"  name="permanent_address" id="permanent_address" class="form-control" ></div><div class="form-group">
                                    <label for="temporary_address">Temporary_address</label><input type="text" value = "{{$personal_detail->temporary_address}}"  name="temporary_address" id="temporary_address" class="form-control" ></div><div class="form-group">
                                    <label for="dob">Dob</label><input type="text" value = "{{$personal_detail->dob}}"  name="dob" id="dob" class="form-control" ></div><div class="form-group">
                                    <label for="p_zone">P_zone</label><input type="text" value = "{{$personal_detail->p_zone}}"  name="p_zone" id="p_zone" class="form-control" ></div><div class="form-group">
                                    <label for="p_district">P_district</label><input type="text" value = "{{$personal_detail->p_district}}"  name="p_district" id="p_district" class="form-control" ></div><div class="form-group">
                                    <label for="t_zone">T_zone</label><input type="text" value = "{{$personal_detail->t_zone}}"  name="t_zone" id="t_zone" class="form-control" ></div><div class="form-group">
                                    <label for="t_district">T_district</label><input type="text" value = "{{$personal_detail->t_district}}"  name="t_district" id="t_district" class="form-control" ></div><div class="form-group">
                                    <label for="nationality">Nationality</label><input type="text" value = "{{$personal_detail->nationality}}"  name="nationality" id="nationality" class="form-control" ></div><div class="form-group">
                                    <label for="cv">Cv</label><input type="text" value = "{{$personal_detail->cv}}"  name="cv" id="cv" class="form-control" ></div><div class="form-group">
                                    <label for="citizenship_no">Citizenship_no</label><input type="text" value = "{{$personal_detail->citizenship_no}}"  name="citizenship_no" id="citizenship_no" class="form-control" ></div><div class="form-group">
                                    <label for="language">Language</label><input type="text" value = "{{$personal_detail->language}}"  name="language" id="language" class="form-control" ></div><div class="form-group">
                                    <label for="phone_no">Phone_no</label><input type="text" value = "{{$personal_detail->phone_no}}"  name="phone_no" id="phone_no" class="form-control" ></div><div class="form-group">
                                    <label for="email">Email</label><input type="text" value = "{{$personal_detail->email}}"  name="email" id="email" class="form-control" ></div><div class="form-group">
                                    <label for="gender">Gender</label><input type="text" value = "{{$personal_detail->gender}}"  name="gender" id="gender" class="form-control" ></div><div class="form-group">
                                    <label for="marital_status">Marital_status</label><input type="text" value = "{{$personal_detail->marital_status}}"  name="marital_status" id="marital_status" class="form-control" ></div><div class="form-group">
                                    <label for="immediate_contact">Immediate_contact</label><input type="text" value = "{{$personal_detail->immediate_contact}}"  name="immediate_contact" id="immediate_contact" class="form-control" ></div><div class="form-group">
                                    <label for="mobile_no">Mobile_no</label><input type="text" value = "{{$personal_detail->mobile_no}}"  name="mobile_no" id="mobile_no" class="form-control" ></div><div class="form-group">
                                    <label for="contact_no">Contact_no</label><input type="text" value = "{{$personal_detail->contact_no}}"  name="contact_no" id="contact_no" class="form-control" ></div><div class="form-group">
                                    <label for="status">Status</label><input type="text" value = "{{$personal_detail->status}}"  name="status" id="status" class="form-control" ></div><div class="form-group">
                                    <label for="del_flag">Del_flag</label><input type="text" value = "{{$personal_detail->del_flag}}"  name="del_flag" id="del_flag" class="form-control" ></div><div class="form-group">
                                    <label for="father_name">Father_name</label><input type="text" value = "{{$personal_detail->father_name}}"  name="father_name" id="father_name" class="form-control" ></div><div class="form-group">
                                    <label for="grand_father_name">Grand_father_name</label><input type="text" value = "{{$personal_detail->grand_father_name}}"  name="grand_father_name" id="grand_father_name" class="form-control" ></div><div class="form-group">
                                    <label for="posted_date">Posted_date</label><input type="text" value = "{{$personal_detail->posted_date}}"  name="posted_date" id="posted_date" class="form-control" ></div>
<input type="hidden" name="id" id="id" value = "{{$personal_detail->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.personal_details') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
