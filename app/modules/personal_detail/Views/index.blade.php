@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Personal_details		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Personal_details</a></li>

		</ol>

	</section>
	<div class="pad margin no-print">
			<div class="pull-right"><a href="{{route('admin.personal_details_info') }}" class="btn btn-danger"><i class="fa fa-plus-circle"></i> Export All</a></div>
			</div>
	<section class="content">
		<div class="box box-primary">
			<div class="box-body">		
				<!-- /.box-header -->
					<div class="row">
							<div class="col-xs-12">
								<div class="box">
					<div class="box-body">
						<table id="personal_detail-datatable" class="table table-striped table-bordered">
							<thead>
								{{-- <th><input type="checkbox" id="check_all"/></th> --}}
								 <th>SN</th>
								 <th>First Name</th>
								 <th>Middle Name</th>
								 <th>Last Name</th>
								 <th>Specialised Area</th>
								<th>Preferred Post</th>
								<th>Email</th>
								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#personal_detail-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.personal_details.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},
				  name: "sn", searchable: false },
				
			// {data: function(data){
			// 	var fullname = '';
			// 	if (data.middle_name == '' || data.middle_name == null) {
			// 		fullname = data.first_name + ' ' + data.last_name;
			// 	} else {
			//    fullname = data.first_name + ' ' + data.middle_name + ' ' + data.last_name;
			//		}
			//	return fullname;
			//   }, name: "middle_name"},
			{ data: "first_name",name: "first_name"},
			{ data: "midle_name",name: "midle_name"},
			{ data: "last_name",name: "last_name"},
			{ data: "specialised_area",name: "specialised_area"},
			{ data: "preferred_post",name: "preferred_post"},
			{ data: "p_email",name: "tbl_personal_details.email"},
					{ data: function(data,b,c,table) { 
					var buttons = '';

					buttons += "<a class='btn btn-success' href='"+site_url+"/show/"+data.p_id+"' type='button'><i class='fa fa-eye'></i>View</a>&nbsp"; 

					buttons += "<a href='"+site_url+"/delete/"+data.p_id+"' class='btn bg-red waves-effect'><i class='fa fa-trash'></i>Delete</a>";

					return buttons;
					}, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
