
	<table border="1">
        <thead>@php  $total_count=$data['total_count'];
                     $highest=$data['total_work_experience'];
                     $reference=$data['orgreference'];
            @endphp
           
             <tr><td>P_id</td>
                <td>Name</td>
                <td>Father Name</td>
                <td>Grand Father Name</td>
                <td>citizenship No.</td>
                <td>Permanent Address</td>
                <td>Permanent Zone</td>
                <td>Permanent District</td>
                <td>Temporary Address</td>
                <td>Temporary Zone</td>
                <td>Temporary District</td>
                <td>Date of Birth</td>
                <td>Gender </td>
                <td>status</td>
                <td>Nationality</td>
                <td>Langauge</td>
                <td>Email Address</td>
                <td>Landline</td>
                <td>Mobile </td>
                <td>Marital Status</td>
                <td>Imidiate Contact Person</td>
                <td>Imidiate Contact Person No.</td>
                <td>School Board</td>
                <td>School Name</td>
                <td>School Obtained Percentage</td>
                <td>School Passed Year</td>
                <td>10+2 Board</td>
                <td>10+2 Name</td>
                <td>10+2 Obtained Percentage</td>
                <td>10+2 Passed Year</td>
                <td>Bachelor Board</td>
                <td>Bachelor college Name</td>
                <td>Bachelor College Degree</td>
                <td>Bachelor Obtained Percentage/GPA</td>
                <td>Bachelor Passed Year</td>
                <td>Master Board</td>
                <td>Master College Name</td>
                <td>Master College Degree/faculty</td>
                <td>Master Obtained Percentage/GPA</td>
                <td>Master Passed Year</td>
                <td>Prefered Post</td>
                <td>Specialized Area</td>
                <td>Travel Option</td>
                <td>Travel License</td>
                <td>Prefered Location Inside</td>
                <td>Prefered Location Outside</td>

                @for($i=1;$i<$total_count+1;$i++)
                <td>Course {{ $i }} </td>
                <td> Institute {{$i }}</td>
                
                @endfor
                @for($i=1;$i<$highest+1;$i++)
                <td>Organization {{ $i }} </td>
                <td> Department {{$i }}</td>
                <td>Position {{$i}}</td>
                <td> Experience From {{$i }}</td>
                <td> Experience To {{$i }}</td> 
                @endfor
               
                @for($i=1;$i<$reference+1;$i++)
                <td>Reference {{ $i }} </td>
                <td> Reference Post {{$i }}</td>
                <td> Reference Organization {{$i}}</td>
                <td> Reference Organization Address {{$i }}</td>
                <td>Reference Organization Email {{$i }}</td> 
                <td>Reference Organization Telephone {{$i }}</td> 
                @endfor
                <td>Submited Date and Time</td>
                <td>CV</td>
            </tr> 
        </thead>
    
    @foreach($data as $individual_data)
    @php $personal_detail=$individual_data['personal_details']; 
         $education= $individual_data['education'];
         $extra_course= $individual_data['extra_course'];
         $preference_available= $individual_data['preference_available'];
         $work_experience= $individual_data['work_experience'];
         $orgreference= $individual_data['orgreference'];
         $reference_organization= $individual_data['reference_organization'];
         $working_experience= $individual_data['working_experience'];
          $locations = array();
          if ($preference_available) {
            $locations =json_decode(@$preference_available[0]->preferred_location,true);
          } else {
              $locations = array('Inside' => "", 'Outside' => "");
          }
    @endphp

<tbody>
    <tr>
    {{-- 0 --}}
    <td>{{ $personal_detail['p_id'] }}</td>
    <td> {{ $personal_detail['first_name'] }}@if($personal_detail['midle_name']!=null) {{ $personal_detail['midle_name']}} @endif {{ $personal_detail['last_name'] }}</td>
      <td>{{ $personal_detail['father_name'] }}</td>
      <td>{{ $personal_detail['grand_father_name'] }}</td>
      <td>{{ $personal_detail['citizenship_no'] }}</td>
      <td>{{ $personal_detail['permanent_address'] }}</td>
      <td>{{ $personal_detail['p_zone'] }}</td>
      <td>{{ $personal_detail['p_district'] }}</td>
      <td>{{ $personal_detail['temporary_address'] }}</td>
      <td>{{ $personal_detail['t_zone'] }}</td>
      <td>{{ $personal_detail['t_district'] }}</td>
      <td>{{ $personal_detail['dob'] }}</td>
      <td>{{ $personal_detail['gender'] }}</td>
      <td>{{ $personal_detail['status'] }}</td>
      <td>{{ $personal_detail['nationality'] }}</td>
      <td>{{ $personal_detail['language'] }}</td>
      <td>{{ $personal_detail['email'] }}</td>
      <td>{{ $personal_detail['phone_no'] }}</td>
      <td>{{ $personal_detail['mobile_no'] }}</td>
      <td>{{ $personal_detail['marital_status'] }}</td>
      <td>{{ $personal_detail['immediate_contact'] }} </td>
      <td>{{ $personal_detail['contact_no']}}</td>  
 
      {{-- 1 --}}
      <td>{{ @$education[0]['s_board'] }}</td>
      <td>{{ @$education[0]['s_school'] }}</td>
      <td>{{ @$education[0]['s_marks'] }}</td>
      <td>{{ @$education[0]['s_passed_year'] }}</td>
      <td>{{ @$education[0]['c_university'] }}</td>
      <td>{{ @$education[0]['c_college'] }}</td>
      <td>{{ @$education[0]['c_marks'] }}</td>
      <td>{{ @$education[0]['c_passed_year'] }}</td>
      <td>{{ @$education[0]['b_university'] }}</td>
      <td>{{ @$education[0]['b_college'] }}</td>
      <td>{{ @$education[0]['b_faculty'] }}</td>
      <td>{{ @$education[0]['b_marks'] }}</td>
      <td>{{ @$education[0]['b_passed_year'] }}</td>
      <td>{{ @$education[0]['m_university'] }}</td>
      <td>{{ @$education[0]['m_college'] }}</td>
      <td>{{ @$education[0]['m_faculty'] }}</td>
      <td>{{ @$education[0]['m_marks'] }}</td>
      <td>{{ @$education[0]['m_passed_year'] }}</td>
  
      <td>{{ @$preference_available[0]['preferred_post']}}</td>
      <td>{{ @$preference_available[0]['specialised_area']}}</td>
      <td>{{ @$preference_available[0]['travelling_option']}}</td>
      <td>{{ @$preference_available[0]['driving_license']}}</td>
      <td>{{ @$locations['Inside']}}</td>
      <td>{{ @$locations['Outside']}}</td>
      



        {{-- manish update
            @if(isset($extra_course))
           @php  $counter=0;
           $i=0; @endphp
            @foreach($extra_course as $ext)
                <td>{{ $ext['course'] }}</td>
                <td>{{ $ext['institude'] }}</td>
               @php  $counter++; @endphp
            @endforeach
            @if($counter<$total_count)
                <td> </td>
                <td> </td>
                @php $counter++; @endphp
                @endif
        @else
        @for($i=0; $i<=$total_count; $i++)
            <td> </td>
            <td> </td>
        @endfor
        @endif --}}

        @php
        if ($extra_course) {
            $count_extra = count($extra_course);
        } else {
            $count_extra = 0;
        }
        @endphp

        @for ($i = 0; $i < $total_count; $i++)
          
            @if ($count_extra > 0)
                @if ($count_extra - 1  >= $i )
                    <td>{{$extra_course[$i]['course']  }} </td>
                    <td>{{$extra_course[$i]['institude']}} </td>
                @else
                    <td>{{-- empty1 --}} </td>
                    <td>{{-- empty2 --}} </td>                 
                @endif
            @else 
                <td>{{-- empty3 --}} </td>
                <td>{{-- empty4 --}}</td>
            @endif
        @endfor


        {{-- 7 --}}

        @php
        if ($work_experience) {
            $count_experience = count($work_experience);
        } else {
            $count_experience = 0;
        }
        @endphp

        @for ($i = 0; $i < $highest; $i++)
          
            @if ($count_experience > 0)
                @if ($count_experience - 1  >= $i )
                    <td>{{ @$work_experience[$i]['organization']}}</td>
                    <td>{{ @$work_experience[$i]['department']}}</td>
                    <td>{{ @$work_experience[$i]['position']}}</td>
                    <td>{{ @$work_experience[$i]['experience_from']}}</td>
                    <td>{{ @$work_experience[$i]['experience_to']}}</td>
                @else
                    <td>{{-- empty1 --}} </td>
                    <td>{{-- empty2 --}} </td> 
                    <td></td>
                    <td></td>
                    <td></td>                
                @endif
            @else 
                <td>{{-- empty3 --}} </td>
                <td>{{-- empty4 --}}</td>
                <td></td>
                <td></td>
                <td></td>  
            @endif
        @endfor
            {{-- 4 --}}




            @php
            if ($orgreference) {
                $count_reference = count($orgreference);
            } else {
                $count_reference = 0;
            }
            @endphp
    
            @for ($i = 0; $i < $reference; $i++)
              
                @if ($count_reference > 0)
                    @if ($count_reference - 1  >= $i )
                        <td>{{ @$orgreference[$i]['reference']}}</td>
                        <td>{{ @$orgreference[$i]['post']}}</td>
                        <td>{{ @$orgreference[$i]['organization']}}</td>
                        <td>{{ @$orgreference[$i]['address']}}</td>
                        <td>{{ @$orgreference[$i]['email']}}</td>
                        <td>{{ @$orgreference[$i]['telephone']}}</td>
                    @else
                        <td>{{ " " }}</td>
                        <td>{{ " " }}</td>
                        <td>{{ " " }}</td>
                        <td>{{ " " }}</td>
                        <td>{{ " " }}</td>
                        <td>{{ " " }}</td>                  
                    @endif
                @else 
                <td>{{ " " }}</td>
                <td>{{ " " }}</td>
                <td>{{ " " }}</td>
                <td>{{ " " }}</td>
                <td>{{ " " }}</td>
                <td>{{ " " }}</td>                  
              
                @endif
            @endfor
         <td>{{ $personal_detail['posted_date']}}</td>  
        <td>{{ url('/uploads/cv/'.$personal_detail['cv']) }}</td>  
    @endforeach</tr></tbody></table>