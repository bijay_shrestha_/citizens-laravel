<?php

namespace App\Modules\Work_experience\Model;


use Illuminate\Database\Eloquent\Model;

class Work_experience extends Model
{
    public  $table = 'tbl_work_experience';

    protected $fillable = ['w_id','p_id','organization','position','department','experience','experience_from','experience_to',];
}
