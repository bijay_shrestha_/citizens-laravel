@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Work_experiences   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.work_experiences') }}">tbl_work_experience</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.work_experiences.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                                    <label for="w_id">W_id</label><input type="text" name="w_id" id="w_id" class="form-control" ></div><div class="form-group">
                                    <label for="p_id">P_id</label><input type="text" name="p_id" id="p_id" class="form-control" ></div><div class="form-group">
                                    <label for="organization">Organization</label><input type="text" name="organization" id="organization" class="form-control" ></div><div class="form-group">
                                    <label for="position">Position</label><input type="text" name="position" id="position" class="form-control" ></div><div class="form-group">
                                    <label for="department">Department</label><input type="text" name="department" id="department" class="form-control" ></div><div class="form-group">
                                    <label for="experience">Experience</label><input type="text" name="experience" id="experience" class="form-control" ></div><div class="form-group">
                                    <label for="experience_from">Experience_from</label><input type="text" name="experience_from" id="experience_from" class="form-control" ></div><div class="form-group">
                                    <label for="experience_to">Experience_to</label><input type="text" name="experience_to" id="experience_to" class="form-control" ></div>
<input type="hidden" name="id" id="id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.work_experiences') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
