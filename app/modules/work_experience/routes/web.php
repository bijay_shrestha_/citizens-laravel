<?php



Route::group(array('prefix'=>'admin/','module'=>'Work_experience','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Work_experience\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('work_experiences/','AdminWork_experienceController@index')->name('admin.work_experiences');
    Route::post('work_experiences/getwork_experiencesJson','AdminWork_experienceController@getwork_experiencesJson')->name('admin.work_experiences.getdatajson');
    Route::get('work_experiences/create','AdminWork_experienceController@create')->name('admin.work_experiences.create');
    Route::post('work_experiences/store','AdminWork_experienceController@store')->name('admin.work_experiences.store');
    Route::get('work_experiences/show/{id}','AdminWork_experienceController@show')->name('admin.work_experiences.show');
    Route::get('work_experiences/edit/{id}','AdminWork_experienceController@edit')->name('admin.work_experiences.edit');
    Route::match(['put', 'patch'], 'work_experiences/update/{id}','AdminWork_experienceController@update')->name('admin.work_experiences.update');
    Route::get('work_experiences/delete/{id}', 'AdminWork_experienceController@destroy')->name('admin.work_experiences.edit');
});




Route::group(array('module'=>'Work_experience','namespace' => 'App\Modules\Work_experience\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('work_experiences/','Work_experienceController@index')->name('work_experiences');
    
});