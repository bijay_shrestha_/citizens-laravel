<?php

namespace App\modules\social_media_link\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\social_media_link\Model\Social_media_link;
use Carbon\Carbon;

class AdminSocial_media_linkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response //SOCIAL_MEDIA_LINK
     */
    public function index()
    {
        $page['title'] = 'Social Media Link';
        return view("social_media_link::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getsocial_media_linksJson(Request $request)
    {
        $social_media_link = new Social_media_link;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $social_media_link->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('id', "DSC")->get();

        // Display limited list        
        $rows = $social_media_link->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->orderBy('id', "DSC")->get();

        //To count the total values present
        $total = $social_media_link->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Social Media Link | Create';
        return view("social_media_link::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $datafor_social_media = $data;
        unset($datafor_social_media['upload_image']);
        $datafor_social_media['del_flag'] = 0;
        $datafor_social_media['icon'] = $datafor_social_media['image_name'];
        unset($datafor_social_media['image_name']);
        $datafor_social_media = check_case($datafor_social_media, 'create');        
        $success = Social_media_link::Insert($datafor_social_media);

        $social_medias = Social_media_link::all();
        $social_media_id = $social_medias[count($social_medias)-1]['id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "SOCIAL_MEDIA_LINK", 'file_id' => $social_media_id]); //create acrivity log     
        
        return redirect()->route('admin.social_media_links')->with('success', "Data added successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $social_media_link = Social_media_link::findOrFail($id);
        $page['title'] = 'Social Media Link | Update';
        return view("social_media_link::edit",compact('page','social_media_link'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "SOCIAL_MEDIA_LINK", 'file_id' => $id]); //create activity log
        $social_media = Social_media_link::where('id', $id)->firstOrFail();
        $datafor_social_media = $data;
        unset($datafor_social_media['upload_image']);

        if (array_key_exists('delete_old_image', $data)) {
            if ((file_exists(public_path().'/uploads/icon/'. $social_media->icon))  && $social_media->icon) {
                unlink(public_path().'/uploads/icon/'. $social_media->icon);
                if (file_exists(public_path().'/uploads/icon/thumb/'. $social_media->icon)) {
                    unlink(public_path().'/uploads/icon/thumb/'. $social_media->icon);
                } 
            }
            if (!$data['image_name']) {
                $data['image_name'] = '';
                $datafor_social_media['image_name'] = '';
            }
            unset($datafor_social_media['delete_old_image']);
        } else {
            if (!$data['image_name']) {
                $data['image_name'] = $social_media->icon;
                $datafor_social_media['image_name'] = $social_media->icon;
            }
        }
        $datafor_social_media['icon'] = $datafor_social_media['image_name'];
        unset($datafor_social_media['image_name']);
        $datafor_social_media = check_case($datafor_social_media, 'edit');        
        $success = DB::table('tbl_social_media_link')->where('id', $id)->update($datafor_social_media);
        return redirect()->route('admin.social_media_links')->with('success', "Data updated successfully.");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_social_media_link', 'id', $id);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "SOCIAL_MEDIA_LINK", 'file_id' => $id]); //create activity log
        return redirect()->route('admin.social_media_links')->with('success', "Data deleted successfully.");
        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_social_media_link', 'id');
        return redirect()->route('admin.social_media_links')->with('success', "Data updated successfully.");
    }

}
