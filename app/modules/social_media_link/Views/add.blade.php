@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Social Media Links   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.social_media_links') }}">Social Media Link</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.social_media_links.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                        <label for="name">Name</label><input type="text" name="name" id="name" class="form-control" required >
                    </div>
                    
                    <div class="form-group">
                        <label for="link">Link</label><input type="text" name="link" id="link" class="form-control" >
                    
                    <div class="form-group">
                        <label for="image_name">Icon </label><br>
                        <label id="upload_image_name" style="display:none"></label>
                        <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                        <div id="image-status"></div>
                        <input type="file" id="upload_image" name="upload_image" style="display:block"/>
                        <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                    </div>

                    
                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="status" id="status1" @if(old('status') == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="status" id="status0" @if(old('status') == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.social_media_links') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
        <script type="text/javascript">
          var directory = 'icon';
    </script>
    @include('data_changing_script_create')
    @include('file_changing_script')

@endsection
