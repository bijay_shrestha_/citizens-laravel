@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Social Media Links   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.social_media_links') }}">Social Media Link</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.social_media_links.update', $social_media_link->id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                    <div class="form-group">
                        <label for="name">Name</label><input type="text" value = "{{$social_media_link->name}}"  name="name" id="name" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="link">Link</label><input type="text" value = "{{$social_media_link->link}}"  name="link" id="link" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="image_name">Icon </label><br>
                        <label id="upload_image_name" style="display:none"></label>
                        <div id="delete_old_image_requested"></div>
                        <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                        <div id="image-status"></div>
                        <input type="file" id="upload_image" name="upload_image" @if (file_exists(public_path().'/uploads/icon/thumb/'. $social_media_link->icon) && $social_media_link->icon) style="display:none" @else style="display:block" @endif />
                        <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                        <div id="imagedata">
                        @if (file_exists(public_path().'/uploads/icon/'. $social_media_link->icon) && $social_media_link->icon)
                    
                            <img src="{{url('uploads/icon/thumb/'.$social_media_link->icon)}}">
                            <a href="javascript:void(0)" id="change-image_option" title="Change Image">&nbsp; &nbsp; <img src="{{('/images/logo/cancel.png')}}" border="0"/> Change the old image</a>
                        @endif                                
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="status" id="status1" @if($social_media_link->status == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="status" id="status0" @if($social_media_link->status == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>

<input type="hidden" name="id" id="id" value = "{{$social_media_link->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.social_media_links') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
        <script type="text/javascript">
          var directory = 'icon';
    </script>
    @include('data_changing_script_edit')
    @include('file_changing_script')

@endsection
