<?php



Route::group(array('prefix'=>'admin/','module'=>'social_media_link','middleware' => ['web','auth'], 'namespace' => 'App\modules\social_media_link\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('social_media_links/','AdminSocial_media_linkController@index')->name('admin.social_media_links');
    Route::post('social_media_links/getsocial_media_linksJson','AdminSocial_media_linkController@getsocial_media_linksJson')->name('admin.social_media_links.getdatajson');
    Route::get('social_media_links/create','AdminSocial_media_linkController@create')->name('admin.social_media_links.create');
    Route::post('social_media_links/store','AdminSocial_media_linkController@store')->name('admin.social_media_links.store');
    Route::get('social_media_links/show/{id}','AdminSocial_media_linkController@show')->name('admin.social_media_links.show');
    Route::get('social_media_links/edit/{id}','AdminSocial_media_linkController@edit')->name('admin.social_media_links.edit');
    Route::match(['put', 'patch'], 'social_media_links/update/{id}','AdminSocial_media_linkController@update')->name('admin.social_media_links.update');
    Route::get('social_media_links/delete/{id}', 'AdminSocial_media_linkController@destroy')->name('admin.social_media_links.edit');
    Route::get('social_media_links/change/{text}/{status}/{id}', 'AdminSocial_media_linkController@change');

});




Route::group(array('module'=>'social_media_link','namespace' => 'App\modules\social_media_link\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('social_media_links/','Social_media_linkController@index')->name('social_media_links');
    
});