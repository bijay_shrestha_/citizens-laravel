<?php

namespace App\Modules\Social_media_link\Model;


use Illuminate\Database\Eloquent\Model;

class Social_media_link extends Model
{
    public  $table = 'tbl_social_media_link';

    protected $fillable = ['id','name','link','icon','status','approvel_status','approved','del_flag',];
}
