@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Remittances   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.remittances') }}">Remittance</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.remittances.update', $remittance->id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                    <div class="form-group">
                        <label for="type">Type </label>
                        <select name="type" class="form-control">
                            @foreach ($remit_catagories as $category)
                                <option value="{{$category['id']}}" 
                                 @if ($remittance->type == $category['id']) selected @endif
                                > {{$category['title']}} </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="remittance_company">Remittance Company</label><input type="text" value = "{{$remittance->remittance_company}}"  name="remittance_company" id="remittance_company" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="remittance_product">Remittance Product</label><input type="text" value = "{{$remittance->remittance_product}}"  name="remittance_product" id="remittance_product" class="form-control" >
                    </div>
                   
                    <div class="form-group">
                        <label for="remittance_receiving_countries">Remittance Receiving Countries</label><input type="text" value = "{{$remittance->remittance_receiving_countries}}"  name="remittance_receiving_countries" id="remittance_receiving_countries" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="status" id="status1" @if($remittance->status == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="status" id="status0" @if($remittance->status == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>

                    <input type="hidden" name="id" id="id" value = "{{$remittance->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.remittances') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
