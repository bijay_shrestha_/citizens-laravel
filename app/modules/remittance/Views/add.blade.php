@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Remittances   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.remittances') }}">Remittance</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.remittances.store') }}"  method="post">
                <div class="box-body">                

                    <div class="form-group">
                        <label for="type">Type </label>
                        <select name="type" class="form-control">
                            @foreach ($remit_catagories as $category)
                                <option value="{{$category['id']}}" 
                                 @if (old('type') == $category['id']) selected @endif
                                > {{$category['title']}} </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="remittance_company">Remittance Company</label><input type="text" name="remittance_company" id="remittance_company" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="remittance_product">Remittance Product</label><input type="text" name="remittance_product" id="remittance_product" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="remittance_receiving_countries">Remittance Receiving Countries</label><input type="text" name="remittance_receiving_countries" id="remittance_receiving_countries" class="form-control" >
                    </div>
                    
                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="status" id="status1" @if(old('status') == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="status" id="status0" @if(old('status') == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.remittances') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
