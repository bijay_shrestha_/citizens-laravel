<?php



Route::group(array('prefix'=>'admin/','module'=>'remittance','middleware' => ['web','auth'], 'namespace' => 'App\modules\remittance\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('remittances/','AdminRemittanceController@index')->name('admin.remittances');
    Route::post('remittances/getremittancesJson','AdminRemittanceController@getremittancesJson')->name('admin.remittances.getdatajson');
    Route::get('remittances/create','AdminRemittanceController@create')->name('admin.remittances.create');
    Route::post('remittances/store','AdminRemittanceController@store')->name('admin.remittances.store');
    Route::get('remittances/show/{id}','AdminRemittanceController@show')->name('admin.remittances.show');
    Route::get('remittances/edit/{id}','AdminRemittanceController@edit')->name('admin.remittances.edit');
    Route::match(['put', 'patch'], 'remittances/update/{id}','AdminRemittanceController@update')->name('admin.remittances.update');
    Route::get('remittances/delete/{id}', 'AdminRemittanceController@destroy')->name('admin.remittances.edit');
    Route::get('remittances/change/{text}/{status}/{id}', 'AdminRemittanceController@change');

});




Route::group(array('module'=>'remittance','namespace' => 'App\modules\remittance\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('remittances/','RemittanceController@index')->name('remittances');
    
});