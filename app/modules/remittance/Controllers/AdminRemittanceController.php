<?php

namespace App\modules\remittance\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\remittance\Model\Remittance;
use App\modules\remit_catagory\Model\Remit_catagory;
use App\modules\remittance_content\Model\Remittance_content;
use Carbon\Carbon;

class AdminRemittanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Remittance';
        $remittance_content = Remittance_content::where([['del_flag', 0], ['status', 1]])->first();
        return view("remittance::index",compact('page', 'remittance_content'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getremittancesJson(Request $request)
    {
        $remittance = new Remittance;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $remittance->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('id', "DSC")->get();

        // Display limited list        
        $rows = $remittance->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->orderBy('id', "DSC")->where('del_flag',0)->get();

        //To count the total values present
        $total = $remittance->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Remittance | Create';
        $remit_catagories = Remit_catagory::where('del_flag', 0)->get()->toArray();
        return view("remittance::add",compact('page', 'remit_catagories'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $datafor_remittance = $data;
        $datafor_remittance['added_by'] = Auth::user()->id;
        $datafor_remittance['added_date'] = new Carbon();
        $datafor_remittance['del_flag'] = 0;
        $datafor_remittance = check_case($datafor_remittance, 'create');        
        $success = Remittance::Insert($datafor_remittance);
        $remittances = Remittance::all();
        $remittance_id = $remittances[count($remittances)-1]['id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "REMITTANCE", 'file_id' => $remittance_id]); //create acrivity log     
        return redirect()->route('admin.remittances')->with('success', "Data inserted successfully.");
        
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $remittance = Remittance::findOrFail($id);
        $page['title'] = 'Remittance | Update';
        $remit_catagories = Remit_catagory::where('del_flag', 0)->get()->toArray();
        return view("remittance::edit",compact('page','remittance', 'remit_catagories'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $remittance = Remittance::where('id', $id)->firstOrFail();
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "REMITTANCE", 'file_id' => $id]); //create activity log
        
        $datafor_remittance = $data;
        $datafor_remittance['modified_by'] = Auth::user()->id;
        $datafor_remittance['modified_date'] = new Carbon();
        $datafor_remittance = check_case($datafor_remittance, 'edit');        
        DB::table('tbl_remittance')->where('id', $id)->update($datafor_remittance);
        return redirect()->route('admin.remittances')->with('success', "Data updated successfully.");        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_remittance', 'id', $id);
        return redirect()->route('admin.remittances')->with('success', "Data deleted for trash.");

        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_remittance', 'id');
        return redirect()->route('admin.remittances')->with('success', "Data updated Successfully.");
    }

}
