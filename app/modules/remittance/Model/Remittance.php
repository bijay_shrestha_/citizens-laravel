<?php

namespace App\Modules\Remittance\Model;


use Illuminate\Database\Eloquent\Model;

class Remittance extends Model
{
    public  $table = 'tbl_remittance';

    protected $fillable = ['id','type','remittance_company','remittance_product','remittance_receiving_countries','approvel_status','approved','status','del_flag','added_date','added_by','modified_date','modified_by',];
}
