<?php

namespace App\Modules\Orgreference\Model;


use Illuminate\Database\Eloquent\Model;

class Orgreference extends Model
{
    public  $table = 'tbl_orgreference';

    protected $fillable = ['r_id','p_id','reference','post','organization','address','email','telephone',];
}
