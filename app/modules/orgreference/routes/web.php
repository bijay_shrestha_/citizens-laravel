<?php



Route::group(array('prefix'=>'admin/','module'=>'Orgreference','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Orgreference\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('orgreferences/','AdminOrgreferenceController@index')->name('admin.orgreferences');
    Route::post('orgreferences/getorgreferencesJson','AdminOrgreferenceController@getorgreferencesJson')->name('admin.orgreferences.getdatajson');
    Route::get('orgreferences/create','AdminOrgreferenceController@create')->name('admin.orgreferences.create');
    Route::post('orgreferences/store','AdminOrgreferenceController@store')->name('admin.orgreferences.store');
    Route::get('orgreferences/show/{id}','AdminOrgreferenceController@show')->name('admin.orgreferences.show');
    Route::get('orgreferences/edit/{id}','AdminOrgreferenceController@edit')->name('admin.orgreferences.edit');
    Route::match(['put', 'patch'], 'orgreferences/update/{id}','AdminOrgreferenceController@update')->name('admin.orgreferences.update');
    Route::get('orgreferences/delete/{id}', 'AdminOrgreferenceController@destroy')->name('admin.orgreferences.edit');
});




Route::group(array('module'=>'Orgreference','namespace' => 'App\Modules\Orgreference\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('orgreferences/','OrgreferenceController@index')->name('orgreferences');
    
});