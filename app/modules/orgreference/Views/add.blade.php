@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Orgreferences   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.orgreferences') }}">tbl_orgreference</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.orgreferences.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                                    <label for="r_id">R_id</label><input type="text" name="r_id" id="r_id" class="form-control" ></div><div class="form-group">
                                    <label for="p_id">P_id</label><input type="text" name="p_id" id="p_id" class="form-control" ></div><div class="form-group">
                                    <label for="reference">Reference</label><input type="text" name="reference" id="reference" class="form-control" ></div><div class="form-group">
                                    <label for="post">Post</label><input type="text" name="post" id="post" class="form-control" ></div><div class="form-group">
                                    <label for="organization">Organization</label><input type="text" name="organization" id="organization" class="form-control" ></div><div class="form-group">
                                    <label for="address">Address</label><input type="text" name="address" id="address" class="form-control" ></div><div class="form-group">
                                    <label for="email">Email</label><input type="text" name="email" id="email" class="form-control" ></div><div class="form-group">
                                    <label for="telephone">Telephone</label><input type="text" name="telephone" id="telephone" class="form-control" ></div>
<input type="hidden" name="id" id="id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.orgreferences') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
