@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Orgreferences   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.orgreferences') }}">tbl_orgreference</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.orgreferences.update') }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                                    <label for="r_id">R_id</label><input type="text" value = "{{$orgreference->r_id}}"  name="r_id" id="r_id" class="form-control" ></div><div class="form-group">
                                    <label for="p_id">P_id</label><input type="text" value = "{{$orgreference->p_id}}"  name="p_id" id="p_id" class="form-control" ></div><div class="form-group">
                                    <label for="reference">Reference</label><input type="text" value = "{{$orgreference->reference}}"  name="reference" id="reference" class="form-control" ></div><div class="form-group">
                                    <label for="post">Post</label><input type="text" value = "{{$orgreference->post}}"  name="post" id="post" class="form-control" ></div><div class="form-group">
                                    <label for="organization">Organization</label><input type="text" value = "{{$orgreference->organization}}"  name="organization" id="organization" class="form-control" ></div><div class="form-group">
                                    <label for="address">Address</label><input type="text" value = "{{$orgreference->address}}"  name="address" id="address" class="form-control" ></div><div class="form-group">
                                    <label for="email">Email</label><input type="text" value = "{{$orgreference->email}}"  name="email" id="email" class="form-control" ></div><div class="form-group">
                                    <label for="telephone">Telephone</label><input type="text" value = "{{$orgreference->telephone}}"  name="telephone" id="telephone" class="form-control" ></div>
<input type="hidden" name="id" id="id" value = "{{$orgreference->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.orgreferences') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
