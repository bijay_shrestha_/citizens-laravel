<?php



Route::group(array('prefix'=>'admin/','module'=>'source_of_fund','middleware' => ['web','auth'], 'namespace' => 'App\modules\source_of_fund\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('source_of_funds/','AdminSource_of_fundController@index')->name('admin.source_of_funds');
    Route::post('source_of_funds/getsource_of_fundsJson','AdminSource_of_fundController@getsource_of_fundsJson')->name('admin.source_of_funds.getdatajson');
    Route::get('source_of_funds/create','AdminSource_of_fundController@create')->name('admin.source_of_funds.create');
    Route::post('source_of_funds/store','AdminSource_of_fundController@store')->name('admin.source_of_funds.store');
    Route::get('source_of_funds/show/{id}','AdminSource_of_fundController@show')->name('admin.source_of_funds.show');
    Route::get('source_of_funds/edit/{id}','AdminSource_of_fundController@edit')->name('admin.source_of_funds.edit');
    Route::match(['put', 'patch'], 'source_of_funds/update/{id}','AdminSource_of_fundController@update')->name('admin.source_of_funds.update');
    Route::get('source_of_funds/delete/{id}', 'AdminSource_of_fundController@destroy')->name('admin.source_of_funds.edit');
});




Route::group(array('module'=>'source_of_fund','namespace' => 'App\modules\source_of_fund\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('source_of_funds/','Source_of_fundController@index')->name('source_of_funds');
    
});