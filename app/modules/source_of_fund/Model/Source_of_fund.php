<?php

namespace App\Modules\Source_of_fund\Model;


use Illuminate\Database\Eloquent\Model;

class Source_of_fund extends Model
{
    public  $table = 'tbl_source_of_fund';

    protected $fillable = ['id','source_of_fund','show_or_hide','del_flag',];
}
