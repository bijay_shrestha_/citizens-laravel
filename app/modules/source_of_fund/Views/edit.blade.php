@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Source_of_funds   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.source_of_funds') }}"> Source Of Fund</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.source_of_funds.update', $source_of_fund->id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                    <label for="source_of_fund">Source Of Fund</label><input type="text" value = "{{$source_of_fund->source_of_fund}}"  name="source_of_fund" id="source_of_fund" class="form-control" required >
                </div>


                <div class="form-group">
                    <label for="show_or_hide">Show Or Hide</label>
                    <div class="radio">
                    <label>
                        <input type="radio" value="1" name="show_or_hide" id="show_or_hide1" @if($source_of_fund->show_or_hide == 1) checked @endif  />Yes
                    </label> 
                    <label>
                        <input type="radio" value="0" name="show_or_hide" id="show_or_hide0" @if($source_of_fund->show_or_hide == 1) @else  checked @endif  />No
                    </label>
                    </div>
                </div>
<input type="hidden" name="id" id="id" value = "{{$source_of_fund->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.source_of_funds') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
