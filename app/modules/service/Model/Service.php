<?php

namespace App\Modules\Service\Model;


use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public  $table = 'tbl_service';

    protected $fillable = ['service_id','service_name','description','short_description','image_name','slugname','approvel_status','approved','status','banner_collection_id','del_flag','updated_at'];
}
