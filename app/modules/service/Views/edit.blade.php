@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Services   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.services') }}">tbl_service</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.services.update', $service->service_id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                                        <input type="hidden" value = "{{$service->service_id}}"  name="service_id" id="service_id" class="form-control" >

                                    <div class="form-group">
                                        <label for="service_name">Service Name</label>
                                        <input type="text" value = "{{$service->service_name}}"  name="service_name" id="service_name" class="form-control" >
                                    </div>

                                    <div class="form-group">
                                        <label for="description">Description</label><textarea name="description" id="description" class="form-control my-editor" >{!! $service->description !!}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="short_description">Short_description</label><textarea name="short_description" id="short_description" class="form-control my-editor" >{!! $service->short_description !!}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="image_name">Image </label><br>
                                        <label id="upload_image_name" style="display:none"></label>
                                        <div id="delete_old_image_requested"></div>
                                        <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                                        <div id="image-status"></div>
                                        <input type="file" id="upload_image" name="upload_image" @if (file_exists(public_path().'/uploads/service/thumb/'. $service->image_name) && $service->image_name) style="display:none" @else style="display:block" @endif />
                                        <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                                        <div id="imagedata">
                                        @if (file_exists(public_path().'/uploads/service/'. $service->image_name) && $service->image_name)                                   
                                            <img src="{{url('uploads/service/thumb/'.$service->image_name)}}">
                                            <a href="javascript:void(0)" id="change-image_option" title="Change Image">&nbsp; &nbsp; <img src="{{('/images/logo/cancel.png')}}" border="0"/> Change the old image</a>
                                        @endif                                
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="status">Status</label>
                                        <div class="radio">
                                        <label>
                                            <input type="radio" value="1" name="status" id="status1" @if($service->status == 1) checked @endif  />Yes
                                        </label> 
                                        <label>
                                            <input type="radio" value="0" name="status" id="status0" @if($service->status == 1) @else  checked @endif  />No
                                        </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="banner_collection_id">Banner Collection </label>
                                        <select name="banner_collection_id" class="form-control">
                                            <option value="0">Select Banner </option> 
                                            @foreach ($banner_collections as $banner_collection)
                                                <option value="{{$banner_collection['banner_collection_id']}}" 
                                                 @if ($service->banner_collection_id == $banner_collection['banner_collection_id']) selected @endif
                                                > {{$banner_collection['name']}} </option>
                                            @endforeach
                                        </select>
                                    </div>

                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.services') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
    <script type="text/javascript">
        var directory = 'service';
    </script>
@include('data_changing_script_edit')
@include('file_changing_script')

@endsection
