<?php

namespace App\modules\service\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\service\Model\Service;
use App\modules\banner_collection\Model\Banner_collection;
use App\modules\account\Controllers\AdminAccountController;
use Carbon\Carbon;
use File;


class AdminServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Service';
        return view("service::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getservicesJson(Request $request)
    {
        $service = new Service;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $service->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('service_id', 'DSC')->get();

        // Display limited list        
        $rows = $service->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->orderBy('service_id', 'DSC')->get();

        //To count the total values present
        $total = $service->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Service | Create';
        $banner_collections = Banner_collection::all();
        return view("service::add",compact('page', 'banner_collections'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $admin_account_controller = new AdminAccountController;
        $name_slug = $admin_account_controller->slugify($data['service_name']);
        $slug_name = $admin_account_controller->validate_slug($name_slug);
        $slug_data = DB::table('slugs')->insert(['slug_name' => $slug_name]); //create slug
        $allslug =  DB::table('slugs')->get();
        $dataforservice = $data;
        unset($dataforservice['upload_image']);
        $slug_id = $allslug[count($allslug)-1]->slug_id;
        $dataforservice['slugname'] = $slug_name;
        $dataforservice = check_case($dataforservice, 'create');
        $success = Service::Insert($dataforservice);
        $services = Service::all();
        $service_id = $services[count($services)-1]['service_id'];
        $slug['route'] = 'service/details/' . $service_id;
        DB::table('slugs')->where('slug_id', $slug_id)->update(['route' => $slug['route']]);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "SERVICE", 'file_id' => $service_id]); //create acrivity log     
        
        return redirect()->route('admin.services')->with('success', "Data added Successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::where('service_id',$id)->firstOrFail();
        $page['title'] = 'Service | Update';
        $banner_collections = Banner_collection::all();
        return view("service::edit",compact('page','service', 'banner_collections'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $admin_account_controller = new AdminAccountController;
        $name_slug = $admin_account_controller->slugify($data['service_name']);
        $slug_name = $admin_account_controller->validate_slug($name_slug);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "SERVICE", 'file_id' => $id]); //create activity log
        $service = Service::where('service_id', $id)->firstOrFail();
        $slug['slug_name'] = $admin_account_controller->validate_slug($name_slug, $service->slug_id);
        $slug['route'] = 'service/details/'.$data['service_id'];         
        DB::table('slugs')->where('slug_id', $service->slug_id)->update(['slug_name' =>  $slug['slug_name'], 'route' => $slug['route']]);
        $dataforservice = $data;
        unset($dataforservice['upload_image']);
        $dataforservice['slugname'] = $slug['slug_name'];

        if (array_key_exists('delete_old_image', $data)) {
            if ((file_exists(public_path().'/uploads/service/'. $service->image_name))  && $service->image_name) {
                unlink(public_path().'/uploads/service/'. $service->image_name);
                if (file_exists(public_path().'/uploads/service/thumb/'. $service->image_name)) {
                    unlink(public_path().'/uploads/service/thumb/'. $service->image_name);
                } 
            }
            if (!$data['image_name']) {
                $data['image_name'] = '';
                $dataforservice['image_name'] = '';
            }
            unset($dataforservice['delete_old_image']);
        } else {
            if (!$data['image_name']) {
                $data['image_name'] = $service->image_name;
                $dataforservice['image_name'] = $service->image_name;
            }
        }
        $dataforservice = check_case($dataforservice, 'edit');
        //$data['updated_at'] = new Carbon();
        //$success = Service::where('service_id', $id)->update($data)->exlcude(['updated_at']);
        DB::table('tbl_service')->where('service_id', $id)->update($dataforservice);
        return redirect()->route('admin.services');
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_service', 'service_id', $id);     
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "SERVICE", 'file_id' => $id]); //create activity log   
        return redirect()->route('admin.services')->with('success', "Data updated Successfully.");

        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_service', 'service_id');
        return redirect()->route('admin.services')->with('success', "Data changed Successfully.");
    }


}
