<?php



Route::group(array('prefix'=>'admin/','module'=>'service','middleware' => ['web','auth'], 'namespace' => 'App\modules\service\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('services/','AdminServiceController@index')->name('admin.services');
    Route::post('services/getservicesJson','AdminServiceController@getservicesJson')->name('admin.services.getdatajson');
    Route::get('services/create','AdminServiceController@create')->name('admin.services.create');
    Route::post('services/store','AdminServiceController@store')->name('admin.services.store');
    Route::get('services/show/{id}','AdminServiceController@show')->name('admin.services.show');
    Route::get('services/edit/{id}','AdminServiceController@edit')->name('admin.services.edit');
    Route::match(['put', 'patch'], 'services/update/{id}','AdminServiceController@update')->name('admin.services.update');
    Route::get('services/delete/{id}', 'AdminServiceController@destroy')->name('admin.services.edit');
    Route::get('services/change/{text}/{status}/{id}', 'AdminServiceController@change');
});




Route::group(array('module'=>'Service','namespace' => 'App\modules\service\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('services/','ServiceController@index')->name('services');
    
});