@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Working_experiences		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Working_experiences</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.working_experiences.create') }}" class="btn bg-green waves-effect"  title="create">Create</a>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="working_experience-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								<th >W_id</th>
<th >P_id</th>
<th >Organization</th>
<th >Position</th>
<th >Department</th>
<th >Experience</th>
<th >Organization2</th>
<th >Organization3</th>
<th >Position2</th>
<th >Position3</th>
<th >Department2</th>
<th >Department3</th>
<th >Experience2</th>
<th >Experience3</th>

								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#working_experience-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.working_experiences.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
					{ data: "w_id",name: "w_id"},
            { data: "p_id",name: "p_id"},
            { data: "organization",name: "organization"},
            { data: "position",name: "position"},
            { data: "department",name: "department"},
            { data: "experience",name: "experience"},
            { data: "organization2",name: "organization2"},
            { data: "organization3",name: "organization3"},
            { data: "position2",name: "position2"},
            { data: "position3",name: "position3"},
            { data: "department2",name: "department2"},
            { data: "department3",name: "department3"},
            { data: "experience2",name: "experience2"},
            { data: "experience3",name: "experience3"},
            
					{ data: function(data,b,c,table) { 
					var buttons = '';

					buttons += "<a class='btn bg-red waves-effect' href='"+site_url+"/edit/"+data.id+"' type='button' >Edit</a>&nbsp"; 

					buttons += "<a href='"+site_url+"/delete/"+data.id+"' class='btn bg-red waves-effect' >Delete</a>";

					return buttons;
					}, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
