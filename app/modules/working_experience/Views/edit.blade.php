@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Working_experiences   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.working_experiences') }}">tbl_working_experience</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.working_experiences.update') }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                                    <label for="w_id">W_id</label><input type="text" value = "{{$working_experience->w_id}}"  name="w_id" id="w_id" class="form-control" ></div><div class="form-group">
                                    <label for="p_id">P_id</label><input type="text" value = "{{$working_experience->p_id}}"  name="p_id" id="p_id" class="form-control" ></div><div class="form-group">
                                    <label for="organization">Organization</label><input type="text" value = "{{$working_experience->organization}}"  name="organization" id="organization" class="form-control" ></div><div class="form-group">
                                    <label for="position">Position</label><input type="text" value = "{{$working_experience->position}}"  name="position" id="position" class="form-control" ></div><div class="form-group">
                                    <label for="department">Department</label><input type="text" value = "{{$working_experience->department}}"  name="department" id="department" class="form-control" ></div><div class="form-group">
                                    <label for="experience">Experience</label><input type="text" value = "{{$working_experience->experience}}"  name="experience" id="experience" class="form-control" ></div><div class="form-group">
                                    <label for="organization2">Organization2</label><input type="text" value = "{{$working_experience->organization2}}"  name="organization2" id="organization2" class="form-control" ></div><div class="form-group">
                                    <label for="organization3">Organization3</label><input type="text" value = "{{$working_experience->organization3}}"  name="organization3" id="organization3" class="form-control" ></div><div class="form-group">
                                    <label for="position2">Position2</label><input type="text" value = "{{$working_experience->position2}}"  name="position2" id="position2" class="form-control" ></div><div class="form-group">
                                    <label for="position3">Position3</label><input type="text" value = "{{$working_experience->position3}}"  name="position3" id="position3" class="form-control" ></div><div class="form-group">
                                    <label for="department2">Department2</label><input type="text" value = "{{$working_experience->department2}}"  name="department2" id="department2" class="form-control" ></div><div class="form-group">
                                    <label for="department3">Department3</label><input type="text" value = "{{$working_experience->department3}}"  name="department3" id="department3" class="form-control" ></div><div class="form-group">
                                    <label for="experience2">Experience2</label><input type="text" value = "{{$working_experience->experience2}}"  name="experience2" id="experience2" class="form-control" ></div><div class="form-group">
                                    <label for="experience3">Experience3</label><input type="text" value = "{{$working_experience->experience3}}"  name="experience3" id="experience3" class="form-control" ></div>
<input type="hidden" name="id" id="id" value = "{{$working_experience->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.working_experiences') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
