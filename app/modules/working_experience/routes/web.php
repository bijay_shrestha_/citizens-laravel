<?php



Route::group(array('prefix'=>'admin/','module'=>'Working_experience','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Working_experience\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('working_experiences/','AdminWorking_experienceController@index')->name('admin.working_experiences');
    Route::post('working_experiences/getworking_experiencesJson','AdminWorking_experienceController@getworking_experiencesJson')->name('admin.working_experiences.getdatajson');
    Route::get('working_experiences/create','AdminWorking_experienceController@create')->name('admin.working_experiences.create');
    Route::post('working_experiences/store','AdminWorking_experienceController@store')->name('admin.working_experiences.store');
    Route::get('working_experiences/show/{id}','AdminWorking_experienceController@show')->name('admin.working_experiences.show');
    Route::get('working_experiences/edit/{id}','AdminWorking_experienceController@edit')->name('admin.working_experiences.edit');
    Route::match(['put', 'patch'], 'working_experiences/update/{id}','AdminWorking_experienceController@update')->name('admin.working_experiences.update');
    Route::get('working_experiences/delete/{id}', 'AdminWorking_experienceController@destroy')->name('admin.working_experiences.edit');
});




Route::group(array('module'=>'Working_experience','namespace' => 'App\Modules\Working_experience\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('working_experiences/','Working_experienceController@index')->name('working_experiences');
    
});