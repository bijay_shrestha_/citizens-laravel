<?php

namespace App\Modules\Working_experience\Model;


use Illuminate\Database\Eloquent\Model;

class Working_experience extends Model
{
    public  $table = 'tbl_working_experience';

    protected $fillable = ['w_id','p_id','organization','position','department','experience','organization2','organization3','position2','position3','department2','department3','experience2','experience3',];
}
