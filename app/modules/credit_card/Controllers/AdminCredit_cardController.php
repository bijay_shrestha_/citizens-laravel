<?php

namespace App\modules\credit_card\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\credit_card\Model\Credit_card;
use App\modules\banner_collection\Model\Banner_collection;
use App\modules\faq_collection\Model\Faq_collection;
use App\modules\account\Controllers\AdminAccountController;
use Carbon\Carbon;
use File;


class AdminCredit_cardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Credit Card';
        return view("credit_card::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getcredit_cardsJson(Request $request)
    {
        $credit_card = new Credit_card;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $credit_card->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('credit_card_id',"DSC")->get();

        // Display limited list        
        $rows = $credit_card->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->orderBy('credit_card_id', "DSC")->get();

        //To count the total values present
        $total = $credit_card->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Credit Card | Create';
        $banner_collections = Banner_collection::all();
        $faq_collections = Faq_collection::all();
        return view("credit_card::add",compact('page', 'banner_collections', 'faq_collections'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
       // dd($data);
        $admin_account_controller = new AdminAccountController;
        $name_slug = $admin_account_controller->slugify($data['credit_card_name']);
        $slug_name = $admin_account_controller->validate_slug($name_slug);
        $slug_data = DB::table('slugs')->insert(['slug_name' => $slug_name]); //create slug
        $allslug =  DB::table('slugs')->get();
        $datafor_cc = $data;
        unset($datafor_cc['download_link']);
        unset($datafor_cc['upload_image']);
        unset($datafor_cc['id']);
        $datafor_cc['slug_id'] = $allslug[count($allslug)-1]->slug_id;
        $datafor_cc['slug_name'] = $slug_name;
        $datafor_cc['added_date'] = new Carbon();
        $datafor_cc['del_flag'] = 0;
        $datafor_cc = check_case($datafor_cc, 'create');


        $success = Credit_card::Insert($datafor_cc);
        $credit_cards = Credit_card::all();
        $credit_card_id = $credit_cards[count($credit_cards)-1]['credit_card_id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "credit_card", 'file_id' => $credit_card_id]); //create acrivity log     
        $slug['route'] = 'credit_card/detail/' . $credit_card_id;
        DB::table('slugs')->where('slug_id', $datafor_cc['slug_id'])->update(['route' => $slug['route']]);
           
        return redirect()->route('admin.credit_cards')->with('success', "Data added Successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $credit_card = Credit_card::where('credit_card_id',$id)->first();
        $page['title'] = 'Credit Card | Update';
        $banner_collections = Banner_collection::all();
        $faq_collections = Faq_collection::all();
        return view("credit_card::edit",compact('page','credit_card', 'banner_collections', 'faq_collections'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $admin_account_controller = new AdminAccountController;
        $name_slug = $admin_account_controller->slugify($data['credit_card_name']);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "credit_card", 'file_id' => $id]); //create activity log
        $credit_card = Credit_card::where('credit_card_id', $id)->firstOrFail();
        $slug['slug_name'] = $admin_account_controller->validate_slug($name_slug, $credit_card->slug_id);
        $slug['route'] = 'credit_card/detail/'.$data['credit_card_id'];         
        DB::table('slugs')->where('slug_id', $credit_card->slug_id)->update(['slug_name' =>  $slug['slug_name'], 'route' => $slug['route']]);
        $datafor_cc = $data;
        unset($datafor_cc['download_link']);
        unset($datafor_cc['upload_image']);
        unset($datafor_cc['id']);
        $datafor_cc['slug_name'] = $slug['slug_name'];
        
        if (array_key_exists('delete_old_image', $data)) {
            if ((file_exists(public_path().'/uploads/credit_card/'. $credit_card->image_name))  && $credit_card->image_name) {
                unlink(public_path().'/uploads/credit_card/'. $credit_card->image_name);
                if (file_exists(public_path().'/uploads/credit_card/thumb/'. $credit_card->image_name)) {
                    unlink(public_path().'/uploads/credit_card/thumb/'. $credit_card->image_name);
                } 
            }
            if (!$data['image_name']) {
                $data['image_name'] = '';
                $datafor_cc['image_name'] = '';
            }
            unset($datafor_cc['delete_old_image']);
        } else {
            if (!$data['image_name']) {
                $data['image_name'] = $credit_card->image_name;
                $datafor_cc['image_name'] = $credit_card->image_name;
            }
        }

        if (array_key_exists('delete_old_file', $data)) {
            if ((file_exists(public_path().'/uploads/credit_card/form/'. $credit_card->download_form_link)) && $credit_card->download_form_link) {
                unlink(public_path().'/uploads/credit_card/form/'. $credit_card->download_form_link);                
            }
            if (!$data['download_form_link']) {
                $data['download_form_link'] = '';
                $datafor_cc['download_form_link'] = '';
            }
            unset($datafor_cc['delete_old_file']);
        } else {
            if (!$data['download_link']) {
                $data['download_form_link'] = $credit_card->download_form_link;
                $datafor_cc['download_form_link'] = $credit_card->download_form_link;
            }            
        }
        $datafor_cc['modified_date'] = new Carbon();
        $datafor_cc = check_case($datafor_cc, 'create');
        $success = DB::table('tbl_credit_cards')->where('credit_card_id', $id)->update($datafor_cc);
        return redirect()->route('admin.credit_cards')->with('success', "Data updated Successfully.");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_credit_cards', 'credit_card_id', $id, 'deleted_by', 'deleted_date');
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "credit_card", 'file_id' => $id]); //create acrivity log     
        return redirect()->route('admin.credit_cards')->with('success', "Data deleted for trash.");
        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_credit_cards', 'credit_card_id', 'deleted_by', 'deleted_date');
        return redirect()->route('admin.credit_cards')->with('success', "Data updated.");
    }



}
