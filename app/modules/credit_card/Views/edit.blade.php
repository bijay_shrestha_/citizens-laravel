@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Credit Cards   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.credit_cards') }}">tbl_credit_cards</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.credit_cards.update', $credit_card->credit_card_id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                                    <div class="form-group">
                                    <input type="hidden" value="{{$credit_card->credit_card_id}}"  name="credit_card_id" id="credit_card_id" class="form-control" ></div>

                                    
                                    <div class="form-group">
                                        <label for="banner_collection_id">Banner Collection </label>
                                        <select name="banner_collection_id" class="form-control">
                                            <option value="0">Select Banner </option> 
                                            @foreach ($banner_collections as $banner_collection)
                                                <option value="{{$banner_collection['banner_collection_id']}}" 
                                                 @if ($credit_card->banner_collection_id == $banner_collection['banner_collection_id']) selected @endif
                                                > {{$banner_collection['name']}} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="faq_collection_id">FAQ</label>
                                        <select name="faq_collection_id" class="form-control">
                                        @foreach($faq_collections as $faq_collection) 
                                                <option value="{{$faq_collection['faq_collection_id']}}"
                                                 @if ($credit_card->faq_collection_id == $faq_collection['faq_collection_id']) selected @endif
                                                >{{$faq_collection['faq_collection_name']}}</option>
                                        @endforeach
                                        </select>
                                    </div>


                                   <div class="form-group">
                                    <label for="credit_card_name">Credit Card Name (*)</label><input type="text" value = "{{$credit_card->credit_card_name}}"  name="credit_card_name" id="credit_card_name" class="form-control" required>
                                   </div>
                                   
                                   <div class="form-group">
                                       <label for="image_name">Image </label><br>
                                       <label id="upload_image_name" style="display:none"></label>
                                        <div id="delete_old_image_requested"></div>
                                       <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                                       <div id="image-status"></div>
                                       <input type="file" id="upload_image" name="upload_image" @if (file_exists(public_path().'/uploads/credit_card/thumb/'. $credit_card->image_name) && $credit_card->image_name) style="display:none" @else style="display:block" @endif />
                                       <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                                       <div id="imagedata">
                                       @if (file_exists(public_path().'/uploads/credit_card/'. $credit_card->image_name) && $credit_card->image_name)                                   
                                           <img src="{{url('uploads/credit_card/thumb/'.$credit_card->image_name)}}">
                                           <a href="javascript:void(0)" id="change-image_option" title="Change Image">&nbsp; &nbsp; <img src="{{('/images/logo/cancel.png')}}" border="0"/> Change the old image</a>
                                       @endif                                
                                       </div>
                                   </div>

                                    <div class="form-group">
                                        <label for="short_description">Short Description</label><textarea  name="short_description" id="short_description" class="form-control my-editor" >{!! $credit_card->short_description !!}</textarea>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="description">Description (*Description must contain atleast 250 Letters )</label><textarea name="description" id="description" class="form-control my-editor" >{!! $credit_card->description !!}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="representative_apr">Representative Apr</label><input type="text" value = "{{$credit_card->representative_apr}}"  name="representative_apr" id="representative_apr" class="form-control">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="purchase_rate">Purchase Rate</label><input type="text" value = "{{$credit_card->purchase_rate}}"  name="purchase_rate" id="purchase_rate" class="form-control" >
                                    </div>
                                    <div class="form-group">
                                        <label for="credit_limit">Credit Limit</label><input type="text" value = "{{$credit_card->credit_limit}}"  name="credit_limit" id="credit_limit" class="form-control" >
                                    </div>
                                    <div class="form-group">
                                              <label>Download Form file</label><br/><label id="upload_download_link" style="display:none"></label>
                                              <div id="delete_old_file_requested"></div>
                                              <input type="file" name="download_link" id="download_link" @if (file_exists(public_path().'/uploads/credit_card/form/'. $credit_card->download_form_link) && $credit_card->download_form_link) style="display:none" @else style="display:block" @endif >
                                              <div id="file-status"></div>
                                              <input type="hidden" name="download_form_link" id="download_form_link"/>
                                              <a href="javascript:void(0)" id="change-file" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                                              <div id="filedata">
                                              @if (file_exists(public_path().'/uploads/credit_card/form/'. $credit_card->download_form_link) && $credit_card->download_form_link)
                                              
                                                  <a  href="{{url('uploads/credit_card/form/'.$credit_card->download_form_link)}}" target="_blank">{{$credit_card->download_form_link}}</a>
                                                  <a href="javascript:void(0)" id="change-file_option" title="Change file">&nbsp; &nbsp; <img src="{{('/images/logo/cancel.png')}}" border="0"/> Change the old file</a>
                                              @endif                                
                                              </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="sequence">Sequence</label><input type="text" value = "{{$credit_card->sequence}}"  name="sequence" id="sequence" class="form-control" >
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="notes">Notes</label>
                                        <textarea name="notes" id="notes" class="form-control my-editor" >{!! $credit_card->notes !!}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="status">Status</label>
                                        <div class="radio">
                                        <label>
                                            <input type="radio" value="1" name="status" id="status1" @if($credit_card->status == 1) checked @endif  />Yes
                                        </label> 
                                        <label>
                                            <input type="radio" value="0" name="status" id="status0" @if($credit_card->status == 1) @else  checked @endif  />No
                                        </label>
                                        </div>
                                    </div>
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.credit_cards') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
    <script type="text/javascript">
        var directory = 'credit_card';
    </script>
@include('data_changing_script_edit')
@include('file_changing_script')

@endsection
