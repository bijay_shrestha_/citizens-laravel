<?php

namespace App\Modules\Credit_card\Model;


use Illuminate\Database\Eloquent\Model;

class Credit_card extends Model
{
    public  $table = 'tbl_credit_cards';

    protected $fillable = ['credit_card_id','parent_id','template_id','business_type','credit_card_name','slug_id','slug_name','faq_collection_id','banner_collection_id','image_name','description','short_description','representative_apr','purchase_rate','credit_limit','download_form_link','apply_online_link','sequence','approvel_status','approved','added_date','modified_date','del_flag','deleted_by','deleted_date','notes','status',];
}
