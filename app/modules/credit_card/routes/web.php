<?php



Route::group(array('prefix'=>'admin/','module'=>'credit_card','middleware' => ['web','auth'], 'namespace' => 'App\modules\credit_card\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('credit_cards/','AdminCredit_cardController@index')->name('admin.credit_cards');
    Route::post('credit_cards/getcredit_cardsJson','AdminCredit_cardController@getcredit_cardsJson')->name('admin.credit_cards.getdatajson');
    Route::get('credit_cards/create','AdminCredit_cardController@create')->name('admin.credit_cards.create');
    Route::post('credit_cards/store','AdminCredit_cardController@store')->name('admin.credit_cards.store');
    Route::get('credit_cards/show/{id}','AdminCredit_cardController@show')->name('admin.credit_cards.show');
    Route::get('credit_cards/edit/{id}','AdminCredit_cardController@edit')->name('admin.credit_cards.edit');
    Route::match(['put', 'patch'], 'credit_cards/update/{id}','AdminCredit_cardController@update')->name('admin.credit_cards.update');
    Route::get('credit_cards/delete/{id}', 'AdminCredit_cardController@destroy')->name('admin.credit_cards.edit');
    Route::get('credit_cards/change/{text}/{status}/{id}', 'AdminCredit_cardController@change');

});




Route::group(array('module'=>'credit_card','namespace' => 'App\modules\credit_card\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('credit_cards/','Credit_cardController@index')->name('credit_cards');
    
});