<?php



Route::group(array('prefix'=>'admin/','module'=>'media_contact','middleware' => ['web','auth'], 'namespace' => 'App\modules\media_contact\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('media_contacts/','AdminMedia_contactController@index')->name('admin.media_contacts');
    Route::post('media_contacts/getmedia_contactsJson','AdminMedia_contactController@getmedia_contactsJson')->name('admin.media_contacts.getdatajson');
    Route::get('media_contacts/create','AdminMedia_contactController@create')->name('admin.media_contacts.create');
    Route::post('media_contacts/store','AdminMedia_contactController@store')->name('admin.media_contacts.store');
    Route::get('media_contacts/show/{id}','AdminMedia_contactController@show')->name('admin.media_contacts.show');
    Route::get('media_contacts/edit/{id}','AdminMedia_contactController@edit')->name('admin.media_contacts.edit');
    Route::match(['put', 'patch'], 'media_contacts/update/{id}','AdminMedia_contactController@update')->name('admin.media_contacts.update');
    Route::get('media_contacts/delete/{id}', 'AdminMedia_contactController@destroy')->name('admin.media_contacts.edit');
    Route::get('media_contacts/change/{text}/{status}/{id}', 'AdminMedia_contactController@change');
});




Route::group(array('module'=>'Media_contact','namespace' => 'App\Modules\Media_contact\Controllers', 'middleware' => 'web'), function() {
    //Your routes belong to this module.
    Route::get('media_contacts/','Media_contactController@index')->name('media_contacts');
    
});