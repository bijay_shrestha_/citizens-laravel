<?php

namespace App\Modules\Media_contact\Model;


use Illuminate\Database\Eloquent\Model;

class Media_contact extends Model
{
    public  $table = 'tbl_media_contact';

    protected $fillable = ['blog_id','blog_title','tags','slug_id','slug_name','image_name','contents','approvel_status','approved','added_date','added_by','post_date','modified_date','status','del_flag','deleted_by','deleted_date','sequence',];
}
