<?php

namespace App\modules\media_contact\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\media_contact\Model\Media_contact;
use App\modules\account\Controllers\AdminAccountController;
use Carbon\Carbon;


class AdminMedia_contactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Media contact';
        return view("media_contact::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getmedia_contactsJson(Request $request)
    {
        $media_contact = new Media_contact;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $media_contact->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('blog_id', "DSC")->get();

        // Display limited list        
        $rows = $media_contact->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->orderBy('blog_id', "DSC")->get();

        //To count the total values present
        $total = $media_contact->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Media Contact | Create';
        return view("media_contact::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $admin_account_controller = new AdminAccountController;
        $name_slug = $admin_account_controller->slugify($data['blog_title']);
        $slug_name = $admin_account_controller->validate_slug($name_slug);
        $slug_data = DB::table('slugs')->insert(['slug_name' => $slug_name]); //create slug
        $allslug =  DB::table('slugs')->get();
        $datafor_m_contact = $data;
        unset($datafor_m_contact['upload_image']);
        unset($datafor_m_contact['tags']);

        $datafor_m_contact['slug_id'] = $allslug[count($allslug)-1]->slug_id;
        $datafor_m_contact['slug_name'] = $slug_name;
        $datafor_m_contact['del_flag'] = 0;
        $datafor_m_contact['added_by'] = Auth::user()->id;
        $datafor_m_contact['sequence'] = ($datafor_m_contact['sequence'] == null) ? "0":$datafor_m_contact['sequence'];
        $datafor_m_contact['added_date'] = new Carbon();
        $datafor_m_contact = check_case($datafor_m_contact, 'create');        


        $success = Media_contact::Insert($datafor_m_contact); 
        $media_contacts = Media_contact::all();
        $media_contact_id = $media_contacts[count($media_contacts)-1]['blog_id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "MEDIA_CONTACT", 'file_id' => $media_contact_id]); //create activity log
        $slug['route'] = 'media_contact/detail/' . $media_contact_id;
        DB::table('slugs')->where('slug_id', $datafor_m_contact['slug_id'])->update(['route' => $slug['route']]);

        /* needed only when tags are needed
        $tags = explode(',', $data['tags']);
        foreach($tags as $tag)
        {       
            DB::table('tbl_tags')->insert(['section' => "media_contact", 'section_id' => $media_contact_id, 'tag_title' => $tag]);
        }*/
        return redirect()->route('admin.media_contacts')->with('success', "Data inserted successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $media_contact = Media_contact::where('blog_id',$id)->firstOrFail();
        $page['title'] = 'Media Contact | Update';
        
        $media_contact_tags = '';
        /*
        $tags = DB::table('tbl_tags')->where([['section_id',$id], ['section', 'media_contact']])->get();
        if ($tags) {
            foreach($tags as $tag) {
                $media_contact_tag[] = $tag->tag_title; 
            }
            $media_contact_tags = implode(',', $media_contact_tag);
        }
        */
        return view("media_contact::edit",compact('page','media_contact', 'media_contact_tags'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        //dd($data);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "MEDIA_CONTACT", 'file_id' => $id]); //create activity log
        $media_contact = Media_contact::where('blog_id', $id)->firstOrFail();
        $admin_account_controller = new AdminAccountController;
        $name_slug = $admin_account_controller->slugify($data['blog_title']);
        $slug['slug_name'] = $admin_account_controller->validate_slug($name_slug, $media_contact->slug_id);
        $slug['route'] = 'media_contact/detail/'.$id;         //$data['loan_id'] = $id
        DB::table('slugs')->where('slug_id', $media_contact->slug_id)->update(['slug_name' =>  $slug['slug_name'], 'route' => $slug['route']]);                
        $datafor_m_contact = $data;
        unset($datafor_m_contact['upload_image']);
        unset($datafor_m_contact['tags']);

        $datafor_m_contact['slug_name'] = $slug['slug_name'];
        
        if (array_key_exists('delete_old_image', $data)) {
            if ((file_exists(public_path().'/uploads/media_contact/'. $media_contact->image_name))  && $media_contact->image_name) {
                unlink(public_path().'/uploads/media_contact/'. $media_contact->image_name);
                if (file_exists(public_path().'/uploads/media_contact/thumb/'. $media_contact->image_name)) {
                    unlink(public_path().'/uploads/media_contact/thumb/'. $media_contact->image_name);
                } 
            }
            if (!$data['image_name']) {
                $data['image_name'] = '';
                $datafor_m_contact['image_name'] = '';
            }
            unset($datafor_m_contact['delete_old_image']);
        } else {
            if (!$data['image_name']) {
                $data['image_name'] = $media_contact->image_name;
                $datafor_m_contact['image_name'] = $media_contact->image_name;
            }
        }
        $datafor_m_contact['modified_date'] = new Carbon();
        $datafor_m_contact = check_case($datafor_m_contact, 'edit');        
        DB::table('tbl_media_contact')->where('blog_id', $id)->update($datafor_m_contact);

        //for tags table        
        /*$tags_data = DB::table('tbl_tags')->where([['section_id',$id], ['section', 'media_contact']])->delete();
        if (array_key_exists('tags', $data)) {
            $tags = explode(',', $data['tags']);
            foreach($tags as $tag)
            {       
                DB::table('tbl_tags')->insert(['section' => "media_contact", 'section_id' => $id, 'tag_title' => $tag]);
            }
            
        }*/        
        
        return redirect()->route('admin.media_contacts')->with('success', "Data updated successfully.");
        
        //
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_media_contact', 'blog_id', $id, 'deleted_by', 'deleted_date');
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "MEDIA_CONTACT", 'file_id' => $id]); //create acrivity log
        return redirect()->route('admin.media_contacts')->with('success', "Data deleted successfully.");
        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_media_contact', 'blog_id', 'deleted_by', 'deleted_date');
        return redirect()->route('admin.media_contacts')->with('success', "Data updated successfully.");
    }

}
