<?php

namespace App\modules\media_contact\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\banner\Model\Banner;

class Media_contactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'media_contacts';
        //
        $mediacontacts = get_lists('tbl_media_contact');
        $banners = Banner::where([['del_flag', 0], ['status', 1],['banner_collection_id', 130]])->get()->toArray();        
         return view('front.media_contact.index', compact('mediacontacts', 'banners'));
    }

}
