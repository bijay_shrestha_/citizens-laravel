@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Media Contacts		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Media Contacts</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.media_contacts.create') }}" class="btn bg-green waves-effect"  title="create">Create</a>
					</div>
					<!-- /.box-header -->
					<br>
					@if ($message = Session::get('success'))
					<div class="btn btn-success">
						{{$message}}
					</div>
					@endif
					<div class="box-body">
						<table id="media_contact-datatable" class="table table-striped table-bordered">
							<thead>
							<th>SN</th>
							<th >Title</th>
							<th >Image </th>
							<th >Approvel Status</th>
							<th >Approved</th>
							<th >Status</th>
							<th >Sequence</th>
							<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#media_contact-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.media_contacts.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
            { data: "blog_title",name: "blog_title"},
              { data: function (data, b,c, table){
              	var imagefile = '';
              	if (data.image_name) {
              		imagefile += '<img src="{{url("/uploads/media_contact/thumb/")}}/'+data.image_name+'">';
              	}
              	return imagefile;

              }, name:'image_name', searchable: false},
              { data: "approvel_status",name: "approvel_status"},
             { data: function (data)  {
             	var content = "";

             	content += "<a href='{{url('/admin/media_contacts/change/')}}/approved/" + data.approvel_status + "/" + data.blog_id + "'style='color:";
             	if (data.approved == "approved") { content += "green"; }
             	content += "'>Approved</a><br>";
             	
             	content += "<a href='{{url('/admin/media_contacts/change/')}}/pending/" + data.approvel_status + "/" + data.blog_id + "'style='color:";
             	if (data.approved == "pending") { content += "green"; }
             	content += "'>Pending</a><br>";

             	content += "<a href='{{url('/admin/media_contacts/change/')}}/denied/" + data.approvel_status + "/" + data.blog_id + "'style='color:";
             	if (data.approved == "denied") { content += "green"; }
             	content += "'>Denied</a><br>";
             	
             	return content;
             },name:'approved', searchable: false},
            { data: function(data){
            	if (data.status == 1) {
            		return "<img src='{{url('/images/logo/accept.png')}}'>";
            	} else {
            		return "<img src='{{url('/images/logo/cancel.png')}}'>";
            	}
            	;
            }, name:'status', searchable: false},
              { data: "sequence",name: "sequence"},
                     
              { data: function(data,b,c,table) { 
              var buttons = '';

              buttons += "<a class='btn bg-green waves-effect' href='"+site_url+"/edit/"+data.blog_id+"' type='button' ><i class='fa fa-edit'></i> Edit</a>&nbsp"; 

              buttons += "<a href='"+site_url+"/delete/"+data.blog_id+"' class='btn bg-red waves-effect' ><i class='fa fa-trash'></i>  Delete</a>";

              return buttons;
              }, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
