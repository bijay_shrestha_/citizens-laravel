<?php



Route::group(array('prefix'=>'admin/','module'=>'official_notice','middleware' => ['web','auth'], 'namespace' => 'App\modules\official_notice\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('official_notices/','AdminOfficial_noticeController@index')->name('admin.official_notices');
    Route::post('official_notices/getofficial_noticesJson','AdminOfficial_noticeController@getofficial_noticesJson')->name('admin.official_notices.getdatajson');
    Route::get('official_notices/create','AdminOfficial_noticeController@create')->name('admin.official_notices.create');
    Route::post('official_notices/store','AdminOfficial_noticeController@store')->name('admin.official_notices.store');
    Route::get('official_notices/show/{id}','AdminOfficial_noticeController@show')->name('admin.official_notices.show');
    Route::get('official_notices/edit/{id}','AdminOfficial_noticeController@edit')->name('admin.official_notices.edit');
    Route::match(['put', 'patch'], 'official_notices/update/{id}','AdminOfficial_noticeController@update')->name('admin.official_notices.update');
    Route::get('official_notices/delete/{id}', 'AdminOfficial_noticeController@destroy')->name('admin.official_notices.edit');
    Route::get('official_notices/change/{text}/{status}/{id}', 'AdminOfficial_noticeController@change');
});




Route::group(array('module'=>'official_notice','namespace' => 'App\modules\official_notice\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('official_notices/','Official_noticeController@index')->name('official_notices');
    
});