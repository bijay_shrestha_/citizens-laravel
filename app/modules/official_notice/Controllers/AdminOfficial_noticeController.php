<?php

namespace App\modules\official_notice\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\official_notice\Model\Official_notice;
use App\modules\account\Controllers\AdminAccountController;
use Carbon\Carbon;

class AdminOfficial_noticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Official Notice';
        return view("official_notice::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getofficial_noticesJson(Request $request)
    {
        $official_notice = new Official_notice;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $official_notice->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('blog_id', 'DSC')->get();

        // Display limited list        
        $rows = $official_notice->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->orderBy('blog_id', 'DSC')->get();

        //To count the total values present
        $total = $official_notice->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Official Notice | Create';
        return view("official_notice::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $admin_account_controller = new AdminAccountController;
        $name_slug = $admin_account_controller->slugify($data['blog_title']);
        $slug_name = $admin_account_controller->validate_slug($name_slug);
        $slug_data = DB::table('slugs')->insert(['slug_name' => $slug_name]); //create slug
        $allslug =  DB::table('slugs')->get();
        $datafor_on = $data;
        unset($datafor_on['upload_image']);
        $slug_id = $allslug[count($allslug)-1]->slug_id;

        $datafor_on['sequence'] = $datafor_on['sequence'] ? $datafor_on['sequence']: 0;
        $datafor_on['slug_name'] = $slug_name;
        $datafor_on['slug_id'] = $slug_id;
        $datafor_on['added_date'] = new Carbon();
        $datafor_on['added_by'] = Auth::user()->id;
        $datafor_on['del_flag'] = 0;
        $datafor_on = check_case($datafor_on, 'create');        
        $success = Official_notice::Insert($datafor_on);

        $official_notices = Official_notice::all();
        $official_notice_id = $official_notices[count($official_notices)-1]['blog_id'];
        $slug['route'] = 'official_notice/detail/' . $official_notice_id;
        DB::table('slugs')->where('slug_id', $slug_id)->update(['route' => $slug['route']]);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "OFFICIAL_NOTICES", 'file_id' => $official_notice_id]); //create acrivity log     
     
        return redirect()->route('admin.official_notices')->with('success', "Data added successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $official_notice = Official_notice::where('blog_id', $id)->firstOrFail();
        $page['title'] = 'Official Notice | Update';
        return view("official_notice::edit",compact('page','official_notice'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $admin_account_controller = new AdminAccountController;
        $name_slug = $admin_account_controller->slugify($data['blog_title']);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "OFFICIAL_NOTICES", 'file_id' => $id]); //create activity log

        $official_notice = Official_notice::where('blog_id', $id)->firstOrFail();
        $slug['slug_name'] = $admin_account_controller->validate_slug($name_slug, $official_notice->slug_id);
        $slug['route'] = 'official_notice/detail/'.$data['blog_id'];         
        DB::table('slugs')->where('slug_id', $official_notice->slug_id)->update(['slug_name' =>  $slug['slug_name'], 'route' => $slug['route']]);
        $datafor_on = $data;
        $datafor_on['sequence'] = $datafor_on['sequence'] ? $datafor_on['sequence']: 0;
        unset($datafor_on['upload_image']);
        $datafor_on['slug_name'] = $slug['slug_name'];
        
        if (array_key_exists('delete_old_image', $data)) {
            if ((file_exists(public_path().'/uploads/official_notice/'. $official_notice->image_name))  && $official_notice->image_name) {
                unlink(public_path().'/uploads/official_notice/'. $official_notice->image_name);
                if (file_exists(public_path().'/uploads/official_notice/thumb/'. $official_notice->image_name)) {
                    unlink(public_path().'/uploads/official_notice/thumb/'. $official_notice->image_name);
                } 
            }
            if (!$data['image_name']) {
                $data['image_name'] = '';
                $datafor_on['image_name'] = '';
            }
            unset($datafor_on['delete_old_image']);
        } else {
            if (!$data['image_name']) {
                $data['image_name'] = $official_notice->image_name;
                $datafor_on['image_name'] = $official_notice->image_name;
            }
        }
        $datafor_on['modified_date'] = new Carbon();
        $datafor_on = check_case($datafor_on, 'edit');        
        $success = DB::table('tbl_official_notices')->where('blog_id', $id)->update($datafor_on);

        
        //$success = Official_notice::where('id', $id)->update($data);
        return redirect()->route('admin.official_notices')->with('success', "Data updated successfully.");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_official_notices', 'blog_id', $id, 'deleted_by', 'deleted_date');
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "OFFICIAL_NOTICES", 'file_id' => $id]); //create activity log

        return redirect()->route('admin.official_notices')->with('success', "Data deleted successfully.");
        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_official_notices', 'blog_id', 'deleted_by', 'deleted_date');
        return redirect()->route('admin.official_notices')->with('success', "Data updated successfully.");
    }


}
