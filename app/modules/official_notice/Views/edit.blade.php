@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Official Notices   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.official_notices') }}">Official Notices</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.official_notices.update', $official_notice->blog_id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                        <input type="hidden" value = "{{$official_notice->blog_id}}"  name="blog_id" id="blog_id" class="form-control" >
                    <div class="form-group">
                        <label for="blog_title">Blog Title</label><input type="text" value = "{{$official_notice->blog_title}}"  name="blog_title" id="blog_title" class="form-control" required >
                    </div>

                    <div class="form-group">
                        <label for="image_name">Image </label><br>
                        <label id="upload_image_name" style="display:none"></label>
                        <div id="delete_old_image_requested"></div>
                        <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                        <div id="image-status"></div>
                        <input type="file" id="upload_image" name="upload_image" @if (file_exists(public_path().'/uploads/official_notice/thumb/'. $official_notice->image_name) && $official_notice->image_name) style="display:none" @else style="display:block" @endif />
                        <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                        <div id="imagedata">
                        @if (file_exists(public_path().'/uploads/official_notice/'. $official_notice->image_name) && $official_notice->image_name)
                    
                            <img src="{{url('uploads/official_notice/thumb/'.$official_notice->image_name)}}">
                            <a href="javascript:void(0)" id="change-image_option" title="Change Image">&nbsp; &nbsp; <img src="{{('/images/logo/cancel.png')}}" border="0"/> Change the old image</a>
                        @endif                                
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="contents">Contents</label><textarea name="contents" id="contents" class="form-control  my-editor" >{!! $official_notice->contents !!}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="post_date">Post Date</label><input type="date" value = "{{$official_notice->post_date}}"  name="post_date" id="post_date" class="form-control" required >
                    </div>


                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="status" id="status1" @if($official_notice->status == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="status" id="status0" @if($official_notice->status == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="sequence">Sequence</label><input type="text" value = "{{$official_notice->sequence}}"  name="sequence" id="sequence" class="form-control" >
                    </div>
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" id="btn-submit">Save</button>
                    <a href="{{ route('admin.official_notices') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
    <script>
         
      <?php //tinymce('contents');?>

      $('#btn-submit').on('click',function(){
        
       
        var name        = $('#blog_title').val();
        var image     = $('#image_name').val();
      
        var post_date     = $('#post_date').val();
         var content     = $('#contents').val();
        
       
        if(name =='')
        {   
            
           alert('Please Enter Blog title');
            return false;
        }
        /*if(image =='')
         {
             alert('Please upload an image for Blog');
            return false;
         }*/ 
        
         if(post_date =='')
         {
             alert('Please Enter date to posted');
            return false;
         }
         if(content =='')
         {
             alert('Please write content for blog');
            return false;
         }

        
       
    });

    </script>
         <script type="text/javascript">
        var directory = 'official_notice';
    </script>
    @include('data_changing_script_edit')
    @include('file_changing_script')
@endsection
