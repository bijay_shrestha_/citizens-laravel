<?php



Route::group(array('prefix'=>'admin/','module'=>'cr_network','middleware' => ['web','auth'], 'namespace' => 'App\modules\cr_network\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('cr_networks/','AdminCr_networkController@index')->name('admin.cr_networks');
    Route::post('cr_networks/getcr_networksJson','AdminCr_networkController@getcr_networksJson')->name('admin.cr_networks.getdatajson');
    Route::get('cr_networks/create','AdminCr_networkController@create')->name('admin.cr_networks.create');
    Route::post('cr_networks/store','AdminCr_networkController@store')->name('admin.cr_networks.store');
    Route::get('cr_networks/show/{id}','AdminCr_networkController@show')->name('admin.cr_networks.show');
    Route::get('cr_networks/edit/{id}','AdminCr_networkController@edit')->name('admin.cr_networks.edit');
    Route::match(['put', 'patch'], 'cr_networks/update/{id}','AdminCr_networkController@update')->name('admin.cr_networks.update');
    Route::get('cr_networks/delete/{id}', 'AdminCr_networkController@destroy')->name('admin.cr_networks.edit');
    Route::get('cr_networks/change/{text}/{status}/{id}', 'AdminCr_networkController@change');
});




Route::group(array('module'=>'cr_network','namespace' => 'App\modules\cr_network\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('cr_networks/','Cr_networkController@index')->name('cr_networks');
    
});