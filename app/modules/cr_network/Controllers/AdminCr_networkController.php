<?php

namespace App\modules\cr_network\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\cr_network\Model\Cr_network;
use Carbon\Carbon;

class AdminCr_networkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Corresponding Network';
        return view("cr_network::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getcr_networksJson(Request $request)
    {
        $cr_network = new Cr_network;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $cr_network->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('network_id', "DSC")->get();

        // Display limited list        
        $rows = $cr_network->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->orderBy('network_id', "DSC")->get();

        //To count the total values present
        $total = $cr_network->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Corresponding Network | Create';
        $countries = DB::table('tbl_countries')->get();
        return view("cr_network::add",compact('page', 'countries'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $datafor_CRN = $data;
        $datafor_CRN['del_flag'] = 0;
        $datafor_CRN['added_by'] = Auth::user()->id;
        $datafor_CRN['added_date'] = new Carbon();
        $datafor_CRN = check_case($datafor_CRN, 'create');
        $success = Cr_network::Insert($datafor_CRN);
        $cr_networks = Cr_network::all();
        $cr_network_id = $cr_networks[count($cr_networks)-1]['network_id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "cr_network", 'file_id' => $cr_network_id]); //create acrivity log     
        
        
        return redirect()->route('admin.cr_networks')->with('success', 'Data Updated Succesfully.');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cr_network = Cr_network::where('network_id',$id)->firstOrFail();
        $page['title'] = 'Corresponding Network | Update';
        $countries = DB::table('tbl_countries')->get();
        return view("cr_network::edit",compact('page','cr_network', 'countries'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $cr_network = Cr_network::where('network_id',$id)->firstOrFail();
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "cr_network", 'file_id' => $id]); //create activity log
        $datafor_CRN = $data;
        $datafor_CRN['modified_date'] = new Carbon();
        $datafor_CRN = check_case($datafor_CRN, 'edit');
        DB::table('tbl_cr_networks')->where('network_id', $id)->update($datafor_CRN);
        return redirect()->route('admin.cr_networks')->with('success', "Data updated successfully.");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_cr_networks', 'network_id', $id, 'deleted_by', 'deleted_date');
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "cr_network", 'file_id' => $id]); //create acrivity log             
        return redirect()->route('admin.cr_networks')->with('success', "Data deleted successfully.");

    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_cr_networks', 'network_id', 'deleted_by', 'deleted_date');
        return redirect()->route('admin.cr_networks')->with('success', "Data updated successfully.");
    }

}
