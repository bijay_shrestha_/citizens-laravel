@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Corresponding Network
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.cr_networks') }}">Corresponding Network</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.cr_networks.update',$cr_network->network_id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                                    <input type="hidden" value = "{{$cr_network->network_id}}"  name="network_id" id="network_id" class="form-control" >

                                    <div class="form-group">
                                        <label for="country_id">Countries </label>
                                        <select name="country_id" class="form-control" required>
                                            <option value="0">Select Country </option> 
                                            @foreach ($countries as $country)
                                                <option value="{{$country->id}}" 
                                                 @if ($cr_network->country_id == $country->id) selected @endif
                                                > {{$country->name}} </option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="bank_name">Bank Name (*)</label><input type="text" value = "{{$cr_network->bank_name}}"  name="bank_name" id="bank_name" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="bank_address">Bank Address (*)</label><input type="text" value = "{{$cr_network->bank_address}}"  name="bank_address" id="bank_address" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="bank_currency">Bank Currency (*)</label><input type="text" value = "{{$cr_network->bank_currency}}" required name="bank_currency" id="bank_currency" class="form-control" >
                                    </div>

                                    <div class="form-group">
                                        <label for="nostro_account_no">Nostro Account No</label><input type="text" value = "{{$cr_network->nostro_account_no}}"  name="nostro_account_no" id="nostro_account_no" class="form-control" >
                                    </div>

                                    <div class="form-group">
                                        <label for="swift_code">Swift Code</label><input type="text" value = "{{$cr_network->swift_code}}"  name="swift_code" id="swift_code" class="form-control" >
                                    </div>

                                    <div class="form-group">
                                        <label for="rtgs">Rtgs</label><input type="text" value = "{{$cr_network->rtgs}}"  name="rtgs" id="rtgs" class="form-control" >
                                    </div>

                                    <div class="form-group">
                                        <label for="status">Status</label>
                                        <div class="radio">
                                        <label>
                                            <input type="radio" value="1" name="status" id="status1" @if($cr_network->status == 1) checked @endif  />Yes
                                        </label> 
                                        <label>
                                            <input type="radio" value="0" name="status" id="status0" @if($cr_network->status == 1) @else  checked @endif  />No
                                        </label>
                                        </div>
                                    </div>

                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.cr_networks') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
