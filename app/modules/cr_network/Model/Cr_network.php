<?php

namespace App\Modules\Cr_network\Model;


use Illuminate\Database\Eloquent\Model;

class Cr_network extends Model
{
    public  $table = 'tbl_cr_networks';

    protected $fillable = ['network_id','country_id','bank_name','bank_address','bank_currency','nostro_account_no','swift_code','rtgs','approvel_status','approved','added_by','added_date','modified_date','deleted_by','deleted_date','del_flag','status',];
}
