@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Stocks   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.stocks') }}">tbl_stock</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.stocks.update') }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                                    <label for="stock_id">Stock_id</label><input type="text" value = "{{$stock->stock_id}}"  name="stock_id" id="stock_id" class="form-control" ></div><div class="form-group">
                                    <label for="symbol">Symbol</label><input type="text" value = "{{$stock->symbol}}"  name="symbol" id="symbol" class="form-control" ></div><div class="form-group">
                                    <label for="trunover">Trunover</label><input type="text" value = "{{$stock->trunover}}"  name="trunover" id="trunover" class="form-control" ></div><div class="form-group">
                                    <label for="closing_price">Closing_price</label><input type="text" value = "{{$stock->closing_price}}"  name="closing_price" id="closing_price" class="form-control" ></div><div class="form-group">
                                    <label for="created_time">Created_time</label><input type="text" value = "{{$stock->created_time}}"  name="created_time" id="created_time" class="form-control" ></div><div class="form-group">
                                    <label for="status">Status</label><input type="text" value = "{{$stock->status}}"  name="status" id="status" class="form-control" ></div>
<input type="hidden" name="id" id="id" value = "{{$stock->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.stocks') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
