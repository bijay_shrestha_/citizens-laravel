@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Stocks   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.stocks') }}">tbl_stock</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.stocks.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                                    <label for="stock_id">Stock_id</label><input type="text" name="stock_id" id="stock_id" class="form-control" ></div><div class="form-group">
                                    <label for="symbol">Symbol</label><input type="text" name="symbol" id="symbol" class="form-control" ></div><div class="form-group">
                                    <label for="trunover">Trunover</label><input type="text" name="trunover" id="trunover" class="form-control" ></div><div class="form-group">
                                    <label for="closing_price">Closing_price</label><input type="text" name="closing_price" id="closing_price" class="form-control" ></div><div class="form-group">
                                    <label for="created_time">Created_time</label><input type="text" name="created_time" id="created_time" class="form-control" ></div><div class="form-group">
                                    <label for="status">Status</label><input type="text" name="status" id="status" class="form-control" ></div>
<input type="hidden" name="id" id="id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.stocks') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
