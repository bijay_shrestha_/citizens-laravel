<?php

namespace App\Modules\Stock\Model;


use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    public  $table = 'tbl_stock';

    protected $fillable = ['stock_id','symbol','trunover','closing_price','created_time','status',];
}
