<?php



Route::group(array('prefix'=>'admin/','module'=>'stock','middleware' => ['web','auth'], 'namespace' => 'App\modules\stock\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('stocks/','AdminStockController@index')->name('admin.stocks');
    Route::post('stocks/getstocksJson','AdminStockController@getstocksJson')->name('admin.stocks.getdatajson');
    Route::get('stocks/create','AdminStockController@create')->name('admin.stocks.create');
    Route::post('stocks/store','AdminStockController@store')->name('admin.stocks.store');
    Route::get('stocks/show/{id}','AdminStockController@show')->name('admin.stocks.show');
    Route::get('stocks/edit/{id}','AdminStockController@edit')->name('admin.stocks.edit');
    Route::match(['put', 'patch'], 'stocks/update/{id}','AdminStockController@update')->name('admin.stocks.update');
    Route::get('stocks/delete/{id}', 'AdminStockController@destroy')->name('admin.stocks.edit');
});




Route::group(array('module'=>'stock','namespace' => 'App\modules\stock\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('stocks/','StockController@index')->name('stocks');
    
});