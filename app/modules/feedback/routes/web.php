<?php



Route::group(array('prefix'=>'admin/','module'=>'feedback','middleware' => ['web','auth'], 'namespace' => 'App\modules\feedback\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('feedback/','AdminFeedbackController@index')->name('admin.feedback');
    Route::post('feedback/getfeedbackJson','AdminFeedbackController@getfeedbackJson')->name('admin.feedback.getdatajson');
    Route::get('feedback/create','AdminFeedbackController@create')->name('admin.feedback.create');
    Route::post('feedback/store','AdminFeedbackController@store')->name('admin.feedback.store');
    Route::get('feedback/show/{id}','AdminFeedbackController@show')->name('admin.feedback.show');
    Route::get('feedback/edit/{id}','AdminFeedbackController@edit')->name('admin.feedback.edit');
    Route::match(['put', 'patch'], 'feedback/update/{id}','AdminFeedbackController@update')->name('admin.feedback.update');
    Route::get('feedback/delete/{id}', 'AdminFeedbackController@destroy')->name('admin.feedback.edit');
});




Route::group(array('module'=>'feedback','namespace' => 'App\Modules\Feedback\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('feedback/','FeedbackController@index')->name('feedback');
    
});