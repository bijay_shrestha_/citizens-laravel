<?php

namespace App\Modules\Feedback\Model;


use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    public  $table = 'tbl_feedbacks';

    protected $fillable = ['id','name','email','telephone','account_num','comment','send_date','deleted_date','del_flag','deleted_by',];
}
