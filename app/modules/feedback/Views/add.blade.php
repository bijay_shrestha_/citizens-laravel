@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Feedback   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.feedback') }}">tbl_feedbacks</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.feedback.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                                    <label for="name">Name</label><input type="text" name="name" id="name" class="form-control" ></div><div class="form-group">
                                    <label for="email">Email</label><input type="text" name="email" id="email" class="form-control" ></div><div class="form-group">
                                    <label for="telephone">Telephone</label><input type="text" name="telephone" id="telephone" class="form-control" ></div><div class="form-group">
                                    <label for="account_num">Account_num</label><input type="text" name="account_num" id="account_num" class="form-control" ></div><div class="form-group">
                                    <label for="comment">Comment</label><input type="text" name="comment" id="comment" class="form-control" ></div><div class="form-group">
                                    <label for="send_date">Send_date</label><input type="text" name="send_date" id="send_date" class="form-control" ></div><div class="form-group">
                                    <label for="deleted_date">Deleted_date</label><input type="text" name="deleted_date" id="deleted_date" class="form-control" ></div><div class="form-group">
                                    <label for="del_flag">Del_flag</label><input type="text" name="del_flag" id="del_flag" class="form-control" ></div><div class="form-group">
                                    <label for="deleted_by">Deleted_by</label><input type="text" name="deleted_by" id="deleted_by" class="form-control" ></div>
<input type="hidden" name="id" id="id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.feedback') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
