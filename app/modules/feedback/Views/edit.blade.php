@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Feedback   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.feedback', $feedback->id) }}">tbl_feedbacks</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.feedback.update') }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                                    <label for="name">Name</label><input type="text" value = "{{$feedback->name}}"  name="name" id="name" class="form-control" ></div><div class="form-group">
                                    <label for="email">Email</label><input type="text" value = "{{$feedback->email}}"  name="email" id="email" class="form-control" ></div><div class="form-group">
                                    <label for="telephone">Telephone</label><input type="text" value = "{{$feedback->telephone}}"  name="telephone" id="telephone" class="form-control" ></div><div class="form-group">
                                    <label for="account_num">Account_num</label><input type="text" value = "{{$feedback->account_num}}"  name="account_num" id="account_num" class="form-control" ></div><div class="form-group">
                                    <label for="comment">Comment</label><input type="text" value = "{{$feedback->comment}}"  name="comment" id="comment" class="form-control" ></div><div class="form-group">
                                    <label for="send_date">Send_date</label><input type="text" value = "{{$feedback->send_date}}"  name="send_date" id="send_date" class="form-control" ></div><div class="form-group">
                                    <label for="deleted_date">Deleted_date</label><input type="text" value = "{{$feedback->deleted_date}}"  name="deleted_date" id="deleted_date" class="form-control" ></div><div class="form-group">
                                    <label for="del_flag">Del_flag</label><input type="text" value = "{{$feedback->del_flag}}"  name="del_flag" id="del_flag" class="form-control" ></div><div class="form-group">
                                    <label for="deleted_by">Deleted_by</label><input type="text" value = "{{$feedback->deleted_by}}"  name="deleted_by" id="deleted_by" class="form-control" ></div>
<input type="hidden" name="id" id="id" value = "{{$feedback->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.feedback') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
