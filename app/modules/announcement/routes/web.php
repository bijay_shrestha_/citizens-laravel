<?php



Route::group(array('prefix'=>'admin/','module'=>'announcement','middleware' => ['web','auth'], 'namespace' => 'App\modules\announcement\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('announcements/','AdminAnnouncementController@index')->name('admin.announcements');
    Route::post('announcements/getannouncementsJson','AdminAnnouncementController@getannouncementsJson')->name('admin.announcements.getdatajson');
    Route::get('announcements/create','AdminAnnouncementController@create')->name('admin.announcements.create');
    Route::post('announcements/store','AdminAnnouncementController@store')->name('admin.announcements.store');
    Route::get('announcements/show/{id}','AdminAnnouncementController@show')->name('admin.announcements.show');
    Route::get('announcements/edit/{id}','AdminAnnouncementController@edit')->name('admin.announcements.edit');
    Route::match(['put', 'patch'], 'announcements/update/{id}','AdminAnnouncementController@update')->name('admin.announcements.update');
    Route::get('announcements/delete/{id}', 'AdminAnnouncementController@destroy')->name('admin.announcements.edit');
    Route::get('announcements/change/{text}/{status}/{id}', 'AdminAnnouncementController@change');

});




Route::group(array('module'=>'announcement','namespace' => 'App\modules\announcement\Controllers', 'middleware' => 'web'), function() {
    //Your routes belong to this module.
    Route::get('announcements/','AnnouncementController@index')->name('announcements');
    
});