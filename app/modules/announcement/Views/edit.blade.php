@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Announcements   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.announcements') }}">Announcement</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.announcements.update', $announcement->a_id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                    <div class="form-group">
                        <label for="title">Title</label><input type="text" value = "{{$announcement->title}}"  name="title" id="title" class="form-control" required>
                    </div>


                    <div class="form-group">
                        <label for="cat_id">Category</label>
                        <select name="cat_id" id="cat_id" class="form-control">
                            @foreach($notice_categories as $category)
                                <option value="{{$category['id']}}" @if($category['id'] == $announcement->cat_id) selected @endif >{{$category['category_name']}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="content">Content</label><textarea name="content" id="content" class="form-control my-editor" >{!! $announcement->content !!}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="image_name">Image </label><br>
                        <label id="upload_image_name" style="display:none"></label>
                         <div id="delete_old_image_requested"></div>
                        <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                        <div id="image-status"></div>
                        <input type="file" id="upload_image" name="upload_image" @if (file_exists(public_path().'/uploads/announcement/thumb/'. $announcement->image_name) && $announcement->image_name) style="display:none" @else style="display:block" @endif />
                        <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                        <div id="imagedata">
                        @if (file_exists(public_path().'/uploads/announcement/'. $announcement->image_name) && $announcement->image_name)                                   
                            <img src="{{url('uploads/announcement/thumb/'.$announcement->image_name)}}">
                            <a href="javascript:void(0)" id="change-image_option" title="Change Image">&nbsp; &nbsp; <img src="{{('/images/logo/cancel.png')}}" border="0"/> Change the old image</a>
                        @endif                                
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="date">Post Date</label><input type="date" value = "{{$announcement->date}}"  name="date" id="date" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="ex_date">Expiry Date</label><input type="date" value = "{{$announcement->ex_date}}"  name="ex_date" id="ex_date" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="status" id="status1" @if($announcement->status == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="status" id="status0" @if($announcement->status == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.announcements') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
    <script type="text/javascript">
        var directory = 'announcement';
    </script>
@include('data_changing_script_edit')
@include('file_changing_script')

@endsection
