<?php

namespace App\modules\announcement\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;

class AnnouncementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $where = ['approvel_status', "delete"];
 //       $where = [['column_name1', 'value1'], ['column2', 'value2']];
   //     $where = [['approvel_status', 'delete']];
   //     dd(count($where));
        /*foreach ($where as $key => $value) {
            print_r($value);
            echo $key . '  <br>';
            if (is_array($value)) {
                foreach ($value as $field => $d) {
                    echo $field . ' ' . $d .  ' &nbsp; &nbsp; &nbsp; &nbsp;';
                }
            } else {
                echo $key . ' ' . $value .  ' &nbsp; &nbsp; &nbsp; &nbsp;';
            }
            echo '<br>';
        }
        
       // dd($where);
        */
    //    $where = [['fieldname1', "val1"], ['fieldname2', "val2"]];

        $announcements = get_lists('tbl_announcement');
        $banners =  DB::table('tbl_banners')->where('banner_collection_id', 3)->get();
        $banners = objToArray($banners);
        $notice_categories = get_lists('tbl_notice_categories');
        $page['title'] = 'announcements';
//        return view('front.announcement.index', compact('page', 'announcements', 'banners', 'notice_categories'));
        return view('front.announcement.index', compact('page', 'announcements', 'banners', 'notice_categories'));
    }

}
