<?php

namespace App\modules\announcement\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\announcement\Model\Announcement;
use App\modules\notice_category\Model\Notice_category;
use Carbon\Carbon;

class AdminAnnouncementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Announcement';
        return view("announcement::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getannouncementsJson(Request $request)
    {
        $announcement = new Announcement;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $announcement->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('a_id', "DSC")->get();

        // Display limited list        
        $rows = $announcement->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->orderBy('a_id', "DSC")->where('del_flag',0)->get();

        //To count the total values present
        $total = $announcement->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Announcement | Create';
        $notice_categories = Notice_category::where('del_flag', 0)->get()->toArray();
        return view("announcement::add",compact('page', 'notice_categories'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $dataforannounce = $data;
        unset($dataforannounce['upload_image']);      
        $dataforannounce['created_at'] = new Carbon();
        $dataforannounce['del_flag'] = 0;
        $dataforannounce['approvel_status'] = "ok";
        $dataforannounce['approved'] = "approved";
        $dataforannounce['banner_collection_id'] = 0;
        $success = Announcement::Insert($dataforannounce);
        $announcements = Announcement::all();
        $announcement_id = $announcements[count($announcements)-1]['a_id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "announcement", 'file_id' => $announcement_id]); //create activity log     
        return redirect()->route('admin.announcements')->with('success', "Data inserted successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $announcement = Announcement::where('a_id',$id)->firstOrFail();
        $page['title'] = 'Announcement | Update';
        $notice_categories = Notice_category::where('del_flag', 0)->get()->toArray();
        return view("announcement::edit",compact('page','announcement', 'notice_categories'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $data = $request->except('_token', '_method');
        $announcement = Announcement::where('a_id', $id)->firstOrFail();
        $dataforannounce = $data;
        unset($dataforannounce['upload_image']);
        
        if (array_key_exists('delete_old_image', $data)) {
            if ((file_exists(public_path().'/uploads/announcement/'. $announcement->image))  && $announcement->image) {
                unlink(public_path().'/uploads/announcement/'. $announcement->image);
                if (file_exists(public_path().'/uploads/announcement/thumb/'. $announcement->image)) {
                    unlink(public_path().'/uploads/announcement/thumb/'. $announcement->image);
                } 
            }
            if (!$data['image_name']) {
                $data['image_name'] = '';
                $dataforannounce['image_name'] = '';
            }
            unset($dataforannounce['delete_old_image']);
        } else {
            if (!$data['image_name']) {
                $data['image_name'] = $announcement->image;
                $dataforannounce['image_name'] = $announcement->image;
            }
        }
        $success = DB::table('tbl_announcement')->where('a_id', $id)->update($dataforannounce);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "announcement", 'file_id' => $id]); //create activity log
        return redirect()->route('admin.announcements')->with('success', "Data updated Successfully.");
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_announcement', 'a_id', $id, 'deleted_by', 'deleted_date');
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "announcement", 'file_id' => $id]); //create activity log
        return redirect()->route('admin.announcements')->with('success', "Data deleted Successfully.");

        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_announcement', 'a_id', 'deleted_by', 'deleted_date');
        return redirect()->route('admin.announcements')->with('success', "Data updated Successfully.");
    }

}
