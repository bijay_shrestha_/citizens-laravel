<?php

namespace App\Modules\Announcement\Model;


use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    public  $table = 'tbl_announcement';

    protected $fillable = ['a_id','title','content','image_name','banner_collection_id','cat_id','date','ex_date','approvel_status','approved','del_flag','status','created_at','deleted_date','deleted_by',];
}
