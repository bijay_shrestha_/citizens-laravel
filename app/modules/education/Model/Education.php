<?php

namespace App\Modules\Education\Model;


use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    public  $table = 'tbl_education';

    protected $fillable = ['e_id','p_id','s_board','s_school','s_degree','s_marks','s_division','s_passed_year','c_university','c_college','c_degree','c_marks','c_division','c_passed_year','b_university','b_college','b_faculty','b_marks','b_division','b_passed_year','m_university','m_college','m_faculty','m_marks','m_division','m_passed_year','course_name','institude_name',];
}
