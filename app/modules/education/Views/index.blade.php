@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Education		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Education</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.education.create') }}" class="btn bg-green waves-effect"  title="create">Create</a>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="education-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								<th >E_id</th>
<th >P_id</th>
<th >S_board</th>
<th >S_school</th>
<th >S_degree</th>
<th >S_marks</th>
<th >S_division</th>
<th >S_passed_year</th>
<th >C_university</th>
<th >C_college</th>
<th >C_degree</th>
<th >C_marks</th>
<th >C_division</th>
<th >C_passed_year</th>
<th >B_university</th>
<th >B_college</th>
<th >B_faculty</th>
<th >B_marks</th>
<th >B_division</th>
<th >B_passed_year</th>
<th >M_university</th>
<th >M_college</th>
<th >M_faculty</th>
<th >M_marks</th>
<th >M_division</th>
<th >M_passed_year</th>
<th >Course_name</th>
<th >Institude_name</th>

								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#education-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.education.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
					{ data: "e_id",name: "e_id"},
            { data: "p_id",name: "p_id"},
            { data: "s_board",name: "s_board"},
            { data: "s_school",name: "s_school"},
            { data: "s_degree",name: "s_degree"},
            { data: "s_marks",name: "s_marks"},
            { data: "s_division",name: "s_division"},
            { data: "s_passed_year",name: "s_passed_year"},
            { data: "c_university",name: "c_university"},
            { data: "c_college",name: "c_college"},
            { data: "c_degree",name: "c_degree"},
            { data: "c_marks",name: "c_marks"},
            { data: "c_division",name: "c_division"},
            { data: "c_passed_year",name: "c_passed_year"},
            { data: "b_university",name: "b_university"},
            { data: "b_college",name: "b_college"},
            { data: "b_faculty",name: "b_faculty"},
            { data: "b_marks",name: "b_marks"},
            { data: "b_division",name: "b_division"},
            { data: "b_passed_year",name: "b_passed_year"},
            { data: "m_university",name: "m_university"},
            { data: "m_college",name: "m_college"},
            { data: "m_faculty",name: "m_faculty"},
            { data: "m_marks",name: "m_marks"},
            { data: "m_division",name: "m_division"},
            { data: "m_passed_year",name: "m_passed_year"},
            { data: "course_name",name: "course_name"},
            { data: "institude_name",name: "institude_name"},
            
					{ data: function(data,b,c,table) { 
					var buttons = '';

					buttons += "<a class='btn bg-red waves-effect' href='"+site_url+"/edit/"+data.id+"' type='button' >Edit</a>&nbsp"; 

					buttons += "<a href='"+site_url+"/delete/"+data.id+"' class='btn bg-red waves-effect' >Delete</a>";

					return buttons;
					}, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
