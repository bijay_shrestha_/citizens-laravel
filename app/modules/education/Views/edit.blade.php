@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Education   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.education') }}">tbl_education</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.education.update') }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                                    <label for="e_id">E_id</label><input type="text" value = "{{$education->e_id}}"  name="e_id" id="e_id" class="form-control" ></div><div class="form-group">
                                    <label for="p_id">P_id</label><input type="text" value = "{{$education->p_id}}"  name="p_id" id="p_id" class="form-control" ></div><div class="form-group">
                                    <label for="s_board">S_board</label><input type="text" value = "{{$education->s_board}}"  name="s_board" id="s_board" class="form-control" ></div><div class="form-group">
                                    <label for="s_school">S_school</label><input type="text" value = "{{$education->s_school}}"  name="s_school" id="s_school" class="form-control" ></div><div class="form-group">
                                    <label for="s_degree">S_degree</label><input type="text" value = "{{$education->s_degree}}"  name="s_degree" id="s_degree" class="form-control" ></div><div class="form-group">
                                    <label for="s_marks">S_marks</label><input type="text" value = "{{$education->s_marks}}"  name="s_marks" id="s_marks" class="form-control" ></div><div class="form-group">
                                    <label for="s_division">S_division</label><input type="text" value = "{{$education->s_division}}"  name="s_division" id="s_division" class="form-control" ></div><div class="form-group">
                                    <label for="s_passed_year">S_passed_year</label><input type="text" value = "{{$education->s_passed_year}}"  name="s_passed_year" id="s_passed_year" class="form-control" ></div><div class="form-group">
                                    <label for="c_university">C_university</label><input type="text" value = "{{$education->c_university}}"  name="c_university" id="c_university" class="form-control" ></div><div class="form-group">
                                    <label for="c_college">C_college</label><input type="text" value = "{{$education->c_college}}"  name="c_college" id="c_college" class="form-control" ></div><div class="form-group">
                                    <label for="c_degree">C_degree</label><input type="text" value = "{{$education->c_degree}}"  name="c_degree" id="c_degree" class="form-control" ></div><div class="form-group">
                                    <label for="c_marks">C_marks</label><input type="text" value = "{{$education->c_marks}}"  name="c_marks" id="c_marks" class="form-control" ></div><div class="form-group">
                                    <label for="c_division">C_division</label><input type="text" value = "{{$education->c_division}}"  name="c_division" id="c_division" class="form-control" ></div><div class="form-group">
                                    <label for="c_passed_year">C_passed_year</label><input type="text" value = "{{$education->c_passed_year}}"  name="c_passed_year" id="c_passed_year" class="form-control" ></div><div class="form-group">
                                    <label for="b_university">B_university</label><input type="text" value = "{{$education->b_university}}"  name="b_university" id="b_university" class="form-control" ></div><div class="form-group">
                                    <label for="b_college">B_college</label><input type="text" value = "{{$education->b_college}}"  name="b_college" id="b_college" class="form-control" ></div><div class="form-group">
                                    <label for="b_faculty">B_faculty</label><input type="text" value = "{{$education->b_faculty}}"  name="b_faculty" id="b_faculty" class="form-control" ></div><div class="form-group">
                                    <label for="b_marks">B_marks</label><input type="text" value = "{{$education->b_marks}}"  name="b_marks" id="b_marks" class="form-control" ></div><div class="form-group">
                                    <label for="b_division">B_division</label><input type="text" value = "{{$education->b_division}}"  name="b_division" id="b_division" class="form-control" ></div><div class="form-group">
                                    <label for="b_passed_year">B_passed_year</label><input type="text" value = "{{$education->b_passed_year}}"  name="b_passed_year" id="b_passed_year" class="form-control" ></div><div class="form-group">
                                    <label for="m_university">M_university</label><input type="text" value = "{{$education->m_university}}"  name="m_university" id="m_university" class="form-control" ></div><div class="form-group">
                                    <label for="m_college">M_college</label><input type="text" value = "{{$education->m_college}}"  name="m_college" id="m_college" class="form-control" ></div><div class="form-group">
                                    <label for="m_faculty">M_faculty</label><input type="text" value = "{{$education->m_faculty}}"  name="m_faculty" id="m_faculty" class="form-control" ></div><div class="form-group">
                                    <label for="m_marks">M_marks</label><input type="text" value = "{{$education->m_marks}}"  name="m_marks" id="m_marks" class="form-control" ></div><div class="form-group">
                                    <label for="m_division">M_division</label><input type="text" value = "{{$education->m_division}}"  name="m_division" id="m_division" class="form-control" ></div><div class="form-group">
                                    <label for="m_passed_year">M_passed_year</label><input type="text" value = "{{$education->m_passed_year}}"  name="m_passed_year" id="m_passed_year" class="form-control" ></div><div class="form-group">
                                    <label for="course_name">Course_name</label><input type="text" value = "{{$education->course_name}}"  name="course_name" id="course_name" class="form-control" ></div><div class="form-group">
                                    <label for="institude_name">Institude_name</label><input type="text" value = "{{$education->institude_name}}"  name="institude_name" id="institude_name" class="form-control" ></div>
<input type="hidden" name="id" id="id" value = "{{$education->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.education') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
