<?php



Route::group(array('prefix'=>'admin/','module'=>'Education','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Education\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('education/','AdminEducationController@index')->name('admin.education');
    Route::post('education/geteducationJson','AdminEducationController@geteducationJson')->name('admin.education.getdatajson');
    Route::get('education/create','AdminEducationController@create')->name('admin.education.create');
    Route::post('education/store','AdminEducationController@store')->name('admin.education.store');
    Route::get('education/show/{id}','AdminEducationController@show')->name('admin.education.show');
    Route::get('education/edit/{id}','AdminEducationController@edit')->name('admin.education.edit');
    Route::match(['put', 'patch'], 'education/update/{id}','AdminEducationController@update')->name('admin.education.update');
    Route::get('education/delete/{id}', 'AdminEducationController@destroy')->name('admin.education.edit');
});




Route::group(array('module'=>'Education','namespace' => 'App\Modules\Education\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('education/','EducationController@index')->name('education');
    
});