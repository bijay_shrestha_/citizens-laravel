<?php

namespace App\modules\remit_catagory\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\remit_catagory\Model\Remit_catagory;

class AdminRemit_catagoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Remit Category';
        return view("remit_catagory::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getremit_catagoriesJson(Request $request)
    {
        $remit_catagory = new Remit_catagory;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $remit_catagory->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('id', "DSC")->get();

        // Display limited list        
        $rows = $remit_catagory->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->orderBy('id', "DSC")->get();

        //To count the total values present
        $total = $remit_catagory->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Remit Category | Create';
        return view("remit_catagory::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $data_for_remit_cat = $data;
        $data_for_remit_cat['del_flag'] = 0;
        $data_for_remit_cat['sequence'] = $data_for_remit_cat['sequence'] ? $data_for_remit_cat['sequence']: 0;
        $success = Remit_catagory::Insert($data_for_remit_cat);
        return redirect()->route('admin.remit_catagories')->with('success', "Data added successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $remit_catagory = Remit_catagory::findOrFail($id);
        $page['title'] = 'Remit Category | Update';
        return view("remit_catagory::edit",compact('page','remit_catagory'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $data_for_remit_cat = $data;
        $data_for_remit_cat['sequence'] = $data_for_remit_cat['sequence'] ? $data_for_remit_cat['sequence']: 0;
        $success = DB::table('tbl_remit_catagories')->where('id', $id)->update($data_for_remit_cat);
        return redirect()->route('admin.remit_catagories')->with('success', "Data updated successfully.");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_remit_catagories', 'id', $id);
        return redirect()->route('admin.remit_catagories')->with('success', "Data deleted for trash.");

        //
    }
}
