<?php



Route::group(array('prefix'=>'admin/','module'=>'remit_catagory','middleware' => ['web','auth'], 'namespace' => 'App\modules\remit_catagory\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('remit_catagories/','AdminRemit_catagoryController@index')->name('admin.remit_catagories');
    Route::post('remit_catagories/getremit_catagoriesJson','AdminRemit_catagoryController@getremit_catagoriesJson')->name('admin.remit_catagories.getdatajson');
    Route::get('remit_catagories/create','AdminRemit_catagoryController@create')->name('admin.remit_catagories.create');
    Route::post('remit_catagories/store','AdminRemit_catagoryController@store')->name('admin.remit_catagories.store');
    Route::get('remit_catagories/show/{id}','AdminRemit_catagoryController@show')->name('admin.remit_catagories.show');
    Route::get('remit_catagories/edit/{id}','AdminRemit_catagoryController@edit')->name('admin.remit_catagories.edit');
    Route::match(['put', 'patch'], 'remit_catagories/update/{id}','AdminRemit_catagoryController@update')->name('admin.remit_catagories.update');
    Route::get('remit_catagories/delete/{id}', 'AdminRemit_catagoryController@destroy')->name('admin.remit_catagories.edit');
});




Route::group(array('module'=>'remit_catagory','namespace' => 'App\modules\remit_catagory\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('remit_catagories/','Remit_catagoryController@index')->name('remit_catagories');
    
});