@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Remit Categories   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.remit_catagories') }}">Remit Categories</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.remit_catagories.update', $remit_catagory->id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                    <label for="title">Title</label><input type="text" value = "{{$remit_catagory->title}}"  name="title" id="title" class="form-control" required >
                </div>
                
                <div class="form-group">
                    <label for="sequence">Sequence</label><input type="number" value = "{{$remit_catagory->sequence}}"  name="sequence" id="sequence" class="form-control" >
                </div>
          
                <div class="form-group">
                    <label for="status">Status</label>
                    <div class="radio">
                    <label>
                        <input type="radio" value="1" name="status" id="status1" @if($remit_catagory->status == 1) checked @endif  />Yes
                    </label> 
                    <label>
                        <input type="radio" value="0" name="status" id="status0" @if($remit_catagory->status == 1) @else  checked @endif  />No
                    </label>
                    </div>
                </div>

                <input type="hidden" name="id" id="id" value = "{{$remit_catagory->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.remit_catagories') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
