<?php

namespace App\Modules\Remit_catagory\Model;


use Illuminate\Database\Eloquent\Model;

class Remit_catagory extends Model
{
    public  $table = 'tbl_remit_catagories';

    protected $fillable = ['id','title','sequence','status','del_flag',];
}
