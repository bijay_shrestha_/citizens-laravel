<?php



Route::group(array('prefix'=>'admin/','module'=>'Preference_available','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Preference_available\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('preference_availables/','AdminPreference_availableController@index')->name('admin.preference_availables');
    Route::post('preference_availables/getpreference_availablesJson','AdminPreference_availableController@getpreference_availablesJson')->name('admin.preference_availables.getdatajson');
    Route::get('preference_availables/create','AdminPreference_availableController@create')->name('admin.preference_availables.create');
    Route::post('preference_availables/store','AdminPreference_availableController@store')->name('admin.preference_availables.store');
    Route::get('preference_availables/show/{id}','AdminPreference_availableController@show')->name('admin.preference_availables.show');
    Route::get('preference_availables/edit/{id}','AdminPreference_availableController@edit')->name('admin.preference_availables.edit');
    Route::match(['put', 'patch'], 'preference_availables/update/{id}','AdminPreference_availableController@update')->name('admin.preference_availables.update');
    Route::get('preference_availables/delete/{id}', 'AdminPreference_availableController@destroy')->name('admin.preference_availables.edit');
});




Route::group(array('module'=>'Preference_available','namespace' => 'App\Modules\Preference_available\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('preference_availables/','Preference_availableController@index')->name('preference_availables');
    
});