@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Preference_availables   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.preference_availables') }}">tbl_preference_available</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.preference_availables.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                                    <label for="a_id">A_id</label><input type="text" name="a_id" id="a_id" class="form-control" ></div><div class="form-group">
                                    <label for="p_id">P_id</label><input type="text" name="p_id" id="p_id" class="form-control" ></div><div class="form-group">
                                    <label for="preferred_post">Preferred_post</label><input type="text" name="preferred_post" id="preferred_post" class="form-control" ></div><div class="form-group">
                                    <label for="specialised_area">Specialised_area</label><input type="text" name="specialised_area" id="specialised_area" class="form-control" ></div><div class="form-group">
                                    <label for="preferred_location">Preferred_location</label><input type="text" name="preferred_location" id="preferred_location" class="form-control" ></div><div class="form-group">
                                    <label for="expected_salary">Expected_salary</label><input type="text" name="expected_salary" id="expected_salary" class="form-control" ></div><div class="form-group">
                                    <label for="driving_license">Driving_license</label><input type="text" name="driving_license" id="driving_license" class="form-control" ></div><div class="form-group">
                                    <label for="travelling_option">Travelling_option</label><input type="text" name="travelling_option" id="travelling_option" class="form-control" ></div><div class="form-group">
                                    <label for="vacancy_code">Vacancy_code</label><input type="text" name="vacancy_code" id="vacancy_code" class="form-control" ></div>
<input type="hidden" name="id" id="id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.preference_availables') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
