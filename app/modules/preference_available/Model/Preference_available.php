<?php

namespace App\Modules\Preference_available\Model;


use Illuminate\Database\Eloquent\Model;

class Preference_available extends Model
{
    public  $table = 'tbl_preference_available';

    protected $fillable = ['a_id','p_id','preferred_post','specialised_area','preferred_location','expected_salary','driving_license','travelling_option','vacancy_code',];
}
