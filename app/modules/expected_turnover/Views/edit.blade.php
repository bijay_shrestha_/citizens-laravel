@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Expected Turnovers   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.expected_turnovers') }}">Expected Turnover</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.expected_turnovers.update',$expected_turnover->id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                    <div class="form-group">
                        <label for="expected_turnover">Expected Turnover</label><input type="text" value = "{{$expected_turnover->expected_turnover}}"  name="expected_turnover" id="expected_turnover" class="form-control" required >
                    </div>
                    
                    <div class="form-group">
                        <label for="show_or_hide">Show Or Hide</label>
                        <div class="radio">
                        <label>
                            <input type="radio" value="1" name="show_or_hide" id="show_or_hide1" @if($expected_turnover->show_or_hide == 1) checked @endif  />Yes
                        </label> 
                        <label>
                            <input type="radio" value="0" name="show_or_hide" id="show_or_hide0" @if($expected_turnover->show_or_hide == 1) @else  checked @endif  />No
                        </label>
                        </div>
                    </div>

<input type="hidden" name="id" id="id" value = "{{$expected_turnover->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.expected_turnovers') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
