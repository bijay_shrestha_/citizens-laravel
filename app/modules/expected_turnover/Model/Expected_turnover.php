<?php

namespace App\Modules\Expected_turnover\Model;


use Illuminate\Database\Eloquent\Model;

class Expected_turnover extends Model
{
    public  $table = 'tbl_expected_turnover';

    protected $fillable = ['id','expected_turnover','show_or_hide','del_flag',];
}
