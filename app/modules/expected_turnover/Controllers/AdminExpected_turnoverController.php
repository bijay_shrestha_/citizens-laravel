<?php

namespace App\modules\expected_turnover\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\expected_turnover\Model\Expected_turnover;

class AdminExpected_turnoverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Expected Turnover';
        return view("expected_turnover::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getexpected_turnoversJson(Request $request)
    {
        $expected_turnover = new Expected_turnover;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $expected_turnover->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->orderBy('id', "DSC")->where('del_flag',0)->get();

        // Display limited list        
        $rows = $expected_turnover->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->orderBy('id', "DSC")->where('del_flag',0)->get();

        //To count the total values present
        $total = $expected_turnover->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Expected Turnover | Create';
        return view("expected_turnover::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $data['del_flag'] = 0;
        $success = Expected_turnover::Insert($data);
        return redirect()->route('admin.expected_turnovers')->with('success', "Data inserted successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expected_turnover = Expected_turnover::findOrFail($id);
        $page['title'] = 'Expected Turnover | Update';
        return view("expected_turnover::edit",compact('page','expected_turnover'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $success = DB::table('tbl_expected_turnover')->where('id', $id)->update($data);
        return redirect()->route('admin.expected_turnovers')->with('success', "Data updated successfully.");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_expected_turnover', 'id', $id);
        return redirect()->route('admin.expected_turnovers')->with('success', "Data deleted successfully.");
        //
    }
}
