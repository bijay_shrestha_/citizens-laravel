<?php



Route::group(array('prefix'=>'admin/','module'=>'expected_turnover','middleware' => ['web','auth'], 'namespace' => 'App\modules\expected_turnover\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('expected_turnovers/','AdminExpected_turnoverController@index')->name('admin.expected_turnovers');
    Route::post('expected_turnovers/getexpected_turnoversJson','AdminExpected_turnoverController@getexpected_turnoversJson')->name('admin.expected_turnovers.getdatajson');
    Route::get('expected_turnovers/create','AdminExpected_turnoverController@create')->name('admin.expected_turnovers.create');
    Route::post('expected_turnovers/store','AdminExpected_turnoverController@store')->name('admin.expected_turnovers.store');
    Route::get('expected_turnovers/show/{id}','AdminExpected_turnoverController@show')->name('admin.expected_turnovers.show');
    Route::get('expected_turnovers/edit/{id}','AdminExpected_turnoverController@edit')->name('admin.expected_turnovers.edit');
    Route::match(['put', 'patch'], 'expected_turnovers/update/{id}','AdminExpected_turnoverController@update')->name('admin.expected_turnovers.update');
    Route::get('expected_turnovers/delete/{id}', 'AdminExpected_turnoverController@destroy')->name('admin.expected_turnovers.edit');
});




Route::group(array('module'=>'expected_turnover','namespace' => 'App\modules\expected_turnover\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('expected_turnovers/','Expected_turnoverController@index')->name('expected_turnovers');
    
});