<?php

namespace App\Modules\Activity_log\Model;


use Illuminate\Database\Eloquent\Model;

class Activity_log extends Model
{
    public  $table = 'activity_log';

    protected $fillable = ['id','user_id','action','date','file','file_id',];
}
