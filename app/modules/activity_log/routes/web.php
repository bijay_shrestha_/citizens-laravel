<?php



Route::group(array('prefix'=>'admin/','module'=>'activity_log','middleware' => ['web','auth'], 'namespace' => 'App\modules\activity_log\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('activity_logs/','AdminActivity_logController@index')->name('admin.activity_logs');
    Route::post('activity_logs/getactivity_logsJson','AdminActivity_logController@getactivity_logsJson')->name('admin.activity_logs.getdatajson');
    Route::get('activity_logs/create','AdminActivity_logController@create')->name('admin.activity_logs.create');
    Route::post('activity_logs/store','AdminActivity_logController@store')->name('admin.activity_logs.store');
    Route::get('activity_logs/show/{id}','AdminActivity_logController@show')->name('admin.activity_logs.show');
    Route::get('activity_logs/edit/{id}','AdminActivity_logController@edit')->name('admin.activity_logs.edit');
    Route::match(['put', 'patch'], 'activity_logs/update/{id}','AdminActivity_logController@update')->name('admin.activity_logs.update');
    Route::get('activity_logs/delete/{id}', 'AdminActivity_logController@destroy')->name('admin.activity_logs.edit');
});




Route::group(array('module'=>'activity_log','namespace' => 'App\modules\activity_log\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('activity_logs/','Activity_logController@index')->name('activity_logs');
    
});