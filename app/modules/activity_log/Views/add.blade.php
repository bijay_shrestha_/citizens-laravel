@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Activity_logs   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.activity_logs') }}">activity_log</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.activity_logs.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                                    <label for="user_id">User_id</label><input type="text" name="user_id" id="user_id" class="form-control" ></div><div class="form-group">
                                    <label for="action">Action</label><input type="text" name="action" id="action" class="form-control" ></div><div class="form-group">
                                    <label for="date">Date</label><input type="text" name="date" id="date" class="form-control" ></div><div class="form-group">
                                    <label for="file">File</label><input type="text" name="file" id="file" class="form-control" ></div><div class="form-group">
                                    <label for="file_id">File_id</label><input type="text" name="file_id" id="file_id" class="form-control" ></div>
<input type="hidden" name="id" id="id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.activity_logs') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
