@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Activity Logs		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Activity Logs</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						{{-- <a href="{{ route('admin.activity_logs.create') }}" class="btn bg-green waves-effect"  title="create">Create</a> --}}
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="activity_log-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								<th >User Id</th>
								<th >Action</th>
								<th >Date</th>
								<th >File</th>
								<th >File Id</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#activity_log-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.activity_logs.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
		    { data: "user_id",name: "user_id"},
            { data: "action",name: "action"},
            { data: "date",name: "date"},
            { data: "file",name: "file"},
            { data: "file_id",name: "file_id"},
            
/*					{ data: function(data,b,c,table) { 
					var buttons = '';

					buttons += "<a class='btn bg-red waves-effect' href='"+site_url+"/edit/"+data.id+"' type='button' >Edit</a>&nbsp"; 

					buttons += "<a href='"+site_url+"/delete/"+data.id+"' class='btn bg-red waves-effect' >Delete</a>";

					return buttons;
					}, name:'action',searchable: false},*/
				]
			});
		});

		
	</script>
@endsection
