@extends('admin.layout.main')
@section('content')
<link rel="stylesheet" href="{{ asset('nepalidatepicker/css/nepali.datepicker.css') }}">

    <section class="content-header">
        <h1>
            Add Interest Rate Spreads   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.interest_rate_spreads') }}">Interest Rate Spread</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.interest_rate_spreads.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                        <label for="name">Name</label><input type="text" name="name" id="name" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="interest_rate_spread_rate">Interest Rate Spread Rate</label><input type="text" name="interest_rate_spread_rate" id="interest_rate_spread_rate" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="date">Date</label><input type="text" name="date" id="date" class="form-control" required>
                    </div>

                    <div class="form-group">
                         <label for="status">Status</label>
                         <div class="radio">
                         <label>
                             <input type="radio" value="1" name="status" id="status1" @if(old('status') == 1) checked @endif  />Yes
                         </label> 
                         <label>
                             <input type="radio" value="0" name="status" id="status0" @if(old('status') == 1) @else  checked @endif  />No
                         </label>
                         </div>
                     </div>

                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.interest_rate_spreads') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection

@section('custom_scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $('#date').nepaliDatePicker();

    }); 
</script>
    <script type="text/javascript">
//    var directory = 'brate';
</script>
<script src="{{asset('nepalidatepicker/js/nepali.datepicker.js')}}"></script>
{{--
@include('data_changing_script_create')
@include('file_changing_script') --}}

@endsection
