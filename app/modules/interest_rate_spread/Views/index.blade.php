@extends('admin.layout.main')
@section('content')
<style type="text/css">
	.inputfile {
		width: 0.1px;
		height: 0.1px;
		opacity: 0;
		overflow: hidden;
		position: absolute;
		z-index: -1;
	}
	.inputfile + label {
		font-size: 12px;
		font-weight: 100;
		color: white;
		background-color: grey;
		padding: 0.625rem 1.25rem;
		/*display: inline-block;*/
	}

	.inputfile:focus + label,
	.inputfile + label:hover {
		background-color: cadetblue;
	}
	</style>
	<section class="content-header">
		<h1>
			Interest_rate_spreads		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Interest Rate Spreads</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{url('/uploads/excel_imports/interest_rate_spreads/interestspread.xlsx')}}" class="btn bg-green waves-effect"  title="Excel"> <i class="fa fa-cloud-upload"></i>  Sample Excel</a> &nbsp;
						<a href="{{ route('admin.interest_rate_spreads.create') }}" class="btn bg-green waves-effect"  title="create">Create</a>
						<div class="pad margin no-print">
						<div class="pull-right" style="text-align:left;">
						    <form  action="{{url('admin/interest_rate_spreads/uploadExcel')}}" method="post" enctype="multipart/form-data" >
													
													{{csrf_field()}}
														<input type="file" name="userfile" id="userfile" class="inputfile" data-multiple-caption="{count} files selected">										
														<label for="userfile"><span>Choose a file</span></label>
														<button class="btn bg-green waves-effect" id="btn_import_file" type="submit">Import</button>
														<br>
														<br>
														
													{!! ($errors->first('userfile','<font color="red"> :message</font>')) !!}
												</form>
						   
						   
						   
						</div>
						</div>
					</div>
					@if ($message = Session::get('success'))
					<div class="btn btn-success">
						{{$message}}
					</div>
					@endif

					<!-- /.box-header -->
					<div class="box-body">
						<table id="interest_rate_spread-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								<th >Interest Rate Spread Id</th>
								<th >Name</th>
								<th >Interest Rate Spread Rate</th>
								<th >Date</th>
								<th >Approvel Status</th>
								<th >Approved</th>
								<th >Status</th>
								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#interest_rate_spread-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.interest_rate_spreads.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
					{ data: "interest_rate_spread_id",name: "interest_rate_spread_id"},
            { data: "name",name: "name"},
            { data: "interest_rate_spread_rate",name: "interest_rate_spread_rate"},
            { data: "date",name: "date"},
            { data: "approvel_status",name: "approvel_status"},
            { data: function (data)  {
            	var content = "";

            	content += "<a href='{{url('/admin/interest_rate_spreads/change/')}}/approved/" + data.approvel_status + "/" + data.interest_rate_spread_id + "'style='color:";
            	if (data.approved == "approved") { content += "green"; }
            	content += "'>Approved</a><br>";
            	
            	content += "<a href='{{url('/admin/interest_rate_spreads/change/')}}/pending/" + data.approvel_status + "/" + data.interest_rate_spread_id + "'style='color:";
            	if (data.approved == "pending") { content += "green"; }
            	content += "'>Pending</a><br>";

            	content += "<a href='{{url('/admin/interest_rate_spreads/change/')}}/denied/" + data.approvel_status + "/" + data.interest_rate_spread_id + "'style='color:";
            	if (data.approved == "denied") { content += "green"; }
            	content += "'>Denied</a><br>";
            	
            	return content;
            },name:'approved', searchable: false},

            { data: "status",name: "status"},
            
            { data: function(data,b,c,table) { 
            var buttons = '';

            buttons += "<a class='btn bg-green waves-effect' href='"+site_url+"/edit/"+data.interest_rate_spread_id+"' type='button' ><i class='fa fa-edit'></i> Edit</a>&nbsp"; 

            buttons += "<a href='"+site_url+"/delete/"+data.interest_rate_spread_id+"' class='btn bg-red waves-effect' ><i class='fa fa-trash'></i>  Delete</a>";

            return buttons;
            }, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
