<?php



Route::group(array('prefix'=>'admin/','module'=>'interest_rate_spread','middleware' => ['web','auth'], 'namespace' => 'App\modules\interest_rate_spread\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('interest_rate_spreads/','AdminInterest_rate_spreadController@index')->name('admin.interest_rate_spreads');
    Route::post('interest_rate_spreads/getinterest_rate_spreadsJson','AdminInterest_rate_spreadController@getinterest_rate_spreadsJson')->name('admin.interest_rate_spreads.getdatajson');
    Route::get('interest_rate_spreads/create','AdminInterest_rate_spreadController@create')->name('admin.interest_rate_spreads.create');
    Route::post('interest_rate_spreads/store','AdminInterest_rate_spreadController@store')->name('admin.interest_rate_spreads.store');
    Route::get('interest_rate_spreads/show/{id}','AdminInterest_rate_spreadController@show')->name('admin.interest_rate_spreads.show');
    Route::get('interest_rate_spreads/edit/{id}','AdminInterest_rate_spreadController@edit')->name('admin.interest_rate_spreads.edit');
    Route::match(['put', 'patch'], 'interest_rate_spreads/update/{id}','AdminInterest_rate_spreadController@update')->name('admin.interest_rate_spreads.update');
    Route::get('interest_rate_spreads/delete/{id}', 'AdminInterest_rate_spreadController@destroy')->name('admin.interest_rate_spreads.edit');
    Route::get('interest_rate_spreads/change/{text}/{status}/{id}', 'AdminInterest_rate_spreadController@change');
    Route::post('interest_rate_spreads/uploadExcel', 'AdminInterest_rate_spreadController@uploadExcel');
});




Route::group(array('module'=>'interest_rate_spread','namespace' => 'App\modules\interest_rate_spread\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('interest_rate_spreads/','Interest_rate_spreadController@index')->name('interest_rate_spreads');
    
});