<?php

namespace App\modules\interest_rate_spread\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\interest_rate_spread\Model\Interest_rate_spread;
use Carbon\Carbon;
use App\Imports\UsersImport;
//use Maatwebsite\Excel\Facades\Excel;
use Excel;


class AdminInterest_rate_spreadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Interest Rate Spread';
        return view("interest_rate_spread::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getinterest_rate_spreadsJson(Request $request)
    {
        $interest_rate_spread = new Interest_rate_spread;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $interest_rate_spread->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->orderBy('interest_rate_spread_id', "DSC")->get();

        // Display limited list        
        $rows = $interest_rate_spread->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->orderBy('interest_rate_spread_id', "DSC")->where('del_flag',0)->get();

        //To count the total values present
        $total = $interest_rate_spread->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Interest Rate Spread | Create';
        return view("interest_rate_spread::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $datafor_ratespread = $data;

        /*if ($datafor_ratespread['date']) {
            $datafor_ratespread['date'] = date('Y-m-d H:i:s',strtotime($data['date']));
        }*/
        $datafor_ratespread['added_by'] = Auth::user()->id;
        $datafor_ratespread['added_date'] = new Carbon();
        $datafor_ratespread['del_flag'] = 0;
        $datafor_ratespread = check_case($datafor_ratespread, 'create');
        $success = Interest_rate_spread::Insert($datafor_ratespread);
        return redirect()->route('admin.interest_rate_spreads')->with('success', "Data inserted successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $interest_rate_spread = Interest_rate_spread::where('interest_rate_spread_id', $id)->firstOrFail();
        $page['title'] = 'Interest Rate Spread | Update';
        return view("interest_rate_spread::edit",compact('page','interest_rate_spread'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        $base_rate = Interest_rate_spread::where('interest_rate_spread_id',$id)->firstOrFail();
        $datafor_base = $data;        
        /*if ($datafor_base['date']) {
            $datafor_base['date'] = date('Y-m-d H:i:s',strtotime($data['date']));
        }*/
        $datafor_base['modified_date'] = new Carbon();
        $datafor_ratespread = check_case($datafor_ratespread, 'edit');

        DB::table('tbl_interest_rate_spread')->where('interest_rate_spread_id', $id)->update($datafor_base);
        return redirect()->route('admin.interest_rate_spreads')->with('success', "Data updated successfully.");        
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_interest_rate_spread', 'interest_rate_spread_id', $id, 'deleted_by', 'deleted_date');
        return redirect()->route('admin.interest_rate_spreads')->with('success', "Data deleted successfully.");
        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_interest_rate_spread', 'interest_rate_spread_id', 'deleted_by', 'deleted_date');
        return redirect()->route('admin.interest_rate_spreads')->with('success', "Data updated successfully.");
    }

    public function uploadExcel(Request $request)
    {
        $this->validate($request, ['userfile' => 'required|mimes:xls,xlsx,xlsm']);
        Excel::import(new UsersImport, request()->file('userfile'));
        return back()->with('success', "Data deleted successfully.");
    }

}
