<?php

namespace App\Modules\Interest_rate_spread\Model;


use Illuminate\Database\Eloquent\Model;

class Interest_rate_spread extends Model
{
    public  $table = 'tbl_interest_rate_spread';

    protected $fillable = ['interest_rate_spread_id','name','interest_rate_spread_rate','date','approvel_status','approved','added_by','added_date','modified_date','deleted_by','deleted_date','del_flag','status',];
}
