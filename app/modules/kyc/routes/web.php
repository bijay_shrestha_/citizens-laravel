<?php



Route::group(array('prefix'=>'admin/','module'=>'kyc','middleware' => ['web','auth'], 'namespace' => 'App\modules\kyc\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('kycs/','AdminKycController@index')->name('admin.kycs');
    Route::post('kycs/getkycsJson','AdminKycController@getkycsJson')->name('admin.kycs.getdatajson');
    Route::get('kycs/create','AdminKycController@create')->name('admin.kycs.create');
    Route::post('kycs/store','AdminKycController@store')->name('admin.kycs.store');
    Route::get('kycs/show/{id}','AdminKycController@show')->name('admin.kycs.show');
    Route::get('kycs/edit/{id}','AdminKycController@edit')->name('admin.kycs.edit');
    Route::match(['put', 'patch'], 'kycs/update/{id}','AdminKycController@update')->name('admin.kycs.update');
    Route::get('kycs/delete/{id}', 'AdminKycController@destroy')->name('admin.kycs.edit');
});




Route::group(array('module'=>'kyc','namespace' => 'App\modules\kyc\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('kycs/','KycController@index')->name('kycs');
    
});