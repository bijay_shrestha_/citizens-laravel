<?php

namespace App\Modules\Kyc\Model;


use Illuminate\Database\Eloquent\Model;

class Kyc extends Model
{
    public  $table = 'tbl_kyc';

    protected $fillable = ['kyc_id','a_id','mother_name','grandmother_name','daughter_name','father_in_law_name','son_name','daughter_in_law_name','expected_monthly_turnover','expected_monthly_transaction','purpose_of_account','source_of_fund','high_reason','if_beneficial_owner','beneficial_owner_name','beneficial_relation','beneficial_citizen','beneficial_address','beneficial_contact','expiry_date','is_high_risk_customer','source_of_fund_input','purpose_of_fund_input','if_nrn',];
}
