@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Kycs   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.kycs') }}">tbl_kyc</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.kycs.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                                    <label for="kyc_id">Kyc_id</label><input type="text" name="kyc_id" id="kyc_id" class="form-control" ></div><div class="form-group">
                                    <label for="a_id">A_id</label><input type="text" name="a_id" id="a_id" class="form-control" ></div><div class="form-group">
                                    <label for="mother_name">Mother_name</label><input type="text" name="mother_name" id="mother_name" class="form-control" ></div><div class="form-group">
                                    <label for="grandmother_name">Grandmother_name</label><input type="text" name="grandmother_name" id="grandmother_name" class="form-control" ></div><div class="form-group">
                                    <label for="daughter_name">Daughter_name</label><input type="text" name="daughter_name" id="daughter_name" class="form-control" ></div><div class="form-group">
                                    <label for="father_in_law_name">Father_in_law_name</label><input type="text" name="father_in_law_name" id="father_in_law_name" class="form-control" ></div><div class="form-group">
                                    <label for="son_name">Son_name</label><input type="text" name="son_name" id="son_name" class="form-control" ></div><div class="form-group">
                                    <label for="daughter_in_law_name">Daughter_in_law_name</label><input type="text" name="daughter_in_law_name" id="daughter_in_law_name" class="form-control" ></div><div class="form-group">
                                    <label for="expected_monthly_turnover">Expected_monthly_turnover</label><input type="text" name="expected_monthly_turnover" id="expected_monthly_turnover" class="form-control" ></div><div class="form-group">
                                    <label for="expected_monthly_transaction">Expected_monthly_transaction</label><input type="text" name="expected_monthly_transaction" id="expected_monthly_transaction" class="form-control" ></div><div class="form-group">
                                    <label for="purpose_of_account">Purpose_of_account</label><input type="text" name="purpose_of_account" id="purpose_of_account" class="form-control" ></div><div class="form-group">
                                    <label for="source_of_fund">Source_of_fund</label><input type="text" name="source_of_fund" id="source_of_fund" class="form-control" ></div><div class="form-group">
                                    <label for="high_reason">High_reason</label><input type="text" name="high_reason" id="high_reason" class="form-control" ></div><div class="form-group">
                                    <label for="if_beneficial_owner">If_beneficial_owner</label><input type="text" name="if_beneficial_owner" id="if_beneficial_owner" class="form-control" ></div><div class="form-group">
                                    <label for="beneficial_owner_name">Beneficial_owner_name</label><input type="text" name="beneficial_owner_name" id="beneficial_owner_name" class="form-control" ></div><div class="form-group">
                                    <label for="beneficial_relation">Beneficial_relation</label><input type="text" name="beneficial_relation" id="beneficial_relation" class="form-control" ></div><div class="form-group">
                                    <label for="beneficial_citizen">Beneficial_citizen</label><input type="text" name="beneficial_citizen" id="beneficial_citizen" class="form-control" ></div><div class="form-group">
                                    <label for="beneficial_address">Beneficial_address</label><input type="text" name="beneficial_address" id="beneficial_address" class="form-control" ></div><div class="form-group">
                                    <label for="beneficial_contact">Beneficial_contact</label><input type="text" name="beneficial_contact" id="beneficial_contact" class="form-control" ></div><div class="form-group">
                                    <label for="expiry_date">Expiry_date</label><input type="text" name="expiry_date" id="expiry_date" class="form-control" ></div><div class="form-group">
                                    <label for="is_high_risk_customer">Is_high_risk_customer</label><input type="text" name="is_high_risk_customer" id="is_high_risk_customer" class="form-control" ></div><div class="form-group">
                                    <label for="source_of_fund_input">Source_of_fund_input</label><input type="text" name="source_of_fund_input" id="source_of_fund_input" class="form-control" ></div><div class="form-group">
                                    <label for="purpose_of_fund_input">Purpose_of_fund_input</label><input type="text" name="purpose_of_fund_input" id="purpose_of_fund_input" class="form-control" ></div><div class="form-group">
                                    <label for="if_nrn">If_nrn</label><input type="text" name="if_nrn" id="if_nrn" class="form-control" ></div>
<input type="hidden" name="id" id="id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.kycs') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
