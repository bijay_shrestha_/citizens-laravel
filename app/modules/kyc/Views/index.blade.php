@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Kycs		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Kycs</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.kycs.create') }}" class="btn bg-green waves-effect"  title="create">Create</a>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="kyc-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								<th >Kyc_id</th>
<th >A_id</th>
<th >Mother_name</th>
<th >Grandmother_name</th>
<th >Daughter_name</th>
<th >Father_in_law_name</th>
<th >Son_name</th>
<th >Daughter_in_law_name</th>
<th >Expected_monthly_turnover</th>
<th >Expected_monthly_transaction</th>
<th >Purpose_of_account</th>
<th >Source_of_fund</th>
<th >High_reason</th>
<th >If_beneficial_owner</th>
<th >Beneficial_owner_name</th>
<th >Beneficial_relation</th>
<th >Beneficial_citizen</th>
<th >Beneficial_address</th>
<th >Beneficial_contact</th>
<th >Expiry_date</th>
<th >Is_high_risk_customer</th>
<th >Source_of_fund_input</th>
<th >Purpose_of_fund_input</th>
<th >If_nrn</th>

								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#kyc-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.kycs.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
					{ data: "kyc_id",name: "kyc_id"},
            { data: "a_id",name: "a_id"},
            { data: "mother_name",name: "mother_name"},
            { data: "grandmother_name",name: "grandmother_name"},
            { data: "daughter_name",name: "daughter_name"},
            { data: "father_in_law_name",name: "father_in_law_name"},
            { data: "son_name",name: "son_name"},
            { data: "daughter_in_law_name",name: "daughter_in_law_name"},
            { data: "expected_monthly_turnover",name: "expected_monthly_turnover"},
            { data: "expected_monthly_transaction",name: "expected_monthly_transaction"},
            { data: "purpose_of_account",name: "purpose_of_account"},
            { data: "source_of_fund",name: "source_of_fund"},
            { data: "high_reason",name: "high_reason"},
            { data: "if_beneficial_owner",name: "if_beneficial_owner"},
            { data: "beneficial_owner_name",name: "beneficial_owner_name"},
            { data: "beneficial_relation",name: "beneficial_relation"},
            { data: "beneficial_citizen",name: "beneficial_citizen"},
            { data: "beneficial_address",name: "beneficial_address"},
            { data: "beneficial_contact",name: "beneficial_contact"},
            { data: "expiry_date",name: "expiry_date"},
            { data: "is_high_risk_customer",name: "is_high_risk_customer"},
            { data: "source_of_fund_input",name: "source_of_fund_input"},
            { data: "purpose_of_fund_input",name: "purpose_of_fund_input"},
            { data: "if_nrn",name: "if_nrn"},
            
					{ data: function(data,b,c,table) { 
					var buttons = '';

					buttons += "<a class='btn bg-red waves-effect' href='"+site_url+"/edit/"+data.id+"' type='button' >Edit</a>&nbsp"; 

					buttons += "<a href='"+site_url+"/delete/"+data.id+"' class='btn bg-red waves-effect' >Delete</a>";

					return buttons;
					}, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
