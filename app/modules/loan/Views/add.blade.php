@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Loans   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.loans') }}">tbl_loans</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form"  enctype="multipart/form-data" action="{{ route('admin.loans.store') }}"  method="post">
                <div class="box-body">                
                                    <div class="form-group">
                                        <label for="faq_collection_id">Faq  type</label>
                                        <select name="faq_collection_id" class="form-control">
                                        @foreach($faq_collections as $faq_collection) 
                                                <option value="{{$faq_collection['faq_collection_id']}}"
                                                 @if (old('faq_collection_id') == $faq_collection['faq_collection_id']) selected @endif
                                                >{{$faq_collection['faq_collection_name']}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="parent_id">Parent</label>
                                        <select name="parent_id" class="form-control">
                                            <option value="0">Select Parent </option> 
                                            @foreach ($loan_parents as $loan_parent)
                                                <option value="{{$loan_parent['id']}}"
                                                  @if (old('parent_id') == $loan_parent['id']) selected @endif
                                                >{{$loan_parent['name']}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                    
                                    <div class="form-group">
                                    <label for="loan_name">Loan Name (*)</label><input type="text" name="loan_name" id="loan_name" class="form-control"  required></div>
                                                          <div>                          
                      <label>Add Interest Rate</label>
                    </div>
                         
                            <div id="input1" style="margin-bottom:4px; border:1px #CCC solid; padding:10px" class="clonedInput">
                            
                            
                            <div class="form-group">
                                <div id="label_rt"><label for="rate_name">Rate name</label></div>
                                <div id="input_rt"><input type="text" class="form-control" name="rate_name[]" id="rate_name"/></div>
                            </div>
                            <div class="form-group">
                                <div id="label_max_rt"><label for="rate_max">General Max Rate(% p.a)</label></div>
                                <div id="input_max_rt"><input type="text" class="form-control" name="general_max_rate[]" id="rate_max"/></div>
                            </div>
                            <div class="form-group">
                                <div id="label_min_rt"><label for="rate_min">General Min Rate(% p.a)</label></div>
                                <div id="input_min_rt"><input type="text" class="form-control" name="general_min_rate[]" id="rate_min"/></div>
                            </div>
                             <div class="form-group">
                                <div id="label_max_rt"><label for="rate_max">Prime A category Max Rate(% p.a)</label></div>
                                <div id="input_max_rt"><input type="text" class="form-control" name="primea_max_rate[]" id="rate_max"/></div>
                            </div>
                            <div class="form-group">
                                <div id="label_min_rt"><label for="rate_min">Prime A category Min Rate(% p.a)</label></div>
                                <div id="input_min_rt"><input type="text" class="form-control" name="primea_min_rate[]" id="rate_min"/></div>
                            </div>
                             <div class="form-group">
                                <div id="label_max_rt"><label for="rate_max">Prime B category Max Rate(% p.a)</label></div>
                                <div id="input_max_rt"><input type="text" class="form-control" name="primeb_max_rate[]" id="rate_max"/></div>
                            </div>
                            <div class="form-group">
                                <div id="label_min_rt"><label for="rate_min">Prime B category Min Rate(% p.a)</label></div>
                                <div id="input_min_rt"><input type="text" class="form-control" name="primeb_min_rate[]" id="rate_min"/></div>
                            </div>
                            <div class="form-group">
                              <label>Notes</label>
                              <div id="input_note"><input type="text" class="form-control" name="notes[]" id="notes" /></div>
                            </div>
                            <div class="form-group">
                                <div id="label_status"><label for="rate_status">Status</label></div>
                                <div id="input_status">
                                <select name="rate_status[]" class="form-control" id="rate-status">
                                  <option value="1">ON</option>
                                  <option value="0">OFF</option>
                                </select>
                               
                                </div>
                            </div>
                          </div>  

                            <div>
                             <input type="button" id="btnAdd" value="Add more Rates" />
                             <input type="button" id="btnDel" value="remove Rate" />
                          </div>


                                    <div class="form-group">
                                    <label for="fixed_rate">Fixed Rate</label><input type="text" name="fixed_rate" id="fixed_rate" class="form-control" ></div>

                                    <div class="form-group">
                                        <label for="banner_collection_id">Banner Collection </label>
                                        <select name="banner_collection_id" class="form-control">
                                            <option value="0">Select Banner </option> 
                                            @foreach ($banner_collections as $banner_collection)
                                                <option value="{{$banner_collection['banner_collection_id']}}" 
                                                 @if (old('banner_collection_id') == $banner_collection['banner_collection_id']) selected @endif
                                                > {{$banner_collection['name']}} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="instant_cash">Instant Cash</label>
                                        <div class="radio">
                                        <label>
                                            <input type="radio" value="1" name="instant_cash" id="instant_cash1" @if(old('instant_cash') == 1) checked @endif/>Yes
                                        </label> 
                                        <label>
                                            <input type="radio" value="0" name="instant_cash" id="instant_cash0" @if(old('instant_cash') == 1) @else checked @endif />No</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="image_name">Image</label><br>
                                        <label id="upload_image_name" style="display:none"></label>
                                        <input type="text" name="image_name" id="image_name" class="form-control" style="display:none" />
                                        <div id="image-status"></div>
                                        <input type="file" id="upload_image" name="upload_image" style="display:block"/>
                                        <a href="javascript:void(0)" id="change-image" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>
                                    </div>

                                    <div class="form-group">
                                    <label for="description">Description</label>
                                     <textarea name="description" id="description" class="form-control my-editor" >{!! old('description') !!}</textarea>
                                    </div><div class="form-group">
                                    <label for="facilities">Facilities</label>
                                    <textarea name="facilities" id="facilities" class="form-control my-editor" >{!! old('facilities') !!}</textarea>
                                    </div>
                                    
                                    <div class="form-group">
                                              <label>Download Form file</label><br/><label id="upload_download_link" style="display:none"></label>
                                              <!-- <input type="text" name="download_form_link" id="download_form_link"  value="" class="form-control"> -->
                                              <input type="file" name="download_link" id="download_link" >
                                              <div id="file-status"></div>
                                              <input type="hidden" name="download_form_link" id="download_form_link"/>
                                              <a href="javascript:void(0)" id="change-file" title="Delete" style="display:none"><img src="{{('/images/logo/cancel.png')}}" border="0"/></a>

                                    </div>

                                    <div class="form-group">
                                    
                                    <div class="form-group">
                                        <label for="status">Status</label>
                                        <div class="radio">
                                        <label>
                                            <input type="radio" value="1" name="status" id="status1" @if(old('status') == 1) checked @endif  />Yes
                                        </label> 
                                        <label>
                                            <input type="radio" value="0" name="status" id="status0" @if(old('status') == 1) @else  checked @endif  />No
                                        </label>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                    </div>
                             
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.loans') }}" class="btn btn-danger">Cancel</a>
                </div>

            </form>
        </div>
    </section>
    <script type="text/javascript">
    var directory = 'loan';
</script>
@include('data_changing_script_create')
@include('file_changing_script')

@endsection
