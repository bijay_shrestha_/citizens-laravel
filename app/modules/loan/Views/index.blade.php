@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Loans		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Loans</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.loans.create') }}" class="btn bg-green waves-effect"  title="create">Create</a>
					</div>
					<!-- /.box-header -->
					<br>
					@if ($message = Session::get('success'))
					<div class="btn btn-success">
						{{$message}}
					</div>
					@endif
					<div class="box-body">
						<table id="loan-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
<th >Parent</th>
<th >Loan Name</th>
<th >Interest_rate</th>
<th >Fixed_rate</th>
<th >Account</th>
<th >Instant_cash</th>
<th >Form Link</th>
<th >Approvel_status</th>
<th >Approved</th>
<th >Status</th>

								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#loan-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.loans.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
		    { data: "parentname",name: "parentname"},
            { data: "loan_name",name: "loan_name"},
            { data: "interest_rate",name: "interest_rate"},
            { data: "fixed_rate",name: "fixed_rate"},
            { data: "account",name: "account"},
             { data: function(data){
            	if (data.instant_cash == 1) {
            		return "<img src='{{url('/images/logo/accept.png')}}'>";
            	} else {
            		return "<img src='{{url('/images/logo/cancel.png')}}'>";
            	}
            	;
            }, name:'instant_cash', searchable: false},
            { data: function (data,b,c,table) {
            	var thefile = '';
            	if (data.download_form_link) {
            		thefile = '<a href="{{url("/uploads/loan/forms")}}/'+data.download_form_link+'" target="_blank">'+data.download_form_link+'</a>';
            	} 

            	return thefile;           	
            }, name:'download_form_link', searchable: false},
            { data: "approvel_status",name: "approvel_status"},
            { data: function (data)  {
            	var content = "";

            	content += "<a href='{{url('/admin/loans/change/')}}/approved/" + data.approvel_status + "/" + data.loan_id + "'style='color:";
            	if (data.approved == "approved") { content += "green"; }
            	content += "'>Approved</a><br>";
            	
            	content += "<a href='{{url('/admin/loans/change/')}}/pending/" + data.approvel_status + "/" + data.loan_id + "'style='color:";
            	if (data.approved == "pending") { content += "green"; }
            	content += "'>Pending</a><br>";

            	content += "<a href='{{url('/admin/loans/change/')}}/denied/" + data.approvel_status + "/" + data.loan_id + "'style='color:";
            	if (data.approved == "denied") { content += "green"; }
            	content += "'>Denied</a><br>";
            	
            	return content;
            },name:'approved', searchable: false},
           { data: function(data){
            	if (data.status == 1) {
            		return "<img src='{{url('/images/logo/accept.png')}}'>";
            	} else {
            		return "<img src='{{url('/images/logo/cancel.png')}}'>";
            	}
            	;
            }, name:'status', searchable: false},
             
					{ data: function(data,b,c,table) { 
					var buttons = '';

					buttons += "<a class='btn bg-green waves-effect' href='"+site_url+"/edit/"+data.loan_id+"' type='button' ><i class='fa fa-edit'></i> Edit</a>&nbsp"; 

					buttons += "<a href='"+site_url+"/delete/"+data.loan_id+"' class='btn bg-red waves-effect' > <i class='fa fa-trash'></i> Delete</a>";

					return buttons;
					}, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
