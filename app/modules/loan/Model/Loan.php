<?php

namespace App\Modules\Loan\Model;


use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    public  $table = 'tbl_loans';

    protected $fillable = ['loan_id','parent_id','template_id','business_type','loan_name','interest_rate','fixed_rate','account','instant_cash','slug_id','slug_name','image_name','banner_collection_id','faq_collection_id','description','facilities','download_form_link','approvel_status','approved','deleted_by','deleted_date','del_flag','status','added_date','modified_date',];
}
