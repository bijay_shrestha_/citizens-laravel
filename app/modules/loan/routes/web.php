<?php



Route::group(array('prefix'=>'admin/','module'=>'loan','middleware' => ['web','auth'], 'namespace' => 'App\modules\loan\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('loans/','AdminLoanController@index')->name('admin.loans');
    Route::post('loans/getloansJson','AdminLoanController@getloansJson')->name('admin.loans.getdatajson');
    Route::get('loans/create','AdminLoanController@create')->name('admin.loans.create');
    Route::post('loans/store','AdminLoanController@store')->name('admin.loans.store');
    Route::get('loans/show/{id}','AdminLoanController@show')->name('admin.loans.show');
    Route::get('loans/edit/{id}','AdminLoanController@edit')->name('admin.loans.edit');
    Route::match(['put', 'patch'], 'loans/update/{id}','AdminLoanController@update')->name('admin.loans.update');
    Route::get('loans/delete/{id}', 'AdminLoanController@destroy')->name('admin.loans.edit');
    Route::get('loans/change/{text}/{status}/{id}', 'AdminLoanController@change');

});




Route::group(array('module'=>'loan','namespace' => 'App\modules\loan\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('loans/','LoanController@index')->name('loans');
    
});