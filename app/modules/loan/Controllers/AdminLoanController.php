<?php

namespace App\modules\loan\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\loan\Model\Loan;
use App\modules\banner_collection\Model\Banner_collection;
use App\modules\faq_collection\Model\Faq_collection;
use App\modules\loan_parent\Model\Loan_parent;
use App\modules\account\Controllers\AdminAccountController;
use App\modules\loan_interest_rate\Model\Loan_interest_rate;
use Carbon\Carbon;
use Validator;
use File;
use Image;


class AdminLoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Loan';
        return view("loan::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getloansJson(Request $request)
    {
        $loan = DB::table('tbl_loans');
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $loan->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )
        ->leftjoin('tbl_loan_parent', 'tbl_loan_parent.id', '=', 'tbl_loans.parent_id')
        ->select('tbl_loans.*', 'tbl_loan_parent.name as parentname')
        ->where('tbl_loans.del_flag',0)->orderBy('tbl_loans.loan_id', "DSC")
        ->get();
        // Display limited list        
        $rows = $loan->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('tbl_loans.del_flag',0)->orderBy('tbl_loans.loan_id', "DSC")->get();

        //To count the total values present
        $total = $loan->where('tbl_loans.del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Loan | Create';
        $banner_collections = Banner_collection::all();
        $faq_collections = Faq_collection::all();
        $loan_parents = Loan_parent::all();
        return view("loan::add",compact('page','banner_collections', 'faq_collections', 'loan_parents'));

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $interest_rates = Loan_interest_rate::all();
        $admin_account_controller = new AdminAccountController;
        $name_slug = $admin_account_controller->slugify($data['loan_name']);
        $slug_name = $admin_account_controller->validate_slug($name_slug);
        $slug_data = DB::table('slugs')->insert(['slug_name' => $slug_name]); //create slug
        $allslug =  DB::table('slugs')->get();
        $dataforloan = $data;
        $rating_clones = count($dataforloan['rate_name']);
        unset($dataforloan['rate_name']);
        unset($dataforloan['general_max_rate']);
        unset($dataforloan['general_min_rate']);
        unset($dataforloan['primea_max_rate']);
        unset($dataforloan['primea_min_rate']);
        unset($dataforloan['primeb_max_rate']);
        unset($dataforloan['primeb_min_rate']);
        unset($dataforloan['notes']);
        unset($dataforloan['rate_status']);
        unset($dataforloan['id']);
        unset($dataforloan['download_link']);
        unset($dataforloan['upload_image']);
        
        $dataforloan['slug_id'] = $allslug[count($allslug)-1]->slug_id;
        $dataforloan['slug_name'] = $slug_name;
        //$dataforloan['created_by'] = Auth::user()->id;
        $dataforaccount['del_flag'] = 0;
        $dataforloan['added_date'] = new Carbon();        
        $dataforloan = check_case($dataforloan, 'create');        
        $success = Loan::Insert($dataforloan);
        $loans = Loan::all();
        $loan_id = $loans[count($loans)-1]['loan_id'];
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "add", 'date' => new Carbon(), 'file' => "LOANS", 'file_id' => $loan_id]); //create acrivity log
        $slug['route'] = 'loan/details/' . $loan_id;
        DB::table('slugs')->where('slug_id', $dataforloan['slug_id'])->update(['route' => $slug['route']]);
        $interest_rates = [];
        for ($i = 0; $i < $rating_clones; $i++) {
            $interest_rate = [];
            $interest_rate['loan_id'] = $loan_id;
            $interest_rate['rate_name'] = $request->rate_name[$i];
            $interest_rate['general_per_annum_min'] = $request->general_min_rate[$i];
            $interest_rate['general_per_annum_max'] = $request->general_max_rate[$i];
            $interest_rate['prime_a_category_max'] = $request->primea_max_rate[$i];
            $interest_rate['prime_a_category_min'] = $request->primea_min_rate[$i];
            $interest_rate['prime_b_category_max'] = $request->primeb_max_rate[$i];
            $interest_rate['prime_b_category_min'] = $request->primeb_min_rate[$i];
            $interest_rate['notes'] = $request->notes[$i];
            $interest_rate['status'] = $request->rate_status[$i];
            $interest_rate['added_by'] = Auth::user()->id;
            $interest_rate['added_date'] = new Carbon();
            $success_interest_rate = Loan_interest_rate::Insert($interest_rate); //create interest rate
            $interest_rates["data".($i +1)] = $interest_rate; 
        }
        return redirect()->route('admin.loans')->with('success', "Data added Successfully.");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $loan = Loan::where('loan_id',$id)->first();
        $page['title'] = 'Loan | Update';
        $banner_collections = Banner_collection::all();
        $faq_collections = Faq_collection::all();
        $loan_parents = Loan_parent::all();
        $loan_interest_rates = Loan_interest_rate::where('loan_id', $id)->where('del_flag', 0)->get();
        return view("loan::edit",compact('page','loan','banner_collections', 'faq_collections', 'loan_parents', 'loan_interest_rates'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');
        //dd($data);
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "update", 'date' => new Carbon(), 'file' => "LOANS", 'file_id' => $id]); //create activity log
        $loan = Loan::where('loan_id', $id)->firstOrFail();
        $admin_account_controller = new AdminAccountController;
        $name_slug = $admin_account_controller->slugify($data['loan_name']);
        $slug['slug_name'] = $admin_account_controller->validate_slug($name_slug, $loan->slug_id);
        $slug['route'] = 'loan/details/'.$id;         //$data['loan_id'] = $id
        DB::table('slugs')->where('slug_id', $loan->slug_id)->update(['slug_name' =>  $slug['slug_name'], 'route' => $slug['route']]);                
        $dataforloan = $data;
        $rating_clones = count($dataforloan['rate_name']);
        unset($dataforloan['id']);
        unset($dataforloan['rate_id']);
        unset($dataforloan['rate_name']);
        unset($dataforloan['general_max_rate']);
        unset($dataforloan['general_min_rate']);
        unset($dataforloan['primea_max_rate']);
        unset($dataforloan['primea_min_rate']);
        unset($dataforloan['primeb_max_rate']);
        unset($dataforloan['primeb_min_rate']);
        unset($dataforloan['rate_status']);
        unset($dataforloan['notes']);
        unset($dataforloan['loan_id']);
        unset($dataforloan['download_link']);
        unset($dataforloan['upload_image']);
        $dataforloan['slug_name'] = $slug['slug_name'];
        
        if (array_key_exists('delete_old_image', $data)) {
            if ((file_exists(public_path().'/uploads/loan/'. $loan->image_name))  && $loan->image_name) {
                unlink(public_path().'/uploads/loan/'. $loan->image_name);
                if (file_exists(public_path().'/uploads/loan/thumb/'. $loan->image_name)) {
                    unlink(public_path().'/uploads/loan/thumb/'. $loan->image_name);
                } 
            }
            if (!$data['image_name']) {
                $data['image_name'] = '';
                $dataforloan['image_name'] = '';
            }
            unset($dataforloan['delete_old_image']);
        } else {
            if (!$data['image_name']) {
                $data['image_name'] = $loan->image_name;
                $dataforloan['image_name'] = $loan->image_name;
            }
        }

        if (array_key_exists('delete_old_file', $data)) {
            if ((file_exists(public_path().'/uploads/loan/form/'. $loan->download_form_link)) && $loan->download_form_link) {
                unlink(public_path().'/uploads/loan/form/'. $loan->download_form_link);                
            }
            if (!$data['download_form_link']) {
                $data['download_form_link'] = '';
                $dataforloan['download_form_link'] = '';
            }
            unset($dataforloan['delete_old_file']);
        } else {
            if (!$data['download_link']) {
                $data['download_form_link'] = $loan->download_form_link;
                $dataforloan['download_form_link'] = $loan->download_form_link;
            }            
        }

        $dataforloan['modified_date'] = new Carbon();
        $dataforloan = check_case($dataforloan, 'edit');        
        DB::table('tbl_loans')->where('loan_id', $id)->update($dataforloan);

        //for updating rates
        $loan_interest_rates = Loan_interest_rate::where('loan_id', $id)->where('del_flag', 0)->get();
        $ids = [];
        for ($i = 0 ; $i < count($loan_interest_rates); $i++) {
            $thecount = 0;
            for ($j = 0; $j < count($request->rate_name); $j++) {
                
                if (($request->rate_id[$j] == $loan_interest_rates[$i]->rate_id)) {
                    $thecount++;
                } 
            }
            if ($thecount == 0) {
                /*$interest_rate['del_flag'] = 1;
                $interest_rate['deleted_by'] = Auth::user()->id;
                $interest_rate['deleted_at'] = new Carbon();
                $success = Loan_interest_rate::where('rate_id', $loan_interest_rates[$i]->rate_id)->update($interest_rate);
                */
                $data_delete = soft_delete('tbl_loan_interest_rates', 'rate_id', $loan_interest_rates[$i]->rate_id, 'deleted_by', 'deleted_date');
            }
        }        
        for ($k = 0; $k < count($request->rate_name); $k++) {
                $interest_rate = [];

                $interest_rate['rate_name'] = $request->rate_name[$k];
                $interest_rate['general_per_annum_min'] = $request->general_min_rate[$k];
                $interest_rate['general_per_annum_max'] = $request->general_max_rate[$k];
                $interest_rate['prime_a_category_max'] = $request->primea_max_rate[$k];
                $interest_rate['prime_a_category_min'] = $request->primea_min_rate[$k];
                $interest_rate['prime_b_category_max'] = $request->primeb_max_rate[$k];
                $interest_rate['prime_b_category_min'] = $request->primeb_min_rate[$k];
                $interest_rate['notes'] = $request->notes[$k];
                $interest_rate['status'] = $request->rate_status[$k];

                $interest_rate['rate_name'] = $request->rate_name[$k];
            
            if ($request->rate_id[$k] == null || $request->rate_id[$k] == '') {
                $interest_rate['loan_id'] = $id;
                $interest_rate['added_by'] = Auth::user()->id;
                $interest_rate['added_date'] = new Carbon();
                $success_interest_rate = Loan_interest_rate::Insert($interest_rate); //create interest rate
            } else {
                for ($l =0; $l <count($loan_interest_rates); $l++) {
                    if (($request->rate_id[$k] == $loan_interest_rates[$l]->rate_id)) {
                       // $success = Loan_interest_rate::where('rate_id', $request->rate_id[$k])->update($interest_rate);
                        $success = DB::table('tbl_loan_interest_rates')->where('rate_id', $request->rate_id[$k])->update($interest_rate);
                        
                    }
                }
            }
        }
        

        return redirect()->route('admin.loans')->with('success', "Data updated Successfully.");
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = soft_delete('tbl_loans', 'loan_id', $id, 'deleted_by', 'deleted_date');
        DB::table('activity_log')->insert(['user_id' => Auth::user()->id, 'action' => "delete", 'date' => new Carbon(), 'file' => "LOANS", 'file_id' => $id]); //create acrivity log
        return redirect()->route('admin.loans')->with('success', "Data deleted for trash.");

        //
    }

    public function change($text, $status, $id) 
    {
        $data = change_status($text, $status, $id, 'tbl_loans', 'loan_id', 'deleted_by', 'deleted_date');
        return redirect()->route('admin.loans')->with('success', "Data updated Successfully.");
    }

}
