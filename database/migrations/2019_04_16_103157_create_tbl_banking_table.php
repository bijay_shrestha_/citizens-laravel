<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblBankingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_banking', function (Blueprint $table) {
            $table->increments('banking_id');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->text('short_description')->nullable();
            $table->string('image_name')->nullable();
            $table->integer('banner_collection_id')->default(0);
            $table->string('download_form')->nullable();
            $table->string('approvel_status');
            $table->string('approved');
            $table->string('slug_name')->nullable();
            $table->integer('slug_id');
            $table->tinyInteger('status');
            $table->tinyInteger('del_flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_banking');
    }
}
