<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPersonalDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_personal_details', function (Blueprint $table) {

            $table->increments('p_id');
            $table->string('first_name');
            $table->string('midle_name')->nullable();
            $table->string('last_name');
            $table->string('permanent_address');
            $table->string('temporaray_address')->nullable();
            $table->string('dob');
            $table->string('p_zone');
            $table->string('p_district');
            $table->string('t_zone')->nullable();
            $table->string('t_district')->nullable();
            $table->string('nationality');
            $table->string('cv');
            $table->string('citizenship_no');
            $table->string('language');
            $table->string('phone_no')->nullable();
            $table->string('email');
            $table->enum('gender',['Male','Female']);
            $table->enum('marital_status',['Married','Unmarried']);
            $table->string('immediate_contact')->nullable();
            $table->string('mobile_no')->nullable();
            $table->string('contact_no')->nullable();
            $table->tinyInteger('status');
            $table->tinyInteger('del_flag');
            $table->string('father_name');
            $table->string('grand_father_name')->nullable();
            $table->dateTime('posted_date')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
