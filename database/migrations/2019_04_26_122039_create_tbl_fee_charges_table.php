<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblFeeChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_fee_charges', function (Blueprint $table) {
            $table->increments('fee_charge_id');
            $table->integer('parent_id')->default(0);
            $table->string('fee_charge_name');
            $table->string('file_name')->nullable();
            $table->string('rupee');
            $table->string('usd');
            $table->text('fee_and_charges')->nullable();
            $table->string('approvel_status');
            $table->string('approved');
            $table->integer('added_by');
            $table->dateTime('added_date');
            $table->dateTime('modified_date')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->dateTime('deleted_date')->nullable();
            $table->tinyInteger('del_flag');
            $table->tinyInteger('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_fee_charges');
    }
}
