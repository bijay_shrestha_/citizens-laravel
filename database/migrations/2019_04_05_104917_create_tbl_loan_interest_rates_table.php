<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblLoanInterestRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_loan_interest_rates', function (Blueprint $table) {
            



            $table->increments('rate_id');
            $table->integer('loan_id');
            $table->string('rate_name')->nullable();
            $table->string('general_per_annum_min')->nullable();
            $table->string('general_per_annum_max')->nullable();
            $table->float('prime_a_category_min', 10, 2)->nullable();
            $table->float('prime_a_category_max', 10, 2)->nullable();
            $table->float('prime_b_category_min', 10, 2)->nullable();
            $table->float('prime_b_category_max', 10, 2)->nullable();
            $table->text('notes')->nullable();
            $table->tinyInteger('status');
            $table->dateTime('added_date');
            $table->integer('added_by');
            $table->dateTime('modified_date')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->dateTime('deleted_date')->nullable();
            $table->tinyInteger('del_flag')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_loan_interest_rates');
    }
}
