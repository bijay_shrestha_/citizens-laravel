<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCreditCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_credit_cards', function (Blueprint $table) {
           



            $table->increments('credit_card_id');
            $table->integer('parent_id')->default(0);
            $table->integer('template_id')->default(0);
            $table->enum('business_type', ['PERSONAL', 'BUSINESS', 'CORPORATE']);
            $table->string('credit_card_name')->nullable();
            $table->integer('slug_id');
            $table->string('slug_name');
            $table->integer('faq_collection_id')->default(0);
            $table->integer('banner_collection_id')->default(0);
            $table->string('image_name')->nullable();
            $table->text('description')->nullable();
            $table->text('short_description')->nullable();
            $table->float('representative_apr', 10, 2)->nullable();
            $table->float('purchase_rate', 10, 2)->nullable();
            $table->string('credit_limit')->default('0');
            $table->string('download_form_link')->nullable();
            $table->string('apply_online_link')->nullable();
            $table->string('sequence')->default('0');
            $table->string('approvel_status')->default("ok");
            $table->string('approved');
            $table->dateTime('added_date');
            $table->dateTime('modified_date')->nullable();
            $table->tinyInteger('del_flag')->default(0);
            $table->integer('deleted_by')->nullable();
            $table->dateTime('deleted_date')->nullable();
            $table->string('notes')->nullable();
            $table->tinyInteger('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_credit_cards');
    }
}
