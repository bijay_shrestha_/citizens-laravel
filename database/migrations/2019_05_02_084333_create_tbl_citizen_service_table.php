<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCitizenServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_citizen_service', function (Blueprint $table) {
            $table->increments('s_id');
            $table->integer('a_id');
            $table->tinyInteger('debit_card')->default(0);
            $table->tinyInteger('mobile_banking_services')->default(0);
            $table->tinyInteger('internet_banking_services')->default(0);
            $table->tinyInteger('locker_facilities')->default(0);
            $table->tinyInteger('ntc')->default(0);
            $table->tinyInteger('e_statement')->default(0);
            $table->tinyInteger('standing_instruction')->default(0);
            $table->tinyInteger('other_service')->default(0);
            $table->tinyInteger('bancassurance')->default(0);
            $table->tinyInteger('otherservice_name')->default(0);
            $table->tinyInteger('is_banking_password')->default(0);
            $table->tinyInteger('is_internet_banking_password')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_citizen_service');
    }
}
