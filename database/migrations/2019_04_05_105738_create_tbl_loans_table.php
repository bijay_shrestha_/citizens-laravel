<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_loans', function (Blueprint $table) {
            



            $table->increments('loan_id');
            $table->integer('parent_id')->default(0);
            $table->integer('template_id')->default(0);
            $table->enum('business_type', ['PERSONAL','BUSINESS', 'CORPORATE']);
            $table->string('loan_name');
            $table->float('interest_rate')->nullable();
            $table->float('fixed_rate')->nullable();
            $table->integer('account')->default(0);
            $table->tinyInteger('instant_cash')->default(0);
            $table->integer('slug_id');
            $table->string('slug_name');
            $table->string('image_name')->nullable();
            $table->integer('banner_collection_id')->default(0);
            $table->integer('faq_collection_id')->default(0);
            $table->text('description')->nullable();
            $table->text('facilities')->nullable();
            $table->string('download_form_link')->nullable();
            $table->string('apply_online_link')->default(0);
            $table->string('submit_form_link')->default(0);
            $table->string('approvel_status')->default("ok");
            $table->string('approved')->default("approved");
            $table->integer('deleted_by')->nullable();
            $table->dateTime('deleted_date')->nullable();
            $table->tinyInteger('del_flag')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->dateTime('added_date');
            $table->dateTime('modified_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_loans');
    }
}
