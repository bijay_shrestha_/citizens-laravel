<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblMediaContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_media_contact', function (Blueprint $table) {
            $table->increments('blog_id');
            $table->string('blog_title');
            $table->string('tags')->nullable();
            $table->integer('slug_id');
            $table->string('slug_name');
            $table->string('image_name')->nullable();
            $table->text('contents');    
            $table->string('approvel_status');
            $table->string('approved');
            $table->dateTime('added_date');
            $table->integer('added_by');
            $table->date('post_date')->nullable();
            $table->dateTime('modified_date')->nullable();
            $table->tinyInteger('status');
            $table->tinyInteger('del_flag');
            $table->integer('deleted_by')->nullable();
            $table->dateTime('deleted_date')->nullable();
            $table->integer('sequence');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_media_contact');
    }
}
