<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPublicationCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_publication_categories', function (Blueprint $table) {
            $table->increments('category_id');
            $table->integer('parent_id')->default(0);
            $table->string('category_name');
            $table->integer('sequence')->default(0);
            $table->string('approvel_status');
            $table->string('approved');
            $table->dateTime('added_date');
            $table->integer('added_by');
            $table->dateTime('modified_date')->nullable();
            $table->dateTime('deleted_date')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->tinyInteger('del_flag');
            $table->tinyInteger('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_publication_categories');
    }
}
