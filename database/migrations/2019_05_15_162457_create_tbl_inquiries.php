<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblInquiries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
Schema::create('tbl_inquiries', function (Blueprint $table) {
    $table->increments('id');
    $table->string('name')->nullable();
    $table->string('sector_email')->nullable();
    $table->string('contact_no')->nullable();
    $table->text('notes')->nullable();
    $table->string('redirect')->nullable();
    $table->integer('sequence')->nullable();
    $table->integer('added_by')->nullable();
    $table->date('added_date')->nullable();
    $table->date('modified_date')->nullable();
    $table->integer('del_flag')->nullable();
    $table->integer('deleted_by')->nullable();
    $table->date('deleted_date')->nullable();
    $table->integer('status')->nullable();													
    });		
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
