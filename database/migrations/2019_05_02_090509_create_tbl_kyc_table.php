<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblKycTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_kyc', function (Blueprint $table) {
           



            $table->increments('kyc_id');
            $table->string('a_id')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('grandmother_name')->nullable();
            $table->text('daughter_name')->nullable();
            $table->string('father_in_law_name')->nullable();
            $table->text('son_name')->nullable();
            $table->text('daughter_in_law_name')->nullable();
            $table->string('expected_monthly_turnover')->nullable();
            $table->string('expected_monthly_transaction')->nullable();
            $table->string('purpose_of_account')->nullable();
            $table->string('source_of_fund')->nullable();
            $table->text('high_reason')->nullable();
            $table->tinyInteger('if_beneficial_owner')->default("0");
            $table->string('beneficial_owner_name')->default("0");
            $table->string('beneficial_relation')->default("0");
            $table->string('beneficial_citizen')->default("0");
            $table->string('beneficial_address')->default("0");
            $table->string('beneficial_contact')->default("0");
            $table->date('expiry_date')->nullable();
            $table->tinyInteger('is_high_risk_customer')->default(0);
            $table->string('source_of_fund_input')->default("0");
            $table->string('purpose_of_fund_input')->default("0");
            $table->tinyInteger('if_nrn')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_kyc');
    }
}
