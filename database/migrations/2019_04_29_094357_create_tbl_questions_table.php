<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_questions', function (Blueprint $table) {
            $table->increments('question_id');
            $table->text('question');
            $table->tinyInteger('status');
            $table->dateTime('added_date');
            $table->integer('added_by');            
            $table->dateTime('modified_date')->nullable();
            $table->integer('deleted_by')->nullable();            
            $table->dateTime('deleted_date')->nullable();            
            $table->tinyInteger('del_flag');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_questions');
    }
}
