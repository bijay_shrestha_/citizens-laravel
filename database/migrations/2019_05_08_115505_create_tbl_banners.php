<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblBanners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_banners', function (Blueprint $table) {




            $table->increments('banner_id');
            $table->integer('banner_collection_id');
            $table->string('image_name')->nullable();
            $table->enum('business_type', ['PERSONAL', 'BUSINESS', 'CORPORATE'])->nullable();
            $table->text('top_box_description')->nullable();
            $table->text('bottom_box_description')->nullable();
            $table->tinyInteger('hide_description');
            $table->string('logo')->nullable();
            $table->string('background')->nullable();
            $table->string('link')->nullable();
            $table->string('link_title')->nullable();
            $table->tinyInteger('new_window')->default(0);
            $table->dateTime('added_date');
            $table->dateTime('modified_date')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->integer('sort_order')->default(0);
            $table->string('position')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->dateTime('deleted_date')->nullable();
            $table->tinyInteger('del_flag')->default(0);
            $table->tinyInteger('status');
            $table->tinyInteger('type')->default(0);
            $table->string('color')->nullable();
            $table->string('bottom-color')->nullable();
            $table->string('design')->nullable();
            $table->string('freepik_link')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_banners');
    }
}
