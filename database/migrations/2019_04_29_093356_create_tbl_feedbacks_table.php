<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_feedbacks', function (Blueprint $table) {




            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('telephone');
            $table->string('account_num');
            $table->text('comment')->nullable();
            $table->dateTime('send_date');
            $table->dateTime('deleted_date')->nullable();
            $table->tinyInteger('del_flag');
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_feedbacks');
    }
}
