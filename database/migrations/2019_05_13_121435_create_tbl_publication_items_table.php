<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPublicationItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_publication_items', function (Blueprint $table) {
           



            $table->increments('item_id');
            $table->integer('category_id')->default(0);
            $table->string('item_name');
            $table->string('file_name')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->text('contain')->nullable();
            $table->tinyInteger('show_homepage')->default(0);
            $table->integer('sequence')->default(0);
            $table->string('approvel_status');
            $table->string('approved');
            $table->dateTime('added_date');
            $table->integer('added_by');
            $table->dateTime('modified_date')->nullable();
            $table->dateTime('deleted_date')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->tinyInteger('del_flag');
            $table->tinyInteger('status');
            $table->tinyInteger('allow_download')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_publication_items');
    }
}
