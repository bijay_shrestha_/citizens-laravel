<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblAccountInterestRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_account_interest_rates', function (Blueprint $table) {
            $table->increments('rate_id');
            $table->integer('account_id');
            $table->string('rate_name')->nullable();
            $table->float('per_annum_min', 10, 2)->nullable();
            $table->float('per_annum_max', 10, 2)->nullable();
            $table->tinyInteger('status');
            $table->integer('added_by');
            $table->integer('deleted_by')->nullable();
            $table->dateTime('deleted_date')->nullable();
            $table->tinyInteger('del_flag')->default(0);
            $table->dateTime('added_date');
            $table->dateTime('modified_date')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_account_interest_rates');
    }
}
