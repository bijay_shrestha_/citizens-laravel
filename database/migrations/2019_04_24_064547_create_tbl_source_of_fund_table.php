<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblSourceOfFundTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_source_of_fund', function (Blueprint $table) {
            $table->increments('id');
            $table->string('source_of_fund');
            $table->tinyInteger('show_or_hide');
            $table->tinyInteger('del_flag');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_source_of_fund');
    }
}
