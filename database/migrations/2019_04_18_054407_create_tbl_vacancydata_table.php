<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblVacancydataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_vacancydata', function (Blueprint $table) {
            



            $table->increments('id');
            $table->string('preferred_post');
            $table->string('specialized_area')->nullable();
            $table->string('vacancy_code')->nullable();
            $table->text('description')->nullable();
            $table->string('approvel_status');
            $table->string('approved');
            $table->tinyInteger('master');
            $table->tinyInteger('bachelor');
            $table->tinyInteger('experience');
            $table->tinyInteger('del_flag');
            $table->integer('created_by');
            $table->dateTime('created_date');
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified_date')->nullable();
            $table->dateTime('vacancy_start_date')->nullable();
            $table->dateTime('vacancy_end_date')->nullable();
            

                                         

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_vacancydata');
    }
}
