<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_service', function (Blueprint $table) {
            $table->increments('service_id');
            $table->string('service_name', 1000);
            $table->text('description')->nullable();
            $table->text('short_description')->nullable();
            $table->string('image_name')->nullable();
            $table->string('slugname')->nullable();
            $table->string('approvel_status')->default("ok");
            $table->enum('approved', ['pending', 'approved', 'denied', ''])->default("approved");
            $table->tinyInteger('status')->default(0);
            $table->integer('banner_collection_id')->default(0);
            $table->tinyInteger('del_flag')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_service');
    }
}
