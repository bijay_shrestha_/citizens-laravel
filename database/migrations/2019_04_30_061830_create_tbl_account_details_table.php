<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblAccountDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_account_details', function (Blueprint $table) {
            



            $table->increments('a_id');
            $table->string('reference_code');
            $table->string('applying_from');
            $table->string('branch');
            $table->string('photo');
            $table->string('type');
            $table->string('account_category');
            $table->string('types_of_individual');
            $table->string('account_type');
            $table->string('currency');
            $table->text('account_name');
            $table->text('father_name');
            $table->text('grandfather_name');
            $table->text('spouse_name');
            $table->string('existing_relationship');
            $table->string('account_number');
            $table->text('dob');
            $table->text('dob_nepali');
            $table->text('gender');
            $table->text('occupation');
            $table->text('nationality');
            $table->text('citizenship_no');
            $table->text('passport_no');
            $table->text('nrn_card');
            $table->text('birth_certificate');
            $table->string('other_id');
            $table->text('place_of_issue');
            $table->text('date_of_issue');
            $table->text('date_of_issue_bs');
            $table->string('age_minor')->nullable();
            $table->string('name_of_guardian')->nullable();
            $table->string('relationship_minor')->nullable();
            $table->text('marital_status');
            $table->text('email');
            $table->text('pan_number');
            $table->text('phone_number');
            $table->text('mobile');
            $table->text('fax_number');
            $table->text('pox_box');
            $table->text('permanent_address_house_no');
            $table->text('permanent_address_vdc');
            $table->text('permanent_address_ward_number');
            $table->text('permanent_address_street');
            $table->text('permanent_address_district');
            $table->text('correspondence_address_house_no');
            $table->text('correspondence_address_vdc');
            $table->text('correspondence_address_ward_number');
            $table->text('correspondence_address_street');
            $table->text('correspondence_address_district');
            $table->enum('account_status', ['New','Denied', 'Approved', 'Pending', '']);
            $table->string('company_name')->nullable();
            $table->string('company_address')->nullable();
            $table->string('com_position')->nullable();
            $table->text('expiry_date');
            $table->string('foreign_address');
            $table->string('country');
            $table->string('contact_no');
            $table->string('type_of_visa')->nullable();
            $table->date('visa_expiry_date')->nullable();
            $table->string('city_state');
            $table->string('account_no')->nullable();
            $table->text('remark')->nullable();
            $table->date('applied_date');
            $table->tinyInteger('del_flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_account_details');
    }
}
