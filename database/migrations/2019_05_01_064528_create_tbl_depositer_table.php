<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDepositerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_depositer', function (Blueprint $table) {
            



            $table->increments('d_id');
            $table->string('a_id');
            $table->string('mr_miss');
            $table->string('family');
            $table->string('dob');
            $table->string('age');
            $table->string('relationship');
            $table->string('permanent_address');
            $table->string('contact_address');
            $table->string('tel_no');
            $table->string('authorized_signature');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_depositer');
    }
}
