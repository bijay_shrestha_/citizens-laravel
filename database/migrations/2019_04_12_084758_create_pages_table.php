<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            



            $table->increments('page_id');
            $table->string('page_title');
            $table->text('description')->nullable();
            $table->string('image_name')->nullable();
            $table->enum('business_type', ['PERSONAL', 'BUSINESS', 'CORPORATE']);
            $table->integer('content_type_id')->default(0);
            $table->integer('parent_id')->default(0);
            $table->integer('template_id')->default(0);
            $table->tinyInteger('default')->default(0);
            $table->integer('sequence')->default(0);
            $table->text('meta_keywords')->nullable();
            $table->text('meta_description')->nullable();
            $table->dateTime('created_date');
            $table->dateTime('modified_date')->nullable();
            $table->integer('slug_id')->default(0);
            $table->string('slug_name')->nullable();
            $table->string('download_form_link')->nullable();
            $table->string('apply_form_link')->nullable();
            $table->string('submit_form_link')->nullable();
            $table->string('url')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->dateTime('deleted_date')->nullable();
            $table->tinyInteger('new_window')->default(0);
            $table->tinyInteger('is_menu')->default(0);
            $table->tinyInteger('del_flag')->default(0);
            $table->tinyInteger('status')->default(0);            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
