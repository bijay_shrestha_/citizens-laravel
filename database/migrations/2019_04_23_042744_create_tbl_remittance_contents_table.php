<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblRemittanceContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_remittance_contents', function (Blueprint $table) {
            



            $table->increments('id');
            $table->string('title');
            $table->text('content')->nullable();
            $table->tinyInteger('status');
            $table->tinyInteger('del_flag');
            $table->dateTime('added_date');
            $table->integer('added_by');
            $table->dateTime('modified_date')->nullable();
            $table->integer('modified_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_remittance_contents');
    }
}
