<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDepositerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_depositer', function (Blueprint $table) {

         


            $table->increments('d_id');
            $table->string('a_id');
            $table->text('mr_miss')->nullable();
            $table->text('depositer_name')->nullable();
            $table->text('relationship')->nullable();
            $table->text('nominee_images')->nullable();
            $table->text('images')->nullable();
            $table->text('nominee_citizenship_fronts')->nullable();
            $table->text('nominee_citizenship_backs')->nullable();
            $table->text('citizenship_fronts')->nullable();
            $table->text('citizenship_backs')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_depositer');
    }
}
