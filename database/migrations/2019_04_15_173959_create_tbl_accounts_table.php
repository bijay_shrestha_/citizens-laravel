<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_accounts', function (Blueprint $table) {




            $table->increments('account_id');
            $table->integer('parent_id')->default(0);
            $table->integer('template_id')->default(0);
            $table->enum('business_type', ['PERSONAL','BUSINESS', 'CORPORATE']);
            $table->string('name');
            $table->integer('slug_id');
            $table->string('slug_name');
            $table->string('image_name')->nullable();
            $table->integer('banner_collection_id')->default(0);
            $table->integer('faq_collection_id')->default(0);
            $table->text('description')->nullable();
            $table->text('facilities')->nullable();
            $table->string('account')->nullable();
            $table->float('interest_rate', 10, 2)->nullable();
            $table->string('fixed_rate')->nullable();
            $table->string('min_balance')->nullable();
            $table->string('download_form_link')->nullable();
            $table->string('apply_online_link')->nullable();
            $table->string('submit_form_link')->nullable();
            $table->tinyInteger('instant_cash')->default(0);
            $table->string('approvel_status');
            $table->enum('approved', ['pending', 'approved', 'denied', '']);
            $table->integer('sequence')->default(0);
            $table->dateTime('added_date');
            $table->integer('added_by');
            $table->dateTime('modified_date')->nullable();
            $table->dateTime('deleted_date')->nullable();
            $table->tinyInteger('del_flag')->default(0);
            $table->integer('deleted_by')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('is_online')->default(0);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_accounts');
    }
}
