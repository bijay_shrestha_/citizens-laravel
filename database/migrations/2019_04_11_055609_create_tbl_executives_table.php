<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblExecutivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_executives', function (Blueprint $table) {

            $table->increments('executive_id');
            $table->enum('team_type', ['BOD Committe','Management Team','Senior Executive','Department']);
            $table->string('salutation', 20);
            $table->string('executive_name');
            $table->string('designation')->nullable();
            $table->string('branch')->nullable();
            $table->string('image_name')->nullable();
            $table->string('facebook')->nullable();
            $table->string('committe')->nullable();
            $table->text('note')->nullable();
            $table->string('approvel_status');
            $table->string('approved');
            $table->integer('added_by');
            $table->dateTime('added_date');
            $table->dateTime('modified_date')->nullable();
            $table->tinyInteger('del_flag')->default(0);
            $table->integer('deleted_by')->nullable();
            $table->dateTime('deleted_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_executives');
    }
}
