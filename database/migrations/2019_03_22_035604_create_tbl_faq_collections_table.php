<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblFaqCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_faq_collections', function (Blueprint $table) {
            $table->increments('faq_collection_id');
            $table->string('faq_collection_name');
            $table->integer('sequence');
            $table->string('approvel_status')->default("ok");
            $table->string('approved')->default("approved");
            $table->integer('added_by');
            $table->integer('deleted_by')->nullable();
            $table->dateTime('deleted_date')->nullable();
            $table->tinyInteger('del_flag')->default(0);
            $table->tinyInteger('status');
            $table->dateTime('added_date');
            $table->dateTime('modified_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_faq_collections');
    }
}
