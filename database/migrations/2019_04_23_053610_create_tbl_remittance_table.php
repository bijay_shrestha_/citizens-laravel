<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblRemittanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_remittance', function (Blueprint $table) {
            



            $table->increments('id');
            $table->string('type')->nullable();
            $table->string('remittance_company')->nullable();
            $table->string('remittance_product')->nullable();
            $table->string('remittance_receiving_countries')->nullable();
            $table->string('approvel_status');
            $table->string('approved');
            $table->tinyInteger('status');
            $table->tinyInteger('del_flag');
            $table->dateTime('added_date');
            $table->integer('added_by');
            $table->dateTime('modified_date')->nullable();
            $table->integer('modified_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_remittance');
    }
}
