<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblReferenceOrganization extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_reference_organization', function (Blueprint $table) {
            $table->increments('r_id');
            $table->integer('p_id');
            $table->string('reference_1');
            $table->string('post_1');
            $table->string('organization_1');
            $table->string('address_1');
            $table->string('email_1');
            $table->string('telephone_1');
            $table->string('reference_2')->nullable();
            $table->string('post_2')->nullable();
            $table->string('organization_2')->nullable();
            $table->string('address_2')->nullable();
            $table->string('email_2')->nullable();
            $table->string('telephone');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
