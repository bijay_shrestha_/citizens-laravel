<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('departments', function (Blueprint $table) {
            $table->string('contact_no')->nullable();
            $table->string('email')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->integer('branch_id')->default(1);
            $table->string('approvel_status')->default("ok");
            $table->string('approved')->default("approved");
            $table->integer('created_by')->default(1);
            $table->integer('modified_by')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('departments', function (Blueprint $table) {
            //
        });
    }
}
