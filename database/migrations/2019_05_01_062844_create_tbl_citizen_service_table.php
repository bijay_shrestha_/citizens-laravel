<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCitizenServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_citizen_service', function (Blueprint $table) {
            $table->increments('s_id');
            $table->integer('a_id');
            $table->string('visa');
            $table->string('mobile_banking');
            $table->string('internet_banking');
            $table->string('locker_facilities');
            $table->string('ntc');
            $table->string('e-statement');
            $table->string('standing_instruction');
            $table->string('other_service');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_citizen_service');
    }
}
