<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblEducation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_education', function (Blueprint $table) {

            $table->increments('e_id');
            $table->string('p_id');
            $table->string('s_board')->nullable();
            $table->string('s_school')->nullable();
            $table->string('s_degree')->nullable();
            $table->string('s_marks')->nullable();
            $table->string('s_division')->nullable();
            $table->date('s_passed_year')->nullable();
            $table->string('c_university')->nullable();
            $table->string('c_college')->nullable();
            $table->string('c_degree')->nullable();
            $table->string('c_marks')->nullable();
            $table->string('c_division')->nullable();
            $table->date('c_passed_year')->nullable();
            $table->string('b_university')->nullable();
            $table->string('b_college')->nullable();
            $table->string('b_faculty')->nullable();
            $table->string('b_marks')->nullable();
            $table->string('b_division')->nullable();
            $table->date('b_passed_year')->nullable();
            $table->string('m_university')->nullable();
            $table->string('m_college')->nullable();
            $table->string('m_faculty')->nullable();
            $table->string('m_marks')->nullable();
            $table->string('m_division')->nullable();
            $table->date('m_passed_year')->nullable();
            $table->string('course_name')->nullable();
            $table->string('institude_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
