<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblOtherMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_other_menu', function (Blueprint $table) {
            



            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('link')->nullable();
            $table->string('approvel_status');
            $table->string('approved');
            $table->tinyInteger('status')->nullable();
            $table->dateTime('created_date')->nullable();
            $table->integer('created_by')->nullable();
            $table->tinyInteger('del_flag')->nullable();
            $table->string('type')->nullable();
            $table->tinyInteger('is_download');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_other_menu');
    }
}
