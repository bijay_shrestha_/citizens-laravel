<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
Schema::create('tbl_contacts', function (Blueprint $table) {

    
    
    $table->increments('contact_id');
    $table->string('sector')->nullable();
    $table->integer('branch')->nullable();
    $table->string('name')->nullable();
    $table->string('email')->nullable();
    $table->string('contact_no')->nullable();
    $table->text('message')->nullable();
    $table->text('reason')->nullable();
    $table->date('date')->nullable();
    $table->string('country')->nullable();
    $table->string('address')->nullable();
    $table->integer('post_code')->nullable();
    $table->integer('added_by')->nullable();
    $table->date('added_date')->nullable();
    $table->date('modified_date')->nullable();
    $table->integer('del_flag')->nullable();
    $table->integer('deleted_by')->nullable();
    $table->date('deleted_date')->nullable();
    $table->integer('status')->nullable();
});		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
