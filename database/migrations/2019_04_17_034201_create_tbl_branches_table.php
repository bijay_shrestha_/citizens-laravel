<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_branches', function (Blueprint $table) {
   



            $table->increments('id');
            $table->string('name');
            $table->string('branch_manager');
            $table->string('address');
            $table->string('email');
            $table->string('contact_no')->nullable();
            $table->string('fax')->nullable();
            $table->double('longitude', 10, 5);
            $table->double('latitude', 10, 5);
            $table->enum('for_section', ['ATM locations','Bank Branches', 'Corporate']);
            $table->string('location');
            $table->string('city');
            $table->string('state')->default("Nepal");
            $table->tinyInteger('wheelchair_access')->default(1);
            $table->integer('withdrawal_limit')->default(0);
            $table->string('redirect')->default('branch');
            $table->string('approvel_status');
            $table->string('approved');
            $table->integer('added_by');
            $table->dateTime('added_date');
            $table->dateTime('modified_date')->nullable();
            $table->tinyInteger('del_flag')->default(0);
            $table->integer('deleted_by')->nullable();
            $table->dateTime('deleted_date')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->integer('district_id');
            
           });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_branches');
    }
}
