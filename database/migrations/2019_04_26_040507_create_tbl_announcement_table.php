<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblAnnouncementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_announcement', function (Blueprint $table) {
            $table->increments('a_id');
            $table->string('title');
            $table->text('content')->nullable();
            $table->string('image_name')->nullable();
            $table->integer('banner_collection_id');
            $table->integer('cat_id');
            $table->date('date')->nullable();
            $table->date('ex_date')->nullable();
            $table->string('approvel_status');
            $table->string('approved');
            $table->tinyInteger('del_flag');
            $table->tinyInteger('status');
            $table->dateTime('created_at');
            $table->dateTime('deleted_date')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_announcement');
    }
}
