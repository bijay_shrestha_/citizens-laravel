<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblInterestRateSpreadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_interest_rate_spread', function (Blueprint $table) {
            $table->increments('interest_rate_spread_id');
            $table->string('name');
            $table->float('interest_rate_spread_rate', 10 ,2)->nullable();
            $table->date('date')->nullable();
            $table->string('approvel_status');
            $table->string('approved');
            $table->integer('added_by');
            $table->dateTime('added_date');
            $table->dateTime('modified_date')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->dateTime('deleted_date')->nullable();
            $table->tinyInteger('del_flag');
            $table->tinyInteger('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_interest_rate_spread');
    }
}
