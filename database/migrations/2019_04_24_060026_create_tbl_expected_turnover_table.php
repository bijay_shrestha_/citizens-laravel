<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblExpectedTurnoverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_expected_turnover', function (Blueprint $table) {
            $table->increments('id');
            $table->string('expected_turnover');
            $table->tinyInteger('show_or_hide');
            $table->tinyInteger('del_flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_expected_turnover');
    }
}
