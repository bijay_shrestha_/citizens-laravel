<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPreferenceAvailable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_preference_available', function (Blueprint $table) {

            $table->increments('a_id');
            $table->integer('p_id');
            $table->string('preferred_post');
            $table->string('specialised_area');
            $table->string('preferred_location');
            $table->string('expected_salary');
            $table->string('driving_license');
            $table->string('travelling_option');
            $table->string('vacancy_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
