<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblForexUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_forex_uploads', function (Blueprint $table) {
            $table->increments('forex_upload_id');
            $table->string('filename');
            $table->integer('added_by');
            $table->dateTime('added_date');
            $table->dateTime('deleted_date')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->integer('del_flag');
            $table->enum('status', ['pending', 'cancel', 'imported']);
            $table->dateTime('imported_date');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_forex_uploads');
    }
}
