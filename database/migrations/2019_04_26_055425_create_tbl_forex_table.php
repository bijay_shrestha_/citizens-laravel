<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblForexTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_forex', function (Blueprint $table) {
            



            $table->increments('ex_rate_id');
            $table->string('currency');
            $table->integer('unit');
            $table->string('cash_buying')->nullable();
            $table->string('buying')->nullable();
            $table->float('selling', 10, 4)->nullable();
            $table->dateTime('ex_date');
            $table->integer('sequence');
            $table->string('approvel_status');
            $table->string('approved');
            $table->dateTime('added_date');
            $table->integer('added_by');
            $table->dateTime('modified_date')->nullable();
            $table->dateTime('deleted_date')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->tinyInteger('del_flag');
            $table->tinyInteger('status');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_forex');
    }
}
