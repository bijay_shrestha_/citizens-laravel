<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_product', function (Blueprint $table) {
            $table->increments('p_id');
            $table->string('image')->nullable();
            $table->string('link')->nullable();
            $table->string('logo')->nullable();
            $table->string('background')->nullable();
            $table->text('short_text')->nullable();
            $table->string('name')->nullable();
            $table->tinyInteger('type');
            $table->string('approvel_status');
            $table->string('approved');
            $table->tinyInteger('status');
            $table->tinyInteger('del_flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_product');
    }
}
