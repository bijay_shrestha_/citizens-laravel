<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblWorkingExperience extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_working_experience', function (Blueprint $table) {
            $table->increments('w_id');
            $table->integer('p_id');
            $table->string('organization')->nullable();
            $table->string('position')->nullable();
            $table->string('department')->nullable();
            $table->string('experience')->nullable();
            $table->string('organization2')->nullable();
            $table->string('organization3')->nullable();
            $table->string('position2')->nullable();
            $table->string('position3')->nullable();
            $table->string('department2')->nullable();
            $table->string('department3')->nullable();
            $table->string('experience2')->nullable();
            $table->string('experience3')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
