<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblFaqTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_faq', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('collection_id');
            $table->string('for_type')->nullable();
            $table->text('heading')->nullable();
            $table->text('contain')->nullable();
            $table->string('redirect')->default('faq');
            $table->string('approvel_status');
            $table->string('approved');
            $table->integer('added_by');
            $table->dateTime('added_date');
            $table->dateTime('modified_date')->nullable();
            $table->tinyInteger('del_flag');
            $table->integer('deleted_by')->nullable();
            $table->dateTime('deleted_date')->nullable();
            $table->tinyInteger('status');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_faq');
    }
}
