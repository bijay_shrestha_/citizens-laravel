<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblAuthorizationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_authorization', function (Blueprint $table) {
            $table->increments('auth_id');
            $table->string('a_id');
            $table->string('longitude');
            $table->string('latitude');
            $table->string('signature1')->nullable();
            $table->string('signature2')->nullable();
            $table->string('signature3')->nullable();
            $table->string('name1')->nullable();
            $table->string('name2')->nullable();
            $table->mediumText('name3')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_authorization');
    }
}
