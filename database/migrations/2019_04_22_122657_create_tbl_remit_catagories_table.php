<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblRemitCatagoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_remit_catagories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('sequence');
            $table->tinyInteger('status');
            $table->tinyInteger('del_flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_remit_catagories');
    }
}
