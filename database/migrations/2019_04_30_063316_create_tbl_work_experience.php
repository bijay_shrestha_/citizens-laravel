<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblWorkExperience extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_work_experience', function (Blueprint $table) {
            $table->increments('w_id');
            $table->integer('p_id')->nullable();
            $table->string('organization')->nullable();
            $table->string('position')->nullable();
            $table->string('department')->nullable();
            $table->string('experience')->nullable();
            $table->date('experience_from')->nullable();
            $table->date('experience_to')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
