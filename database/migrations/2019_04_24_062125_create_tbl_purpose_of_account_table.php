<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPurposeOfAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_purpose_of_account', function (Blueprint $table) {
            $table->increments('id');
            $table->string('purpose_of_account');
            $table->tinyInteger('show_or_hide');
            $table->tinyInteger('del_flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_purpose_of_account');
    }
}
