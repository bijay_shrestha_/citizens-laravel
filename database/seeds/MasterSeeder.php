<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
class MasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'username'      =>'superadmin',
                'control'       => 1,
                'last_visit'    =>date('Y-m-d H:m:s'),
                'status'        =>0,
                'activation_key'=>0,
                'email'         =>'superadmin@system.co',
                'password'      =>Hash::make('password'),
                'created_at'    =>date('Y-m-d H:m:s'),
                'updated_at'    =>date('Y-m-d H:m:s'),
            ],
            [
                'username'      =>'admin',
                'control'       => 1,
                'last_visit'    =>date('Y-m-d H:m:s'),
                'status'        =>0,
                'activation_key'=>0,
                'email'         =>'admin@system.co',
                'password'      =>Hash::make('password'),
                'created_at'    =>date('Y-m-d H:m:s'),
                'updated_at'    =>date('Y-m-d H:m:s'),
            ],
        ];

        $role=[
                'name'                  =>'administrator',
                'display_name'          =>'ADMIN',
                'description'           =>'administrator',
                'created_at'    =>date('Y-m-d H:m:s'),
                'updated_at'    =>date('Y-m-d H:m:s'),
              ];
        $role_user=[
                [
                    'user_id'   =>'1',
                    'role_id'   =>'1'
                ],
                [
                'user_id'   =>'2',
                'role_id'   =>'1'
                ]
        ];
        $permission =[
            'name'                  =>'controlPanel',
            'display_name'          =>'Control Panel',
            'description'           =>'Control Panel Access Permission!!',
            'created_at'    =>date('Y-m-d H:m:s'),
            'updated_at'    =>date('Y-m-d H:m:s'),
        ];

        $permission_role=[
            'permission_id'     =>'1',
            'role_id'           =>'1'
        ];

        DB::table('users')->insert($user);
        DB::table('roles')->insert($role);
        DB::table('role_user')->insert($role_user);
        DB::table('permissions')->insert($permission);
        DB::table('permission_role')->insert($permission_role);
    }
}
