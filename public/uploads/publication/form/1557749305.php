
	<table border="1">
        <tbody>
            <tr>
            <td>Branch</td>
            <td>Type</td>
            <td>Account Category</td>
            <td>Account Type</td>
            <td>Currency</td>
            <td>Account Name</td>
            <td>Applicant Father Name</td>
            <td>Applicant Grandfather Name</td>
            <td>Spouse Name</td>
            <td>Existing Relationship</td>
            <td>Account Number</td>
            <td>DOB</td>
            <td>DOB Nepali</td>
            <td>Occupation</td>
            <td>Nationality</td>
            <td>Citizenship No.</td>
            <td>Passport No.</td>
            <td>Place of Issue</td>
            <td>Date of Issue</td>
            <td>Age of Minor</td>
            <td>Name of Guardian</td>
            <td>Relationship Minor</td>
            <td>Marital Status</td>
            <td>Email</td>
            <td>Phone No.</td>
            <td>Mobile No.</td>
            <td>Fax No</td>
            <td>Pox Box	Corresponding Address</td>
            <td>Permanent Address House No.</td>
            <td>Permanent Address VDC/Municipality</td>
            <td>Permanent Address Ward No.</td>
            <td>Permanent Address District</td>
            <td>Corresponding Address House No.</td>
            <td>Corresponding Address VDC/Municipality</td>
            <td>Corresponding Address Ward No.</td>
            <td>Depositers Nomination name</td>
            <td>Relationship</td>
            <td>Debit Card</td>
            <td>Mobile Banking</td>
            <td>Internet Banking Services</td>
            <td>Longitude</td>
            <td>Latitude</td>
            <td>Mothers Name</td>
            <td>Grand Mothers Name</td>
            <td>Daughters Name</td>
            <td>Sons Name</td>
            <td>Daughter in laws Name</td>
            <td>Father in laws Name</td>
            <td>Expected Monthly Turnover</td>
            <td>Expected Monthly Transaction</td>
            <td>Purpose Of Account</td>
            <td>Source Of Fund</td>
            <td>Image</td>
            <td>Citizen Front</td>
            <td>Citizen Back</td>
            <td>Nominee Image</td>
            <td>Nominee Citizen Front</td>
            <td>Nominee Citizen Back</td>
            </tr>

            @foreach($data as $d)
            <tr>
            <td> {{ $d->branch }} </td>
            <td> {{ $d->type}} </td>
            <td> {{ $d->account_category}} </td>
            <td> {{ $d->account_type}} </td>
            <td> {{ $d->currency}} </td>
            <td> {{ $d->account_name}} </td>
            <td> {{ $d->father_name}} </td>
            <td> {{ $d->grandfather_name}} </td>
            <td> {{ $d->spouse_name}} </td>
            <td> {{ $d->existing_relationship}} </td>
            <td> {{ $d->account_number}} </td>
            <td> {{ $d->dob}} </td>
            <td> {{ $d->dob_nepali}} </td>
            <td> {{ $d->occupation}} </td>
            <td> {{ $d->nationality}} </td>
            <td> {{ $d->citizenship_number}} </td>
            <td> {{ $d->passport_no}} </td>
            <td> {{ $d->place_of_issue}} </td>
            <td> {{ $d->date_of_issue}} </td>
            <td> {{ $d->age_minor}} </td>
            <td> {{ $d->name_of_guardian}} </td>
            <td> {{ $d->relationship_minor}} </td>
            <td> {{ $d->marital_status}} </td>
            <td> {{ $d->email}} </td>
            <td> {{ $d->phone_number}} </td>
            <td> {{ $d->fax_number}} </td>
            <td> {{ $d->pox_box}} </td>
            <td> {{ $d->permanent_address_house_no}} </td>
            <td> {{ $d->permanent_address_vdc}} </td>
            <td> {{ $d->permanent_address_ward_number}} </td>
            <td> {{ $d->permanent_address_district}} </td>
            <td> {{ $d->correspondence_address_house_no}} </td>
            <td> {{ $d->correspondence_address_vdc}} </td>
            <td> {{ $d->correspondence_address_ward_number}} </td>
            <td>  Depositors name </td>
            <td>  Relationship </td>
            <td>  Debit Card </td>
            <td>  Mobile banking </td>
            <td>  Internet Banking </td>
            <td>  Longitude </td>
            <td>  Latitude </td>
            <td>  Mothers name </td>
            <td>  Grand mothers name </td>
            <td>  Daughters name </td>
            <td>  Sons Name  </td>
            <td>Daughter in laws Name</td>
            <td>Father in laws Name</td>
            <td>Expected Monthly Turnover</td>
            <td>Expected Monthly Transaction</td>
            <td>Purpose Of Account</td>
            <td>Source Of Fund</td>
            <td>Image</td>
            <td>Citizen Front</td>
            <td>Citizen Back</td>
            <td>Nominee Image</td>
            <td>Nominee Citizen Front</td>
            <td>Nominee Citizen Back</td>
            </tr>
             @endforeach

        </tbody>
    </table>	
    