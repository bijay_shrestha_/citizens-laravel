-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 06, 2019 at 04:36 PM
-- Server version: 5.5.60-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_labels`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cms`
--

CREATE TABLE IF NOT EXISTS `tbl_cms` (
  `cms_pk_id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_title` varchar(255) NOT NULL,
  `cms_description` text,
  `cms_feature_image` varchar(255) DEFAULT NULL,
  `cms_link` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `identifier` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`cms_pk_id`),
  UNIQUE KEY `cms_pk_id` (`cms_pk_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_cms`
--

INSERT INTO `tbl_cms` (`cms_pk_id`, `cms_title`, `cms_description`, `cms_feature_image`, `cms_link`, `status`, `identifier`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(1, 'HeellOO  titLe one first', '<p>1aaaaaadf<strong>aaaaaaaaa</strong>asds<em>fffffffff</em></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>sds</em></p>\r\n', NULL, 'heelloo-title-one-first', 1, '1ddd267758e4a2c143f905fdb9f1f28d', 1, '2019-02-06 12:53:39', 1, '2019-02-06 14:15:00'),
(2, 'ReellOO  titLe one first', '<p>ooo<em>aaaaa</em></p>\r\n', NULL, 'reelloo-title-one-first', 1, 'dc24b7ccebc0b8078f60d7724007a1d8', 1, '2019-02-06 13:04:43', 0, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
