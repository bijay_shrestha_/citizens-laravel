(function ($) {
  $(function () {
    $(document).off('click.bs.tab.data-api', '[data-hover="tabb"]');
    $(document).on('mouseenter.bs.tab.data-api', '[data-toggle="tabb"], [data-hover="tabb"]', function () {
      $(this).tab('show');
    });
  });
})(jQuery);
