var $ = jQuery;

$(document).ready(function(){

    $('input').click(function() {
        var category = $(this).val();

        var matchedItems = $('.' + category).each(function () {
            var anyChecked = false;
            var classArray = this.className.split(/\s+/);

            for(idx in classArray)
            {
                if ($('#filter-' + classArray[idx]).is(":checked"))
                {
                    anyChecked = true;
                    break;
                }
            }

            if (! anyChecked) $(this).hide();
            else $(this).show();

        });

    });


    $(window).on('load resize', function () {
        dimensions();
    })


    sliderInit();
    mcustomInit();



});



function dimensions() {
    winHeight = $(window).height();
    winWidth = $(window).width();

    /*------------ Taking The Height Start ------------*/

    var accountleftheight = $('.left-big-contianer').outerHeight();
    var sidescrollheadingheight = $('.side-scroll-heading').outerHeight();
    
    
    /*------------- Taking The Height End -------------*/

    $('.right-small-contianer .sidebar-content').height((accountleftheight) -(sidescrollheadingheight) );

}

function sliderInit() {
    $('.bannerSlider').slick({
        arrows: true,
        dots: false,
        autoplay: true,
        speed: 200,
        fade: true,
        cssEase: 'linear'
    });
}

function mcustomInit(){
    $(".nav-large-width").mCustomScrollbar({
        theme: "dark-thin"
    });
/*    $(".scroll-hori").mCustomScrollbar({
      axis:"x",
      theme:"dark-3"
    });*/
}