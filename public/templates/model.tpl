{PHP_TAG}

namespace App\Modules\{MODEL_NAME_UCFIRST}\Model;


use Illuminate\Database\Eloquent\Model;

class {MODEL_NAME_UCFIRST} extends Model
{
    public  $table = '{PREFIX}{TABLE_NAME}';

    protected $fillable = [{MODEL_FORM_FIELD}];
}
