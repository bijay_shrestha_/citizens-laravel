@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit {MODULE_NAME}   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.{MODULE}') }}">{TABLE_NAME}</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.{MODULE}.update') }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                {EDITFIELDS}
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.{MODULE}') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
