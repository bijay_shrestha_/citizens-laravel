<?php
return [
    'modules'=>array(
        "developer_tool", 	//Do Not Change These Files      
        "permission",	//Do Not Change These Files
        "permission_role", 	//Do Not Change These Files      
        "role",	//Do Not Change These Files
        "role_user",  	//Do Not Change These Files     
        "user",	//Do Not Change These Files
/*
        "company",
        "social_setting",
        
*/

        //"department",
        "department_contact",
        "branch",
        "account",
        "banner_collection",
        "account_parent",
        "faq_collection",
        "account_interest_rate",
        "loan",
        "loan_parent",
        "loan_interest_rate",
        "credit_card",
        "service",
        "executive",
        "cr_network",
        "page",
        "other_menu",
        "banking",
        "official_notice",
        "vacancydatum",
        "notice",
        "blog",
        "media_contact",
        "csr",
        "remit_catagory",
        "remittance_content",
        "remittance",
        "faq",
        "expected_transaction",
        "expected_turnover",
        "purpose_of_account",
        "source_of_fund",
        "social_media_link",
        "activity_log",
        "advertisement",
        "product",
        "notice_category",
        "announcement",
        "forex",
        "base_rate",
        "interest_rate_spread",
        "fee_charge",
        "fees_commision",
        "personal_detail",
        "feedback",
        "account_detail",
        "authorization",
        "citizen_service",
        "depositer",
        "kyc",
        "banner",
        "stock",
        "publication_category",
        "publication_item",
        "contact",
        "inquiry",
    ),
];